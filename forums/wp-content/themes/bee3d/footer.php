<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>

   <div class="clearfix"></div>
</div>
</div>
</div> 



<!-- *************************************************************************************** -->
<div class="wrapper row7">
    <div class="container main-contain">
        <footer>
            <div class="row">
                <div class="col-md-4 brd-rt">
                    
                    
                    <?php wp_nav_menu( array( 'menu_class' => 'links',  'theme_location' => 'footerfirstwidget','container' => false ) ); ?>
                    
                    
                   
                </div>
                <div class="col-md-4 brd-rt">
                    <div class="text-center">
                        <a href="#"> <img src="<?php echo get_template_directory_uri(); ?>/images/footer-logo.png" alt="" /></a>
                    </div>
                    <div>
                        <ul class="social-link">
                            <li class="fb"> <a href="#"> &nbsp;</a></li>
                            <li class="tw"> <a href="#" > &nbsp;</a></li>
                            <li class="gm"> <a href="#" > &nbsp;</a></li>
                            <li class="in"> <a href="#"> &nbsp;</a></li>
                        </ul>
                    </div>
                    <p class="text-center"> Copyright 2014 iHive3D M</p>
                </div>

                <div class="col-md-4">
                    <h1 class="font-light white">SIGN UP FOT IHIVE 3D NEWS </h1>
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">First Name</label>
                            <div class="col-sm-7">
                                <input type="email" class="form-control" id="inputEmail3" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Email Address</label>
                            <div class="col-sm-7">
                                <input type="email" class="form-control" id="inputEmail3">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Check which applies</label>
                            <div class="col-sm-offset-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox">3D Newbie
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox">3D Intermediate
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox">3D Pro
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </footer>

    </div>
</div>       

<!---footer complete---->



<?php wp_footer(); ?>
</body>
</html>