<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bee3d_dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'i=a#DH0eq5m($rxyiL^y$X1{28>8lGPIdcoL$]}O_W=zEPg~UG=~;elvg]+TBF&R');
define('SECURE_AUTH_KEY',  'T9q3#ybB+? Y#]z5h!w+3v H4(>u0#m?]gE!ZQVV55rkt~)_!zn]7&Ww?nf-$+_V');
define('LOGGED_IN_KEY',    '`Vt6zP=E(`[;=3#BTZ^(8J-vrD@|:|_[fexGmKsfmo_2FZhxpSX}h7C:pH});:OK');
define('NONCE_KEY',        'g6$C`HtHN~zwxbAJ k.R1XoWE3V{WiuKSZm[nm~xK-vPo(9{!PQD)]ISnT_[Dwcg');
define('AUTH_SALT',        'YbG(PcA,hrzh{<5E)2ZvG*8=_|FQK8-6qt>z{NC-?j%{:o IoG_ILdQ1dq6mtB^m');
define('SECURE_AUTH_SALT', 'N+TQ^9JfOi E&L:_kjH)Qo4:Gq_K}!uuFYhT@nyG17$,RInwP=(Up#TxGaW^4C]~');
define('LOGGED_IN_SALT',   'TKil&zo0/N(0U!4CwSh+IHE!.>mVJKne3-_HK;y7`8H{pcupxhYpa.ubKbQLTNfG');
define('NONCE_SALT',       't<7w,Lqz#8oScxVFL&xe~5ReTjkoP3f::5B$RPCiLr<<ZFfcZrn7jx@0_RH3oEPR');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'iweb_xforum_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');