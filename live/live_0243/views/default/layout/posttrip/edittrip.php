<style type="text/css">
#ui-datepicker-div, .ui-datepicker{ font-size: 85%; }
</style>
 
<script type="text/javascript" src="<?php echo base_url().getThemeName(); ?>/js/bootstrap.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().getThemeName(); ?>/css/bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url().getThemeName(); ?>/js/jquery.tagsinput.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
<script>
    $(function() {
        $("#complete_date").datepicker({minDate:0});
		$("#confirm_date").datepicker({minDate:0});
		$('#destination_desc_tag').tagsInput({width:'auto'});
		
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
$("#total_day").keydown(function(event) {
        // Allow: backspace, delete, tab, escape, and enter
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });
$("#min_budget").keydown(function(event) {
        // Allow: backspace, delete, tab, escape, and enter
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });
$("#max_budget").keydown(function(event) {
        // Allow: backspace, delete, tab, escape, and enter
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });

});
</script>	
 <script type="text/javascript">
function setnight(x)
{
				if(isNaN(x) == false)
				{
				if(x >= 32)
				{
				document.getElementById('total_day').value='';
				}
				x++;
				document.getElementById('total_night').value=x;
				}
				else
				{
				return false;
				}
				
				var y = document.getElementById('total_night').value;
				if(y >= 32)
				{
					document.getElementById('total_night').value='';
				}
				
				
			}
</script>


<div class="contener">
                    <div class="clear"></div>
                    <div id="bidcontener">
                    	<div class="bid_form_center">
                        <?php 
						if($msg!='')
						{
							if($msg == 'fail')
							{?>
								 <div id="error" class="msgsuccess"><ul><?php  echo "Trip update fail"; ?></ul></div>
							<?php }
							if($msg == 'success' )
							{?>
								 <div id="error" class="msgsuccess"><ul><?php  echo "Trip Updated Successfully"; ?></ul></div>
							<?php }
						}
						?>
                        
                         <?php if($this->input->post('edittrip')) { if($error!=''){  ?>
                            <div id="error" class="errmsgcl"><ul><?php  echo $error; ?></ul></div>
                         <?php } } ?>
                    <div  class="tab_content" style="display: inline-block;">
                        <div class="tab1_cont">
                            <div class="tab1_cont_left">
                           
                                <h2>Your Travel Trip</h2>
                              <!--  <span style="margin:0 0 0 15px;">All Packages</span>-->
                                <div class="inner_page_user">
                                	<?php
									$attributes = array('name'=>'editTripForm','id'=>'editTripForm','autocomplete'=>'off');
									echo form_open('trip/edit/'.$trip_id,$attributes);
									?>
                                    <div class="user_page_in_fo">
                                        	<label>Select Package <span class="fontred">*</span></label>
                                            <select name="trip_package" id="trip_package">
	                                            <option value="0" <?php if($trip_package == 0){?> selected="selected"<?php }?>>All</option>
                                                <option value="1" <?php if($trip_package == 1){?> selected="selected"<?php }?>>Family</option>
                                               <option value="2" <?php if($trip_package == 2){?> selected="selected"<?php }?>>Group</option>
                                               <option value="3" <?php if($trip_package == 3){?> selected="selected"<?php }?>>Honeymoon</option>
                                               <option value="4" <?php if($trip_package == 4){?> selected="selected"<?php }?>>Weekend</option>
                                               <option value="5" <?php if($trip_package == 5){?> selected="selected"<?php }?>>Start</option>
                                             </select>
                                        </div>
                                    	  <div class="clear"></div>	
                                    	<div class="user_page_in_fo">
                                            <label>Departure from <span class="fontred">*</span></label>
                                            <input type="text" name="departure_from" id="departure_from" value="<?php echo $departure_from;?>"  />
                                        </div>
                                        <div class="user_page_in_fo">
                                            <label>Want to go to <span class="fontred">*</span></label>
                                            <input type="text" name="want_go_to" id="want_go_to" value="<?php echo $want_go_to;?>"  />
                                        </div>
                                        
                                         <div class="user_in_fo_try" style="padding:0 50px 0 0">
                                        	<label>Rooms</label>
                                            <select name="room" id="room">
                                            	 <?php for($ik=1;$ik<=10;$ik++){
												 if($room == $ik){
												 ?>
												 <option value="<?php echo $ik;?>" selected="selected"><?php echo $ik;?></option>
												 <?php }else{
												 ?>
													
													<option value="<?php echo $ik;?>"><?php echo $ik;?></option>
												<?php }}?>
                                            </select>
                                        </div>
                                        
                                       
                                        <script type="text/javascript">
										 function setmonth(x)
										 {
											var str_mn = x.split('/');
											document.getElementById('trip_month').value = str_mn[0];
										 }
                                     </script>   
                                         <input type="hidden" name="trip_month" id="trip_month" value="<?php echo $trip_month;?>"/>
                                        
                                        <div class="many_persons">
                                        	<span>How many persons?</span>
                                        </div>
                                       
                                       <div class="clear"></div>
                                        <div class="user_in_fo_try" >
                                            <label>Total Person</label>
                                             <select name="total_people" class="select" id="total_people" onChange="">
                                         	 <?php for($i=1;$i<=10;$i++){
											 if($total_people == $i){
											 ?>
                                             <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
											 <?php }else{
											 ?>
												
                                          		<option value="<?php echo $i;?>"><?php echo $i;?></option>
											<?php }}?>
                                            </select>
                                         </div>
                                        
                                      	
                                         <div class="user_in_fo_try">
                                        	<label>Adults</label>
                                            <div id="adult_div">
                                                <select name="adult" id="adult">
                                                     <?php for($j=1;$j<=10;$j++){
													 if($adult == $j){?>
                                                     <option value="<?php echo $j;?>" selected="selected"><?php echo $j;?></option>
													 <?php }
													 else{?>
                                          		<option value="<?php echo $j;?>"><?php echo $j;?></option>
											<?php }}?>
                                                </select>
                                           </div>
                                        </div>
                                        <div class="user_in_fo_try">
                                        	<label>Children</label>
                                           <div id="adult_div">
                                                <select name="child" id="child">
                                                    <?php for($k=0;$k<=10;$k++){
													if($child == $k){?>
													<option value="<?php echo $k;?>" selected="selected"><?php echo $k;?></option>
													<?php }else{?>
                                          		<option value="<?php echo $k;?>"><?php echo $k;?></option>
											<?php }}?>
                                                </select>
                                           </div>
                                        </div>
                                        <div class="user_page_in_fo">
                                         <label>Total Days <span class="fontred">*</span></label>
                                          <input type="text" id="total_day" name="total_day" value="<?php echo $total_day;?>" onkeyup="setnight(this.value)"
/>
                                         </div>
                                         <div class="user_page_in_fo">
                                         <label>Total Nights</label>
                                         <input type="text" id="total_night" name="total_night" value="<?php echo $total_night;?>" readonly="readonly" style="background-color:#42BCFA;"/>
                                        </div> 
                                        
                                        <div class="user_in_fo_try">
                                        	<label>Currency</label>
                                            <select id="currency" name="currency">
                                            <?php if($get_currency){ 
													foreach($get_currency as $gc){
													if($currency == $gc->currency_code_id)
													{?>
                                                    <option title="<?php echo $gc->currency_symbol;?>" value="<?php echo $gc->currency_code_id;?>" selected="selected"><?php echo $gc->currency_code;?></option>
													<?php }
													else{?>
                                            			<option title="<?php echo $gc->currency_symbol;?>" value="<?php echo $gc->currency_code_id;?>"><?php echo $gc->currency_code;?></option>
											<?php }}}?>
                                            </select>
                                        </div>
                                        <div class="user_in_fo_try">
                                         <label>Min. budget <span class="fontred">*</span></label>
                                          <input type="text" id="min_budget" name="min_budget" value="<?php echo $min_budget;?>"/>
                                         </div>
                                         <div class="user_in_fo_try">
                                         <label>Max. budget <span class="fontred">*</span></label>
                                         <input type="text" id="max_budget" name="max_budget" value="<?php echo $max_budget;?>"/>
                                        </div> 
                                        
                                        <div class="user_page_in_fo">
                                         <label>Confirm date <span class="fontred">*</span></label>
                                          <input type="text" id="confirm_date" name="confirm_date" value="<?php echo $confirm_date;?>"/>
                                         </div>
                                         <div class="user_page_in_fo">
                                         <label>Complete date <span class="fontred">*</span></label>
                                         <input type="text" id="complete_date" name="complete_date" value="<?php echo $complete_date;?>"/>
                                        </div> 
                                 
                                    <div class="many_persons">
                                    	<span>Destination <span class="fontred">*</span></span>
                                    </div>
                                    <style>
									.tab1_cont_left span{ margin:0;}
									.tag span{ font-weight:normal;}
                                    </style>
                                    <div class="destination">
                                    	 <input id="destination_desc_tag" type="text" class="tags" name="destination_place" value="<?php echo $destination_place;?>" />
                                    </div>
                                </div>
                                
                        	</div>
                        <div class="cont_right">
                            <div class="conet_center">
                            	<div class="coment_img">
                                	<h2>Special comments</h2>
                                    <img src="<?php echo base_url().getThemeName(); ?>/images/coment_img.png" alt="" class="img" />
                                    <textarea name="special_comment" id="special_comment"><?php echo $special_comment;?></textarea>
                                </div>
                                <div class="hot_rate">
                                	<h1></h1>
                                    <div class="rate_inside_main">
                                        <div class="hot_rate_inside">
                                            <input type="checkbox" name="trip_hotel" value="1" <?php if($trip_hotel == 1){?> checked="checked"<?php }?>/>
                                            <img src="<?php echo base_url().getThemeName(); ?>/images/hotel.png" alt="" class="img"  />
                                            <label>Hotels</label>
                                        </div>
                                        <div class="hot_rate_inside">
                                            <input type="checkbox" name="trip_car" value="1" <?php if($trip_car == 1){?> checked="checked"<?php }?>/>
                                            <img src="<?php echo base_url().getThemeName(); ?>/images/car.png" alt="" class="img"  />
                                            <label>Cars</label>
                                        </div>
                                        <div class="hot_rate_inside">
                                            <input type="checkbox" name="trip_plane" value="1" <?php if($trip_plane == 1){?> checked="checked"<?php }?>/>
                                            <img src="<?php echo base_url().getThemeName(); ?>/images/plane.png" alt="" class="img"  />
                                            <label>Flights</label>
                                        </div>
                                        <div class="hot_rate_inside">
                                            <input type="checkbox" name="trip_cruises" value="1" <?php if($trip_cruises == 1){?> checked="checked"<?php }?>/>
                                            <img src="<?php echo base_url().getThemeName(); ?>/images/cruises.png" alt="" class="img"  />
                                            <label>Cruises</label>
                                        </div>
                                        <div class="hot_rate_inside">
                                            <input type="checkbox" name="railways" value="1" <?php if($railways == 1){?> checked="checked"<?php }?>/>
                                            <img src="<?php echo base_url().getThemeName(); ?>/images/railways.png" alt="" class="img"  />
                                            <label>Railways</label>
                                        </div>
                                    </div>

                                    <div class="build_pack1">
                                        <div class="build_pack_top1">
                                            <img class="img" alt="" src="<?php echo base_url().getThemeName(); ?>/images/bag.png">
                                            <h3>Build a package</h3>
                                        </div>
                                  <div class="build_pack_center1">
                                            <div class="build_to_in1">
                                                <input type="radio" name="b_pack" value="1" id="b_pack1" <?php if($b_pack == 1){?> checked="checked"<?php }?>/ >
                                              <label>Flight + Hotel + Car</label>
                                            </div>
                                            <div class="build_to_in1">
                                                <input type="radio" name="b_pack" value="2" id="b_pack2" <?php if($b_pack == 2){?> checked="checked"<?php }?>/>
                                                <label>Flight + Hotel </label>
                                            </div>
                                            <div class="build_to_in1">
                                                <input type="radio" name="b_pack" value="3" id="b_pack3" <?php if($b_pack == 3){?> checked="checked"<?php }?>/>
                                                <label>Flight + Car</label>
                                            </div>
                                        </div>
                       			 	</div>

                                    <div class="hotel_rate_pack">
                                    	<div class="preview">
                                    		<a href="#myModal"  role="button" data-toggle="modal" onClick="preview()">Preview</a>
                                    	</div>
                                      
									  		<input type="hidden" name="trip_id" id="trip_id" value="<?php echo $trip_id; ?>"/>
                                             <input type="hidden" name="trip_start_lat" id="trip_start_lat" value=""/>
                                             <input type="hidden" name="trip_start_long" id="trip_start_long" value=""/>
                                             <input type="hidden" name="trip_end_lat" id="trip_end_lat" value=""/>
                                             <input type="hidden" name="trip_end_long" id="trip_end_long" value=""/>
                                        	<input type="submit" name="edittrip" id="finish" value="Update" class="finish_btn"/>
                              
                                       </form>    
                                        
                <script type="text/javascript">
				function preview()
				{
					initialize();
					
					var package;
					var pack_id = document.getElementById('trip_package').value;
					if(pack_id == 0)
					{
						package = 'All';
					}
					else if(pack_id == 1)
					{
						package = 'Family';
					}
					else if(pack_id == 2)
					{
						package = 'Group';
					}
					else if(pack_id == 3)
					{
						package = 'Honeymoon';
					}
					else if(pack_id == 4)
					{
						package = 'Weekend';
					}
					else
					{
						package = 'Start';
					}
					document.getElementById('pre_pack').innerHTML= "<strong>Packages</strong> : "+package+"";
					
					
					var str_departure = document.getElementById('departure_from').value;
					var departure_from =  str_departure.split(',');
					document.getElementById('pre_departure').innerHTML= "<strong>Start Trip</strong> : "+departure_from[0]+"";
					
					var str_go_to = document.getElementById('want_go_to').value;
					var want_go_to =  str_go_to.split(',');
					document.getElementById('pre_goto').innerHTML= "<strong>End Trip</strong> : "+want_go_to[0]+"";
					
					
					var month_no;
					var month_name;
					var trp_month = document.getElementById('trip_month').value;
					if(trp_month == 1)
					{
						month_no = '1';
						month_name ='January';
					}
					else if(trp_month == 2)
					{
						month_no = '2';
						month_name ='February';
					}
					else if(trp_month == 3)
					{
						month_no = '3';
						month_name ='March';
					}
					else if(trp_month == 4)
					{
						month_no = '4';
						month_name ='April';
					}
					else if(trp_month == 5)
					{
						month_no = '5';
						month_name ='May';
					}
					else if(trp_month == 6)
					{
						month_no = '6';
						month_name ='June';
					}
					else if(trp_month == 7)
					{
						month_no = '7';
						month_name ='July';
					}
					else if(trp_month == 8)
					{
						month_no = '8';
						month_name ='August';
					}
					else if(trp_month == 9)
					{
						month_no = '9';
						month_name ='September';
					}
					else if(trp_month == 10)
					{
						month_no = '10';
						month_name ='October';
					}
					else if(trp_month == 11)
					{
						month_no = '11';
						month_name ='November';
					}
					else
					{
						month_no = '12';
						month_name ='December';
					}
					document.getElementById('pre_month').innerHTML= "<strong>Travel Month</strong> : "+month_name+","+month_no+"";
					
					
					var total_day = document.getElementById('total_day').value;
					var total_night = document.getElementById('total_night').value;
					
					document.getElementById('pre_duration').innerHTML= "<strong>Duration</strong> : "+total_night+"-"+total_day+" Nights/Day";
					
					var trp_room = document.getElementById('room').value;
					document.getElementById('pre_room').innerHTML = trp_room;
					
					var trp_adult = document.getElementById('adult').value;
					document.getElementById('pre_adult').innerHTML = trp_adult;
					
					
					var trp_child = document.getElementById('child').value;
					document.getElementById('pre_child').innerHTML = trp_child;
					
					
					var trp_min_budg = document.getElementById('min_budget').value;
					var trp_max_budg = document.getElementById('max_budget').value;
					var trp_currency = $('#currency option:selected').attr("title");
					
					document.getElementById('pre_budget').innerHTML = "Budget : <p>"+trp_currency+ trp_min_budg+ " - "+trp_currency+trp_max_budg+"</p>";
					
					var trp_special_comment = document.getElementById('special_comment').value;
					document.getElementById('pre_special_comment').innerHTML = trp_special_comment;
					
					var trp_destination_desc_tag = document.getElementById('destination_desc_tag').value;
					document.getElementById('trp_destination').innerHTML = trp_destination_desc_tag;
					
					var trp_b_pack1 = document.getElementsByName("b_pack").value;
				
					if(document.getElementById('b_pack1').checked) 
					{
						
						document.getElementById('pre_include_1').style.display='block';
						document.getElementById('pre_include_2').style.display='none';
						document.getElementById('pre_include_3').style.display='none';
					}
					else if(document.getElementById('b_pack2').checked)
					{
						document.getElementById('pre_include_1').style.display='none';
						document.getElementById('pre_include_2').style.display='block';
						document.getElementById('pre_include_3').style.display='none';
						
					}
					else
					{
						document.getElementById('pre_include_1').style.display='none';
						document.getElementById('pre_include_2').style.display='none';
						document.getElementById('pre_include_3').style.display='block';
					}
					
					
				
					
				}
                </script>                        
                                        

			<div class="modal hide fade preview_modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="header_img" style="height:auto;"></div><div class="clear"></div>
  <div class="prv_center">
     <h2>Preview</h2>
     <p>(same will be appear to the travel agents)</p>
  </div><div class="clear"></div>
  
  <div class="prw_main">
    <div class="prw_lt">
      <div class="trp_detail">
        <span id="pre_pack"><strong>Packages</strong> : All Packages</span><br/>
        <span id="pre_departure"><strong>Start Trip</strong> : Ahmedabad</span><br/>
        <span id="pre_goto"><strong>End Trip</strong> : Simla</span><br/>
        <span id="pre_month"><strong>Travel Month</strong> : January 3</span><br/>
        <span id="pre_duration"><strong>Duration</strong> : 4-7 Nights/Day</span><br/>
      </div>
      
      <div class="trp_detail">
        <span><strong>Passenger Details </strong></span>
        <table border="0" cellpadding="0" cellspacing="0">
          <tr>
          	<td> <span><strong>Rooms</strong></span></td>
            <td><span><strong>Adults</strong></span></td>
            <td><span><strong>Children</strong></span></td>
          </tr>
          <tr>
          	<td> <span id="pre_room">1</span></td>
            <td><span id="pre_adult">2</span></td>
            <td><span id="pre_child">3</span></td>
          </tr>
        </table>        
      </div>
      
       <div class="budget_popup">
       
         <span id="pre_budget">Budget : <p>$1000 - $1500</p></span>
       </div>
    </div>
    <div class="prw_rt">
       <h3>Special Comments</h3>
       <div class="view_more_prw">
       
 <textarea id="pre_special_comment" name="pre_special_comment" class="pre_text" readonly="readonly" style="background-color:#FFFFFF;   width:325px; "></textarea>
       
       </div>
       
        <h3>Trip Places</h3>
        
         <div class="trp_plc">
          <div style="padding:10px;">
          	
                 <textarea id="trp_destination" name="trp_destination" class="pre_text" readonly="readonly" style="background-color:#FFFFFF"></textarea>
          </div>
       </div>
     
    
    <script>

      function initialize() {
	  
	  	var trp_st_lat =  document.getElementById('trip_start_lat').value;
		var trp_st_lng =  document.getElementById('trip_start_long').value;
		var trp_end_lat = document.getElementById('trip_end_lat').value;
		var trp_end_lng = document.getElementById('trip_end_long').value;
		
		var map_lat = parseInt(parseInt(trp_st_lat) + parseInt(trp_end_lat) / 2);
		var map_lng = parseInt(parseInt(trp_st_lng) + parseInt(trp_end_lng) / 2);
		
		/*var map_lat = trp_st_lat;
		var map_lng = trp_st_lng;*/
		
	    var myLatLng = new google.maps.LatLng(map_lat, map_lng);
        var mapOptions = {
          zoom: 2,
          center: myLatLng,
          mapTypeId: google.maps.MapTypeId.TERRAIN
        };

	/* new google.maps.LatLng(-18.142599, 178.431),
            new google.maps.LatLng(-27.46758, 153.027892)*/
        var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
		  var map1 = new google.maps.Map(document.getElementById('map_canvas_1'), mapOptions);
		    var map2 = new google.maps.Map(document.getElementById('map_canvas_2'), mapOptions);
		
		
	
        var flightPlanCoordinates = [
            new google.maps.LatLng(trp_st_lat, trp_st_lng),
            new google.maps.LatLng(trp_end_lat, trp_end_lng)
           
        ];
        var flightPath = new google.maps.Polyline({
          path: flightPlanCoordinates,
          strokeColor: '#1AB5F6',
          strokeOpacity: 1.0,
          strokeWeight: 2
        });
		
		 var flightPath1 = new google.maps.Polyline({
          path: flightPlanCoordinates,
          strokeColor: '#1AB5F6',
          strokeOpacity: 1.0,
          strokeWeight: 2
        });
		
		 var flightPath2 = new google.maps.Polyline({
          path: flightPlanCoordinates,
          strokeColor: '#1AB5F6',
          strokeOpacity: 1.0,
          strokeWeight: 2
        });

        flightPath.setMap(map);
		 flightPath1.setMap(map1);
		  flightPath2.setMap(map2);
      }
    </script>
        <h3>Trip must include</h3>
        
        <div id="pre_include_1" style="display:block">
        <table border="0" cellpadding="0" cellspacing="0">
          <tr>
           <td>
            <img src="<?php echo base_url().getThemeName(); ?>/images/flight.png" style="margin-top:5px; margin-bottom:5px;" /><br />
             <span class="tr_fnt">Flight</span>
           </td>
           <td><font color="#ffffff" style="padding-left:20px; padding-right:20px;"><strong>+</strong></font></td>
           <td> <img src="<?php echo base_url().getThemeName(); ?>/images/hote.png" style="margin-top:5px; margin-bottom:5px;" /><br />
             <span class="tr_fnt">Hotel</span></td>
            <td><font color="#ffffff" style="padding-left:20px; padding-right:20px;"><strong>+</strong></font></td>
           <td> <img src="<?php echo base_url().getThemeName(); ?>/images/ca.png" style="margin-top:5px; margin-bottom:5px;" /><br />
             <span class="tr_fnt">Car</span></td>
           
          </tr>
          
          <tr>
          	<td colspan="4"><span class="tr_fnt" style="margin-left:30px;"><a href="#">Trip map</a></span><br /> <div style="margin-top:5px; margin-bottom:5px; margin-left:40px; height:150px; width:150px;" id="map_canvas_1"></div></td>
          </tr>
        </table>
        </div>
        <div id="pre_include_2" style="display:none">
        <table border="0" cellpadding="0" cellspacing="0">
          <tr>
           <td>
            <img src="<?php echo base_url().getThemeName(); ?>/images/flight.png" style="margin-top:5px; margin-bottom:5px;" /><br />
             <span class="tr_fnt">Flight</span>
           </td>
           <td><font color="#ffffff" style="padding-left:20px; padding-right:20px;"><strong>+</strong></font></td>
           <td> <img src="<?php echo base_url().getThemeName(); ?>/images/hote.png" style="margin-top:5px; margin-bottom:5px;" /><br />
             <span class="tr_fnt">Hotel</span></td>
           
             
             <td> <span class="tr_fnt" style="margin-left:30px;"><a href="#">Trip map</a></span><br /> 
             <div style="margin-top:5px; margin-bottom:5px; margin-left:40px; height:150px; width:150px;" id="map_canvas_2"></div>
             </td>
          </tr>
          
        </table>
        </div>
        <div id="pre_include_3" style="display:none">
        <table border="0" cellpadding="0" cellspacing="0">
          <tr>
           <td>
            <img src="<?php echo base_url().getThemeName(); ?>/images/flight.png" style="margin-top:5px; margin-bottom:5px;" /><br />
             <span class="tr_fnt">Flight</span>
           </td>
            <td><font color="#ffffff" style="padding-left:20px; padding-right:20px;"><strong>+</strong></font></td>
           <td> <img src="<?php echo base_url().getThemeName(); ?>/images/ca.png" style="margin-top:5px; margin-bottom:5px;" /><br />
             <span class="tr_fnt">Car</span></td>
             
             <td> <span class="tr_fnt" style="margin-left:30px;"><a href="#">Trip map</a></span><br />
             <div style="margin-top:5px; margin-bottom:5px; margin-left:40px; height:150px; width:150px;" id="map_canvas"></div>
             </td>
          </tr>
        </table>
        </div>
        <div class="fr"><a href="#" class="close opacity cls" data-dismiss="modal"></a></div>
       
       
       
    </div>
  </div>
  
  
  
  
</div>
       
                                    	
                                	</div>
                                    
                                </div>
                            </div>
                        </div>
                        
                    </div>
                
            </div>
            </div>
        </div>
        </div>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>  
 <link href="/maps/documentation/javascript/examples/default.css" rel="stylesheet">
 <!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>-->

        <script> 
	
		
		var options = {		
		  types: ['(cities)']
		 // componentRestrictions: {country: 'us'}
		};
		
	   var departure_from = new google.maps.places.Autocomplete($("#departure_from")[0], options);
        google.maps.event.addListener(departure_from, 'place_changed', function() {
		 	var place_start = departure_from.getPlace();
			
			var trip_start_lat = place_start.geometry.location.lat();
			var trip_start_long = place_start.geometry.location.lng();
			
			 document.getElementById('trip_start_lat').value = trip_start_lat;
			 document.getElementById('trip_start_long').value = trip_start_long;
			
            
			 console.log(place_start.address_components);
            });
			
		  var want_go_to = new google.maps.places.Autocomplete($("#want_go_to")[0], options);
        google.maps.event.addListener(want_go_to, 'place_changed', function() {
                var place_end = want_go_to.getPlace();

				var trip_end_lat = place_end.geometry.location.lat();
				var trip_end_long = place_end.geometry.location.lng();
				
				document.getElementById('trip_end_lat').value = trip_end_lat;
			 	document.getElementById('trip_end_long').value = trip_end_long;

                console.log(place_end.address_components);
            });	
</script>
