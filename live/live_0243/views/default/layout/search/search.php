<script src="<?php echo base_url().$theme;?>/js/jquery-ui-1.8.2.js"></script>
<link rel="stylesheet" href="<?php echo base_url().$theme;?>/css/jquery.ui.all.css" />
 
 <script>

  $(document).ready(function(){
     
	 	$("#resetfilter").click(function(){				
			if($("#resetfilter:checked").val()==1){
				window.location.href='<?php echo site_url('search/trip'); ?>';
			}		
		});
		
		$("#sortasc").click(function(){
			$("#showasc").show();
			$("#showdesc").hide();
			$("#sort_type").val('asc');
			tripsearch();
		});
		
		$("#sortdesc").click(function(){
			$("#showasc").hide();
			$("#showdesc").show();
			$("#sort_type").val('desc');
			tripsearch();
		});
		
		$("#sortasc1").click(function(){
			$("#showasc").show();
			$("#showdesc").hide();
			$("#sort_type").val('asc');
			tripsearch();
		});
		
		$("#sortdesc1").click(function(){
			$("#showasc").hide();
			$("#showdesc").show();
			$("#sort_type").val('desc');
			tripsearch();
		});
  });	 
  
  
  function tripsearch()
  {
  	 var from_place= $("#from_place").val();
	 var to_place= $("#to_place").val();
	 
	  var min_amount= $("#min_amount").val();
	  var max_amount= $("#max_amount").val();
	  
		  
	  
	   var package_type = [];
	   
		 $("#allpackage :checked").each(function(){
		    if($(this).val()==0)
			{
				$('#allpackage input[type=checkbox]').attr('checked', false);	
				$(this).attr('checked', true);	
			}
			
		   	package_type.push($(this).val());
			
		 });
	 
	 
		var cnttype=0;
		for (var i = 0; i < package_type.length; i++)
		{
			if(package_type[i]==0)
			{
				cnttype=1;
				break;
			}
		}
	 
		 if(cnttype==1)
		 {	
		 	for (var i = 0; i < package_type.length; i++)
			{
				package_type.pop();
			}	 
		 }
	 
	
	   var time_left_hours= $("#time_left_hours").val();
	   var post_date= $("#post_date").val();
	   
	   var sort_type= $("#sort_type").val(); 
	   
	   
	   
	   var searchurl = '<?php echo base_url();?>search/trip_search_ajax/?from_place='+from_place+'&to_place='+to_place+'&min_amount='+min_amount+'&max_amount='+max_amount+'&package_type='+package_type+'&time_left_hours='+time_left_hours+'&post_date='+post_date+'&sort_type='+sort_type;
	   
	   
	   		var res = $.ajax({						
				type: 'POST',
				url: searchurl,
				dataType: 'html', 
				cache: false,
				async: false                     
			}).responseText;							
			 
	 	$("#contentsearch").html(res);
		
		gettotalsearchtrip(searchurl);
  }
  
  function gettotalsearchtrip(searchurl)
  {
  		var from_place= $("#from_place").val();
	 	var to_place= $("#to_place").val();
	 
	  var min_amount= $("#min_amount").val();
	  var max_amount= $("#max_amount").val();
	  
		  
	  
	   var package_type = [];
	   
		 $("#allpackage :checked").each(function(){
		    if($(this).val()==0)
			{
				$('#allpackage input[type=checkbox]').attr('checked', false);	
				$(this).attr('checked', true);	
			}
			
		   	package_type.push($(this).val());
			
		 });
	 
	 
		var cnttype=0;
		for (var i = 0; i < package_type.length; i++)
		{
			if(package_type[i]==0)
			{
				cnttype=1;
				break;
			}
		}
	 
		 if(cnttype==1)
		 {	
		 	for (var i = 0; i < package_type.length; i++)
			{
				package_type.pop();
			}	 
		 }
	 
	 
	
	   var time_left_hours= $("#time_left_hours").val();
	   var post_date= $("#post_date").val();
	   
	   var sort_type= $("#sort_type").val(); 
	   
	   
	   
	   var searchurl = '<?php echo base_url();?>search/total_trip_search_ajax/?from_place='+from_place+'&to_place='+to_place+'&min_amount='+min_amount+'&max_amount='+max_amount+'&package_type='+package_type+'&time_left_hours='+time_left_hours+'&post_date='+post_date+'&sort_type='+sort_type;
	   
	   
  		var res = $.ajax({						
				type: 'POST',
				url: searchurl,
				dataType: 'html', 
				cache: false,
				async: false                     
			}).responseText;							
			 
	 	$("#showtotaltrips").html(res);
  }
  
 
  </script>   
       
   
   
<div id="bidcontener"><!--start_#bidcontener-->
                <div class="bid_form_center">
                	<div class="inner_page_main">
                    	<div class="inner_page_top">
                        	<div class="inner_page">
                                <label>Search Trip&nbsp;:&nbsp;:&nbsp;&nbsp;&nbsp; From</label>
                                <input type="text" name="from_place" id="from_place" placeholder="Trips From Place" value="" style="width:230px;" />
                                
                                 <label>To</label>
                                <input type="text" name="to_place" id="to_place" placeholder="Trips To Place" value="" style="width:230px;" />
                                
                            </div>
                            <!--<div class="inner_page_right">
                            	<a href="#">Preferences</a>
                                <a href="#">Preferences</a>
                            </div>-->
                            <input type="button" onclick="tripsearch()" border="0" value="Search" class="conversation_search marT3 marL10" style="line-height:22px;">

                        </div>
                    </div>
                 
                    <div class="tip_indus">
                        <div class="india_tips">
                            <h3>Advance Search</h3>
                       </div>
                       <div class="protject">
                            <strong>Trips:</strong>
                            <span id="showtotaltrips"><?php echo $total_rows; ?></span>
                        </div>    
                	</div>
                    
                    <div class="inner_cener_page">
                       
                        <!--start_inner_cener_page_left-->
                        <div class="inner_cener_page_left">
                        	
                            <div class="resent_filter">                            	
                                <input type="checkbox" id="resetfilter" name="resetfilter" value="1" class="img" />
                                <h2>Reset filters</h2>
                                <input type="hidden" id="sort_type" name="sort_type" value="<?php echo $sort_type; ?>" />
                                
                            </div>
                            
                            
                            
                            <div class="budget">
                                <div class="budget_center">
                                	<div class="budget_center_top">
                                    	<h3>Budget</h3>
                                    </div>
                                    <div class="fixed_price">
                                    	<label>Min-budget</label><br />
                                      </div>
                                    <div class="resent_slide">
                                    	<div id="min_budget"></div>
                                    </div>
                                  
                                    <div class="fixed_price">
                                       	<label>Max-budget</label><br />
                                    </div>
                                    <div class="resent_slide">
                                     	<div id="max_budget"></div>
                                    </div>
                                    
                                    <div class="resent_in_fo">
                                    	<input type="text" readonly name="min_amount" value="" id="min_amount"/>
                                    	<input type="text" readonly name="max_amount" value="" id="max_amount"/>
                                    </div>
                                 
                                </div>
                            </div>
                            <div class="budget">
                                <div class="budget_center" id="allpackage">
                                	<div class="budget_center_top">
                                    	<h3>Trip Package</h3>
                                    </div>
                                    <div class="fixed_price">
                                    	<input type="checkbox" name="trip_package[]" value="0" onclick="tripsearch()">
                                        <label>All</label>
                                    </div>
                                    <div class="fixed_price">
                                    	<input type="checkbox" name="trip_package[]" value="2" onclick="tripsearch()">
                                        <label>Group</label>
                                    </div>
                                    <div class="fixed_price">
                                    	<input type="checkbox" name="trip_package[]" value="1" onclick="tripsearch()">
                                        <label>Family</label>
                                    </div>
                                    <div class="fixed_price">
                                    	<input type="checkbox" name="trip_package[]" value="3" onclick="tripsearch()">
                                        <label>Honey moon</label>
                                    </div>
                                     <div class="fixed_price">
                                    	<input type="checkbox" name="trip_package[]" value="4" onclick="tripsearch()">
                                        <label>Weekend</label>
                                    </div>
                                </div>
                            </div>
                            
                            
                           
                            
                            <div class="budget">
                                <div class="budget_center">
                                	<div class="budget_center_top">
                                    	<h3>Time Left</h3>
                                    </div>
                                    <div class="resent_slide">
                                    	<div id="time_left"></div>
                                    </div>
                                      <div class="resent_in_fo" id="showtimeleft">
                                  <input type="text" name="time_left_hours" readonly value="" id="time_left_hours" /><label>hours</label>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            <div class="budget">
                                <div class="budget_center">
                                	<div class="budget_center_top">
                                    	<h3>Posted Before Days</h3>
                                    </div>
                                    <div class="resent_slide">
                                    	<div id="post_date_trip"></div>
                                    </div>
                                      <div class="resent_in_fo" id="showpostdate">
                                      <input type="text" name="post_date" readonly value="" id="post_date" /><label>days</label>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            
                            
                          
<script>
    $(function() {
		
				
		/*slider for min budget*/
		$("#min_budget").slider({
            range: "min",
            value: 0,
            min: 0,
            max: 700,
            slide:function(event,ui){
                $("#min_amount").val(ui.value);
				if(ui.value>0) { $("#min_amount").show(); } else {   $("#min_amount").hide(); }
				tripsearch();
            }
        });
		
		var minamt=$("#min_budget").slider("value");
		if(minamt>0) { $("#min_amount").show(); } else {   $("#min_amount").hide(); }
        $("#min_amount").val(minamt);
		
		
		/*slider for max budget*/
        $("#max_budget").slider({
            range: "min",
            value: 0,
            min: 0,
            max: 5000,
            slide:function(event,ui){
                $("#max_amount").val(ui.value);
				if(ui.value>0) { $("#max_amount").show(); } else {   $("#max_amount").hide(); }
				tripsearch();
            }
        });
		
		var maxamt=$("#max_budget").slider("value");
		if(maxamt>0) { $("#max_amount").show(); } else {   $("#max_amount").hide(); }
        $("#max_amount").val(maxamt);
		
		
		
		/*slider for time left*/
		$( "#time_left").slider({
            range: "min",
            value: 0,
            min: 0,
            max: 24,
            slide:function(event,ui){
                $("#time_left_hours").val(ui.value);
				if(ui.value>0) { $("#showtimeleft").show(); } else {   $("#showtimeleft").hide(); }
				tripsearch();
            }
        });
		
		var timeleft=$("#time_left").slider("value");
		if(timeleft>0) { $("#showtimeleft").show(); } else {   $("#showtimeleft").hide(); }
        $("#time_left_hours").val(timeleft);
		
		
		
		/*Slider for past days*/
		$("#post_date_trip").slider({
	        range: "min",
            value: 0,
            min: 0,
            max: 7,
            slide:function(event,ui){
                $("#post_date").val(ui.value);
				if(ui.value>0) { $("#showpostdate").show(); } else {   $("#showpostdate").hide(); }
				tripsearch();
	        }				
        });
		var postdays=$("#post_date_trip").slider("value");
		if(postdays>0) { $("#showpostdate").show(); } else {   $("#showpostdate").hide(); }
        $("#post_date").val();
		
		
		
    });
	
    </script>  
    
    
    
                        </div>
                        <!--end_inner_cener_page_left-->
                        
                       <!--start_inner_cener_page_center-->
                        <div class="inner_cener_page_center">                        	
                        
                            <div class="date" style="padding:0px; margin:0px; border-top:none;">
                            	<div class="sort_br">
                                	<h3>Sort by Date&nbsp;:</h3>
                                    
                                    
                        <div id="showdesc" style=" float:left; display: <?php if($sort_type=='desc') { ?> block <?php } else { ?> none<?php } ?>;">
                                  
                 <span><a id="sortdesc" href="javascript:void(0)">Descending</a></span>
                     <h4><strong style="margin:0; padding:0; color:#000000; font-size:15px;">|</strong>            
                    &nbsp;<a id="sortasc" href="javascript:void(0)">Ascending</a></h4>
                                    
                           </div>
                           
                       <div id="showasc" style="float:left; display: <?php if($sort_type=='asc')  { ?> block<?php } else { ?>none<?php } ?>;">
                                    
                           <h4>&nbsp; <a id="sortdesc1" href="javascript:void(0)">Descending</a>
                             <strong style="margin:0; padding:0; color:#000000; font-size:15px;">|</strong>            
                         &nbsp;</h4>  <span><a id="sortasc1" href="javascript:void(0)">Ascending</a></span>                               
                         </div>
                                
                                    
                                    
                                    
                                </div>
                                <div class="sort_br">&nbsp;</div>
                            </div>
                            <div class="clear"></div>
                          
                          <div  id="contentsearch" >
                          
                          <?php  if($result) { foreach($result as $row) { 
						  
						  			
						 $user_image= base_url().'upload/no_image.png';					
						
								
					 	 if($row->profile_image!='') {  
								if(file_exists(base_path().'upload/user/'.$row->profile_image)) {
									$user_image=base_url().'upload/user/'.$row->profile_image;
								}
							}
						  
						  ?>
                          
                            <div class="rearrange">
                                <a href="<?php echo site_url('from/'.$row->trip_from_place.'/to/'.$row->trip_to_place.'/'.$row->trip_id);?>"><img src="<?php echo $user_image; ?>" alt="" class="img"></a>
                                
                                
                            	<h1><a href="<?php echo site_url('from/'.$row->trip_from_place.'/to/'.$row->trip_to_place.'/'.$row->trip_id);?>"><?php echo ucwords($row->trip_from_place);?> > <?php echo ucwords($row->trip_to_place);?> </a></h1>
                                
                                <span><strong>Budget:</strong> <?php echo get_currency_symbol($row->currency_code_id);?><?php echo $row->trip_min_budget; ?> - <?php echo get_currency_symbol($row->currency_code_id); ?><?php echo $row->trip_max_budget; ?> | <strong><?php echo $row->trip_nights; ?>/<?php echo $row->trip_days; ?></strong> Nights/Days | <strong>Posted:</strong> <?php echo getDuration($row->trip_added_date); ?> | <strong>Ends:</strong> 
								
								<?php $remain_date=getReverseDuration(date('Y-m-d H:i:s'),$row->trip_confirm_date);
								
								$remain_str='';
								
								if($remain_date['day']>0)
								{
									$remain_str .= $remain_date['day'].'d, ';
								}
								
								if($remain_date['day']>0 && $remain_date['hour']>0)
								{
									$remain_str .= $remain_date['hour'].'h';
								}
								
								if(($remain_date['day']=='' || $remain_date['day']==0) && $remain_date['hour']>0)
								{
									$remain_str .= $remain_date['hour'].'h, ';
								}
								
								if(($remain_date['day']=='' || $remain_date['day']==0) && $remain_date['hour']>0 && $remain_date['minute']>0)
								{
									$remain_str .= $remain_date['minute'].'m';
								}
								
								
								if(($remain_date['day']=='' || $remain_date['day']==0) && ($remain_date['hour']=='' || $remain_date['hour']==0) && $remain_date['minute']>0)
								{
									$remain_str .= $remain_date['minute'].'m, ';
								}
								
								if(($remain_date['day']=='' || $remain_date['day']==0) && ($remain_date['hour']=='' || $remain_date['hour']==0) && $remain_date['minute']>0 && $remain_date['second']>0)
								{
									$remain_str .= $remain_date['second'].'s';
								}
								
								echo $remain_str;
								?>
                                
                                  | <strong style="margin:0; padding:0; color:#0979b2;">
                               <?php echo get_total_bid_on_offer($row->trip_id);?> Proposals</strong></span>
                                <p><?php echo $row->trip_special_comment; ?> </p>
                                <div class="clear"></div>
                                <h3>Package:</h3>
                                <h2>&nbsp;
                                
                                <?php 
						$package_name='';
						if($row->package_type == 0){
							$package_name = 'All';
						}
						else if($row->package_type == 1)
						{
							$package_name = 'Family';
						}
						else if($row->package_type == 2)
						{
							$package_name = 'Group';
						}
						else if($row->package_type == 3)
						{
							$package_name = 'Honeymoon';
						}
						else if($row->package_type == 4)
						{
							$package_name = 'Weekend';
						}
						else
						{
							$package_name = 'Start';
							
						}
						
						 echo $package_name ;?></h2>
                               
                            </div>
                             <div class="clear"></div>
                             
                             
                            <?php } ?>
                            
                             <div class="clear"></div>
                  
                  
                 
           <div class="gonext">
          <?php  echo $page_link; 	?>
         </div> <div class="clear"></div>
	
         
                   
	   
							 <div class="clear"></div>
						<?php	} else { ?>
                            
                             <div class="rearrange">
                                                              
                            	<h1><a href="javascript:void(0)">No Trips Found in Search Criteria.</a></h1>
                                                             
                            </div>
                             <div class="clear"></div>
                            
                            <?php } ?>
                            
                            
                            
                            
                            </div> <!---contentsearch-->
                            
                            
                        </div><!--end_inner_cener_page_center-->
                        
                       	
                        
                        
                        
        
                        
                        
                        
                        <!--start_inner_cener_page_right-->
                        <div class="inner_cener_page_right">
                         <div class="fr agnt"><a href="<?php echo site_url('travelagent/directory');?>">All Agents</a></div>
                        	
                            
                                        
                <?php $top_agents=get_top_agent(5); 
                                        if($top_agents) { ?>
                    
                    <div class="posted">                           
                        <h2>EggTrip Agents...</h2>
                    </div>
                    
                    
                    <div class="travel_agent1">
                    </div>

			<?php foreach($top_agents as $top_agent) {
            
            ////////======= trip post usser image
            $agent_image= base_url().'upload/no_image.png';
            
            if($top_agent->profile_image!='') {
                if(file_exists(base_path().'upload/user/'.$top_agent->profile_image)) {
                    $agent_image=base_url().'upload/user/'.$top_agent->profile_image;
                }
            }
            ?>
            
            <div class="egg_trip1">
            <?php echo anchor('travelagent/'.$top_agent->profile_name,'<img src="'.$agent_image.'" alt=""class="img" />');?>
            
           <?php /*?> <div class="orenge_box1">
            <h6 <?php if($top_agent->agent_level>9) { ?> style="padding: 2px;"<?php } else { ?> style="padding:2px 2px 2px 5px;"<?php } ?>><?php echo $top_agent->agent_level;?></h6>
            </div><?php */?>
            
            <span><?php echo anchor('travelagent/'.$top_agent->profile_name,ucwords($top_agent->first_name.' '.$top_agent->last_name),' style="color:#0979B2;"');?> <!--<br> <strong>selected by Egg Trip</strong>--></span>
            </div>
            
             <div class="travel_agent1"> </div>
            
            <?php } 
             } ?>
                            
                            
                            
                          
                           
                           
                           
                           
                          
                           
                           
                           
                            
                           
                            
                            
                        
                            
                            
                        
                            
                            
                            
                            
                            
                            <div class="join_now">
                            	<h2>How come you’re not here?</h2>
                                <a href="#">Join now !</a>
                            </div>
                            
                            <div id="fb-root"></div>
							<script>(function(d, s, id) {
                              var js, fjs = d.getElementsByTagName(s)[0];
                              if (d.getElementById(id)) return;
                              js = d.createElement(s); js.id = id;
                              js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                              fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                            
                            <div class="facebook_likebox">
                            <div class="fb-like-box" data-href="https://www.facebook.com/pages/fundraisingscriptcom/187170521330689" data-width="183" data-height="364" data-show-faces="true" data-stream="false" data-header="true"></div>
                            </div>
                            
                            
                            
                        </div>
                        <!--end_inner_cener_page_right-->
                        
                	</div>
                </div>
            <div class="clear"></div>
            </div>