<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53b182c86f415a5c"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
    
    <link href="<?php echo base_url().getThemeName(); ?>/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url().getThemeName(); ?>/css/owl.theme.css" rel="stylesheet">
	<script src="<?php echo base_url().getThemeName(); ?>/js/jquery-1.9.1.min.js"></script> 
    <script src="<?php echo base_url().getThemeName(); ?>/js/owl.carousel.js"></script>
    
    
  
   
   <!-- second rotation code -->
 
   <link href="<?php echo base_url().getThemeName(); ?>/rotation1/css/rotate.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo base_url().getThemeName(); ?>/rotation1/js/jquery.ui.touch-punch.min.js"></script>
    
    <?php 
    if($design_gallery_type ==1)
	{
    ?>
   <script type="text/javascript" src="<?php echo base_url().getThemeName(); ?>/rotation1/js/script.js"></script>
   
   <?php }?>
   <!-- end of second rotation code-->
    	
   	<script>
    $(document).ready(function() {
		
	  $("#owl-demo").owlCarousel({
        autoPlay: 3000,
        items : 6,
        itemsDesktop : [1199,2],
        itemsDesktopSmall : [979,2]
      });


	
});

function like_dislike(like_status,design_id)
{
	if(design_id)
	{
			$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>design/like_dislike",         //the script to call to get data          
			data: {like_status : like_status,design_id:design_id},
		   // data: $("#member_form").serialize(),
			dataType: '',                //data format      
			success: function(data)          //on receive of reply
				{
					if(data == 'success')
					{
						var orig_total_likes = $("#total_likes").val();
						
						if(like_status == "0")
						{
							
							 var new_like_count =  parseFloat(orig_total_likes) - parseFloat(1);
							 //alert(new_like_count);
							 $(".like-cls").html("Like");
							 $(".like-cls").attr("onclick","like_dislike('1','<?php echo $design_id;?>')");
						}
						else
						{
							 var new_like_count =  parseFloat(orig_total_likes) + parseFloat(1);
							 $(".like-cls").html("Dislike");
							 $(".like-cls").attr("onclick","like_dislike('0','<?php echo $design_id;?>')");
						}
						
						$("#total_likes").val(new_like_count);
						$("#total_likes_count").html(new_like_count);
						 // alert(like_status);
					
						
						
					}
				} 
			});
		}
		else
		{
			//alert('rr');
		}
}
    </script>

<!-- *************************************************************************************** -->
<div class="wrapper row8 page-padding-top">
   
  
   
</div>

<!-- *************************************************************************************** -->
<div class="wrapper row4">
    <div class="container">
		<div class="main-contain">
        	<h1 class="text-center white"><span class="font-light">BROWSE, SHARE SELL AND CUSTOMIZE DESIGNS  </h1>
        	<div class="video-gallery">
            	<div class="row">
                	<div class="col-md-3 col-sm-3 text-center"> 
                    	<div class="margin-bottom-15">
                        	<!-- <img src="<?php echo base_url().getThemeName(); ?>/images/upload_download.png" alt="Logo" class="img-responsive" />-->
                            <div class="gallery_link">
                             <h1 class="top-link"> <a href="<?php echo site_url("design/add_me")?>"> Uplaod your Design </a> </h1>
							 <?php 
							 if(get_authenticateUserID() > 0)
							 {
								 if(get_authenticateUserID() != $user_id)
								{?>
								 <h1> <a href="<?php echo site_url("design/buy/".base64_encode($design_id))?>"> Download a Design </a> </h1>
							   <?php }
							   else
							   {?>
							   <h1> <a href="javascript://"> Download a Design </a> </h1>
							   <?php }
							   }
						   else
						   {?>
						   	<h1> <a href="<?php echo site_url("login")?>"> Download a Design </a> </h1>
						   <?php }?>
                           </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9">
                    <div id="owl-demo" class="owl-carousel">
                    <?php
                
					if($user_design_galley)
					{
						$i =1;
						$j =0;
					   foreach($user_design_galley as $udg)
					   { 
					      if($j==0)
						  {
							  echo '<div class="item">';
							  $j =1;
						  }
						  
						 
						 if($udg->attachment_name != "" && is_file(base_path()."upload/design/".$udg->attachment_name))
						 {?>
                           <span> <a href="<?php echo site_url("design/".$udg->design_slug); ?>"> <img src="<?php echo base_url()."upload/design/".$udg->attachment_name;?>" alt="Image"> 
                           <?php echo $udg->design_title; ?>  </a> </span>
					     <?php }
						 else {?>
							  <span> <a href="<?php echo site_url("design/".$udg->design_slug);?>"> <img src="<?php echo base_url().getThemeName(); ?>/images/120x85.jpg" alt="Image">
                              <?php echo $udg->design_title; ?>  </a> </span>
						<?php  }
						
					  
					   
					   if($i>=2)
					   {
						   echo "</div>";
						   $j = 0;
						   $i = 0;
						}
						
						
					      $i++;
						  
						
					   }
					
					}
					?>
                    	
                         
<!--                            <div class="item"> 
                            		<span> <a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/120x85.jpg" alt="Image"> AGP6OR ORING GRIPPER LOREM IPSUM </a> </span> 
                            		<span> <a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/120x85.jpg" alt="Image"> AGP6OR ORING GRIPPER LOREM IPSUM</a> </span>  
                            </div>
                            <div class="item"> 
                            		<span> <a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/120x85.jpg" alt="Image"> DUAL VEE </a> </span> 
                            		<span> <a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/120x85.jpg" alt="Image"> DUAL VEE</a> </span>  
                            </div>
                            <div class="item"> 
                            		<span> <a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/120x85.jpg" alt="Image"> LOVE ANIMATION  </a> </span> 
                            		<span> <a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/120x85.jpg" alt="Image">  LOVE ANIMATION </a> </span>  
                            </div>
                            <div class="item"> 
                            		<span> <a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/120x85.jpg" alt="Image"> AGP6OR ORING GRIPPER LOREM IPSUM  </a> </span> 
                            		<span> <a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/120x85.jpg" alt="Image"> AGP6OR ORING GRIPPER LOREM IPSUM </a> </span>  
                            </div>
                            <div class="item"> 
                            		<span> <a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/120x85.jpg" alt="Image"> DUAL VEE </a> </span> 
                            		<span> <a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/120x85.jpg" alt="Image"> DUAL VEE</a> </span>  
                            </div>
                             <div class="item"> 
                            		<span> <a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/120x85.jpg" alt="Image"> AGP6OR ORING GRIPPER LOREM IPSUM   </a> </span> 
                            		<span> <a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/120x85.jpg" alt="Image"> AGP6OR ORING GRIPPER LOREM IPSUM </a> </span>  
                            </div>
                            <div class="item"> 
                            		<span> <a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/120x85.jpg" alt="Image"> DUAL VEE </a> </span> 
                            		<span> <a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/120x85.jpg" alt="Image"> DUAL VEE</a> </span>  
                            </div>
-->                         </div>
                    	
                    </div>
                </div>
            </div> 
        </div>	
    </div>
</div>
<!-- *************************************************************************************** -->
<div class="wrapper row9">
	
    	 <div class="video-detail v-detail-bg">
    	 	<div class="container">
         	<div class="col-md-12" > 
            	<div class="v-detail-title">
                	<h2><?php echo $design_title;?> </h2>
                    <h3> BY <strong> <?php echo $design_by;?></strong> - <?php echo date($site_setting->date_format,strtotime($design_date));?></h3>
                </div>	
            </div>
           </div>
           
           <div class="container">
         	<div class="col-md-9"> 
           		
           	 	<div class="row">
           	 		<div class="col-md-12">
           	 		<div class="row">
                	<div class="col-md-9">
                    	<div class="video-box">
                        	<!-- <div>  -->
							
							<?php 
							
							if($design_gallery_type == 1)
							{
								
								$desing_image_list = $this->design_model->get_all_images_of_design($design_id);
								
								
								if($desing_image_list)
								{
									$i = 0;
									echo "<div class='menu'><ul class='list'>";
									foreach($desing_image_list as $dl)
									{
										if($dl->attachment_name != "" && is_file(base_path()."upload/design/".$dl->attachment_name))	
										{
											if($i ==0)
											{echo "<li>" ; }
											else {
												echo "<li style='display:none'>";
											}
										echo "
										<img src='".base_url()."upload/design/".$dl->attachment_name."' class='video-border img-responsive'>
										</li>";
									
									   }	
										$i++;
									}
									echo "</ul></div>";
									
								}
								
							}
							else
								 {
								
							     echo "<div>";  
							          if($main_image != '')
										{?>
											<img src="<?php echo base_url()?>upload/design_orig/<?php echo $main_image;?>" alt="" class="video-border img-responsive">
										<?php }
										else
										{?>
										<img src="<?php echo base_url().getThemeName(); ?>/images/img1.png" class="img1 video-border img-responsive" alt="">
										<?php }
										
										echo "</div>";
										
								}	
							?>
                        	<!--	<img src="<?php echo base_url().getThemeName(); ?>/images/img1.png" alt="video image" class="video-border img-responsive" />-->
                            <!-- </div> -->
                            <div  class="text-center downlaod-bnt">
							<?php 
							if(get_authenticateUserID() > 0)
							 {
							if(get_authenticateUserID() != $user_id)
							{?>
                            	<a href="<?php echo site_url('design/buy/'.base64_encode($design_id));?>"><img src="<?php echo base_url().getThemeName(); ?>/images/download_file.png" alt="Download File" class="img-responsive" /> </a>
						<?php }
						}
						else
						{?>
						    	<a href="<?php echo site_url('login');?>"><img src="<?php echo base_url().getThemeName(); ?>/images/download_file.png" alt="Download File" class="img-responsive" /> </a>
						<?php }?>		
                            </div>
                        </div>
                        
                
                    </div>
                    
                    <div class="col-md-3 row">
                    	<div class="v-detail-list" >
                        	<ul>
                            	<li><?php echo $design_point;?> CREDITS </li>
                                <li> <span id="total_likes_count"><?php echo $design_like_count;?></span> LIKES 
								
<?php 
											//if(get_authenticateUserID() != $user_id)
												//{?>
												<div class="pull-right">
													<?php if(get_authenticateUserID() > 0)
													{
													$chk_like = $this->design_model->check_like_track(get_authenticateUserID(),$design_id);													
													if($chk_like)
													{
														if($chk_like->like_status == 1)
														{?>
														<a class="like-cls" id="dislike" href="javascript://" onclick="like_dislike('0','<?php echo $design_id;?>');">Dislike &nbsp;</a>
														<?php }
														else
														{?>
														<a class="like-cls" id="like" href="javascript://" onclick="like_dislike('1','<?php echo $design_id;?>');">Like &nbsp;</a>
														<?php }
														
													}
													else
													{?>
														<a class="like-cls" href="javascript://" onclick="like_dislike('1','<?php echo $design_id;?>');">Like &nbsp;</a>
													<?php }
													
													?>
					
													<?php }
													//}?>				
                                                      <input type="hidden" name="total_likes" id="total_likes" value="<?php echo $design_like_count;?>" />				
								</li>
                                <li><?php echo $design_view_count;?> VIEWS</li>
                              
                                <li><!-- Go to www.addthis.com/dashboard to customize your tools -->
<div class="addthis_sharing_toolbox"></div></li>
                            </ul>
                        </div>
						<?php 
						if($get_one_design_tag)
						{?>
						<div class="v-tags">
                        	<h2> Tags </h2>
							<dl>          
							<?php 
							foreach($get_one_design_tag as $ot)
							{?>
							
                                <dt><?php echo $ot->tag_name;?> </dt>
                                <dd><?php echo get_tag_rel_count($ot->tag_id,'design');?></dd>
                            <?php }?>
							</dl>
                        
						</div>	
						<?php }
						?>
           				         		
                            
                    </div>
                </div>
                
               </div>
               
               <!-- vcommetnt-->
                       <div class="v-comment"> 
                    <div class="">
                    		<h3>COMMENTS</h3>
							<div class="media">
							<div class="fb-comments" data-href="http://ihive3d.com/developer/design/view_me" data-numposts="5" data-colorscheme="light"></div>
							</div>
                           
                    </div>
           		</div>
               <!-- end of commnet-->
              </div>
            </div>   	 
            <div class="col-md-3">
            	<div class="v-left-listing"> 
                
            	<h1>  CHECK OUT THESE DESIGNS TOO!  </h1>
                <div>
                	<ul>	
					<?php
					
				
					if($user_design)
					{
						foreach($user_design as $pd)
						{
						$main_design_image =  $this->design_model->get_one_main_design_image($pd->design_id);
						?>
						<li>
							<a href="<?php echo site_url('design/'.$pd->design_slug);?>"> <h2> <?php echo $pd->design_title;?></h2> 
							<?php if($main_design_image != '')
							{?>
							<img src="<?php echo base_url()?>upload/design/<?php echo $main_design_image;?>" alt="Video Listing Image" class="img-responsive" />     
							<?php 
							}
							else
							{?>
								<img src="<?php echo base_url().getThemeName(); ?>/images/160x110.png" alt="Video Listing Image" class="img-responsive" width="255"/>     
						<?php }?>		
								</a>
						</li>
						<?php }
					}
					else
					{
						if($popular_design)
						{
							foreach($popular_design as $pd)
							{
							$main_design_image =  $this->design_model->get_one_main_design_image($pd->design_id);
							?>
							<li>
								<a href="<?php echo site_url('design/'.$pd->design_slug);?>"> <h2> <?php echo $pd->design_title;?></h2> 
								<?php if($main_design_image != '')
								{?>
								<img src="<?php echo base_url()?>upload/design/<?php echo $main_design_image;?>" alt="Video Listing Image" class="img-responsive" />     
								<?php 
								}
								else
								{?>
									<img src="<?php echo base_url().getThemeName(); ?>/images/160x110.png" alt="Video Listing Image" class="img-responsive" width="255"/>     
							<?php }?>		
									</a>
							</li>
							<?php }
						}
					}
					?>
					
                   
                    </ul>
                    	
                </div>
                
                </div>
            </div>
            <div class="clearfix"></div>
         </div>
    </div>
</div> 

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>