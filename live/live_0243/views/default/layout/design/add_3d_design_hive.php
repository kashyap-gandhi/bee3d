<!-- Add fancyBox main JS and CSS files -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url().getThemeName(); ?>/js/tag/jquery.tagsinput.css" />
<!-- responsive lightbox -->
<?php /*?> <link rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/magnific-popup/magnific-popup.css">
<?php */?><script type="text/javascript" src="<?php echo base_url()?>tinymce/tinymce.min.js"></script>
	<!-- aditional stylesheets -->
	<script type="text/javascript">
	tinymce.init({selector:'#design_content'});
	function append_div2()
{
	var tmp_div2 = document.createElement("div");
	tmp_div2.className = "";								
	
	var glry_cnt=document.getElementById('glry_cnt').value;
	
	/*if(glry_cnt<5)
	{*/
		document.getElementById('glry_cnt').value=parseInt(glry_cnt)+1;
		var num=parseInt(glry_cnt)+1;
		
		tmp_div2.id='galry'+num;
		var content=document.getElementById('more2').innerHTML;
		var str = '<div onclick="remove_gallery_div('+num+')" style="font-weight:normal;cursor:pointer;color:#ff0000; width:100px;">Remove</div><div class="clear"></div>';			
		tmp_div2.innerHTML = content +str;	
	
		document.getElementById('add_more2').appendChild(tmp_div2);
		
				
	
}


function remove_gallery_div(id)
{						
		var element = document.getElementById('galry'+id);
		var add_more = parent.document.getElementById('add_more2');
		
		element.parentNode.removeChild(element);
	
		
		var glry_cnt=document.getElementById('glry_cnt').value;
		document.getElementById('glry_cnt').value=parseInt(glry_cnt)-1;
			
		document.getElementById('addimg').style.display='block';
	
	}
	
	function show_parent_design()
	{
		$('#parent_design_div').slideToggle();
	}
	
	function delete_design_attachment(id)
	{
		var y = confirm('Are you sure yo delete image?');
		if(y)
		{
			$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>design/delete_design_attachment",         //the script to call to get data          
			data: {attachment_id: id},
		   // data: $("#member_form").serialize(),
			dataType: '',                //data format      
			success: function(data)          //on receive of reply
				{
					if(data == 'success')
					{
						//$('#coupon_gallery_span_'+id).removeClass('img_upload');
						$('#image_attach_'+id).html('');
					}
				} 
			});
		}
		else
		{
			//alert('rr');
		}
	}
	</script>
<!-- parsley -->
		<!-- responsive table -->
			<link href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>
		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Dashboard</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
						
						<!-- main content -->
						<div class="row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
										<?php if($design_id > 0)
										{?>
										Edit
										<?php 
										}else
										{?>
										Add
										<?php }?> 
										Description of 3D Design
										</h4>
									</div>
									<?php if($error!='') { ?>
											<div class="alert alert-danger">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
													<?php  echo $error; ?>
												
											</div>
									 <?php  } ?>
									
									<div class="panel-body">
											<?php
												$attributes = array('name'=>'design_form','id'=>'design_form','class'=>'');
												echo form_open_multipart('design/add_me',$attributes);
											?>
											<div class="form_sep">
												<label for="reg_input_name" class="req">Name of 3D Design</label>
												<input type="text" id="design_title" name="design_title" class="form-control" data-required="true" value="<?php echo $design_title;?>">
											</div>
											<div class="form_sep">
												<label for="reg_input_email" class="req">Detail Description of 3D Design</label>
												<textarea name="design_content" id="design_content" cols="30" rows="4" class="form-control"><?php echo $design_content;?></textarea>
												
											</div>
											
											<div class="form_sep">
												<label for="reg_select" class="req">Design Category</label>
												<select name="design_category" id="design_category" class="form-control" data-required="true">
													<option value="">--Select Category--</option>
													<?php 
													if($category)
													{
														foreach($category as $cat)
														{?>
														<option value="<?php echo $cat->category_id;?>" <?php if($design_category == $cat->category_id){?> selected="selected"<?php }?>><?php echo $cat->category_name;?></option>
														
														<?php }
														
													}
													?>
												</select>
											</div>
											
											<div class="form_sep">
												<label for="reg_select" class="req">Select Gallery Type</label>
												<select name="design_gallery_type" id="design_gallery_type" class="form-control" data-required="true">
													<option value=" ">--Select Gallery Type--</option>
													<option value="0" <?php if($design_gallery_type == 0){?> selected="selected"<?php }?>><?php echo "Normal";?></option>	
													<option value="1" <?php if($design_gallery_type == 1){?> selected="selected"<?php }?>><?php echo "360 Rotate";?></option>	
													<option value="2" <?php if($design_gallery_type == 2){?> selected="selected"<?php }?>><?php echo "Slider";?></option>
													
												</select>
											</div>
											
											<div class="form_sep">
												<label for="reg_textarea_message">Design Image</label>
												<div id="add_more2">
														<div id="more2">
														<input type="file" class="form-control" id="attachment_name[]" name="attachment_name[]" value="<?php echo $design_point;?>"></div>
												</div>	
												<div class="clear"></div>
											</div>
											
												
											<input type="hidden" name="glry_cnt" id="glry_cnt" value="1" />
						<div id="addimg" class="add_icon" ><img src="<?php echo base_url().getThemeName(); ?>/images/add.png" height="16" width="16" border="0" title="add more" onclick="append_div2();" style="cursor:pointer;" /></div>	
									
									<nav class="panel_controls" style="display:none;">
										<div class="row" style="display:none;">
											<div class="col-sm-4">
												<span class="gal_lay_change lay_active" id="gal_toGrid"><i class="icon-th"></i></span>
												<span class="gal_lay_change" id="gal_toList"><i class="icon-align-justify"></i></span>
											</div>
											<div class="col-sm-4">
												<select name="gal_filter_a" id="gal_filter_a" class="form-control" multiple>
												</select>
											</div>
											<div class="col-sm-4">
												<select name="gal_filter_b" id="gal_filter_b" class="form-control" multiple>
												</select>
											</div>
										</div>
									</nav>
									<div class="form_sep">
									
									<?php if($design_images)
									{?>
										<ul id="gallery_grid" class="" style="list-style:none;">
										<?php foreach($design_images as $di)
										{?>
										
										<li class="mix user_1 nature travel" data-name="Image 11" data-timestamp="1379912400" id="image_attach_<?php echo $di->attachment_id; ?>">
										<a href="#myModal" data-toggle="modal" data-img-url="<?php echo base_url()?>upload/design_orig/<?php echo $di->attachment_name;?>">
											
												<img src="<?php echo base_url()?>upload/design/<?php echo $di->attachment_name;?>" class="img-thumbnail" alt="">
											</a>
											<br/>
												<a href="javascript://" onclick="delete_design_attachment('<?php echo $di->attachment_id;?>');">Delete</a>	
									
												
											</li>
											
										<?php }?>	
											
										</ul>			
									<?php }?>	
									</div>
									<div class="clear"></div>	
											<div class="form_sep">
												<label for="reg_textarea_message" class="req">Design Points</label>
												<input type="text" id="design_point" name="design_point" class="form-control" value="<?php echo $design_point;?>">
											</div>
											
											<div class="form_sep">
												<label for="reg_textarea_message">Design Referal ID</label>
												<input type="text" id="design_ref_id" name="design_ref_id" class="form-control" value="<?php echo $design_ref_id;?>">
											</div>
											
											<div class="form_sep">
												<label for="reg_textarea_message">Design By</label>
												<input type="text" id="design_by" name="design_by" class="form-control" value="<?php echo $design_by;?>">
											</div>
											
											<div class="form_sep">
												<label for="reg_textarea_message">Design is modified</label>
												<input type="checkbox" id="design_is_modified" name="design_is_modified" class="form-control" value="1" onclick="show_parent_design();" <?php if($design_is_modified == 1){?> checked="checked"<?php }?>>
											</div>
											
											<?php 
											if($design_is_modified == 1)
											{
												$prd = 'style="display:block;"';
											}
											else
											{
												$prd = 'style="display:none;"';
											}?>
											<div class="form_sep" id="parent_design_div" <?php echo $prd;?>>
												<label for="reg_select">Parent Design</label>
												<select name="parent_design_id" id="parent_design_id" class="form-control" data-required="true">
													<option value="">--Select Parent Design--</option>
													<?php 
													if($alldesign)
													{
														foreach($alldesign as $dg)
														{?>
														<option value="<?php echo $dg->design_id;?>" <?php if($parent_design_id == $dg->design_id){?> selected="selected"<?php }?>><?php echo $dg->design_title;?></option>
														
														<?php }
														
													}
													?>
												</select>
											</div>
											
											<div class="form_sep">
												<label for="reg_textarea_message">Meata Keywords</label>
												<textarea rows="3" cols="70" class="form-control autosize_textarea" name="design_meta_keyword" id="design_meta_keyword" ><?php echo $design_meta_keyword;?></textarea>
												
											</div>
											
											<div class="form_sep">
												<label for="reg_textarea_message">Meta Description</label>
											<textarea rows="3" cols="70" class="form-control autosize_textarea" name="design_meta_description" id="design_meta_description" ><?php echo $design_meta_description;?></textarea>

											</div>
											
											<div class="form_sep">
												<label for="reg_textarea_message">Design Tag</label>
												<div id="add_more_tag">
											<div id="more_tag">
											<input id="tags_1" type="text" class="tags" name="tag_name" value="<?php echo $design_tag;?>" />
											</div>
											</div>
											<div class="clear"></div>
											</div>
											<div class="form_sep">
											<input type="hidden" name="design_id" id="design_id" value="<?php echo $design_id;?>"/>
											<input type="submit" name="submit" id="submit" value="submit" class="btn btn-default"/>
											</div>	
										</form>
									</div>
								</div>
							</div>
							
						</div>
						
					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
			<div id="footer_space"></div>
			
			<!-- Modal -->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="text-align:center;" >
<div class="modal-body">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <img src="#"/>
</div>
<!--<div class="modal-footer">
<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
</div>-->
</div>
		</div>

       
<script src="<?php echo base_url().getThemeName(); ?>/js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url().getThemeName(); ?>/js/tag/jquery.tagsinput.js"></script>
<?php /*?><!-- mixItUp -->
			<script src="js/lib/mixitup/jquery.mixitup.min.js"></script>
	<!-- responsive lightbox -->
      <script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/magnific-popup/jquery.magnific-popup.min.js"></script>
			
			
<!-- multiple select -->
<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/multiple-select/jquery.multiple.select.js"></script>
<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/pages/ebro_gallery.js"></script>
<?php */?>
<script type="text/javascript">
$(document).ready(function(){
$('#tags_1').tagsInput({width:'auto'});

//===== Usual validation engine=====//
	$("#design_form").validate({
		rules: {
			design_title: "required",
			design_point: "required",
			design_content: "required",
			design_category:"required"
			
		}

	});
	$('li a').click(function(e) {
    $('#myModal img').attr('src', $(this).attr('data-img-url')); 
});
});	
	
</script>
