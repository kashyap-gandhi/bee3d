<script type="text/javascript">
function delete_design(id)
{
	var ans = confirm("Are you sure, you want to delete Design?");
	if(ans)
	{
		location.href = "<?php echo base_url(); ?>design/delete_me/"+id;
	}else{
		return false;
	}
}
</script>
	<!-- aditional stylesheets -->
		<!-- responsive table -->
			<link href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>

		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Design</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
					<?php if($msg!='') { 
						if($msg == 'success')
						{?>
					
							<div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "Category inserted successfully."; ?>
								
							</div>
					 <?php  }
					 if($msg == 'success_design')
					 {?>
					 <div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "Design inserted successfully."; ?>
								
							</div>
					 <?php }
					 if($msg == 'update_design')
					 {?>
					  <div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "Design updated successfully."; ?>
								
							</div>
					 <?php }
					 if($msg == 'delete_design')
					 {?>
					  <div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "Design deleted successfully."; ?>
								
							</div>
					 <?php }
					 } ?>

						<!-- main content -->
						<div class="row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Manage Personal Data Base of 3D Designs
										<span class="span_right"><a href="<?php echo site_url('design/add_me');?>" class=""><h4>Add Design</a></h4></span>
										</h4>
									</div>
									
									<table id="resp_table" class="table toggle-square" data-filter="#table_search" data-page-size="40">
										<thead>
											<tr>
												<th>Title</th>
												<th>Category</th>
												<th>Points</th>
												<th width="35%">Action</th>
											</tr>
										</thead>
										<tbody>
											<?php if($result)
											{
												foreach($result as $res)
												{?>
											<tr>
												<td><?php echo ucfirst($res->design_title);?></td>
												<td><?php echo ucfirst($res->category_name);?></td>
												<td><?php echo $res->design_point;?></td>
												<td>

													
													<a class="btn btn-primary" href="<?php echo site_url('design/'.$res->design_slug);?>"><span data-toggle="tooltip" data-placement="top auto" title="View object in 3D viewer">View</span></a>
													<a class="btn btn-success" title="Edit description of 3D Object" href="<?php echo site_url('design/edit_me/'.$res->design_id);?>"><span data-toggle="tooltip" data-placement="top auto" title="Edit description of 3D Object">Edit</span></a>
													<a class="btn btn-danger" title="Delete 3D Object from personal data base" href="javascript://" onclick="delete_design('<?php echo $res->design_id;?>');"><span data-toggle="tooltip" data-placement="top auto" title="Delete 3D Object from database">Delete</span></a>
												</td>
											</tr>	
											<?php }}
											else
											{?>
											<tr>
												<td colspan="4">You have not added any design yet!!</td>
												
											</tr>
											<?php }?>
											
											
										</tbody>
									
											<tr>
												<td colspan="6" class="text-center">
													<ul class="">
													<?php echo $page_link;?>
													</ul>
												</td>
											</tr>
									
									</table>
								</div>
							</div>
						</div>
						
					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
			<div id="footer_space"></div>
		</div>

       <!-- responsive table -->
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.sort.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.filter.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.paginate.js"></script>

			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/pages/ebro_responsive_table.js"></script>
