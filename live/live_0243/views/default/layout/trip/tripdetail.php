
<script type="text/javascript" src="<?php echo base_url().getThemeName(); ?>/js/easySlider1.7.js"></script>
<script src="<?php echo base_url().getThemeName(); ?>/js/jquery-ui-1.9.0.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url().getThemeName(); ?>/js/bootstrap.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().getThemeName(); ?>/css/bootstrap.css" />

<script type="text/javascript">
		$(document).ready(function(){	
			$("#slider").easySlider({
				auto: true,
				continuous: true
				
			});
		});	
	</script>
<script>
	$(function() {
		$( "#tab_prd" ).tabs();
	});
	
  function hidden_trip_offer(trip_offer_id,trip_id)
  {
  	
			if(trip_offer_id=='') { return false; }
  			var r=confirm("Are you sure want to hidden this offer?");
			if (r==true)
			  {
			  	x=1;
				
			  }
			else
			  {
			 	 x=0;
			  }
			
			if(x== 0)
			{
				return false;
			}
		
			window.location.href = "<?php echo site_url('trip/hidden/');?>"+"/"+trip_offer_id+"/"+trip_id;
	
   }
   
  function decline_trip_offer(trip_offer_id,trip_id)
  {
  	
			if(trip_offer_id=='') { return false; }
  			var r=confirm("Are you sure want to decline this offer?");
			if (r==true)
			  {
			  	x=1;
				
			  }
			else
			  {
			 	 x=0;
			  }
			
			if(x== 0)
			{
				return false;
			}
		
			window.location.href = "<?php echo site_url('trip/decline/');?>"+"/"+trip_offer_id+"/"+trip_id;
	
   }
   
   
  function remove_hidden_trip_offer(trip_offer_id,trip_id)
  {
  	
			if(trip_offer_id=='') { return false; }
  			var r=confirm("Are you sure want to remove to all proposal this offer?");
			if (r==true)
			  {
			  	x=1;
				
			  }
			else
			  {
			 	 x=0;
			  }
			
			if(x== 0)
			{
				return false;
			}
		
			window.location.href = "<?php echo site_url('trip/remove_hidden_trip_offer/');?>"+"/"+trip_offer_id+"/"+trip_id;
	
   }
   
	
</script>
<link href="<?php echo base_url().getThemeName(); ?>/css/screen.css" rel="stylesheet" type="text/css" media="screen" /> 
<div id="bidcontener">

  
  <!--start_#bidcontener-->
  <div class="bid_form_center">
  
   <?php if($msg != '') {
			if($msg =='success') {?>     
				 		 <div id="error" class="msgsuccess" style="margin-top:10px;margin-bottom:10px;"><?php  echo "Trip offer has been post successfully."; ?></div>
                         <div class="clear"></div>
			   <?php }
			  if($msg =='update')  
			  {?>
			   <div id="error" class="msgsuccess" style="margin-top:10px; margin-bottom:10px;"><?php  echo "Trip offer has been updated successfully."; ?></div>
               <div class="clear"></div>
			  <?php }
			  if($msg == 'delete')
			  {?>
              <div id="error" class="msgsuccess" style="margin-top:10px;margin-bottom:10px;"><?php  echo "Trip offer has been deleted  successfully."; ?></div>
              <div class="clear"></div>
			  <?php }
			  if($msg == 'hidden')
			  {?>
               <div id="error" class="msgsuccess" style="margin-top:10px;margin-bottom:10px;"><?php  echo "Trip offer has been set to hidden successfully."; ?></div>
              <div class="clear"></div>
			  <?php }
			  if($msg == 'decline')
			  {?>
               <div id="error" class="msgsuccess" style="margin-top:10px;margin-bottom:10px;"><?php  echo "Trip offer has been set to  decline successfully."; ?></div>
              <div class="clear"></div>
			  <?php }
			  if($msg == 'ask')
			  {?>
               <div id="error" class="msgsuccess" style="margin-top:10px;margin-bottom:10px;"><?php  echo "Your question has been sent successfully."; ?></div>
              <div class="clear"></div>
			  <?php }
			  if($msg == 'reply')
			  {?>
			   <div id="error" class="msgsuccess" style="margin-top:10px;margin-bottom:10px;"><?php  echo "Reply has been sent successfully."; ?></div>
              <div class="clear"></div>
			 <?php  }
			 if($msg =='all')
			 {?>
              <div id="error" class="msgsuccess" style="margin-top:10px;margin-bottom:10px;"><?php  echo "Trip offer add to all proposal successfully."; ?></div>
              <div class="clear"></div>
			 <?php }
			 
			  if($msg =='donotedit')
			 {?>
              <div id="error" class="msgsuccess" style="margin-top:10px;margin-bottom:10px;"><?php  echo "You cannot allow to edit trip one its assigned or expired."; ?></div>
              <div class="clear"></div>
			 <?php }
	 } ?>
    <div class="left_side">
      <div class="center_top">
        <div class="center_left">
          <h1>Trip: <?php echo $trip_info->trip_from_place;?> > <?php echo $trip_info->trip_to_place;?></h1>
         
         
      
<p>posted <span>about <?php echo getDuration($trip_info->trip_added_date);?></span> by <?php echo $user_info->profile_name;?></p>
        </div>
       
        <div class="center_right">
          <div class="photo" style="margin:10px 31px 0 0;"> 
          <?php 
          	 	$user_image= base_url().'upload/no_image.png';
				if($user_info->profile_image!='') {  
			
					if(file_exists(base_path().'upload/user/'.$user_info->profile_image)) {
				
						$user_image=base_url().'upload/user/'.$user_info->profile_image;
						
					}
					
				}
			?>
            <?php echo anchor('travelagent/'.$user_info->profile_name,'<img src="'.$user_image.'" alt="" width="65" height="67"  />'); ?>
           
            
        </div>
        </div>
         <div class="clear"></div>
         <div class="ask_me" style="float:right;">
         		
                <?php
			if(get_authenticateUserID() > 0){
			
			if(get_authenticateUserID() != $trip_info->user_id)
			{
			
			if(check_user_agent(get_authenticateUserID()))
			{ 
				if(strtotime($trip_info->trip_confirm_date)>=strtotime(date('Y-m-d H:i:s')) && $trip_info->trip_activity_status==0 && ($trip_info->trip_agent_assign_id==0 || $trip_info->trip_agent_assign_id=='') ) {
				
				?>
            
                <a href="#myModal"  role="button" data-toggle="modal">Ask Question</a>
            <?php } } } } ?>    
                
           </div>
        
        <div class="clear"></div>
      </div>
      <div class="trip_requirements"> <img src="<?php echo base_url().getThemeName(); ?>/images/pen.png" alt="" class="img" />
        <h1>Trip Requirements</h1>
      </div>
      <div class="center_middle">
        <div class="middle_left">
          <div class="packages_box">
            <div class="packages_box_center">
              <div class="packages_text">
                <p>Packages &nbsp;&nbsp;:&nbsp;
                	<span>
                    	<?php 
						$package_name='';
						if($trip_info->package_type == 0){
							$package_name = 'All';
						}
						else if($trip_info->package_type == 1)
						{
							$package_name = 'Family';
						}
						else if($trip_info->package_type == 2)
						{
							$package_name = 'Group';
						}
						else if($trip_info->package_type == 3)
						{
							$package_name = 'Honeymoon';
						}
						else if($trip_info->package_type == 4)
						{
							$package_name = 'Weekend';
						}
						else
						{
							$package_name = 'Start';
							
						}
						?>
                		<?php echo $package_name.' Package';?>
                    </span>
                </p>
                <p>Start Trip&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;<span><?php echo $trip_info->trip_from_place;?></span></p>
                <p>End Trip&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<span><?php echo $trip_info->trip_to_place;?></span></p>
                <p>Trip Month&nbsp;&nbsp;:&nbsp;
                	<span><?php
					$month_name='';
					if($trip_info->trip_month == 1)
					{
						$month_name ='January';
					}
					else if($trip_info->trip_month == 2)
					{
						$month_name ='February';
					}
					else if($trip_info->trip_month == 3)
					{
						$month_name ='March';
					}
					else if($trip_info->trip_month == 4)
					{
						$month_name ='April';
					}
					else if($trip_info->trip_month == 5)
					{
						$month_name ='May';
					}
					else if($trip_info->trip_month == 6)
					{
						$month_name ='June';
					}
					else if($trip_info->trip_month == 7)
					{
						$month_name ='July';
					}
					else if($trip_info->trip_month == 8)
					{
						$month_name ='August';
					}
					else if($trip_info->trip_month == 9)
					{
						$month_name ='September';
					}
					else if($trip_info->trip_month == 10)
					{
						$month_name ='October';
					}
					else if($trip_info->trip_month == 11)
					{
						$month_name ='November';
					}
					else
					{
						$month_name ='December';
					}
					
					 
					?> 
                   <?php echo $month_name.' '.$trip_info->trip_month;?>
                    </span>
                  </p>
                <p>Duration&nbsp;&nbsp;:&nbsp;<span><?php echo $trip_info->trip_days;?>-<?php echo $trip_info->trip_nights;?> Nights/Day</span></p>
              </div>
            </div>
          </div>
          <div class="packages_box">
            <div class="packages_box_top"></div>
            <div class="packages_box_center">
              <div class="packages_text">
                <span >Rooms</span>&nbsp;&nbsp;&nbsp;&nbsp;
                <span>Adults</span>&nbsp;&nbsp;&nbsp;&nbsp;
                <span>Children</span> 
                
                <span>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $trip_info->total_room;?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span><?php echo $trip_info->total_adult;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;
                <span><?php echo $trip_info->total_child;?></span> </div>
            </div>
            <div class="packages_box_bottom"></div>
          </div>
          <div class="packages_box">
            <div class="packages_box_top"></div>
            <div class="packages_box_center">
              <div class="packages_text">
                <p>Budget : <strong><?php echo get_currency_symbol($trip_info->currency_code_id);?><?php echo $trip_info->trip_min_budget;?> - <?php echo get_currency_symbol($trip_info->currency_code_id);?><?php echo $trip_info->trip_max_budget;?></strong></p>
              </div>
            </div>
            <div class="packages_box_bottom"> </div>
          </div>
        </div>
        <div class="middle_right">
        	<?php if($trip_info->trip_special_comment != '')
			{?>
              <div class="special_comments">
                <h1>Special Comments</h1>
              </div>
              <div class="view_more_prw"> <span><?php echo  $trip_info->trip_special_comment;?></span>
              </div>
               <?php }?>
          <div class="trip_places">
            <h1>Trip Places</h1>
          </div>
          <div class="trp_plc">
            <div style="padding:10px;"> 
            	<?php 
				$place = explode(',',$trip_info->trip_destination_places);
				if($place)
				{
				foreach($place as $pl){
				?>
                	<span><?php echo $pl;?></span>
				<?php }}
				?>
              </div>
           <!-- <div class="viewmore_fnr"> <a href="#"><b class="create"></b> view more...</a> </div>-->
          </div>
          
        
         
          
          <div class="trip_must_include">
            <h1>Trip must include</h1>
          </div>
          
          <?php 
	   $atts = array(
		  'width'      => '500',
		  'height'     => '500',
		  'scrollbars' => 'yes',
		  'status'     => 'yes',
		  'resizable'  => 'yes',
		  'screenx'    => '0',
		  'screeny'    => '0'
		);
		  	if($trip_info->trip_build_package == 1)
			{
		  ?>
          	<div class="icons">
                                    <div class="flight">
                                        <img src="<?php echo base_url().getThemeName(); ?>/images/plane.png" alt="" class="img" />
                                        <a href="#">Flight</a>
                                    </div>
                                    <div class="hotel">
                                        <img src="<?php echo base_url().getThemeName(); ?>/images/hotel.png" alt="" class="img" />
                                        <a href="#">Hotel</a>
                                    </div>
                                    <div class="car">
                                        <img src="<?php echo base_url().getThemeName(); ?>/images/car.png" alt="" class="img" /><br />
                                        <a href="#">Car</a>
                                    </div>
                                    <div class="map">
                                         <?php 
										
										 echo anchor_popup('trip/map/'.$trip_info->trip_id,'<span>Trip Map</span>', $atts); ?>
                                         <br />
                                       <?php  echo anchor_popup('trip/map/'.$trip_info->trip_id,' <img src="'.base_url().getThemeName().'/images/map.png" alt="" class="img" />', $atts); ?>
                                       <br />
                                    </div>
                                </div>
          <?php  }else if($trip_info->trip_build_package == 2){?>
          
         	 <div class="icons">
                                    <div class="flight">
                                        <img src="<?php echo base_url().getThemeName(); ?>/images/plane.png" alt="" class="img" />
                                        <a href="#">Flight</a>
                                    </div>
                                    <div class="hotel">
                                        <img src="<?php echo base_url().getThemeName(); ?>/images/hotel.png" alt="" class="img" />
                                        <a href="#">Hotel</a>
                                    </div>
                                    <div class="map" style="float:none;">
                                         <?php 
										
										 echo anchor_popup('trip/map/'.$trip_info->trip_id,'<span>Trip Map</span>', $atts); ?>
                                         <br />
                                     <?php  echo anchor_popup('trip/map/'.$trip_info->trip_id,' <img src="'.base_url().getThemeName().'/images/map.png" alt="" class="img" />', $atts); ?>
                                       <br />
                                    </div>
                                </div>
          <?php }else{?>
         	<div class="icons">
                                    <div class="flight">
                                        <img src="<?php echo base_url().getThemeName(); ?>/images/plane.png" alt="" class="img" />
                                        <a href="#">Flight</a>
                                    </div>
                                    <div class="car">
                                        <img src="<?php echo base_url().getThemeName(); ?>/images/car.png" alt="" class="img" /><br />
                                        <a href="#">Car</a>
                                    </div>
                                    <div class="map" style="float:none;">
                                         <?php 
										
										 echo anchor_popup('trip/map/'.$trip_info->trip_id,'<span>Trip Map</span>', $atts); ?>
                                         <br />
                                      
                                       
                                       <?php  echo anchor_popup('trip/map/'.$trip_info->trip_id,' <img src="'.base_url().getThemeName().'/images/map.png" alt="" class="img" />', $atts); ?>
                                       
                                       <br />
                                    </div>
                                </div>
         <?php } ?>
        </div>
        <div class="clear"></div>
         
        <div class="conservations">
          <h2>Confirm Egg Trip by : </h2>
          <span>&nbsp;&nbsp;&nbsp;<?php echo date($site_setting->date_format,strtotime($trip_info->trip_confirm_date));?></span><br />
          <h2>Complete Trip by : </h2>
          <span>&nbsp;&nbsp;&nbsp;<?php echo date($site_setting->date_format,strtotime($trip_info->trip_must_complete_date));?></span> </div>
      </div>
      
      
      
       <?php if($trip_conversation){?>
      
      <div class="trip_requirements"> <img src="<?php echo base_url().getThemeName(); ?>/images/ques.png" alt="" class="img" />
        <h1>Conservations</h1>
      </div>
      <!--Conversation Start-->
      <div class="conserva">
      <?php foreach($trip_conversation as $trc){
		$ask_by_user_info = check_user_agent($trc->post_by_user_id);
		 $ask_user_image= base_url().'upload/no_image.png';					
			if($ask_by_user_info->profile_image!='') {  
			if(file_exists(base_path().'upload/user/'.$ask_by_user_info->profile_image)) {
				$ask_user_image=base_url().'upload/user/'.$ask_by_user_info->profile_image;
			}
		}?>
      
        <div class="conservations"><?php echo anchor('user/'.$ask_by_user_info->profile_name,'<img src="'. $ask_user_image.'" alt="" class="img" />');?>
         <?php /*?> <div class="orenge_box conver_ie">
            <h6><?php echo $ask_by_user_info->agent_level;?></h6>
          </div><?php */?>
          <h1><?php echo anchor('user/'.$ask_by_user_info->profile_name,$ask_by_user_info->first_name.' '.$ask_by_user_info->last_name);?></h1>
          <p><?php echo $trc->conversation;?></p>
        </div>
        <div class="clear"></div>
        	<?php 
			$check_reply = check_message_reply($trc->trip_conversation_id);
			if($trc->reply_trip_conversation_id == 0 && $check_reply == 1)
			{
				$get_rply = $this->trip_model->get_conversation_reply($trc->trip_conversation_id);
				
				
				$reply_by_user_info = get_user_profile_by_id($get_rply->post_by_user_id);
				
				
				 $reply_user_image= base_url().'upload/no_image.png';				
			if($reply_by_user_info->profile_image!='') {  
			if(file_exists(base_path().'upload/user/'.$reply_by_user_info->profile_image)) {
				$reply_user_image=base_url().'upload/user/'.$reply_by_user_info->profile_image;
			}
		}
				
				
			?>
            		<div class="conservations padL75R30">
                                <?php echo anchor('user/'.$reply_by_user_info->profile_name,'<img src="'. $reply_user_image.'" alt="" class="img" />');?>
                               <?php $check_agent_info = check_user_agent($get_rply->post_by_user_id);
							   if($check_agent_info) { ?>
                               <?php /*?><div class="orenge_box conver_ie">                         
                                <h6><?php echo $check_agent_info->agent_level;?></h6>                              
                            </div><?php */?>
                            <?php } ?>
                            
                            
                                <h1><?php echo anchor('user/'.$reply_by_user_info->profile_name,$reply_by_user_info->first_name.' '.$reply_by_user_info->last_name);?></h1>
                                <p><?php echo $get_rply->conversation;?></p>
                            </div>
                              <div class="clear"></div>
			<?php }?>
        <?php 
		
		
		if($check_reply == 0 && $trip_info->user_id == get_authenticateUserID()){?>
         <a href="#myModal2"  role="button" data-toggle="modal" onclick="set_trip_conversation_id(<?php echo $trc->trip_conversation_id;?>)">Reply</a>
         <?php }?>
         <div class="clear"></div>
       <?php }?> 
          
      </div>
      
      <?php }?>
      
      <script type="text/javascript">
	  function set_trip_conversation_id(x)
	  {
	  	document.getElementById('trip_conversation_id').value = x;
	  }
      </script>
    
     <!--End Conversation Start-->
      
        <?php if($similartrip){ ?>
      <div class="trips_similar"> <img src="<?php echo base_url().getThemeName(); ?>/images/pin.png" alt="" class="img" />
        <h1>Trips similar to this one</h1>
      </div>
      <div class="similar_trips">
        <div id="slider">
          <ul>
			 
			<?php    foreach($similartrip as $sm){?>
               <li style="width:300px">
               
               <?php 
          	 	$user_similar_image= base_url().'upload/no_image.png';
				if($sm->profile_image!='') {  
			
					if(file_exists(base_path().'upload/user/'.$sm->profile_image)) {
				
						$user_similar_image=base_url().'upload/user/'.$sm->profile_image;
						
					}
					
				}
			?>
           
              <div class="watering_plants">  <?php echo anchor('travelagent/'.$sm->profile_name,'<img src="'.$user_similar_image.'" alt="" class="img" />'); ?>
               
                <h3><a href="<?php echo site_url('from/'.$sm->trip_from_place.'/to/'.$sm->trip_to_place.'/'.$sm->trip_id);?>"><?php echo ucwords($sm->trip_from_place);?> > <?php echo ucwords($sm->trip_to_place);?> </a>
          </h3>
                <p>posted by <span> <?php echo anchor('travelagent/'.$sm->profile_name,$sm->profile_name);?>. </span><i><br />
                <?php echo get_currency_symbol($sm->currency_code_id);?><?php echo $sm->trip_min_budget;?>
                  - <?php echo get_currency_symbol($sm->currency_code_id);?><?php echo $sm->trip_max_budget;?> for Trips of this type.</i></p>
                <div class="stars"> 
                  <?php $check_agent_detail=$this->travelagent_model->check_agent_detail($sm->user_id);
                  		$total_rate=get_user_total_rate($sm->user_id);
						$total_review=get_user_total_review($sm->user_id);
					?>  
                    
                     <div class="strmn"><div class="str_sel" style="width:<?php if($total_rate>5) { ?>100<?php } else { echo round($total_rate*2);?>0<?php } ?>%;"></div></div>
            </div>
                <div class="bid_blubtn"> <a href="#">Bid</a> </div>
              </div>
              
            </li>
			   <?php }?>      
            	
         
          </ul>
        </div>
      </div>
      
       <?php }?>      
      <div class="clear"></div>
      
      
      <?php
			if(get_authenticateUserID() > 0){
			
			if(get_authenticateUserID() == $trip_info->user_id)
			{ ?>
      
      <script>
	  $(document).ready(function(){
	  $('#tab_propose li').click(function(){
	   var temp = $(this).find('a').attr('id');
	   document.getElementById('sort_type_search').value = temp;
	  
	  $('#tab_propose li').removeClass('active');
	  $(this).addClass('active');
	
	  
	  });
	  });
	  </script>
      <script type="text/javascript">
	  function sortby_rate(id)
	  {
	  	
		if(id=='')
		{
			return false;
		}
		var srttype = document.getElementById('sort_type_search').value;
		
		var tempby = document.getElementById('sort_by_search').value;
		
		if(tempby == 'desc')
		{
			document.getElementById('sort_by_search').value = 'asc';
		}
		if(tempby == 'asc')
		{
			document.getElementById('sort_by_search').value = 'desc';
		}
		var srtby=document.getElementById('sort_by_search').value;
		
		
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else if(window.ActiveXObject) {
			try {
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {
					xmlhttp = false;
				}
			}
		}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				alert('rr');
				//document.getElementById('project_card_category').innerHTML= '<a class="category_name" href="#">'+xmlhttp.responseText+'</a>';
			}
		}
		
		var url = '<?php echo site_url('trip/sort_by_rate'); ?>/'+id+'/'+srtby+'/'+srttype;
		
		xmlhttp.open("GET",url,true);
		xmlhttp.send(null);

		
	}
	
	function sort_by_date(id)
	{
		
	  	if($('#sort_date_up_down').hasClass("sort_date_up"))
		{
			$('#sort_date_up_down').removeClass('sort_date_up');
			$('#sort_date_up_down').addClass('sort_date_down');
		}
		else
		{
			$('#sort_date_up_down').addClass('sort_date_up');
			$('#sort_date_up_down').removeClass('sort_date_down');
		}
		
		if(id=='')
		{
			return false;
		}
		var srttype = document.getElementById('sort_type_search').value;
		
		var tempby = document.getElementById('sort_by_search').value;
		
		if(tempby == 'desc')
		{
			document.getElementById('sort_by_search').value = 'asc';
		}
		if(tempby == 'asc')
		{
			document.getElementById('sort_by_search').value = 'desc';
		}
		var srtby=document.getElementById('sort_by_search').value;
		
		
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else if(window.ActiveXObject) {
			try {
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {
					xmlhttp = false;
				}
			}
		}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				if(srttype == 'hidden')
				{
					document.getElementById('main_propose_hidden').innerHTML= xmlhttp.responseText;
				}
				else if(srttype == 'decline')
				{
					document.getElementById('main_propose_decline').innerHTML= xmlhttp.responseText;
				}
				else if(srttype == 'invited')
				{
					document.getElementById('main_propose_invited').innerHTML= xmlhttp.responseText;
				}
				else
				{
					document.getElementById('main_propose').innerHTML= xmlhttp.responseText;
				}
			}
		}
		
		var url = '<?php echo site_url('trip/sort_by_date'); ?>/'+id+'/'+srtby+'/'+srttype;
		
		xmlhttp.open("GET",url,true);
		xmlhttp.send(null);

		
	
	}
      </script>
      <input type="hidden" name="sort_type" id="sort_type_search" value="all"/>
      <input type="hidden" name="sort_by" id="sort_by_search" value="desc"/>
      <div class="all_proposals" id="tab_prd">
        <ul id="tab_propose">
          <li class="active" ><a href="#tab1" id="all"><span>All Proposals (<?php echo count_trip_proposal($trip_info->trip_id);?>)</span></a></li>
          <li><a href="#tab2" id="hidden"><span>Hidden (<?php echo count_hidden_proposal($trip_info->trip_id);?>)</span></a></li>
          <li><a href="#tab3" id="decline"><span>Declined (<?php echo count_decline_proposal($trip_info->trip_id);?>)</span></a></li>
          <li><a href="#tab4" id="invited"><span>Invited (<?php echo count_invited_proposal($trip_info->trip_id);?>)</span></a></li>
        </ul>
        <div class="avg_high" style="margin-left:0px;"> <span>Avg:<?php echo get_currency_symbol($trip_info->currency_code_id).''.$trip_average;?> High: <?php echo get_currency_symbol($trip_info->currency_code_id).''.$trip_max;?> Low: <?php echo get_currency_symbol($trip_info->currency_code_id).''.$trip_low;?></span> <span>Filter By:</span>
          <h3 class="sort_rate_up"><a href="javascript:void(0)" onclick="sortby_rate(<?php echo $trip_info->trip_id;?>);">Rating</a></h3>
          <span>Sort B:</span>
          <h3 id="sort_date_up_down" class="sort_date_up"><a href="javascript:void(0)" onclick="sort_by_date(<?php echo $trip_info->trip_id;?>);">Submit Date (Latest)</a></h3>
          <div class="clear"></div>
        </div>
        
        <div id="tab1">
          <div class="jaimim_crararry" id="main_propose">
           
         	<?php if($trip_proposal)
			{foreach($trip_proposal as $tp){
			
			 $proposal_user_info = check_user_agent($tp->user_id);
			 
			 $user_image= base_url().'upload/no_image.png';					
				 if($proposal_user_info->profile_image!='') {  
						if(file_exists(base_path().'upload/user/'.$proposal_user_info->profile_image)) {
								$user_image=base_url().'upload/user/'.$proposal_user_info->profile_image;
						}
				}
						
			?>
            	 <div class="rating_center" style="margin-bottom:10px;" id="proposal_<?php echo $tp->trip_offer_id;?>">
         	     <div class="rating_center_top">
                <div class="rating_center_top_left"> 
                 <?php echo anchor('user/'.$proposal_user_info->profile_name,'<img src="'. $user_image.'" class="img" alt=""  />');?>
                </div>
               <?php /*?> <div class="orenge_box" style="margin-top:0;">
                       <h6 <?php if($proposal_user_info->agent_level>9) { ?> style="padding-left:2px;" <?php } else { ?> style="padding-left:5px;" <?php } ?>><?php echo $proposal_user_info->agent_level; ?></h6> </div><?php */?>
                
                <div class="rating_center_top_right">
                  <h1><?php echo anchor('user/'.$proposal_user_info->profile_name,ucwords($proposal_user_info->first_name.' '.$proposal_user_info->last_name));?>
                </h1>
                 <?php
					$total_rate=get_user_total_rate($proposal_user_info->user_id);
					$total_review=get_user_total_review($proposal_user_info->user_id);
				?>  
                 <div class="strmn"><div class="str_sel" style="width:<?php if($total_rate>5) { ?>100<?php } else { echo round($total_rate*2);?>0<?php } ?>%;"></div></div> 
                <span>( <?php echo anchor('user/'.$proposal_user_info->profile_name. '/reviews',$total_review.' Review','class="fpass"');  ?>  ) |  <?php echo agent_total_trip($proposal_user_info->user_id);?> Trips | <?php echo anchor('user/'.$proposal_user_info->profile_name,'View Full Profile');?> |  <a href="javascript:void(0)" onclick="return hidden_trip_offer(<?php echo $tp->trip_offer_id;?>,<?php echo $tp->trip_id;?>)">Hidden</a> |  <a href="javascript:void(0)" onclick="return decline_trip_offer(<?php echo $tp->trip_offer_id;?>,<?php echo $tp->trip_id;?>)">Decline</a></span> </div>
              </div>
            
              
              <script type="text/javascript">
				
				function viewmoredetail(x)
				{
				  
				  var showid  = 'viewmore_'+x;
				  var cid = 'showdetail_'+x;
				 
				  if($('#'+cid).hasClass("view_up"))
				  {
				  	$('#'+cid).removeClass('view_up');
					$('#'+cid).addClass('view_down');
				  }
				  else
				  {
				  	$('#'+cid).addClass('view_up');
					$('#'+cid).removeClass('view_down');
				  }
				  $('#'+showid).slideToggle("slow"); 
				 }
			    </script>
              
              
            	 <div class="proposal" id="proposal_view_<?php echo  $tp->trip_offer_id;?>">
                <h3>Proposal</h3>
                <span><?php echo date($site_setting->date_format,strtotime($tp->offer_date));?></span>
                <p><?php echo substr($tp->offer_content,0,100);?></p>
                <?php $proposal_offer_doc = $this->trip_model->trip_offer_doc($tp->trip_id,$tp->user_id);
				?>
                
                
                
                <a href="javascript:void(0)" class="view_up" onclick="viewmoredetail(<?php echo  $tp->trip_offer_id;?>);" id="showdetail_<?php echo $tp->trip_offer_id;?>">View More</a>
                 <a href="#" class="docview">Print</a> <a href="#" class="docview">Download as :</a> 
                <?php 
				if($proposal_offer_doc)
				{
					foreach($proposal_offer_doc as $pd)
					{
						$ext =  explode('.',$pd->offer_attachment);
						if($ext[1] == 'pdf')
						{?>
							<a target="_blank" href="<?php echo base_url();?>upload/trip_offer_doc/<?php echo $pd->offer_attachment; ?>" style="background:none;"> <img class="img" alt="" src="<?php echo base_url().getThemeName(); ?>/images/pdf.png"></a>
						<?php }
						else if($ext[1] == 'doc' || $ext[1] == 'docx')
						{?>
							<a target="_blank" href="<?php echo base_url();?>upload/trip_offer_doc/<?php echo $pd->offer_attachment; ?>" style="background:none;"> <img class="img" alt="" src="<?php echo base_url().getThemeName(); ?>/images/world_files.png"></a>
						<?php }
						else
						{?>
                        <a target="_blank" href="<?php echo base_url();?>upload/trip_offer_doc/<?php echo $pd->offer_attachment; ?>" style="background:none;"> <img class="img" alt="" src="<?php echo base_url().getThemeName(); ?>/images/pdf.png"></a>
						<?php }
					}
				}?>
              </div>
              <div class="proposal" id="viewmore_<?php echo $tp->trip_offer_id;?>" style="display:none;"> <p><?php echo $tp->offer_content;?></p></div>
              	 <div class="samdolar">
                <h1><?php echo get_currency_symbol($tp->currency_code_id).''.$tp->final_offer_amount;?></h1>
              </div>
              
               </div>
               
            
            
             <?php }}
			 else{?>
			 <div class="no_proposals">
       			 <h1>No Proposals Found For This Trip.</h1>
      		</div>
			 <?php }?> 
              
           
          </div>
        </div>
        
        <div id="tab2">
        
        <script type="text/javascript">
			function viewmoredetail_hidden(x)
				 {
				 	var showid_hidden  = 'viewmore_hidden_'+x;
				  	var cid_hidden = 'showdetail_hidden_'+x;
				 
				  if($('#'+cid_hidden).hasClass("view_up"))
				  {
				  	$('#'+cid_hidden).removeClass('view_up');
					$('#'+cid_hidden).addClass('view_down');
				  }
				  else
				  {
				  	$('#'+cid_hidden).addClass('view_up');
					$('#'+cid_hidden).removeClass('view_down');
				  }
				  $('#'+showid_hidden).slideToggle("slow"); 
				 }
        </script>
          <div class="jaimim_crararry" id="main_propose_hidden">
            <?php if($hidden_trip)
			{foreach($hidden_trip as $tp){
			
			 $proposal_user_info = check_user_agent($tp->user_id);
			 
			 $user_image= base_url().'upload/no_image.png';					
				 if($proposal_user_info->profile_image!='') {  
						if(file_exists(base_path().'upload/user/'.$proposal_user_info->profile_image)) {
								$user_image=base_url().'upload/user/'.$proposal_user_info->profile_image;
						}
				}
						
			?>
            	 <div class="rating_center" style="margin-bottom:10px;" id="proposal_<?php echo $tp->trip_offer_id;?>">
         	     <div class="rating_center_top">
                <div class="rating_center_top_left"> 
                 <?php echo anchor('user/'.$proposal_user_info->profile_name,'<img src="'. $user_image.'" class="img" alt=""  />');?>
                </div>
                <?php /*?><div class="orenge_box" style="margin-top:0;">
                       <h6 <?php if($proposal_user_info->agent_level>9) { ?> style="padding-left:2px;" <?php } else { ?> style="padding-left:5px;" <?php } ?>><?php echo $proposal_user_info->agent_level; ?></h6> </div><?php */?>
                
                <div class="rating_center_top_right">
                  <h1><?php echo anchor('user/'.$proposal_user_info->profile_name,ucwords($proposal_user_info->first_name.' '.$proposal_user_info->last_name));?>
                </h1>
                 <?php
					$total_rate=get_user_total_rate($proposal_user_info->user_id);
					$total_review=get_user_total_review($proposal_user_info->user_id);
				?>  
                 <div class="strmn"><div class="str_sel" style="width:<?php if($total_rate>5) { ?>100<?php } else { echo round($total_rate*2);?>0<?php } ?>%;"></div></div> 
                <span>( <?php echo anchor('user/'.$proposal_user_info->profile_name. '/reviews',$total_review.' Review','class="fpass"');  ?>  ) |  <?php echo agent_total_trip($proposal_user_info->user_id);?> Trips | <?php echo anchor('user/'.$proposal_user_info->profile_name,'View Full Profile');?> |  <a href="javascript:void(0)" onclick="return remove_hidden_trip_offer(<?php echo $tp->trip_offer_id;?>,<?php echo $tp->trip_id;?>)">Back To All</a> |  <a href="javascript:void(0)" onclick="return decline_trip_offer(<?php echo $tp->trip_offer_id;?>,<?php echo $tp->trip_id;?>)">Decline</a></span> </div>
              </div>
            	 <div class="proposal">
                <h3>Proposal</h3>
                <span><?php echo date($site_setting->date_format,strtotime($tp->offer_date));?></span>
                <p><?php echo $tp->offer_content;?></p>
                <?php $proposal_offer_doc = $this->trip_model->trip_offer_doc($tp->trip_id,$tp->user_id);
				?>
                <a href="javascript:void(0)" class="view_up" onclick="viewmoredetail_hidden(<?php echo  $tp->trip_offer_id;?>);" id="showdetail_hidden_<?php echo $tp->trip_offer_id;?>">View More</a> 
               <a href="#" class="docview">Print</a> <a href="#" class="docview">Download as :</a> 
                <?php 
				if($proposal_offer_doc)
				{
					foreach($proposal_offer_doc as $pd)
					{
						$ext =  explode('.',$pd->offer_attachment);
						if($ext[1] == 'pdf')
						{?>
							<a target="_blank" href="<?php echo base_url();?>upload/trip_offer_doc/<?php echo $pd->offer_attachment; ?>" style="background:none;"> <img class="img" alt="" src="<?php echo base_url().getThemeName(); ?>/images/pdf.png"></a>
						<?php }
						else if($ext[1] == 'doc' || $ext[1] == 'docx')
						{?>
							<a target="_blank" href="<?php echo base_url();?>upload/trip_offer_doc/<?php echo $pd->offer_attachment; ?>" style="background:none;"> <img class="img" alt="" src="<?php echo base_url().getThemeName(); ?>/images/world_files.png"></a>
						<?php }
						else
						{?>
                        <a target="_blank" href="<?php echo base_url();?>upload/trip_offer_doc/<?php echo $pd->offer_attachment; ?>" style="background:none;"> <img class="img" alt="" src="<?php echo base_url().getThemeName(); ?>/images/pdf.png"></a>
						<?php }
					}
				}?>
              </div>
                 <div class="proposal" id="viewmore_hidden_<?php echo $tp->trip_offer_id;?>" style="display:none;"> <p><?php echo $tp->offer_content;?></p></div>
              	 <div class="samdolar">
                <h1><?php echo get_currency_symbol($tp->currency_code_id).''.$tp->final_offer_amount;?></h1>
              </div>
              
               </div>
               
            
            
             <?php }}
			 else{?>
			 <div class="no_proposals">
       			 <h1>No Hidden Proposals Found For This Trip.</h1>
      		</div>
			 <?php }?> 
          </div>
        </div>
        <div id="tab3">
        
         <script type="text/javascript">
		 
		 		 function viewmoredetail_decline(x)
				 {
				 	var showid_decline  = 'viewmore_decline_'+x;
				  	var cid_decline = 'showdetail_decline_'+x;
				 
				  if($('#'+cid_decline).hasClass("view_up"))
				  {
				  	$('#'+cid_decline).removeClass('view_up');
					$('#'+cid_decline).addClass('view_down');
				  }
				  else
				  {
				  	$('#'+cid_decline).addClass('view_up');
					$('#'+cid_decline).removeClass('view_down');
				  }
				  $('#'+showid_decline).slideToggle("slow"); 
				 }
        </script>
        
          <div class="jaimim_crararry" id="main_propose_decline">
            <?php if($decline_trip)
			{foreach($decline_trip as $tp){
			
			 $proposal_user_info = check_user_agent($tp->user_id);
			 
			 $user_image= base_url().'upload/no_image.png';					
				 if($proposal_user_info->profile_image!='') {  
						if(file_exists(base_path().'upload/user/'.$proposal_user_info->profile_image)) {
								$user_image=base_url().'upload/user/'.$proposal_user_info->profile_image;
						}
				}
						
			?>
            	 <div class="rating_center" style="margin-bottom:10px;" id="proposal_<?php echo $tp->trip_offer_id;?>">
         	     <div class="rating_center_top">
                <div class="rating_center_top_left"> 
                 <?php echo anchor('user/'.$proposal_user_info->profile_name,'<img src="'. $user_image.'" class="img" alt=""  />');?>
                </div>
                <?php /*?><div class="orenge_box" style="margin-top:0;">
                       <h6 <?php if($proposal_user_info->agent_level>9) { ?> style="padding-left:2px;" <?php } else { ?> style="padding-left:5px;" <?php } ?>><?php echo $proposal_user_info->agent_level; ?></h6> </div><?php */?>
                
                <div class="rating_center_top_right">
                  <h1><?php echo anchor('user/'.$proposal_user_info->profile_name,ucwords($proposal_user_info->first_name.' '.$proposal_user_info->last_name));?>
                </h1>
                 <?php
					$total_rate=get_user_total_rate($proposal_user_info->user_id);
					$total_review=get_user_total_review($proposal_user_info->user_id);
				?>  
                 <div class="strmn"><div class="str_sel" style="width:<?php if($total_rate>5) { ?>100<?php } else { echo round($total_rate*2);?>0<?php } ?>%;"></div></div> 
                <span>( <?php echo anchor('user/'.$proposal_user_info->profile_name. '/reviews',$total_review.' Review','class="fpass"');  ?>  ) |  <?php echo agent_total_trip($proposal_user_info->user_id);?> Trips | <?php echo anchor('user/'.$proposal_user_info->profile_name,'View Full Profile');?> |  <a href="javascript:void(0)" onclick="return remove_hidden_trip_offer(<?php echo $tp->trip_offer_id;?>,<?php echo $tp->trip_id;?>)">Back To All</a></span> </div>
              </div>
            	 <div class="proposal">
                <h3>Proposal</h3>
                <span><?php echo date($site_setting->date_format,strtotime($tp->offer_date));?></span>
                <p><?php echo $tp->offer_content;?></p>
                <?php $proposal_offer_doc = $this->trip_model->trip_offer_doc($tp->trip_id,$tp->user_id);
				?>
                <a href="javascript:void(0)" class="view_up" onclick="viewmoredetail_decline(<?php echo  $tp->trip_offer_id;?>);" id="showdetail_decline_<?php echo $tp->trip_offer_id;?>">View More</a> 
               <a href="#" class="docview">Print</a> <a href="#" class="docview">Download as :</a> 
                <?php 
				if($proposal_offer_doc)
				{
					foreach($proposal_offer_doc as $pd)
					{
						$ext =  explode('.',$pd->offer_attachment);
						if($ext[1] == 'pdf')
						{?>
							<a target="_blank" href="<?php echo base_url();?>upload/trip_offer_doc/<?php echo $pd->offer_attachment; ?>" style="background:none;"> <img class="img" alt="" src="<?php echo base_url().getThemeName(); ?>/images/pdf.png"></a>
						<?php }
						else if($ext[1] == 'doc' || $ext[1] == 'docx')
						{?>
							<a target="_blank" href="<?php echo base_url();?>upload/trip_offer_doc/<?php echo $pd->offer_attachment; ?>" style="background:none;"> <img class="img" alt="" src="<?php echo base_url().getThemeName(); ?>/images/world_files.png"></a>
						<?php }
						else
						{?>
                        <a target="_blank" href="<?php echo base_url();?>upload/trip_offer_doc/<?php echo $pd->offer_attachment; ?>" style="background:none;"> <img class="img" alt="" src="<?php echo base_url().getThemeName(); ?>/images/pdf.png"></a>
						<?php }
					}
				}?>
              </div>
                 <div class="proposal" id="viewmore_decline_<?php echo $tp->trip_offer_id;?>" style="display:none;"> <p><?php echo $tp->offer_content;?></p></div>
              	 <div class="samdolar">
                <h1><?php echo get_currency_symbol($tp->currency_code_id).''.$tp->final_offer_amount;?></h1>
              </div>
              
               </div>
               
            
            
             <?php }}
			 else{?>
			 <div class="no_proposals">
       			 <h1>No Decline Proposals Found For This Trip.</h1>
      		</div>
			 <?php }?> 
          </div>
        </div>
        <div id="tab4">
        
         <script type="text/javascript">
			
				 function viewmoredetail_invited(x)
				 {
				 	var showid_invited  = 'viewmore_invited_'+x;
				  	var cid_invited = 'showdetail_invited_'+x;
				 
				  if($('#'+cid_invited).hasClass("view_up"))
				  {
				  	$('#'+cid_invited).removeClass('view_up');
					$('#'+cid_invited).addClass('view_down');
				  }
				  else
				  {
				  	$('#'+cid_invited).addClass('view_up');
					$('#'+cid_invited).removeClass('view_down');
				  }
				  $('#'+showid_invited).slideToggle("slow"); 
				 }
        </script>
          <div class="jaimim_crararry" id="main_propose_invited">
            <?php if($invited_trip)
			{foreach($invited_trip as $tp){
			
			 $proposal_user_info = check_user_agent($tp->invite_to_user_id);
			 $user_image= base_url().'upload/no_image.png';					
				 if($proposal_user_info->profile_image!='') {  
						if(file_exists(base_path().'upload/user/'.$proposal_user_info->profile_image)) {
								$user_image=base_url().'upload/user/'.$proposal_user_info->profile_image;
						}
				}
						
			?>
            	 <div class="rating_center" style="margin-bottom:10px;" id="proposal_<?php echo $tp->trip_offer_id;?>">
         	     <div class="rating_center_top">
                <div class="rating_center_top_left"> 
                 <?php echo anchor('user/'.$proposal_user_info->profile_name,'<img src="'. $user_image.'" class="img" alt=""  />');?>
                </div>
                <?php /*?><div class="orenge_box" style="margin-top:0;">
                       <h6 <?php if($proposal_user_info->agent_level>9) { ?> style="padding-left:2px;" <?php } else { ?> style="padding-left:5px;" <?php } ?>><?php echo $proposal_user_info->agent_level; ?></h6> </div><?php */?>
                
                <div class="rating_center_top_right">
                  <h1><?php echo anchor('user/'.$proposal_user_info->profile_name,ucwords($proposal_user_info->first_name.' '.$proposal_user_info->last_name));?>
                </h1>
                 <?php
					$total_rate=get_user_total_rate($proposal_user_info->user_id);
					$total_review=get_user_total_review($proposal_user_info->user_id);
				?>  
                 <div class="strmn"><div class="str_sel" style="width:<?php if($total_rate>5) { ?>100<?php } else { echo round($total_rate*2);?>0<?php } ?>%;"></div></div> 
                <span>( <?php echo anchor('user/'.$proposal_user_info->profile_name. '/reviews',$total_review.' Review','class="fpass"');  ?>  ) |  <?php echo agent_total_trip($proposal_user_info->user_id);?> Trips | <?php echo anchor('user/'.$proposal_user_info->profile_name,'View Full Profile');?> |  <a href="javascript:void(0)" onclick="return hidden_trip_offer(<?php echo $tp->trip_offer_id;?>,<?php echo $tp->trip_id;?>)">Hidden</a> |  <a href="javascript:void(0)" onclick="return decline_trip_offer(<?php echo $tp->trip_offer_id;?>,<?php echo $tp->trip_id;?>)">Decline</a></span> </div>
              </div>
              <?php if($tp->trip_offer_id > 0){?>
            	 <div class="proposal">
                <h3>Proposal</h3>
                <span><?php echo date($site_setting->date_format,strtotime($tp->offer_date));?></span>
                <p><?php echo $tp->offer_content;?></p>
                <?php $proposal_offer_doc = $this->trip_model->trip_offer_doc($tp->trip_id,$tp->user_id);
				?>
                
                 <a href="javascript:void(0)" class="view_up" onclick="viewmoredetail_invited(<?php echo  $tp->trip_offer_id;?>);" id="showdetail_invited_<?php echo $tp->trip_offer_id;?>">View More</a> 
               <a href="#" class="docview">Print</a> <a href="#" class="docview">Download as :</a> 
                <?php 
				if($proposal_offer_doc)
				{
					foreach($proposal_offer_doc as $pd)
					{
						$ext =  explode('.',$pd->offer_attachment);
						if($ext[1] == 'pdf')
						{?>
							<a target="_blank" href="<?php echo base_url();?>upload/trip_offer_doc/<?php echo $pd->offer_attachment; ?>" style="background:none;"> <img class="img" alt="" src="<?php echo base_url().getThemeName(); ?>/images/pdf.png"></a>
						<?php }
						else if($ext[1] == 'doc' || $ext[1] == 'docx')
						{?>
							<a target="_blank" href="<?php echo base_url();?>upload/trip_offer_doc/<?php echo $pd->offer_attachment; ?>" style="background:none;"> <img class="img" alt="" src="<?php echo base_url().getThemeName(); ?>/images/world_files.png"></a>
						<?php }
						else
						{?>
                        <a target="_blank" href="<?php echo base_url();?>upload/trip_offer_doc/<?php echo $pd->offer_attachment; ?>" style="background:none;"> <img class="img" alt="" src="<?php echo base_url().getThemeName(); ?>/images/pdf.png"></a>
						<?php }
					}
				}?>
              </div>
              
               <div class="proposal" id="viewmore_invited_<?php echo $tp->trip_offer_id;?>" style="display:none;"> <p><?php echo $tp->offer_content;?></p></div>
                
              	 <div class="samdolar">
                <h1><?php echo get_currency_symbol($tp->currency_code_id).''.$tp->final_offer_amount;?></h1>
              </div>
              <?php }?>
              
               </div>
               
            
            
             <?php }}
			 else{?>
			 <div class="no_proposals">
       			 <h1>No Invited Proposals Found For This Trip.</h1>
      		</div>
			 <?php }?> 
          </div>
        </div>
      </div>
     <!-- <div class="view_more"> <a href="#">View more Proposals</a> </div>-->
      <div class="clear"></div>
      
      <?php  } } ?>
      
    </div>
    
      <?php $this->load->view($theme .'/layout/trip/trip_detail_sidebar');?>
    
    <div class="clear"></div>
  </div>
  
  <div class="clear"></div>
  <div class="clear"></div>
</div>

<!--Ask Question-->
<div class="modal hide fade preview_modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="header_img"></div><div class="clear"></div>
  <div class="prv_center">
     <h2>Ask Question</h2>
  </div><div class="clear"></div>
  <script type="text/javascript">
  	function checkfield()
	{
	
		var chks = document.getElementById('ask_question').value;
		if(chks.length == 0 || chks.length < 15)
		{
			document.getElementById('err_ask').style.display="block";
			return false;
		}
		else
		{
			return true;
		}
		 
		 
	}
  </script>
  <?php
	$attributes = array('name'=>'Askquestion','id'=>'Askquestion','autocomplete'=>'off','onsubmit'=>'return checkfield()');
	echo form_open('trip/askquestion/'.$trip_id,$attributes);
	?>
 <div class="ask_img">
    <textarea id="ask_question" name="ask_question"></textarea>
  </div>
  <div id="err_ask" style="margin:0 0 0 20px; color:#FF0000; font-weight:bold; display:none;">* Sorry!! Description should be of minimum 15 characters.
</div>
  <div class="ask">
  	<input type="submit" value="Ask Question" name="submit"/>
    <div class="clear"></div>
   </div>
    </form>    
  <div class="clear"> </div>
   <div class="fr"><a href="#" class="close opacity cls" data-dismiss="modal"></a></div>
</div>
<!--End-->

<!--Reply QUestion-->
<div class="modal hide fade preview_modal" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="header_img"></div><div class="clear"></div>
  <div class="prv_center">
     <h2>Reply</h2>
  </div><div class="clear"></div>
  <script type="text/javascript">
  	function checkfield_rply()
	{
	
		var chks = document.getElementById('ask_question_rply').value;
		if(chks.length == 0 || chks.length < 15)
		{
			document.getElementById('err_ask_rply').style.display="block";
			return false;
		}
		else
		{
			return true;
		}
		 
		 
	}
  </script>
  <?php
	$attributes = array('name'=>'Askquestion_rply','id'=>'Askquestion_rply','autocomplete'=>'off','onsubmit'=>'return checkfield_rply()');
	echo form_open('trip/askquestion_reply/'.$trip_id,$attributes);
	?>
 <div class="ask_img">
    <textarea id="ask_question_rply" name="ask_question_rply"></textarea>
  </div>
  <div id="err_ask_rply" style="margin:0 0 0 20px; color:#FF0000; font-weight:bold; display:none;">* Sorry!! Description should be of minimum 15 characters.
</div>
  <div class="ask">
      <input type="hidden" name="trip_conversation_id" id="trip_conversation_id" value=""/>
      
  	<input type="submit" value="Reply" name="submit"/>

    <div class="clear"></div>
   </div>
    </form>    
  <div class="clear"> </div>
   <div class="fr"><a href="#" class="close opacity cls" data-dismiss="modal"></a></div>
  
  
</div>
<!--End Reply Question-->
