<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>  
 <link href="http://code.google.com//apis/maps/documentation/javascript/examples/default.css" rel="stylesheet" type="text/css">
<script>

	$(document).ready(function(){
		initialize();
	});

      function initialize() {
	  
	  	var trp_st_lat = '<?php echo $trip_from_lattitude;?>';
		var trp_st_lng =  '<?php echo $trip_from_longitude;?>';
		var trp_end_lat = '<?php echo $trip_to_lattitude;?>';
		var trp_end_lng = '<?php echo $trip_to_longitude;?>';
		
		var map_lat = parseInt(parseInt(trp_st_lat) + parseInt(trp_end_lat) / 2);
		var map_lng = parseInt(parseInt(trp_st_lng) + parseInt(trp_end_lng) / 2);
		
		/*var map_lat = trp_st_lat;
		var map_lng = trp_st_lng;*/
		
	    var myLatLng = new google.maps.LatLng(map_lat, map_lng);
        var mapOptions = {
          zoom: 2,
          center: myLatLng,
          mapTypeId: google.maps.MapTypeId.TERRAIN
        };

	/* new google.maps.LatLng(-18.142599, 178.431),
            new google.maps.LatLng(-27.46758, 153.027892)*/
        var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
		 
        var flightPlanCoordinates = [
            new google.maps.LatLng(trp_st_lat, trp_st_lng),
            new google.maps.LatLng(trp_end_lat, trp_end_lng)
           
        ];
        var flightPath = new google.maps.Polyline({
          path: flightPlanCoordinates,
          strokeColor: '#1AB5F6',
          strokeOpacity: 1.0,
          strokeWeight: 2
        });
		
        flightPath.setMap(map);
		
      }
    </script>
 
  <div style="height:500px; width:500px;" id="map_canvas"></div>