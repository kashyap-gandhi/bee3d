<?php
$site_setting=site_setting();
$data['site_setting']=$site_setting;
?>
<!-- start container ================================================== -->

<div class="container">
	<div class="col-md-9" style="margin:25px 0 0 0; padding:0;">
    	<div class="span9" style="margin:0; padding:0;">
        	<div class="search-tital"><h1><?php echo $this->lang->line('search.title')?></h1>
			<br/><br/>
       <?php
			$attributes = array('name'=>'frm_search_task_worker','class'=>'fr marT10');
			
			if($cat_name!='')
			{
				echo form_open('challenge/category/'.$cat_name,$attributes);
			
			} else {
				
					echo form_open('challenge/search',$attributes);
				
			}		
				
	   ?>
            <input type="text" name="search" id="search"   placeholder="<?php echo $this->lang->line('search.text_search')?>" value="<?php echo urldecode($search); ?>" />
            <input type="submit" class="submit" value="Search" class="btn yellow-btn">
        </form>
         </div>
            
            
            <div class="totel-task"> 
            
            <span><?php echo $total_rows; ?> <?php echo $this->lang->line('search.tasks_found')?></span>
            
           <strong><?php if($total_rows != 0) {  if($offset==0) { echo "1"; }  else { echo $offset; } echo '-';  if(($offset+$limit)>=$total_rows) { echo $total_rows; } else { echo $offset+$limit; } echo ' of '.$total_rows; }?></strong>
           
            </div>
            <div class="task-lissting">
        
			
            
            <ul>
            
  			<?php 
						
						$worker_arr=array();
						
							if($result) {
									foreach($result as $row) { 
									
							?>
                        <li>
                       
                        	<div class="task-first">
                        
                           <div class="task-discripcation">
                            	<h2><?php echo anchor('challenge/'.$row->challenge_slug,$row->challenge_title,' class=""');?></h2>
                                
                                <p>
									<?php                                            
										$challenge_criteria= strip_tags($row->challenge_criteria);		
										$challenge_criteria=str_replace('KSYDOU','"',$challenge_criteria);
										$challenge_criteria=str_replace('KSYSING',"'",$challenge_criteria);
		
										$strlen = strlen($challenge_criteria);
										if($strlen > 50) { echo substr($challenge_criteria,0,80).' ...';}
										else { echo $challenge_criteria; } 
                                    ?>   
								</p>
                               <h4>
                                <?php  
									$taskdate = $row->challenge_date;
									echo getDuration($taskdate,$row->challenge_id);
								?>
                                </h4>
                                
                               <strong>Points</strong> 
                               <span><?php echo $row->challenge_point;?></span>
                           </div>
                            
                        </div>
                        
                            <div class="clear"></div>
                        </li>
                        
                        <?php } }
						
						
						 ?>
            		</ul>

					 <?php if($total_rows>10) { ?>
					<div class="gonext">
                    <?php echo $page_link; ?>
                    </div>
				<?php } ?>
                    <div class="clear"></div>
        
         </div>
        </div>
     
    <?php echo $this->load->view($theme.'/layout/challenge/search_side_bar',$data); ?>  
        <div class="clear"></div>


    </div>
</div>


<!-- end container ================================================== -->

