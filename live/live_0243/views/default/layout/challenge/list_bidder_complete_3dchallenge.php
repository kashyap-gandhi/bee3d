<div id="wrapper_all">
  <?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
  <!-- mobile navigation -->
  <nav id="mobile_navigation"></nav>
  <section id="breadcrumbs">
    <div class="container">
      <ul>
        <li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
        <li><span>Dashboard</span></li>
      </ul>
    </div>
  </section>
  <section class="container clearfix main_section">
    <div id="main_content_outer" class="clearfix">
      <div id="main_content"> 
        
        <!-- main content -->
        
        <h2>Challenge</h2>
        <?php if($msg!='') { 
						if($msg == 'success')
						{?>
					
							<div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "Category inserted successfully."; ?>
								
							</div>
					 <?php  }
					 if($msg == 'success_challenge')
					 {?>
					 <div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "challenge inserted successfully."; ?>
								
							</div>
					 <?php }
					 if($msg == 'update_challenge')
					 {?>
					  <div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "challenge updated successfully."; ?>
								
							</div>
					 <?php }
					 if($msg == 'delete_challenge')
					 {?>
					  <div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "challenge deleted successfully."; ?>
								
							</div>
					 <?php }
					 } ?>
        <!-- ************************************************************** -->
        <div class="row">
          <div class="col-sm-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">Closed Challenge 
				<span class="span_right">
				<?php echo $this->load->view($theme .'/layout/challenge/common_bidder_menu')?>
				</h4>
              </div>
              <div class="panel-body">
                <div class="row">
                <?php if($result)
				{
					foreach($result as $res)
				 {
				 	 $challenge_bid_info = get_challenge_win_info($res->challenge_id);
				 	
				 	?>
                  <div class="jobs-box clearfix">
                    <div class="col-sm-8">
                      <h4 class="no-margin"> <a href="<?php echo site_url("challenge/".$res->challenge_slug); ?>"> <?php echo ucfirst($res->challenge_title);?></a> </h4>
                      <p> <?php echo ucfirst($res->challenge_meta_description);?>  </p>
                      <p> <strong> <?php echo getDuration($res->challenge_start_date); ?> </strong></p>
                    </div>
                    <div class="col-sm-4">
                      <div class="jobsbox-rt-top">
                        <div class="margin-bottom-15">
                          <ul class="list-unstyled">
						   <?php 
						  $challenge_status='';
						  if($res->challenge_activity_status == 0)
						  {
						  	$challenge_status='Posted';
						  }
						  else if($res->challenge_activity_status == 1)
						  {
						  	$challenge_status='Assigned';
						  }
						  else if($res->challenge_activity_status == 2)
						  {	
						  	$challenge_status='Completed';
						  }
						  else 
						  {
						  	$challenge_status='Closed';
						  }

						  ?>
						   <li> <strong> Challenge Status : </strong> <?php echo $challenge_status; ?></li>
                            <li> <strong> Posted : </strong> <?php echo date($site_setting->date_format,strtotime($res->challenge_start_date)) ?></li>
                            <li> <strong> Post by : </strong> 
							<?php 
								$poster_info = get_user_info($res->user_id); 
								if($poster_info)
								{
									echo $poster_info->full_name; 
								}
								else
								{
									echo "N/A";
								}
							?> 
							</li>
							
							 <?php if($challenge_bid_info)
					  		{
								$runner_info = get_user_info($challenge_bid_info->user_id); 
								if($runner_info)
								{
									$user_name = ucwords($runner_info->full_name);
								}
								else
								{
									$user_name = 'N/A';
								}
							?>
							 <li> <strong> Assigned to : </strong> <?php echo $user_name; ?> </li>
							<?php }?>		 
							
							 <?php
						  if($challenge_bid_info)
						  {?>
						  	
						  	  <li> <strong>Challenge  accepted points   : </strong> <?php echo $challenge_bid_info->bid_price;  ?></li>
						  <?php }
						  ?>
						  
						  
                          </ul>
                        </div>
                      </div>
                      <div class=""> 
					   <!-- <a href="<?php echo site_url("challenge/offer/".$res->challenge_id); ?>"class="btn btn-success" type="button">Bids : <?php echo bid_counter($res->challenge_id); ?></a>  -->
					  <label class="btn btn-danger">Points : <?php echo $res->challenge_point;?></label> 
					  
					  </div>
					  <br>
                    </div>
                  </div> <!-- Loop -->
                  <?php }
                 
                     }
				else
				{
					echo "<div style='margin-left:10px;'>No records found!!</div>";
				}?>
                   <div><?php echo $page_link;  ?></div> 
                </div>
              </div>
            </div>
          </div>
        </div>
        
         
      </div>
    </div>
    <?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?> </section>
  <div id="footer_space"></div>
</div>