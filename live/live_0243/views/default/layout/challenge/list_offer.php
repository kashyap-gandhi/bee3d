<script type="text/javascript">
function delete_challenge(id)
{
	var ans = confirm("Are you sure, you want to delete challenge?");
	if(ans)
	{
		location.href = "<?php echo base_url(); ?>challenge/delete_me/"+id;
	}else{
		return false;
	}
}
</script>
	<!-- aditional stylesheets -->
		<!-- responsive table -->
			<link href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>

		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>challenge</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
					<?php if($msg!='') { 
						if($msg == 'success')
						{?>
					
							<div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "Category inserted successfully."; ?>
								
							</div>
					 <?php  }
					 if($msg == 'accept')
					 {?>
					 <div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "offer accepted successfully."; ?>
								
							</div>
					 <?php }
					 if($msg == 'update_challenge')
					 {?>
					  <div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "challenge updated successfully."; ?>
								
							</div>
					 <?php }
					 if($msg == 'delete_challenge')
					 {?>
					  <div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "challenge deleted successfully."; ?>
								
							</div>
					 <?php }
					 } if($msg == 'complete')
					 {?>
					  <div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "challenge completed successfully."; ?>
								
							</div>
					 <?php }
					  ?>

						<!-- main content -->
						<div class="row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Challenge Offer
										<span class="span_right pad6"><a href="<?php echo site_url('challenge/add_me');?>" class=""><h4>Add challenge</h4></a></span>
									     <!-- <span class="span_right pad6"><a href="<?php echo site_url('challenge/loss');?>" class=""><h4>Lost</h4></a></span>	
										<span class="span_right pad6"><a href="<?php echo site_url('challenge/closed');?>" class=""><h4>Closed</h4></a></span>
										<span class="span_right pad6"><a href="<?php echo site_url('challenge/assigned');?>" class=""><h4>Assigned</h4></a></span>	
										<span class="span_right pad6"><a href="<?php echo site_url('challenge/open');?>" class=""><h4>Open</h4></a></span> -->
										
										<span class="span_right pad6"><a href="<?php echo site_url('challenge/all');?>" class=""><h4>All</h4></a></span>
										
									
										
										
										
										
										</h4>
									</div>
									
									<table id="resp_table" class="table toggle-square" data-filter="#table_search" data-page-size="40">
										<thead>
											<tr>
												
												<th>User Name</th>
												<th>Price</th>
												<th>Description</th>
												<th>Won</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php if($result)
											{
												foreach($result as $res)
												{?>
											<tr>
												
												<td><?php echo ucfirst($res->full_name);?></td>
												<td><?php echo $res->bid_price;?></td>
												<td><?php echo $res->bid_description;?></td>
												<td><?php if($res->is_won == 1){echo "<img src='".base_url().getThemeName()."/images/star.png' width='30px' height='30px'>";}else {echo "no" ;}?></td>
											
												<td>
													<?php
													if($res->challenge_activity_status == 0 || $res->is_won == 1 )
													{
													?>
													<a class="btn btn-primary" href="<?php echo site_url('challenge/conversation/'.$res->challenge_bid_id);?>"><span data-toggle="tooltip" data-placement="top auto" title="Conversation">Conversation</span></a>
													<?php }?>
													<?php
													if($res->challenge_activity_status == 0 && strtotime(date("Y-m-d",strtotime($res->challenge_end_date))) >= strtotime(date("Y-m-d")))
													{
													?>
													<a class="btn btn-success" href="<?php echo site_url("challenge/invoice/".$res->challenge_bid_id); ?>""><span data-toggle="tooltip" data-placement="top auto" title="Accept offer">Accept</span></a>
												<?php }
												?>
												</td>
											</tr>	
											<?php }}
											else
											{?>
											<tr>
												<td colspan="3">No Records Found!!</td>
												
											</tr>
											<?php }?>
											
											
										</tbody>
									
											<tr>
												<td colspan="6" class="text-center">
													<ul class="">
													<?php echo $page_link;?>
													</ul>
												</td>
											</tr>
									
									</table>
								</div>
							</div>
						</div>
						
					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
			<div id="footer_space"></div>
		</div>

       <!-- responsive table -->
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.sort.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.filter.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.paginate.js"></script>

			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/pages/ebro_responsive_table.js"></script>
