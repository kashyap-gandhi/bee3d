<script type="text/javascript">
function delete_challenge(id)
{
	var ans = confirm("Are you sure, you want to delete challenge?");
	if(ans)
	{
		location.href = "<?php echo base_url(); ?>challenge/delete_me/"+id;
	}else{
		return false;
	}
}
</script>
	<!-- aditional stylesheets -->
		<!-- responsive table -->
			<link href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>

		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>challenge Loss</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
					<?php if($msg!='') { 
						if($msg == 'success')
						{?>
					
							<div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "Category inserted successfully."; ?>
								
							</div>
					 <?php  }
					 if($msg == 'success_challenge')
					 {?>
					 <div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "challenge inserted successfully."; ?>
								
							</div>
					 <?php }
					 if($msg == 'update_challenge')
					 {?>
					  <div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "challenge updated successfully."; ?>
								
							</div>
					 <?php }
					 if($msg == 'delete_challenge')
					 {?>
					  <div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "challenge deleted successfully."; ?>
								
							</div>
					 <?php }
					 } ?>

						<!-- main content -->
						<div class="row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Manage 3D challenge
									<span class="span_right pad6"><a href="<?php echo site_url('challenge/add_me');?>" class=""><h4>Add challenge</h4></a></span>
									     <span class="span_right pad6"><a href="<?php echo site_url('challenge/loss');?>" class=""><h4>Lost</h4></a></span>	
										<span class="span_right pad6"><a href="<?php echo site_url('challenge/closed');?>" class=""><h4>Closed</h4></a></span>
										<span class="span_right pad6"><a href="<?php echo site_url('challenge/assigned');?>" class=""><h4>Assigned</h4></a></span>	
										<span class="span_right pad6"><a href="<?php echo site_url('challenge/open');?>" class=""><h4>Open</h4></a></span>
										</h4>
									</div>
									
									<table id="resp_table" class="table toggle-square" data-filter="#table_search" data-page-size="40">
										<thead>
											<tr>
												<th>Sr. No.</th>
												<th>Title</th>
												<th>Category</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php if($result)
											{
												foreach($result as $res)
												{?>
											<tr>
												<td><?php echo $res->challenge_id;?></td>
												<td><?php echo ucfirst($res->challenge_title);?></td>
												<td><?php echo ucfirst($res->category_name);?></td>
												<td>
													<a class="btn btn-primary" href="<?php echo site_url('challenge/'.$res->challenge_slug);?>"><span data-toggle="tooltip" data-placement="top auto" title="View">View</span></a>
													<a class="btn btn-success" href="<?php echo site_url('challenge/edit_me/'.$res->challenge_id);?>"><span data-toggle="tooltip" data-placement="top auto" title="Edit Challenge">Edit</span></a>
													<a class="btn btn-danger" href="javascript://" onclick="delete_challenge('<?php echo $res->challenge_id;?>');"><span data-toggle="tooltip" data-placement="top auto" title="Delete Challenge">Delete</span></a>
												</td>
											</tr>	
											<?php }}
											else
											{?>
											<tr>
												<td colspan="3">No Records Found!!</td>
												
											</tr>
											<?php }?>
											
											
										</tbody>
									
											<tr>
												<td colspan="6" class="text-center">
													<ul class="">
													<?php echo $page_link;?>
													</ul>
												</td>
											</tr>
									
									</table>
								</div>
							</div>
						</div>
						
					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
			<div id="footer_space"></div>
		</div>

       <!-- responsive table -->
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.sort.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.filter.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/foot