<div id="wrapper_all">
  <?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
  <!-- mobile navigation -->
  <nav id="mobile_navigation"></nav>
  <section id="breadcrumbs">
    <div class="container">
      <ul>
        <li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
        <li><span>Dashboard</span></li>
      </ul>
    </div>
  </section>
  <section class="container clearfix main_section">
    <div id="main_content_outer" class="clearfix">
      <div id="main_content"> 
        
        <!-- main content -->
        
        <h2>Dashboard</h2>
        <div class="row">
          <div class="col-sm-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">Lorem ipsum <span class="span_right"><a class="" href="#">Add Video</a></span> </h4>
              </div>
              <table data-page-size="40" data-filter="#table_search" class="table toggle-square footable-loaded footable no-paging" id="resp_table">
                <thead>
                  <tr>
                    <th>Sr. No.</th>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr style="display: table-row;">
                    <td>1</td>
                    <td>Video2</td>
                    <td>Funny</td>
                    <td><a href="#" class="btn btn-primary">View</a> <a href="#" class="btn btn-success">Edit</a> <a href="javascript://" class="btn btn-danger">Delete</a></td>
                  </tr>
                  <tr style="display: table-row;">
                    <td>2</td>
                    <td>Test Update</td>
                    <td>Funny</td>
                    <td><a href="#" class="btn btn-primary">View</a> <a href="#" class="btn btn-success">Edit</a> <a href="javascript://" class="btn btn-danger">Delete</a></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        
        <!-- ************************************************************** -->
        <div class="row">
          <div class="col-sm-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">My Running Jobs <span class="span_right"><a class="" href="#">All </a> | <a href="#" class="btn btn-primary">Post jobs</a> </span> </h4>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="jobs-box clearfix">
                    <div class="col-sm-9">
                      <h4 class="no-margin"> lorem ipsum </h4>
                      <p> Nulla congue nec est suscipit molestie. Nulla dictum nisi ac ligula fringilla, sed tristique elit viverra. Donec accumsan turpis turpis, at pulvinar enim imperdiet eu. Cras volutpat nisi sed metus tincidunt interdum. Donec pharetra nisi quis mauris ultricies aliquam. Mauris a tortor dui. Vestibulum facilisis orci ut justo ornare, luctus sodales nulla elementum. Duis vel varius sem. Etiam odio leo, tempus eget ligula accumsan, ultrices rutrum nunc. Cras et nisi dui. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer ultrices lectus egestas urna bibendum, a dignissim lorem scelerisque. </p>
                      <p> <strong> 29 min ago </strong></p>
                    </div>
                    <div class="col-sm-3">
                      <div class="jobsbox-rt-top">
                        <div class="margin-bottom-15">
                          <ul class="list-unstyled">
                            <li> <strong> Posted : </strong> 21 Jun 2014, 12:32 am</li>
                            <li> <strong> Post by : </strong> Lorem ipsim </li>
                          </ul>
                        </div>
                      </div>
                      <div class=""> <a href="#"class="btn btn-primary" type="button">Bids</a> <a href="#"class="btn btn-success" type="button">Points : 1121</a> <a href="#"class="btn btn-danger" type="button">Demo</a> </div>
                    </div>
                  </div> <!-- Loop -->
                  
                  <div class="jobs-box clearfix">
                    <div class="col-sm-9">
                      <h4 class="no-margin"> lorem ipsum </h4>
                      <p> Nulla congue nec est suscipit molestie. Nulla dictum nisi ac ligula fringilla, sed tristique elit viverra. Donec accumsan turpis turpis, at pulvinar enim imperdiet eu. Cras volutpat nisi sed metus tincidunt interdum. Donec pharetra nisi quis mauris ultricies aliquam. Mauris a tortor dui. Vestibulum facilisis orci ut justo ornare, luctus sodales nulla elementum. Duis vel varius sem. Etiam odio leo, tempus eget ligula accumsan, ultrices rutrum nunc. Cras et nisi dui. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer ultrices lectus egestas urna bibendum, a dignissim lorem scelerisque. </p>
                      <p> <strong> 29 min ago </strong></p>
                    </div>
                    <div class="col-sm-3">
                      <div class="jobsbox-rt-top">
                        <div class="margin-bottom-15">
                          <ul class="list-unstyled">
                            <li> <strong> Posted : </strong> 21 Jun 2014, 12:32 am</li>
                            <li> <strong> Post by : </strong> Lorem ipsim </li>
                          </ul>
                        </div>
                      </div>
                      <div class=""> <a href="#"class="btn btn-primary" type="button">Bids</a> <a href="#"class="btn btn-success" type="button">Points : 1121</a> <a href="#"class="btn btn-danger" type="button">Demo</a> </div>
                    </div>
                  </div> <!-- Loop -->
                </div>
              </div>
            </div>
          </div>
        </div>
        
          <!-- ************************************************************** -->
        <div class="row">
          <div class="col-sm-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">My Running Jobs <span class="span_right"><a class="" href="#">All </a> | <a href="#" class="btn btn-primary">Post jobs</a> </span> </h4>
              </div>
              <div class="panel-body">
                 <div class="chat_app">
							<div class="row">
								<div class="col-sm-11 col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title pull-left">Chatroom</h4>
											<div class="btn-group btn-group-sm pull-right">
												<button data-title="New Chat" data-container="body" data-toggle="tooltip" type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-comments"></span></button>
												<button data-title="Settings" data-container="body" data-toggle="tooltip" type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-cog"></span></button>
												<button data-title="Close Chat" data-container="body" data-toggle="tooltip" type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-remove"></span></button>
											</div>
										</div>
										<div class="chat_messages">
																						<div class="chat_message ch_right">
												<img class="img-thumbnail" src="img/avatars/avatar_16.jpg" alt="">
												<div class="chat_message_body">
													<p>Placeat qui non quibusdam iure est. Accusantium aut ab tempora laboriosam aut. Ullam voluptates quo qui repellendus deleniti qui.</p>
													<p class="chat_message_date">13:07:52, 03.04.86</p>
												</div>
											</div>
											<div class="chat_message">
												<img class="img-thumbnail" src="img/avatars/avatar_10.jpg" alt="">
												<div class="chat_message_body">
													<p>Quis quia praesentium laborum numquam provident illum ut. Laudantium officiis culpa quae rerum vel. Ut doloribus ea nisi at nisi.</p>
													<p class="chat_message_date">07:57:36, 29.11.77</p>
												</div>
											</div>
											<div class="chat_message ch_right">
												<img class="img-thumbnail" src="img/avatars/avatar_5.jpg" alt="">
												<div class="chat_message_body">
													<p>Quo assumenda culpa sint est. Inventore libero magnam consequatur pariatur facere aspernatur ut. Omnis pariatur deserunt quas et laudantium quo. Velit tempora corporis qui omnis qui.</p>
													<p class="chat_message_date">02:51:32, 11.04.13</p>
												</div>
											</div>
											<div class="chat_message">
												<img class="img-thumbnail" src="img/avatars/avatar_15.jpg" alt="">
												<div class="chat_message_body">
													<p>Eveniet in ab quae est. Et id eum consequatur adipisci nam. Esse voluptates ut officiis iusto. Tenetur inventore exercitationem est beatae. Est ex expedita in ut dolore vel vitae.</p>
													<p class="chat_message_date">22:49:18, 04.08.96</p>
												</div>
											</div>
											<div class="chat_message ch_right">
												<img class="img-thumbnail" src="img/avatars/avatar_17.jpg" alt="">
												<div class="chat_message_body">
													<p>Culpa esse omnis veritatis delectus adipisci. Quidem nemo molestias dolorem et omnis consequuntur. Dolorum dolor ut vitae consequatur labore quis. Et voluptatum totam quaerat odio quis commodi rem.</p>
													<p class="chat_message_date">11:23:46, 10.02.91</p>
												</div>
											</div>
											<div class="chat_message">
												<img class="img-thumbnail" src="img/avatars/avatar_13.jpg" alt="">
												<div class="chat_message_body">
													<p>Qui assumenda magnam molestiae accusantium. Qui ad illum sint recusandae est. Ut explicabo illum doloremque beatae autem ut. Quo repellendus qui facilis ut rem et minus.</p>
													<p class="chat_message_date">18:53:20, 15.11.93</p>
												</div>
											</div>
											<div class="chat_message ch_right">
												<img class="img-thumbnail" src="img/avatars/avatar_4.jpg" alt="">
												<div class="chat_message_body">
													<p>Voluptatem magni voluptatem libero id et. Harum hic cumque porro. Itaque quia rerum vitae.</p>
													<p class="chat_message_date">07:09:56, 29.06.11</p>
												</div>
											</div>
											<div class="chat_message">
												<img class="img-thumbnail" src="img/avatars/avatar_1.jpg" alt="">
												<div class="chat_message_body">
													<p>Qui voluptatem non iure dicta quod atque qui. Ut qui officiis amet excepturi ipsa. Iste magni aut incidunt dolore sapiente facilis nam.</p>
													<p class="chat_message_date">19:57:10, 11.06.05</p>
												</div>
											</div>
											<div class="chat_message ch_right">
												<img class="img-thumbnail" src="img/avatars/avatar_17.jpg" alt="">
												<div class="chat_message_body">
													<p>Sint aliquam temporibus omnis aut ea porro. Porro ad deleniti pariatur est. Ipsam a accusamus expedita at voluptas.</p>
													<p class="chat_message_date">04:26:28, 30.11.84</p>
												</div>
											</div>
											<div class="chat_message">
												<img class="img-thumbnail" src="img/avatars/avatar_11.jpg" alt="">
												<div class="chat_message_body">
													<p>Aut quis qui eius fugit sit cupiditate. Omnis beatae veniam reprehenderit non omnis ut molestias. Sequi animi tempora sapiente. A enim rem quia repudiandae blanditiis.</p>
													<p class="chat_message_date">19:07:12, 08.02.95</p>
												</div>
											</div>
											<div class="chat_message ch_right">
												<img class="img-thumbnail" src="img/avatars/avatar_1.jpg" alt="">
												<div class="chat_message_body">
													<p>Sint eum aut consequuntur ea. Repellendus doloribus sapiente quo sit voluptate. Recusandae nulla incidunt maiores hic. Qui repellendus iste ut modi mollitia et.</p>
													<p class="chat_message_date">23:53:23, 25.09.92</p>
												</div>
											</div>
										</div>
										<div class="panel-footer">
											 <div class="form-group">
													<textarea class="form-control" rows="3" cols="10" id="reg_textarea" name="reg_textarea"></textarea>
												</div>
                                                
                                               <div class="form-group">
													<button class="btn btn-primary" type="button">Complete job</button>
                                                    <button class="btn btn-danger" type="button">Send</button>
												</div> 
										</div>
									</div>
								</div>
								<!--<div class="col-sm-4 col-md-3 chat_users">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title pull-left">Contacts (12)</h4>
											<button type="button" class="btn btn-default btn-sm pull-right"><span class="icon-refresh"></span></button>
										</div>
										<table class="table table-striped">
											<tbody>
																								<tr>
													<td>
														<a href="#">
															<span class="stat stat_online"></span>
															Miss Edwin Turner IV															
														</a>
													</td>
												</tr>
												<tr>
													<td>
														<a href="#">
															<span class="stat stat_busy"></span>
															Jany Roob															
														</a>
													</td>
												</tr>
												<tr>
													<td>
														<a href="#">
															<span class="stat stat_offline"></span>
															Dasia Morissette															
														</a>
													</td>
												</tr>
												<tr>
													<td>
														<a href="#">
															<span class="stat stat_offline"></span>
															Bettye Ortiz															
														</a>
													</td>
												</tr>
												<tr>
													<td>
														<a href="#">
															<span class="stat stat_online"></span>
															Lula Mraz															
														</a>
													</td>
												</tr>
												<tr>
													<td>
														<a href="#">
															<span class="stat stat_offline"></span>
															Ada Considine															
														</a>
													</td>
												</tr>
												<tr>
													<td>
														<a href="#">
															<span class="stat stat_offline"></span>
															Andy Hilpert															
														</a>
													</td>
												</tr>
												<tr>
													<td>
														<a href="#">
															<span class="stat stat_busy"></span>
															Vincent Cassin															
														</a>
													</td>
												</tr>
												<tr>
													<td>
														<a href="#">
															<span class="stat stat_busy"></span>
															Ali Crist															
														</a>
													</td>
												</tr>
												<tr>
													<td>
														<a href="#">
															<span class="stat stat_online"></span>
															Ransom Gleichner															
														</a>
													</td>
												</tr>
												<tr>
													<td>
														<a href="#">
															<span class="stat stat_offline"></span>
															Elyse Blanda															
														</a>
													</td>
												</tr>
												<tr>
													<td>
														<a href="#">
															<span class="stat stat_offline"></span>
															Cullen Jacobs															
														</a>
													</td>
												</tr>
												<tr>
													<td>
														<a href="#">
															<span class="stat stat_busy"></span>
															Miss Stephen Ryan DVM															
														</a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>-->
							</div>
						</div>
              </div>
            </div>
          </div>
        </div>
        
        <!-- ************************************************************** --> 
        
      </div>
    </div>
    <?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?> </section>
  <div id="footer_space"></div>
</div>