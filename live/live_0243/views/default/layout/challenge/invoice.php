<div id="wrapper_all">
  <?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
  <!-- mobile navigation -->
  <nav id="mobile_navigation"></nav>
  <section id="breadcrumbs">
    <div class="container">
      <ul>
        <li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
        <li><span>Dashboard</span></li>
      </ul>
    </div>
  </section>
  <section class="container clearfix main_section">
    <div id="main_content_outer" class="clearfix">
      <div id="main_content"> 
        
        <!-- main content -->
        
        <h2>Points Pay</h2>
       
        
        <!-- ************************************************************** -->
      
        
          <!-- ************************************************************** -->
        <div class="row">
          <div class="col-sm-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                Points Pay <!-- <span class="span_right">
                <a class="" href="#">All </a> | <a href="#" class="btn btn-primary">Post jobs</a> </span>  -->
                </h4>
              </div>
              <div class="panel-body">
                 <div class="chat_app">
							<div class="row">
								<div class="col-sm-11 col-md-12">
									<div class="panel panel-default">
									
										<table width="100%" cellspacing="4" cellpadding="4" border="0">
										<tr>
											<td width="30%"> Challenge Title</td>
										   <td width="70%"> <?php echo $challenge_details->challenge_title; ?></td>
										</tr>
									<tr>
											<td width="30%"> Challenge Points</td>
										   <td width="70%"> <?php echo $challenge_details->challenge_point; ?></td>
										</tr>
                                        <tr>
											<td colspan="2">	<hr></td>
										</tr>
										<tr>
											<td width="30%"> Assign To</td>
											
										   <td width="70%"> <b><?php echo getUserProfileName($bid_detail["user_id"]); ?></b></td>
										</tr>
										
										
										<!--<tr>
											<td width="30%"> Pay By </td>
										   <td width="70%"> Wallet<input type="radio" checked="checked" /></td>
										</tr>-->
										<tr>
											<td width="30%"> Offer Points </td>
										   <td width="70%"> <?php echo $bid_detail["bid_price"]; ?></td>
										</tr>
										<tr>
											<td colspan="2">	<hr></td>
										</tr>
										<tr>
											<td width="30%"> Points in wallet </td>
										   <td width="70%"> <?php echo my_wallet_amount(); ?></td>
										</tr>
                                        
                                        <tr>
											<td width="30%"> After Pay Points in wallet </td>
										   <td width="70%"> <?php echo my_wallet_amount()- $bid_detail["bid_price"]; ?></td>
										</tr>
										
										<tr>
											<td colspan="2" align=""> </td>
										</tr>
										
									
									<?php
									if(my_wallet_amount() >= $bid_detail["bid_price"])
									{
									?>
									<tr>
											<td width="30%"> &nbsp;</td>
									<td width="70%"><a href="<?php echo site_url("challenge/pay/".$challenge_bid_id); ?>"class="btn btn-success" type="button">Pay</a></td>
									</tr>
									<?php }
                                   else {?>	
                                   	<tr>
                                   			<td width="30%"> &nbsp;</td>
                                  <td width="70%"> <a href="<?php echo site_url("package/all/".base64_encode("purchase")); ?>"class="btn btn-success" type="button">Pay</a>	
                                   	<?php }?>
                                   	</tr>
										</table>
									</div>
								</div>
								
							</div>
						</div>
              </div>
            </div>
          </div>
        </div>
        
        <!-- ************************************************************** --> 
        
      </div>
    </div>
    <?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?> </section>
  <div id="footer_space"></div>
</div>