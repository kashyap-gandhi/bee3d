<script src="<?php echo base_url().getThemeName(); ?>/js/jquery-ui-1.9.0.custom.js"></script>
<script>
	$(function() {
		$( "#tab_prd" ).tabs();
	});
</script>


<div id="bidcontener"><!--start_#bidcontener-->

	<div class="bid_form_center">
	
		<div class="left_side">
		
			<div class="bid_page_top">
			
				<div class="bid_page_right">
				 
					<?php if($agent_info){ ?>
						 <?php /*?><div class="orenge_box" style="margin-left:120px; margin-top:-9px;">
							<h6><?php echo $agent_info->agent_level?></h6>
						</div><?php */?>
					<?php } ?>
					
					
					<div class="bid_page_photo">
						<?php 
							////////======= user image 
							$user_image= base_url().'upload/no_image.png';
		 					
							 if($user_info->profile_image!='') {  
								if(file_exists(base_path().'upload/user_thumb/'.$user_info->profile_image)) {
									$user_image=base_url().'upload/user_orig/'.$user_info->profile_image;	
								}					
							}
							echo anchor('user/'.$user_info->profile_name,'<img src="'.$user_image.'" alt="" class="img"/>');
						?>		
					</div>
					
				</div>
				
				<div class="bid_page_left">
				
					<?php if($agent_info){ ?>
						<h1> <?php echo $agent_info->agency_name; ?></h1>
						
						<strong>Contact Person &nbsp; :</strong>                    
						<span>&nbsp;&nbsp;<?php echo ucfirst($agent_info->contact_person_name); ?></span><br />
						
						<label>Support &nbsp; :</label>
						<span class="support">&nbsp; &nbsp;<?php echo $agent_info->agency_customer_support_no; ?>(24X7, 365 days - help) for travel</span><br />
						
						<label>Agency Website &nbsp; :</label>
						<span class="support">&nbsp; &nbsp;<?php echo anchor($agent_info->agency_website,$agent_info->agency_website,'target="_blank"');?></span><br />
						
						<label>Address &nbsp; :</label>
						<span style="margin:0; padding:0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#585858; font-weight:normal; " class="support">&nbsp; &nbsp;<?php echo $agent_info->agency_location; ?></span><br />
					<?php } else { ?>
						
						<h1><?php echo ucfirst($user_info->first_name).' '.ucfirst($user_info->last_name); ?></h1>
						
						<strong>About User &nbsp; :</strong>                    
						<span style="font-weight:normal;">&nbsp;&nbsp;<?php echo $user_info->about_user; ?></span><br />

						<label>Address &nbsp; :</label>
						<span style="margin:0; padding:0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#585858; font-weight:normal; " class="support">&nbsp; &nbsp;<?php echo $user_info->user_location; ?></span><br />
					
					<?php } ?>	
					
					<label>Social page &nbsp; :</label>
					<span class="support">&nbsp; &nbsp;
						
						<?php  if($user_info->facebook_link!='') { ?>
						
						<a href="<?php echo $user_info->facebook_link; ?>" target="_blank"><img src="<?php echo base_url().getThemeName(); ?>/images/facebook.png" alt="" class="img"  /></a>
								
						<?php } if($user_info->twitter_link!='') { ?>
						
						<a href="<?php echo $user_info->twitter_link; ?>" target="_blank"><img src="<?php echo base_url().getThemeName(); ?>/images/twister.png" alt="" class="img"  /></a>
						 
						<?php } if($user_info->linkedin_link!='') { ?>
						 
						<a href="<?php echo $user_info->linkedin_link; ?>" target="_blank"><img src="<?php echo base_url().getThemeName(); ?>/images/linkedin.png" alt="" class="img"  /></a>
						 
						<?php }if($user_info->youtube_link!='') { ?>
						 
						<a href="<?php echo $user_info->youtube_link; ?>" target="_blank"><img src="<?php echo base_url().getThemeName(); ?>/images/youtube.png" alt="" class="img"  /></a>
						 
						<?php } if($user_info->yelp_link!='') { ?>
						
						<a href="<?php echo $user_info->yelp_link; ?>" target="_blank"><img src="<?php echo base_url().getThemeName(); ?>/images/yelp.png" alt="" class="img"  /></a>  
						  
						<?php } if($user_info->blog_link!='') { ?>
						 
						<a href="<?php echo $user_info->blog_link; ?>" target="_blank"><img src="<?php echo base_url().getThemeName(); ?>/images/rss.png" alt="" class="img"  /></a>	
						 
						<?php } if($user_info->own_site_link!='') { ?>
						
						<a href="<?php echo $user_info->own_site_link; ?>" target="_blank"><img src="<?php echo base_url().getThemeName(); ?>/images/home.png" alt="" class="img"  /></a>
						<?php } if($user_info->digg_link!='') { ?>
						 
						<a href="<?php echo $user_info->digg_link; ?>" target="_blank"><img src="<?php echo base_url().getThemeName(); ?>/images/digg.png" alt="" class="img"  /></a>	
						 
						<?php } if($user_info->stumblupon_link!='') { ?>
						
						<a href="<?php echo $user_info->stumblupon_link; ?>" target="_blank"><img src="<?php echo base_url().getThemeName(); ?>/images/su.png" alt="" class="img"  /></a>
						<?php } ?>

					</span><br />
					
					<!--<label>Search keywords &nbsp; :</label>
					<span style="margin:0; padding:0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#585858; font-weight:normal; ">&nbsp; &nbsp;Dubai Trip, Bangkok Trip.</span>-->
					
				</div>
				
			<div class="clear"></div>
			</div>
			
			
			<div class="recent1">
				<div class="recent_rew1">
					<?php echo anchor('user/'.$profile_name,'Recent Activity');?>
				</div>
				<div class="recen_view2">
					<?php echo anchor('user/'.$profile_name.'/reviews','Reviews');?>
				</div>
			</div>
			
				<?php 
					if($result) {
						
						foreach($result as $review){
						
							////////======= user image 
							$user_image= base_url().'upload/no_image.png';
							
							 if($review->profile_image!='') {  
								if(file_exists(base_path().'upload/user_thumb/'.$review->profile_image)) {
									$user_image=base_url().'upload/user_orig/'.$review->profile_image;	
								}					
							}
							
				?>
					<div class="new_comment1">
					
						<div class="new_comment_left1">
							<div class="woman_photo1">
								<?php echo anchor('user/'.$review->profile_name,'<img src="'.$user_image.'" alt="" class="img" height="50" width="50"/>');?>
							</div>
						</div>
						
						<div class="new_comment_right1">
						
							<div class="comment1">
								<h4>Review for &nbsp;</h4> 
								<h2><?php echo anchor('from/'.$review->trip_from_place.'/to/'.$review->trip_to_place.'/'.$review->trip_id,'Trip: '.$review->trip_from_place.' &gt; '.$review->trip_to_place,'class="homepick"');?></h2>
							</div>
							
							<div class="comment2">
								<?php
									$total_rate=get_user_total_rate($user_info->user_id);
									$total_review=get_user_total_review($user_info->user_id);
								?>   
						
								<div class="strmn"><div class="str_sel" style="width:<?php if($review->user_rating > 5) { ?>100<?php } else { echo round($review->user_rating*2);?>0<?php } ?>%;"></div></div>
					
								<p class="fl" style="width: 540px; margin-left:20px;"><?php echo $review->user_review ?></p>
								
								<div class="clear"></div>
								
								<span>about <?php echo getDuration($review->user_review_date);?></span>
								
							</div>
							
						</div>
						
					</div>
				<?php
						}
					} else { echo '<div class="new_comment1"><center><b>No Record Found</b></center></div>'; }
				?>
			
				<?php if($total_rows > $limit) { ?>
					<div class="gonext">
                    <?php echo $page_link; ?>
                    </div>
				<?php } ?>
			
			
		 </div>
		
		 <?php  echo $this->load->view($theme.'/layout/user/profile_sidebar'); ?>
		
		
	</div>
	<div class="clear"></div>
</div><!--end_#bidcontener-->
<div class="clear"></div>