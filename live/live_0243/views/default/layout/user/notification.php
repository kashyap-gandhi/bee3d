		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Notification</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
						<!-- main content -->
						<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Notification</h4>
									</div>
									<?php 
									if($result)
									{?>
										<div class="panel-body">
										<?php 
										foreach($result as $usn)
										{
											
											
										if($usn->challenge_id > 0)
										{
											$challenge_data = $this->challenge_model->get_one_challenge($usn->challenge_id);
										}
										if($usn->design_id > 0)
										{
											$design_data = $this->design_model->get_one_design($usn->design_id);
										}
										if($usn->video_id > 0)
										{
											$video_data = $this->video_model->get_one_video($usn->video_id);
										}
										?>
											
											<?php if($usn->design_id > 0)
											{
											
												if($usn->act == 'DESIGN_DOWNLOAD' )
												{
													if($design_data)
													{?>
												<div class="alert alert-success">You have downloaded <a href="<?php echo site_url('design/'.$design_data->design_slug);?>" class="alert-link"><?php echo $design_data->design_title;?></a>, <span class="span_right"><?php echo date($site_setting->date_format,strtotime($usn->notification_date));?></span></div>
												<?php
													}
												}
											}
											
											if($usn->design_id > 0)
											{
												if($usn->act == 'DESIGN_LIKE')
												{
												if($design_data)
													{
												?>
												<div class="alert alert-warning">You have liked design <a href="<?php echo site_url('design/'.$design_data->design_slug);?>" class="alert-link"><?php echo $design_data->design_title;?></a>, <span class="span_right"><?php echo date($site_setting->date_format,strtotime($usn->notification_date));?></span> </div>
												<?php } }
											}
											
											if($usn->design_id > 0)
											{
												if($usn->act == 'DESIGN_BUY')
												{ if($design_data)
													{?>
												<div class="alert alert-warning">You have buy design <a href="<?php echo site_url('design/'.$design_data->design_slug);?>" class="alert-link"><?php echo $design_data->design_title;?></a>,<span class="span_right"><?php echo date($site_setting->date_format,strtotime($usn->notification_date));?></span> </div>
												<?php } }
											}
											
											
											if($usn->video_id > 0)
											{
												if($usn->act == 'VIDEO_DOWNLOAD')
												{
												if($video_data)
													{
												?>
												<div class="alert alert-info">You have downloaded video <a href="<?php echo site_url('video/'.$video_data->video_slug);?>" class="alert-link"><?php echo $video_data->video_title;?></a> <span class="span_right"><?php echo date($site_setting->date_format,strtotime($usn->notification_date));?></span></div>
												<?php 
												}
											}
											}
											
											if($usn->video_id > 0)
											{
												if($usn->act == 'VIDEO_BUY')
												{
												if($video_data)
													{?>
												<div class="alert alert-warning">You have buy video <a href="<?php echo site_url('video/'.$video_data->video_slug);?>" class="alert-link"><?php echo $video_data->video_title;?></a>, <span class="span_right"><?php echo date($site_setting->date_format,strtotime($usn->notification_date));?></span></div>
												<?php } }
											}
											
											if($usn->video_id > 0)
											{
												if($usn->act == 'VIDEO_LIKE')
												{
												if($video_data)
													{
												?>
												<div class="alert alert-info">You have liked video <a href="<?php echo site_url('video/'.$video_data->video_slug);?>" class="alert-link"><?php echo $video_data->video_title;?></a>, <span class="span_right"><?php echo date($site_setting->date_format,strtotime($usn->notification_date));?></span></div>
												<?php 
												} }
											}
											
											
											if($usn->challenge_id > 0)
											{
												$challenge_data = $this->challenge_model->get_one_challenge($usn->challenge_id);
												if($usn->act == 'CHALLENGE_POST')
												{
												
												if($challenge_data) { ?>
												<div class="alert alert-warning">You have created challenge <a href="<?php echo site_url('challenge/'.$challenge_data->challenge_slug);?>" class="alert-link"><?php echo $challenge_data->challenge_title;?></a>, <span class="span_right"><?php echo date($site_setting->date_format,strtotime($usn->notification_date));?></span> </div>
												<?php }  }
											}
											
											if($usn->challenge_id > 0)
											{
												if($usn->act == 'CHALLENGE_BID_POST')
												{  
												if($challenge_data) {?>
												<div class="alert alert-warning">You have bid on challenge <a href="<?php echo site_url('challenge/'.$challenge_data->challenge_slug);?>" class="alert-link"><?php echo $challenge_data->challenge_title;?></a>, <span class="span_right"><?php echo date($site_setting->date_format,strtotime($usn->notification_date));?></span> </div>
												<?php } } 
											}
											
											if($usn->challenge_id > 0)
											{
												if($usn->act == 'BID_ON_YOUR_CHALLENGE')
												{
												if($challenge_data) {?>
												<div class="alert alert-warning">You get new bid on challenge<a href="<?php echo site_url('challenge/'.$challenge_data->challenge_slug);?>" class="alert-link"><?php echo $challenge_data->challenge_title;?></a>, <span class="span_right"><?php echo date($site_setting->date_format,strtotime($usn->notification_date));?></span> </div>
												<?php }
												}
											}
											
											if($usn->challenge_id > 0)
											{
												if($usn->act == 'BID_ON_CHALLENGE')
												{
												
												if($challenge_data) {?>
												<div class="alert alert-warning">You have bid on challenge <a href="<?php echo site_url('challenge/'.$challenge_data->challenge_slug);?>" class="alert-link"><?php echo $challenge_data->challenge_title;?></a>, <span class="span_right"><?php echo date($site_setting->date_format,strtotime($usn->notification_date));?></span> </div>
												<?php }
												}
											}
											
											if($usn->challenge_id > 0)
											{
												if($usn->act == 'CHALLENGE_COMPLETE_BY_OWNER')
												{
												if($challenge_data) {?>
												<div class="alert alert-warning">Challenge has been completed by challenge Owner <a href="<?php echo site_url('challenge/'.$challenge_data->challenge_slug);?>" class="alert-link"><?php echo $challenge_data->challenge_title;?></a>, <span class="span_right"><?php echo date($site_setting->date_format,strtotime($usn->notification_date));?></span> </div>
												<?php }
												}
											}
											
											if($usn->challenge_id > 0)
											{
												if($usn->act == 'CHALLENGE_COMPLETE_BY_YOU')
												{ 
												if($challenge_data) {?>
												<div class="alert alert-warning">You have completed <a href="<?php echo site_url('challenge/'.$challenge_data->challenge_slug);?>" class="alert-link"><?php echo $challenge_data->challenge_title;?></a>, <span class="span_right"><?php echo date($site_setting->date_format,strtotime($usn->notification_date));?></span> </div>
												<?php } }
											}
											
											if($usn->challenge_id > 0)
											{
												if($usn->act == 'CHALLENGE_COMPLETE_BY_BIDDER')
												{
												if($challenge_data) {?>
												<div class="alert alert-warning">Challenge completed by bidder <a href="<?php echo site_url('challenge/'.$challenge_data->challenge_slug);?>" class="alert-link"><?php echo $challenge_data->challenge_title;?></a>, <span class="span_right"><?php echo date($site_setting->date_format,strtotime($usn->notification_date));?></span>  </div>
												<?php } }
											}
											
											if($usn->challenge_id > 0)
											{
												if($usn->act == 'CHALLENGE_ACCEPT_BY_YOU')
												{
												if($challenge_data) {?>
												<div class="alert alert-warning">You have accept bid on <a href="<?php echo site_url('challenge/'.$challenge_data->challenge_slug);?>" class="alert-link"><?php echo $challenge_data->challenge_title;?></a>, <span class="span_right"><?php echo date($site_setting->date_format,strtotime($usn->notification_date));?></span>  </div>
												<?php } }
											}
											
											if($usn->challenge_id > 0)
											{
												if($usn->act == 'YOUR_BID_ACCEPT')
												{
												
												if($challenge_data) {?>
												<div class="alert alert-warning">Your bid has been accepted. <a href="<?php echo site_url('challenge/'.$challenge_data->challenge_slug);?>" class="alert-link"><?php echo $challenge_data->challenge_title;?></a>, <span class="span_right"><?php echo date($site_setting->date_format,strtotime($usn->notification_date));?></span>  </div>
												<?php } }
											}
											
											
											
											?>
									
										<?php }?>
										</div>
									<?php 
									}
									else
									{?>
									<div>
									No records found
									</div>
									<?php }
									?>
								
								<div class="pagination">
								<?php echo $page_link;?>
								</div>	
								</div>
								
							</div>
					</div>
				</div>
				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
				
			</section>
			<div id="footer_space"></div>
		</div>

       