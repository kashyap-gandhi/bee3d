
<?php 
	$sel_user_profile = '';
	$sel_edit_account = '';
	$sel_agent_profile = '';
	$sel_password='';
	$sel_notifications='';
	$sel_mytrip = '';
	$sel_myoffer = '';
	$sel_creditcard = '';
	$sel_transaction = '';
	$sel_mydeal='';
	
	if(isset($select))
	{
		if($select=='user_profile') {  $sel_user_profile='class="active"';  } else { $sel_user_profile=''; } 
		if($select=='edit_account') {  $sel_edit_account='class="active"';  } else { $sel_edit_account=''; } 
		if($select=='agent_profile') {  $sel_agent_profile='class="active"';  } else { $sel_agent_profile=''; } 
		if($select=='password') {  $sel_password='class="active"';  } else { $sel_password=''; } 
		if($select=='notifications') {  $sel_notifications='class="active"';  } else { $sel_notifications=''; } 
		if($select=='mytrip') {  $sel_mytrip='class="active"';  } else { $sel_mytrip=''; } 
		if($select=='myoffer') {  $sel_myoffer='class="active"';  } else { $sel_myoffer=''; } 
		if($select=='creditcard') {  $sel_creditcard='class="active"';  } else { $sel_creditcard=''; } 
		if($select=='transaction_history') {  $sel_transaction='class="active"';  } else { $sel_transaction=''; } 
		if($select=='mydeal') {  $sel_mydeal='class="active"';  } else { $sel_mydeal=''; } 
		if($select=='user_notification') {  $sel_notification='class="active"';  } else { $sel_notification=''; } 
		
	}
?>


<div class="right_side">

	<?php 
		$agent_profile = get_user_agent_detail(get_authenticateUserID());
		
		if($agent_profile) {
			$verify_msg = '';
			
			$user_card_info = get_user_card_info(get_authenticateUserID());
			$agent_document = get_agent_document(get_authenticateUserID());
			
			if($user_card_info) {
				if($user_card_info->card_verify_status == 0){
					$verify_msg .= '<li>'.anchor('my_card','Please Verify Credit Card Details','style="color:red;font-size: 14px;font-weight: bold;"').'</li>';
				}
			} else {
				$verify_msg .= '<li>'.anchor('my_card','Please Verify Credit Card Details','style="color:red;font-size: 14px;font-weight: bold;"').'</li>';
			}
	
			if(!$agent_document){
				$verify_msg .= '<li>'.anchor('travelagent/profile#document','Please Add your Background Proof','style="color:red;font-size: 14px;font-weight: bold;"').'</li>';	
			}
			
			if($verify_msg != '') {
				//echo '<ul style="margin-left: 5px;">'.$verify_msg.'</ul><br />';
			}
		}
	?>
	
    	<div class="deal_btn_dash fl"><?php echo anchor('deal/adddeal','Add deal')?></div>
    <div class="clear"></div><br />


	<div class="estim marB5">Profile</div>
	<ul class="accr">
		<li><?php echo anchor('user/'.getUserProfileName(),'See Profile',$sel_user_profile)?></li>
    </ul><div class="clear"></div>
	
	
	<div class="estim marB5 martop20">Your Account</div>
	<ul class="accr">
		<li><?php echo anchor('user/edit','My Account',$sel_edit_account)?></li>
		
		
		
		<li><?php echo anchor('user/change_password','Change password',$sel_password)?></li>
		<li><?php echo anchor('notifications','Notifications',$sel_notification)?></li>
		<?php /*?><li><?php echo anchor('mytrip/all','MyTrips',$sel_mytrip)?></li><?php */?>
       
		
        
        
        
        <?php if($agent_profile) { ?>
			<li><?php echo anchor('travelagent/profile','Agent Profile',$sel_agent_profile)?></li>
           <?php /*?> <li><?php echo anchor('myoffer/all','MyOffers',$sel_myoffer)?></li>
            <li><?php echo anchor('my_card','My Credit Card',$sel_creditcard)?></li>
            <li><?php echo anchor('transaction','Transaction History',$sel_transaction)?></li><?php */?>
             <li><?php echo anchor('deal/mydeal','My Deal',$sel_mydeal)?></li>
           
		<?php } else { ?>
			<li><?php echo anchor('travelagent/became','Became a Travel Agent',$sel_agent_profile)?></li>
		<?php } ?>
		
		
  	</ul><div class="clear"></div>
	
<div class="clear"></div>
</div>
                    