<script type="text/javascript" src="<?php echo base_url().getThemeName(); ?>/js/jquery.wysiwyg.js"></script> 
  
<style>
.label_radio {
    cursor: pointer;
    display: inline-block;
    line-height: 20px;
	
    padding-bottom: 9px;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.2);
}
.has-js .label_check,
.has-js .label_radio { padding-left: 34px; float:left; }
.has-js .label_radio { background: url(<?php echo base_url().getThemename(); ?>/images/radio-off.png) no-repeat; height:21px; padding:12px;}
.has-js label.r_on { background: url(<?php echo base_url().getThemename(); ?>/images/radio-on.png) no-repeat; height:21px;}
.has-js .label_check input,
.has-js .label_radio input { position: absolute; left: -9999px; }
.label_new_radio p
{
	float:left;
}
</style>
<script>
function bindeditor()
{



$('textarea.wysiwyg').wysiwyg({
  				  controls: {
    				 strikeThrough : { visible : true },
    				 underline     : { visible : true },
      
   					 separator00 : { visible : false },
      
     				 justifyLeft   : { visible : true },
      				 justifyCenter : { visible : true },
     				 justifyRight  : { visible : true },
     				 justifyFull   : { visible : true },
      
 				     separator01 : { visible : false },
      
   				     indent  : { visible : false },
				    outdent : { visible : false },
      
     				 separator02 : { visible : false },
      
    				  subscript   : { visible : false },
    				  superscript : { visible : false },
      
   					   separator03 : { visible : false },
  	    
  					    undo : { visible : false },
   					   redo : { visible : false },
      
    				  separator04 : { visible : false },
      
     				 insertOrderedList    : { visible : false },
    				  insertUnorderedList  : { visible : false },
    				  insertHorizontalRule : { visible : false },
      
    				  h1mozilla : { visible : false && $.browser.mozilla, className : 'h1', command : 'heading', arguments : ['h1'], tags : ['h1'], tooltip : "Header 1" },
     				 h2mozilla : { visible : false && $.browser.mozilla, className : 'h2', command : 'heading', arguments : ['h2'], tags : ['h2'], tooltip : "Header 2" },
     				 h3mozilla : { visible : false && $.browser.mozilla, className : 'h3', command : 'heading', arguments : ['h3'], tags : ['h3'], tooltip : "Header 3" },
      
					
					 h1 : { visible : false && !( $.browser.mozilla ), className : 'h1', command : 'formatBlock', arguments : ['<H1>'], tags : ['h1'], tooltip : "Header 1" },
     				 h2 : { visible : false && !( $.browser.mozilla ), className : 'h2', command : 'formatBlock', arguments : ['<H2>'], tags : ['h2'], tooltip : "Header 2" },
   					   h3 : { visible : false && !( $.browser.mozilla ), className : 'h3', command : 'formatBlock', arguments : ['<H3>'], tags : ['h3'], tooltip : "Header 3" },
					 
					 
					 
					 
					 
					  h4mozilla : { visible : false && $.browser.mozilla, className : 'h4', command : 'heading', arguments : ['h4'], tags : ['h4'], tooltip : "Header 4" },
     				 h5mozilla : { visible : false && $.browser.mozilla, className : 'h5', command : 'heading', arguments : ['h5'], tags : ['h5'], tooltip : "Header 5" },
     				 h6mozilla : { visible : false && $.browser.mozilla, className : 'h6', command : 'heading', arguments : ['h6'], tags : ['h6'], tooltip : "Header 6" },
      
					
					 h4 : { visible : false && !( $.browser.mozilla ), className : 'h4', command : 'formatBlock', arguments : ['<H4>'], tags : ['h4'], tooltip : "Header 4" },
     				 h5 : { visible : false && !( $.browser.mozilla ), className : 'h5', command : 'formatBlock', arguments : ['<H5>'], tags : ['h5'], tooltip : "Header 5" },
   					   h6 : { visible : false && !( $.browser.mozilla ), className : 'h6', command : 'formatBlock', arguments : ['<H6>'], tags : ['h6'], tooltip : "Header 6" },
      
   					   separator07 : { visible : false },
      
     				 cut   : { visible : false },
     				 copy  : { visible : false },
     				 paste : { visible : false },
					 insertImage : {visible : false},
					 removeFormat : {visible : false},
					 createLink : {visible : false},
					  separator05 : { visible : false },
					   separator06 : { visible : false },
					   separator08 : { visible : false },
					  separator09 : { visible : false }
   					 }
  				});


			
 }  
 
       

</script>

<script type="text/javascript">
var d = document;
var safari = (navigator.userAgent.toLowerCase().indexOf('safari') != -1) ? true : false;
var gebtn = function(parEl,child) { return parEl.getElementsByTagName(child); };
onload = function() {
    
    var body = gebtn(d,'body')[0];
    body.className = body.className && body.className != '' ? body.className + ' has-js' : 'has-js';
    
    if (!d.getElementById || !d.createTextNode) return;
    var ls = gebtn(d,'label');
		
	
    for (var i = 0; i < ls.length; i++) {
        var l = ls[i];
        if (l.className.indexOf('label_') == -1) continue;
        var inp = gebtn(l,'input')[0];
      
		
		if (l.className == 'label_radio') {
            l.className = (safari && inp.checked == true || inp.checked) ? 'label_radio r_on' : 'label_radio r_off';
            l.onclick = turn_radio;
        };
    };
};



var turn_radio = function() {
	
    var inp = gebtn(this,'input')[0];
    if (this.className == 'label_radio r_off' || inp.checked) {
		var ls = gebtn(this.parentNode,'label');
		
		if(this.id == 'air_radio_quote')
		{
			load_air_trip();
		}
		if(this.id == 'trip_radio_quote')
		{
			load_trip_quote();
		}
		for (var i = 0; i < ls.length; i++) {
            var l = ls[i];
            if (l.className.indexOf('label_radio') == -1)  continue;
            l.className = 'label_radio r_off';
        };
        this.className = 'label_radio r_on';
        if (safari) inp.click();
    } else {
        this.className = 'label_radio r_off';
        if (safari) inp.click();
    };
};


 function load_air_trip()
 {
	 	
	
		$('#lnk_quote').fadeOut('slow');
		$('#quote_radio').fadeIn('slow');
		
			var dealid = document.getElementById('tem_deal_id').value; 
	
			var pname = document.getElementById('profile_name').value ;
			var strURL='<?php echo site_url('user/load_air_trip/');?>'+'/'+pname+'/'+dealid;
			var xmlhttp;
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
				document.getElementById('preview').style.display = 'block';
				document.getElementById('egg_quote').style.display = 'none';
				
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			
			}
			xmlhttp.onreadystatechange=function()
				{
				
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById('egg_quote').style.display = 'block';
					document.getElementById("egg_quote").innerHTML =xmlhttp.responseText;
					document.getElementById('preview').style.display = 'none';
					bindeditor();
					bind_air_loc();
					
					
				}
			}
			xmlhttp.open("GET",strURL,true);
			xmlhttp.send();
		
	 }

function load_trip_quote()
{
	
	 	
	
		$('#lnk_quote').fadeOut('slow');
		$('#quote_radio').fadeIn('slow');
			var dealid = document.getElementById('tem_deal_id').value; 
			var pname = document.getElementById('profile_name').value ;
			var strURL='<?php echo site_url('user/load_trip_quote/');?>'+'/'+pname+'/'+dealid;
			var xmlhttp;
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
				document.getElementById('preview').style.display = 'block';
				document.getElementById('egg_quote').style.display = 'none';
			
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			
			}
			xmlhttp.onreadystatechange=function()
				{
				
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById('egg_quote').style.display = 'block';
					document.getElementById("egg_quote").innerHTML =xmlhttp.responseText;
					document.getElementById('preview').style.display = 'none';
					bindeditor();
					bind_trip_loc();
					
					
				}
			}
			xmlhttp.open("GET",strURL,true);
			xmlhttp.send();
		
	 
}

function submit_air_quote()
{

	var con = 1;
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	
	if(document.getElementById('airline_from').value == '')
	{
		document.getElementById('err_air_from').style.display = 'block';
		con = 0;
	}
	if(document.getElementById('airline_from').value != '')
	{
		document.getElementById('err_air_from').style.display = 'none';
		
	}
	if(document.getElementById('airline_to').value == '')
	{
		document.getElementById('err_air_to').style.display = 'block';
		con = 0;
	
	}
	if(document.getElementById('airline_to').value != '')
	{
		document.getElementById('err_air_to').style.display = 'none';
		
	}
	if(document.getElementById('airline_message').value == '')
	{
		document.getElementById('err_air_message').style.display = 'block';
		con = 0;
	}
	if(document.getElementById('airline_message').value != '')
	{
		document.getElementById('err_air_message').style.display = 'none';
		
	}
	if(document.getElementById('airline_quote_email').value == '')
	{
		document.getElementById('err_air_email').style.display = 'block';
		con = 0;
	}
	if(document.getElementById('airline_quote_email').value != '')
	{
		document.getElementById('err_air_email').style.display = 'none';
		
	}
	if(reg.test(document.getElementById('airline_quote_email').value) == false && document.getElementById('airline_quote_email').value != '')
	{
		document.getElementById('err_air_email_invalid').style.display='block';
		con = 0;
	}
	
		if(con == 0)
		{
		 	return false;
		}
		return true;

}

function submit_trip_quote_valid()
{
	

	var con = 1;
	var reg_trip = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	
	if(document.getElementById('trip_from').value == '')
	{
		document.getElementById('err_trip_from').style.display = 'block';
		con = 0;
	}
	if(document.getElementById('trip_from').value != '')
	{
		document.getElementById('err_trip_from').style.display = 'none';
		
	}
	if(document.getElementById('trip_to').value == '')
	{
		document.getElementById('err_trip_to').style.display = 'block';
		con = 0;
	
	}
	if(document.getElementById('trip_to').value != '')
	{
		document.getElementById('err_trip_to').style.display = 'none';
		
	}
	if(document.getElementById('total_day').value == '')
	{
		document.getElementById('err_trip_day').style.display = 'block';
		con = 0;
	
	}
	if(document.getElementById('total_day').value != '')
	{
		document.getElementById('err_trip_day').style.display = 'none';
		
	}
	 
	if(document.getElementById('trip_quote_msg').value == '')
	{
		document.getElementById('err_trip_quote_msg').style.display = 'block';
		con = 0;
	}
	if(document.getElementById('trip_quote_msg').value != '')
	{
		document.getElementById('err_trip_quote_msg').style.display = 'none';
		
	}
	if(document.getElementById('trip_email').value == '')
	{
		document.getElementById('err_trip_email').style.display = 'block';
		con = 0;
	}
	if(document.getElementById('trip_email').value != '')
	{
		document.getElementById('err_trip_email').style.display = 'none';
		
	}
	if(reg_trip.test(document.getElementById('trip_email').value) == false && document.getElementById('trip_email').value != '')
	{
		document.getElementById('err_trip_email_invalid').style.display='block';
		con = 0;
	}
	
	if(con == 0)
	{
		return false;
	}
	return true;

}


</script>



<div id='preview' style="display:none;"><img src="<?php echo base_url().$theme;?>/images/process.gif" class="loader" alt="Uploading...." width="48" height="48" style="margin-top:23px; margin-left:130px;" /></div>
<input type="hidden" id="profile_name" value="<?php echo $profile_name;?>"/>
<div class="right_side">
		
			
				
                
     <?php if(get_authenticateUserID()>0){ 
			  
			  	if($user_info->user_id == get_authenticateUserID() || ($this->session->userdata('email_invite_user_id') == $user_info->user_id && $this->session->userdata('email_verification_code')!='') ) { ?>
               
			<div class="ask_me_orange" id="lnk_quote" style="display:none;">
					<a href="<?php echo site_url('user/edit');?>">Edit Profile</a>
			</div>
            <div class="clear"></div>
           
            <?php }else{ ?>
            
            <div class="ask_me" id="lnk_quote" style="display:block;">
					<a href="javascript://" id="request_quote"  onclick="load_air_trip();">Request Quote</a>
				</div>
                <div class="clear"></div>
           
            <?php }}else{ 
			
					if($this->session->userdata('email_invite_user_id') == $user_info->user_id && $this->session->userdata('email_verification_code')== $user_info->email_verification_code  ) { ?>
            
            <div class="ask_me_orange" id="lnk_quote">
					<a href="<?php echo site_url('home/editagent');?>">Edit Profile</a>
			</div>
            <div class="clear"></div>
           
           <?php } else { ?>
            <div class="ask_me" id="lnk_quote">
					<a href="javascript://" id="request_quote" onclick="load_air_trip();">Request Quote</a>
				</div>
                <div class="clear"></div>
                
            <?php } }  ?>
            
			<div class="top_trip_types" style="float:left; margin-top:10px;display:none;" id="quote_radio">
            	<label class="label_radio" id="air_radio_quote" for="radio-01"><input name="sample-radio" id="signupowner" value="1" type="radio" checked /> </label><p class="label_new_radio" style="float:left; padding:5px; line-height:10px;">Air Line</p>
				 <label class="label_radio" id="trip_radio_quote" for="radio-02"><input name="sample-radio" id="signupsitter" value="1" type="radio"/> </label><p class="label_new_radio" style="float:left; padding:5px; line-height:10px;">Request Quote</p>
            </div>
            
          	<!--Egg Quote-->
            <div id="egg_quote"></div>
          <div class="clear"></div>
          <!--Quotes-->
          	
			<!--<div class="top_trip_types">
				<h2>Tips</h2>
				<p>David did a great job. PIcked up my
					items, packaged and shipped, and 
					sent electronic.<br />
					<strong style="color:#b2b2b2; margin:0; padding:0; font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal;">2 days ago</strong></p>
			</div>-->
			<div class="clear"></div>
		
			<!--Top Trip Types listing start-->
			<?php if($agent_info) { ?>
			
				<?php /*?><div class="top_trip_types">
					<h2>Top Trip Types</h2>
					
					 <?php 
					 
					 $top_trip = top_trip_user($agent_info->user_id);
					 $top_agent_trip='';
					 
					 $i=0;
					 if($top_trip)
					 {
						foreach($top_trip as $trp)
						{
							$i++;
							if($trp->package_type == 1)
							{
								$rate1 = get_user_total_trip_rate($user_info->user_id,1);
								$top_agent_trip.='<strong>Family</strong><span>&nbsp;&nbsp;'.$trp->total_trip.' trips done ('.$rate1.'/5 stars)</span><br />';
							}
							else if($trp->package_type == 2)
							{
								$rate2 = get_user_total_trip_rate($user_info->user_id,2);
								$top_agent_trip.='<strong>Group</strong><span>&nbsp;&nbsp;'.$trp->total_trip.' trips done ('.$rate2.'/5 stars)</span><br />';
							}
							else if($trp->package_type == 3)
							{
								$rate3 = get_user_total_trip_rate($user_info->user_id,3);
								$top_agent_trip.='<strong>Honeymoon</strong><span>&nbsp;&nbsp;'.$trp->total_trip.' trips done ('.$rate3.'/5 stars)</span><br />';
							}
							else if($trp->package_type == 4)
							{
								$rate4 = get_user_total_trip_rate($user_info->user_id,4);
								$top_agent_trip.='<strong>Weekend</strong><span>&nbsp;&nbsp;'.$trp->total_trip.' trips done ('.$rate4.'/5 stars)</span><br />';
							}
							else
							{
								$rate = get_user_total_trip_rate($user_info->user_id,0);
								$top_agent_trip.='<strong>All</strong><span>&nbsp;&nbsp;'.$trp->total_trip.' trips done ('.$rate.'/5 stars)</span><br />';
							}
	
						}
					 }
					 
					if($top_agent_trip!='') { echo $top_agent_trip; } ?>
	
				</div><?php */?>
			
			<?php } ?>
			<!--Top Trip Types listing end-->
			
			<!--agent approved icon listing start-->
<script>
$(document).ready(function(){	 
	$('.verify').tooltip({placement:'bottom'});
});  
</script>
			<div class="stars" style="padding-left:5px; padding-top:10px;">
			
				<?php 
					if($agent_info) { 
						if($agent_info->agent_app_approved ==1) { 
				?>
						<img class="img verify" data-title="Application Verified" alt="" src="<?php echo base_url().getThemeName(); ?>/images/man_at_work.png">
						
				<?php } if($agent_info->agent_background_approved ==1) { ?>
				
						<img class="img verify" data-title="Background Verified" alt="" src="<?php echo base_url().getThemeName(); ?>/images/maskret.png">
						
				<?php } if($agent_info->agent_phone_approved ==1) { ?>
				
						<img class="img verify" data-title="Phone Verified" alt="" src="<?php echo base_url().getThemeName(); ?>/images/atphone.png">
						
				<?php }  } ?>
			
				<img class="img verify" data-title="IP Verified" alt="" src="<?php echo base_url().getThemeName(); ?>/images/ip.png">
				
			</div>
			<!--agent approved icon listing end-->
			
			
			<!--facebook share link start-->
			<div class="profike verify" data-title="Share on Facebook">
				<img src="<?php echo base_url().getThemeName(); ?>/images/facebook_icons.png" alt="" class="img" />

				<a href="javascript:void()" onClick="window.open('http://www.facebook.com/share.php?u=<?php echo site_url('user/'.$user_info->profile_name);?>&amp;t=<?php echo ucfirst($user_info->first_name).' '.ucfirst($user_info->last_name); ?>','Share on Facebook','height=300,width=600,top=50,left=300');" > Share this profike on Facebook</a>
				
			</div>
			<!--facebook share link end-->
			
			
		<?php /*?>	<div class="top_trip_types martop20 marB5">
				<h2><?php echo ucfirst($user_info->first_name).'’s Trip Rating';?></h2>
			</div><?php */?>
			

			<?php /*?><div class="stars" style="padding-left:5px;">
				<?php
					$total_rate=get_user_total_rate($user_info->user_id);
					$total_review=get_user_total_review($user_info->user_id);
				?>
				<div class="strmn"><div class="str_sel" style="width:<?php if($total_rate>5) { ?>100<?php } else { echo round($total_rate*2);?>0<?php } ?>%;"></div></div>
				<span style="color:#878787; margin-left:5px; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;"><?php echo $total_rate; ?>/5 <strong style="color:#0979b2; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;">(<?php echo anchor('user/'.$user_info->profile_name.'/reviews',$total_review.' reviews'); ?>)</strong></span>
				
			</div><?php */?>
			
			<?php 
				/*?>if($reviews) {
					foreach($reviews as $review){
						////////======= user image 
						$user_image= base_url().'upload/no_image.png';
						
						 if($review->profile_image!='') {  
							if(file_exists(base_path().'upload/user_thumb/'.$review->profile_image)) {
								$user_image=base_url().'upload/user_orig/'.$review->profile_image;	
							}					
						}
			?>
				<div class="main_review">
				
					<?php echo anchor('user/'.$review->profile_name,'<img src="'.$user_image.'" alt="" class="img"/>');?>
					
					<p>Review for <span><?php echo anchor('from/'.$review->trip_from_place.'/to/'.$review->trip_to_place.'/'.$review->trip_id,'Trip: '.$review->trip_from_place.' &gt; '.$review->trip_to_place,'class="homepick"');?></span></p>
					
					<p><?php echo $review->user_review ?></p>
					<p><?php echo getDuration($review->user_review_date);?></p>
				</div>
			
			<?php 
					}
				} else {
					echo '<div class="main_review"><p>No Record Found</p></div>';
				}<?php */
			?>

			
		<div class="clear"></div>
		</div>
		<div class="clear"></div>
        
        <script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>  
		<!-- <link href="/maps/documentation/javascript/examples/default.css" rel="stylesheet">-->
	    <script> 
	
		function bind_air_loc()
		{
		var options = {		
		  types: ['(cities)']
		 // componentRestrictions: {country: 'us'}
		};
		
	   var departure_from = new google.maps.places.Autocomplete($("#airline_from")[0], options);
        google.maps.event.addListener(airline_from, 'place_changed', function() {
		 	var place_start = airline_from.getPlace();
			
			 console.log(place_start.address_components);
            });
			
		  var want_go_to = new google.maps.places.Autocomplete($("#airline_to")[0], options);
        google.maps.event.addListener(airline_to, 'place_changed', function() {
                var place_end = airline_to.getPlace();
                console.log(place_end.address_components);
            });	
		
		}
		
		function bind_trip_loc()
		{
				var options = {		
		  types: ['(cities)']
		 // componentRestrictions: {country: 'us'}
		};
		
		 var trip_from = new google.maps.places.Autocomplete($("#trip_from")[0], options);
        google.maps.event.addListener(trip_from, 'place_changed', function() {
                var place_from = trip_from.getPlace();
                console.log(place_from.address_components);
            });		
			
		 var trip_to = new google.maps.places.Autocomplete($("#trip_to")[0], options);
        google.maps.event.addListener(trip_to, 'place_changed', function() {
                var place_to = trip_to.getPlace();
                console.log(place_to.address_components);
            });		
		}
</script>   