		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Dashboard</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
						
						<!-- main content -->
						
						<h2>Profile</h2>
						<?php if($msg!='') { ?>
								<div class="alert alert-success">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<?php  echo "User profile updated successfully."; ?>
									
								</div>
						 <?php  } ?>
						<div class="row">
							<div class="col-sm-12">
								<div class="user_heading">
									<div class="row">
										<div class="col-sm-2 hidden-xs">
										<?php
										 $user_image= base_url().'upload/no_image.png';
										 if($user_info->profile_image!='') {  
											if(file_exists(base_path().'upload/user/'.$user_info->profile_image)) {
												$user_image=base_url().'upload/user/'.$user_info->profile_image;	
											}					
										}
										?>
											<img src="<?php echo $user_image;?>" alt="" class="img-thumbnail user_avatar">
										</div>
										<div class="col-sm-10">
											<div class="user_heading_info">
												<div class="user_actions pull-right">
													<a href="<?php echo site_url('user/edit');?>" class="edit_form" data-toggle="tooltip" data-placement="top auto" title="Edit profile"><span class="icon-edit"></span></a>
												</div>
												<h1><?php echo ucfirst($user_info->first_name);?> <?php echo ucfirst($user_info->last_name);?></h1>

											</div>
										</div>
									</div>
								</div>
								<div class="user_content">
									<div class="row">
										<div class="col-sm-10 col-sm-offset-2">
											<form class="form-horizontal user_form">
												<h3 class="heading_a">General</h3>
												
												<div class="form-group">
													<label class="col-sm-2 control-label">Name</label>
													<div class="col-sm-10 editable">
														<p class="form-control-static"><?php echo ucfirst($user_info->first_name);?> <?php echo ucfirst($user_info->last_name);?></p>
													
													</div>
												</div>
												
												<h3 class="heading_a">Contact info</h3>
												<div class="form-group">
													<label class="col-sm-2 control-label">Email</label>
													<div class="col-sm-10 editable">
														<p class="form-control-static"><?php echo $user_info->email;?></p>
														
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label">Phone</label>
													<div class="col-sm-10 editable">
														<p class="form-control-static"><?php if($user_info->phone_no != '' && $user_info->phone_no > 0){echo ucfirst($user_info->phone_no);}else{echo 'N/A';}?></p>
														
													</div>
												</div>
												
												<h3 class="heading_a">Other info</h3>
												<div class="form-group">
													<label class="col-sm-2 control-label">Facebook Link</label>
													<div class="col-sm-10 editable">
														<p class="form-control-static"><?php if($user_info->twitter_link  != '' ){echo $user_info->facebook_link;}else{echo 'N/A';}?></p>
														
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label">Twitter Link</label>
													<div class="col-sm-10 editable">
														<p class="form-control-static"><?php if($user_info->twitter_link  != '' ){echo $user_info->twitter_link ;}else{echo 'N/A';}?></p>
														
													</div>
												</div>
												
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
			<div id="footer_space"></div>
		</div>

       