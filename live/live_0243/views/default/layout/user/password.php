<!-- responsive table -->
			<link href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>
		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Video</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
						
						<!-- main content -->
						<div class="row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Change Password</h4>
									</div>
								<?php if($error!='' && $error != 'update'){ ?>
											<div class="alert alert-danger">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
													<?php  echo $error; ?>
												
											</div>
									 <?php  } ?>
                                     
                                     
                                     <?php if($error!='' && $error == 'update'){ ?>
											<div class="alert alert-success">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
													<?php  echo "Password Reset Successfully"; ?>
												
											</div>
									 <?php  } ?>
									
									<div class="panel-body">
											<?php
						                $attributes = array('name'=>'changepasswordForm','id'=>'changepasswordForm','class'=>'');
						                echo form_open('user/change_password',$attributes);
					                     ?>
											<div class="form_sep">
												<label for="reg_input_name" class="req">New Password</label>
												<input type="password" id="new_password" name="new_password" class="form-control" data-required="true" value="">
											</div>
											<div class="form_sep">
												<label for="reg_input_email" class="req">Verify New Password</label>
												
												<input type="password" id="confirm_password" name="confirm_password" class="form-control" data-required="true" value="">
											</div>
											
											
											
											
											<div class="form_sep">
											<input type="hidden" name="user_id" id="user_id" value="<?php //echo $user_id;?>"/>
											<input type="submit" name="submit" id="submit" value="Update" class="btn btn-default"/>
											</div>	
										</form>
									</div>
								</div>
							</div>
							
						</div>
						
					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
			<div id="footer_space"></div>
			
			<!-- Modal -->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="text-align:center;" >
<div class="modal-body">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <img src="#"/>
</div>
<!--<div class="modal-footer">
<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
</div>-->
</div>
		</div>

       
<script src="<?php echo base_url().getThemeName(); ?>/js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url().getThemeName(); ?>/js/tag/jquery.tagsinput.js"></script>
<?php /*?><!-- mixItUp -->
			<script src="js/lib/mixitup/jquery.mixitup.min.js"></script>
	<!-- responsive lightbox -->
      <script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/magnific-popup/jquery.magnific-popup.min.js"></script>
			
			
<!-- multiple select -->
<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/multiple-select/jquery.multiple.select.js"></script>
<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/pages/ebro_gallery.js"></script>
<?php */?>
       

<script type="text/javascript">
$(document).ready(function(){
$('#tags_1').tagsInput({width:'auto'});

//===== Usual validation engine=====//
	$("#video_form").validate({
		rules: {
			video_title: "required",
			video_point: "required",
			video_content: "required",
			//video_category:"required",
			video_url: {
				required: true,
				url:true
			}
			
		}

	});
	
});	
	
</script>
