<!-- responsive lightbox -->
<?php /*?> <link rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/magnific-popup/magnific-popup.css">
<?php */?><script type="text/javascript" src="<?php echo base_url()?>tinymce/tinymce.min.js"></script>
	<!-- aditional stylesheets -->
	<script type="text/javascript">
	tinymce.init({selector:'#about_user'});
	
	</script>
<!-- parsley -->
		<!-- responsive table -->
			<link href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>
		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Profile</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
						
						<!-- main content -->
						<div class="row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title"><?php if($user_id > 0){?>Edit<?php }else{?>Add<?php }?> Profile</h4>
									</div>
									<?php if($error!='') { ?>
											<div class="alert alert-danger">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
													<?php  echo $error; ?>
												
											</div>
									 <?php  } ?>
									
									<div class="panel-body">
											<?php
												$attributes = array('name'=>'editForm','id'=>'editForm','class'=>'');
												echo form_open_multipart('user/edit',$attributes);
											?>
											<div class="form_sep">
												<label for="reg_input_name" class="req">First Name</label>
												<input type="text" id="first_name" name="first_name" class="form-control" data-required="true" value="<?php echo $first_name;?>">
											</div>
											
											<div class="form_sep">
												<label for="reg_input_name" class="req">Last Name</label>
												<input type="text" id="last_name" name="last_name" class="form-control" data-required="true" value="<?php echo $last_name;?>">
											</div>
											
											<div class="form_sep">
												<label for="reg_input_name" >Mobile No</label>
												<input type="text" id="mobile_no" name="mobile_no" class="form-control" data-required="true" value="<?php echo $mobile_no;?>">
											</div>
											
											<div class="form_sep">
												<label for="reg_input_name" >Phone No</label>
												<input type="text" id="phone_no" name="phone_no" class="form-control" data-required="true" value="<?php echo $phone_no;?>">
											</div>
											
											<div class="form_sep">
												<label for="reg_input_name">Zipcode</label>
												<input type="text" id="zip_code" name="zip_code" class="form-control" data-required="true" value="<?php echo $zip_code;?>">
											</div>
											
											
											<div class="form_sep">
												<label for="reg_input_name" class="req">Email</label>
												<input type="text" id="email" name="email" class="form-control" data-required="true" value="<?php echo $email;?>">
											</div>
											
											<div class="form_sep">
												<label for="reg_input_email">About Me</label>
												<textarea name="about_user" id="about_user" cols="30" rows="4" class="form-control"><?php echo $about_user;?></textarea>
											</div>
											
											
											<div class="form_sep" >
												<label for="reg_textarea_message">Upload Image</label>
												<input type="file" class="form-control" id="profileimage" name="profileimage" />
												<div class="clear"></div>
											</div>
											
											
										<div class="form_sep">
												<label for="reg_input_name">Personal Website</label>
												<input type="text" id="own_site_link" name="own_site_link" class="form-control" data-required="true" value="<?php echo $own_site_link;?>">
											</div>
											
										<div class="form_sep">
												<label for="reg_input_name">Facebook Link</label>
												<input type="text" id="facebook_link" name="facebook_link" class="form-control" data-required="true" value="<?php echo $facebook_link;?>">
											</div>
											
										<div class="form_sep">
												<label for="reg_input_name">Twiiter Profile Link</label>
												<input type="text" id="twitter_link" name="twitter_link" class="form-control" data-required="true" value="<?php echo $twitter_link;?>">
											</div>
											
										<div class="form_sep">
												<label for="reg_input_name" >Linkedin Profile Link</label>
												<input type="text" id="linkedin_link" name="linkedin_link" class="form-control" data-required="true" value="<?php echo $linkedin_link;?>">
											</div>		
									
									<div class="clear"></div>	
											<br/>
											<div class="form_sep">
											<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id;?>"/>
											<input type="submit" name="submit" id="submit" value="submit" class="btn btn-default"/>
											</div>	
										</form>
									</div>
								</div>
							</div>
							
						</div>
						
					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
			<div id="footer_space"></div>
			
			<!-- Modal -->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="text-align:center;" >
<div class="modal-body">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <img src="#"/>
</div>
<!--<div class="modal-footer">
<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
</div>-->
</div>
		</div>

       
<script src="<?php echo base_url().getThemeName(); ?>/js/jquery.validate.js"></script>
