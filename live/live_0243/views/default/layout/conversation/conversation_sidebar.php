<script type="text/javascript">
$(document).ready(function(){
$('#open_bid_frm').click(function(){

$('#bid_frm').slideToggle('slow');
});

$('#open_edit_bid_frm').click(function(){
$('#bid_frm').slideToggle('slow');
});


$("#offer_amount").keydown(function(event) {
        // Allow: backspace, delete, tab, escape, and enter
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });
});
</script>
<script type="text/javascript" src="<?php echo base_url().getThemeName(); ?>/js/jquery.wysiwyg.js"></script> 
<script>
$(function(){

$('textarea.wysiwyg').wysiwyg({
  				  controls: {
    				 strikeThrough : { visible : true },
    				 underline     : { visible : true },
      
   					 separator00 : { visible : false },
      
     				 justifyLeft   : { visible : true },
      				 justifyCenter : { visible : true },
     				 justifyRight  : { visible : true },
     				 justifyFull   : { visible : true },
      
 				     separator01 : { visible : false },
      
   				     indent  : { visible : false },
				    outdent : { visible : false },
      
     				 separator02 : { visible : false },
      
    				  subscript   : { visible : false },
    				  superscript : { visible : false },
      
   					   separator03 : { visible : false },
  	    
  					    undo : { visible : false },
   					   redo : { visible : false },
      
    				  separator04 : { visible : false },
      
     				 insertOrderedList    : { visible : false },
    				  insertUnorderedList  : { visible : false },
    				  insertHorizontalRule : { visible : false },
      
    				  h1mozilla : { visible : false && $.browser.mozilla, className : 'h1', command : 'heading', arguments : ['h1'], tags : ['h1'], tooltip : "Header 1" },
     				 h2mozilla : { visible : false && $.browser.mozilla, className : 'h2', command : 'heading', arguments : ['h2'], tags : ['h2'], tooltip : "Header 2" },
     				 h3mozilla : { visible : false && $.browser.mozilla, className : 'h3', command : 'heading', arguments : ['h3'], tags : ['h3'], tooltip : "Header 3" },
      
					
					 h1 : { visible : false && !( $.browser.mozilla ), className : 'h1', command : 'formatBlock', arguments : ['<H1>'], tags : ['h1'], tooltip : "Header 1" },
     				 h2 : { visible : false && !( $.browser.mozilla ), className : 'h2', command : 'formatBlock', arguments : ['<H2>'], tags : ['h2'], tooltip : "Header 2" },
   					   h3 : { visible : false && !( $.browser.mozilla ), className : 'h3', command : 'formatBlock', arguments : ['<H3>'], tags : ['h3'], tooltip : "Header 3" },
					 
					 
					 
					 
					 
					  h4mozilla : { visible : false && $.browser.mozilla, className : 'h4', command : 'heading', arguments : ['h4'], tags : ['h4'], tooltip : "Header 4" },
     				 h5mozilla : { visible : false && $.browser.mozilla, className : 'h5', command : 'heading', arguments : ['h5'], tags : ['h5'], tooltip : "Header 5" },
     				 h6mozilla : { visible : false && $.browser.mozilla, className : 'h6', command : 'heading', arguments : ['h6'], tags : ['h6'], tooltip : "Header 6" },
      
					
					 h4 : { visible : false && !( $.browser.mozilla ), className : 'h4', command : 'formatBlock', arguments : ['<H4>'], tags : ['h4'], tooltip : "Header 4" },
     				 h5 : { visible : false && !( $.browser.mozilla ), className : 'h5', command : 'formatBlock', arguments : ['<H5>'], tags : ['h5'], tooltip : "Header 5" },
   					   h6 : { visible : false && !( $.browser.mozilla ), className : 'h6', command : 'formatBlock', arguments : ['<H6>'], tags : ['h6'], tooltip : "Header 6" },
      
   					   separator07 : { visible : false },
      
     				 cut   : { visible : false },
     				 copy  : { visible : false },
     				 paste : { visible : false },
					 insertImage : {visible : false},
					 removeFormat : {visible : false},
					 createLink : {visible : false},
					  separator05 : { visible : false },
					   separator06 : { visible : false },
					   separator08 : { visible : false },
					  separator09 : { visible : false }
   					 }
  				});

});
			
            

</script>  

<script type="text/javascript">
function submitattachment_valid()
{
	
	var check=false;
/*	var bgchk=document.workerApplyForm.worker_background[0].checked;
	
	var glry_cnt=document.getElementById('glry_cnt').value;

	if(bgchk==true)
	{*/
		
        var chks = document.getElementsByName('file_up[]');
 	    var hasChecked = false;
    
		/*if(glry_cnt==1)
		{*/
			
		
			for (var i = 0; i < chks.length; i++)
			{
					if (chks[i].value=='')
					{
							/*check=false;
							var dv = document.getElementById('err1');
							
							dv.className = "error";
							dv.style.clear = "both";
							dv.style.minWidth = "110px";
							dv.style.margin = "5px";
							dv.innerHTML ='<ul><p style="color:#FF0000"><?php //echo 'Attachment require';?></p></ul>';
							dv.style.display='block';						
							hasChecked = true;    */      
							return true;              
							
					}
					else 
					{						
							value = chks[i].value;
							t1 = value.substring(value.lastIndexOf('.') + 1,value.length);
							
							
						
							if(   t1=='doc' || t1=='DOC' ||  t1=='docx' || t1=='DOCX'  || t1=='pdf' || t1=='PDF')
							{
								document.getElementById('err1').style.display='none';
								check=true;
							}
							else
							{							
								check=false;
								var dv = document.getElementById('err1');
								dv.className = "error";
								dv.style.clear = "both";
								dv.style.minWidth = "110px";
								dv.style.margin = "5px";
								dv.innerHTML = '<ul><p style="color:#FF0000"><?php echo 'Attachment not valid';?></p></ul>';
								dv.style.display='block';
								hasChecked = true;							
											
							}
					
					}
									
			}
		
		
		/*}
		else
		{
			check=true;
		}*/
		
		
		if(check==false)
		{
			return false;
		}
		else { return true; }
		
	
	/*} else {   return true; } */
		
	 
	 
	 
}

function editsubmitattachment_valid()
{
	
	
	var check=false;
/*	var bgchk=document.workerApplyForm.worker_background[0].checked;
	
	var glry_cnt=document.getElementById('glry_cnt').value;

	if(bgchk==true)
	{*/
		
        var chks = document.getElementsByName('file_up[]');
 	    var hasChecked = false;
    
		/*if(glry_cnt==1)
		{*/
			
		
			for (var i = 0; i < chks.length; i++)
			{
					if (chks[i].value=='')
					{
							/*check=false;
							var dv = document.getElementById('err1_edit');
							
							dv.className = "error";
							dv.style.clear = "both";
							dv.style.minWidth = "110px";
							dv.style.margin = "5px";
							dv.innerHTML ='<ul><p style="color:#FF0000"><?php //echo 'Attachment require';?></p></ul>';
							dv.style.display='block';						
							hasChecked = true;              */    
							
							return true;      
							
					}
					else 
					{						
							value = chks[i].value;
							t1 = value.substring(value.lastIndexOf('.') + 1,value.length);
								if(t1=='doc' || t1=='DOC' ||  t1=='docx' || t1=='DOCX'  || t1=='pdf' || t1=='PDF')
							{
								document.getElementById('err1').style.display='none';
								check=true;
							}
							else
							{							
								check=false;
								var dv = document.getElementById('err1_edit');
								dv.className = "error";
								dv.style.clear = "both";
								dv.style.minWidth = "110px";
								dv.style.margin = "5px";
								dv.innerHTML = '<ul><p style="color:#FF0000"><?php echo 'Attachment not valid';?></p></ul>';
								dv.style.display='block';
								hasChecked = true;							
											
							}
					
					}
									
			}
		
		
		/*}
		else
		{
			check=true;
		}*/
		
		if(check==false)
		{
			return false;
		}
		else { return true; }
		
	
	/*} else {   return true; } */
		
	 
	 
	 
}
function set_final_offer(offer)
{
	var offer_agent = offer;
	var admin_fees = <?php echo $site_setting->admin_fees;?>;
	var admin_fees = (offer * admin_fees) / 100;
	document.getElementById('admin_fees').value= admin_fees;
	var final_offer  = parseFloat(offer) + parseFloat(admin_fees);
	document.getElementById('final_offer_amount').value= final_offer;
	document.getElementById('final_offer_agent').value= final_offer;
	
	
	
}

function set_edit_final_offer(offer)
{
	var offer_agent = offer;
	var admin_fees = <?php echo $site_setting->admin_fees;?>;
	var admin_fees = (offer * admin_fees) / 100;
	document.getElementById('edit_admin_fees').value= admin_fees;
	var final_offer  = parseFloat(offer) + parseFloat(admin_fees);
	document.getElementById('edit_final_offer_amount').value= final_offer;
	document.getElementById('edit_final_offer_agent').value= final_offer;
	
	
	
}

 function delete_trip_offer(trip_offer_id,trip_id)
  {
  	
			if(trip_offer_id=='') { return false; }
  			var r=confirm("Are you sure want to delete this offer?");
			if (r==true)
			  {
			  	x=1;
				
			  }
			else
			  {
			 	 x=0;
			  }
			
			if(x== 0)
			{
				return false;
			}
		
			window.location.href = "<?php echo site_url('trip/delete_trip_offer/');?>"+"/"+trip_offer_id+"/"+trip_id;
	
   }

</script>



<div class="right_side" >


<?php
if(get_authenticateUserID() > 0){

if(get_authenticateUserID() != $trip_info->user_id)
{

if(check_user_agent(get_authenticateUserID()))
{
if($bid_now == 0) {
?>
<div class="bid_now"><a href="javascript:void(0)" id="open_bid_frm">Bid Now</a> </div>
<?php } } } else {

if(strtotime($trip_info->trip_confirm_date)>=strtotime(date('Y-m-d H:i:s')) && $trip_info->trip_activity_status==0 && ($trip_info->trip_agent_assign_id==0 || $trip_info->trip_agent_assign_id=='') ) { ?>

<div class="bid_now"><a href="<?php echo site_url('trip/edit/'.$trip_id); ?>">Edit Trip</a> </div>

<?php } } }
else
{ ?>
<div class="bid_now"><a id="open_bid_frm_login" href="<?php echo site_url('home/login/'.base64_encode(current_url())); ?>">Bid Now</a></div>
<?php } ?>
     
      <div class="trip_travel">
        <h5>Budget</h5>
        <strong style="font-size:28px;"><?php echo get_currency_symbol($trip_info->currency_code_id).' '.$trip_info->trip_min_budget;?> - <?php echo get_currency_symbol($trip_info->currency_code_id).' '.$trip_info->trip_max_budget;?></strong> <span>Price range you can 
        expect to pay for Trips
        similar to this one.</span> </div>
     <!--Bid form-->
	 
	 <?php if(strtotime($trip_info->trip_confirm_date)>=strtotime(date('Y-m-d H:i:s')) && $trip_info->trip_activity_status==0 && ($trip_info->trip_agent_assign_id==0 || $trip_info->trip_agent_assign_id=='') ) { ?> 
	 
      <div id="bid_frm"  <?php if($sow_bid_frm == 1){?>style="display:block;"<?php }else{?> style="display:none;"<?php }?>>
          <!--bid form-->
          <div class="clear" style="height:15px;"></div>
            <?php if($error != '') { ?>     
				 		 <div id="error" class="errmsgcl" style="margin-top:10px;"><?php  echo $error; ?></div>
			   <?php } ?>
          <div class="describe"> <img src="<?php echo base_url().getThemeName(); ?>/images/kery_arrow.png" alt="" class="img"  />
            <h2>Describe your Proposal</h2>
          </div>
          <?php
		  if($trip_offer_id =='')
		  {
			$attributes = array('name'=>'praposal','id'=>'praposal','autocomplete'=>'off','onsubmit'=>'return submitattachment_valid()');
		}else
		{
			$attributes = array('name'=>'praposal','id'=>'praposal','autocomplete'=>'off','onsubmit'=>'return submitattachment_valid()');		
		}	
			echo form_open_multipart('trip/post_offer/',$attributes);
		?>
          <div class="type_in_to_text">
           <!-- WYSIWYG editor --> 
            <textarea cols="28" rows="10" class="wysiwyg" id="proposal_desc" name="proposal_desc" 
            onkeyup="limitText(this.form.proposal_desc,this.form.countdown,4000)"><?php echo $proposal_desc;?></textarea> 
            
            <?php if($trip_id != '')
			{
				$trip_offer_doc = $this->trip_model->trip_offer_doc($trip_id,get_authenticateUserID());
				$dcnt=1;
			 	if($trip_offer_doc) { 
				foreach($trip_offer_doc as $doc) { 
					if(file_exists(base_path().'upload/trip_offer_doc/'.$doc->offer_attachment)) 
					{?>
					<div> <b><a target="_blank" href="<?php echo base_url();?>upload/trip_offer_doc/<?php echo $doc->offer_attachment; ?>"><?php echo $doc->offer_attachment; ?></a></b></div>
					<?php }
				}
			}}
				
			?>
            <div class="clear"></div>
            <!-- End of WYSIWYG editor --> 
             <a href="javascript:void(0)">Add Attachment</a>
             <div id="err1" style="display:none;"></div>		
            <input name="file_up[]" type="file" />
            
            </div>
          

          <div class="describe"> <img src="<?php echo base_url().getThemeName(); ?>/images/bag.png" alt="" class="img"  />
            <h2>Build a package</h2>
          </div>
          <div class="type_to_in">
           <input type="radio" name="offer_build_package" value="1" id="b_pack1" <?php if($offer_build_package == 1){?> checked="checked"<?php }?>/ >
            <label>Flight + Hotel + Car</label>
          </div>
          <div class="type_to_in">
             <input type="radio" name="offer_build_package" value="2" id="b_pack2" <?php if($offer_build_package == 2){?> checked="checked"<?php }?>/>
            <label>Flight + Hotel </label>
          </div>
          <div class="type_to_in">
             <input type="radio" name="offer_build_package" value="3" id="b_pack3" <?php if($offer_build_package == 3){?> checked="checked"<?php }?>/>
            <label>Flight + Car</label>
          </div>
          
          <div class="describe"> <img src="<?php echo base_url().getThemeName(); ?>/images/watch.png" alt="" class="img"  />
            <h2>Cost & Timing</h2>
          </div>
          <div class="cost_timing">
            <div class="currency"><?php /*?> <span>Currency Calculator</span> <img src="<?php echo base_url().getThemeName(); ?>/images/calcu.png" alt="" class="img"  /> <?php */?></div>
            <div class="ofer_price"> <img src="<?php echo base_url().getThemeName(); ?>/images/dolar.png" alt="" class="img"? />
              <label>Offer Price</label>
              <span><?php echo get_currency_symbol($trip_info->currency_code_id);?></span>
              <input type="text" name="offer_amount" value="<?php echo $offer_amount;?>" id="offer_amount" onkeyup="set_final_offer(this.value);"/>
            </div>
          </div>
          <div class="billed_client">
            <div class="bidli">
              <h4>Billed to Client</h4>
              <span>Include</span>
              <p>EggTrip Fee : <?php echo $site_setting->admin_fees.'%';?></p>
            </div>
            <strong><?php echo get_currency_symbol($trip_info->currency_code_id);?></strong>
            <input type="text" name="final_offer_amount" id="final_offer_amount" value="<?php echo $final_offer_amount;?>" disabled="disabled"/>
          </div>
          <div class="describe"> <img src="<?php echo base_url().getThemeName(); ?>/images/watch2.png" alt="" class="img"  />
            <h2>Trip Duration</h2>
          </div>
          <div class="tripduration">
            <div class="ncryp">
              <label>Days</label>
              <input type="text" name="offer_days" value="<?php echo $offer_days;?>"/>
            </div>
            <div class="ncryp">
              <label>Nights</label>
              <input type="text" name="offer_nights" value="<?php echo $offer_nights;?>" />
            </div>
            <div class="clear"></div>
             <div class="ask lt" style="margin-top:10px; margin-bottom:10px;">
             <input type="hidden" name="trip_id" id="trip_id" value="<?php echo $trip_id;?>"/>
              <input type="hidden" name="admin_fees" id="admin_fees" value="<?php echo $admin_fees;?>"/>
               <input type="hidden" name="trip_offer_id" id="trip_offer_id" value="<?php echo $trip_offer_id;?>"/>
               <input type="hidden" name="final_offer_agent" id="final_offer_agent" value="<?php echo $final_offer_agent;?>"/>
             <input type="submit" name="proposalbutton" id="proposalbutton" value="Offer"/></div>
          </div><div class="clear"></div>
         </form> 
          <!--bid form-->
     </div>
	 
	 <?php } ?>
     <!--end bid from-->
     
       <!-- agent bid display here 
       if user login and user is agent and user bid on this trip then only-->
       <?php if($agent_bid){
	   foreach($agent_bid as $ab){
	  ?>
          <div class="trip_travel">
            <h5>Bid</h5>
            <strong><?php echo get_currency_symbol($ab->currency_code_id).' '.$ab->final_offer_amount;?></strong> 
            <span>&nbsp;</span>
           
          </div>
	<!--Only trip activity status =0 and trip assign_agentid =0 or '' and trip confirm > current and -->
	<?php if(strtotime($trip_info->trip_confirm_date)>=strtotime(date('Y-m-d H:i:s')) && $trip_info->trip_activity_status==0 && ($trip_info->trip_agent_assign_id==0 || $trip_info->trip_agent_assign_id=='') ) { ?> 
      <div class="edit">
        <ul>
          <li><a href="javascript:void(0)" id="open_edit_bid_frm">Edit</a></li>
          <li><span style="color:#666666;">|</span></li>
          <li><a href="javascript:void(0)" onclick="return delete_trip_offer(<?php echo $ab->trip_offer_id;?>,<?php echo $ab->trip_id;?>)">Delete</a></li>
        </ul>
      </div>
	<?php }  ?>
      <!--Only trip activity status =0 and trip assign_agentid =0 or '' and trip confirm > current and -->
      <div class="egg_trip" style="position:relative; overflow:visible;"> 
      	 <?php $agent_image= base_url().'upload/no_image.png';					
				 if($ab->profile_image!='') {  
					if(file_exists(base_path().'upload/user_thumb/'.$ab->profile_image)) {
						$user_image=base_url().'upload/user_thumb/'.$ab->profile_image;
					}
			}
		?>
         <?php echo anchor('user/'.$ab->profile_name,'<img src="'. $agent_image.'" alt="" class="img" />');
           		$check_agent_detail=$this->travelagent_model->check_agent_detail($ab->user_id);
        	if($check_agent_detail) { 
          ?>
        <?php /*?> <h6 class="orenge_box_new"><?php echo $check_agent_detail->agent_level;?></h6> <?php */?>    <?php } ?>  
           
        <span>
        <?php echo anchor('user/'.$ab->profile_name,$ab->full_name);?> <br>
        <strong>selected by <?php echo anchor('user/'.$user_info->profile_name,$user_info->profile_name);?></strong></span> </div>
      <div class="egg_coment">
        <p><span>Proposals:</span> <?php echo  $ab->offer_content;?></p>
      </div>
     <?php }}?>   
    <!-- agent bid display here-->
    
    <div class="clear"></div>
    
      <!---on complete-->
	  
	  <?php 
	  if($trip_info->trip_agent_assign_id > 0 && ($trip_info->trip_activity_status == 2 || $trip_info->trip_activity_status == 3) && strtotime(date('Y-m-d H:i:s')) >= strtotime($trip_info->trip_confirm_date)){
	 	 $trip_review = get_trip_review($trip_info->trip_id,$trip_info->trip_agent_assign_id);

		  if($trip_review){
		  
			$assign_user_info = get_user_profile_by_id($trip_info->trip_agent_assign_id);
			$assign_agent_detail=$this->travelagent_model->check_agent_detail($trip_info->trip_agent_assign_id);
		  
			$assign_user_image= base_url().'upload/no_image.png';					
				 if($assign_user_info->profile_image!='') {  
					if(file_exists(base_path().'upload/user_thumb/'.$assign_user_info->profile_image)) {
						$assign_user_image=base_url().'upload/user_thumb/'.$assign_user_info->profile_image;
					}
			}

	  ?>
      <div class="travel_agent">
        <h1>Travel Agent Rating for this Trip</h1>
		
		<div class="strmn" style="margin-bottom:10px;"><div class="str_sel" style="width:<?php if($trip_review->user_rating > 5) { ?>100<?php } else { echo round($trip_review->user_rating*2);?>0<?php } ?>%;"></div></div>
		
		
      <div class="egg_trip"> <img src="<?php echo $assign_user_image; ?>" alt="" class="img"  />
	  
	  	<?php /*?><?php if($assign_agent_detail) {  ?>
        <div class="orenge_box coversation_ie">
          <h6><?php echo $assign_agent_detail->agent_level;?></h6>
        </div>
		<?php } ?><?php */?>
		
        <span><!--Egg Trip<br />-->
       <?php echo anchor('user/'.$assign_user_info->profile_name,$assign_user_info->full_name);?> <br />
        <strong>selected by <?php echo anchor('user/'.$user_info->profile_name,$user_info->profile_name);?></strong></span> </div>
      <div class="egg_coment">
        <p><span>Comment:</span> <?php echo $trip_review->user_review;?></p>
      </div>
	  <?php } } ?>
	   <div class="clear"></div>
      <!---on complete
      (curre_date > confirm_date and activyt_status =0 ) || ( ( activity_status==2 || activety_status==3 ) && trip_assign_aget_id > 0 )-->
 
 
 
 
 <!--status-->
      <div class="trip_status">
        <h3>Trip Status</h3>
      </div>
      <?php $curre_date = date('Y-m-d H:i:s');
	  
	   if($trip_info->trip_activity_status == 0 && $trip_info->trip_confirm_date > $curre_date)
	  {?>
      <div class="assigned" style="width:250px;"> <img src="<?php echo base_url().getThemeName(); ?>/images/safari_bag.png" alt="" class="img">
        <h2>Posted    <?php echo date($site_setting->date_format,strtotime($trip_info->trip_added_date));?></h2>
      </div>
      <?php }
	  else{?>
      <div class="posted1"> <img src="<?php echo base_url().getThemeName(); ?>/images/safari_bag.png" alt="" class="img">
        <h2>Posted    <?php echo date($site_setting->date_format,strtotime($trip_info->trip_added_date));?></h2>
      </div>
	  <?php } 
	  if($trip_info->trip_activity_status == 1 && $trip_info->trip_agent_assign_id > 0 && $trip_info->trip_assign_date != ''){?>
      <div class="assigned"> <a href="javascript:void(0)"> <img src="<?php echo base_url().getThemeName(); ?>/images/lage.png" alt="" class="img"></a>
        <h2>Assigned    <?php echo date($site_setting->date_format,strtotime($trip_info->trip_assign_date));?></h2>
      </div>
      
      
      
      
      
      <div class="travel_agent" style="margin-top:10px;"> 
	  <?php
	  		$assign_user_info = get_user_profile_by_id($trip_info->trip_agent_assign_id);
			$assign_agent_detail=$this->travelagent_model->check_agent_detail($trip_info->trip_agent_assign_id);
			
			$total_rate=get_user_total_rate($trip_info->trip_agent_assign_id);
			$total_review=get_user_total_review($trip_info->trip_agent_assign_id);
			
			$assign_user_image= base_url().'upload/no_image.png';					
				 if($assign_user_info->profile_image!='') {  
					if(file_exists(base_path().'upload/user_thumb/'.$assign_user_info->profile_image)) {
						$assign_user_image=base_url().'upload/user_thumb/'.$assign_user_info->profile_image;
					}
			}
          ?>
         
	  
		
	  <div class="strmn" style="margin-bottom:10px;"><div class="str_sel" style="width:<?php if($total_rate>5) { ?>100<?php } else { echo round($total_rate*2);?>0<?php } ?>%;"></div></div>
	  </div>    
    
      <div class="egg_trip"><?php echo anchor('user/'.$assign_user_info->profile_name,'<img src="'. $assign_user_image.'" alt="" class="img" />'); ?>
	  
	  	<?php /*?><?php if($assign_agent_detail) {  ?>
        <div class="orenge_box coversation_ie">
          <h6><?php echo $assign_agent_detail->agent_level;?></h6>
        </div>
		<?php } ?><?php */?>
        <span><!--Egg Trip<br>-->
        <?php echo anchor('user/'.$assign_user_info->profile_name,$assign_user_info->full_name);?><br>
        <strong>selected by <?php echo anchor('user/'.$user_info->profile_name,$user_info->profile_name);?></strong></span> </div>
        
        
        
        
        
        
        
        <?php }else {?>
         <div class="posted1"> <a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/lage.png" alt="" class="img"></a>
        <h2>Assigned </h2>
      </div>	
		<?php }
		if($trip_info->trip_activity_status == 2 && $trip_info->trip_agent_assign_id > 0 && $trip_info->trip_completed_date != ''){?>
      <div class="assigned"> <img src="<?php echo base_url().getThemeName(); ?>/images/right.png" alt="" class="img">
        <h2>Completed <?php echo date($site_setting->date_format,strtotime($trip_info->trip_completed_date));?></h2>
      </div>
      
          
      <div class="travel_agent" style="margin-top:10px;"> 
	  <?php
	  		$assign_user_info = get_user_profile_by_id($trip_info->trip_agent_assign_id);
			$assign_agent_detail=$this->travelagent_model->check_agent_detail($trip_info->trip_agent_assign_id);
			
			$total_rate=get_user_total_rate($trip_info->trip_agent_assign_id);
			$total_review=get_user_total_review($trip_info->trip_agent_assign_id);
			
			$assign_user_image= base_url().'upload/no_image.png';					
				 if($assign_user_info->profile_image!='') {  
					if(file_exists(base_path().'upload/user_thumb/'.$assign_user_info->profile_image)) {
						$assign_user_image=base_url().'upload/user_thumb/'.$assign_user_info->profile_image;
					}
			}
          ?>
         
	  
		
	  <div class="strmn" style="margin-bottom:10px;"><div class="str_sel" style="width:<?php if($total_rate>5) { ?>100<?php } else { echo round($total_rate*2);?>0<?php } ?>%;"></div></div>
	  </div>    
    
      <div class="egg_trip"><?php echo anchor('user/'.$assign_user_info->profile_name,'<img src="'. $assign_user_image.'" alt="" class="img" />'); ?>
	  
	  	<?php /*?><?php if($assign_agent_detail) {  ?>
        <div class="orenge_box coversation_ie">
          <h6><?php echo $assign_agent_detail->agent_level;?></h6>
        </div>
		<?php } ?><?php */?>
        <span><!--Egg Trip<br>-->
        <?php echo anchor('user/'.$assign_user_info->profile_name,$assign_user_info->full_name);?><br>
        <strong>selected by <?php echo anchor('user/'.$user_info->profile_name,$user_info->profile_name);?></strong></span> </div>
        
        
        
        
        
        
      
      
      <?php } else{?>
        <div class="posted1"> <img src="<?php echo base_url().getThemeName(); ?>/images/right.png" alt="" class="img">
        <h2>Completed</h2>
      </div>
     <?php }
	
	 if(($curre_date > $trip_info->trip_confirm_date && $trip_info->trip_activity_status == 0 ) || ( ( $trip_info->trip_activity_status == 2 || $trip_info->trip_activity_status ==3 ) && $trip_info->trip_agent_assign_id > 0 ))
	 {?>
      <div class="assigned"> <img src="<?php echo base_url().getThemeName(); ?>/images/lock.png" alt="" class="img">
        <h2>Closed <?php echo date($site_setting->date_format,strtotime($trip_info->trip_close_date));?></h2>
      </div>
      <?php }else{?>
       <div class="posted2"> <img src="<?php echo base_url().getThemeName(); ?>/images/lock.png" alt="" class="img">
        <h2>Closed</h2>
      </div>
	  <?php }?>
      <div class="clear"></div>
      
       <?php
	
	        $facbookurl='http://www.facebook.com/sharer.php?u='.site_url('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id).'&t='.$user_info->profile_name;
            
            $twitterurl = 'http://twitter.com/home?status='.site_url('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id);
            
            $redditurl = 'http://reddit.com/submit?url='.site_url('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id).'&title='.$user_info->profile_name;
            
            $tumblrurl = 'http://www.tumblr.com/share/link?url='.site_url('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id).'&name='.$user_info->profile_name;
            
            $stumbleuponurl = 'http://www.stumbleupon.com/submit?url='.site_url('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id).'&title='.$user_info->profile_name;
			
		$delicious_url = 'http://del.icio.us/post?url='.site_url('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id);
		
		$digg_url = 'http://digg.com/submit?phase=2&url='.site_url('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id);
			
			?>
      
      <div class="marB20">
        <div class="estim marB10">Share with Friends</div>
       
        
       <a rel="nofollow" onClick="javascript:window.open('<?php echo $facbookurl; ?>','Share on Facebook','height=350,width=650')" class="share-facebook" href="javascript:void(0)"><img src="<?php echo base_url().getThemeName(); ?>/images/facebkbtn.png" alt="" title="Share on Facebook" /></a>
      
        <a rel="nofollow" onClick="javascript:window.open('<?php echo $twitterurl; ?>','Share on Twitter','height=350,width=650')" class="share-twitter" href="javascript:void(0)" title="share link with title"><img src="<?php echo base_url().getThemeName(); ?>/images/twitterbtn.png" alt="" title="Share on Twitter" /></a>
      
          <a onClick="javascript:window.open('<?php echo $redditurl; ?>','Share on Reddit','height=350,width=650')" href="javascript:void(0)" title="Reddit"><img src="<?php echo base_url().getThemeName(); ?>/images/redditbtn.png" alt="" title="Share on Reddit"/></a>
            
            <a onClick="javascript:window.open('<?php echo $delicious_url; ?>','Share on Tumblr','height=350,width=650')" href="javascript:void(0)" title="Share on Tumblr" ><img src="<?php echo base_url().getThemeName(); ?>/images/ctc.png" alt="" title="Share on Tumblr"/></a>
            
            <a onClick="javascript:window.open('<?php echo $stumbleuponurl; ?>','Share on Stumble Upon','height=350,width=650')" href="javascript:void(0)" title="Share on Stumble Upon"><img src="<?php echo base_url().getThemeName(); ?>/images/stumbleupbtn.png" alt="" title="Share on Stumble Upon"/></a>
            
              <a href="javascript:void(0)" onClick="window.open('<?php echo $digg_url; ?>','Share on Digg','height=350,width=650');" title="Share On Digg"><img src="<?php echo base_url().getThemeName(); ?>/images/digg.png" alt=""/></a>
         
            
            <a href="mailto:"><img src="<?php echo base_url().getThemeName(); ?>/images/mailbtn.png" /></a>
            
          </div>
      <!--status-->
      
       
    </div>