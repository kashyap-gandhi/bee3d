		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Dashboard</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
               
					<div id="main_content">
						
						<!-- main content -->
                        
                        <?php 
						if($msg == "success")
						{ ?>
						  
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                         Package Has been purchased Successfully.
                                                                       
                         </div>
                         
                         <?php }?>
                         
                           <?php 
						if($msg == "fail")
						{ ?>
						  
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        Payment Process has been not completed.Please Contact Administrator.
                                                                       
                         </div>
                         
                         <?php }?>
 


						     
						<h2>Package</h2>
						<div class="row"> 
                    
                    
                        <?php 
						
						if($package_list)
						{
						    foreach($package_list as $pl)
							{?>
                            <div class="col-sm-4">
								<div class="panel panel-default">
									<div class="panel-body">
										<?php //  echo $pl->package_short_description; ?>
                                         <h3 class="text-center"> <?php echo $pl->package_points; ?>  </h3>
                                         <p> <h3 class="text-center"> CREDITS  </h3></p>
                                         <h3 class="text-center">
                                          <?php 
										  $credit_setting=credit_setting();
										 /// $rate=$credit_setting->point2paisa;
										 if($pl->package_price > 0)
										 {
										  $rate = $pl->package_points / $pl->package_price;
										  }
										  else
										  {
										   $rate =0;
										  }
										 echo $site_setting->currency_symbol."".$pl->package_price; ?>  <?php echo $site_setting->currency_code;?> </h3>
                                          <p class="text-center"> ( <?php echo $site_setting->currency_symbol."".$rate." / Credit" ;?> ) <?php echo $site_setting->currency_code;?>  </p> 
                                         
                                        <p class="text-center"> <a href="<?php echo site_url("package/buy_credit/".base64_encode($pl->package_id)); ?>" class="btn btn-default" type="submit">Buy Now</a> </p>
									</div>
								</div>
							</div>
					  <?php }
						}
						?>
							<!--<div class="col-sm-4">
								<div class="panel panel-default">
									<div class="panel-body">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat nihil obcaecati dolore atque distinctio incidunt ex ducimus perferendis ullam! Tempore unde saepe veritatis illo fugiat itaque ipsum error omnis velit.
                                         <h3 class="text-center"> $ 1232.00 </h3>
                                        <p class="text-center"> <a class="btn btn-default" type="submit">Buy Now</a> </p>
									</div>
								</div>
							</div>
                            <div class="col-sm-4">
								<div class="panel panel-default">
									<div class="panel-body">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat nihil obcaecati dolore atque distinctio incidunt ex ducimus perferendis ullam! Tempore unde saepe veritatis illo fugiat itaque ipsum error omnis velit.
                                         <h3 class="text-center"> $ 1232.00 </h3>
                                        <p class="text-center"> <a class="btn btn-default" type="submit">Buy Now</a> </p>
									</div>
								</div>
							</div>
                            <div class="col-sm-4">
								<div class="panel panel-default">
									<div class="panel-body">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat nihil obcaecati dolore atque distinctio incidunt ex ducimus perferendis ullam! Tempore unde saepe veritatis illo fugiat itaque ipsum error omnis velit.
                                         <h3 class="text-center"> $ 1232.00 </h3>
                                        <p class="text-center"> <a class="btn btn-default" type="submit">Buy Now</a> </p>
									</div>
								</div>
							</div>
-->                         </div>   


					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
			<div id="footer_space"></div>
		</div>

       