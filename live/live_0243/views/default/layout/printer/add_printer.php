<!-- responsive lightbox -->
	<!-- parsley -->
		<!-- responsive table -->
			<link href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>
		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Dashboard</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
						
						<!-- main content -->
						<div class="row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title"><?php if($store_id > 0){?>Edit<?php }else{?>Add<?php }?>  3D Printer to Personal Data Base</h4>
									</div>
									<?php if($error!='') { ?>
											<div class="alert alert-danger">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
													<?php  echo $error; ?>
												
											</div>
									 <?php  } ?>
									
									<div class="panel-body">
											<?php
												$attributes = array('name'=>'store_form','id'=>'store_form','class'=>'');
												echo form_open_multipart('printer/add_me',$attributes);
											?>
											
												
											<div class="form_sep">
												<label for="reg_select">Type or Brand of 3D Printer</label>
												<select name="category_id" id="category_id" class="form-control" data-required="true">											<option value="">--Select Category--</option>
												<?php
												if($store_category)
												{ 
													foreach($store_category as $sct)
													{?>																																					                                                       <option value="<?php echo $sct->category_id;?>" <?php if($category_id == $sct->category_id){?> selected="selected"<?php }?>><?php echo $sct->category_name;?></option>										
													<?php }
												}
												?>
												</select>
											</div>
											
											<div class="form_sep">
												<label for="reg_input_name" class="req">3D Printer Title</label>
												<input type="text" id="store_name" name="store_name" class="form-control" data-required="true" value="<?php echo $store_name;?>">
											</div>
											
											<div class="form_sep">
												<label for="reg_input_name" class="req">Email</label>
												<input type="text" id="store_email" name="store_email" class="form-control" data-required="true" value="<?php echo $store_email;?>">
											</div>
											
											<div class="form_sep">
												<label for="reg_input_name" class="req">Phone</label>
												<input type="text" id="store_phone" name="store_phone" class="form-control" data-required="true" value="<?php echo $store_phone;?>">
											</div>
											
											<div class="form_sep">
												<label for="reg_input_name">URL</label>
												<input type="text" id="store_url" name="store_url" class="form-control" data-required="true" value="<?php echo $store_url;?>">
											</div>
											
											<div class="form_sep">
												<label for="reg_input_email" class="req">Address</label>
												 <input type="text" name="store_address" id="store_address" value="<?php echo $store_address;?>" class="form-control"/>
											</div>
											
											<div class="form_sep">
												<label for="reg_input_email" class="req">Address2</label>
											 <input type="text" name="store_address2" id="store_address2" value="<?php echo $store_address2;?>" class="form-control"/>
											</div>
											
											<div class="form_sep">
												<label for="reg_input_name" class="req">City</label>
												<input type="text" id="store_ctiy" name="store_ctiy" class="form-control" data-required="true" value="<?php echo $store_ctiy;?>">
											</div>
											
											<div class="form_sep">
												<label for="reg_input_name" class="req">Province or State</label>
												<input type="text" id="store_state" name="store_state" class="form-control" data-required="true" value="<?php echo $store_state;?>">
											</div>
											
											<div class="form_sep">
												<label for="reg_input_name" class="req">Country</label>
												<input type="text" id="store_country" name="store_country" class="form-control" data-required="true" value="<?php echo $store_country;?>">
											</div>
											
											<div class="form_sep">
												<label for="reg_input_name" class="req">Postal Code</label>
												<input type="text" id="store_postal_code" name="store_postal_code" class="form-control" data-required="true" value="<?php echo $store_postal_code;?>">
											</div>
											
											
											
											<div class="form_sep">
											 <div id="map-canvas" style="width: 100%; height: 500px;"></div>
											</div>
											
											<div class="form_sep">
											<input type="hidden" name="store_id" id="store_id" value="<?php echo $store_id;?>"/>
											<input type="hidden" id="store_lat" name="store_lat" value="<?php echo $store_lat;?>">																                                            <input type="hidden" id="store_long" name="store_long" value="<?php echo $store_long;?>">
											<input type="submit" name="submit" id="submit" value="submit" class="btn btn-default"/>
											</div>	
										</form>
									</div>
								</div>
							</div>
							
						</div>
						
					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
			<div id="footer_space"></div>
			
			<!-- Modal -->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="text-align:center;" >
<div class="modal-body">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <img src="#"/>
</div>
<!--<div class="modal-footer">
<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
</div>-->
</div>
		</div>

       
<script src="<?php echo base_url().getThemeName(); ?>/js/jquery.validate.js"></script>
<?php /*?><!-- mixItUp -->
			<script src="js/lib/mixitup/jquery.mixitup.min.js"></script>
	<!-- responsive lightbox -->
      <script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/magnific-popup/jquery.magnific-popup.min.js"></script>
			
			
<!-- multiple select -->
<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/multiple-select/jquery.multiple.select.js"></script>
<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/pages/ebro_gallery.js"></script>
<?php */?>
<script type="text/javascript">
$(document).ready(function(){

//===== Usual validation engine=====//
	$("#store_form").validate({
		rules: {
			store_name: "required",
			store_address: "required",
			store_address2: "required",
			store_ctiy:"required",
			store_state: "required",
			store_country: "required",
			store_postal_code:"required",
			store_email:"required",
			store_phone:"required",
			store_status: "required"
		}

	});
	$('li a').click(function(e) {
    $('#myModal img').attr('src', $(this).attr('data-img-url')); 
});
});	
	
</script>
<script src="jquery.min.js" type="text/javascript"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>

        <script>

            //        var placeSearch, autocomplete;
            //var componentForm = {
            //  street_number: 'short_name',
            //  route: 'long_name',
            //  locality: 'long_name',
            //  administrative_area_level_1: 'short_name',
            //  country: 'long_name',
            //  postal_code: 'short_name'
            //};

            var placeSearch, autocomplete,map;
            var marker,geocoder;
            var componentForm = {
                street_number: 'long_name',
                route: 'long_name',
                locality: 'long_name',
                administrative_area_level_1: 'long_name',
                country: 'long_name',
                postal_code: 'long_name'
            };


            /*var system_lat=-33.8688;
            var system_long=151.2195;*/
			var system_lat=<?php echo $site_setting->default_latitude;?>;
            var system_long=<?php echo $site_setting->default_longitude;?>;
            
            <?php if($store_id>0) { ?>
            
            var store_lat = <?php echo $store_lat?>;
            var store_long = <?php echo $store_long?>;
            
            system_lat=store_lat;
            system_long=store_long;
            
            <?php } ?>
            
            

            function initialize() {

                geocoder = new google.maps.Geocoder();

                 var myLatlng = new google.maps.LatLng( system_lat, system_long);
                 
                var mapOptions = {
                    center: myLatlng,
                    zoom: 13,
                    mapTypeId: google.maps.MapTypeId.HYBRID
                };
                var map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);


                //var defaultBounds = new google.maps.LatLngBounds(new google.maps.LatLng(53.0026644,4.866806));
                // map.fitBounds(defaultBounds);


                var input = document.getElementById('store_address');

                //  var types = document.getElementById('type-selector');
                //  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                //  map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.bindTo('bounds', map);

                var infowindow = new google.maps.InfoWindow();
                var marker = new google.maps.Marker({
                    map: map,
                    position: myLatlng,
                    draggable:true,
                    anchorPoint: new google.maps.Point(0, -29)
                });


                google.maps.event.addListener(marker,'drag',function() {
                    geocodePosition(marker.getPosition());
                });

                google.maps.event.addListener(marker,'dragend',function() {
                    geocodePosition(marker.getPosition());
                });

                google.maps.event.addListener(autocomplete, 'place_changed', function(){
                    infowindow.close();
                    marker.setVisible(false);
                    var place = autocomplete.getPlace();


                    if (!place.geometry) {
                        return;
                    }

                    showaddress_fields(place);

                    // If the place has a geometry, then present it on a map.
                    if (place.geometry.viewport) {
                        map.fitBounds(place.geometry.viewport);
                    } else {
                        map.setCenter(place.geometry.location);
                        map.setZoom(17);  // Why 17? Because it looks good.
                    }
                    marker.setIcon(/** @type {google.maps.Icon} */({
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(35, 35)
                    }));
                    marker.setPosition(place.geometry.location);
                    marker.setVisible(true);

                    var address = '';
                    if (place.address_components) {
                        address = [
                            (place.address_components[0] && place.address_components[0].short_name || ''),
                            (place.address_components[1] && place.address_components[1].short_name || ''),
                            (place.address_components[2] && place.address_components[2].short_name || ''),
                            (place.address_components[3] && place.address_components[3].short_name || ''),
                            (place.address_components[4] && place.address_components[4].short_name || ''),
                            (place.address_components[5] && place.address_components[5].short_name || ''),
                            (place.address_components[6] && place.address_components[6].short_name || '')
                        ].join(' ');
                    }

                    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                    infowindow.open(map, marker);
                });




                // Sets a listener on a radio button to change the filter type on Places
                // Autocomplete.
                function setupClickListener(id, types) {
                    var radioButton = document.getElementById(id);
                    google.maps.event.addDomListener(radioButton, 'click', function() {
                        autocomplete.setTypes(types);
                    });
                }


                setupClickListener('changetype-all', []);
                setupClickListener('changetype-establishment', ['establishment']);
                setupClickListener('changetype-geocode', ['geocode']);


            }



            google.maps.event.addDomListener(window, 'load', initialize);



            function showaddress_fields(place) {


                document.getElementById("store_lat").value = place.geometry.location.lat();
                document.getElementById("store_long").value = place.geometry.location.lng();



                //            alert(place.formatted_address);
                //            alert(place.address_components.length);
                var formatted_Address = place.formatted_address;
                var address = "", city = "", state = "", zip = "", country = "", country_short = "",formattedAddress = "";


                // Get each component of the address from the place details
                // and fill the corresponding field on the form.
                for (var i = 0; i < place.address_components.length; i++) {
                    var addr = place.address_components[i];


                    // alert(addr.types[0]);

                    /*
                     *  var addressType = place.address_components[i].types[0];
                        if (componentForm[addressType]) {
                                var val = place.address_components[i][componentForm[addressType]];
                                document.getElementById(addressType).value = val;
                                //formatted_Address = formatted_Address.replace(place.address_components[i]['long_name'],"");
                        }
                     */

                    // check if this entry in address_components has a type of country
                    if (addr.types[0] == 'country'){
                        country = addr.long_name;
                        country_short = addr.short_name;
                    }
                    //                    else if (addr.types[0] == 'street_number'){// address 1
                    //                            address = address + " " + addr.long_name;
                    //                    }
                    //                    else if (addr.types[0] == 'street_address'){// address 1
                    //                            address = address + " " + addr.long_name;
                    //                    }
                    //                    else if (addr.types[0] == 'establishment'){
                    //                            address = address + " " + addr.long_name;
                    //                    }
                    //                    else if (addr.types[0] == 'sublocality'){
                    //                            address = address + " " + addr.long_name;
                    //                    }
                    //                    else if (addr.types[0] == 'route') {// address 2
                    //                            address = address + " " + addr.long_name;
                    //                    }

                    else if (addr.types[0] == 'floor'){
                        address = address + " " + addr.long_name;
                    }
                    else if (addr.types[0] == 'premise'){
                        address = address + " " + addr.long_name;
                    }
                    else if (addr.types[0] == 'parking'){
                        address = address + " " + addr.long_name;
                    }
                    else if (addr.types[0] == 'establishment'){
                        address = address + " " + addr.long_name;
                    }
                    else if (addr.types[0] == 'natural_feature'){
                        address = address + " " + addr.long_name;
                    }
                    else if (addr.types[0] == 'airport'){
                        address = address + " " + addr.long_name;
                    }
                    else if (addr.types[0] == 'parking'){
                        address = address + " " + addr.long_name;
                    }
                    else if (addr.types[0] == 'post_box'){
                        address = address + " " + addr.long_name;
                    }
                    else if (addr.types[0] == 'postal_town'){
                        address = address + " " + addr.long_name;
                    }
                    else if (addr.types[0] == 'street_number'){// address 1
                        address = address + " " + addr.long_name;
                    }
                    else if (addr.types[0] == 'street_address'){// address 1
                        address = address + " " + addr.long_name;
                    }
                    else if (addr.types[0] == 'establishment'){
                        address = address + " " + addr.long_name;
                    }
                    else if (addr.types[0] == 'neighborhood'){
                        address = address + " " + addr.long_name;
                    }
                    else if (addr.types[0] == 'sublocality_level_5'){
                        address = address + " " + addr.long_name;
                    }
                    else if (addr.types[0] == 'sublocality_level_4'){
                        address = address + " " + addr.long_name;
                    }
                    else if (addr.types[0] == 'sublocality_level_3'){
                        address = address + " " + addr.long_name;
                    }
                    else if (addr.types[0] == 'sublocality_level_2'){
                        address = address + " " + addr.long_name;
                    }
                    else if (addr.types[0] == 'sublocality_level_1'){
                        address = address + " " + addr.long_name;
                    }
                    else if (addr.types[0] == 'sublocality'){
                        address = address + " " + addr.long_name;
                    }
                    else if (addr.types[0] == 'route'){// address 2
                        address = address + " " + addr.long_name;
                    }

                    else if (addr.types[0] == 'postal_code'){// Zip
                        zip = addr.short_name;
                    }

                    else if (addr.types[0] == ['administrative_area_level_1'] || addr.types[0] == ['administrative_area_level_2']){ // State
                        state = ""+state+" "+addr.long_name;
                    }

                    else if (addr.types[0] == ['locality']){// City
                        city = addr.long_name;
                    }

                    //document.getElementById('country_short').value = country_short;
                    document.getElementById('store_postal_code').value = zip;
                    document.getElementById('store_country').value = country;
                    document.getElementById('store_state').value = state;
                    document.getElementById('store_ctiy').value = city;

                }

                //alert(formatted_Address);
                if (formatted_Address.indexOf(document.getElementById('street_number')) < 0 && formatted_Address.indexOf(document.getElementById('route')) < 0) {
                   // document.getElementById('store_address').value = "";

                    //setTimeout(function(){ document.getElementById('address').value = address;},500);
                }

            }




            function address_validate() {
                var country_short;


                var address = '';

                var store_address =   $.trim($("#store_address").val());
                var store_address2 =   $.trim($("#store_address2").val());
                var city =   $.trim($("#store_city").val());
                var state =  $.trim($("#store_state").val());
                var postal_code =  $.trim($("#store_postal_code").val());
                var country =   $.trim($("#store_country").val());

                if(store_address!=''){
                    address+=store_address;
                }
                if(store_address2!=''){
                    if(address!=''){
                        address+=','+store_address2;
                    } else {
                        address+=store_address2;
                    }
                }
                if(city!=''){
                    if(address!=''){
                        address+=','+city;
                    } else {
                        address+=city;
                    }
                }
                if(state!=''){
                    if(address!=''){
                        address+=','+state;
                    } else {
                        address+=state;
                    }
                }
                if(country!=''){
                    if(address!=''){
                        address+=','+country;
                    } else {
                        address+=country;
                    }
                }

                //alert(address);



                geocoder.geocode({'address': address }, function(results, status) {
                    //return false;
                    if(status == 'OK'){
                        for (var i = 0; i < results[0].address_components.length; i++) {
                            var addr = results[0].address_components[i];
                            if (addr.types[0] == 'country'){
                                //                        country = addr.long_name;
                                country_short = addr.short_name;
                                //                        alert(country_short);
                                //                        alert(results[0].geometry.location.lat());
                                //                        alert(results[0].geometry.location.lng());
                                //document.getElementById('country_short').value = country_short;
                                if(document.getElementById("store_lat").value==''){
                                    document.getElementById("store_lat").value = results[0].geometry.location.lat();
                                } 
                                if(document.getElementById("store_long").value==''){
                                    document.getElementById("store_long").value = results[0].geometry.location.lng();
                                }
                                //checkformsubmit = true;

                                //$("#store_form").submit();
                                return true;
                            }
                        }
                    }
                    if (status == 'ZERO_RESULTS') {
                        alert('Could not found proper address.');
                        return false;
                    } else {

                        alert("An error occured while validating this address");
                        return false;
                    }
                    //return true;
                });

                // check if this entry in address_components has a type of country


            }


            $(document).ready(function(){
                $("#store_address2,#store_city,#store_state,#store_country").on('blur',function(){
                    blurit();
                });
            });

            //Manual address fill by user.
            function blurit(){
		
                var country_short;

                var address = '';

                var latitude = $.trim($("#store_lat").val());
                var longitude = $.trim($("#store_long").val());
               // var store_address =   $.trim($("#store_address").val());
                var store_address2 =   $.trim($("#store_address2").val());
                var city =   $.trim($("#store_city").val());
                var state =  $.trim($("#store_state").val());
                var postal_code =  $.trim($("#store_postal_code").val());
                var country =   $.trim($("#store_country").val());

                if(store_address!=''){
                    address+=store_address;
                }
                if(store_address2!=''){
                    if(address!=''){
                        address+=','+store_address2;
                    } else {
                        address+=store_address2;
                    }
                }
                if(city!=''){
                    if(address!=''){
                        address+=','+city;
                    } else {
                        address+=city;
                    }
                }
                if(state!=''){
                    if(address!=''){
                        address+=','+state;
                    } else {
                        address+=state;
                    }
                }
                if(country!=''){
                    if(address!=''){
                        address+=','+country;
                    } else {
                        address+=country;
                    }
                }

                //alert(address);




                geocoder.geocode({
                    'address': address
                }, function(results, status) {
                    //return false;
                    // alert(status);
                    if(status == 'OK'){

                        for (var i = 0; i < results[0].address_components.length; i++) {
                            var addr = results[0].address_components[i];
                            // alert(addr.types[0]);
                            if (addr.types[0] == 'country'){

                                country_short = addr.short_name;

                                // document.getElementById('country_short').value = country_short;



                                //alert(document.getElementById('latitude').value);
                                // alert(document.getElementById('longitude').value);
                                $("#store_lat").val(results[0].geometry.location.lat());
                                $("#store_long").val(results[0].geometry.location.lng());




                                var lat = results[0].geometry.location.lat();
                                var lon = results[0].geometry.location.lng();

                                var myLatlng = new google.maps.LatLng( lat, lon);




                                map = new google.maps.Map(document.getElementById('map-canvas'), {
                                    zoom: 14,
                                    center: myLatlng,
                                    mapTypeId: google.maps.MapTypeId.HYBRID
                                });

                                marker = new google.maps.Marker({
                                    position: myLatlng,
                                    map: map,
                                    draggable : true
                                });

                                //User direct change marker pin.
                                google.maps.event.addListener(marker, 'dragend', function() {
                                    geocodePosition(marker.getPosition());
                                });



                            }
                        }
                    }
                    else if (status == 'ZERO_RESULTS') {
                        alert('Enter valid street address.');
                        return false;
                    } else {
                        alert("An error occured while validating this address");
                        return false;
                    }
                    //return true;
                });
            }



            function geocodePosition(pos)
            {
                geocoder = new google.maps.Geocoder();
                geocoder.geocode
                ({
                    latLng: pos
                },
                function(results, status)
                {
                    if (status == google.maps.GeocoderStatus.OK)
                    {

                        //alert(results[0].formatted_address);
                        document.getElementById("store_lat").value = results[0].geometry.location.lat();
                        document.getElementById("store_long").value = results[0].geometry.location.lng();

                    }
                    else
                    {

                    }
                }
            );
            }




        </script>
