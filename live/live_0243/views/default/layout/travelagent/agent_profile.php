<script type="application/javascript">

function append_div2()
{
	var tmp_div2 = document.createElement("div");
	tmp_div2.className = "";								
	
	var glry_cnt=document.getElementById('glry_cnt').value;
	
	if(glry_cnt<5)
	{
		document.getElementById('glry_cnt').value=parseInt(glry_cnt)+1;
		var num=parseInt(glry_cnt)+1;
		
		tmp_div2.id='galry'+num; 
		
		var content='<label>Document '+num+'</label><div class="textbox">';		
		content=content + document.getElementById('more2').innerHTML;
		var str = '</div><div onclick="remove_div('+num+')" class="fr" style="text-align:left;font-weight:bold;cursor:pointer;color:#990000;padding-top: 27px;">Remove</div><div class="clear"></div><div class="clear"></div>';			
		tmp_div2.innerHTML = content +str;	
	
		document.getElementById('add_more2').appendChild(tmp_div2);
		
		if(parseInt(glry_cnt)+1>=5)
		{
			document.getElementById('addimg').style.display='none';
		}
	}
	else
	{
		document.getElementById('addimg').style.display='none';
	}
							
	
}


function remove_div(id)
{						
		var element = document.getElementById('galry'+id);
		var add_more = parent.document.getElementById('add_more2');
		
		var add_parent=add_more.parentNode.offsetHeight;			
		var remove_height=parseInt(element.offsetHeight)+20;		
		var final_height=add_parent - remove_height;	
		
		element.parentNode.removeChild(element);
		add_more.parentNode.style.height = final_height+'px';
		
		var glry_cnt=document.getElementById('glry_cnt').value;
		document.getElementById('glry_cnt').value=parseInt(glry_cnt)-1;
			
		document.getElementById('addimg').style.display='block';
	
	}


function submitattachment_valid()
{

        var chks = document.getElementsByName('file_up[]');
 
        var hasChecked = false;
		
		var chekattch =0;
     
	 
	 		   <?php 						
						if($agent_document){						
							foreach($agent_document as $doc){							
								if(file_exists(base_path().'upload/agent_doc/'.$doc->agent_document)) { ?>
								
								var chekattch =1;	
						<?php break;  } } } ?>
						
						
						
        for (var i = 0; i < chks.length; i++)
        {
                if (chks[i].value=='')
                {
                       
					   if(chekattch==0)
					   {					
							 check=false;
							var dv = document.getElementById('err1');
							
							dv.className = "errmsgcl";
							dv.style.margin = "10px";
							dv.innerHTML ='<p>Please Add your Id Proof.</p>';
							dv.style.display='block';						
							hasChecked = true;       
						}	                 
						
                }
				else 
				{						
						value = chks[i].value;
						t1 = value.substring(value.lastIndexOf('.') + 1,value.length);
						if( t1=='jpg' || t1=='jpeg' || t1=='gif' || t1=='png' || t1=='JPEG' || t1=='JPG'  ||  t1=='PNG' || t1=='GIF' || t1=='pdf' || t1=='PDF')
						{
							document.getElementById('err1').style.display='none';
							check=true;
						}
						else
						{							
							check=false;
							var dv = document.getElementById('err1');
							
							dv.className = "errmsgcl";
							dv.style.margin = "10px";
							dv.innerHTML = '<p>Attachment type is not valid.</p>';
							dv.style.display='block';
							hasChecked = true;							
										
						}
				
				}
								
        }

		if(check==false)
		{
			return false;
		}
		else { return true; }

	 
}


</script>



	<div id="bidcontener"><!--start_#bidcontener-->
                
		<div class="bid_form_center">
		
        	<div class="left_side">
			
				<div class="describe_requirements">
					<h1>Edit Agent Profile</h1>
				</div>
				
				<!--show error or update messages start-->
				
				<?php if($error!=''){ ?>
				
					<div class="errmsgcl" id="error" style="margin-top:10px;">
						<?php  echo $error; ?>
					</div>
					
				<?php 
					}  
					
					if($msg!=''){
				
  			   		 	if($msg == 'update'){
				?>
				
					<div class="msgsuccess" id="error" style="margin-top:10px;">
						<p><?php  echo "Agent Profile Updated successfully"; ?></p>   
					</div>
					
				<?php }
				
					if($msg == 'create'){
				?>
				
					<div class="msgsuccess" id="error" style="margin-top:10px;">
						<p><?php  echo "Agent Profile Created successfully"; ?></p>   
					</div>
					
				<?php }
				
				 }?>
				
				
				<div id="err1" style="display:none;"></div>
				
				<div class="clear"></div> 
				
               <!--show error or update messages end-->  
			        
			   <div class="edt_account">
			   
					<label class="rt"><span class="fontred">*</span>Required Fields</label><div class="clear"></div>
					
					 <?php
						$attributes = array('name'=>'editForm','id'=>'editForm','class'=>'form_design','onsubmit'=>'return submitattachment_valid()');
						echo form_open_multipart('travelagent/profile',$attributes);
					?>

					
					

					<label>Agency Name<span class="fontred">*</span></label>
					<div class="textbox"><input type="text" placeholder="Agency Name" name="agency_name" id="agency_name" value="<?php echo $agency_name;?>" /></div>
					<div class="clear"></div>
					
					
					<label>Contact Person Name<span class="fontred">*</span></label>
					<div class="textbox"><input type="text" placeholder="Contact Person" name="contact_person_name" id="contact_person_name" value="<?php echo $contact_person_name;?>" /></div>
					<div class="clear"></div>
					
					<label>Location<span class="fontred">*</span></label>
					<div class="textbox"><input type="text" placeholder="Austin, TX" name="agency_location" id="agency_location" value="<?php echo $agency_location;?>" /></div>
					<div class="clear"></div>
					
				
					
					<label>Support E-mail</label>
					<div class="textbox"><input type="text" placeholder="email@address.com" id="agency_contact_email" name="agency_contact_email" value="<?php echo $agency_contact_email;?>" /></div>
					<div class="clear"></div>
                            
					<label >Phone no.<span class="fontred">*</span></label>
					<div class="textbox"><input type="text" placeholder="989898989898" name="agency_phone" id="agency_phone" value="<?php echo $agency_phone;?>" /></div>
					<div class="clear"></div>
                            
					<label>Customer Support</label>
					<div class="textbox"><input type="text" placeholder="9898989898" name="agency_customer_support_no" id="agency_customer_support_no" value="<?php echo $agency_customer_support_no;?>" /></div>
					<label>Please share 24/7 Customer suppport detail that help traveller during travelling.</label>
                    <div class="clear"></div>

					<label>Website</label>
					<div class="textbox"><input type="text" placeholder="www.elenonwork.com" name="agency_website"  value="<?php if($agency_website == '0'){}  else{ echo $agency_website; }?>"/></div>
                    <div class="clear"></div>
                    
                     <div style="margin:0 0 0 176px; width:400px;">
							<?php 
							if($member_association)
							{
								foreach($member_association as $ma)
								{?>
                                <div style="min-width:100px; display:inline-block;">
                               <div class="checkbox"><input type="checkbox" name="agent_member_of_association[]" value="<?php echo $ma->member_association_id;?>" id="<?php echo $ma->member_association_id;?>" <?php if($agent_member_of_association){if(in_array($ma->member_association_id,$agent_member_of_association)){?> checked="checked"<?php }}?>/></div>
                            <label style="float:left; width:93px; height:20px; margin-left:5px; margin-top:3px; color:#636363; font-size:14px; font-weight:bold; font-family:Arial;"><?php echo $ma->member_association;?></label>	
                            </div>
								<?php }
								
							}
							?>
                          </div>  
					
					 <div class="clear"></div>
					 
					 
					<div id="add_more2" style="display:block;">  
						<label id="document">Document 1</label>
						<div class="textbox" id="more2"><input type="file" placeholder=""  name="file_up[]" /></div><div class="clear"></div>
					</div>
			
					 <div id="addimg" style="background:url(<?php echo base_url().$theme; ?>/images/add_more.png) no-repeat; margin-left: 170px; height: 30px;">
						 <a href="javascript:void(0)" onclick="append_div2();" style="font-weight: bold; font-size: 12px; color: white; padding-left: 30px; line-height:26px;">Add More</a>
					</div>
					   <div class="clear"></div>
					
					<?php 
						$dcnt=1;
						
						if($agent_document){
						
							foreach($agent_document as $doc){
							
								if(file_exists(base_path().'upload/agent_doc/'.$doc->agent_document)) {
					
					?>
        	
           			<label style="height:5px;">&nbsp;</label> <div class="accr lt"><?php echo $dcnt.')&nbsp;&nbsp;';?><a target="_blank" href="<?php echo base_url();?>upload/agent_doc/<?php echo $doc->agent_document; ?>"><?php echo $doc->agent_document; ?></a></div>
               <div class="clear"></div>
            
					<?php $dcnt++;   
								}
							}
						}
					?>
					<input type="hidden" name="glry_cnt" id="glry_cnt" value="<?php echo $dcnt; ?>" />
 					
					<input type="hidden" value="<?php echo $user_id; ?>" id="user_id" name="user_id">
					<input type="hidden" value="<?php echo $agent_id; ?>" id="agent_id" name="agent_id">
					<div class="btn fr" style="margin-right:175px; margin-top:10px;">              
						<input type="submit" value="Update" id="updatebtn" name="updatebtn">
					</div>
                           
					
					</form>

			   </div>
               <div class="clear"></div>
			   
			</div>
            
			<?php echo $this->load->view($theme.'/layout/user/user_sidebar'); ?>

		</div>
        <div class="clear"></div>
		
	</div>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>  
<script> 
	var options = {		
	types: ['(cities)']
	//componentRestrictions: {country: 'us'}
	};
	
	var agency_location = new google.maps.places.Autocomplete($("#agency_location")[0], options);
	google.maps.event.addListener(agency_location, 'place_changed', function() {
		var place_start = agency_location.getPlace();
		console.log(agency_location.address_components);
	}); 
</script>