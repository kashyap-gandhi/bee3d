<script>
	$(function() {
		$( "#tab_prd" ).tabs();
		$( "#tab_prd_sec" ).tabs();
	});
</script>

	<div id="bidcontener"><!--start_#bidcontener-->
	
		<div class="bid_form_center">
		
			<div class="left_side">
			
				<div class="main_run" id="tab_prd"> 
                    <div class="running_tab" id="">
                       <ul>
                         <li><?php echo anchor('myoffer/all','All Trips');?></li>
                         <li><?php echo anchor('myoffer/open','Open Trips');?></li>
                         <li><?php echo anchor('myoffer/assigned','Assigned Trips');?></li>
                         <li><?php echo anchor('myoffer/completed','Completed Trips','id="active"');?></li>
						 <li><?php echo anchor('myoffer/close','Close Trips');?></li>
                       </ul>
                    </div><div class="clear"></div>
                    
                    <div id="running">
                    
                         <div class="describe_requirements">
                            <h1>My Completed Offers</h1>
                        </div>
                        
                        <ul class="run_rips">
						
						<?php 
							if($result) {
								foreach($result as $row){
								
									////////======= trip post usser image 
									$user_image= base_url().'upload/no_image.png';
				 
									 if($row->profile_image!='') {  
										if(file_exists(base_path().'upload/user_thumb/'.$row->profile_image)) {
											$user_image=base_url().'upload/user_orig/'.$row->profile_image;	
										}					
									}
									
									////////======= trip status 
									$status = '';
									$trip_activity_status = $row->trip_activity_status;
									
									if($trip_activity_status == 0 && ($row->trip_agent_assign_id == 0 || $row->trip_agent_assign_id == '') && strtotime($row->trip_confirm_date) >= strtotime(date('Y-m-d H:i:s'))){
										$status ='Open';
									}
									elseif($trip_activity_status == 1 && $row->trip_agent_assign_id > 0){
										$status ='Assigned';
									}
									elseif($trip_activity_status == 2 && $row->trip_agent_assign_id > 0 && strtotime($row->trip_confirm_date) <= strtotime(date('Y-m-d H:i:s'))){
										$status ='Completed';
									}
									elseif(($trip_activity_status == 0 || $trip_activity_status == 3) && ($row->trip_agent_assign_id == 0 || $row->trip_agent_assign_id == '') && strtotime($row->trip_confirm_date) <= strtotime(date('Y-m-d H:i:s'))){
										$status ='Closed';
									}
									
									///////======= trip package 
									$package = '';
									
									if($row->package_type == 0){ $package = 'All Packages';}
									if($row->package_type == 1){ $package = 'Family Packages';}
									if($row->package_type == 2){ $package = 'Group Packages';}
									if($row->package_type == 3){ $package = 'Honeymoon';}
									if($row->package_type == 4){ $package = 'Weekend Breakaways';}
									
									///////======= trip month 
									$trip_month ='';
									
									if($row->trip_month == 1) { $trip_month = 'January'; }
									elseif($row->trip_month == 2) { $trip_month = 'February'; }
									elseif($row->trip_month == 3) { $trip_month = 'March'; }
									elseif($row->trip_month == 4) { $trip_month = 'April'; }
									elseif($row->trip_month == 5) { $trip_month = 'May'; }
									elseif($row->trip_month == 6) { $trip_month = 'June'; }
									elseif($row->trip_month == 7) { $trip_month = 'July'; }
									elseif($row->trip_month == 8) { $trip_month = 'August'; }
									elseif($row->trip_month == 9) { $trip_month = 'September'; }
									elseif($row->trip_month == 10) { $trip_month = 'October'; }
									elseif($row->trip_month == 11) { $trip_month = 'November'; }
									elseif($row->trip_month == 12) { $trip_month = 'December'; }
									else{ $trip_month = 'January'; }
									
									
						?>
								<li>
								   <div class="runner_img">
									 <?php echo anchor('user/'.$row->profile_name,'<img src="'.$user_image.'" alt="" />');?>
								   </div>
								   
								   <div class="runner_sec">
									 <?php echo anchor('from/'.$row->trip_from_place.'/to/'.$row->trip_to_place.'/'.$row->trip_id,$row->trip_from_place.' > '.$row->trip_to_place);?>
									 
									 <p>Special Comments :
										 <span style="font-weight:normal;"><?php 
											$trip_special_comment= $row->trip_special_comment;		
											$trip_special_comment=str_replace('KSYDOU','"',$trip_special_comment);
											$trip_special_comment=str_replace('KSYSING',"'",$trip_special_comment);
			
											$strlen = strlen($trip_special_comment);
											if($strlen > 30) { echo substr($trip_special_comment,0,30).' ...';}
											else { echo $trip_special_comment; } 
										?></span>
									 </p>
									 <p>Packages : <span style="font-weight:normal;"><?php echo $package;?></span></p>
									 <p>Month : <span style="font-weight:normal;"><?php echo $trip_month;?></span></p>
									 <p>Nights/Day : <span style="font-weight:normal;"><?php echo $row->trip_nights.' Nights / '.$row->trip_days.' Days';?></span></p>
									 <p>Rooms : <span style="font-weight:normal;"><?php echo $row->total_room.' Rooms';?></span></p>
									 <p>Adults : <span style="font-weight:normal;"><?php echo $row->total_adult.' Adults';?></span></p>
									 <p>Children : <span style="font-weight:normal;"><?php echo $row->total_child.' Children';?></span></p>
									 <p>Trip Places : <span style="font-weight:normal;"><?php echo $row->total_child.' Children';?></span></p>
									 
								   </div>
								   
								   <div class="runner_third">
									 <ul class="ulnobor">
									  <li>
									   <span class="color"> Now you can start work.</span>
									   <p class="simple_col">Trip Status: <font color="#E66000"><?php echo $status;?></font></p>
									   
									   <p class="simple_col">Posted : <font color="#686868" style="font-size:12px;"><?php echo date($site_setting->date_time_format,strtotime($row->trip_added_date)); ?></font></p>
								
										<?php 
											if(($trip_activity_status == 1) || ($trip_activity_status == 2)) {
		
												if($row->trip_agent_assign_id!='' && $row->trip_agent_assign_id > 0) { 
												
													$asssign_user= get_user_profile_by_id($row->trip_agent_assign_id);
													//$asssign_user = $this->mytrip_model->get_offer_final_price($row->trip_id,$row->trip_agent_assign_id);
													
										?>
										
											<p class="simple_col">Assigned To : <font color="#0979B2"><?php if($asssign_user){ echo anchor('user/'.$asssign_user->profile_name,$asssign_user->first_name.' '.substr($asssign_user->last_name,0,1),'class="fpass"'); } ?></font></p>
											
									   <?php } if($trip_activity_status == 1) { ?>
									   
											<p class="simple_col">Assigned : <font color="#686868" style="font-size:12px;"><?php echo date($site_setting->date_time_format,strtotime($row->trip_assign_date)); ?></font></p>
											
										<?php }  if($trip_activity_status == 2) { ?>
										
											<p class="simple_col">Completed : <font color="#686868" style="font-size:12px;"><?php echo date($site_setting->date_time_format,strtotime($row->trip_completed_date)); ?></font></p>
											
										<?php }  } ?>
								
								

									  
								
									  </li>
									 </ul>
								   </div><div class="clear"></div>
								   
								   <div class="bot">
									 <div class="days"><p><?php echo getDuration($row->trip_added_date);?></p></div>
									 
									 <div class="right">
									   <a class="conversation_grn">Conversation</a>
									   <span class="tsk_price"><?php echo 'Task Price:'.$row->currency_symbol.$row->trip_min_budget.' - '.$row->currency_symbol.$row->trip_max_budget; ?></span>
									   <a class="conversation">Dispite</a>

									  	 <span class="tsk_price"><?php echo 'Final Price:'.$row->currency_symbol.$row->final_offer_amount; ?></span>
									   
									 </div>
								   </div>
                        	 </li>
						<?php
								
								}
							} else { echo '<li><center><b>No Record Found</b></center></li>'; }
						 
						 ?>
                       </ul>
                        
                        </div>
						
						
						<?php if($total_rows>10) { ?>
							<div class="gonext">
								<?php echo $page_link; ?>
							</div>
						<?php } ?>
 
                </div>
				<div class="clear"></div>
			  
			</div>
			
			<?php echo $this->load->view($theme.'/layout/user/user_sidebar'); ?>
			
		</div>
        <div class="clear"></div>
		
     </div>
	 
