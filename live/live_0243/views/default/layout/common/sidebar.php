<?php
$active_design='';
$active_transaction='';
$active_wallet='';
$active_video='';
$active_package='';
$active_credit='';
$active_design_favourite='';
$active_video_favourite='';
$active_video_download='';
$active_design_download='';
$active_3dprinter='';
$active_challenge = '';
$active_mychallenge='';
if($active_menu == "3ddesign")
{
	$active_design = 'class="parent_active"';
}
if($active_menu == "transaction")
{
	$active_transaction = 'class="parent_active"';
}
if($active_menu == "wallet")
{
	$active_wallet = 'class="parent_active"';
}
if($active_menu == "video")
{
	$active_video = 'class="parent_active"';
}
if($active_menu == "mypackage")
{
	$active_package = 'class="parent_active"';
}
if($active_menu == "buycredit")
{
	$active_credit = 'class="parent_active"';
}

if($active_menu == "favourite_design")
{
	$active_design_favourite = 'class="parent_active"';
}

if($active_menu == "favourite_video")
{
	$active_video_favourite= 'class="parent_active"';
}
if($active_menu == "videodownload")
{
	$active_video_download= 'class="parent_active"';
}
if($active_menu == "designdownload")
{
	$active_design_download= 'class="parent_active"';
}
if($active_menu == "3dprinter")
{
	$active_3dprinter= 'class="parent_active"';
}
if($active_menu == "3dchallenge")
{
	$active_challenge = 'class="parent_active"';
}
if($active_menu == "3dchallenge_running")
{
	$active_mychallenge = 'class="parent_active"';
}

?>
<nav id="sidebar">
					
					<ul id="text_nav_v" class="side_text_nav">
						<li <?php echo $active_design;?>>
							<a href="<?php echo site_url('design/all');?>"><span class="icon-picture"></span>Design Hive</a>
							<!--<ul>
								<li class="link_active"><a href="<?php //echo site_url('design/manage_3d_design');?>">Design Hive</a></li>
							</ul>-->
						</li>
						<li <?php echo $active_video;?>>
							<a href="<?php echo site_url('video/all');?>"><span class="icon-play-circle"></span>Videos</a>
						</li>
						<li <?php echo $active_challenge;?> <?php echo $active_mychallenge;?>>
							<a href="javascript:void(0)"><span class="icon-tasks"></span>Challenge</a>
							<ul>
								<li <?php echo $active_challenge;?>><a href="<?php echo site_url('challenge/all');?>">Your challenges</a></li>
								<li <?php echo $active_mychallenge;?>><a href="<?php echo site_url('challenge/running');?>">Applied challenges</a></li>
							</ul>
						</li>
						
						
						<li <?php echo $active_3dprinter;?>>
							<a href="<?php echo site_url('printer');?>"><span class="icon-map-marker"></span>3D Printers</a>
						</li>
						<li <?php echo $active_transaction;?>>
							<a href="<?php echo site_url('transaction');?>"><span class="icon-file-text-alt"></span>Transaction</a>
						</li>
						<li <?php echo $active_wallet;?>>
							<a href="<?php echo site_url('wallet');?>"><span class="icon-credit-card"></span>Wallet</a>
						</li>
						
						<li <?php echo $active_package;?>>
							<a href="<?php echo site_url('user/mypackage');?>"><span class="icon-trello"></span>My Package</a>
						</li>
						
						<li <?php echo $active_credit;?>>
							<a href="<?php echo site_url('package/all');?>"><span class="icon-bullseye"></span>Buy Credit</a>
						</li>
						<li <?php echo $active_design_favourite;?> <?php echo $active_video_favourite;?>>
							<a href="javascript:void(0)"><span class="icon-heart"></span>My Favourites</a>
							<ul>
								<li <?php echo $active_design_favourite;?>><a href="<?php echo site_url('favorite/design');?>">Design</a></li>
								<li <?php echo $active_video_favourite;?>><a href="<?php echo site_url('favorite/video');?>">Video</a></li>
							</ul>
						</li>
						 
						<li <?php echo $active_video_download;?> <?php echo $active_design_download;?>>
							<a href="javascript:void(0)"><span class="icon-download"></span>My Downloads</a>
							<ul>
								<li <?php echo $active_design_download;?>><a href="<?php echo site_url('user/designdownload');?>">Design</a></li>
								<li <?php echo $active_video_download;?>><a href="<?php echo site_url('user/videodownload');?>">Video</a></li>
							</ul>
						</li>
						
						
						
						
						<!--<li>
							<a href="javascript:void(0)"><span class="icon-th-list"></span>Forms</a>
							<ul>
								<li><a href="form_regular_elements.html">Regular elements</a></li>
								<li><a href="form_extended_elements.html">Extended elements</a></li>
								<li><a href="form_multiupload.html">Multiupload </a></li>
								<li><a href="form_validation.html">Form validation</a></li>
								<li><a href="wizard.html">Wizard</a></li>
								<li><a href="wysiwg.html">WYSIWG Editor</a></li>
							</ul>
						</li>
						<li>
							<a href="javascript:void(0)"><span class="icon-puzzle-piece"></span>Components</a>
							<ul>
								<li><a href="calendar.html">Calendar</a></li>
								<li><a href="charts.html">Charts</a></li>
								<li><a href="contact_list.html">Contact List</a></li>
								<li><a href="file_manager.html">File manager</a></li>
								<li><a href="gallery.html">Gallery</a></li>
								<li><a href="gmaps.html">Google Maps</a></li>
								<li>
									<a href="javascript:void(0)">Tables</a>
									<ul>
										<li><a href="datatables.html">Datatables</a></li>
										<li><a href="regular_tables.html">Regular</a></li>
										<li><a href="slick_grid.html">Slick Grid</a></li>
										<li><a href="table_responsive.html">Responsive Table</a></li>
									</ul>
								</li>
							</ul>
						</li>
						<li>
							<a href="javascript:void(0)"><span class="icon-beaker"></span>UI Elements</a>
							<ul>
								<li><a href="alerts_buttons.html">Alerts, Buttons</a></li>
								<li><a href="grid.html">Grid</a></li>
								<li><a href="icons.html">Icons</a></li>
								<li><a href="notifications.html">Notifications</a></li>
								<li><a href="panels.html">Panels</a></li>
								<li><a href="tabs_accordions.html">Tabs, Accordions</a></li>
								<li><a href="tooltips_popovers.html">Tooltips, Popovers</a></li>
								<li><a href="typography.html">Typography</a></li>
							</ul>
						</li>
						<li>
							<a href="javascript:void(0)"><span class="icon-leaf"></span>Other Pages</a>
							<ul>
								<li><a href="blank.html">Blank page</a></li>
								<li><a href="chat.html">Chat</a></li>
								<li><a href="contact_page.html">Contact Page</a></li>
								<li><a href="error_404.html">Error 404</a></li>
								<li><a href="help_faq.html">Help/Faq</a></li>
								<li><a href="invoices.html">Invoices</a></li>
								<li><a href="login.html">Login Page</a></li>
								<li><a href="mailbox.html">Mailbox</a></li>
								<li><a href="user_profile.html">User profile</a></li>
								<li><a href="settings.html">Site Settings</a></li>
							</ul>
						</li>-->
						
					</ul>
				</nav>