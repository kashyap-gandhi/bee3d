<div class="wrapper row10">
    <div class="container main-contain">
    <div class="login-block">
        <h1 class="text-center font-light white">Reset Password  </h1>
            <div class="login_wrapper">
            <div class="login_panel">
                <!--<div class="login_head">
                        <h1>Log In</h1>
                    </div>-->
                <div class="master-form"> 
                    			<?php
								$attributes = array('name'=>'login_form','id'=>'login_form','class'=>'');
								echo form_open('home/reset_password/'.$user_id.'/'.$code,$attributes);
							?>
<?php if($error!='') { ?>
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php  echo $error; ?>
					
				</div>
  		 <?php  } ?>
                              <div class="form-group">
                                <label for="exampleInputEmail1" class="label-control">New Password :</label>
                                <input type="password" class="form-control" id="new_password" name="new_password" placeholder="Enter New  Password">
                              </div>
                               <div class="form-group">
                                <label for="exampleInputEmail1" class="label-control">Retype New Password  :</label>
                                <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm New Password.">
                              </div>
                            
                              <div class="form-group text-center margin-top-20">
							  <input type="submit" name="submit" value="Submit" class="btn yellow-btn"/>
                              </div>
                              <div class="text-center">
                                 <a href="<?php echo site_url("login") ?>" class="morelink">Forgot password?</a>
                              </div>
                         </form> 
                     </div>
                    
                    </div>
                </div> 
            </div>
        </div>    
    </div>
</div>  

<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/jquery.min.js"></script>