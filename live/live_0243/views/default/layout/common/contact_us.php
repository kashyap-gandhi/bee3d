<div class="wrapper row10">
    <div class="container main-contain">
    <div class="login-block">
        <h1 class="text-center font-light white">CONTACT US</h1>
            <div class="login_wrapper">
            <div class="login_panel">
                <!--<div class="login_head">
                        <h1>Log In</h1>
                    </div>-->
                <div class="master-form"> 
                    			<?php
								$attributes = array('name'=>'login_form','id'=>'login_form','class'=>'');
								echo form_open('home/contactus',$attributes);
							?>
<?php if($error!='') { ?>
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php  echo $error; ?>
					
				</div>
  		 <?php  } ?>
         
         <?php if($msg!='') { ?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php  if($msg == "success"){echo  "Your enquiry sent Successfully." ;}
						?>
					
				</div>
  		 <?php  } ?>
                              <div class="form-group">
                                <label for="exampleInputEmail1" class="label-control">First Name : *</label>
                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter First Name">
                              </div>
							  
							  <div class="form-group">
                                <label for="exampleInputEmail1" class="label-control">Last Name : *</label>
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter Last Name">
                              </div>
							  
							  <div class="form-group">
                                <label for="exampleInputEmail1" class="label-control">Email : *</label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Enter email address">
                              </div>
							  
							  <div class="form-group">
                                <label for="exampleInputEmail1" class="label-control">Subject :</label>
                                <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter subject">
                              </div>
							  
							  
							  
							  <div class="form-group">
                                <label for="exampleInputEmail1" class="label-control">Message :</label>
								<textarea id="message" name="message" class="form-control" cols="20" rows="10" style="height:100px;"></textarea>
                              </div>
							  
                              <div class="form-group text-center margin-top-20">
							  <input type="submit" name="submit" value="Submit" class="btn yellow-btn"/>
                              </div>
                              
                         </form> 
                     </div>
                    
                    </div>
                </div> 
            </div>
        </div>    
    </div>
</div>  

<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/jquery.min.js"></script>