<div class="wrapper row10">
    <div class="container main-contain">
    <div class="login-block">
        <h1 class="text-center font-light white">LOGIN HERE  </h1>
            <div class="login_wrapper">
            <div class="login_panel">
                <!--<div class="login_head">
                        <h1>Log In</h1>
                    </div>-->
                <div class="master-form"> 
                    			<?php
								$attributes = array('name'=>'login_form','id'=>'login_form','class'=>'');
								echo form_open('home/login',$attributes);
							?>
<?php if($error!='') { ?>
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php  echo $error; ?>
					
				</div>
  		 <?php  } ?>
         
         <?php if($msg!='') { ?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php  if($msg == "reset"){echo  "Your Password has been reset Successfully." ;}
						       if($msg =="allready_reset") {echo  "Reset Password link expired" ;}  
							   if($msg =="signup_success") {echo  "You registration process is successfully completed.Please check email for more detail" ;}  
							   if($msg =="verify") {echo  "You have successfully verified your email" ;}         
						?>
					
				</div>
  		 <?php  } ?>
                              <div class="form-group">
                                <label for="exampleInputEmail1" class="label-control">User Name :</label>
                                <input type="text" class="form-control" id="login_email" name="login_email" placeholder="Enter User Name">
                              </div>
                               <div class="form-group">
                                <label for="exampleInputEmail1" class="label-control">Password  :</label>
                                <input type="password" class="form-control" id="login_password" name="login_password" placeholder="Password should have at least 8 characters.">
                              </div>
                              <div class="form-group">
                                   <div class="checkbox">
                                        <label class="label-control">  <input type="checkbox" class="chkbox" id="login_remember" name="login_remember"> Remember me  </label>
                                   </div>     
                              </div>
                              <div class="form-group text-center margin-top-20">
							  <input type="submit" name="submit" value="Submit" class="btn yellow-btn"/>
                              </div>
                              <div class="text-center">
                                 <a href="<?php echo site_url("forget_password") ?>" class="morelink">Forgot password?</a>
                              </div>
                         </form> 
                     </div>
                    
                    </div>
                </div> 
            </div>
        </div>    
    </div>
</div>  

<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/jquery.min.js"></script>