<?php 
	$data = array(
		'facebook'		=> $this->fb_connect->fb,
		'fbSession'		=> $this->fb_connect->fbSession,
		'user'			=> $this->fb_connect->user,
		'uid'			=> $this->fb_connect->user_id,
		'fbLogoutURL'	=> $this->fb_connect->fbLogoutURL,
		'fbLoginURL'	=> $this->fb_connect->fbLoginURL,	
		'base_url'		=> site_url('home/facebook'),
		'appkey'		=> $this->fb_connect->appkey,
	);
	
	$t_setting = twitter_setting();	            
    $f_setting = facebook_setting();
	$google_setting = google_setting();
	$yahoo_setting = yahoo_setting();
	$site_setting = site_setting();
?>


<div class="register_right">
	<?php if(($t_setting->twitter_login_enable == '1') || ($f_setting->facebook_login_enable == '1') || ($google_setting->google_enable == '1') || ($yahoo_setting->yahoo_enable == '1')){ ?>
	
		<h2>Use another account</h2><div class="clear"></div>
		<hr/><div class="clear"></div>
		<div class="sharebuttons">
			<?php if($t_setting->twitter_login_enable == '1'){ ?>
				 <div class="twitterbtn"><em></em><?php echo anchor('home/twitter_auth/','Twitter');?></div>
			<?php } ?>
		   
		   <?php if($google_setting->google_enable == '1'){ ?>
				<div class="googleplusbtn"><em></em><a onClick="window.parent.location.href='<?php echo site_url('home/google_signin');?>'" href="javascript:void(0)">Google</a></div>
		   <?php } ?>
		   
			<?php if($f_setting->facebook_login_enable == '1'){ ?>
				<div class="facebookbtn"><em></em> <?php echo anchor($data['fbLoginURL'],'Facebook');?></div>
			<?php } ?>
		   
		   <?php if($yahoo_setting->yahoo_enable == '1'){ ?>
				<div class="yahoobtn"><em></em><a onClick="window.parent.location.href='<?php echo site_url('home/yahoo_signin');?>'" href="javascript:void(0)">Yahoo</a></div>
		   <?php } ?>
		   
		</div><div class="clear"></div>
	
	<?php } ?>
     <h2 style="padding-bottom:0; margin-bottom:0;">Do more with <?php echo $site_setting->site_name; ?></h2><div class="clear"></div>
   
    <hr /><div class="clear"></div>
 <div class="dmore">
        <ul>
            <li>Privacy controls for your images</li><div class="clear"></div>
            <li>Delete and edit your uploaded images</li>
            <li>Create fast and easy photo albums</li>
            <li>Get link codes fast, to any of your images</li>
            <li>Have complete control over your images</li>
            <li>Add captions and titles to your images</li>
            <li>Make your account public, or keep it secret</li>
            <li>Place captions on images in the <a href="<?php echo site_url('gallery'); ?>">Gallery</a></li>
            <li>Like or Dislike images in the <a href="<?php echo site_url('gallery'); ?>">Gallery</a></li>
             <li>Even more with an <a href="<?php echo site_url('register/upgrade'); ?>"><?php echo $site_setting->site_name; ?> Pro Account</a></li>
        </ul>
    </div>
</div>