<script type="text/javascript" src="<?php echo base_url().getThemeName(); ?>/js/bootstrap.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().getThemeName(); ?>/css/bootstrap.css" />
<script type="text/javascript">
 function chek_mail(email)
 {
			if(email=='') { return false; }
			var strURL='<?php echo site_url('home/email_check_ajax/');?>/'+email;
			var xmlhttp;
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			
			}
			xmlhttp.onreadystatechange=function()
				{
				
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					
					if(xmlhttp.responseText=='exist')
					{
						document.getElementById("email").value='';
						document.getElementById("email").focus();
						document.getElementById("err_mail_exist").style.display='block';
					}
					else
					{
						document.getElementById("err_mail_exist").style.display='none';
					}
				}
			}
			xmlhttp.open("GET",strURL,true);
			xmlhttp.send();
		
	 }
</script>
<div class="contener">   
         <div class="tab_cont">
			<div class="main">
				<ul class="tabs1" id="tabs">
					<!--<li><a class="tab1" href="<?php //echo site_url('home/register');?>"><span>Personal Information</span></a></li>-->
					<li  class="active"><a class="tab2" href="<?php echo site_url('home/registeragent');?>"><span>Travel Agent Details</span></a></li>
                    <div class="clear"></div>
				</ul>
                <div class="clear"></div>
                <div id="ptab2" class="tab_content">
                <div id="bidcontener">
                    <div class="bid_form_center">
                    	<div class="agent_regi">
                           	<img src="<?php echo base_url().getThemeName(); ?>/images/agent_img1.jpg" alt="" class="img" />
                           	<h4>Agent Registration</h4>
                        </div>
                        
                         <div class="clear"></div>
                    	
              			<?php if($this->input->post('registeragent')) { if($error!=''){  ?>
                            <div id="error" class="errmsgcl"><ul><?php  echo $error; ?></ul></div>
                         <?php } } ?>
                         <div class="clear"></div>	
                    	<?php
						$attributes = array('name'=>'signupForm','id'=>'signupForm','autocomplete'=>'off');
						echo form_open('home/registeragent/'.$msg,$attributes);
			  			?>
                    		
                        	  <!--<label style="float:left; width:146px; height:32px; margin-left:25px; margin-top:25px; color:#636363; font-size:14px; font-family:Arial;">User Name <span class="fontred">*</span></label>
                            <div class="textbox"><input type="text" placeholder="User Name" name="agentprofilename" id="agentprofilename" value="<?php //echo $agentprofilename;?>" /></div>
                            <div class="clear"></div>-->
                          
                            <label style="float:left; width:146px; height:32px; margin-left:25px; margin-top:25px; color:#636363; font-size:14px; font-family:Arial;">E-mail <span class="fontred">*</span></label>
                            <div class="textbox"><input type="text" placeholder="email@address.com" id="email" name="agentemail" value="<?php echo $agent_email;?>" onchange="chek_mail(this.value);" />
                             <div style="color:#FF0000; display:none; padding:8px;" id="err_mail_exist" >There is an existing account associated with this email.</span></div>	
                            </div>
                            <div class="clear"></div>
                            
                           <label style="float:left; width:146px; height:32px; margin-left:25px; margin-top:25px; color:#636363; font-size:14px; font-family:Arial;">First Name <span class="fontred">*</span></label>
                            <div class="textbox"><input type="text" placeholder="First Name" name="agentfirstname" id="agentfirstname" value="<?php echo $agentfirstname;?>" /></div>
                            <div class="clear"></div>
                            
                              <label style="float:left; width:146px; height:32px; margin-left:25px; margin-top:25px; color:#636363; font-size:14px; font-family:Arial;">Last Name <span class="fontred">*</span></label>
                            <div class="textbox"><input type="text" placeholder="Last Name" name="agentlastname" id="agentlastname" value="<?php echo $agentlastname;?>" /></div>
                            <div class="clear"></div>
                            
                              <label style="float:left; width:146px; height:32px; margin-left:25px; margin-top:25px; color:#636363; font-size:14px; font-family:Arial;">Password <span class="fontred">*</span></label>
                            <div class="textbox"><input type="password"  name="agentpassword" id="agentpassword" value="<?php echo $agentpassword;?>"/></div>
                            <div class="clear"></div>
                            <label style="float:left; width:146px; height:32px; margin-left:25px; margin-top:25px; color:#636363; font-size:14px; font-family:Arial;">Confirm password <span class="fontred">*</span></label>
                            <div class="textbox"><input type="password"  name="agentconfirmpassword" id="agentconfirmpassword" value="<?php echo $agentconfirmpassword;?>"/></div>
                            <div class="clear"></div>
                            
                             <label style="float:left; width:146px; height:32px; margin-left:25px; margin-top:25px; color:#636363; font-size:14px; font-family:Arial;">Agency Name <span class="fontred">*</span></label>
                            <div class="textbox"><input type="text" placeholder="Agency Name" name="agencyname" id="agencyname" value="<?php echo $agencyname;?>" /></div>
                            <div class="clear"></div>
                            <label style="float:left; width:147px; height:32px; margin-left:25px; margin-top:25px; color:#636363; font-size:14px; font-family:Arial;">Contact Person Name <span class="fontred">*</span></label>
                            <div class="textbox"><input type="text" placeholder="Contact Person" name="contactperson" id="contactperson" value="<?php echo $contactperson;?>" /></div>
                            <div class="clear"></div>
                            <label style="float:left; width:146px; height:32px; margin-left:25px; margin-top:25px; color:#636363; font-size:14px; font-family:Arial;">Location <span class="fontred">*</span></label>
                            <div class="textbox"><input type="text" placeholder="Austin, TX" name="agency_location" id="agency_location" value="<?php echo $agency_location;?>" /></div>
                            <div class="clear"></div>
                            
                           
                            
                            <label style="float:left; width:146px; height:32px; margin-left:25px; margin-top:25px; color:#636363; font-size:14px; font-family:Arial;">Support E-mail</label>
                            <div class="textbox"><input type="text" placeholder="email@address.com" id="agent_support_email" name="agent_support_email" value="<?php echo $agent_support_email;?>" /></div>
                            <div class="clear"></div>
                            
                            <label style="float:left; width:146px; height:32px; margin-left:25px; margin-top:25px; color:#636363; font-size:14px; font-family:Arial;">Phone no. <span class="fontred">*</span></label>
                            <div class="textbox"><input type="text" placeholder="9898989898" name="agency_phone" id="agency_phone" value="<?php echo $agency_phone;?>" /></div>
                            <div class="clear"></div>
                            
                            <label style="float:left; width:146px; height:32px; margin-left:25px; margin-top:25px; color:#636363; font-size:14px; font-family:Arial;">Customer Support</label>
                            <div class="textbox"><input type="text" placeholder="9898989898" name="agency_customer_support_no" id="agency_customer_support_no" value="<?php echo $agency_customer_support_no;?>" /></div><label style="float:left; font-size:13px; font-family:Arial; color:#636363; width:346px; height:32px; margin-left:10px; margin-top:25px;">Please share 24/7 Customer suppport detail that help traveller during travelling.</label>
                            <div class="clear"></div>
                            <label style="float:left; width:146px; height:32px; margin-left:25px; margin-top:25px; color:#636363; font-size:14px; font-family:Arial;">Website</label>
                            <div class="textbox"><input type="text" placeholder="www.elenonwork.com" id="agency_website" name="agency_website"  value="<?php echo $agency_website;?>"/></div>
                            <div class="clear"></div>
                            <label style="float:left; width:318px; height:17px; margin-left:180px; margin-top:25px; color:#636363; font-size:14px; font-family:Arial;">Please  select Assocoations you are a member of :</label>
                            <div class="clear"></div>
                            
                            <div style="margin:0 0 0 176px; width:400px;">
							<?php 
							if($member_association)
							{
								foreach($member_association as $ma)
								{?>
                                <div style="min-width:100px; display:inline-block;">
                               <div class="checkbox"><input type="checkbox" name="memberassociate[]" value="<?php echo $ma->member_association_id;?>" id="<?php echo $ma->member_association_id;?>" <?php if($memberassociate){if(in_array($ma->member_association_id,$memberassociate)){?> checked="checked"<?php }}?>/></div>
                            <label style="float:left; width:81px; height:20px; margin-left:5px; margin-top:3px; color:#636363; font-size:14px; font-weight:bold; font-family:Arial;"><?php echo $ma->member_association;?></label>	
                            </div>
								<?php }
								
							}
							?>
                          </div>  
                            
                             <div class="clear"></div>
                            
                          
                           
                           <div style="padding:15px 0 0 175px;">
                         
                            <?php	
							if($site_setting->captcha_enable==1)
							{
								if($this->session->userdata('register34Agent')=='') { 
								echo recaptcha_get_html($site_setting->captcha_public_key, $error);
								}
							}
							?> 
                       
                            </div>	
                             <div class="checkbox" style="margin-left:171px; margin-top:20px; margin-bottom:10px;"><input type="checkbox" name="agree" value="agree"/></div>
                            <label style="float:left;height:20px; margin-top:22px;color:#636363; font-size:14px; font-weight:bold; font-family:Arial;">I accept the Travel Agent agreement which is the <a href="#"><u style="color:#0a7ab3;">terms and conditions</u></a> apply to my use of <a href="#"><u style="color:#0a7ab3;"><?php echo $site_setting->site_name; ?></u></a></label> 
                            <div class="clear"></div>
                          <script type="text/javascript">
							function preview()
							{
								var pre_email = document.getElementById('email').value;
								document.getElementById('pre_email').innerHTML= '&nbsp;&nbsp;'+pre_email;
								
								var pre_name= document.getElementById('agentfirstname').value+' '+document.getElementById('agentlastname').value;
								document.getElementById('pre_name').innerHTML= '&nbsp;&nbsp;'+pre_name;
								
								var agency_name = document.getElementById('agencyname').value;
								document.getElementById('pre_agency_name').innerHTML= '&nbsp;&nbsp;'+agency_name;
								
								var pre_phone = document.getElementById('agency_phone').value;
								document.getElementById('pre_phone').innerHTML= '&nbsp;&nbsp;'+pre_phone;
								
								
								var cnt_per = document.getElementById('contactperson').value;
								document.getElementById('pre_contact_person').innerHTML= '&nbsp;&nbsp;'+cnt_per;
								
								var support_mail = document.getElementById('agent_support_email').value;
								document.getElementById('pre_support').innerHTML= '&nbsp;&nbsp;'+support_mail;
								
								var agency_web = document.getElementById('agency_website').value;
								document.getElementById('pre_website_link').innerHTML= '&nbsp;&nbsp;'+agency_web;
								
								var agency_loc = document.getElementById('agency_location').value;
								document.getElementById('pre_address').innerHTML= '&nbsp;&nbsp;'+agency_loc;
								
							}
                            </script>
                             <div class="checkbox1">
							 	<div class="btn_pre">
                                <a href="#myModal"  role="button" data-toggle="modal" onClick="preview()">Register</a>
                               <!--	<input type="submit" name="registeragent" id="registeragent" value="REGISTER" />-->
                             	<div class="clear"></div>
                             	</div>
                             </div>
                             
  <div class="modal hide fade preview_modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="fr"><a href="#" class="close opacity cls" data-dismiss="modal"></a></div>
  <div class="header_img" style="height:auto;"></div><div class="clear"></div>
  <div class="prv_center">
     <h2>Preview</h2>
  </div><div class="clear"></div>
  
  <div class="prw_main">
    
    <div class="prw_rt" style="width:auto;">
     	
			<div class="bid_page_top" style="width:550px;">
	            
                <div class="bid_page_photo" style="display:inline-block; float:left;">
					<img class="img" alt="" src="<?php echo base_url(); ?>/upload/no_image.png">		
                     
				</div>
              <!--  <div style="margin-left:105px; margin-top:-9px;" class="orenge_box">
						<h6>0</h6>
					</div>-->
				<div class="bid_page_right">
	             	 
                	
                  <div class="bid_page_left" style="width:414px; padding:5px;">
                     <strong>Email &nbsp; :</strong>    
                    <span id="pre_email">&nbsp;&nbsp;</span><br />

                     <div class="clear"></div>
                     
                      <strong>Name &nbsp; :</strong>    
                    <span id="pre_name">&nbsp;&nbsp;</span><br />
                     <div class="clear"></div>
                    
                    <strong>Agency Name &nbsp; :</strong>    
                    <span id="pre_agency_name">&nbsp;&nbsp;</span><br />
                     <div class="clear"></div>
                        
				 	 <strong>Contact Person &nbsp; :</strong>                    
					<span id="pre_contact_person">&nbsp;&nbsp;</span><br />
                      <div class="clear"></div>
                     
                      <strong>Support&nbsp; :</strong>
                      <span class="support" id="pre_support">&nbsp; &nbsp;(24X7, 365 days - help) for travel</span><br />
                        <div class="clear"></div> 
                        
                         <strong>Website &nbsp; :</strong>
                     	<span class="support" id="pre_website_link">&nbsp; &nbsp;</span><br />
                        <div class="clear"></div> 
                        
                         <strong>Phone &nbsp; :</strong>
                     	<span class="support" id="pre_phone">&nbsp; &nbsp;</span><br />
                        <div class="clear"></div>  
                        
                        <strong>Address &nbsp; :</strong>
                        <span class="support" id="pre_address"></span><br />
                         <div class="clear"></div>  
                   </div>                 
				</div>
				
			</div>
				
				
				
			<div class="clear"></div>
			</div>
           
            <div class="clear"></div>
            <div style="margin:5px 0 0 227px;display:inline-block; width:60%;"> 
				<div class="btn_pre" style="float:left;">
            <a href="#myModal"  role="button" data-dismiss="modal">Edit</a>
            <div class="clear"></div>
            
            </div>
            	<div class="edt_btn_trp">	
              <input type="submit" name="registeragent" id="registeragent" value="Complete" />	
  			</div>
            
           </div>
          
            
       
      
       
       
       
    </div>
       <div class="bottom_back"></div>
       
  </div>
  
  
  
  
</div>
                             
                            <div class="bottom_div">
                            	<div class="bottom_div_left">
                                	<p>© Copyright 2011. Powered by Rockers Technologies</p>
                                </div>
                                
                                

                                <div class="bottom_div_right">
                                	<img src="<?php echo base_url().getThemeName(); ?>/images/paypal.png" alt="" class="img" />
                                    <img src="<?php echo base_url().getThemeName(); ?>/images/visa.png" alt="" class="img" />
                                    <img src="<?php echo base_url().getThemeName(); ?>/images/mastercard.png" alt="" class="img" />
                                    <img src="<?php echo base_url().getThemeName(); ?>/images/currus.png" alt="" class="img" />
                                    <img src="<?php echo base_url().getThemeName(); ?>/images/2co.png" alt="" class="img" />
                                    <img src="<?php echo base_url().getThemeName(); ?>/images/discover.png" alt="" class="img" />
                                </div>
                               
                           </div>
                           
                           
        <script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>  
          <script> 
		var options = {		
		types: ['(cities)']
		  //componentRestrictions: {country: 'us'}
		};
	
	   var agency_location = new google.maps.places.Autocomplete($("#agency_location")[0], options);
        google.maps.event.addListener(agency_location, 'place_changed', function() {
                var place_start = agency_location.getPlace();
                console.log(agency_location.address_components);
            }); 
			</script>
                    	</form>
                    </div>
                    </div>
              </div>      
              </div>
			</div>
            </div>
           
           
            