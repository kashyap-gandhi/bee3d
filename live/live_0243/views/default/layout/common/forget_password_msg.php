<?php $site_setting=site_setting(); ?>
<style>
		body {padding:80px 0 0}
		textarea, input[type="password"], input[type="text"], input[type="submit"] {-webkit-appearance: none}
		.navbar-brand {font:300 15px/18px 'Roboto', sans-serif}
		.login_wrapper {position:relative;width:380px;margin:0 auto}
		.login_panel {background:#f8f8f8;padding:20px;-webkit-box-shadow: 0 0 0 4px #ededed;-moz-box-shadow: 0 0 0 4px #ededed;box-shadow: 0 0 0 4px #ededed;border:1px solid #ddd;position:relative}
		.login_head {margin-bottom:20px}
		.login_head h1 {margin:0;font:300 20px/24px 'Roboto', sans-serif}
		.login_submit {padding:10px 0}
		.login_panel label a {font-size:11px;margin-right:4px}
		.align-center {text-align:center !important;}
		.sucess-msg {color:#090;}
		@media (max-width: 767px) {
			body {padding-top:40px}
			.navbar {display:none}
			.login_wrapper {width:100%;padding:0 20px}
		}
	</style>
	<br/>
<div class="wrapper" style="min-height:250px;">
		<h4 class="align-center sucess-msg">We sent you an e-mail with instructions for password recovery. Please, check your email..!!</h4>

			<h4 class="align-center sucess-msg">If you don't receive email then Check your spam folder, or contact  webmaster..</h4>
	</div>
	
