<?php 
$user_info = get_user_profile_by_id(get_authenticateUserID());
$user_notification = get_user_unread_notification(get_authenticateUserID());
?>
<header id="top_header">
	<div class="container">
		<div class="row">
		
			<div class="col-xs-6 col-sm-2">
				<a href="<?php echo site_url('home');?>" class="logo_main" title="Ebro Admin Template:"><img src="<?php echo base_url().getThemeName()?>/images/footer-logo.png" height="50"/></a>
			</div>
			
			<div class="col-sm-push-6 col-sm-3 text-right ">
		<div class="notification_dropdown">
					<a href="<?php echo site_url('user/notifications');?>" class="notification_icon">
						<?php if($user_notification)
						{?>
						<span class="label label-danger"><?php echo $user_notification;?></span>
						<?php }?>
						<i class="icon-comment-alt icon-2x"></i>
					</a>
			</div>		
				<div class="notification_separator"></div>	
				<?php /*<div class="notification_separator"></div>
				<div class="notification_dropdown dropdown">
					<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
						<span class="label label-danger">12</span>
						<i class="icon-envelope-alt icon-2x"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-wide">
						<li>
							<div class="dropdown_heading">Messages</div>
							<div class="dropdown_content">
								<ul class="dropdown_items">
									<li>
										<h3><a href="#">Lorem ipsum dolor sit amet</a></h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
										<p class="large_info">Sean Walter, 24.05.2014</p>
										<i class="icon-exclamation-sign indicator"></i>
									</li>
									<li>
										<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam assumenda&hellip;</p>
										<p class="large_info">Sean Walter, 24.05.2014</p>
									</li>
									<li>
										<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
										<p>Lorem ipsum dolor sit amet, consectetur&hellip;</p>
										<p class="large_info">Sean Walter, 24.05.2014</p>
										<i class="icon-exclamation-sign indicator"></i>
									</li>
								</ul>
							</div>
							<div class="dropdown_footer">
								<a href="#" class="btn btn-sm btn-default">Show all</a>
								<div class="pull-right dropdown_actions">
									<a href="#"><i class="icon-refresh"></i></a>
									<a href="#"><i class="icon-cog"></i></a>
								</div>
							</div>
						</li>
					</ul>
				</div><?php */?>
				
			</div>
			<?php
				 $user_image= base_url().'upload/no_image.png';
				 if($user_info->profile_image!='') {  
					if(file_exists(base_path().'upload/user/'.$user_info->profile_image)) {
						$user_image=base_url().'upload/user/'.$user_info->profile_image;	
					}					
				}
				?>
			<div class="col-xs-6 col-sm-push-4 col-sm-3">
				<div class="pull-right dropdown">
					<a href="#" class="user_info dropdown-toggle" title="Jonathan Hay" data-toggle="dropdown">
						<img src="<?php echo $user_image;?>" alt="">
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo site_url('user/profile');?>">Profile</a></li>
						<li><a href="<?php echo site_url('user/change_password');?>">Change Password</a></li>
						<li><a href="<?php echo site_url('home/logout');?>">Log Out</a></li>
					</ul>
				</div>
			</div>
			<!--<div class="col-xs-12 col-sm-pull-6 col-sm-4">
				<form class="main_search">
					<input type="text" id="search_query" name="search_query" class="typeahead form-control">
					<button type="submit" class="btn btn-primary btn-xs"><i class="icon-search icon-white"></i></button>
				</form> 
			</div>-->
		</div>
	</div>
</header>						
<nav id="top_navigation">
	<div class="container">
		<ul id="icon_nav_h" class="top_ico_nav clearfix">
			<li>
				<a href="<?php echo site_url('user/dashboard');?>">
					<i class="icon-home icon-2x"></i>
					<span class="menu_label">Home</span>
				</a>
			</li>
			<!--<li>             
				<a href="#">
					<i class="icon-edit icon-2x"></i>
					<span class="menu_label">Learning Hive</span>
				</a>
			</li>-->
			<li>             
				<a href="<?php echo site_url('design/all');?>">
					<i class="icon-picture icon-2x"></i>
					<span class="menu_label">Design Hive</span>
				</a>
			</li>
			<li>             
				<a href="<?php echo site_url('video/all');?>">
					<!--<span class="label label-danger">12</span>-->
					<i class="icon-play-circle icon-2x"></i>
					<span class="menu_label">Videos</span>
				</a>
			</li>
			
			<li>             
				<a href="<?php echo site_url('challenge/all');?>">
					<!--<span class="label label-danger">12</span>-->
					<i class="icon-tasks icon-2x"></i>
					<span class="menu_label">Challenge Hive</span>
				</a>
			</li>
			
			<li>             
				<a href="<?php echo site_url('printer');?>">
					<i class="icon-map-marker icon-2x"></i>
					<span class="menu_label">3D Printers</span>
				</a>
			</li>
			
			<li>             
				<a href="<?php echo site_url('transaction');?>">
					<!--<span class="label label-danger">12</span>-->
					<i class="icon-file-text-alt icon-2x"></i>
					<span class="menu_label">Transaction</span>
				</a>
			</li>
			
			<li>             
				<a href="<?php echo site_url('wallet');?>">
					<!--<span class="label label-danger">12</span>-->
					<i class="icon-credit-card icon-2x"></i>
					<span class="menu_label">Wallet</span>
				</a>
			</li>
			
			<li>             
				<a href="<?php echo site_url('user/mypackage');?>">
					<!--<span class="label label-danger">12</span>-->
					<i class="icon-trello icon-2x"></i>
					<span class="menu_label">My Package</span>
				</a>
			</li>
			
			<li class="active">             
				<a href="<?php echo site_url('package/all');?>">
					<!--<span class="label label-danger">12</span>-->
					<i class="icon-bullseye icon-2x"></i>
					<span class="menu_label">Buy Credit</span>
				</a>
			</li>
			
						<!--class="active"-->
			
		</ul>
	</div>
</nav>
