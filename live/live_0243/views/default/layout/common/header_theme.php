	<!-- bootstrap framework-->
		<link rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/fronttheme/bootstrap/css/bootstrap.min.css">
	<!-- todc-bootstrap -->
		<link rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/fronttheme/css/todc-bootstrap.min.css">
	<!-- font awesome -->        
		<link rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/fronttheme/css/font-awesome/css/font-awesome.min.css">
	<!-- flag icon set -->        
		<link rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/fronttheme/img/flags/flags.css">
	<!-- retina ready -->
		<link rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/fronttheme/css/retina.css">	
	
	<!-- aditional stylesheets -->
        <!-- vector map -->
			<link rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/jvectormap/jquery-jvectormap-1.2.2.css">

	<!-- ebro styles -->
		<link rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/fronttheme/css/style.css">
	<!-- ebro theme -->
		<link rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/fronttheme/css/theme/color_8.css" id="theme">

		
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="css/ie.css">
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/ie/html5shiv.js"></script>
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/ie/respond.min.js"></script>
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/ie/excanvas.min.js"></script>
	<![endif]-->

	<!-- custom fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/jquery.min.js"></script>
	<!-- bootstrap framework -->
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/bootstrap/js/bootstrap.min.js"></script>
