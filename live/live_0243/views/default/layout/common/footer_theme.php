 <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        &copy; <?php echo date('Y');?> iHive 3D
                    </div>
                    <div class="col-sm-8">
                        <ul>
                            <li><a href="<?php echo site_url('user/dashboard');?>">Dashboard</a></li>
                            <li>&middot;</li>
                            <li><a href="<?php echo site_url('home/content/terms-of-use');?>">Terms of Service</a></li>
                            <li>&middot;</li>
                            <li><a href="<?php echo site_url('home/content/privacy-policy');?>">Privacy Policy</a></li>
                            <li>&middot;</li>
                            <li><a href="<?php echo site_url('home/contactus');?>">Contact us</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-1 text-right">
                        <small class="text-muted">v1.0</small>
                    </div>
                </div>
            </div>
        </footer>
        
	<!-- jQuery -->
		
	<!-- jQuery resize event -->
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/jquery.ba-resize.min.js"></script>
	<!-- retina ready -->
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/jquery_cookie.min.js"></script>
	<!-- retina ready -->
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/retina.min.js"></script>
	<!-- tinyNav -->
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/tinynav.js"></script>
	<!-- sticky sidebar -->
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/jquery.sticky.js"></script>
	<!-- Navgoco -->
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/navgoco/jquery.navgoco.min.js"></script>
	<!-- jMenu -->
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/jMenu/js/jMenu.jquery.js"></script>
	<!-- typeahead -->
        <script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/typeahead.js/typeahead.min.js"></script>
        <script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/typeahead.js/hogan-2.0.0.js"></script>
	
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/ebro_common.js"></script>


		<!-- peity (small charts) -->
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/peity/jquery.peity.min.js"></script>
		<!-- vector map -->
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
		<!-- charts -->
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/flot/jquery.flot.min.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/flot/jquery.flot.pie.min.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/flot/jquery.flot.time.min.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/flot/jquery.flot.tooltip.min.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/flot/jquery.flot.resize.js"></script>
		<!-- easy pie chart -->
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/easy-pie-chart/jquery.easy-pie-chart.js"></script>
			
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/pages/ebro_dashboard1.js"></script>
			<!-- responsive lightbox -->

<!-- mixItUp -->
<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/mixitup/jquery.mixitup.min.js"></script>

<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/magnific-popup/jquery.magnific-popup.min.js"></script>

<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/pages/ebro_gallery.js"></script>



	<!--[if lte IE 9]>
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/ie/jquery.placeholder.js"></script>
		<script>
			$(function() {
				$('input, textarea').placeholder();
			});
		</script>
	<![endif]-->
	
    <!-- style switcher -->
		<?php /*?><div id="style_switcher" class="switcher_open">
            <a href="#" class="switcher_toggle"><i class="icon-cog"></i></a>
			<div class="style_items">
				<p class="style_title">Theme</p>
				<ul class="clearfix" id="theme_switch">
					<li class="style_active" style="background:#48ac2e" title="color_1">Color 1</li>
					<li style="background:#993399" title="color_2">Color 2</li>
					<li style="background:#168cbe" title="color_3">Color 3</li>
					<li style="background:#d61110" title="color_4">Color 4</li>
					<li style="background:#e96710" title="color_5">Color 5</li>
					<li style="background:#262626" title="color_6">Color 6</li>
					<li style="background:#01aaad" title="color_7">Color 7</li>
					<li style="background:#9c5100" title="color_8">Color 8</li>
					<li style="background:#e31a8f" title="color_9">Color 9</li>
					<li style="background:#ffbb0e" title="color_10">Color 10</li>
					<li style="background:#79be0b" title="color_11">Color 11</li>
					<li style="background:#887171" title="color_12">Color 12</li>
					<li style="background:#28abe2" title="color_13">Color 13</li>
					<li style="background:#2f7138" title="color_14">Color 14</li>
					<li style="background:#ce4627" title="color_15">Color 15</li>
					<li style="background:#693986" title="color_16">Color 16</li>
					<li style="background:#7f8c8d" title="color_17">Color 17</li>
					<li style="background:#2c3e50" title="color_18">Color 18</li>
					<li style="background:#34495e" title="color_19">Color 19</li>
					<li style="background:#1abc9c" title="color_20">Color 20</li>
				</ul>
			</div>
			<div class="style_items" id="sidebar_switch">
				<p class="style_title">Sidebar position</p>
				<label class="radio-inline">
					<input type="radio" name="sidebar_position" id="sidebar_left" value="left" checked> Left
				</label>
				<label class="radio-inline">
					<input type="radio" name="sidebar_position" id="sidebar_right" value="right"> Right
				</label>
			</div>
			<div class="style_items" id="layout_switch">
				<p class="style_title">Layout</p>
				<select name="layout_style" id="layout_style" class="form-control">
					<option value="boxed" class="hidden-sm hidden-md">Boxed</option>
					<option value="fixed">Fixed</option>
					<option value="full_width" class="hidden-sm hidden-md">Full Width</option>
				</select>
			</div>
			<div class="style_items" id="style_pattern">
				<p class="style_title">Pattern (boxed layout)</p>
				<ul class="clearfix">
					<li class="pattern_active" style="background:url(img/patterns/pattern_1.png) no-repeat 0 0" title="pattern_1">Pattern 1</li>
					<li style="background:url(img/patterns/pattern_2.png) no-repeat 0 0" title="pattern_2">Pattern 2</li>
					<li style="background:url(img/patterns/pattern_3.png) no-repeat 0 0" title="pattern_3">Pattern 3</li>
					<li style="background:url(img/patterns/pattern_4.png) no-repeat 0 0" title="pattern_4">Pattern 4</li>
					<li style="background:url(img/patterns/pattern_5.png) no-repeat 0 0" title="pattern_5">Pattern 5</li>
					<li style="background:url(img/patterns/pattern_6.png) no-repeat 0 0" title="pattern_6">Pattern 6</li>
					<li style="background:url(img/patterns/pattern_7.png) no-repeat 0 0" title="pattern_7">Pattern 7</li>
					<li style="background:url(img/patterns/pattern_8.png) no-repeat 0 0" title="pattern_8">Pattern 8</li>
					<li style="background:url(img/patterns/pattern_9.png) no-repeat 0 0" title="pattern_9">Pattern 9</li>
					<li style="background:url(img/patterns/pattern_10.png) no-repeat 0 0" title="pattern_10">Pattern 10</li>
				</ul>
			</div>
			<div class="text-center">
				<button class="btn btn-default" id="style_reset">Reset</button>
			</div>
        </div><?php */?>
	
