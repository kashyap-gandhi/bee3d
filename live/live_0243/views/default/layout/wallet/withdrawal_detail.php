<!-- parsley -->
<script type="text/javascript">
function change_div_method(str)
{
	if(str=='bank')
	{
		document.getElementById('bank_div').style.display='block';
		document.getElementById('check_div').style.display='none';
		document.getElementById('gateway_div').style.display='none';
	}
	if(str=='check')
	{
		document.getElementById('check_div').style.display='block';
		document.getElementById('bank_div').style.display='none';
		document.getElementById('gateway_div').style.display='none';
	}
	if(str=='gateway')
	{
		document.getElementById('gateway_div').style.display='block';
		document.getElementById('bank_div').style.display='none';
		document.getElementById('check_div').style.display='none';
	}
	
}
</script>

<script>
function numericFilter(txb) {	 
	 txb.value = txb.value.replace(/[^0-9\.]/g,'');
}
</script>

		<!-- responsive table -->
<link href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>
		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Wallet</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
						
						<!-- main content -->
						<div class="row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Wallet (<?php echo $total_wallet_amount;	?>)										<span>Authorize Balance :( <?php echo getuserpoints_auth(get_authenticateUserID());	?>)</span>
										<span class="span_right">
										<?php echo anchor('wallet/','Wallet History'.'('.$total_wallet_amount.')','class="fpass"'); ?> &nbsp;|&nbsp;
									  
											<?php if($total_wallet_amount>$wallet_setting->wallet_minimum_amount) { ?>
											
												<?php echo anchor('wallet/withdraw_wallet','Withdraw','class="fpass"'); ?> 											 <?php } ?>
										</span>
										</h4>
									</div>
									<?php 
									if($error != '') { ?>
											<div class="alert alert-danger">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
													<?php  echo $error; ?>
												
											</div>
									 <?php  } ?>
									
									<div class="panel-body">
									<div class="incon">
									
																  
									
									<table width="100%" cellspacing="4" cellpadding="4" border="0">
									<tbody>
									<tr>
										<td width="30%" valign="middle" align="left" class="lab1">Withdraw Amount</td>
										
										<td width="70%" valign="top" align="left"><?php echo $amount; ?></td>
									</tr>
									
									
									<tr>
										<td valign="top" align="left"  class="lab1">Withdraw Method</td>
										<td valign="top" align="left"> 	
										
									<?php if($withdraw_method=='bank') { ?>By Net Banking<?php } 
									 if($withdraw_method=='check') { ?>By Cheque<?php } 
									  if($withdraw_method=='gateway') { ?>By Payment Gateway<?php } ?>
										
										
										</td>
										</tr>        
									</tbody>
									</table>
									
									
									
									<div style="display: <?php if($withdraw_method=='bank') { echo "block"; } else { echo "none"; } ?>;" id="bank_div">
									
									<div id="detail-bg1" class="title2">Bank Detail</div>
									
									<table width="100%" cellspacing="4" cellpadding="4" border="0">
									
									<tbody>
									<tr>
									<td width="30%" valign="middle" align="left" class="lab1">Bank Name</td>
									<td width="70%" valign="top" align="left"><?php echo $bank_name; ?></td>
									</tr>
									
									
									<tr>
									<td valign="middle" align="left" class="lab1">Account Holder Name</td>
									<td valign="top" align="left"><?php echo $bank_account_holder_name; ?></td>
									</tr>
									
									<tr>
									<td valign="middle" align="left" class="lab1">Account Number</td>
									<td valign="top" align="left"><?php echo $bank_account_number; ?></td>
									</tr>
									
									<tr>
									<td valign="middle" align="left" class="lab1">Bank Branch</td>
									<td valign="top" align="left"><?php echo $bank_branch; ?></td>
									</tr>
									
									<tr>
									<td valign="middle" align="left" class="lab1">Bank IFSC Code</td>
									
									<td valign="top" align="left"><?php echo $bank_ifsc_code; ?></td>
									</tr>
									
									<tr>
									<td valign="middle" align="left" class="lab1">Bank Address</td>
									 <td valign="top" align="left"><?php echo $bank_address; ?></td>
									</tr>
									
									<tr>
									<td valign="middle" align="left" class="lab1">Bank City</td>
									<td valign="top" align="left"><?php echo $bank_city; ?></td>
									</tr>
									
									<tr>
									<td valign="middle" align="left" class="lab1">Bank State</td>
									<td valign="top" align="left"><?php echo $bank_state; ?></td>
									</tr>
									
									<tr>
									<td valign="middle" align="left" class="lab1">Bank Country</td>
									<td valign="top" align="left"><?php echo $bank_country; ?></td>
									</tr>
									
									<tr>
									<td valign="middle" align="left" class="lab1">Bank Postal Code</td>
									<td valign="top" align="left"><?php echo $bank_zipcode; ?></td>
									</tr>
									
									</tbody></table>
									
									</div>
									
									
									
									<div style="display: <?php if($withdraw_method=='check') { echo "block"; } else { echo "none"; } ?>;" id="check_div">
									<div  id="detail-bg1" class="title2">Cheque Bank Detail</div>
									<table width="100%" cellspacing="4" cellpadding="4" border="0">
									
									<tbody>
									
									<tr>
									<td width="30%" align="left" valign="middle" class="lab1">Bank Name</td>
									
									<td width="70%" align="left" valign="top"><?php echo $check_name; ?></td>
									</tr>
									
									
									<tr>
									<td valign="middle" align="left" class="lab1">Account Holder Name</td>
									
									<td valign="top" align="left"><?php echo $check_account_holder_name; ?></td>
									</tr>
									
									
									<tr>
									<td valign="middle" align="left" class="lab1">Bank Account Number</td>
									
									<td valign="top" align="left"><?php echo $check_account_number; ?></td>
									</tr>
									
									
									<tr>
									<td valign="middle" align="left" class="lab1">Bank Branch</td>
									
									<td valign="top" align="left"><?php echo $check_branch; ?></td>
									</tr>
									
									
									
									<tr>
									<td valign="middle" align="left" class="lab1">Bank IFSC Code</td>
									
									<td valign="top" align="left"><?php echo $check_unique_id; ?></td>
									</tr>
									
									
									<tr>
									<td valign="middle" align="left" class="lab1">Bank Address</td>
									
									<td valign="top" align="left"><?php echo $check_address; ?></td>
									</tr>
									
									
									
									
									
									
									<tr>
									<td valign="middle" align="left" class="lab1">Bank City</td>
									
									<td valign="top" align="left"><?php echo $check_city; ?></td>
									</tr>
									
									
									<tr>
									<td valign="middle" align="left" class="lab1">Bank State</td>
									
									<td valign="top" align="left"><?php echo $check_state; ?></td>
									</tr>
									
									
									<tr>
									<td valign="middle" align="left" class="lab1">Bank Country</td>
									
									<td valign="top" align="left"><?php echo $check_country; ?></td>
									</tr>
									
									
									<tr>
									<td valign="middle" align="left" class="lab1">Bank Postal Code</td>
									
									<td valign="top" align="left"><?php echo $check_zipcode; ?></td>
									</tr>
									
									</tbody></table>
									</div>
									
									
									<div style="display: <?php if($withdraw_method=='gateway') { echo "block"; } else { echo "none"; } ?>;" id="gateway_div">
									<div id="detail-bg1" class="title2">Payment Gateway Detail</div>
									<table width="100%" cellspacing="4" cellpadding="4" border="0">
									
									<tbody><tr>
									<td width="30%"  align="left" valign="middle" class="lab1">Gateway Name</td>
									<td width="70%"  align="left" valign="top"><?php echo $gateway_name; ?></td>
									</tr>
									
									<tr>
									<td valign="middle" align="left" class="lab1">Gateway Account</td>
									<td valign="top" align="left"><?php echo $gateway_account; ?></td>
									</tr>
									
									<tr>
									<td valign="middle" align="left" class="lab1">Gateway City</td>
									<td valign="top" align="left"><?php echo $gateway_city; ?></td>
									</tr>
									
									
									<tr>
									<td valign="middle" align="left" class="lab1">Gateway State</td>
									
									<td valign="top" align="left"><?php echo $gateway_state; ?></td>
									</tr>
									
									
									<tr>
									<td valign="middle" align="left" class="lab1">Gateway State</td>
									
									<td valign="top" align="left"><?php echo $gateway_country; ?></td>
									</tr>
									
									
									<tr>
									<td valign="middle" align="left" class="lab1">Gateway Postal Code</td>
									
									<td valign="top" align="left"><?php echo $gateway_zip; ?></td>
									</tr>
									
									</tbody></table>
					
                					<div class="clear"></div>
								</div>
							</div>
									</div>

								</div>
							</div>
							
						</div>
						
					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
		</div>
<div id="footer_space"></div>

