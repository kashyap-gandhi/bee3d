	<!-- aditional stylesheets -->
		<!-- responsive table -->
			<link href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>

		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Wallet</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
					
						<!-- main content -->
						<div class="row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Wallet (<?php echo $total_wallet_amount;	?>)								<span>Authorize Balance :( <?php echo getuserpoints_auth(get_authenticateUserID());	?>)</span>
							
								<span class="span_right"><?php echo anchor('package/all','Deposit','class="fpass"'); ?> &nbsp;|&nbsp; 							<?php if($total_wallet_amount>$wallet_setting->wallet_minimum_amount) { ?>
								<?php echo anchor('wallet/withdraw_wallet','Withdraw','class="fpass"'); ?>&nbsp;|&nbsp;							<?php echo anchor('wallet/my_withdraw','Withdraw History','class="fpass"'); ?> 
							  
							 <?php } ?>
</span>
										</h4> 
								
									</div>
									
									<table id="resp_table" class="table toggle-square" data-filter="#table_search" data-page-size="40">
										<thead>
											<tr>
												<th><?php echo 'TYPE';?></th>
												<th>Points</th>
												<th>Pay By</th>
												<!--<th>Transaction ID</th>  -->
												<th>Fees</th>
                                                <th>For</th>
												<th>Date</th>
												<!--<th>Status</th>-->
											</tr>
										</thead>
										<tbody>
											<?php if($wallet_details)
											{
											 $i=0;
												foreach($wallet_details as $ws)
												{
													$cls='debit';
													if($ws->credit>0)
													{
														$cls='credit';				
													}
													if($ws->debit>0)
													{
														$cls='debit';				
													}
													/*if($ws->admin_status=='Review') 
													{
														$cls='review';
													}*/
												?>
											<tr class="<?php echo $cls; ?>">
												<td><?php if($ws->credit>0) { echo 'Credit'; } if($ws->debit>0) { echo 'Dedit';} ?></td>
												<td><?php if($ws->credit>0) { echo "+".number_format($ws->credit,2); } if($ws->debit>0) { echo "-".number_format($ws->debit,2); } ?></td>
												<td><?php if($ws->is_authorize == 1) { echo 'Authorize'; }  else { echo 'Internal'; } ?></td>
												<!--<td><?php //if($ws->transaction_id != ''){echo $ws->transaction_id;}else{echo "N/A	";} ?></td>--><td><?php if($ws->total_cut_price >0) { echo $ws->total_cut_price; }   ?></td>
												<td>
												<?php 
												$trans_detail = get_one_transaction($ws->transaction_id);
												$tree_pay='';
												if($ws->transaction_id > 0 && $ws->transaction_id != '')
												{
													$chk_treepay = check_for_treepay($ws->transaction_id);
													if($chk_treepay)
													{
														$tree_pay = ':Tree Pay';
													}
												}
												
												
												$design_detail ='';
												$package_detail = '';
												$challenge_detail='';
												if($trans_detail)
												{
													if($trans_detail->design_id > 0)
													{
														$design_detail = $this->design_model->get_one_design($trans_detail->design_id);
													}
													
													if($trans_detail->package_id > 0)
													{
															$package_detail = $this->package_model->get_one_package_detail($trans_detail->package_id);
													}
													if($trans_detail->challenge_id > 0)
													{
															$challenge_detail = $this->challenge_model->get_one_challenge($trans_detail->challenge_id);
													}
												}
												if($design_detail)
												{
													echo $design_detail->design_title.$tree_pay;
												}
												
												else if($package_detail)
												{
													echo $package_detail["package_name"].$tree_pay;
												}
												else if($challenge_detail)
												{
													echo $challenge_detail->challenge_title.$tree_pay;
												}
												else
												{
													echo "N/A";
												}
												?>
												</td>
												
												<td><?php echo date($site_setting->date_time_format,strtotime($ws->wallet_date)); ?></td>
												<!--<td align="center" valign="middle"><?php //if($ws->status != ''){echo $ws->status;}else{echo "N/A";}?></td>-->
											</tr>	
											<?php }}
											else
											{?>
											<tr>
												<td colspan="6">No Records Found!!</td>
												
											</tr>
											<?php }?>
											
											
										</tbody>
									
											<tr>
												<td colspan="6" class="text-center">
													<ul class="">
													<?php echo $page_link;?>
													</ul>
												</td>
											</tr>
									
									</table>
								</div>
							</div>
						</div>
						
					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
			<div id="footer_space"></div>
		</div>

       <!-- responsive table -->
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.sort.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.filter.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.paginate.js"></script>

			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/pages/ebro_responsive_table.js"></script>
