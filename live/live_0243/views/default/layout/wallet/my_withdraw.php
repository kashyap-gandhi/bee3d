		<!-- responsive table -->
			<link href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>

		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Wallet</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
					<?php if($msg!='') { 
						if($msg == 'success')
						{?>
					
							<div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "Withdraw successfully."; ?>
								
							</div>
					 <?php  }
					 }
					 ?>
					
						<!-- main content -->
						<div class="row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Wallet History(<?php echo $total_wallet_amount;?>)
										<span>Authorize Balance :( <?php echo getuserpoints_auth(get_authenticateUserID());	?>)</span>
								
										<span class="span_right"><?php echo anchor('wallet/add_wallet','Deposit','class="fpass"'); ?> &nbsp;|&nbsp; 							<?php if($total_wallet_amount>$wallet_setting->wallet_minimum_amount) { ?>
								<?php echo anchor('wallet/withdraw_wallet','Withdraw','class="fpass"'); ?>&nbsp;|&nbsp;							<?php echo anchor('wallet/my_withdraw','Withdraw History','class="fpass"'); ?> 
							  
							 <?php } ?>
</span>
										</h4>
										<?php 
									if($msg=='add' && $msg!=''){?>
									<div class="marTB10" id="success"><p><?php echo 'wallet.index_msg';?></p></div>
									<?php }  if($msg=='fail' && $msg!='') { ?>
									
									<div class="marTB10" id="error"><p><?php echo 'wallet.index_msg2';?></p></div>
									<?php } ?> 
									</div>
									
									<table id="resp_table" class="table toggle-square" data-filter="#table_search" data-page-size="40">
										<thead>
											<tr>
												<th>Points</th>
												<th>Gateway</th>
												<th>Date added</th>
												<th>Status</th>                                     
												<th>Action</th>
										</tr>
										</thead>
										<tbody>
											<?php 
											 
										 if($withdraw_details) {
										 $i=0;
											foreach($withdraw_details as $rs) { 
											if($rs->withdraw_status=='pending')
											{
												$cls='credit';				
											}
											else
											{
												$cls='debit';
											}			
											?>
											<tr class="<?php echo $cls; ?>">
				<td align="left" valign="middle" style="padding-right:8px;"><?php echo number_format($rs->withdraw_amount,2);  ?></td>
				<td align="left" valign="middle" ><?php if($rs->withdraw_method=='bank') { echo 'Net Banking'; } if($rs->withdraw_method=='check') { echo 'Check'; } if($rs->withdraw_method=='gateway') { echo 'Gateway'; } ?>  </td>
				
				
				<td align="left" valign="middle"><?php echo date('d,M Y H:i:s',strtotime($rs->withdraw_date)); ?></td>
				
				
				
				
		<td align="left" valign="middle" style="text-transform:capitalize;"><?php echo $rs->withdraw_status; ?></td>
				
				
				<td align="left" valign="middle"><?php if($rs->withdraw_status=='pending') { echo anchor('wallet/withdraw_details/'.$rs->withdraw_id, 'View','class="fpass"'); } else { echo anchor('wallet/withdrawal_detail/'.$rs->withdraw_id,'View','class="fpass"'); }?></td>
			
				</tr>	
											<?php }}
											else
											{?>
											<tr>
												<td colspan="5">No Records Found!!</td>
												
											</tr>
											<?php }?>
											
											
										</tbody>
									
											
												 <?php //if($total_rows>$limit) { ?>
                                            <tfoot>     
											<tr>
												<td colspan="5" class="text-center">		 
															<ul class="">
															<?php echo $page_link; ?>
															</ul>
																 <div class="clear"></div>
														
											   </td></tr>	<?php //} ?>   

													
													
												</td>
											</tr>
                                            </tfoot>
									
									</table>
								</div>
							</div>
						</div>
						
					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
			<div id="footer_space"></div>
		</div>

       <!-- responsive table -->
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.sort.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.filter.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.paginate.js"></script>

			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/pages/ebro_responsive_table.js"></script>
