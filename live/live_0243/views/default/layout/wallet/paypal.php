<script type="text/javascript">
jQuery(document).ready(function() {	
	
	jQuery("#learnmore").fancybox({
				'titlePosition'		: 'inside',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			});
	
  
});

</script>
		
	<?php echo $this->load->view(getThemeName().'/layout/common/sidemenu');?>
	<div id="content">
	 
	  
			<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-home"></i> <?php echo 'Set up your Paypal E-mail'; ?></h4>
				</div>
			</div>
			
			<div class="row-fluid">
					<div class="span12">
						<div class="box">
							 
							<div class="box-head">
								<i class="icon-list-ul"></i>
								<span><?php echo 'Your Billing Information'; ?></span>
							</div>
							<div class="box-body box-body-nopadding">
							  
							  <?php    if($error != '') { ?>     
								<div class="alert alert-error">
									<?php echo 'There were problems with the following fields:'; ?>
									<?php echo $error; ?>
								</div>
								<?php } elseif($msg=='verify') {  ?>
								<div class="alert alert-info">
									  <?php echo 'Paypal Information Added Successfully.'; ?>
								</div>   
										   
									  <?php } elseif($verify == 0 || $msg=='fail') {  ?>
									   <div class="alert alert-error">
									<?php echo 'Please Verify Your Paypal.'; ?>
									</div>
										   
									  <?php } ?>
      
								<?php
								$site_setting=site_setting();
								$data['site_setting']=$site_setting;
								
								  ?>
								<?php
									$attributes = array('name'=>'frmPaypal','id'=>'frmPaypal','class'=>'form-horizontal form-bordered');
									echo form_open('wallet/verify_paypal',$attributes);
								?>

									<div class="control-group">
										<label for="textfield" class="control-label"><?php echo 'Paypal First Name'; ?></label>
										<div class="controls">
											<div class="input-append">
												<input name="user_name" id="user_name" type="text" class="input-medium" value="<?php echo $user_name;?>" />				</div>
										</div>
									</div>
									<div class="control-group">
										<label for="textfield" class="control-label"><?php echo 'Paypal Last Name'; ?></label>
										<div class="controls">
										<div class="input-append">
												<input name="last_name" id="last_name" type="text" class="input-medium" value="<?php echo $last_name;?>" />
											</div>
										</div>
									</div>
									<div class="control-group">
										<label for="textfield" class="control-label"><?php echo 'Paypal E-mail'; ?></label>
										<div class="controls">

											<div class="input-prepend">
												<span class="add-on">@</span>
												<input name="paypal_email" id="paypal_email" type="text" class="input-large" value="<?php echo $paypal_email;?>" />
											</div>
										</div>
									</div>
									
									<div class="form-actions">
										<input type="submit" class="button button-basic-blue" value="Update">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
		</div>
	<div class="navi-functions">
		<div class="btn-group btn-group-custom">
			<a href="#" class="button button-square layout-not-fixed notify" rel="tooltip" title="Toggle fixed-nav" data-notify-message="Fixed nav is now {{state}}" data-notify-title="Toggled fixed nav">
				<i class="icon-unlock"></i>
			</a>
			<a href="#" class="button button-square layout-not-fluid notify button-active" rel="tooltip" title="Toggle fixed-layout" data-notify-message="Fixed layout is now {{state}}" data-notify-title="Toggled fixed layout">
				<i class="icon-exchange"></i>
			</a>
			<a href="#" class="button button-square toggle-active notify" rel="tooltip" title="Toggle Automatic data refresh" data-notify-message="Automatic data-refresh is now {{state}}" data-notify-title="Toggled automatic data refresh">
				<i class="icon-refresh"></i>
			</a>
			<a href="#" class="button button-square button-active force-last notify-toggle toggle-active notify" rel="tooltip" title="Toggle notification"  data-notify-message="Notifications turned {{state}}" data-notify-title="Toggled notifications">
				<i class="icon-bullhorn"></i>
			</a>
		</div>
	</div>
