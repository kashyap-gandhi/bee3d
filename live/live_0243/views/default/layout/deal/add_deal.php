<script type="text/javascript">
$(document).ready(function(){
$("#deal_amount").keydown(function(event) {
        // Allow: backspace, delete, tab, escape, and enter
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });
});
</script>
<div id="bidcontener"><!--start_#bidcontener-->
                
		<div class="bid_form_center">
		
        	<div class="left_side">
			
            
            
				<div class="describe_requirements">
					<h1><?php if($deal_id!='' && $deal_id>0) {?>Edit<?php } else { ?>Add<?php } ?> Deal</h1>
				</div>
				
				<!--show error or update messages start-->
				
				<?php if($error!=''){ ?>
				
					<div class="errmsgcl" id="error" style="margin-top:10px;">
						<?php  echo $error; ?>
					</div>
					
				<?php 
					}  
					
					if($msg!=''){
				
  			   		 	if($msg == 'update'){
				?>
				
					<div class="msgsuccess" id="error" style="margin-top:10px;">
						<p><?php  echo "Account Updated successfully"; ?></p>   
					</div>
					
				<?php } }?>
				<div class="clear"></div> 
				<script type="text/javascript" >
            
             jQuery(document).ready(function(){ 
					
					$('#btnchangeimage').click(function(){
						 $('#deal_image').trigger('click');						
					});
                }); 
            </script>
                 
               <!--show error or update messages end-->  
			   
                <?php
						$attributes = array('name'=>'dealForm','id'=>'dealForm','class'=>'form_design');
						echo form_open_multipart('deal/adddeal',$attributes);
					?>
                    
             <div class="edt_account">
			   
					<div class="clear"></div>
				    
                     <label>Title</label>
					<div class="textbox"><input type="text" placeholder="Title" name="deal_title" id="deal_title" value="<?php echo $deal_title;?>"></div><div class="clear"></div>
                    
                    
                      <label>Amount</label>
					<div class="textbox"><input type="text" placeholder="Amount" name="deal_amount" id="deal_amount" value="<?php echo $deal_amount;?>"></div><div class="clear"></div>
                    
                    
                      <label>Days</label>
					<div class="textbox"><input type="text" placeholder="2" name="deal_days" id="deal_days" value="<?php echo $deal_days;?>"></div><div class="clear"></div>
                    
                    
                       <label>Image</label>
					
                    
                    <div class="deal-wrapper"> 
                   <input type="file"  name="deal_image" id="deal_image"  value="Find Images" />
                   <span class="button" id="btnchangeimage">Change a Photo</span>
                  </div><div class="clear"></div>
                  
                   <label>&nbsp;</label>
                   
                     <?php
				 $deal_image_url= base_url().'upload/no_deal.png';
		 					
				 if($prev_deal_image!='') {  
					if(file_exists(base_path().'upload/deal/'.$prev_deal_image)) {
						$deal_image_url=base_url().'upload/deal/'.$prev_deal_image;	
					}					
				}
				?>  <div style="width:155px; height:155px; text-align:center;overflow:hidden;" >
                          <img src="<?php echo $deal_image_url; ?>" width="155" />     
                   <input type="hidden" name="prev_deal_image" id="prev_deal_image" value="<?php echo $prev_deal_image;?>" />
                   </div><div class="clear"></div>
                    
                    
                      <label>Summary</label>
					<div class=""><textarea placeholder="Summary" name="deal_summary" id="deal_summary" class="textarea"><?php echo $deal_summary;?></textarea></div><div class="clear"></div>
                    
                    
                    	<label>Description</label>
					<div class=""><textarea placeholder="Description" name="deal_description" id="deal_description" class="textarea"><?php echo $deal_description;?></textarea></div><div class="clear"></div>
                    
                    
                    
                    <div style="margin-right:175px; margin-top:10px;" class="btn fr">  
                     <input type="hidden" name="deal_id" id="deal_id" value="<?php echo $deal_id;?>"/>            
					<input type="submit" name="submit" id="adddeal_btn" value="Submit">
                       
					</div>
                    
                    
                     </div>
               
               
             
                
                </form>
			   
			</div>
            
			<?php echo $this->load->view($theme.'/layout/user/user_sidebar'); ?>

		</div>
        <div class="clear"></div>
		
	</div>
