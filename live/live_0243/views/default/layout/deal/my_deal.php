
<script type="text/javascript">
function delete_deal(id)
{
	var x = confirm('Are you sure to delete this deal?');
	if(x)
	{
		window.location.href='<?php echo site_url('deal/deletedeal')?>'+'/'+id;
	}
	else
	{
		return false;
	}
}
</script>
	<div id="bidcontener"><!--start_#bidcontener-->
    
	
		<div class="bid_form_center">
        <?php 
	if($msg != '') {
			if($msg =='success') {?>     
				 		 <div id="error" class="msgsuccess" style="margin-top:10px;margin-bottom:10px;"><?php  echo "Deal has been post successfully."; ?></div>
                         <div class="clear"></div>
			   <?php }
			   
			   
			   if($msg == 'update')
			   {?>
			   	 <div id="error" class="msgsuccess" style="margin-top:10px;margin-bottom:10px;"><?php  echo "Deal has been update successfully."; ?></div>
                         <div class="clear"></div>
			   <?php }
			   
			   
			    if($msg == 'hotdeal')
			   {?>
			   	 <div id="error" class="msgsuccess" style="margin-top:10px;margin-bottom:10px;"><?php  echo "Deal has been successfully set as Hot Deal."; ?></div>
                         <div class="clear"></div>
			   <?php }
			   
			    if($msg == 'removehot')
			   {?>
			   	 <div id="error" class="msgsuccess" style="margin-top:10px;margin-bottom:10px;"><?php  echo "Deal has been successfully removed from Hot Deal."; ?></div>
                         <div class="clear"></div>
			   <?php }
			   
			   
			    if($msg == 'reachlimit')
			   {?>
			   	 <div id="error" class="msgsuccess" style="margin-top:10px;margin-bottom:10px;"><?php  echo "You have reached your maximum deal limit. Please delete old deal."; ?></div>
                         <div class="clear"></div>
			   <?php }
			   
			   
			   
			   if($msg == 'delete')
			   {?>
               	 <div id="error" class="msgsuccess" style="margin-top:10px;margin-bottom:10px;"><?php  echo "Deal has been delete successfully."; ?></div>
                         <div class="clear"></div>
			   <?php }}
			   ?>
		
			<div class="left_side">
			
				<div class="main_run" id="tab_prd"> 
                    <div id="running">
                    
                         <div class="describe_requirements">
                            <h1>My Deals</h1>
                        </div>
                        
                        <ul class="run_rips">
						
						<?php 
							if($result) {
								foreach($result as $row){
								
									////////======= trip post usser image 
									$user_data = get_user_profile_by_id($row->user_id);
									
									
									 $deal_image_url= base_url().'upload/no_deal.png';
		 					
									 if($row->deal_image!='') {  
										if(file_exists(base_path().'upload/deal/'.$row->deal_image)) {
											$deal_image_url=base_url().'upload/deal/'.$row->deal_image;	
										}					
									}
				
									
									////////======= trip status 
									$status = '';
									$deal_status = $row->deal_status;
									
									if($deal_status == 0){
										$status ='Inactive';
									}
									elseif($deal_status == 1){
										$status ='Pending';
									}
									elseif($deal_status == 2){
										$status ='Accept';
									}
									else{
										$status ='Reject';
									}
									
						?>
								<li>
                                
                               
                                
								   <div class="runner_img">
                                  
									 <?php echo anchor('travelagent/'.$user_data->profile_name,'<img src="'.$deal_image_url.'" alt="" />');?>
								   </div>
								   
								   <div class="runner_sec">
									 <p>Title : <span style="font-weight:normal;"><?php echo $row->deal_title;?></span></p>
									 <p>Summary : <span style="font-weight:normal;"><?php echo $row->deal_summary;?></span></p>
									 <p>Description : <span style="font-weight:normal;"><?php echo $row->deal_description;?></span></p>
									 <p>Amount : <span style="font-weight:normal;"><?php echo $row->deal_amount;?></span></p>
                                     
                                     <p>Days : <span style="font-weight:normal;"><?php echo $row->deal_days;?> Days / <?php echo $row->deal_days+1;?> Nights</span></p>
									 
								   </div>
                                    <div class="runner_third">
									 <ul class="ulnobor">
									  <li>
									   <p class="simple_col">Deal Status: <font color="#E66000"><?php echo $status;?></font></p>
                                       <p class="simple_col">Post Date: <font color="#E66000"><?php echo date($site_setting->date_format,strtotime($row->deal_date));?></font></p>
                                       <?php if($row->deal_update_date != '0000-00-00 00:00:00'){?>
                                       <p class="simple_col">Update Date: <font color="#E66000"><?php echo date($site_setting->date_format,strtotime($row->deal_update_date));?></font></p>
                                       <?php }?>
									
									  </li>
									 </ul>
								   </div>
                                   
								  <div class="clear"></div>
								   <div class="bot">
									 <div class="days"><p><?php echo getDuration($row->deal_date);?></p></div>
                                     <div class="right">
                                     
									 <?php if($row->deal_is_hot==1) { 
									 
									 echo anchor('deal/removehot/'.$row->deal_id,'Hot Deal','class="conversation_blue" style="color:#FFFFFF"'); 
									 } 
									 
									 else { echo anchor('deal/sethotdeal/'.$row->deal_id,'Set As Hot','class="conversation_orange" style="color:#FFFFFF"'); }?>
                                     
                                     <?php echo anchor('deal/editdeal/'.$row->deal_id,'Edit','class="conversation_grn" style="color:#FFFFFF"');?>
									  <a href="javascript://" class="deal_del_btn" onclick="delete_deal(<?php echo $row->deal_id;?>);"> Delete</a>		   
                                       
									 </div>
								   </div>
                        	 </li>
						<?php
								
								}
							} else { echo '<li><center><b>No Record Found</b></center></li>'; }
						 
						 ?>
                       </ul>
                     
                        </div>
						
						
						<?php if($total_rows>10) { ?>
							<div class="gonext">
								<?php echo $page_link; ?>
							</div>
						<?php } ?>
 
                </div>
				<div class="clear"></div>
			  
			</div>
			
			<?php echo $this->load->view($theme.'/layout/user/user_sidebar'); ?>
			
		</div>
        <div class="clear"></div>
		
     </div>
	 
