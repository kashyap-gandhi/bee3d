<!-- Add fancyBox main JS and CSS files -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url().getThemeName(); ?>/js/tag/jquery.tagsinput.css" />
<!-- responsive lightbox -->
<?php /*?> <link rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/magnific-popup/magnific-popup.css">
<?php */?><script type="text/javascript" src="<?php echo base_url()?>tinymce/tinymce.min.js"></script>
	<!-- aditional stylesheets -->
	<script type="text/javascript">
	tinymce.init({selector:'#video_content'});
	function append_div2()
{
	var tmp_div2 = document.createElement("div");
	tmp_div2.className = "";								
	
	var glry_cnt=document.getElementById('glry_cnt').value;
	
	/*if(glry_cnt<5)
	{*/
		document.getElementById('glry_cnt').value=parseInt(glry_cnt)+1;
		var num=parseInt(glry_cnt)+1;
		
		tmp_div2.id='galry'+num;
		var content=document.getElementById('more2').innerHTML;
		var str = '<div onclick="remove_gallery_div('+num+')" style="font-weight:normal;cursor:pointer;color:#ff0000; width:100px;">Remove</div><div class="clear"></div>';			
		tmp_div2.innerHTML = content +str;	
	
		document.getElementById('add_more2').appendChild(tmp_div2);
		
				
	
}


function remove_gallery_div(id)
{						
		var element = document.getElementById('galry'+id);
		var add_more = parent.document.getElementById('add_more2');
		
		element.parentNode.removeChild(element);
	
		
		var glry_cnt=document.getElementById('glry_cnt').value;
		document.getElementById('glry_cnt').value=parseInt(glry_cnt)-1;
			
		document.getElementById('addimg').style.display='block';
	
	}
	
	function show_parent_video()
	{
		$('#parent_video_div').slideToggle();
	}
	
	function show_video(str)
	{
		if(str == 'external')
		{
			$('#external_video').show();
			$('#internal_video').hide();
		}
		else
		{
			$('#internal_video').show();
			$('#external_video').hide();
		}
	}
	
	function delete_design_attachment(id)
	{
		var y = confirm('Are you sure yo delete image?');
		if(y)
		{
			$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>design/delete_design_attachment",         //the script to call to get data          
			data: {attachment_id: id},
		   // data: $("#member_form").serialize(),
			dataType: '',                //data format      
			success: function(data)          //on receive of reply
				{
					if(data == 'success')
					{
						//$('#coupon_gallery_span_'+id).removeClass('img_upload');
						$('#image_attach_'+id).html('');
					}
				} 
			});
		}
		else
		{
			//alert('rr');
		}
	}
	</script>
<!-- parsley -->
		<!-- responsive table -->
			<link href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>
		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Video</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
						
						<!-- main content -->
						<div class="row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title"><?php if($video_id > 0){?>Edit<?php }else{?>Add<?php }?> Video</h4>
									</div>
									<?php if($error!='') { ?>
											<div class="alert alert-danger">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
													<?php  echo $error; ?>
												
											</div>
									 <?php  } ?>
									
									<div class="panel-body">
											<?php
												$attributes = array('name'=>'video_form','id'=>'video_form','class'=>'');
												echo form_open_multipart('video/add_me',$attributes);
											?>
											<div class="form_sep">
												<label for="reg_input_name" class="req">Name</label>
												<input type="text" id="video_title" name="video_title" class="form-control" data-required="true" value="<?php echo $video_title;?>">
											</div>
											<div class="form_sep">
												<label for="reg_input_email" class="req">Detail Description of Video</label>
												<textarea name="video_content" id="video_content" cols="30" rows="4" class="form-control"><?php echo $video_content;?></textarea>
												
											</div>
											
											<div class="form_sep">
												<label for="reg_select" class="req">Video Category</label>
												
												<select name="video_category" id="video_category" class="form-control" data-required="true">
													<option value="">--Select Category--</option>
													<?php 
													if($category)
													{
														foreach($category as $cat)
														{
														?>
														<option value="<?php echo $cat->category_id;?>" <?php if($video_category == $cat->category_id){?> selected="selected"<?php }?>><?php echo $cat->category_name;?></option>
														
														<?php }
														
													}
													?>
												</select>
											</div>
											
											
											<input type="hidden" name="video_type" class="form-control" value="1"/>	
											
											<?php /*?><div class="form_sep">
												<label for="reg_textarea_message">Video Type</label>
												<div class="radio">
												<input type="radio" name="video_type" class="form-control" value="1" onclick="show_video('external');"/>	External
												</div>
												<div class="radio">
												<input type="radio" name="video_type" class="form-control" value="2" onclick="show_video('internal');"/>	Internal
												</div>	
											</div>
											
											<div class="form_sep">
												<label for="reg_textarea_message">Comment Allow</label>
												<div class="radio">
												<input type="radio" name="comment_allow" class="form-control" value="0" <?php if($comment_allow == 0){?> checked="checked"<?php }?>/>	No
												</div>
												<div class="radio">
												<input type="radio" name="comment_allow" class="form-control" value="1" <?php if($comment_allow == 1){?> checked="checked"<?php }?>/>	Yes								</div>	
											</div><?php */?>
											
											<input type="hidden" name="comment_allow" id="comment_allow" value="1"/>
											
											<div class="form_sep" id="internal_video" style="display:none;">
												<label for="reg_textarea_message">Upload Video</label>
												<input type="file" class="form-control" id="video_file" name="video_file" />
												<div class="clear"></div>
											</div>
											
											<div class="form_sep" id="external_video" style="display:block;">
												<label for="reg_textarea_message">3rd Party Video URL</label>
												<input type="text" class="form-control" id="video_url" name="video_url"  value="<?php echo $video_url;?>" />
												<div class="clear"></div>
											</div>
									
									
									<div class="clear"></div>	
											<div class="form_sep">
												<label for="reg_textarea_message" class="req">Video Points</label>
												<input type="text" id="video_point" name="video_point" class="form-control" value="<?php echo $video_point;?>">
											</div>
											
											<?php /*?><div class="form_sep">
												<label for="reg_textarea_message">Video Referal ID</label>
												<input type="text" id="video_ref_id" name="video_ref_id" class="form-control" value="<?php echo $video_ref_id;?>">
											</div><?php */?>
											
											
											
											
											<div class="form_sep">
												<label for="reg_textarea_message">Video is modified</label>
												<input type="checkbox" id="video_is_modified" name="video_is_modified" class="form-control" value="1" onclick="show_parent_video();" <?php if($video_is_modified == 1){?> checked="checked"<?php }?>>
											</div>
											
											<?php 
											if($video_is_modified == 1)
											{
												$prd = 'style="display:block;"';
											}
											else
											{
												$prd = 'style="display:none;"';
											}?>
											<div class="form_sep" id="parent_video_div" <?php echo $prd;?>>
												<label for="reg_select">Parent Video</label>
												<select name="parent_video_id" id="parent_video_id" class="form-control" data-required="true">
													<option value="">--Select Parent Video--</option>
													<?php 
													if($allvideo)
													{
														foreach($allvideo as $dg)
														{?>
														<option value="<?php echo $dg->video_id;?>" <?php if($parent_video_id == $dg->video_id){?> selected="selected"<?php }?>><?php echo $dg->video_title;?></option>
														
														<?php }
														
													}
													?>
												</select>
											</div>
											
											<div class="form_sep">
												<label for="reg_textarea_message">Meata Keywords</label>
												<textarea rows="3" cols="70" class="form-control autosize_textarea" name="video_meta_keyword" id="video_meta_keyword" ><?php echo $video_meta_keyword;?></textarea>
												
											</div>
											
											<div class="form_sep">
												<label for="reg_textarea_message">Meta Description</label>
											<textarea rows="3" cols="70" class="form-control autosize_textarea" name="video_meta_description" id="video_meta_description" ><?php echo $video_meta_description;?></textarea>

											</div>
											
											<div class="form_sep">
												<label for="reg_textarea_message">Video Tag</label>
												<div id="add_more_tag">
											<div id="more_tag">
											<input id="tags_1" type="text" class="tags" name="tag_name" value="<?php echo $video_tag;?>" />
											</div>
											</div>
											<div class="clear"></div>
											</div>
											<div class="form_sep">
											<input type="hidden" name="video_id" id="video_id" value="<?php echo $video_id;?>"/>
											<input type="submit" name="submit" id="submit" value="submit" class="btn btn-default"/>
											</div>	
										</form>
									</div>
								</div>
							</div>
							
						</div>
						
					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
			<div id="footer_space"></div>
			
			<!-- Modal -->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="text-align:center;" >
<div class="modal-body">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <img src="#"/>
</div>
<!--<div class="modal-footer">
<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
</div>-->
</div>
		</div>

       
<script src="<?php echo base_url().getThemeName(); ?>/js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url().getThemeName(); ?>/js/tag/jquery.tagsinput.js"></script>
<?php /*?><!-- mixItUp -->
			<script src="js/lib/mixitup/jquery.mixitup.min.js"></script>
	<!-- responsive lightbox -->
      <script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/magnific-popup/jquery.magnific-popup.min.js"></script>
			
			
<!-- multiple select -->
<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/multiple-select/jquery.multiple.select.js"></script>
<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/pages/ebro_gallery.js"></script>
<?php */?>
       

<script type="text/javascript">
$(document).ready(function(){
$('#tags_1').tagsInput({width:'auto'});

//===== Usual validation engine=====//
	$("#video_form").validate({
		rules: {
			video_title: "required",
			video_point: "required",
			video_content: "required",
			//video_category:"required",
			video_url: {
				required: true,
				url:true
			}
			
		}

	});
	
});	
	
</script>
