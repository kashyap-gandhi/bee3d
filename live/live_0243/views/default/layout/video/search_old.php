<style>
.media img{ height:120px;}
</style>
	<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			//echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<!--<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Dashboard</span></li>						
					</ul>
				</div>
			</section>
                -->
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
				<!--<div id="main_content">-->
					<div class="row">
							<div class="col-sm-12">
							<?php
									$attributes = array('name'=>'video_form','id'=>'video_form','class'=>'');
									echo form_open_multipart('video/search',$attributes);
								?>
								
								<div class="panel panel-default">
								
									<div class="panel-heading">
										<h4 class="panel-title pull-left">Video List</h4>
										<input type="submit" name="submit" id="submit" value="Search" class="pull-right btn btn-default" style="padding-top:7px;" />
										<input type="text" name="keyword" id="keyword" class="pull-right input-small form-control" placeholder="Find..." id="contact_search" value="<?php echo $keyword;?>">
										<select name="category_id" id="category_id" class="pull-right input-small form-control">							<option value="">Category</option>
										<?php 
											if($category)
											{
											foreach($category as $ct)
											{?>
											<option value="<?php echo $ct->category_id;?>" <?php if($category_id> 0){if($category_id == $ct->category_id){?> selected="selected"<?php }}?>> <?php echo $ct->category_name;?></option>
											<?php }
											}?>
										</select>
										
									</div>
								</form>	
									<?php 
									if($result)
									{?>
									
										<div id="contact_list" class="contact_list">
										<ul>
											<li> 
											<!--	<h4 data-contact-filter="company_1">Company 1</h4>-->
												<ul>
												<?php 
												foreach($result as $res)
													{
													?>
													<li>
														<div class="media">
													
															<div class="media-body">
																<p class="contact_list_username"><a href="<?php echo site_url('video/'.$res->video_slug);?>"><?php echo $res->video_title;?></a></p>
																<?php 
																if($res->category_name !='')
																{
																?>
																<p class="contact_list_info"><span class="muted">Category:</span> <?php echo $res->category_name;?></p><?php }?>
																<p class="contact_list_info"><span class="muted">Video Points:</span> <?php echo $res->video_point;?></p>
															</div>
															<?php if(get_authenticateUserID() != $res->user_id)
															{?>
															<div class="pull-right">
															<?php 
															$wallet_amount = getuserpoints(get_authenticateUserID());
															if($wallet_amount > 0)
															{
															?>
																<a class="btn btn-warning" href="<?php echo site_url('video/buy/'.base64_encode($res->video_id));?>">Buy Now</a>
															<?php }
															else
															{
																if(get_authenticateUserID() >0)
																{?>
																<a class="btn btn-warning" href="<?php echo site_url('video/buypoint');?>">Buy Now</a>
																<?php }
																else
																{
																?>
																<a class="btn btn-warning" href="<?php echo site_url('home/login');?>">Buy Now</a>
															<?php }}?>	
															</div>
															<?php }?>
															
														</div>
													</li>
												<?php }?>	
												</ul>
											</li>
										</ul>
								<div class="pull-right" style="width:22%; margin-right:5px;">
							<ul class="">
									<?php echo $page_link;?>
							</ul>		
							<div>
									</div>
									
								<?php 
								}else{?>	
									<div class="contact_list_no_result">
										<div class="alert alert-error text-center">
											No contacts to display.
										</div>
									</div>
								<?php }?>	
								</div>
							</div>
							
						</div>
					<!--</div>	-->
				</div>

				<?php //	echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
			<div id="footer_space"></div>
			
			<!-- Modal -->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="text-align:center;" >
<div class="modal-body">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <img src="#"/>
</div>
<!--<div class="modal-footer">
<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
</div>-->
</div>
		</div>

       
<script src="<?php echo base_url().getThemeName(); ?>/js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url().getThemeName(); ?>/js/tag/jquery.tagsinput.js"></script>
<?php /*?><!-- mixItUp -->
			<script src="js/lib/mixitup/jquery.mixitup.min.js"></script>
	<!-- responsive lightbox -->
      <script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/magnific-popup/jquery.magnific-popup.min.js"></script>
			
			
<!-- multiple select -->
<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/multiple-select/jquery.multiple.select.js"></script>
<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/pages/ebro_gallery.js"></script>
<?php */?>
<script type="text/javascript">
$(document).ready(function(){
$('#tags_1').tagsInput({width:'auto'});

//===== Usual validation engine=====//
	/*$("#design_form").validate({
		rules: {
			design_title: "required",
			design_point: "required",
			design_content: "required"
			
		}

	});*/
	$('li a').click(function(e) {
    $('#myModal img').attr('src', $(this).attr('data-img-url')); 
});
});	
	
</script>
