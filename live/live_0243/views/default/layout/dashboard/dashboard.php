<div id="bidcontener"><!--start_#bidcontener-->

	<div class="bid_form_center">
	
		<div class="left_side">
		
		
			<!--user Inforamtion and Edit Button, Post Trip Button start-->
			<div class="bid_page_top">
			
				<div class="bid_page_right_dashboard">
					<?php 
						$check_user_agent = check_user_agent(get_authenticateUserID());
						if($check_user_agent){
					?>
						<?php /*?><div class="orenge_box" style="margin-left:55px; margin-top:-9px;">
							<h6><?php echo $check_user_agent->agent_level?></h6>
						</div><?php */?>
					<?php } ?>	
					
					
					<div class="bid_page_photo_dashboard">
						<?php 
							////////======= user image 
							$user_image= base_url().'upload/no_image.png';
		 					
							 if($user_info->profile_image!='') {  
								if(file_exists(base_path().'upload/user_thumb/'.$user_info->profile_image)) {
									$user_image=base_url().'upload/user_thumb/'.$user_info->profile_image;	
								}					
							}
							$agent_profile = get_user_agent_detail(get_authenticateUserID());
							if($agent_profile)
							{
								echo anchor('travelagent/'.$user_info->profile_name,'<img src="'.$user_image.'" alt="" class="img"/>');
							}
							else
							{
								echo anchor('user/'.$user_info->profile_name,'<img src="'.$user_image.'" alt="" class="img"/>');
							}
						?>		
					</div>
					
				</div>
				
				<div class="bid_page_left">
					<strong>Name &nbsp; :</strong>                    
					<span>&nbsp;&nbsp;<?php echo ucfirst($user_info->first_name).' '.ucfirst($user_info->last_name);?></span><br>
				 
					<strong>Address &nbsp; :</strong>
					<span class="support" style="margin:0; padding:0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#585858; font-weight:normal; ">&nbsp; &nbsp;<?php echo ucfirst($user_info->user_location);?></span> 
					      <br />
						<strong>Email &nbsp; :</strong>                    
					<span>&nbsp; &nbsp;<?php echo $user_info->email;?></span><br>
				  
				</div>
				
				<div class="button_dashborad">
				  <div class="edt_btn"><?php echo anchor('user/edit','Edit','style="float:right;"')?></div>
                 <?php /*if(!$check_user_agent){?>
				  <div class="edt_btn_trp"><?php echo anchor('trip/post','Post a Trip')?></div>
                 <?php }*/?>
                  
				</div>
				
			<div class="clear"></div>
			</div>
			<!--user Inforamtion and Edit Button, Post Trip Button end-->
			
			
			<!--Drafted Trip listing start-->
			<?php /*if($draft_trips) { ?>
			
				<div class="describe_requirements">
					<h1>Trips in Draft</h1>
				</div><div class="clear"></div>
				
				<div class="taskhist">
				
				  <ul>
					<?php foreach($draft_trips as $draft_trip) { ?>
					
					<li>
					  <div class="taskhleft">
						<div>
						  <?php echo anchor('from/'.$draft_trip->trip_from_place.'/to/'.$draft_trip->trip_to_place.'/'.$draft_trip->trip_id,'Trip: '.$draft_trip->trip_from_place.' > '.$draft_trip->trip_to_place,'class="homepick"');?>
						</div>
						
						<div>
						  <span class="geo">Drafted <?php echo getDuration($draft_trip->trip_added_date);?></span>
						</div>
					  </div>
					  
					  <div class="taskhrig">
					  
					  
					  	<script type="text/javascript">
							function remove_div2(e,id)
							{
								if(id != '' || id != 0){
									//window.location.href = "<?php //echo site_url('/trip/delete_task'); ?>/"+id;
								}
								
								var pid=e.parentNode;
								var i = pid.parentNode;
								i.style.display="none";
							}
						</script> 


						<a class="fr" href="javascript:void();" onclick="remove_div2(this,'<?php echo $draft_trip->trip_id; ?>')">
						  <img src="<?php echo base_url().getThemeName(); ?>/images/close.png" />
						</a><div class="clear"></div>

						<?php echo anchor('trip/post/'.$draft_trip->trip_id,'post it!','id="postit"');?>
					  </div>
					</li>

					<?php } ?>
				  </ul>
				
				</div><div class="clear"></div>
			<?php }*/ ?>
			<!--Drafted Trip listing end-->
			
			
			<!--Last Trip listing start-->
			<?php /*if($last_trips) { ?>
			
				<div class="describe_requirements">
					<h1>Last Trips History</h1>
				</div><div class="clear"></div>
				
				<div class="conserva">
			
					<?php foreach($last_trips as $last_trip){ ?>
					
					  <div class="last_trip">
						 <?php echo anchor('from/'.$last_trip->trip_from_place.'/to/'.$last_trip->trip_to_place.'/'.$last_trip->trip_id,'Trip: '.$last_trip->trip_from_place.' &gt; '.$last_trip->trip_to_place,'class="homepick"');?> <br />
						  <span class="geo"><strong>Posted Date</strong> : <?php echo date($site_setting->date_time_format,strtotime($last_trip->trip_added_date)); ?></span>
						  
						  <?php 
if(($last_trip->trip_activity_status == 1 || $last_trip->trip_activity_status == 2 || $last_trip->trip_activity_status == 3)  && $last_trip->trip_agent_assign_id > 0) {
								
									
									
										$asssign_user= get_user_profile_by_id($last_trip->trip_agent_assign_id);
										$asssign_user = $this->mytrip_model->get_offer_final_price($last_trip->trip_id,$last_trip->trip_agent_assign_id);
										
										if($asssign_user) { 
										
										////////======= user image 
										$offer_user_image= base_url().'upload/no_image.png';
					 
										 if($asssign_user->profile_image!='') {  
											if(file_exists(base_path().'upload/user_thumb/'.$asssign_user->profile_image)) {
												$offer_user_image=base_url().'upload/user_thumb/'.$asssign_user->profile_image;	
											}					
										}
										
										$check_user_agent = check_user_agent($asssign_user->user_id);
										
										
							
						 ?>
							 <span class="geo marL10"><strong>Assigned Date</strong> : <?php echo date($site_setting->date_time_format,strtotime($last_trip->trip_assign_date)); ?></span>
					  	 
					
						   <div class="conservations_dashboard padLT24">
						   
								<div style="float:left;">
							
									<?php echo anchor('user/'.$asssign_user->profile_name,'<img src="'.$offer_user_image.'" alt="" class="img" height="70" width="70" />');?>
									<?php if($check_user_agent) { ?>
										<?php /*?><div class="orenge_box dash_ie">
										<h6><?php echo $check_user_agent->agent_level;?></h6>
										</div><?php ?>
									<?php } ?>
									
								</div>
							
								<div style="width:595px;" class="fl">
								
									<h1><?php echo anchor('user/'.$asssign_user->profile_name,ucwords($asssign_user->first_name.' '.substr($asssign_user->last_name,0,1).'.'));?></h1>
										<?php
										$get_trip_review = get_trip_review($last_trip->trip_id,$last_trip->trip_agent_assign_id);
										
										if($get_trip_review){
									?>
									
									<div class="rating">
										<div class="strmn" style="margin-left:20px;"><div class="str_sel" style="width:<?php if($get_trip_review->user_rating > 5) { ?>100<?php } else { echo round($get_trip_review->user_rating*2);?>0<?php } ?>%;"></div></div>
									
									</div><br />
                                    
                                    <p><?php echo $get_trip_review->user_review;?></p>
                                    
									<?php } ?>  
                                    
                                    
								  
									 
								</div>
							 
							</div>
						
						<?php }  } else { echo ' <div class="conservations_dashboard padLT24" style="width:650px;"></div>'; }?>
						
					</div>
					<div class="clear"></div>
					<?php } ?>

				</div><div class="clear"></div>
			
				<div class="see_all">
					<?php echo anchor('mytrip','See more');?>
				</div>
		   		<div class="clear"></div>
		   <?php }*/ ?>
		   <!--Last Trip listing end-->
			
		
        
        
        <?php $agent_profile = get_user_agent_detail(get_authenticateUserID());
		
		/*if($agent_profile) { ?>
        
        <script src="<?php echo base_url().getThemeName(); ?>/js/jquery-ui-1.9.0.custom.js"></script>
<script>
	$(function() {
		
		
		$(".running_tab ul li:first a").attr('id',"active").show(); //Activate first tab
				
				//On Click Event
				$(".running_tab ul li").click(function(){
					$(".running_tab ul li a").attr("id",''); //Remove any "active" class
					$(this).find("a").attr('id',"active"); //Add "active" class to selected tab
					$(".allhide").hide(); //Hide all tab content
					var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
					$(activeTab).fadeIn(); //Fade in the active content				
					return false;
				});
	});
</script>
        
			<div class="main_run" id="tab_prd"> 
			
            <div class="running_tab" id="">
			   <ul>
				 <li><a href="#today">Today</a></li>
				 <li><a href="#yesterday">Yesterday</a></li>				
			   </ul>
			</div><div class="clear"></div>
		
		
			<!--Today Trips Listing start-->
			<div id="today" class="allhide">
	
				<div class="conserva">
				
					<?php if($today_trips) { 
					
							foreach($today_trips as $today_trip) { 
							
								////////======= trip post usser image 
								$user_image= base_url().'upload/no_image.png';
			 
								 if($today_trip->profile_image!='') {  
									if(file_exists(base_path().'upload/user_thumb/'.$today_trip->profile_image)) {
										$user_image=base_url().'upload/user_thumb/'.$today_trip->profile_image;	
									}					
								}
					?>
				
				   <div class="conservations_dashboard">
						 
					   <div style="float:left;">
							<?php echo anchor('travelagent/'.$today_trip->profile_name,'<img src="'.$user_image.'" alt=""class="img" width="70" height="70" />');?>
							
							<!--<div class="orenge_box dash_ie">
								<h6>15</h6>
							</div>
							-->
						</div>
						<div class="fl" style="width:500px;">
						
						<h1><?php echo anchor('from/'.$today_trip->trip_from_place.'/to/'.$today_trip->trip_to_place.'/'.$today_trip->trip_id,'Trip: '.$today_trip->trip_from_place.' &gt; '.$today_trip->trip_to_place,'class="homepick"');?></h1>
						<p class="geo" >Posted <?php echo getDuration($today_trip->trip_added_date);?>
						
							&nbsp;Posted by&nbsp;&nbsp;<?php echo anchor('travelagent/'.$today_trip->profile_name,'<strong>'.$today_trip->first_name.' '.substr($today_trip->last_name,0,1).'.</strong>','style="color:#53C22F; font-style:normal;"');?></p>
					  
					   
						</div>
						  <div class="edt_btn_dash fl"><?php echo anchor('from/'.$today_trip->trip_from_place.'/to/'.$today_trip->trip_to_place.'/'.$today_trip->trip_id,'Bid Now');?></div>
					</div>
				
					<?php } }?>	
	
				</div><div class="clear"></div>
					<?php if($today_trips) { ?>
				<div class="see_all">
					<?php echo anchor('search','See more');?>
				</div><div class="clear"></div>
                <?php } ?>
				
			</div>
			<!--Today Trips Listing end-->	
		
		
			<!--yesterday Trips Listing start-->
			<div id="yesterday" class="allhide" style="display:none;">

				<div class="conserva">
			
					<?php if($yesterday_trips) { 
					
							foreach($yesterday_trips as $yesterday_trip) { 
							
								////////======= trip post usser image 
								$user_image= base_url().'upload/no_image.png';
			 
								 if($yesterday_trip->profile_image!='') {  
									if(file_exists(base_path().'upload/user_thumb/'.$yesterday_trip->profile_image)) {
										$user_image=base_url().'upload/user_thumb/'.$yesterday_trip->profile_image;	
									}					
								}
					?>
					
					   <div class="conservations_dashboard">
					   
							<div style="float:left;">
								<?php echo anchor('travelagent/'.$yesterday_trip->profile_name,'<img src="'.$user_image.'" alt=""class="img" width="70" height="70" />');?>
							
							<!--	<div class="orenge_box dash_ie">
									<h6>15</h6>
								</div>-->
								
							</div>
							
							<div class="fl" style="width:500px;">
								
								<h1><?php echo anchor('from/'.$yesterday_trip->trip_from_place.'/to/'.$yesterday_trip->trip_to_place.'/'.$yesterday_trip->trip_id,'Trip: '.$yesterday_trip->trip_from_place.' &gt; '.$yesterday_trip->trip_to_place,'class="homepick"');?></h1>
								<p class="geo" >Posted <?php echo getDuration($yesterday_trip->trip_added_date);?>
						
								&nbsp;Posted by&nbsp;&nbsp;<?php echo anchor('travelagent/'.$yesterday_trip->profile_name,'<strong>'.$yesterday_trip->first_name.' '.substr($yesterday_trip->last_name,0,1).'.</strong>','style="color:#53C22F; font-style:normal;"');?></p>
								
							</div>
							
							  <div class="edt_btn_dash fl"><?php echo anchor('from/'.$yesterday_trip->trip_from_place.'/to/'.$yesterday_trip->trip_to_place.'/'.$yesterday_trip->trip_id,'Bid Now');?></div>
						</div>

					<?php } }?>	
			   
				</div><div class="clear"></div>
					<?php if($yesterday_trips) { ?>
				<div class="see_all">
					<?php echo anchor('search/trip','See more');?>
				</div><div class="clear"></div>
				<?php } ?>
			</div>
			<!--yesterday Trips Listing end-->	
		
	   		</div>
            
            <?php }*/ ?>
            
            
		</div>
		
		
		
		
		<?php echo $this->load->view($theme.'/layout/dashboard/dashboard_sidebar'); ?>
		
		
		</div>
	<div class="clear"></div>
</div><!--end_#bidcontener-->
            
        
