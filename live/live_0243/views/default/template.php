<?php $site_setting=site_setting(); ?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name=viewport content="initial-scale=1, minimum-scale=1, width=device-width">
<head>
   <title><?php echo $pageTitle; ?></title>   
    <meta name="description" content="<?php echo $metaDescription; ?>" />
	<meta name="keywords" content="<?php echo $metaKeyword; ?>" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
			<link rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/css/developer.css" id="theme">

    <script type="text/javascript">
		var baseUrl='<?php if(strstr(site_url(),'index.php')) { echo site_url().'/'; } else { echo base_url(); } ?>';
		var baseThemeUrl='<?php echo base_url().getThemeName(); ?>';
	</script>
<!--<script type="text/javascript" src="<?php //echo base_url().getThemeName(); ?>/js/lib/jquery-1.7.1.js"></script>-->
                           
 
</head>
<body class="full_width"> 
<?php 
echo $header; 
echo $banner;
echo $content_center;
echo $content_left;
echo $content_side;
echo $activity_box;
echo $campaign_box;
echo $press_release;
echo $footer_capaign;
echo $footer; 
echo $footer_js; 
?>

</body>
</html>