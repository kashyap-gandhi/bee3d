<?php
class Video extends IWEB_Controller {
	
	/*
	Function name :Dashboard()
	Description :Its Default Constuctor which called when home object initialzie.its load necesary models
	*/
	
	function Video()
	{
		parent::__construct();	
		$this->load->model('home_model');	
		$this->load->model('user_model');	
		$this->load->model('video_model');	
		$this->load->model('wallet_model');	
			
	}
	
	function index($slug='')
	{
		$chkvideo = check_video_exist($slug);
		if(!$chkvideo)
		{
			redirect('home/404');
			
		}
		else
		{
			$video_id = $chkvideo->video_id;
			$video_detail =  $this->video_model->check_exist_video($video_id,0);
			$data = array();
			$data['site_setting']=site_setting();
			$data['active_menu']="Video";
			$theme = getThemeName();
			$this->template->set_master_template($theme .'/template.php');
			
			$data['theme']=$theme;
			$data['allvideo'] = get_modified_video($video_id);
			$data['category'] = get_parent_video_category();
			
			//Set design view count
			$video_view = $this->video_model->set_video_view($video_id);
			
			$one_video = $this->video_model->get_one_video($video_id);
			$get_one_video_tag = $this->video_model->get_one_video_tag($video_id,$video_id);
			
			$data['user_video']= $this->video_model->get_user_video($one_video->user_id,4,$video_id);
			$data['popular_video']= $this->video_model->get_popular_video(4,$video_id);
			
			/*if($get_one_video_tag)
			{
				$video_tag ='';
				foreach($get_one_video_tag as $gt)
				{
					$video_tag .= $gt->tag_name.',';
				}
				
			}*/
			$data['get_one_video_tag']=$get_one_video_tag;
			$data['user_id']=$one_video->user_id;
			$data['video_id']=$one_video->video_id;
			//$data['video_tag'] = substr($video_tag,0,-1);
			$data['video_title']=$one_video->video_title;
			$data['video_url']=$one_video->video_url;
			$data['video_content']=$one_video->video_content;
			$data['category_id']=$one_video->category_id;
			$data['video_point']=$one_video->video_point;
			$data['video_is_modified']=$one_video->video_is_modified;
			$data['video_meta_keyword']=$one_video->video_meta_keyword;
			$data['video_meta_description']=$one_video->video_meta_description;
			$data['parent_video_id']=$one_video->parent_video_id;
			$data['video_date']=$one_video->video_date;
			$data['video_view_count']=$one_video->video_view_count;
			$data['video_like_count']=$one_video->video_like_count;
	
			$meta_setting=meta_setting();
			
			$pageTitle=$one_video->video_title.'-'.$meta_setting->title;
			$metaDescription=$one_video->video_meta_description.'-'.$meta_setting->meta_description;
			$metaKeyword=$one_video->video_meta_keyword.'-'.$meta_setting->meta_keyword;
			
			$this->template->write('pageTitle',$pageTitle,TRUE);
			$this->template->write('metaDescription',$metaDescription,TRUE);
			$this->template->write('metaKeyword',$metaKeyword,TRUE);
			$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
			$this->template->write_view('content_center',$theme .'/layout/video/view_video',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();		
		}
		
	}
	
	function all($limit='20',$offset=0,$msg='')
	{

		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="video";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['msg']=$msg;
		
		$this->load->library('pagination');
		$config['uri_segment']='4';
		$config['base_url'] = base_url().'video/all/'.$limit.'/';
		$config['total_rows']= $this->video_model->get_total_user_video_count(get_authenticateUserID());
	
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->video_model->get_video_result(get_authenticateUserID(),$limit,$offset);
		
		$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$pageTitle='Video - '.$meta_setting->title;
		$metaDescription='Video - '.$meta_setting->meta_description;
		$metaKeyword='Video  - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/video/list_video',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	
	}
	
	function add_me()
	{
		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="video";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$data['category'] = get_parent_video_category();
		
		$data['allvideo'] = get_allvideo();
		$data['category_id']='';
		$data['video_ref_id']='';
		$data['video_link']='';
		$data['video_id']='';
		$data["error"] = "";
		$meta_setting=meta_setting();
		
		if($_POST)
		{
			$this->form_validation->set_rules('video_title', 'Video Title', 'trim|required');
			$this->form_validation->set_rules('video_point', 'Video Point', 'trim|required');
			$this->form_validation->set_rules('video_content', 'Video Content', 'trim|required');
			//$this->form_validation->set_rules('video_category', 'Video Category', 'required');
			$this->form_validation->set_rules('video_url', 'Video URL', 'required|url');
			
			if($this->form_validation->run() == FALSE)
				{
						if(validation_errors())
						{
							$data["error"] = validation_errors();
						}
						else
						{
								$data["error"] = "";
						}
						
						$data['video_title']=$this->input->post('video_title');
						$data['video_content']=$this->input->post('video_content');
						$data['video_category']=$this->input->post('video_category');
						$data['video_point']=$this->input->post('video_point');
						$data['video_ref_id']=$this->input->post('video_ref_id');
						$data['video_is_modified']=$this->input->post('video_is_modified');
						$data['video_meta_keyword']=$this->input->post('video_meta_keyword');
						$data['video_meta_description']=$this->input->post('video_meta_description');
						$data['video_tag']=$this->input->post('video_tag');
						$data['comment_allow']=$this->input->post('comment_allow');
						$data['video_type']=$this->input->post('video_type');
						$data['video_url']=$this->input->post('video_url');
						
						
				}
				else
				{
					
					if($this->input->post('video_id') != '')
					{
						
						$result = $this->video_model->update_video();
						redirect('video/all/20/0/update_video');
					}
					else
					{
						$result = $this->video_model->insert_video();
						redirect('video/all/20/0/success_video');
					}
				}	
		}
		else
		{
			$data['video_title']=$this->input->post('video_title');
			$data['video_content']=$this->input->post('video_content');
			$data['video_category']=$this->input->post('video_category');
			$data['video_point']=$this->input->post('video_point');
			$data['video_ref_id']=$this->input->post('video_ref_id');
			$data['video_is_modified']=$this->input->post('video_is_modified');
			$data['video_meta_keyword']=$this->input->post('video_meta_keyword');
			$data['video_meta_description']=$this->input->post('video_meta_description');
			$data['video_tag']=$this->input->post('video_tag');
			$data['comment_allow']=$this->input->post('comment_allow');
			$data['video_type']=$this->input->post('video_type');
			$data['video_url']=$this->input->post('video_url');
			
		}
		
		$pageTitle='Video - '.$meta_setting->title;
		$metaDescription='Video - '.$meta_setting->meta_description;
		$metaKeyword='Video - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/video/add_video',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	}
	
	function edit_me($video_id='')
	{
		
		if(!check_user_authentication()) {  redirect('login'); } 
		
		$video_detail =  $this->video_model->check_exist_video($video_id,get_authenticateUserID());
		
		if(!$video_detail)
		{
			redirect('video/all');
		}
		
		if($video_id != '' && $video_id > 0)
		{
		
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="video";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$data['allvideo'] = get_modified_video($video_id);
		$data['category'] = get_parent_video_category();
		$data['error']='';
		$one_video= $this->video_model->get_one_video($video_id);
		
		$get_one_video_tag = $this->video_model->get_one_video_tag($video_id);
		$video_tag ='';
		if($get_one_video_tag)
		{
			
			foreach($get_one_video_tag as $gt)
			{
				$video_tag .= $gt->tag_name.',';
			}
			
		}
		
		$data['video_id']=$one_video->video_id;
		$data['video_tag'] = substr($video_tag,0,-1);
		$data['video_title']=$one_video->video_title;
		$data['video_content']=$one_video->video_content;
		$data['video_category']=$one_video->category_id;
		$data['video_point']=$one_video->video_point;
		$data['video_ref_id']=$one_video->video_ref_id;
		//$data['design_by']=$one_video->design_by;
		$data['video_is_modified']=$one_video->video_is_modified;
		$data['video_meta_keyword']=$one_video->video_meta_keyword;
		$data['video_meta_description']=$one_video->video_meta_description;
		$data['parent_video_id']=$one_video->parent_video_id;
		$data['comment_allow']=$one_video->comment_allow;
		$data['video_url']=$one_video->video_url;

		$meta_setting=meta_setting();
		
		$pageTitle='Video - '.$meta_setting->title;
		$metaDescription='Video - '.$meta_setting->meta_description;
		$metaKeyword='Video - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/video/add_video',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	}
		else
		{
			redirect('user/dashboard');
		}
	
	}
	
	
	function delete_me($video_id)
	{
		
		if(!check_user_authentication()) {  redirect('login'); } 
		
		$video_detail =  $this->video_model->check_exist_video($video_id,get_authenticateUserID());
		if(!$video_detail)
		{
			redirect('video/all');
		}
		
		
		if($video_id != '' && $video_id > 0)
		{
			$delete_tag=$this->db->query("delete from ".$this->db->dbprefix('video_tags')." where video_id='".$video_id."'");
			$delete_cat=$this->db->query("delete from ".$this->db->dbprefix('video_category_rel')." where video_id='".$video_id."'");
			
			$video_comment=$this->db->query("delete from ".$this->db->dbprefix('video_comment')." where video_id='".$video_id."'");
			$video_view = $this->db->query("delete from ".$this->db->dbprefix('video_view')." where video_id='".$video_id."'");
			$video_share = $this->db->query("delete from ".$this->db->dbprefix('video_share')." where video_id='".$video_id."'");
			$video_like = $this->db->query("delete from ".$this->db->dbprefix('video_like')." where video_id='".$video_id."'");
			$video_download = $this->db->query("delete from ".$this->db->dbprefix('video_download')." where video_id='".$video_id."'");
			$delete_tag=$this->db->query("delete from ".$this->db->dbprefix('video')." where video_id='".$video_id."'");
			
			redirect('video/all/20/0/delete_video');
		}
		else
		{
			redirect('user/dashboard');
		}
		
	}
	
	
	function search($limit='20',$category_id=0,$keyword='',$offset=0,$msg='')
	{
	
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="video";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['category'] = get_parent_video_category();
		$data['msg']=$msg;
		
		$this->load->library('pagination');
		
		if($_POST)
		{
			
	
			$category_ids = '';
			if(isset($_POST['category'])){ $category_ids =$_POST['category']; };
			$keyword = $_POST['keyword'];
			
			if($keyword == "")
			{
				$keyword = 0;
			}
			if($category_ids)
			{
				$category_id = implode("-",$category_ids);
			}
			else
			{
				$category_id  = 0;
				
			}
			
			$limit = $this->input->post("limit");
		
			$config['uri_segment']='5';
			$config['base_url'] = base_url().'video/search/'.$limit.'/'.$category_id.'/'.$keyword;
			$config['total_rows']= $this->video_model->get_total_category_video_count($category_id,$keyword);
			$config['per_page'] = $limit;		
			$this->pagination->initialize($config);		
			$data['page_link'] = $this->pagination->create_links();
			$data['result'] = $this->video_model->get_category_video_result($limit,$offset,$category_id,$keyword);
			
		}
		else
		{
			//$category_id='';
			//$keyword='';
			
			
			
			
			$config['uri_segment']='5';
			$config['base_url'] = base_url().'video/search/'.$limit.'/'.$category_id.'/'.$keyword;
			$config['total_rows']= $this->video_model->get_total_category_video_count($category_id,$keyword);
			$config['per_page'] = $limit;		
			$this->pagination->initialize($config);		
			$data['page_link'] = $this->pagination->create_links();
			$data['result'] = $this->video_model->get_category_video_result($limit,$offset,$category_id,$keyword);
		}
		
		/*echo "<pre>";
		print_r($data['result']);
		die;*/
		
		
	
		$data['category_id']=$category_id;
		$data['keyword']=$keyword;
		$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$pageTitle='Video - '.$meta_setting->title;
		$metaDescription='Video - '.$meta_setting->meta_description;
		$metaKeyword='Video - '.$meta_setting->meta_keyword;
		
	$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/video/search',$data,TRUE);
	$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	
	}
	
	function add_design_category()
	{
		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3ddesign";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data["error"] = "";
		$data['category'] = get_parent_category();
	
		if($this->input->post('submit')) 
		{
		
				$this->form_validation->set_rules('category_name', 'Category Name', 'trim|required');
				if($this->form_validation->run() == FALSE)
				{
						if(validation_errors())
						{
							$data["error"] = validation_errors();
						}
						else
						{
								$data["error"] = "";
						}
						
						$data['category_parent_id']=$this->input->post('category_parent_id');
						$data['category_name']=$this->input->post('category_name');
						$data['category_description']=$this->input->post('category_description');
						$data['category_status']=$this->input->post('category_status');

				}
				else
				{
					$result = $this->video_model->insert_design_category();
					redirect('design/all/20/0/success');
				}	
		
		}	
		else
		{
			$data['category_name']=$this->input->post('category_name');
			$data['category_description']=$this->input->post('category_description');
		}	
		$pageTitle='3D-Design Hive- '.$meta_setting->title;
		$metaDescription='3D-Design Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-Design Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/design/add_3d_design_category',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	}
	function view_me($video_id='')
	{
		
		//if(!check_user_authentication()) {  redirect('login'); } 
		
		if($video_id == '')
		{
			redirect('home/404');
		}
		$video_detail =  $this->video_model->check_exist_video($video_id,0);
		if(!$video_detail)
		{
			redirect('video/all');
		}
		
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="Video";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$data['allvideo'] = get_modified_video($video_id);
		$data['category'] = get_parent_video_category();
		
		//Set design view count
		$video_view = $this->video_model->set_video_view($video_id);
		
		$one_video = $this->video_model->get_one_video($video_id);
		$get_one_video_tag = $this->video_model->get_one_video_tag($video_id);
		
		
		$data['popular_video']= $this->video_model->get_popular_video(4);
		
		/*if($get_one_video_tag)
		{
			$video_tag ='';
			foreach($get_one_video_tag as $gt)
			{
				$video_tag .= $gt->tag_name.',';
			}
			
		}*/
		$data['get_one_video_tag']=$get_one_video_tag;
		$data['user_id']=$one_video->user_id;
		$data['video_id']=$one_video->video_id;
		//$data['video_tag'] = substr($video_tag,0,-1);
		$data['video_title']=$one_video->video_title;
		$data['video_url']=$one_video->video_url;
		$data['video_content']=$one_video->video_content;
		$data['category_id']=$one_video->category_id;
		$data['video_point']=$one_video->video_point;
		$data['video_is_modified']=$one_video->video_is_modified;
		$data['video_meta_keyword']=$one_video->video_meta_keyword;
		$data['video_meta_description']=$one_video->video_meta_description;
		$data['parent_video_id']=$one_video->parent_video_id;
		$data['video_date']=$one_video->video_date;
		$data['video_view_count']=$one_video->video_view_count;
		$data['video_like_count']=$one_video->video_like_count;

		$meta_setting=meta_setting();
		
		$pageTitle=$one_video->video_title.'-'.$meta_setting->title;
		$metaDescription=$one_video->video_meta_description.'-'.$meta_setting->meta_description;
		$metaKeyword=$one_video->video_meta_keyword.'-'.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/video/view_video',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	
	}
	
	function buy($video_id = '')
	{
		$video_id = base64_decode($video_id);
		if(!check_user_authentication()) {  redirect('login'); } 
		if($video_id == '')
		{
			redirect('home/404');
		}
		$video_detail =  $this->video_model->check_exist_video($video_id,0);
		if(!$video_detail)
		{
			redirect('video/all');
		}
		
		$res = $this->video_model->video_buy_process($video_id);
		if($res == 1)
		{
			$data_notification = array('to_user_id'=>get_authenticateUserID(),
			'video_id'=>$video_id,
			'act'=>'VIDEO_BUY',
			'is_read'=>1,
			'notification_date'=>date("Y-m-d H:i:s")
			);
			$this->db->insert('system_notification',$data_notification);
			
			redirect('user/videodownload/20/0/success_video');
		}
		else if($res == 'NO_BALANCE')
		{
			redirect('design/buypoint');
		}
		else
		{
			redirect('video/all');
		}
	}
	
	function like_dislike($like =0 ,$id = 0)
	{
		if(!check_user_authentication())
		{
			$this->session->set_userdata('previousPage', 'video/view_me/'.$id);
			 redirect("home/login");
		}
		
		$like_status = $_REQUEST['like_status'];
		$video_id = $_REQUEST['video_id'];
		$res = $this->video_model->set_like_dislike($like_status,$video_id);
		echo "success";
		exit;
		
	}

	
}
?>