<?php
class Favorite extends IWEB_Controller {
	
	/*
	Function name :Dashboard()
	Description :Its Default Constuctor which called when home object initialzie.its load necesary models
	*/
	
	function Favorite ()
	{
		parent::__construct();	
		$this->load->model('home_model');	
		$this->load->model('user_model');	
		$this->load->model('favourite_model');	
		$this->load->model('wallet_model');	
			
	}
	
	function index()
	{
			
		if(!check_user_authentication()) {  redirect('login'); } 
		 redirect('favourite/design'); 
		
	}
	
	function design($limit='20',$offset=0,$msg='')
	{

		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="favourite_design";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['msg']=$msg;
		
		$this->load->library('pagination');
		$config['uri_segment']='4';
		$config['base_url'] = base_url().'favourite_design/design/'.$limit.'/';
		$config['total_rows']= $this->favourite_model->get_total_favourite_design_count();
		
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->favourite_model->get_favourite_design_result($limit,$offset);
		
		$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$pageTitle='Favorite Design - '.$meta_setting->title;
		$metaDescription='Favorite Design - '.$meta_setting->meta_description;
		$metaKeyword='Favorite Design  - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/favourites/list_favourites_design',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	
	}
	
	function video($limit='20',$offset=0,$msg='')
	{

		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="favourite_video";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['msg']=$msg;
		
		$this->load->library('pagination');
		$config['uri_segment']='4';
		$config['base_url'] = base_url().'favourite_design/video/'.$limit.'/';
		$config['total_rows']= $this->favourite_model->get_total_favourite_video_count();
		
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->favourite_model->get_favourite_video_result($limit,$offset);

		$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$pageTitle='Favorite Video - '.$meta_setting->title;
		$metaDescription='Favorite Video - '.$meta_setting->meta_description;
		$metaKeyword='Favorite Video - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/favourites/list_favourites_video',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	
	}
	
	function search($limit='20',$offset=0,$category_id=0,$keyword='',$msg='')
	{
	
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="video";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['category'] = get_parent_video_category();
		$data['msg']=$msg;
		
		$this->load->library('pagination');
		
		if($_POST)
		{
			$category_id = $_POST['category_id'];
			$keyword = $_POST['keyword'];
			if($category_id > 0)
			{
				$category_id = $category_id;
			}
			else
			{
				$category_id  = 0;
			}
			$config['uri_segment']='6';
			$config['base_url'] = base_url().'video/search/'.$limit.'/'.$offset.'/'.$category_id.'/'.$keyword;
			$config['total_rows']= $this->favourite_model->get_total_category_video_count($category_id,$keyword);
			$config['per_page'] = $limit;		
			$this->pagination->initialize($config);		
			$data['page_link'] = $this->pagination->create_links();
			$data['result'] = $this->favourite_model->get_category_video_result($limit,$offset,$category_id,$keyword);
			
		}
		else
		{
			$category_id='';
			$keyword='';
			
			$config['uri_segment']='4';
			$config['base_url'] = base_url().'design/search/'.$limit.'/';
			$config['total_rows']= $this->favourite_model->get_total_video_count();
			$config['per_page'] = $limit;		
			$this->pagination->initialize($config);		
			$data['page_link'] = $this->pagination->create_links();
			$data['result'] = $this->favourite_model->get_all_video_result($limit,$offset);
		}
		
		/*echo "<pre>";
		print_r($data['result']);
		die;*/
		$data['category_id']=$category_id;
		$data['keyword']=$keyword;
		$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$pageTitle='Video - '.$meta_setting->title;
		$metaDescription='Video - '.$meta_setting->meta_description;
		$metaKeyword='Video - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/video/search',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	
	}
	

	
}
?>