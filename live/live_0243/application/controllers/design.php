<?php
class Design extends IWEB_Controller {
	
	/*
	Function name :Dashboard()
	Description :Its Default Constuctor which called when home object initialzie.its load necesary models
	*/
	
	function Design()
	{
		parent::__construct();	
		$this->load->model('home_model');	
		$this->load->model('user_model');	
		$this->load->model('design_model');	
		$this->load->model('package_model');	
		$this->load->model('wallet_model');	
		$this->load->library('zip');
			
	}
	
	function index($slug='')
	{
			
		$chkdesign = check_design_exist($slug);
		if(!$chkdesign)
		{
			redirect('home/404');
			
		}
		else
		{
		
			$design_id = $chkdesign->design_id; 
			
			$design_detail =  $this->design_model->check_exist_design($design_id,0);
			
			$data = array();
			$data['site_setting']=site_setting();
			$data['active_menu']="3ddesign";
			$theme = getThemeName();
			$this->template->set_master_template($theme .'/template.php');
			
			$data['theme']=$theme;
			$data['category'] = get_parent_category();
			$data['alldesign'] = get_alldesign();
			
			//Set design view count
			$design_view = $this->design_model->set_design_view($design_id);
			
			$one_design = $this->design_model->get_one_design($design_id);
			
			$get_one_design_tag = $this->design_model->get_one_design_tag($design_id);
			$data['design_images']= $this->design_model->get_one_design_images($design_id);
			
			$data['user_design']= $this->design_model->get_user_design($one_design->user_id,4,$design_id);
			
			$data['user_design_galley']= $this->design_model->get_user_design_gallery($one_design->user_id,$design_id);
			
		
			
			$data['popular_design']= $this->design_model->get_popular_design(4,$design_id);
			
			$desing_user_info = get_user_profile_by_id($one_design->user_id);
			
			/*if($get_one_design_tag)
			{
				$design_tag ='';
				foreach($get_one_design_tag as $gt)
				{
					$design_tag .= $gt->tag_name.',';
				}
				
			}*/
			$data['get_one_design_tag']=$get_one_design_tag;
			$data['main_image'] =  $this->design_model->get_one_main_design_image($design_id);
			$data['user_id']=$one_design->user_id;
			$data['design_id']=$one_design->design_id;
			//$data['design_tag'] = substr($design_tag,0,-1);
			$data['design_title']=$one_design->design_title;
			$data['design_content']=$one_design->design_content;
			$data['category_id']=$one_design->category_id;
			$data['design_point']=$one_design->design_point;
			$data['design_ref_id']=$one_design->design_ref_id;
			$data['design_by']=ucwords($desing_user_info->full_name);
			$data['design_is_modified']=$one_design->design_is_modified;
			$data['design_meta_keyword']=$one_design->design_meta_keyword;
			$data['design_meta_description']=$one_design->design_meta_description;
			$data['parent_design_id']=$one_design->parent_design_id;
			$data['design_date']=$one_design->design_date;
			$data['design_view_count']=$one_design->design_view_count;
			$data['design_like_count']=$one_design->design_like_count;
		    $data['design_gallery_type']=$one_design->design_gallery_type;
			
			$meta_setting=meta_setting();
			
			$pageTitle=$one_design->design_title.'-'.$meta_setting->title;
			$metaDescription=$one_design->design_meta_description.'-'.$meta_setting->meta_description;
			$metaKeyword=$one_design->design_meta_keyword.'-'.$meta_setting->meta_keyword;
			
			$this->template->write('pageTitle',$pageTitle,TRUE);
			$this->template->write('metaDescription',$metaDescription,TRUE);
			$this->template->write('metaKeyword',$metaKeyword,TRUE);
			$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
			$this->template->write_view('content_center',$theme .'/layout/design/view_design',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();	
		}
		//if(!check_user_authentication()) {  redirect('login'); } 
		 //redirect('design/all'); 
		
	}
	
	function all($limit='20',$offset=0,$msg='')
	{
		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3ddesign";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['msg']=$msg;
		
		$this->load->library('pagination');
		$config['uri_segment']='4';
		$config['base_url'] = base_url().'design/all/'.$limit.'/';
		$config['total_rows']= $this->design_model->get_toala_user_design_count(get_authenticateUserID());
	
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->design_model->get_design_result(get_authenticateUserID(),$limit,$offset);
		
		$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		$pageTitle='3D-Design Hive- '.$meta_setting->title;
		$metaDescription='3D-Design Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-Design Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/design/list_3ddesign_hive',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	
	}
	
	function add_me()
	{
		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3ddesign";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$data['category'] = get_parent_category();
		$data['alldesign'] = get_alldesign();
		$data['category_id']='';
		$data['parent_design_id']='';
	//	$data['design_images']='';
	//	$data['design_id']='';
		$data['design_gallery_type']='';		
		$data["error"] = "";
		$meta_setting=meta_setting();
		
		if($_POST)
		{
			$this->form_validation->set_rules('design_title', 'Design Title', 'trim|required');
			$this->form_validation->set_rules('design_point', 'Design Point', 'trim|required');
			$this->form_validation->set_rules('design_content', 'Design Content', 'trim|required');
			$this->form_validation->set_rules('design_category', 'Design Category', 'required');
			$this->form_validation->set_rules('design_gallery_type', 'Gallery Type', 'required');
			
			if($this->form_validation->run() == FALSE)
				{
						if(validation_errors())
						{
							$data["error"] = validation_errors();
						}
						else
						{
								$data["error"] = "";
						}
						
						$data['design_title']=$this->input->post('design_title');
						$data['design_content']=$this->input->post('design_content');
						$data['design_category']=$this->input->post('design_category');
						$data['design_point']=$this->input->post('design_point');
						$data['design_ref_id']=$this->input->post('design_ref_id');
						$data['design_by']=$this->input->post('design_by');
						$data['design_is_modified']=$this->input->post('design_is_modified');
						$data['design_meta_keyword']=$this->input->post('design_meta_keyword');
						$data['design_meta_description']=$this->input->post('design_meta_description');
						$data['design_tag']=$this->input->post('tag_name');
						
						$data['design_gallery_type']=$this->input->post('design_gallery_type');
						
						$design_id = $this->input->post('design_id');
						if($design_id > 0)
						{
							$data['design_images']= $this->design_model->get_one_design_images($design_id);
							$data['design_id']=$this->input->post('design_id');
						}
						else
						{
							$data['design_images']='';
							$data['design_id']='';
						}
						
				}
				else
				{
					
					if($this->input->post('design_id') != '')
					{
						$result = $this->design_model->update_design();
						redirect('design/all/20/0/update_design');
					}
					else
					{
						$result = $this->design_model->insert_design();
						redirect('design/all/20/0/success_design');
					}
				}	
		}
		else
		{
			$data['design_title']=$this->input->post('design_title');
			$data['design_content']=$this->input->post('design_content');
			$data['design_category']=$this->input->post('design_category');
			$data['design_point']=$this->input->post('design_point');
			$data['design_ref_id']=$this->input->post('design_ref_id');
			$data['design_by']=$this->input->post('design_by');
			$data['design_is_modified']=$this->input->post('design_is_modified');
			$data['design_meta_keyword']=$this->input->post('design_meta_keyword');
			$data['design_meta_description']=$this->input->post('design_meta_description');
			$data['design_tag']=$this->input->post('tag_name');
			$data['design_id']=$this->input->post('design_id');
			$data['design_gallery_type']=$this->input->post('design_gallery_type');
			$data['design_images']='';
			
			
			
		}
		
		$pageTitle='3D-Design Hive- '.$meta_setting->title;
		$metaDescription='3D-Design Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-Design Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/design/add_3d_design_hive',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	}
	
	function edit_me($design_id='')
	{
		
		if(!check_user_authentication()) {  redirect('login'); } 
		
		$design_detail =  $this->design_model->check_exist_design($design_id,get_authenticateUserID());
		
		if(!$design_detail)
		{
			redirect('design/all');
		}
		
		if($design_id != '' && $design_id > 0)
		{
		
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3ddesign";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$data['category'] = get_parent_category();
		$data['alldesign'] = get_modified_design($design_id);
		$data['error']='';
		$one_design = $this->design_model->get_one_design($design_id);
		$get_one_design_tag = $this->design_model->get_one_design_tag($design_id);
		$data['design_images']= $this->design_model->get_one_design_images($design_id);
		
		if($get_one_design_tag)
		{
			$design_tag ='';
			foreach($get_one_design_tag as $gt)
			{
				$design_tag .= $gt->tag_name.',';
			}
			
		}
		
		$data['design_id']=$one_design->design_id;
		$data['design_tag'] = substr($design_tag,0,-1);
		$data['design_title']=$one_design->design_title;
		$data['design_content']=$one_design->design_content;
		$data['design_category']=$one_design->category_id;
		$data['design_point']=$one_design->design_point;
		$data['design_ref_id']=$one_design->design_ref_id;
		$data['design_by']=$one_design->design_by;
		$data['design_is_modified']=$one_design->design_is_modified;
		$data['design_meta_keyword']=$one_design->design_meta_keyword;
		$data['design_meta_description']=$one_design->design_meta_description;
		$data['parent_design_id']=$one_design->parent_design_id;
		$data['design_gallery_type']=$one_design->design_gallery_type;


		$meta_setting=meta_setting();
		
		$pageTitle='3D-Design Hive- '.$meta_setting->title;
		$metaDescription='3D-Design Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-Design Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/design/add_3d_design_hive',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	}
		else
		{
			redirect('user/dashboard');
		}
	
	}
	
	
	function delete_me($design_id)
	{
		
		if(!check_user_authentication()) {  redirect('login'); } 
		
		$design_detail =  $this->design_model->check_exist_design($design_id,get_authenticateUserID());
		if(!$design_detail)
		{
			redirect('design/all');
		}
		
		
		if($design_id != '' && $design_id > 0)
		{
			$delete_tag=$this->db->query("delete from ".$this->db->dbprefix('design_tags')." where design_id='".$design_id."'");
			$delete_cat=$this->db->query("delete from ".$this->db->dbprefix('design_category_rel')." where design_id='".$design_id."'");
			
			$design_images= $this->design_model->get_one_design_images($design_id);
			 if(design_images) 
			{
				foreach($design_images as $di)
				{
					if(file_exists(base_path().'upload/design_orig/'.$di->attachment_name))
					{
						unlink(base_path().'upload/design_orig/'.$di->attachment_name);
					}	
								
					if(file_exists(base_path().'upload/design/'.$di->attachment_name))
					{
						unlink(base_path().'upload/design/'.$di->attachment_name);
					}
				}
				
			}
			
			$delete_attach=$this->db->query("delete from ".$this->db->dbprefix('design_attachment')." where design_id='".$design_id."'");
			$design_comment=$this->db->query("delete from ".$this->db->dbprefix('design_comment')." where design_id='".$design_id."'");
			$design_view = $this->db->query("delete from ".$this->db->dbprefix('design_view')." where design_id='".$design_id."'");
			$design_share = $this->db->query("delete from ".$this->db->dbprefix('design_share')." where design_id='".$design_id."'");
			$design_like = $this->db->query("delete from ".$this->db->dbprefix('design_like')." where design_id='".$design_id."'");
			$design_download = $this->db->query("delete from ".$this->db->dbprefix('design_download')." where design_id='".$design_id."'");
			$delete_tag=$this->db->query("delete from ".$this->db->dbprefix('design')." where design_id='".$design_id."'");
			
			redirect('design/all/20/0/delete_design');
		}
		else
		{
			redirect('user/dashboard');
		}
		
	}
	
	function delete_design_attachment()
	{
		 $attachment_id = $_REQUEST['attachment_id'];
		
		$design_attachment= $this->design_model->get_one_design_attachment($attachment_id);
		 if($design_attachment) 
		{		if(file_exists(base_path().'upload/design_orig/'.$design_attachment->attachment_name))
				{
					unlink(base_path().'upload/design_orig/'.$design_attachment->attachment_name);
				}	
							
				if(file_exists(base_path().'upload/design/'.$design_attachment->attachment_name))
				{
					unlink(base_path().'upload/design/'.$design_attachment->attachment_name);
				}
			
		}
		$delete_attach=$this->db->query("delete from ".$this->db->dbprefix('design_attachment')." where attachment_id='".$attachment_id."'");
		echo "success";
		die;
		
	}
	
	function search($limit='20',$category_id=0,$keyword='',$offset=0,$msg='')
	{
	
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3ddesign";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['category'] = get_parent_category();
		
	
		$data['msg']=$msg;
		
		$this->load->library('pagination');
		
		if($_POST)
		{
		
			$category_ids = '';
			if(isset($_POST['category'])){ $category_ids =$_POST['category']; };
			$keyword = $_POST['keyword'];
			
			if($keyword == "")
			{
				$keyword = 0;
			}
			if($category_ids)
			{
				$category_id = implode("-",$category_ids);
			}
			else
			{
				$category_id  = 0;
				
			}
			
			$limit = $this->input->post("limit");
			
			
			$config['uri_segment']='5';
			$config['base_url'] = base_url().'design/search/'.$limit.'/'.$category_id.'/'.$keyword;
			$config['total_rows']= $this->design_model->get_total_category_design_count($category_id,$keyword);
			$config['per_page'] = $limit;		
			$this->pagination->initialize($config);		
			$data['page_link'] = $this->pagination->create_links();
			$data['result'] = $this->design_model->get_category_design_result($limit,$offset,$category_id,$keyword);
			
		}
		else
		{
			//$category_id='';
			//$keyword='';
			
		    $config['uri_segment']='5';
			$config['base_url'] = base_url().'design/search/'.$limit.'/'.$category_id.'/'.$keyword;
			$config['total_rows']= $this->design_model->get_total_category_design_count($category_id,$keyword);
			$config['per_page'] = $limit;		
			$this->pagination->initialize($config);		
			$data['page_link'] = $this->pagination->create_links();
			$data['result'] = $this->design_model->get_category_design_result($limit,$offset,$category_id,$keyword);
		}
			
		/*echo "<pre>";
		print_r($data['result']);
		die;*/
		$data['category_id']=$category_id;
		$data['keyword']=$keyword;
		$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$pageTitle='3D-Design Hive- '.$meta_setting->title;
		$metaDescription='3D-Design Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-Design Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/design/search',$data,TRUE);
	$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	
	}
	
	function add_design_category()
	{
		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3ddesign";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data["error"] = "";
		$data['category'] = get_parent_category();
	
		if($this->input->post('submit')) 
		{
		
				$this->form_validation->set_rules('category_name', 'Category Name', 'trim|required');
				if($this->form_validation->run() == FALSE)
				{
						if(validation_errors())
						{
							$data["error"] = validation_errors();
						}
						else
						{
								$data["error"] = "";
						}
						
						$data['category_parent_id']=$this->input->post('category_parent_id');
						$data['category_name']=$this->input->post('category_name');
						$data['category_description']=$this->input->post('category_description');
						$data['category_status']=$this->input->post('category_status');

				}
				else
				{
					$result = $this->design_model->insert_design_category();
					redirect('design/all/20/0/success');
				}	
		
		}	
		else
		{
			$data['category_name']=$this->input->post('category_name');
			$data['category_description']=$this->input->post('category_description');
		}	
		$pageTitle='3D-Design Hive- '.$meta_setting->title;
		$metaDescription='3D-Design Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-Design Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/design/add_3d_design_category',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	}
	function view_me($design_id='')
	{
		//if(!check_user_authentication()) {  redirect('login'); } 
		
		if($design_id == '')
		{
			redirect('home/404');
		}
		$design_detail =  $this->design_model->check_exist_design($design_id,0);
		if(!$design_detail)
		{
			redirect('design/all');
		}
		
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3ddesign";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$data['category'] = get_parent_category();
		$data['alldesign'] = get_alldesign();
		
		//Set design view count
		$design_view = $this->design_model->set_design_view($design_id);
		
		$one_design = $this->design_model->get_one_design($design_id);
		
		
		$get_one_design_tag = $this->design_model->get_one_design_tag($design_id);
		$data['design_images']= $this->design_model->get_one_design_images($design_id);
		
		
		$data['popular_design']= $this->design_model->get_popular_design(4);
		
		//$data['popular_design']= $this->design_model->get_user_design(4);
		
		
		/*if($get_one_design_tag)
		{
			$design_tag ='';
			foreach($get_one_design_tag as $gt)
			{
				$design_tag .= $gt->tag_name.',';
			}
			
		}*/
		$data['get_one_design_tag']=$get_one_design_tag;
		$data['main_image'] =  $this->design_model->get_one_main_design_image($design_id);
		$data['user_id']=$one_design->user_id;
		$data['design_id']=$one_design->design_id;
		//$data['design_tag'] = substr($design_tag,0,-1);
		$data['design_title']=$one_design->design_title;
		$data['design_content']=$one_design->design_content;
		$data['category_id']=$one_design->category_id;
		$data['design_point']=$one_design->design_point;
		$data['design_ref_id']=$one_design->design_ref_id;
		$data['design_by']=$one_design->design_by;
		$data['design_is_modified']=$one_design->design_is_modified;
		$data['design_meta_keyword']=$one_design->design_meta_keyword;
		$data['design_meta_description']=$one_design->design_meta_description;
		$data['parent_design_id']=$one_design->parent_design_id;
		$data['design_date']=$one_design->design_date;
		$data['design_view_count']=$one_design->design_view_count;
		$data['design_like_count']=$one_design->design_like_count;
		$data["desing_gallery_type"] = $one_design->design_gallery_type;
		

		$meta_setting=meta_setting();
		
		$pageTitle=$one_design->design_title.'-'.$meta_setting->title;
		$metaDescription=$one_design->design_meta_description.'-'.$meta_setting->meta_description;
		$metaKeyword=$one_design->design_meta_keyword.'-'.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/design/view_design',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	
	}
	
	function buy($design_id = '')
	{
		$design_id = base64_decode($design_id);
		if(!check_user_authentication()) {  redirect('login'); } 
		if($design_id == '')
		{
			redirect('home/404');
		}
		$design_detail =  $this->design_model->check_exist_design($design_id,0);
		if(!$design_detail)
		{
			redirect('design/all');
		}
		$res = $this->design_model->design_buy_process($design_id);
		if($res == 1)
		{
			$data_notification = array('to_user_id'=>get_authenticateUserID(),
			'design_id'=>$design_id,
			'act'=>'DESIGN_BUY',
			'is_read'=>1,
			'notification_date'=>date("Y-m-d H:i:s")
			);
			$this->db->insert('system_notification',$data_notification);
			
			redirect('user/designdownload/20/0/success_design');
			
		}
		else if($res == 'NO_BALANCE')
		{
			redirect('design/buypoint');
		}
		else
		{
			redirect('design/all');
		}
	}
	
	function buypoint()
	{
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$data["package_list"] = $this->package_model->get_package_list();
		$meta_setting=meta_setting();
		
		$pageTitle='3D-Design Hive- '.$meta_setting->title;
		$metaDescription='3D-Design Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-Design Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/user/buypoints',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	}
	
	
	function like_dislike($like =0 ,$id = 0)
	{
		
		if(!check_user_authentication())
		{
			$this->session->set_userdata('previousPage', 'design/view_me/'.$id);
			 redirect("home/login");
		}
		
		$like_status = $_REQUEST['like_status'];
		$design_id = $_REQUEST['design_id'];
		$res = $this->design_model->set_like_dislike($like_status,$design_id);
		echo "success";
		exit;
		
	}

	function download_image_zip($design_id)
	{
		$image_data =  $this->design_model->get_all_images_of_design($design_id);
		if($image_data)
		{
			foreach($image_data as $img)
			{
				$path = base_path().'upload/design/'.$img->attachment_name;
				$this->zip->read_file($path); 
			}
			$this->zip->archive('myimage.zip');
		 	$this->zip->download('myimage.zip'); 
		}
		else
		{
			redirect('design/all');
		}
	}
}
?>