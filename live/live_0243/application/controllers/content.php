<?php
/*********************************************************************************
 * This the taskrabbitclone.com  by Rockers Technology. is paid software. It is released under the terms of 
 * the following BSD License.
 * 
 *  Rockers Technologies (Head Office)
 *    5038,Berthpage Dr
 *    suwanee, GA. Zip Code : 30024
    
 *    E-mail Address : nishu@rockersinfo.com
 * 
 * Copyright � 2012-2020 by Rockers Technology , INC a domestic profit corporation has been duly incorporated under
the laws of the state of georiga , USA. www.rockersinfo.com
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this 
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this 
 *   list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.

 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class Content extends IWEB_Controller {
	
	/*
	Function name :Content()
	Description :Its Default Constuctor which called when content object initialzie.its load necesary models
	*/
	function Content()
	{
		parent::__construct();	
		$this->load->model('content_model');
	}
	
	

	
	
	/*
	Function name :detail()
	Parameter : $title(page title), $id(page id)
	Return : array of page detail
	Use : user can see dynamic generated page content
	Description : user can see dynamic generated page content which called http://hostname/content/detail/help/1			 
	*/
	
	function detail($title,$id)
	{
		$content=$this->content_model->get_content_by_id($id);	
		
		if(!$content)
		{
			redirect('home');
		}
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
	
		
		$meta_setting=meta_setting();
		
		$data['content']=$content;
		
		
		
		$pageTitle=$content->pages_title.' - '.$meta_setting->title;
		$metaDescription=$content->meta_description.' - '.$meta_setting->meta_description;
		$metaKeyword=$content->meta_keyword.' - '.$meta_setting->meta_keyword;
		
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('campaign_box',$theme .'/layout/content/content',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
		
	}
	
	function aboutus()
	{
		$data=array();	
		$site_setting=site_setting();
		$data['site_setting']=$site_setting;
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		$meta_setting=meta_setting();
		$pageTitle=$meta_setting->title;
		$metaDescription=$meta_setting->meta_description;
		$metaKeyword=$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle', 'About Us-'.$pageTitle, TRUE);
		$this->template->write('metaDescription', 'About Us-'.$metaDescription, TRUE);
		$this->template->write('metaKeyword', 'About Us-'.$metaKeyword, TRUE);	
			
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('campaign_box',$theme .'/layout/pages/aboutus',$data,TRUE);		
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	function howitworks()
	{
		$data=array();	
		$site_setting=site_setting();
		$data['site_setting']=$site_setting;
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		$meta_setting=meta_setting();
		$pageTitle=$meta_setting->title;
		$metaDescription=$meta_setting->meta_description;
		$metaKeyword=$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle', 'How it works-'.$pageTitle, TRUE);
		$this->template->write('metaDescription', 'How it works-'.$metaDescription, TRUE);
		$this->template->write('metaKeyword', 'How it works-'.$metaKeyword, TRUE);	
			
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('campaign_box',$theme .'/layout/pages/howitworks',$data,TRUE);		
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	function traveller()
	{
		$data=array();	
		$site_setting=site_setting();
		$data['site_setting']=$site_setting;
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		$meta_setting=meta_setting();
		$pageTitle=$meta_setting->title;
		$metaDescription=$meta_setting->meta_description;
		$metaKeyword=$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle', 'Traveller-'.$pageTitle, TRUE);
		$this->template->write('metaDescription', 'Traveller-'.$metaDescription, TRUE);
		$this->template->write('metaKeyword', 'Traveller-'.$metaKeyword, TRUE);	
			
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('campaign_box',$theme .'/layout/pages/traveller',$data,TRUE);		
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	function travelagent()
	{
		$data=array();	
		$site_setting=site_setting();
		$data['site_setting']=$site_setting;
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		$meta_setting=meta_setting();
		$pageTitle=$meta_setting->title;
		$metaDescription=$meta_setting->meta_description;
		$metaKeyword=$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle', 'Travelagent-'.$pageTitle, TRUE);
		$this->template->write('metaDescription', 'Travelagent-'.$metaDescription, TRUE);
		$this->template->write('metaKeyword', 'Travelagent-'.$metaKeyword, TRUE);	
			
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('campaign_box',$theme .'/layout/pages/travelagent',$data,TRUE);		
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	function terms()
	{
		$data=array();	
		$site_setting=site_setting();
		$data['site_setting']=$site_setting;
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		$meta_setting=meta_setting();
		$pageTitle=$meta_setting->title;
		$metaDescription=$meta_setting->meta_description;
		$metaKeyword=$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle', 'Terms-and-condition-'.$pageTitle, TRUE);
		$this->template->write('metaDescription', 'Terms-and-condition'.$metaDescription, TRUE);
		$this->template->write('metaKeyword', 'Terms-and-condition'.$metaKeyword, TRUE);	
			
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('campaign_box',$theme .'/layout/pages/terms',$data,TRUE);		
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
}

?>