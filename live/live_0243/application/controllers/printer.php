<?php

class Printer extends IWEB_Controller {
    /*
      Function name :User()
      Description :Its Default Constuctor which called when user object initialzie.its load necesary models
     */

    function Printer() {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->model('user_model');
        $this->load->model('printer_model');
        //$this->load->model('video_model');	
    }

    /*
      Function name :index()
      Parameter :none
      Return : none
      Use : redirect to user dashboard
      Description : none
     */

    public function index($limit='20', $offset=0, $msg='') {
        if (!check_user_authentication()) {
            redirect('login');
        }
        $user_info = get_user_profile_by_id(get_authenticateUserID());
        $data['user_info'] = $user_info;
        $data['active_menu'] = '3dprinter';

        $site_setting = site_setting();
        $data['site_setting'] = $site_setting;
        $data['error'] = "";
        $data['msg'] = $msg;

        $this->load->library('pagination');
        $config['uri_segment'] = '4';
        $config['base_url'] = base_url() . 'user/index/' . $limit . '/';
        $config['total_rows'] = $this->printer_model->get_user_location_count(get_authenticateUserID());

        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        $data['page_link'] = $this->pagination->create_links();
        $data['result'] = $this->printer_model->get_user_location(get_authenticateUserID(), $limit, $offset);

        //  $user_notification=$this->user_model->get_user_notification(get_authenticateUserID());
        //$data['user_notification']=$user_notification;


        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        $data['theme'] = $theme;

        $meta_setting = meta_setting();
        $pageTitle = ucfirst($user_info->first_name) . ' ' . ucfirst(substr($user_info->last_name, 0, 1)) . ' - Location ' . $meta_setting->title;
        $metaDescription = ucfirst($user_info->first_name) . ' ' . ucfirst(substr($user_info->last_name, 0, 1)) . ' - Location ' . $meta_setting->meta_description;
        $metaKeyword = ucfirst($user_info->first_name) . ' ' . ucfirst(substr($user_info->last_name, 0, 1)) . ' - Location ' . $meta_setting->meta_keyword;

        $this->template->write('pageTitle', $pageTitle, TRUE);
        $this->template->write('metaDescription', $metaDescription, TRUE);
        $this->template->write('metaKeyword', $metaKeyword, TRUE);
        $this->template->write_view('header', $theme . '/layout/common/header_theme', $data, TRUE);
        $this->template->write_view('content_center', $theme . '/layout/printer/list_printer', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer_theme', $data, TRUE);
        $this->template->render();
    }

    function add_me() {
        if (!check_user_authentication()) {
            redirect('login');
        }
        $data = array();
        $data['site_setting'] = site_setting();
        $data['active_menu'] = "3dprinter";
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        $data['theme'] = $theme;
        $data["error"] = "";
        $meta_setting = meta_setting();

        $data['store_category'] = $this->printer_model->get_all_store_category();

        if ($_POST) {
            $this->form_validation->set_rules('store_name', 'Store Name', 'trim|required');
            $this->form_validation->set_rules('store_address', 'Address', 'trim|required');
            $this->form_validation->set_rules('store_ctiy', 'City', 'trim|required');
            $this->form_validation->set_rules('store_state', 'State', 'required');
            $this->form_validation->set_rules('store_country', 'Country', 'required');
            $this->form_validation->set_rules('store_postal_code', 'Postal Code', 'required');
            //$this->form_validation->set_rules('store_lat', 'Lattitude', 'required');
            //$this->form_validation->set_rules('store_long', 'Longitude', 'required');
            $this->form_validation->set_rules('store_email', 'Email', 'required|valid_email');

            if ($this->input->post('store_url') != '') {
                $this->form_validation->set_rules('store_url', 'Valid URL', 'valid_url');
            }

            if ($this->form_validation->run() == FALSE) {
                if (validation_errors()) {
                    $data["error"] = validation_errors();
                } else {
                    $data["error"] = "";
                }

                $data['store_name'] = $this->input->post('store_name');
                $data['store_address'] = $this->input->post('store_address');
                $data['store_address2'] = $this->input->post('store_address2');
                $data['store_ctiy'] = $this->input->post('store_ctiy');
                $data['store_state'] = $this->input->post('store_state');
                $data['store_postal_code'] = $this->input->post('store_postal_code');
                $data['store_country'] = $this->input->post('store_country');
                $data['store_lat'] = $this->input->post('store_lat');
                $data['store_long'] = $this->input->post('store_long');
                $data['store_email'] = $this->input->post('store_email');
                $data['store_phone'] = $this->input->post('store_phone');
                $data['store_url'] = $this->input->post('store_url');
                $data['store_id'] = $this->input->post('store_id');
                //$data['store_status']=$this->input->post('store_status');
                $data['category_id'] = $this->input->post('category_id');
            } else {

                if ($this->input->post('store_id') != '') {
                    $result = $this->printer_model->update_location();
                    redirect('printer/index/20/0/update_location');
                } else {
                    $result = $this->printer_model->insert_location();
                    redirect('printer/index/20/0/success_location');
                }
            }
        } else {
            $data['store_name'] = $this->input->post('store_name');
            $data['store_address'] = $this->input->post('store_address');
            $data['store_address2'] = $this->input->post('store_address2');
            $data['store_ctiy'] = $this->input->post('store_ctiy');
            $data['store_state'] = $this->input->post('store_state');
            $data['store_postal_code'] = $this->input->post('store_postal_code');
            $data['store_country'] = $this->input->post('store_country');
            $data['store_lat'] = $this->input->post('store_lat');
            $data['store_long'] = $this->input->post('store_long');
            $data['store_email'] = $this->input->post('store_email');
            $data['store_phone'] = $this->input->post('store_phone');
            $data['store_url'] = $this->input->post('store_url');
            $data['store_id'] = $this->input->post('store_id');
            //$data['store_status']=$this->input->post('store_status');
            $data['category_id'] = $this->input->post('category_id');
        }

        $pageTitle = '3D Printer - ' . $meta_setting->title;
        $metaDescription = '3D Printer - ' . $meta_setting->meta_description;
        $metaKeyword = '3D Printer - ' . $meta_setting->meta_keyword;

        $this->template->write('pageTitle', $pageTitle, TRUE);
        $this->template->write('metaDescription', $metaDescription, TRUE);
        $this->template->write('metaKeyword', $metaKeyword, TRUE);
        $this->template->write_view('header', $theme . '/layout/common/header_theme', $data, TRUE);
        $this->template->write_view('content_center', $theme . '/layout/printer/add_printer', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer_theme', $data, TRUE);
        $this->template->render();
    }

    function edit_me($store_id='') {

        if (!check_user_authentication()) {
            redirect('login');
        }

        $store_detail = $this->printer_model->check_exist_store($store_id, get_authenticateUserID());

        if (!$store_detail) {
            redirect('printer');
        }

        if ($store_id != '' && $store_id > 0) {

            $data = array();
            $data['site_setting'] = site_setting();
            $data['active_menu'] = "3dprinter";
            $theme = getThemeName();
            $this->template->set_master_template($theme . '/template.php');

            $data['theme'] = $theme;
            $data['store_category'] = $this->printer_model->get_all_store_category();
            $data['error'] = '';
            $one_location = $this->printer_model->get_one_store($store_id);

            $data['store_id'] = $one_location->store_id;
            $data['store_name'] = $one_location->store_name;
            $data['store_address'] = $one_location->store_address;
            $data['store_address2'] = $one_location->store_address2;
            $data['store_ctiy'] = $one_location->store_ctiy;
            $data['store_state'] = $one_location->store_state;
            $data['store_postal_code'] = $one_location->store_postal_code;
            $data['store_country'] = $one_location->store_country;
            $data['store_lat'] = $one_location->store_lat;
            $data['store_long'] = $one_location->store_long;
            $data['store_email'] = $one_location->store_email;
            $data['store_phone'] = $one_location->store_phone;
            $data['store_url'] = $one_location->store_url;
            //$data['store_status']=$one_location->store_status;
            $data['category_id'] = $one_location->category_id;

            $meta_setting = meta_setting();

            $pageTitle = '3D-Printer- ' . $meta_setting->title;
            $metaDescription = '3D-Printer - ' . $meta_setting->meta_description;
            $metaKeyword = '3D-Printer - ' . $meta_setting->meta_keyword;

            $this->template->write('pageTitle', $pageTitle, TRUE);
            $this->template->write('metaDescription', $metaDescription, TRUE);
            $this->template->write('metaKeyword', $metaKeyword, TRUE);
            $this->template->write_view('header', $theme . '/layout/common/header_theme', $data, TRUE);
            $this->template->write_view('content_center', $theme . '/layout/printer/add_printer', $data, TRUE);
            $this->template->write_view('footer', $theme . '/layout/common/footer_theme', $data, TRUE);
            $this->template->render();
        } else {
            redirect('user/dashboard');
        }
    }

    function delete_me($store_id) {

        if (!check_user_authentication()) {
            redirect('login');
        }

        $store_detail = $this->printer_model->check_exist_store($store_id, get_authenticateUserID());

        if (!$store_detail) {
            redirect('printer');
        }

        if ($store_id != '' && $store_id > 0) {

            $delete_store_rel = $this->db->query("delete from " . $this->db->dbprefix('store_category_rel') . " where store_id='" . $store_id . "'");
            $delete_store = $this->db->query("delete from " . $this->db->dbprefix('store') . " where store_id='" . $store_id . "'");

            redirect('printer/index/20/0/delete_location');
        } else {
            redirect('user/dashboard');
        }
    }

    function email_check($store_email) {

        $cemail = $this->printer_model->emailTaken($store_email);

        if ($cemail) {
            $this->form_validation->set_message('email_check', 'There is an existing account associated with this email.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function search() {
        $data = array();
        $data['site_setting'] = site_setting();
        $data['active_menu'] = "3dprinter";
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');
        $data['theme'] = $theme;
        $meta_setting = meta_setting();
        $data['result'] = ''; //$this->printer_model->get_printer_result();
        
        
        /*$this->session->set_userdata('visitor_user_postal_code', $this->security->xss_clean(''));
        $this->session->set_userdata('visitor_user_lat', $this->security->xss_clean(''));
        $this->session->set_userdata('visitor_user_lon', $this->security->xss_clean(''));*/
        
        

        $pageTitle = '3D-Printer - ' . $meta_setting->title;
        $metaDescription = '3D-Printer - ' . $meta_setting->meta_description;
        $metaKeyword = '3D-Printer - ' . $meta_setting->meta_keyword;

        $this->template->write('pageTitle', $pageTitle, TRUE);
        $this->template->write('metaDescription', $metaDescription, TRUE);
        $this->template->write('metaKeyword', $metaKeyword, TRUE);
        $this->template->write_view('header', $theme . '/layout/common/header_home', $data, TRUE);
        $this->template->write_view('content_center', $theme . '/layout/printer/search', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
        $this->template->render();
    }

    function getstore() {
        $postal_code = '';
        $visitor_lat = '';
        $visitor_lon = '';
        
        $distance_range=10000;

        if ($_POST) {
            if (isset($_POST['lat'])) {
                $visitor_lat = $_POST['lat'];
            }
            if (isset($_POST['lon'])) {
                $visitor_lon = $_POST['lon'];
            }
            if (isset($_POST['postal_code'])) {
                $postal_code = $_POST['postal_code'];
            }
            if (isset($_POST['distance_range'])) {
                $distance_range = $_POST['distance_range'];
            }
        }

        $result = $this->printer_model->get_printer_geo_result($distance_range,$postal_code, $visitor_lat, $visitor_lon);
        echo json_encode($result);
        die;
    }

    function getgeolocate($latitude='', $longitude='') {
        $postal_code = '';
        $visitor_lat = '';
        $visitor_lon = '';

        $result_arr = array();

        if ($_POST) {
            if (isset($_POST['lat'])) {
                $visitor_lat = $_POST['lat'];
            }
            if (isset($_POST['lon'])) {
                $visitor_lon = $_POST['lon'];
            }
        } elseif ($latitude != '' && $longitude != '') {
            $visitor_lat = $latitude;
            $visitor_lon = $longitude;
        }

        if ($visitor_lat != '' && $visitor_lon != '') {

            $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . $visitor_lat . "," . $visitor_lon . '&sensor=true';


            if (function_exists('curl_init')) {
                $defaults = array(
                    CURLOPT_HEADER => 0,
                    CURLOPT_URL => $url,
                    CURLOPT_FRESH_CONNECT => 1,
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_FORBID_REUSE => 1,
                    CURLOPT_TIMEOUT => 15
                );

                $ch = curl_init();
                curl_setopt_array($ch, $defaults);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result, true);

                $postal_code = $this->checkpostalcode($response);
            } else if (function_exists('file_get_contents')) {

                $response = json_decode(file_get_contents($url), true);
                $postal_code = $this->checkpostalcode($response);
            }
        }

        $result_arr['postal_code'] = $postal_code;
        $result_arr['lat'] = $visitor_lat;
        $result_arr['lon'] = $visitor_lon;
        
        /*$this->session->set_userdata('visitor_user_postal_code', $this->security->xss_clean(''));
        $this->session->set_userdata('visitor_user_lat', $this->security->xss_clean(''));
        $this->session->set_userdata('visitor_user_lon', $this->security->xss_clean(''));*/

        $this->session->set_userdata('visitor_user_postal_code', $postal_code);
        $this->session->set_userdata('visitor_user_lat', $visitor_lat);
        $this->session->set_userdata('visitor_user_lon', $visitor_lon);

        echo json_encode($result_arr);
        die;
        //return $postal_code;
    }

    function checkpostalcode($response=array()) {
        //echo "<pre>";
        //print_r($response); 
        $postal_code = '';

        if (isset($response['status']) && strtolower($response['status']) == 'ok') {
            if (isset($response['results']) && !empty($response['results'])) {
                $result = $response['results'];
                foreach ($result as $key => $val) {
                    if (isset($val['address_components']) && !empty($val['address_components'])) {
                        $address_components = $val['address_components'];

                        foreach ($address_components as $akey => $aval) {
                            if (isset($aval['types']) && !empty($aval['types'])) {
                                $types = $aval['types'];
                                foreach ($types as $tkey => $tval) {
                                    if ($tval == 'postal_code') {
                                        if (isset($aval['long_name']) && $aval['long_name'] != '') {
                                            $postal_code = $aval['long_name'];
                                            break;
                                        } elseif (isset($aval['short_name']) && $aval['short_name'] != '') {
                                            $postal_code = $aval['short_name'];
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //die;
        return $postal_code;
    }

    function getapplocate() {
        $postal_code = '';
        $visitor_lat = '';
        $visitor_lon = '';

        $result_arr = array();


        include("geoip/geoipcity.inc");
        include("geoip/geoipregionvars.php");

        $geo = geoip_open("geoip/GeoLiteCity.dat", GEOIP_STANDARD);

        $ip = getRealIP(); //'71.9.127.141'; //'14.139.245.144'; //'184.106.196.45'; //'208.111.40.148'; //


        if ($ip != '') {
            $record = geoip_record_by_addr($geo, $ip);

            //echo "<pre>";
            //print_r($record);
            //die;
            //$city = $record->city;
            //$state = $record->region;
            
             $loc_set=0;
            if ($_POST) {
                if (isset($_POST['lat']) && $_POST['lat']='' && isset($_POST['lon']) && $_POST['lon']!='') {
                    $visitor_lat = $_POST['lat'];
                     $visitor_lon = $_POST['lon'];
                     $loc_set=1;
                }                
            } 
            
                if (isset($record->latitude) && $record->latitude != '' && isset($record->longitude) && $record->longitude != '' && $loc_set==0) {
                    $visitor_lat = $record->latitude;
                    $visitor_lon = $record->longitude;
                }
            

            if (isset($record->postal_code) && $record->postal_code != '') {
                $postal_code = $record->postal_code;
            } else {
                $postal_code = $this->getgeolocate($visitor_lat, $visitor_lon);
            }
        }

        $result_arr['postal_code'] = $postal_code;
        $result_arr['lat'] = $visitor_lat;
        $result_arr['lon'] = $visitor_lon;

        
        /*$this->session->set_userdata('visitor_user_postal_code', $this->security->xss_clean(''));
        $this->session->set_userdata('visitor_user_lat', $this->security->xss_clean(''));
        $this->session->set_userdata('visitor_user_lon', $this->security->xss_clean(''));*/
        
        
        $this->session->set_userdata('visitor_user_postal_code', $postal_code);
        $this->session->set_userdata('visitor_user_lat', $visitor_lat);
        $this->session->set_userdata('visitor_user_lon', $visitor_lon);

        echo json_encode($result_arr);
        die;
    }

}

?>