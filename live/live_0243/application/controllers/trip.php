<?php
/*********************************************************************************
 * This the taskrabbitclone.com  by Rockers Technology. is paid software. It is released under the terms of 
 * the following BSD License.
 * 
 *  Rockers Technologies (Head Office)
 *    5038,Berthpage Dr
 *    suwanee, GA. Zip Code : 30024
    
 *    E-mail Address : nishu@rockersinfo.com
 * 
 * Copyright © 2012-2020 by Rockers Technology , INC a domestic profit corporation has been duly incorporated under
the laws of the state of georiga , USA. www.rockersinfo.com
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this 
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this 
 *   list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.

 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class Trip extends IWEB_Controller {
	
	/*
	Function name :Home()
	Description :Its Default Constuctor which called when home object initialzie.its load necesary models
	*/
	
	function Trip()
	{
		parent::__construct();	
		$this->load->model('trip_model');	
		$this->load->model('travelagent_model');	
		$this->load->helper('cookie');
			
		
	}
	
	
	/*
	Function name :index()
	Parameter :none
	Return : none
	Use : user can see category list, map, recent tasks, top runners and other details
	Description : site home page its default function which called http://hostname/home
				  SEO friendly URL which is declare in config route.php file  http://hostname/
	*/
	public function index($trip_id='')
	{
		if($trip_id != '')
		{
			redirect('home');
		}
		else
		{
			redirect('home');
		}
	}
	
	
	public function post($msg='')
	{	
			
		$data = array();
		$data['msg'] = $msg;
		$data['error'] = '';
		$data['chk_payment_error'] = '';
		$data['chk_date_error'] = '';
		
		$data['get_currency'] = get_currency();
		
		if($this->input->post('posttrip')) 
		{
			
			$this->form_validation->set_rules('b_pack', 'Package', 'required');
			$this->form_validation->set_rules('departure_from', 'Departure place', 'required');
			$this->form_validation->set_rules('want_go_to', 'Go to place', 'required');
			$this->form_validation->set_rules('total_day', 'Total day', 'required|numeric');
			//$this->form_validation->set_rules('total_night', 'Total Night', 'required|numeric');
			$this->form_validation->set_rules('min_budget', 'Minimum budget', 'required|numeric');
			$this->form_validation->set_rules('max_budget', 'Maximum budget', 'required|numeric');
			$this->form_validation->set_rules('confirm_date', 'Confirm date', 'required');
			$this->form_validation->set_rules('complete_date', 'Complete date', 'required');
			$this->form_validation->set_rules('destination_place', 'Destination', 'required');
			
			
			
			if($this->input->post('confirm_date'))
			{
				if($this->input->post('confirm_date') > $this->input->post('complete_date'))
				{
					$chk_date_result='<p>The trip complete date is bigger than confirm date.</p>';
				}
				else
				{
					$chk_date_result ='';
				}
				$chk_date_error = $chk_date_result;
			}
			else
			{
				$chk_date_error ='';
			}
			
			if($this->input->post('total_people'))
			{
				$total_person = $this->input->post('total_people');
				$adults = $this->input->post('adult');
				$child = $this->input->post('child');
				$chk_total_person = $adults + $child;
			
				if($chk_total_person > $total_person)
				{
					$chk_result='<p>Please select correct total number of person.</p>';
				}
				else
				{
					$chk_result ='';
				}
				
				$error = $chk_result;
			}
			else
			{
				$error='';
			}
			if($this->input->post('min_budget') && $this->input->post('min_budget') != 0)
			{
				$min_budget = $this->input->post('min_budget');
				$max_budget = $this->input->post('max_budget');
				
				
				if($min_budget > $max_budget)
				{
					$chk_payment_result='<p>Minimum budget less than maximum budget.</p>';
				}
				else
				{
					$chk_payment_result ='';
				}
				
				$chk_payment_error = $chk_payment_result;
			}
			else
			{
				$chk_payment_error ='';
			}
			
			if($this->input->post('min_budget') == 0 && $this->input->post('min_budget') != '')
			{
				$chk_min_budget = $this->input->post('min_budget');
				if($chk_min_budget == 0)
				{
					$chk_min_budget_resulr='<p>Minimum budget more than zero.</p>';
				}
				else
				{
					$chk_min_budget_resulr ='';
				}
				
				$chk_min_budget_error = $chk_min_budget_resulr;
			
			}
			else
			{
				$chk_min_budget_error ='';
			}
			
			
			
		
			if($this->form_validation->run() == FALSE || $error != "" || $chk_payment_error != "" || $chk_date_error !="" || $chk_min_budget_error != "")
			{
				
				
				$data["error"] = validation_errors().$error.$chk_payment_error.$chk_date_error.$chk_min_budget_error;
				
				$data['trip_package']=$this->input->post('trip_package');
				$data['departure_from']=$this->input->post('departure_from');
				$data['want_go_to']=$this->input->post('want_go_to');
				$data['total_people']=$this->input->post('total_people');
				$data['adult']=$this->input->post('adult');
				$data['child']=$this->input->post('child');
				$data['total_night']=$this->input->post('total_night');
				$data['total_day']=$this->input->post('total_day');
				$data['room']=$this->input->post('room');
				$data['currency']=$this->input->post('currency');
				$data['min_budget']=$this->input->post('min_budget');
				$data['max_budget']=$this->input->post('max_budget');
				$data['confirm_date']=$this->input->post('confirm_date');
				$data['complete_date']=$this->input->post('complete_date');
				$data['trip_hotel']=$this->input->post('trip_hotel');
				$data['trip_car']=$this->input->post('trip_car');
				$data['trip_plane']=$this->input->post('trip_plane');
				$data['trip_cruises']=$this->input->post('trip_cruises');
				$data['railways'] = $this->input->post('railways');
				$data['b_pack'] = $this->input->post('b_pack');
				
				$data['trip_month']= $this->input->post('trip_month');
				$data['destination_place'] = $this->input->post('destination_place');
				$data['special_comment'] =  $this->input->post('special_comment');
				
				
				
			}
			else
			{
				//if(!check_user_authentication()) {  redirect('home/login'); }
				
				$trip_id=$this->trip_model->post_trip();
				if(get_authenticateUserID() == '' || get_authenticateUserID() == 0)
				{
					redirect('home/login');
				}
				else
				{	
					$temp=get_cookie('tripdetail');
					if(!empty($temp))
					{
						delete_cookie('tripdetail');
					}
				
				
					if($trip_id > 0)
					{
					
					$user_info = get_user_profile_by_id(get_authenticateUserID());
					$trip_info = get_one_trip($trip_id);
					
					$username =$user_info->full_name;				
					$user_id=$user_info->user_id;
					
					$trip_from = $trip_info->trip_from_place;
					$trip_to = $trip_info->trip_to_place;
					 $trip_link=site_url('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id);
				
				
				/////////////============new trp email===========	


				
				$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='New Trip'");
				$email_temp=$email_template->row();
				
				if($email_temp)
				{
				
				$email_address_from=$email_temp->from_address;
				$email_address_reply=$email_temp->reply_address;
				
				$email_subject=$email_temp->subject;				
				$email_message=$email_temp->message;
				
				$email = $user_info->email;
				
				 $email_to=$user_info->email;
				
				
				$email_message=str_replace('{break}','<br/>',$email_message);
				$email_message=str_replace('{user_name}',$username,$email_message);
				$email_message=str_replace('{trip_link}',$trip_link,$email_message);
				$email_temp_url=base_url().'email/';
				$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
				$str=$email_message;
				
				/** custom_helper email function **/
				email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str);
			}

							
					$msg=base64_decode($msg);
					if(strstr($msg,'http'))
					{						
						redirect($msg);
					} else {
						 redirect('trip/post/success'); 
								 
					}
					
			
				}
					else
					{
						redirect('trip/post/fail');
					}
				}
			}
		}
		else
		{
			$data['trip_package']='';
			$data['departure_from']='';
			$data['want_go_to']='';
			$data['total_people']='';
			$data['adult']='';
			$data['child']='';
			$data['total_night']='';
			$data['total_day']='';
			$data['room']='';
			$data['currency']='';
			$data['min_budget']='';
			$data['max_budget']='';
			$data['confirm_date']='';
			$data['complete_date']='';
			$data['railways']='';
			$data['trip_hotel']='';
			$data['trip_car']='';
			$data['trip_plane']='';
			$data['trip_cruises']='';
			$data['b_pack']='';
			$data['destination_place']='';
			$data['special_comment']='';
			$data['trip_month']='';
				
		}
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		
		$pageTitle='Post Trip >>'.$meta_setting->title;
		$metaDescription='Post Trip >>'.$meta_setting->meta_description;
		$metaKeyword='Post Trip >>'.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/posttrip/posttrip',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	
	}
	
	public function trip_detail($id='',$msg='')
	{
		
		$data = array();
		$trip_info = get_one_trip($id);
		if(!$trip_info)
		{
			redirect('home');
		}
		
		
		$data['trip_info']=$trip_info;
		
		
		
		$trip_from = $trip_info->trip_from_place;
		$trip_to = $trip_info->trip_to_place;
		$trip_user_id = $trip_info->user_id;
		
		$data['trip_from']=$trip_from;
		$data['trip_to']=$trip_to;
		$data['user_info'] = get_user_profile_by_id($trip_info->user_id);
		$data['similartrip'] = $this->trip_model->get_similar_trip($id,$trip_from,$trip_to);
		
		$data['trip_proposal'] = $this->trip_model->get_trip_proposal($id);
		$data['trip_average'] = $this->trip_model->get_trip_average($id);
		$data['trip_max'] = $this->trip_model->get_trip_high_amount($id);
		$data['trip_low'] = $this->trip_model->get_trip_low_amount($id);
		
		$data['hidden_trip'] =  $this->trip_model->get_hidden_proposal($id);
		$data['decline_trip'] = $this->trip_model->get_decline_proposal($id);
		
		$data['invited_trip'] = $this->trip_model->get_invted_proposal($id);
		
		$data['trip_conversation'] = $this->trip_model->get_trip_conversation($id,$trip_user_id);

		
		/*echo "<pre>";
		//echo get_authenticateUserID();
		print_r($data['trip_conversation']);
		die;*/
		
		$data["error"] = "";
		$data["msg"]=$msg;
		$data['trip_id'] = $id;
		$data['sow_bid_frm']=0;
		
		$chk_bid=0;
		
		$data['agent_bid'] = $this->trip_model->get_agent_offer($id,get_authenticateUserID());
		
		if(get_authenticateUserID() > 0 && $data['agent_bid'] != 0) {
		
		$agent_bid=$data['agent_bid'];
		//echo "<pre>";
		//print_r($data['agent_bid']);
		
			if($agent_bid) { $chk_bid=1;}
		
			$data['bid_now']=$chk_bid;
			$data['proposal_desc'] = $agent_bid[0]->offer_content;
			$data['offer_build_package']=$agent_bid[0]->offer_build_package;
			$data['offer_amount']=$agent_bid[0]->offer_amount;
			$data['offer_days']=$agent_bid[0]->offer_days;
			$data['offer_nights']=$agent_bid[0]->offer_nights;
			$data['final_offer_amount']=$agent_bid[0]->final_offer_amount;
		//	$data['agent_offer']=$agent_bid[0]->offer_nights;
			$data['trip_offer_id']=$agent_bid[0]->trip_offer_id;
			$data['admin_fees']=$agent_bid[0]->admin_fee;
			$data['final_offer_agent']=$agent_bid[0]->final_offer_amount;
		}
		else
		{
			$data['agent_bid'] = '';
			$data['bid_now']=$chk_bid;
			$data['proposal_desc'] = '';
			$data['offer_build_package']='';
			$data['offer_amount']='';
			$data['offer_days']='';
			$data['offer_nights']='';
			$data['final_offer_amount']='';
			//$data['agent_offer']='';
			$data['trip_offer_id']='';
			$data['admin_fees']='';
			$data['final_offer_agent']='';
		}
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$site_setting = site_setting();
		$data['site_setting']=$site_setting;
		
		
		
		$pageTitle='Trip from-'.$trip_from.'-to-'.$trip_to.'-'.$meta_setting->title;
		$metaDescription='Trip from -'.$trip_from.'-to-'.$trip_to.'-'.$meta_setting->meta_description;
		$metaKeyword='Trip from -'.$trip_from.'-to-'.$trip_to.'-'.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/trip/tripdetail',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
		
	}
	
	public function map($trip_id='')
	{
		$data = array();
		$trip_info = get_one_trip($trip_id);
		$trip_from = $trip_info->trip_from_place;
		$trip_to = $trip_info->trip_to_place;
		$data['trip_info']=$trip_info;
		
		$data['trip_from_lattitude']= $trip_info->trip_from_lattitude;
		$data['trip_from_longitude']= $trip_info->trip_from_longitude;
		$data['trip_to_lattitude']= $trip_info->trip_to_lattitude;
		$data['trip_to_longitude']= $trip_info->trip_to_longitude;
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$pageTitle='Trip map from-'.$trip_from.'-to-'.$trip_to.'-'.$meta_setting->title;
		$metaDescription='Trip map from -'.$trip_from.'-to-'.$trip_to.'-'.$meta_setting->meta_description;
		$metaKeyword='Trip map from -'.$trip_from.'-to-'.$trip_to.'-'.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/trip/tripmap',$data,TRUE);
		$this->template->render();	
	}
	
	
		public function edit($trip_id,$msg='')
	{	
		if(!check_user_authentication()) {  redirect('users/sign_in/'.base64_encode(current_url())); }
		
			
	$trip_info = get_one_trip($trip_id);
		if(!$trip_info)
		{
			redirect('home');
		} else {
		
			if(strtotime($trip_info->trip_confirm_date)>=strtotime(date('Y-m-d H:i:s')) && $trip_info->trip_activity_status==0 && ($trip_info->trip_agent_assign_id==0 || $trip_info->trip_agent_assign_id=='') ) {
			
			}
			else
			{
				redirect('from/'.$trip_info->trip_from_place.'/to/'.$trip_info->trip_to_place.'/'.$trip_info->trip_id.'/donotedit');
			}
		}
				
			
			$data['trip_info']=$trip_info;
		
			
		$data = array();
		$data['msg'] = $msg;
		$data['error'] = '';
		$data['chk_payment_error'] = '';
		$data['chk_date_error'] = '';
		$data['get_currency'] = get_currency();
		
		
		
		if($this->input->post('edittrip')) 
		{
			
			$this->form_validation->set_rules('b_pack', 'Package', 'required');
			$this->form_validation->set_rules('departure_from', 'Departure place', 'required');
			$this->form_validation->set_rules('want_go_to', 'Go to place', 'required');
			$this->form_validation->set_rules('total_day', 'Total day', 'required|numeric');
			
			$this->form_validation->set_rules('min_budget', 'Minimum budget', 'required|numeric');
			$this->form_validation->set_rules('max_budget', 'Maximum budget', 'required|numeric');
			$this->form_validation->set_rules('confirm_date', 'Confirm date', 'required');
			$this->form_validation->set_rules('complete_date', 'Complete date', 'required');
			$this->form_validation->set_rules('destination_place', 'Destination', 'required');
			
			
			
			if($this->input->post('confirm_date'))
			{
				if($this->input->post('confirm_date') > $this->input->post('complete_date'))
				{
					$chk_date_result='<p>The trip complete date is bigger than confirm date.</p>';
				}
				else
				{
					$chk_date_result ='';
				}
				$chk_date_error = $chk_date_result;
			}
			else
			{
				$chk_date_error ='';
			}
			
			if($this->input->post('total_people'))
			{
				$total_person = $this->input->post('total_people');
				$adults = $this->input->post('adult');
				$child = $this->input->post('child');
				$chk_total_person = $adults + $child;
			
				if($chk_total_person > $total_person)
				{
					$chk_result='<p>Please select correct total number of person.</p>';
				}
				else
				{
					$chk_result ='';
				}
				
				$error = $chk_result;
			}
			else
			{
				$error='';
			}
			if($this->input->post('min_budget') && $this->input->post('min_budget') != 0)
			{
				$min_budget = $this->input->post('min_budget');
				$max_budget = $this->input->post('max_budget');
				
				if($min_budget > $max_budget)
				{
					$chk_payment_result='<p>Minimum budget less than maximum budget.</p>';
				}
				else
				{
					$chk_payment_result ='';
				}
				
				$chk_payment_error = $chk_payment_result;
			}
			else
			{
				$chk_payment_error ='';
			}
			
			
			if($this->input->post('min_budget') == 0 && $this->input->post('min_budget') != '')
			{
				$chk_min_budget = $this->input->post('min_budget');
				if($chk_min_budget == 0)
				{
					$chk_min_budget_resulr='<p>Minimum budget more than zero.</p>';
				}
				else
				{
					$chk_min_budget_resulr ='';
				}
				
				$chk_min_budget_error = $chk_min_budget_resulr;
			
			}
			else
			{
				$chk_min_budget_error ='';
			}
		
			if($this->form_validation->run() == FALSE || $error != "" || $chk_payment_error != "" || $chk_date_error !="" || $chk_min_budget_error !='')
			{
				
				
				$data["error"] = validation_errors().$error.$chk_payment_error.$chk_date_error.$chk_min_budget_error;
				
				$data['trip_id']=$this->input->post('trip_id');
				$data['trip_package']=$this->input->post('trip_package');
				$data['departure_from']=$this->input->post('departure_from');
				$data['want_go_to']=$this->input->post('want_go_to');
				$data['total_people']=$this->input->post('total_people');
				$data['adult']=$this->input->post('adult');
				$data['child']=$this->input->post('child');
				$data['total_night']=$this->input->post('total_night');
				$data['total_day']=$this->input->post('total_day');
				$data['room']=$this->input->post('room');
				$data['currency']=$this->input->post('currency');
				$data['min_budget']=$this->input->post('min_budget');
				$data['max_budget']=$this->input->post('max_budget');
				$data['confirm_date']=$this->input->post('confirm_date');
				$data['complete_date']=$this->input->post('complete_date');
				$data['trip_hotel']=$this->input->post('trip_hotel');
				$data['trip_car']=$this->input->post('trip_car');
				$data['trip_plane']=$this->input->post('trip_plane');
				$data['trip_cruises']=$this->input->post('trip_cruises');
				$data['railways'] = $this->input->post('railways');
				$data['b_pack'] = $this->input->post('b_pack');
				$data['destination_place'] = $this->input->post('destination_place');
				$data['special_comment'] =  $this->input->post('special_comment');
				$data['trip_month'] =  $this->input->post('trip_month');

			}
			else
			{

				$trip_update=$this->trip_model->edit_trip();
				$trip_id = $this->input->post('trip_id');
				redirect('trip/edit/'.$trip_id.'/success'); 
				
			}
		}
		else
		{
			$data['trip_id']=$trip_info->trip_id;
			$data['trip_package']=$trip_info->package_type;
			$data['departure_from']=$trip_info->trip_from_location;
			$data['want_go_to']=$trip_info->trip_to_location;
			$data['total_people']=$trip_info->total_people;
			$data['adult']=$trip_info->total_adult;
			$data['child']=$trip_info->total_child;
			$data['total_night']=$trip_info->trip_nights;
			$data['total_day']=$trip_info->trip_days;
			$data['room']=$trip_info->total_room;
			$data['currency']=$trip_info->currency_code_id;
			$data['min_budget']=$trip_info->trip_min_budget;
			$data['max_budget']=$trip_info->trip_max_budget;
			$data['confirm_date']= date('m/d/Y', strtotime($trip_info->trip_confirm_date));
			$data['complete_date']=date('m/d/Y', strtotime($trip_info->trip_must_complete_date));
			$data['trip_hotel']=$trip_info->trip_hotels;
			$data['trip_car']=$trip_info->trip_cars;
			$data['trip_plane']=$trip_info->trip_flights;
			$data['trip_cruises']=$trip_info->trip_cruises;
			$data['railways'] = $trip_info->trip_railways;
			$data['b_pack'] = $trip_info->trip_build_package;
			$data['destination_place'] = $trip_info->trip_destination_places;
			$data['special_comment'] =  $trip_info->trip_special_comment;
			$data['trip_month'] = $trip_info->trip_month;

			
			
			
			
				
		}
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		
		$pageTitle='Post Trip >>'.$meta_setting->title;
		$metaDescription='Post Trip >>'.$meta_setting->meta_description;
		$metaKeyword='Post Trip >>'.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/posttrip/edittrip',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	
	}
	
	
	
	public function post_offer()
	{
		
		if(!check_user_authentication()) {  redirect('sign_up'); }
		
		$data = array();
		 $trip_id = $this->input->post('trip_id');
		
		$trip_info = get_one_trip($trip_id);
		if(!$trip_info)
		{
			redirect('home');
		}
		$data['trip_info']=$trip_info;
		$data['trip_id'] = $trip_id;
		
		$trip_from = $trip_info->trip_from_place;
		$trip_to = $trip_info->trip_to_place;
		
		
		$data['trip_from']=$trip_from;
		$data['trip_to']=$trip_to;
		$data['user_info'] = get_user_profile_by_id($trip_info->user_id);
		$data['similartrip'] = $this->trip_model->get_similar_trip($trip_id,$trip_from,$trip_to);
		
		$data['trip_proposal'] = $this->trip_model->get_trip_proposal($trip_id);
		$data['trip_average'] = $this->trip_model->get_trip_average($trip_id);
		$data['trip_max'] = $this->trip_model->get_trip_high_amount($trip_id);
		$data['trip_low'] = $this->trip_model->get_trip_low_amount($trip_id);
		
		$data['hidden_trip'] =  $this->trip_model->get_hidden_proposal($trip_id);
		$data['decline_trip'] =  $this->trip_model->get_decline_proposal($trip_id);
		$data['invited_trip'] = $this->trip_model->get_invted_proposal($trip_id);
		
		$data['similartrip'] = $this->trip_model->get_similar_trip($trip_id,$trip_from,$trip_to);
		
		$data['agent_bid'] = $this->trip_model->get_agent_offer($trip_id,get_authenticateUserID());
		$agent_bid = $data['agent_bid'];
		if($agent_bid) { $chk_bid=1;}
		
		$data['bid_now']=$chk_bid;
		
		$this->form_validation->set_rules('proposal_desc','Description', 'required');
		$this->form_validation->set_rules('offer_build_package', 'Package', 'required');
		$this->form_validation->set_rules('offer_days', 'Offer Day', 'required|numeric');
		$this->form_validation->set_rules('offer_nights', 'Offer Night', 'required|numeric');
		
			if($this->form_validation->run() == FALSE){
			
				if(validation_errors())
				{
					$data["error"] = validation_errors();
					$data['sow_bid_frm']=1;
				}else{
					$data["error"] = "";
					$data['sow_bid_frm']=0;
				}
				
				if($_POST)
				{
					
					$data['proposal_desc'] = $this->input->post('proposal_desc');
					$data['offer_build_package']= $this->input->post('offer_build_package');
					$data['offer_amount']=$this->input->post('offer_amount');
					$data['offer_days']=$this->input->post('offer_days');
					$data['offer_nights']=$this->input->post('offer_nights');
					$data['final_offer_amount']=$this->input->post('final_offer_amount');
					$data['trip_offer_id']=$this->input->post('trip_offer_id');
					$data['admin_fees']=$this->input->post('admin_fees');
					$data['final_offer_agent']=$this->input->post('final_offer_agent');
				}
				else
				{
					$data['proposal_desc'] = '';
					$data['offer_build_package']='';
					$data['offer_amount']='';
					$data['offer_days']='';
					$data['offer_nights']='';
					$data['final_offer_amount']='';
					$data['trip_offer_id']='';
					$data['admin_fees']='';
					$data['final_offer_agent']='';
					
				}
				
				
			
			} 
			else {
			
				if($this->input->post('trip_offer_id') > 0)
				{
					$trip_offer_id =$this->input->post('trip_offer_id');
					$apply=$this->trip_model->update_post_offer($trip_offer_id);
					redirect('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id.'/update');
				}
				else
				{
					$apply=$this->trip_model->post_offer();
					redirect('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id.'/success');
				}
			    //$data["msg"] = "post sucessfully";
				//$data['offer_post'] = $this->input->post('worker_cities');
				
			}
		
		
		
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$site_setting=site_setting();
		
		$data['site_setting'] = $site_setting;
	
		$user_info=get_user_profile_by_id(get_authenticateUserID());
		$data['user_info']=$user_info;
		
		$pageTitle='Trip from-'.$trip_from.'-to-'.$trip_to.'-'.$meta_setting->title;
		$metaDescription='Trip from -'.$trip_from.'-to-'.$trip_to.'-'.$meta_setting->meta_description;
		$metaKeyword='Trip from -'.$trip_from.'-to-'.$trip_to.'-'.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/trip/tripdetail',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
		
	
	
	
	}
	
	public function delete_trip_offer($trip_offer_id='',$trip_id='')
	{
		
		$this->trip_model->delete_trip_offer($trip_offer_id);
		$trip_info = get_one_trip($trip_id);
		$trip_from = $trip_info->trip_from_place;
		$trip_to = $trip_info->trip_to_place;
		redirect('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id.'/delete');
		
	}
	
	function hidden($trip_offer_id='',$trip_id='')
	{
		$trip_info = get_one_trip($trip_id);
		$trip_from = $trip_info->trip_from_place;
		$trip_to = $trip_info->trip_to_place;
		
		if(get_authenticateUserID() == $trip_info->user_id)
		{
			$data['trip_hidden'] = $this->trip_model->trip_hidden($trip_offer_id);
			redirect('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id.'/hidden');
		}
		else
		{
			redirect('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id);
		}
	}
	
	function decline($trip_offer_id='',$trip_id='')
	{
		$trip_info = get_one_trip($trip_id);
		$trip_from = $trip_info->trip_from_place;
		$trip_to = $trip_info->trip_to_place;
		
		if(get_authenticateUserID() == $trip_info->user_id)
		{
			$data['trip_hidden'] = $this->trip_model->trip_decline($trip_offer_id);
			redirect('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id.'/decline');
		}
		else
		{
			redirect('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id);
		}
	}
	
	function remove_hidden_trip_offer($trip_offer_id='',$trip_id='')
	{
		$trip_info = get_one_trip($trip_id);
		$trip_from = $trip_info->trip_from_place;
		$trip_to = $trip_info->trip_to_place;
		
		if(get_authenticateUserID() == $trip_info->user_id)
		{
			$data['trip_hidden'] = $this->trip_model->remove_hidden_trip_offer($trip_offer_id);
			redirect('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id.'/all');
		}
		else
		{
			redirect('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id);
		}
	}
	
	function askquestion($trip_id='')
	{
		$data['ask_question'] = $this->trip_model->conversation($trip_id);
		
		$trip_info = get_one_trip($trip_id);
		$trip_from = $trip_info->trip_from_place;
		$trip_to = $trip_info->trip_to_place;
		
		redirect('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id.'/ask');
	}
	
	function askquestion_reply($trip_id='')
	{
		$data['ask_question'] = $this->trip_model->conversation_reply($trip_id);
		
		$trip_info = get_one_trip($trip_id);
		$trip_from = $trip_info->trip_from_place;
		$trip_to = $trip_info->trip_to_place;
		
		redirect('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id.'/reply');
	}
	
	function sort_by_rate($trip_id,$srtby='desc',$srttype='all')
	{
	
		if($srttype == 'hidden')
		{
			$data['trip_proposal'] = $this->trip_model->get_hidden_proposal($trip_id);
		}
		else if($srttype == 'decline')
		{
			$data['trip_proposal'] = $this->trip_model->get_decline_proposal($trip_id);
		}
		else if($srttype == 'invited')
		{
			$data['trip_proposal'] = $this->trip_model->get_invted_proposal($trip_id);
		}
		else
		{
			if($srtby == 'desc')
			{
				$data['trip_proposal'] = $this->trip_model->get_trip_proposal($trip_id);
			}
			else
			{
				$data['trip_proposal'] = $this->trip_model->get_trip_proposal($trip_id);
			}
		}
	}
	
	function sort_by_date($trip_id,$srtby='desc',$srttype='all')
	{
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$site_setting=site_setting();
		$data['site_setting'] = $site_setting;
		
		
		if($srttype == 'hidden')
		{
			if($srtby == 'desc')
			{
				$data['hidden_trip'] = $this->trip_model->get_hidden_proposal($trip_id);
			}
			else
			{
				$data['hidden_trip'] = $this->trip_model->get_hidden_proposal_asc($trip_id);
			}
			$this->load->view($theme.'/layout/trip/trip_sort_hidden',$data);
		}
		else if($srttype == 'decline')
		{
			if($srtby == 'desc')
			{
				$data['decline_trip'] = $this->trip_model->get_decline_proposal($trip_id);
			}
			else
			{
				$data['decline_trip'] = $this->trip_model->get_decline_proposal_asc($trip_id);
			}
			
			$this->load->view($theme.'/layout/trip/trip_sort_decline',$data);
		}
		else if($srttype == 'invited')
		{
			if($srtby == 'desc')
			{
				$data['invited_trip'] = $this->trip_model->get_invted_proposal($trip_id);
			}
			else
			{
				$data['invited_trip'] = $this->trip_model->get_invted_proposal_asc($trip_id);
			
			}
			$this->load->view($theme.'/layout/trip/trip_sort_invited',$data);
		}
		else 
		{
		
			if($srtby == 'desc')
			{
				$data['trip_proposal'] = $this->trip_model->get_trip_proposal($trip_id);
			}
			else
			{
				$data['trip_proposal'] = $this->trip_model->get_trip_proposal_asc($trip_id);
				
			}
			$this->load->view($theme.'/layout/trip/trip_sort',$data);
		}
		
		
		
	}
	
	

	
	
}

?>