<?php
/*********************************************************************************
 * This the taskrabbitclone.com  by Rockers Technology. is paid software. It is released under the terms of 
 * the following BSD License.
 * 
 *  Rockers Technologies (Head Office)
 *    5038,Berthpage Dr
 *    suwanee, GA. Zip Code : 30024
    
 *    E-mail Address : nishu@rockersinfo.com
 * 
 * Copyright © 2012-2020 by Rockers Technology , INC a domestic profit corporation has been duly incorporated under
the laws of the state of georiga , USA. www.rockersinfo.com
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this 
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this 
 *   list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.

 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class Search extends IWEB_Controller {
	
	/*
	Function name :Search()
	Description :Its Default Constuctor which called when Search object initialzie.its load necesary models
	*/
	
	function Search()
	{
		parent::__construct();	
		$this->load->model('search_model');	
	}
	
	
	/*
	Function name :index()
	Parameter :none
	Return : none
	Use : 
	Description : site home page its default function which called http://hostname/search
				  SEO friendly URL which is declare in config route.php file  http://hostname/
	*/
	public function index($offset=0)
	{
		
		$limit=10;	
		
		
		$data['limit']=$limit;
		$data['per_page'] = $limit;
		
		$s_array=array('asc','desc');
		
		
		
		$from_place='';
		$to_place='';
		$min_amount='';
		$max_amount='';
		$package_type='';
		$time_left_hours='';
		$post_date='';
		$sort_type='';
		
		
		if(isset($_REQUEST['from_place'])) { if($_REQUEST['from_place']!='') {  $from_place=$_REQUEST['from_place'];   } }
		if(isset($_REQUEST['to_place'])) { if($_REQUEST['to_place']!='') {  $to_place=$_REQUEST['to_place'];   } }
		if(isset($_REQUEST['min_amount'])) { if($_REQUEST['min_amount']!='') {  $min_amount=$_REQUEST['min_amount'];   } }
		if(isset($_REQUEST['max_amount'])) { if($_REQUEST['max_amount']!='') {  $max_amount=$_REQUEST['max_amount'];   } }
		if(isset($_REQUEST['package_type'])) { if($_REQUEST['package_type']!='') {  $package_type=$_REQUEST['package_type'];   } }
		if(isset($_REQUEST['time_left_hours'])) { if($_REQUEST['time_left_hours']!='') {  $time_left_hours=$_REQUEST['time_left_hours'];   } }
		if(isset($_REQUEST['post_date'])) { if($_REQUEST['post_date']!='') {  $post_date=$_REQUEST['post_date'];   } }
		if(isset($_REQUEST['sort_type'])) { if($_REQUEST['sort_type']!='') {  $sort_type=$_REQUEST['sort_type'];   } }
		
		if(isset($_REQUEST['per_page'])) { if($_REQUEST['per_page']!='') {  $offset=$_REQUEST['per_page'];   } }
		
		
		if(!in_array($sort_type,$s_array))
		{
			$sort_type='desc';
		}
				
		
		
		
		
		
		$data['from_place']=$from_place;
		$data['to_place']=$to_place;
		$data['min_amount']=$min_amount;
		$data['max_amount']=$max_amount;
		$data['package_type']=$package_type;
		$data['time_left_hours']=$time_left_hours;
		$data['post_date']=$post_date;	
		$data['sort_type']=$sort_type;
		
		$data['offset']=$offset;
		
		
		
		
		$site_setting=site_setting();
		$data['site_setting']=$site_setting;
		
		$this->load->library('pagination');
		
		
	
			
		$config['uri_segment']='3';	
		
		$set_url = 'http://'.$_SERVER['HTTP_HOST'].substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], "/")+1).'search/trip?from_place='.$from_place.'&to_place='.$to_place.'&min_amount='.$min_amount.'&max_amount='.$max_amount.'&package_type='.$package_type.'&time_left_hours='.$time_left_hours.'&post_date='.$post_date.'&sort_type='.$sort_type;
		
			
		$config['base_url'] = $set_url;
		$config['page_query_string']=true;
		
		$config['total_rows'] = $this->search_model->get_total_trip($from_place,$to_place,$min_amount,$max_amount,$package_type,$time_left_hours,$post_date,$sort_type);
		$config['per_page'] = $limit;
		
		$data['total_rows']=$config['total_rows'];
		
		
		$this->pagination->initialize($config);		
			
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->search_model->get_trip_result($limit,$offset,$from_place,$to_place,$min_amount,$max_amount,$package_type,$time_left_hours,$post_date,$sort_type);
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		
		$pageTitle='Search Trip >> '.$meta_setting->title;
		$metaDescription='Search Trip >> '.$meta_setting->meta_description;
		$metaKeyword='Search Trip >> '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/search/search',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	}
	
	
	public function trip_search_ajax($offset=0)
	{
		
		$limit=10;	
		
		
		$data['limit']=$limit;
		$data['per_page'] = $limit;
		
		$s_array=array('asc','desc');
		
		
		
		$from_place='';
		$to_place='';
		$min_amount='';
		$max_amount='';
		$package_type='';
		$time_left_hours='';
		$post_date='';
		$sort_type='';
		
		
		if(isset($_REQUEST['from_place'])) { if($_REQUEST['from_place']!='') {  $from_place=$_REQUEST['from_place'];   } }
		if(isset($_REQUEST['to_place'])) { if($_REQUEST['to_place']!='') {  $to_place=$_REQUEST['to_place'];   } }
		if(isset($_REQUEST['min_amount'])) { if($_REQUEST['min_amount']!='') {  $min_amount=$_REQUEST['min_amount'];   } }
		if(isset($_REQUEST['max_amount'])) { if($_REQUEST['max_amount']!='') {  $max_amount=$_REQUEST['max_amount'];   } }
		if(isset($_REQUEST['package_type'])) { if($_REQUEST['package_type']!='') {  $package_type=$_REQUEST['package_type'];   } }
		if(isset($_REQUEST['time_left_hours'])) { if($_REQUEST['time_left_hours']!='') {  $time_left_hours=$_REQUEST['time_left_hours'];   } }
		if(isset($_REQUEST['post_date'])) { if($_REQUEST['post_date']!='') {  $post_date=$_REQUEST['post_date'];   } }
		if(isset($_REQUEST['sort_type'])) { if($_REQUEST['sort_type']!='') {  $sort_type=$_REQUEST['sort_type'];   } }
		
		if(isset($_REQUEST['per_page'])) { if($_REQUEST['per_page']!='') {  $offset=$_REQUEST['per_page'];   } }
		
		
		if(!in_array($sort_type,$s_array))
		{
			$sort_type='desc';
		}
				
		
		
		
		
		
		$data['from_place']=$from_place;
		$data['to_place']=$to_place;
		$data['min_amount']=$min_amount;
		$data['max_amount']=$max_amount;
		$data['package_type']=$package_type;
		$data['time_left_hours']=$time_left_hours;
		$data['post_date']=$post_date;	
		$data['sort_type']=$sort_type;
		
		$data['offset']=$offset;
		
		
		
		
		$site_setting=site_setting();
		$data['site_setting']=$site_setting;
		
		$this->load->library('pagination');
		
		
		$config['uri_segment']='3';	
		
		$set_url = 'http://'.$_SERVER['HTTP_HOST'].substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], "/")+1).'search/trip?from_place='.$from_place.'&to_place='.$to_place.'&min_amount='.$min_amount.'&max_amount='.$max_amount.'&package_type='.$package_type.'&time_left_hours='.$time_left_hours.'&post_date='.$post_date.'&sort_type='.$sort_type;
		
			
		$config['base_url'] = $set_url;
		$config['page_query_string']=true;
		
		$config['total_rows'] = $this->search_model->get_total_trip($from_place,$to_place,$min_amount,$max_amount,$package_type,$time_left_hours,$post_date,$sort_type);
		$config['per_page'] = $limit;
		
		$data['total_rows']=$config['total_rows'];
		
		
		$this->pagination->initialize($config);		
			
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->search_model->get_trip_result($limit,$offset,$from_place,$to_place,$min_amount,$max_amount,$package_type,$time_left_hours,$post_date,$sort_type);
		
		
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		
		$this->load->view($theme.'/layout/search/search_ajax',$data);
		//exit;
	}
	
	
	public function total_trip_search_ajax($offset=0)
	{
		
		
		$s_array=array('asc','desc');
		
		
		$from_place='';
		$to_place='';
		$min_amount='';
		$max_amount='';
		$package_type='';
		$time_left_hours='';
		$post_date='';
		$sort_type='';
		
		
		if(isset($_REQUEST['from_place'])) { if($_REQUEST['from_place']!='') {  $from_place=$_REQUEST['from_place'];   } }
		if(isset($_REQUEST['to_place'])) { if($_REQUEST['to_place']!='') {  $to_place=$_REQUEST['to_place'];   } }
		if(isset($_REQUEST['min_amount'])) { if($_REQUEST['min_amount']!='') {  $min_amount=$_REQUEST['min_amount'];   } }
		if(isset($_REQUEST['max_amount'])) { if($_REQUEST['max_amount']!='') {  $max_amount=$_REQUEST['max_amount'];   } }
		if(isset($_REQUEST['package_type'])) { if($_REQUEST['package_type']!='') {  $package_type=$_REQUEST['package_type'];   } }
		if(isset($_REQUEST['time_left_hours'])) { if($_REQUEST['time_left_hours']!='') {  $time_left_hours=$_REQUEST['time_left_hours'];   } }
		if(isset($_REQUEST['post_date'])) { if($_REQUEST['post_date']!='') {  $post_date=$_REQUEST['post_date'];   } }
		if(isset($_REQUEST['sort_type'])) { if($_REQUEST['sort_type']!='') {  $sort_type=$_REQUEST['sort_type'];   } }
		
		
		
		if(!in_array($sort_type,$s_array))
		{
			$sort_type='desc';
		}
		
		$data['sort_type']=$sort_type;
		
		
		
		$data['from_place']=$from_place;
		$data['to_place']=$to_place;
		$data['min_amount']=$min_amount;
		$data['max_amount']=$max_amount;
		$data['package_type']=$package_type;
		$data['time_left_hours']=$time_left_hours;
		$data['post_date']=$post_date;
		
		
	
		echo $total_rows = $this->search_model->get_total_trip($from_place,$to_place,$min_amount,$max_amount,$package_type,$time_left_hours,$post_date,$sort_type);
		exit;
		
	}
	
	
}?>