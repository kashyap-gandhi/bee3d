<?php
class Wallet extends IWEB_Controller 
{
	
	/*
	Function name :wallet()
	Description :Its Default Constuctor which called when wallet object initialzie.its load necesary models
	*/
	
	function Wallet()
	{
		parent::__construct();	
		$this->load->model('wallet_model');	
		$this->load->model('home_model');	
		$this->load->model('user_model');	
		$this->load->model('design_model');
		$this->load->model('package_model');
		$this->load->model('challenge_model');
		
	}
	
	/*
	Function name :index()
	Parameter : $offset (for paging) , $msg (custom message)
	Return : array list of all user wallet transaction
	Use : User wallet history
	Description :its default function which called http://hostname/wallet	
	*/
	
	public function index($offset=0,$msg='')
	{
		
		if(!check_user_authentication()) {  redirect('sign_up'); } 
		
		$user_info=get_user_info(get_authenticateUserID());
		$wallet_setting=wallet_setting();		
		if($wallet_setting->wallet_enable==0)
		{
			redirect('dashboard');
		}
		
		
		
		$this->load->library('pagination');		
		$limit = '15';
		//$config['uri_segment']='4';
		$config['base_url'] = site_url('wallet/index');
		$config['total_rows'] = $this->wallet_model->get_total_my_wallet_list();
		$config['per_page'] = $limit;				
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();		
		$data['limit']=$limit;
		$data['total_rows']=$config['total_rows'];	
		$data['offset']=$offset;			
		$data['wallet_details']=$this->wallet_model->my_wallet_list($offset,$limit);	
		$data["active_menu"] = "wallet";
		/*echo "<pre>";
		print_r($data['wallet_details']);
		die;*/
			
		$data['total_wallet_amount']=$this->wallet_model->my_wallet_amount(); 		
		$data['wallet_setting']=$wallet_setting;	
		$data['active_menu']='wallet';	
		$data['msg']=$msg;		
		$theme = getThemeName();
		$data['site_setting']=site_setting();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;		
		$meta_setting=meta_setting();		
		$pageTitle='Wallet History-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->title;
		$metaDescription='Wallet History-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->meta_description;
		$metaKeyword='Wallet History-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->meta_keyword;		
		
		$site_setting = site_setting();
		$data['site_setting'] = site_setting();

		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/wallet/index',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	

		
	}
	
	
	/*
	Function name :add_wallet()
	Parameter : none
	Return : none, redirect to payment gateway function
	Use : User can add amount in wallet using any active installed payment gateway 
	Description : user can add amount in his/her wallet using this function which called http://hostname/add_wallet
	*/
	
	
	function add_wallet()
	{	
		
		//redirect('wallet');
		
		if(!check_user_authentication()) {  redirect('sign_up'); } 
		
	    $site_setting=site_setting();
		$wallet_setting=$this->wallet_model->wallet_setting();		
		if($wallet_setting->wallet_enable==0)
		{
			redirect('dashboard');
		}		
		
		
		$minimum_amount=$wallet_setting->wallet_minimum_amount;		
		$chk_amt=$this->input->post('credit');		
		$amount_error='fail';

		if($this->input->post('credit') >= 0)
		{
		
			if($chk_amt<$minimum_amount)
			{
				$amount_error='fail';
			} else {
				$amount_error='success';
			}
		
		}

		$this->load->library('form_validation');		
		$this->form_validation->set_rules('credit','Points', 'required|numeric');
		$this->form_validation->set_rules('gateway_type', 'Gateway Type', 'required');
		
		if($this->form_validation->run() == FALSE || $amount_error=='fail')
		{	
				if($amount_error=='fail')
				{
					$amount_error="<p>". sprintf('You can not add less then points %s in wallet.',$site_setting->currency_symbol.$minimum_amount)."</p>"; 
				}
				else
				{
					$amount_error='';
				}	
			
				if(validation_errors() || $amount_error!='')
				{
					$data['error'] = validation_errors().$amount_error;
							
				} else{
					$data["error"] = "";
				}	
		
				$data['payment'] = $this->wallet_model->get_paymentact_result();				
				$data['wallet_setting']=$wallet_setting;		
				$data["credit"] = $this->input->post('credit');
				$data["gateway_type"] = $this->input->post('gateway_type');				
				$data['total_wallet_amount']=$this->wallet_model->my_wallet_amount(); 			
				
			
				$theme = getThemeName();
				$data['site_setting']=site_setting();
				$data["active_menu"] = "wallet";
				$this->template->set_master_template($theme .'/template.php');				
				$data['theme']=$theme;				
				$data['active_menu']='wallet';
				$meta_setting=meta_setting();
				$user_info = get_user_info(get_authenticateUserID());				
				$pageTitle='Deposit-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->title;
				$metaDescription='Deposit-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->meta_description;
				$metaKeyword='Deposit-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->meta_keyword;	
				
				$site_setting= site_setting();

				$this->template->write('pageTitle',$pageTitle,TRUE);
				$this->template->write('metaDescription',$metaDescription,TRUE);
				$this->template->write('metaKeyword',$metaKeyword,TRUE);
				$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
				$this->template->write_view('content_center',$theme .'/layout/wallet/add_wallet',$data,TRUE);
				$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
				$this->template->render();	
			}
			else
			{	
				$gateway_id=$this->input->post('gateway_type');	
				$wallet_add_fees=$wallet_setting->wallet_add_fees;
				$amount=$this->input->post('credit');
		    	//$add_fees= number_format((($amount * $wallet_add_fees)/100),2);
				//$total= number_format(($add_fees + $amount),2);				
				
				$total=$amount;
				
				$total= str_replace(',','',$total);
				
				$modname='wallet';
			    $pay=$this->wallet_model->get_paymentid_result($gateway_id);	
				//var_dump($pay);exit;
				redirect('/wallet/'.trim($pay->function_name).'/'.$pay->id.'/'.$total);		
				
			}
	}
	
	
	
	///---------paypal transaction function start from here
	
	
	
	/*
	Function name :paypal()
	Parameter : $id (payment gateway unquie ID), $amt (user added amount), $task_id (use for pay amount for user posted task), $task_comment_id (used for getting runner offer price)
	Return : redirect to paypal.com
	Use : User total amount and othet paypal setting which is set by administrator for adding amount in admin live paypal account
	Description : user added amount pass to paypal.com where user pay amount or cancel transaction
	*/
	
	
	
	function paypal($id,$amt,$task_id=0,$task_comment_id=0)
	{
			
			if(!check_user_authentication()) {  redirect('sign_up'); } 
			
			
			$num='WL'.randomCode();
			
			$this->load->library('paypal_lib');
			
			$Paypal_Email=$this->wallet_model->get_gateway_result($id,'paypal_email');			
			$Paypal_Status=$this->wallet_model->get_gateway_result($id,'site_status');
			$Paypal_Status=(array) $Paypal_Status;
			$Paypal_Email=(array) $Paypal_Email;
			
			$Paypal_Url='https://www.sandbox.paypal.com/cgi-bin/webscri';
			
			if($Paypal_Status['value']=='sandbox')
			{
				$Paypal_Url='https://www.sandbox.paypal.com/cgi-bin/webscri';
			}
			if($Paypal_Status['value']=='live')
			{
				$Paypal_Url='https://www.paypal.com/cgi-bin/webscri';
			}
			
			
			
				$wallet_setting=$this->wallet_model->wallet_setting();			
				$wallet_add_fees=$wallet_setting->wallet_add_fees;
				
				
				if($task_id!=0 && $task_id>0 && $task_comment_id!=0 && $task_comment_id>0)
				{
						
						$task_detail=$this->task_model->get_tasks_detail_by_id($task_id);
						
						if($task_detail)
						{
						
							///////==========total amount====
				
							$task_setting=task_setting();
							
							$total=0;
							
							if($task_detail->extra_cost>0) {
							
							$total=$total+$task_detail->extra_cost;
							
							}
							
							
							
							////===get worker offer price=====
					
							$worker_id='';
							
							$get_worker_detail=$this->db->query("select * from ".$this->db->dbprefix('user')." us, ".$this->db->dbprefix('user_profile')." up, ".$this->db->dbprefix('worker')." wrk, ".$this->db->dbprefix('worker_comment')." cmt where cmt.comment_post_user_id=us.user_id and us.user_id=up.user_id and us.user_id=wrk.user_id and cmt.task_comment_id='".$task_comment_id."'");
							
							
							if($get_worker_detail->num_rows()>0)
							{
							
								$comment_detail=$get_worker_detail->row();
								
								$worker_id=$comment_detail->worker_id;
							
									
								$total=$total+$comment_detail->offer_amount;
							 
							}
							 
							 ///////=======
					 
							 
							 
							 if($task_setting->task_post_fee>0) {
							 
							 $task_site_fee=number_format((($task_setting->task_post_fee*$total) / 100),2); 
						
								 $total=$total+$task_site_fee;
						
							}
								 
								 
								  $total=number_format($total,2);
								
								
						
								$amt=$total;		
		
		
				///////==========total amount====
				
						}
						
						
				
				}
				
				
				$amount=$amt;	
				
				
				
		    	$add_fees= number_format((($amount * $wallet_add_fees)/100),2);
				$total= number_format(($add_fees + $amount),2);
				
				$total= str_replace(',','',$total);
				
			
			$meta_setting=meta_setting();
			$user_info = get_user_info(get_authenticateUserID());
			$data['site_setting']=(array)site_setting();
			$user_detail=(array)$user_info;
			$site_setting=$data['site_setting'];
			
			$paypal_form = '<div data-role="page" data-url="/mobiletaskrabbit-clone/wallet/add_wallet" class="ui-page ui-body-c ui-page-active" style="min-height: 383px;">';
			$paypal_form .= '<form name="paypal_form" action="'.$Paypal_Url.'" method="post">';
			$paypal_form .= '<input type="hidden" name="rm" value="2" >';
			$paypal_form .= '<input type="hidden" name="cmd" value="_xclick" >';
			$paypal_form .= '<input type="hidden" name="quantity" value="1" >';
			$paypal_form .= '<input type="hidden" name="currency_code" value="'.$site_setting['currency_code'].'" >';
			$paypal_form .= '<input type="hidden" name="business" value="'.$Paypal_Email['value'].'" >';
			$paypal_form .= '<input type="hidden" name="return" value="'.site_url('wallet/paypalsuccess/'.$task_id.'/'.$task_comment_id).'" >';
			$paypal_form .= '<input type="hidden" name="cancel_return" value="'.site_url('wallet/paypalcancel/'.$task_id.'/'.$task_comment_id).'" >';
	  		$paypal_form .= '<input type="hidden" name="notify_url" value="'.site_url('wallet/paypalipn').'" >';
			$paypal_form .= '<input type="hidden" name="custom" value="'.$amt.'#'.$id.'#'.get_authenticateUserID().'" >';
			$paypal_form .= '<input type="hidden" name="item_name" value="New Fund added in the Wallet in '.$site_setting['site_name'].' by '.$user_detail['first_name'].' '.$user_detail['last_name'].'" >';
			$paypal_form .= '<input type="hidden" name="item_number" value="'.$num.'" >';
			$paypal_form .= '<input type="hidden" name="amount" value="'.$total.'" >';
			$paypal_form .= '<input type="submit" value="Click here to redirected paypal" name="pp_submit" class="ui-btn-hidden" aria-disabled="false">';
			$paypal_form .= '</form>';
			$paypal_form .= '</div>';
			
			echo $paypal_form;
			
			/*$this->paypal_lib->add_field('currency_code',$site_setting['currency_code']);
			$this->paypal_lib->add_field('business', $Paypal_Email['value']);
			//$this->paypal_lib->add_field('return', site_url('wallet/paypalsuccess/'.$task_id.'/'.$task_comment_id));
			$this->paypal_lib->add_field('return', site_url('wallet/paypalsuccess'));
			$this->paypal_lib->add_field('cancel_return', site_url('wallet/paypalcancel/'.$task_id.'/'.$task_comment_id));
			$this->paypal_lib->add_field('notify_url', site_url('wallet/paypalipn/')); // <-- IPN url
			$this->paypal_lib->add_field('custom', $amt.'#'.$id.'#'.get_authenticateUserID()); // <-- Verify return
			$this->paypal_lib->paypal_url= $Paypal_Url;			
						
			$this->paypal_lib->add_field('item_name', 'New Fund added in the Wallet in '.$site_setting['site_name'].' by '.$user_detail['first_name'].' '.$user_detail['last_name']);
			$this->paypal_lib->add_field('item_number', $num);
			$this->paypal_lib->add_field('amount', $total);
	
			$this->paypal_lib->button('Add Amount to wallet');
			
			$data['paypal_form'] = $this->paypal_lib->paypal_auto_form();*/
			
	}
	
	
	
	
	/*
	Function name :paypalsuccess()
	Parameter : $task_id (use for pay amount for user posted task), $task_comment_id (used for getting runner offer price)
	Return : redirect to wallet history page function or task detail page
	Use : When user successfully paid amount on paypal then user will redirect here.
	Description : paypal.com redirect to this function which called http://hostname/paypalsuccess
	*/
	
	
	
		
	function paypalsuccess($task_id=0,$task_comment_id=0)
	{
			
			if($_POST) {   
				  $custom  = explode('#',$_POST['custom']);
				  $task_id = $custom[1];
				  
				 	if(isset($custom[3])) {
					  $task_comment_id = $custom[3];
					} else {
						$task_comment_id = 0;
					}
			 }
			 
			 $msg= "add";
			  
			
				if($task_id!=0 && $task_id>0 && $task_comment_id!=0 && $task_comment_id>0)
				{
						
						$task_detail=$this->task_model->get_tasks_detail_by_id($task_id);
						
						if($task_detail)
						{
							redirect('task/accept_offer/'.$task_id.'/'.$task_comment_id);
						}
						else
						{
							redirect('wallet/index/0/'.$msg);	
						}
						
				}
				else
				{
					redirect('wallet/index/0/'.$msg);	
				}
			
    }	
	
	
	
	/*
	Function name :paypalcancel()
	Parameter : $task_id (use for pay amount for user posted task), $task_comment_id (used for getting runner offer price)
	Return : redirect to wallet history page function or task detail page
	Use : When user cancel transaction on paypal then user will redirect here.
	Description : paypal.com redirect to this function which called http://hostname/paypalcancel
	*/
	
	
	function paypalcancel($task_id=0,$task_comment_id=0)
	{		
			$msg= "fail";
			
				if($task_id!=0 && $task_id>0 && $task_comment_id!=0 && $task_comment_id>0)
				{
						
						$task_detail=$this->task_model->get_tasks_detail_by_id($task_id);
						
						if($task_detail)
						{
							redirect('tasks/'.$task_detail->task_url_name);
						}
						else
						{
							redirect('wallet/index/0/'.$msg);	
						}
						
				}
				else
				{
					redirect('wallet/index/0/'.$msg);		
				}
	}
	
	
	/*
	Function name :paypalipn()
	Parameter : none
	Return : none
	Use : When user successfully paid amount on paypal then user transaction information send in this function.
	Description : paypal.com send user transaction information to this function which called http://hostname/paypalcancel
	*/

	function paypalipn()
	{	
		$vals = array();
		$strtemp='';
		foreach ($_POST as $key => $value) 
		{					
			$vals[$key] = $value;
			$strtemp.= $key."=".$value.",";
		}
		      
		$wallet_setting=wallet_setting();		
		
		
		$no_payment_after_auto_confirm=$wallet_setting->no_payment_after_auto_confirm;
		
			
			$status=$_POST['payment_status'];
			$custom=explode('#',$_POST['custom']);
			$gateway_id=$custom[1];
			$custom_amt=$custom[0];
			$pay_gross=$_POST['mc_gross'];
			$payee_email=$_POST['payer_email'];
			$payer_status =$_POST['payer_status'];
			$txn_id=$_POST['txn_id'];
			$date=date('Y-m-d H:i:s');
			$ip=$_SERVER['REMOTE_ADDR'];
			$user_id=$custom[2];
			$user_info = get_user_info($user_id);
			$login_user=(array)$user_info;
			
			
			$chk_transaction_id=$this->wallet_model->check_unique_transaction($txn_id);
			
			
			$strtemp.= "chk_transaction_id=".$chk_transaction_id.",";
			
			
			log_message('error',"PAYPAL IPN DATA:".$strtemp);
			
			if((strtolower($status)=='completed' || strtolower($status)=='pending') && $pay_gross>=$custom_amt && $chk_transaction_id==0)
			{
				$admin_status='Review';
					
				$gateway_details=get_payment_gateway_details_by_id($gateway_id);
					
					if($gateway_details)
					{
						if($gateway_details->auto_confirm==1)
						{
							$admin_status='Confirm';
						}
					}
					
					
				  
				  
				    $chk_status_admin=$this->db->query("select * from ".$this->db->dbprefix('wallet')." where user_id='".$user_id."' and admin_status='Confirm'");
					
					
					if($chk_status_admin->num_rows()>$no_payment_after_auto_confirm)
					{
							$admin_status='Confirm';
					}
					
					
					
					
					
					$data=array(
					'debit' => $custom_amt,
					'user_id' => $user_id,
					'status' => $payer_status,
					'admin_status' => $admin_status,
					'wallet_date' => $date,
					'wallet_transaction_id' => $txn_id,
					'wallet_payee_email' => $payee_email,
					'wallet_ip' => $ip	,
					'gateway_id' => $gateway_id				
					);					
					$add_wallet=$this->db->insert('wallet',$data);										
			}
				
	}
	
	
	
	
	///---------paypal credit card function start from here
	
	
	
	
	/*
	Function name :creditcard()
	Parameter : $id (payment gateway unquie ID), $amt (user added amount), $task_id (use for pay amount for user posted task), $task_comment_id (used for getting runner offer price)
	Return : none
	Use : User total amount and othet credit card setting which is set by administrator used for adding amount in admin live account
	Description : user can enter his/her credit card information and pay amount or cancel transaction. This function which called http://hostname/creditcard
	
	NOTE : User Credit Card Information is not save any where in this script or database.
	*/
	
	
	function creditcard($id='',$amt='',$task_id='',$task_comment_id='')
	{
		
		if(!check_user_authentication()) {  redirect('sign_up'); } 
		
		$wallet_setting=wallet_setting();
		$data['wallet_setting']=$wallet_setting;
		
		$site_setting=site_setting();
			$data['site_setting']=site_setting();
		
		$this->load->library('form_validation');		
		$this->form_validation->set_rules('firstName', $this->lang->line('wallet.fname '), 'required');
		$this->form_validation->set_rules('lastName', $this->lang->line('wallet.lname'), 'required');
		
		$this->form_validation->set_rules('creditCardNumber', $this->lang->line('wallet.card_number'), 'required|integer|numeric');	
		$this->form_validation->set_rules('creditCardType', $this->lang->line('wallet.card_type'), 'required|alpha');
		
		$this->form_validation->set_rules('expDateMonth', $this->lang->line('temp.expiration_month'), 'required|integer');
		$this->form_validation->set_rules('expDateYear', $this->lang->line('temp.expiration_year'), 'required|integer');
		
	    $this->form_validation->set_rules('amount','points', 'required|numeric');	
			
			
		$this->form_validation->set_rules('address1',$this->lang->line('wallet.address1'), 'required');
		$this->form_validation->set_rules('city',$this->lang->line('wallet.city'), 'required|alpha_space');
		$this->form_validation->set_rules('state', $this->lang->line('wallet.state'), 'required|alpha_space');
		$this->form_validation->set_rules('zip', $this->lang->line('wallet.postal_code'), 'required|alpha_numeric|min_length['.$site_setting->zipcode_min.']|max_length['.$site_setting->zipcode_max.']');	
		
		
		$data['paymentType']='Sale';
		$data['id']=$id;
		
		$data['task_id']=$task_id;
			$data['task_comment_id']=$task_comment_id;
		
		
		
		if($task_id!=0 && $task_id>0 && $task_comment_id!=0 && $task_comment_id>0)
		{
						
						$task_detail=$this->task_model->get_tasks_detail_by_id($task_id);
						
						if($task_detail)
						{
						
							///////==========total amount====
				
							$task_setting=task_setting();
							
							$total=0;
							
							if($task_detail->extra_cost>0) {
							
							$total=$total+$task_detail->extra_cost;
							
							}
							
							
							
							////===get worker offer price=====
					
							$worker_id='';
							
							$get_worker_detail=$this->db->query("select * from ".$this->db->dbprefix('user')." us, ".$this->db->dbprefix('user_profile')." up, ".$this->db->dbprefix('worker')." wrk, ".$this->db->dbprefix('worker_comment')." cmt where cmt.comment_post_user_id=us.user_id and us.user_id=up.user_id and us.user_id=wrk.user_id and cmt.task_comment_id='".$task_comment_id."'");
							
							
							if($get_worker_detail->num_rows()>0)
							{
							
								$comment_detail=$get_worker_detail->row();
								
								$worker_id=$comment_detail->worker_id;
							
									
								$total=$total+$comment_detail->offer_amount;
							 
							}
							 
							 ///////=======
					 
							 
							 
							 if($task_setting->task_post_fee>0) {
							 
							 $task_site_fee=number_format((($task_setting->task_post_fee*$total) / 100),2); 
						
								 $total=$total+$task_site_fee;
						
							}
								 
								 
								  $total=number_format($total,2);
								
								
						
								$amt=$total;		
		
		
				///////==========total amount====
				
						}
						
						
				
				}
				
		$data['amt']=$amt;		
				
				
		
		
		if($this->form_validation->run() == FALSE )
		{
		
		  	  if(validation_errors())
				{													
					$data["error"] = validation_errors();
				}
				else
				{		
					$data["error"] = "";							
				}
				
				
			    $theme = getThemeName();
			
				$this->template->set_master_template($theme .'/template.php');				
				$data['theme']=$theme;				
				$meta_setting=meta_setting();
				$data["active_menu"] = "wallet";
				$user_info = get_user_info(get_authenticateUserID());
				$data['first_name']=$user_info->first_name;		
				$data['last_name']=$user_info->last_name;		
				$data['total_wallet_amount']=$this->wallet_model->my_wallet_amount(); 	
				$pageTitle='Deposit-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->title;
				$metaDescription='Deposit-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->meta_description;
				$metaKeyword='Deposit-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->meta_keyword;	
				
				$site_setting= site_setting();

				if($this->agent->is_mobile('iphone') && $site_setting->mobile_site == 1)
				{
					$this->template->set_master_template('iphone/template.php');
					$this->template->write_view('content_center', 'iphone/layout/wallet/creditcard', $data, TRUE);
					$this->template->render();
				}
				elseif(($this->agent->is_mobile() || $this->agent->is_robot()) && $site_setting->mobile_site == 1)
				{
					$this->template->set_master_template('mobile/template.php');
					$this->template->write_view('content_center', 'mobile/layout/wallet/creditcard', $data, TRUE);
					$this->template->render();
				}
				else {
					
					$this->template->write('pageTitle',$pageTitle,TRUE);
					$this->template->write('metaDescription',$metaDescription,TRUE);
					$this->template->write('metaKeyword',$metaKeyword,TRUE);
					$this->template->write_view('header',$theme .'/layout/common/header_login',$data,TRUE);
					$this->template->write_view('content_center',$theme .'/layout/wallet/creditcard',$data,TRUE);
					$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
					$this->template->render();	
				}
		}
		else
		{
			$num='WL'.randomCode();			
			$this->load->library('paypal_lib');		
			$this->load->library('creditcard');		
			$gateways=$this->wallet_model->get_gateway_detailByid($id);	
			$config=array();		
			//$gateways=(array) $gateways;
				//var_dump($gateways);
			//exit;
			foreach($gateways as $gatewaydetail)
			{
			$gatewaydetail1=(array) $gatewaydetail;
			$config[$gatewaydetail1["name"]]=$gatewaydetail1["value"];
			
			}
			$crditobj=$this->creditcard->config($config);
			//exit;
			/**
			 * Get required parameters from the web form for the request
			 */
			$paymentType =urlencode( $_POST['paymentType']);
			$firstName =urlencode( $_POST['firstName']);
			$lastName =urlencode( $_POST['lastName']);
			$creditCardType =urlencode( $_POST['creditCardType']);
			$creditCardNumber = urlencode($_POST['creditCardNumber']);
			$expDateMonth =urlencode( $_POST['expDateMonth']);
			
			// Month must be padded with leading zero
			$padDateMonth = str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
			
			$expDateYear =urlencode( $_POST['expDateYear']);
			$cvv2Number = urlencode($_POST['cvv2Number']);
			$address1 = urlencode($_POST['address1']);
			$address2 = urlencode($_POST['address2']);
			$city = urlencode($_POST['city']);
			$state =urlencode( $_POST['state']);
			$zip = urlencode($_POST['zip']);
			
			
			$amount = urlencode($_POST['amount']);
			
			
			if($task_id!=0 && $task_id>0 && $task_comment_id!=0 && $task_comment_id>0)
			{
						
						$task_detail=$this->task_model->get_tasks_detail_by_id($task_id);
						
						if($task_detail)
						{
						
							///////==========total amount====
				
							$task_setting=task_setting();
							
							$total=0;
							
							if($task_detail->extra_cost>0) {
							
							$total=$total+$task_detail->extra_cost;
							
							}
							
							
							
							////===get worker offer price=====
					
							$worker_id='';
							
							$get_worker_detail=$this->db->query("select * from ".$this->db->dbprefix('user')." us, ".$this->db->dbprefix('user_profile')." up, ".$this->db->dbprefix('worker')." wrk, ".$this->db->dbprefix('worker_comment')." cmt where cmt.comment_post_user_id=us.user_id and us.user_id=up.user_id and us.user_id=wrk.user_id and cmt.task_comment_id='".$task_comment_id."'");
							
							
							if($get_worker_detail->num_rows()>0)
							{
							
								$comment_detail=$get_worker_detail->row();
								
								$worker_id=$comment_detail->worker_id;
							
									
								$total=$total+$comment_detail->offer_amount;
							 
							}
							 
							 ///////=======
					 
							 
							 
							 if($task_setting->task_post_fee>0) {
							 
							 $task_site_fee=number_format((($task_setting->task_post_fee*$total) / 100),2); 
						
								 $total=$total+$task_site_fee;
						
							}
								 
								 
								  $total=number_format($total,2);
								
								
						
								$amount=$total;		
		
		
				///////==========total amount====
				
						}
						
						
				
				}
			
			
			
			
				$wallet_setting=$this->wallet_model->wallet_setting();			
				$wallet_add_fees=$wallet_setting->wallet_add_fees;
				
					
				
		    	$add_fees= number_format((($amount * $wallet_add_fees)/100),2);
				$total= number_format(($add_fees + $amount),2);
				
				$total= str_replace(',','',$total);
			
			
			
			
			
			//$currencyCode=urlencode($_POST['currency']);
			$currencyCode="USD";
			$paymentType=urlencode($_POST['paymentType']);
			
			/* Construct the request string that will be sent to PayPal.
			   The variable $nvpstr contains all the variables and is a
			   name value pair string with & as a delimiter */
			$nvpstr="&PAYMENTACTION=$paymentType&AMT=$total&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=".         $padDateMonth.$expDateYear."&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName&STREET=$address1&CITY=$city&STATE=$state".
			"&ZIP=$zip&COUNTRYCODE=US&CURRENCYCODE=$currencyCode";
			
			
			
			/* Make the API call to PayPal, using API signature.
			   The API response is stored in an associative array called $resArray */
			$resArray=$this->creditcard->hash_call("doDirectPayment",$nvpstr);
			
			
			
			$strtemp='';
			foreach ($resArray as $key => $value) 
			{					
			
				$strtemp.= $key."=".$value.",";
			}
		      
		
			log_message('error',"CREDIT CARD IPN DATA:".$strtemp);
			
			
			//var_dump($resArray);
			//exit;
			/* Display the API response back to the browser.
			   If the response from PayPal was a success, display the response parameters'
			   If the response was an error, display the errors received using APIError.php.
			   */
			$ack = strtoupper($resArray["ACK"]);
			
			  if($ack!="SUCCESS") 
			  {
				  $msg= "fail";
				  
				 
				 
				  if($task_id!=0 && $task_id>0 && $task_comment_id!=0 && $task_comment_id>0)
					{
								
								$task_detail=$this->task_model->get_tasks_detail_by_id($task_id);
								
								if($task_detail)
								{
					
				 					 redirect('tasks/'.$task_detail->task_url_name.'/fail');
								}
								else
								{				  
									 redirect('wallet/index/0/'.$msg);
								}
					
					}
					else
					{				  
			     		 redirect('wallet/index/0/'.$msg);
					}
					
			   }
			   else
			   {
				  
					$pay_gross=$resArray['AMT'];			
					$txnid=$resArray['TRANSACTIONID'];
					$date=date('Y-m-d H:i:s');
					$ip=$_SERVER['REMOTE_ADDR'];
					$payee_email=$creditCardNumber;
				
				    $user_id=get_authenticateUserID();
					$payer_status='';
					$gateway_id=$id;
					
					
					
					
					$wallet_setting2=wallet_setting();		
		
		
					$no_payment_after_auto_confirm=$wallet_setting2->no_payment_after_auto_confirm;
		
		
		
					
					$admin_status='Review';
					
					$gateway_details=get_payment_gateway_details_by_id($gateway_id);
					
					if($gateway_details)
					{
						if($gateway_details->auto_confirm==1)
						{
							$admin_status='Confirm';
						}
					}
					
					
					
					
				    $chk_status_admin=$this->db->query("select * from ".$this->db->dbprefix('wallet')." where user_id='".$user_id."' and admin_status='Confirm'");
					if($chk_status_admin->num_rows()>$no_payment_after_auto_confirm)
					{
							$admin_status='Confirm';
					}
					
					
				  $data=array(
					'debit' => $amount,
					'user_id' => $user_id,
					'status' => $payer_status,
					'admin_status' => $admin_status,
					'wallet_date' => $date,
					'wallet_transaction_id' => $txnid,
					'wallet_payee_email' => $payee_email,
					'wallet_ip' => $ip	,
					'gateway_id' => $gateway_id				
					);					
					$add_wallet=$this->db->insert('wallet',$data);		
				  $msg= "add";
					
					
					
					if($task_id!=0 && $task_id>0 && $task_comment_id!=0 && $task_comment_id>0)
					{
								
								$task_detail=$this->task_model->get_tasks_detail_by_id($task_id);
								
								if($task_detail)
								{
					
									redirect('task/accept_offer/'.$task_id.'/'.$task_comment_id);
									
								}
								else
								{						
									redirect('wallet/index/0/'.$msg);	
								}
						}
						else
						{						
							redirect('wallet/index/0/'.$msg);	
						}
					
					
					
			   }
		}	
			
			   
	
	}
	
	
	///---------- wallet withdrawl part
	
	
	
	/*
	Function name :my_withdraw()
	Parameter : $offset (for paging), $msg (for custom message)
	Return : none
	Use : User can see his/her all withdrawal list on here.
	Description : user can enter withdrawal list using this function which called http://hostname/my_withdraw.
	*/
	
	
	function my_withdraw($offset=0,$msg='')
	{
		
		if(!check_user_authentication()) {  redirect('sign_up'); } 
		$wallet_setting=wallet_setting();
		if($wallet_setting->wallet_enable==0)
		{
			redirect('dashboard');
		}
		
		if(get_authenticateUserID()== '')
		{
			redirect('sign_up');
		}
		
		$this->load->library('pagination');
		
		$limit = '15';
		$config['uri_segment']='3';
		$config['base_url'] = site_url('wallet/my_withdraw/');
		$config['total_rows'] = $this->wallet_model->get_total_my_withdraw_list();
		$config['per_page'] = $limit;
				
		$this->pagination->initialize($config);		
		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['limit']=$limit;
		$data['total_rows']=$this->wallet_model->get_total_my_withdraw_list();	
		$data['offset']=$offset;	
		
		$data['withdraw_details']=$this->wallet_model->my_withdraw_list($offset,$limit);
		
		
		$data['total_wallet_amount']=$this->wallet_model->my_wallet_amount(); 		
		$data['wallet_setting']=wallet_setting();
		
		
		$data['error'] = "";
		$data['msg']=$msg;
		$data['active_menu']='wallet';
		
		       $theme = getThemeName();
				$data['site_setting']=site_setting();
				$this->template->set_master_template($theme .'/template.php');				
				$data['theme']=$theme;				
				$meta_setting=meta_setting();
				$user_info = get_user_info(get_authenticateUserID());
				$data['first_name']=$user_info->first_name;		
				$data['last_name']=$user_info->last_name;					
				$pageTitle='Withdrawal History-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->title;
				$metaDescription='Withdrawal History-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->meta_description;
				$metaKeyword='Withdrawal History-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->meta_keyword;	
				
				$site_setting= site_setting();

				/*if($this->agent->is_mobile('iphone') && $site_setting->mobile_site == 1)
				{
					$this->template->set_master_template('iphone/template.php');
					$this->template->write_view('content_center', 'iphone/layout/wallet/my_withdraw', $data, TRUE);
					$this->template->render();
				}
				elseif(($this->agent->is_mobile() || $this->agent->is_robot()) && $site_setting->mobile_site == 1)
				{
					$this->template->set_master_template('mobile/template.php');
					$this->template->write_view('content_center', 'mobile/layout/wallet/my_withdraw', $data, TRUE);
					$this->template->render();
				}
				else {*/
					$this->template->write('pageTitle',$pageTitle,TRUE);
					$this->template->write('metaDescription',$metaDescription,TRUE);
					$this->template->write('metaKeyword',$metaKeyword,TRUE);
					$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
					$this->template->write_view('content_center',$theme .'/layout/wallet/my_withdraw',$data,TRUE);
					$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
					$this->template->render();	
				//}

	
	}	
	
	
	
	/*
	Function name :withdraw_wallet()
	Parameter : none
	Return : boolean
	Use : User can withdraw amount from his/her wallet if user have sufficient amount in wallet.
	Description : user can withdraw amount using this function which called http://hostname/withdraw_wallet.
	*/
	
	function withdraw_wallet()
	{
		
		if(!check_user_authentication()) {  redirect('sign_up'); } 
		$wallet_setting=wallet_setting();
		$site_setting=site_setting();
	    $data['site_setting']=site_setting();
		
		if($wallet_setting->wallet_enable==0)
		{
			redirect('sign_up');
		}
		if($this->session->userdata('user_id')== '')
		{
			redirect('sign_up');
		}
		
		$total_wallet_amount=$this->wallet_model->my_wallet_amount(); 	
		$minimum_amount=$wallet_setting->wallet_minimum_amount;		
		
		$chk_amt=$this->input->post('amount');
		////check total amount=====
		$own_amount_error='success';
		if($this->input->post('amount')>= 0) {
			if($chk_amt>$total_wallet_amount)
			{
				$own_amount_error='fail';			
			}
		}
		/////=====check minimum amount
		$amount_error='success';
		if($this->input->post('amount')>= 0) {
			if($chk_amt<$minimum_amount)
			{
				$amount_error='fail';			
			}
		}
		
		$this->load->library('form_validation');		
		$this->form_validation->set_rules('amount','Points:', 'required|is_natural');
		$this->form_validation->set_rules('withdraw_method', 'Withdraw Method', 'required');
		
		if($this->input->post('withdraw_method')=='bank')
		{
			$this->form_validation->set_rules('bank_name', 'Bank Name', 'required|alpha_space');
			$this->form_validation->set_rules('bank_account_holder_name', 'Account Holder Name', 'required|alpha_space');			
			$this->form_validation->set_rules('bank_account_number', 'Bank Account Number', 'required|alpha_numeric');
			$this->form_validation->set_rules('bank_branch','Bank Branch', 'required|alpha_space');
			$this->form_validation->set_rules('bank_ifsc_code', 'Bank IFSC Code', 'required|alpha_numeric');
			$this->form_validation->set_rules('bank_address', 'Bank Address', 'required');
			$this->form_validation->set_rules('bank_city', 'Bank City', 'required|alpha_space');
			$this->form_validation->set_rules('bank_state', 'Bank State', 'required|alpha_space');
			$this->form_validation->set_rules('bank_country', 'Bank Country', 'required|alpha_space');
			$this->form_validation->set_rules('bank_zipcode', 'Bank Postal Code', 'required|alpha_numeric');
					
		}
		
		if($this->input->post('withdraw_method')=='check')
		{
			$this->form_validation->set_rules('check_name', 'Bank Name', 'required|alpha_space');
			$this->form_validation->set_rules('check_account_holder_name', 'Account Holder Name', 'required|alpha_space');			
			$this->form_validation->set_rules('check_account_number', 'Bank Account Number', 'required|alpha_numeric');
			$this->form_validation->set_rules('check_branch', 'Bank Branch', 'required|alpha_space');
			//$this->form_validation->set_rules('check_unique_id', 'Bank Unique Code', 'required|alpha_numeric');
			$this->form_validation->set_rules('check_address', 'Bank Address', 'required');
			$this->form_validation->set_rules('check_city', 'Bank City', 'required|alpha_space');
			$this->form_validation->set_rules('check_state', 'Bank State', 'required|alpha_space');
			$this->form_validation->set_rules('check_country',  'Bank Country', 'required|alpha_space');
			$this->form_validation->set_rules('check_zipcode', 'Bank Postal Code', 'required|alpha_numeric');

		}
		
		if($this->input->post('withdraw_method')=='gateway')
		{
		
			$this->form_validation->set_rules('gateway_name','Gateway Name', 'required|alpha_space');
			$this->form_validation->set_rules('gateway_account', 'Gateway Account', 'required');
		
			$this->form_validation->set_rules('gateway_city', 'Gateway City', 'required|alpha_space');
			$this->form_validation->set_rules('gateway_state', 'Gateway State', 'required|alpha_space');			
			
			$this->form_validation->set_rules('gateway_country', 'Gateway State', 'required|alpha_space');
			$this->form_validation->set_rules('gateway_zip', 'Gateway Postal Code', 'required|alpha_numeric');		
		
		}
		
		
		if($this->form_validation->run() == FALSE || $amount_error=='fail' || $own_amount_error=='fail'){			
			
			
			
				if($amount_error=='fail')
				{
					//$amount_error="<p>".sprintf('You can not add less then amount %s in wallet.')." , $site_setting->currency_symbol.$minimum_amount)."</p>";
					
					$amount_error="<p>".sprintf('You can not add less then points %s in wallet.' , $minimum_amount)."</p>";
				}
				else
				{
					$amount_error='';
				}
				
				if($own_amount_error=='fail')
				{
					//$own_error="<p>You can not withdraw amount greater than %s .", $site_setting->currency_symbol.$total_wallet_amount)."</p>";
					$own_error="<p>".sprintf('You can not withdraw points greater than %s .',$total_wallet_amount)."</p>";
				}
				else
				{
					$own_error='';
				}
			
			
				if(validation_errors() || $amount_error!=''  || $own_error!='')
				{
					$data['error'] = validation_errors().$amount_error.$own_error;
							
				} else
				{
					$data["error"] = "";
				}
	
		
		
		
		
					$data['total_wallet_amount']=$this->wallet_model->my_wallet_amount(); 		
					$data['wallet_setting']=$this->wallet_model->wallet_setting();
					
					$data['withdraw_method']=$this->input->post('withdraw_method');
					$data['amount']=$this->input->post('amount');
					
					$data['withdraw_id']=$this->input->post('withdraw_id');
					$data['active_menu']='wallet';
					
					$data['bank_name']=$this->input->post('bank_name');
					$data['bank_branch']=$this->input->post('bank_branch');
					$data['bank_ifsc_code']=$this->input->post('bank_ifsc_code');
					$data['bank_address']=$this->input->post('bank_address');
					$data['bank_city']=$this->input->post('bank_city');
					$data['bank_state']=$this->input->post('bank_state');
					$data['bank_country']=$this->input->post('bank_country');
					$data['bank_zipcode']=$this->input->post('bank_zipcode');
					$data['bank_account_holder_name']=$this->input->post('bank_account_holder_name');
					$data['bank_account_number']=$this->input->post('bank_account_number');
					
					
					$data['check_name']=$this->input->post('check_name');
					$data['check_branch']=$this->input->post('check_branch');
					$data['check_unique_id']=$this->input->post('check_unique_id');
					$data['check_address']=$this->input->post('check_address');
					$data['check_city']=$this->input->post('check_city');
					$data['check_state']=$this->input->post('check_state');
					$data['check_country']=$this->input->post('check_country');
					$data['check_zipcode']=$this->input->post('check_zipcode');
					$data['check_account_holder_name']=$this->input->post('check_account_holder_name');
					$data['check_account_number']=$this->input->post('check_account_number');
										
					
					$data['gateway_name']=$this->input->post('gateway_name');
					$data['gateway_account']=$this->input->post('gateway_account');
					$data['gateway_city']=$this->input->post('gateway_city');
					$data['gateway_state']=$this->input->post('gateway_state');
					$data['gateway_country']=$this->input->post('gateway_country');
					$data['gateway_zip']=$this->input->post('gateway_zip');
										
				
				
		       $theme = getThemeName();
				
				$this->template->set_master_template($theme .'/template.php');				
				$data['theme']=$theme;				
				$meta_setting=meta_setting();
				$user_info = get_user_info(get_authenticateUserID());
				$data['active_menu']='wallet';
				$data['first_name']=$user_info->first_name;		
				$data['last_name']=$user_info->last_name;					
				$pageTitle='Withdraw-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->title;
				$metaDescription='Withdraw-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->meta_description;
				$metaKeyword='Withdraw-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->meta_keyword;	
				
				$site_setting= site_setting();

				
				$this->template->write('pageTitle',$pageTitle,TRUE);
				$this->template->write('metaDescription',$metaDescription,TRUE);
				$this->template->write('metaKeyword',$metaKeyword,TRUE);
				$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
				$this->template->write_view('content_center',$theme .'/layout/wallet/withdraw_wallet',$data,TRUE);
				$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
				$this->template->render();	
				
		
		
			}
			else
			{	
				
				if($this->input->post('withdraw_id')!='')
				{
					$this->wallet_model->update_withdraw_request();				
					$msg='update';				
				}
				
				else
				{				
					$this->wallet_model->add_withdraw_request();				
					$msg='success';
				}
					redirect('wallet/my_withdraw/0/'.$msg);		
					
				
			}
		
		
		
		
		
		
	
	}
	
	
	
	/*
	Function name :withdraw_details()
	Parameter : $id (withdrawal ID)
	Return : none
	Use : User can edit his/her withdrawal details untill administrator not confirmed request.
	Description : user can edit withdrawal details using this function which called http://hostname/withdraw_details/$id.
	*/
	
	
	function withdraw_details($id)
	{
	
	
		if(!check_user_authentication()) {  redirect('sign_up'); } 
		
		
		if($id=='' || $id==0)
		{
			redirect('wallet/my_withdraw');
		}
		
		
		$wallet_setting=wallet_setting();
		
		if($wallet_setting->wallet_enable==0)
		{
			redirect('dashboard');
		}
		
		
		$site_setting=site_setting();
		$data['site_setting']=$site_setting;
		
		
		$data['total_wallet_amount']=$this->wallet_model->my_wallet_amount(); 		
		$data['wallet_setting']=$wallet_setting;
		
		$data["error"] = "";
		$data['withdraw_id']=$id;
		
		$withdraw_detail=$this->wallet_model->get_withdraw_detail($id);
		
		$data['active_menu']='wallet';
		
		
		$data['withdraw_method']=$withdraw_detail->withdraw_method;
		$data['amount']=str_replace('.00','',$withdraw_detail->withdraw_amount);
		
		
		$bank_detail=$this->wallet_model->get_withdraw_method_detail($id,'bank');
		$check_detail=$this->wallet_model->get_withdraw_method_detail($id,'check');				
		$gateway_detail=$this->wallet_model->get_withdraw_method_detail($id,'gateway');
		
		if($bank_detail)
		{		
			$data['bank_name']=$bank_detail->bank_name;
			$data['bank_branch']=$bank_detail->bank_branch;
			$data['bank_ifsc_code']=$bank_detail->bank_ifsc_code;
			$data['bank_address']=$bank_detail->bank_address;
			$data['bank_city']=$bank_detail->bank_city;
			$data['bank_state']=$bank_detail->bank_state;
			$data['bank_country']=$bank_detail->bank_country;
			$data['bank_zipcode']=$bank_detail->bank_zipcode;
			$data['bank_account_holder_name']=$bank_detail->bank_account_holder_name;
			$data['bank_account_number']=$bank_detail->bank_account_number;
		
		}
		else
		{
			$data['bank_name']='';
			$data['bank_branch']='';
			$data['bank_ifsc_code']='';
			$data['bank_address']='';
			$data['bank_city']='';
			$data['bank_state']='';
			$data['bank_country']='';
			$data['bank_zipcode']='';
			$data['bank_account_holder_name']='';
			$data['bank_account_number']='';
		}
		
		
		if($check_detail)
		{
			$data['check_name']=$check_detail->bank_name;
			$data['check_branch']=$check_detail->bank_branch;
			$data['check_unique_id']=$check_detail->bank_unique_id;
			$data['check_address']=$check_detail->bank_address;
			$data['check_city']=$check_detail->bank_city;
			$data['check_state']=$check_detail->bank_state;
			$data['check_country']=$check_detail->bank_country;
			$data['check_zipcode']=$check_detail->bank_zipcode;
			$data['check_account_holder_name']=$check_detail->bank_account_holder_name;
			$data['check_account_number']=$check_detail->bank_account_number;
		}
		else
		{
			$data['check_name']='';
			$data['check_branch']='';
			$data['check_unique_id']='';
			$data['check_address']='';
			$data['check_city']='';
			$data['check_state']='';
			$data['check_country']='';
			$data['check_zipcode']='';
			$data['check_account_holder_name']='';
			$data['check_account_number']='';
		
		}
		
		if($gateway_detail)
		{					
		
			$data['gateway_name']=$gateway_detail->gateway_name;
			$data['gateway_account']=$gateway_detail->gateway_account;
			$data['gateway_city']=$gateway_detail->gateway_city;
			$data['gateway_state']=$gateway_detail->gateway_state;
			$data['gateway_country']=$gateway_detail->gateway_country;
			$data['gateway_zip']=$gateway_detail->gateway_zip;
				
		}
		
		else
		{
			$data['gateway_name']='';
			$data['gateway_account']='';
			$data['gateway_city']='';
			$data['gateway_state']='';
			$data['gateway_country']='';
			$data['gateway_zip']='';
		}			
	
	
			 $theme = getThemeName();
				
				$this->template->set_master_template($theme .'/template.php');				
				$data['theme']=$theme;				
				$meta_setting=meta_setting();
				$user_info = get_user_info(get_authenticateUserID());
				$data['first_name']=$user_info->first_name;		
				$data['last_name']=$user_info->last_name;					
				$pageTitle='Withdraw-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->title;
				$metaDescription='Withdraw-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->meta_description;
				$metaKeyword='Withdraw-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->meta_keyword;
				
				
				$site_setting= site_setting();

				
					$this->template->write('pageTitle',$pageTitle,TRUE);
					$this->template->write('metaDescription',$metaDescription,TRUE);
					$this->template->write('metaKeyword',$metaKeyword,TRUE);
					$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
					$this->template->write_view('content_center',$theme .'/layout/wallet/withdraw_wallet',$data,TRUE);
					$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
					$this->template->render();	
				
		
	
	}	
	
	
	
	
	/*
	Function name :withdraw_detail()
	Parameter : $id (withdrawal ID)
	Return : none
	Use : User can see his/her withdrawal details.
	Description : user can see withdrawal details using this function which called http://hostname/withdraw_detail/$id.
	*/
	
	function withdrawal_detail($id)
	{
		
		
		if(!check_user_authentication()) {  redirect('sign_up'); } 
		
		
		if($id=='' || $id==0)
		{
			redirect('wallet/my_withdraw');
		}
		$wallet_setting=wallet_setting();
		
		if($wallet_setting->wallet_enable==0)
		{
			redirect('dashboard');
		}
		
		
			$site_setting=site_setting();
		$data['site_setting']=$site_setting;
		
		
		
		$data['total_wallet_amount']=$this->wallet_model->my_wallet_amount(); 		
		$data['wallet_setting']=wallet_setting();
		
		$data["error"] = "";
		$data['withdraw_id']=$id;
		
		$withdraw_detail=$this->wallet_model->get_withdraw_detail($id);
		
		
		$data['active_menu']='wallet';
		
		$data['withdraw_method']=$withdraw_detail->withdraw_method;
		$data['amount']=str_replace('.00','',$withdraw_detail->withdraw_amount);
		
		
		$bank_detail=$this->wallet_model->get_withdraw_method_detail($id,'bank');
		$check_detail=$this->wallet_model->get_withdraw_method_detail($id,'check');				
		$gateway_detail=$this->wallet_model->get_withdraw_method_detail($id,'gateway');
		
		if($bank_detail)
		{		
			$data['bank_name']=$bank_detail->bank_name;
			$data['bank_branch']=$bank_detail->bank_branch;
			$data['bank_ifsc_code']=$bank_detail->bank_ifsc_code;
			$data['bank_address']=$bank_detail->bank_address;
			$data['bank_city']=$bank_detail->bank_city;
			$data['bank_state']=$bank_detail->bank_state;
			$data['bank_country']=$bank_detail->bank_country;
			$data['bank_zipcode']=$bank_detail->bank_zipcode;
			$data['bank_account_holder_name']=$bank_detail->bank_account_holder_name;
			$data['bank_account_number']=$bank_detail->bank_account_number;
		
		}
		else
		{
			$data['bank_name']='';
			$data['bank_branch']='';
			$data['bank_ifsc_code']='';
			$data['bank_address']='';
			$data['bank_city']='';
			$data['bank_state']='';
			$data['bank_country']='';
			$data['bank_zipcode']='';
			$data['bank_account_holder_name']='';
			$data['bank_account_number']='';
		}
		
		
		if($check_detail)
		{
			$data['check_name']=$check_detail->bank_name;
			$data['check_branch']=$check_detail->bank_branch;
			$data['check_unique_id']=$check_detail->bank_unique_id;
			$data['check_address']=$check_detail->bank_address;
			$data['check_city']=$check_detail->bank_city;
			$data['check_state']=$check_detail->bank_state;
			$data['check_country']=$check_detail->bank_country;
			$data['check_zipcode']=$check_detail->bank_zipcode;
			$data['check_account_holder_name']=$check_detail->bank_account_holder_name;
			$data['check_account_number']=$check_detail->bank_account_number;
		}
		else
		{
			$data['check_name']='';
			$data['check_branch']='';
			$data['check_unique_id']='';
			$data['check_address']='';
			$data['check_city']='';
			$data['check_state']='';
			$data['check_country']='';
			$data['check_zipcode']='';
			$data['check_account_holder_name']='';
			$data['check_account_number']='';
		
		}
		
		if($gateway_detail)
		{					
		
			$data['gateway_name']=$gateway_detail->gateway_name;
			$data['gateway_account']=$gateway_detail->gateway_account;
			$data['gateway_city']=$gateway_detail->gateway_city;
			$data['gateway_state']=$gateway_detail->gateway_state;
			$data['gateway_country']=$gateway_detail->gateway_country;
			$data['gateway_zip']=$gateway_detail->gateway_zip;
				
		}
		
		else
		{
			$data['gateway_name']='';
			$data['gateway_account']='';
			$data['gateway_city']='';
			$data['gateway_state']='';
			$data['gateway_country']='';
			$data['gateway_zip']='';
		}			
	
	
		
		
		
		
		 $theme = getThemeName();
				
		$this->template->set_master_template($theme .'/template.php');				
		$data['theme']=$theme;				
		$meta_setting=meta_setting();
		$user_info = get_user_info(get_authenticateUserID());
		$data['first_name']=$user_info->first_name;		
		$data['last_name']=$user_info->last_name;					
		$pageTitle='Withdraw-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->title;
		$metaDescription='Withdraw-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->meta_description;
		$metaKeyword='Withdraw-'.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->meta_keyword;	
		
		
		$site_setting= site_setting();

		
			$this->template->write('pageTitle',$pageTitle,TRUE);
			$this->template->write('metaDescription',$metaDescription,TRUE);
			$this->template->write('metaKeyword',$metaKeyword,TRUE);
			//$this->template->write_view('header',$theme .'/layout/common/header_login',$data,TRUE);
			$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
			$this->template->write_view('content_center',$theme .'/layout/wallet/withdrawal_detail',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
			$this->template->render();	
				
				
	
	}
	
	
	/*
	Function name :verify_paypal()
	Parameter : none
	Return : none
	Use : verify PayPal Email Address
	Description : user can verify PayPal Email Address using this function 
	*/
	function verify_paypal($msg='',$pay=0)
	{
		
		if(!check_user_authentication()) {  redirect('sign_up'); } 
		
		$data['pay']=$pay;
		$data['msg']=$msg;
		$data['url']=$msg;
		
		$verify='no';
		$user_info=get_user_info(get_authenticateUserID());
		$data['active_menu']='wallet';
		$data['user_name'] = $user_info->first_name;
		$data['last_name'] = $user_info->last_name;
		$data['verify'] = $user_info->paypal_verified;
		if($user_info->paypal_email == '')
		{
			$data['paypal_email']='';
		}
		else
		{
			$data['paypal_email']=$user_info->paypal_email;
		}
		
		$this->load->library('form_validation');
				
		$this->form_validation->set_rules('user_name', $this->lang->line('wallet.paypal_fname'), 'required');
		$this->form_validation->set_rules('last_name', $this->lang->line('wallet.paypal_lname'), 'required');	
		$this->form_validation->set_rules('paypal_email', $this->lang->line('wallet.paypal_email'), 'required|valid_email');
		
		if($this->form_validation->run() == FALSE)
		{
			if(validation_errors())
			{
				
				$data["error"] = validation_errors();
				
			}else{
				$data["error"] = "";
				$data['user_name']=$this->input->post('user_name');
				$data['last_name']=$this->input->post('last_name');
				
				$data['verify'] = $user_info->paypal_verified;
				if($user_info->paypal_email == '')
				{
					$data['paypal_email']='';
				}
				else
				{
					$data['paypal_email']=$user_info->paypal_email;
				}
				
			}
			
		
		} 
		else {
		
			
			$this->db->query("update ".$this->db->dbprefix('user')." set paypal_email='".$this->input->post('paypal_email')."' where user_id='".$this->session->userdata('user_id')."'");
			
			 $paypal_adaptive_detail=$this->db->query("select * from ".$this->db->dbprefix('paypal')."");
			 $paypal=$paypal_adaptive_detail->row();
			
			//////////=================Get PAYPAL SETTING  FROM DATABASE 	================
				$application_id =	$paypal->application_id;
				$api_username=$paypal->paypal_username;
				$api_password=$paypal->paypal_password;	
				$api_key=$paypal->paypal_signature;
				
				if($paypal->site_status=='sandbox')
				{
					$dbend_point='https://svcs.sandbox.paypal.com/';	
					$db_paypalurl='https://www.sandbox.paypal.com/webscr&cmd=';
				}
				elseif($paypal->site_status=='live')
				{
					$dbend_point='https://svcs.paypal.com/';	
					$db_paypalurl='https://www.paypal.com/webscr&cmd=';
				}
				else
				{	
					$dbend_point='https://svcs.sandbox.paypal.com/';	
					$db_paypalurl='https://www.sandbox.paypal.com/webscr&cmd=';		
				}
				
					$actionType='PAY';
					define('API_USERNAME', $api_username);
					define('API_PASSWORD',$api_password );
					define('API_SIGNATURE',$api_key);
					define('API_ENDPOINT',$dbend_point );
					define('USE_PROXY',FALSE);
					define('PROXY_HOST', '127.0.0.1');
					define('PROXY_PORT', '808');
					define('ACK_SUCCESS', 'SUCCESS');
					define('ACK_SUCCESS_WITH_WARNING', 'SUCCESSWITHWARNING');
					define('APPLICATION_ID', $application_id);
					define('DEVICE_ID','mydevice');
					define('PAYPAL_REDIRECT_URL', $db_paypalurl );
					define('DEVELOPER_PORTAL', 'https://developer.paypal.com');
					define('LOGFILENAME', '../Log/logdata.log');
					define('DEVICE_IPADDRESS', $_SERVER['REMOTE_ADDR']);
					define('REQUEST_FORMAT','NV');
					define('RESPONSE_FORMAT','NV');
					define('X_PAYPAL_REQUEST_SOURCE','PHP_NVP_SDK_V1.1');
					$this->load->library('NVP_SampleConstants');
					$NVP_SampleConstants = new NVP_SampleConstants();
					
					try {
			
							$request_array= array
							(
								RequestEnvelope::$requestEnvelopeErrorLanguage=> 'en_US',
								GetVerifiedStatus::$emailAddress=>$this->input->post('paypal_email'),
								GetVerifiedStatus::$firstName=> $this->input->post('user_name'),
								GetVerifiedStatus::$lastName=> $this->input->post('last_name'),
								GetVerifiedStatus::$matchCriteria=> 'NAME',
							);
								 $nvpStr=http_build_query($request_array, '', '&');
								$resArray=$NVP_SampleConstants->hash_call('AdaptiveAccounts/GetVerifiedStatus',$nvpStr);

							//print_r($resArray);die();
								$ack = strtoupper($resArray['responseEnvelope.ack']);
						
							if($ack!="SUCCESS")
							{
								$verify='no';
							}
							else
							{
								/*foreach($resArray as $key => $value) 
								{    			
    									echo "<p> $key: $value</p>";
    							}	
       							*/
								$verify='yes';
								
							}
							
							
				   
					}
		
					catch(Exception $ex) 
					{
						throw new Exception('Error occurred in GetVerifiedStatusReceipt method');
						$verify='no';
						
					}
					
			 
			 /////////===============dn=b insert
			 if($verify=='yes') { 
				$this->db->query("update ".$this->db->dbprefix('user')." set paypal_verified='1' and paypal_email='".$this->input->post('paypal_email')."' where user_id='".$this->session->userdata('user_id')."'");
				$data['error'] = '';
				$data['msg']='success';
				$data['user_name']='';
				$data['last_name']='';
				if($user_info->paypal_email == '')
				{
					$data['paypal_email']='';
				}
				else
				{
					$data['paypal_email']=$user_info->paypal_email;
				}
				$data['verify'] = $user_info->paypal_verified;

				if($this->input->post('pay') > 0){ redirect('user_task/pay_now/'.$this->input->post('pay')); } 
				elseif($this->input->post('url') != ''){ 
					$url = base64_decode($this->input->post('url'));
					redirect($url); 
				}
				else { redirect('wallet/verify_paypal/verify');}
				
				
				
				
			}
			else
			{
				$this->db->query("update ".$this->db->dbprefix('user')." set paypal_verified='0' and paypal_email='' where user_id='".$this->session->userdata('user_id')."'");			
			
				
				$verify_error='<p>'.$this->lang->line('wallet.paypal_correct_email').$resArray['error(0).errorId'].'-'.$resArray['error(0).message'].'</p>'; 	
				
				$data['error'] = $verify_error;
				$data['user_name']=$this->input->post('user_name');
				$data['last_name']=$this->input->post('last_name');
				$data['paypal_email']=$this->input->post('paypal_email');
				$data['verify'] = $user_info->paypal_verified;
				$data['url']=$this->input->post('url');
				
			}
		}
		
		
		
		$data['activities']='';
			
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		
		$meta_setting=meta_setting();
		
		$pageTitle='Set up your PAYPAL- '.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->title;
		$metaDescription='Set up your PAYPAL: '.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->meta_description;
		$metaKeyword='Set up your credit card : '.$user_info->first_name.' '.substr($user_info->last_name,0,1).' - '.$meta_setting->meta_keyword;	
		
		$site_setting= site_setting();

		
			$this->template->write('pageTitle',$pageTitle,TRUE);
			$this->template->write('metaDescription',$metaDescription,TRUE);
			$this->template->write('metaKeyword',$metaKeyword,TRUE);
			$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
			$this->template->write_view('content_center',$theme .'/layout/wallet/paypal',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
			$this->template->render();	
	

		
	
	}	
	
	
}

?>