<?php
class Transaction extends IWEB_Controller {
	
	/*
	Function name :Dashboard()
	Description :Its Default Constuctor which called when home object initialzie.its load necesary models
	*/
	
	function Transaction()
	{
		parent::__construct();	
		$this->load->model('home_model');	
		$this->load->model('user_model');	
		$this->load->model('transaction_model');	
		$this->load->model('challenge_model');	
			
	}
	
	function index($limit='20',$offset=0,$msg='')
	{
			
		if(!check_user_authentication()) {  redirect('login'); } 
		
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="transaction";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['msg']=$msg;
		
		$this->load->library('pagination');
		$config['uri_segment']='4';
		$config['base_url'] = base_url().'transaction/index/'.$limit.'/';
		$config['total_rows']= $this->transaction_model->get_total_user_transaction_count(get_authenticateUserID());
	
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->transaction_model->get_transaction_result(get_authenticateUserID(),$limit,$offset);
		$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$pageTitle='3D-Design Hive- '.$meta_setting->title;
		$metaDescription='3D-Design Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-Design Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/transaction/list_transaction',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
		
	}
	
	
}
?>