<?php
class User extends IWEB_Controller 
{
	
	/*
	Function name :User()
	Description :Its Default Constuctor which called when user object initialzie.its load necesary models
	*/
	function User()
	{
		parent::__construct();	
		$this->load->model('home_model');	
		$this->load->model('user_model');	
		$this->load->model('design_model');	
		$this->load->model('video_model');	
		$this->load->model('challenge_model');	
		
	}
	
	
	/*
	Function name :index()
	Parameter :none
	Return : none
	Use : redirect to user dashboard
	Description : none
	*/
	
	public function index()
	{
		redirect('home');
	}
	
	function dashboard()
	{
		if(!check_user_authentication()) {  redirect('login'); } 
		$data=array();
		$theme = getThemeName();
		
		$data['active_menu']='dashboard';
		$this->template->set_master_template($theme .'/template.php');
		
		$meta_setting=meta_setting();
		$site_setting=site_setting();
		
		$pageTitle=$meta_setting->title;
		$metaDescription=$meta_setting->meta_description;
		$metaKeyword=$meta_setting->meta_keyword;
		
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('campaign_box',$theme .'/layout/user/dashboard',$data,TRUE);		
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
	//	$this->template->write_view('footer_js',$theme .'/layout/common/chart_js',$data,TRUE);
		$this->template->render();
		
	}
	
		
		
	
	function change_password()
	{
	    if(!check_user_authentication()) {  redirect('login'); } 
		  
		  
		$user_info=get_user_profile_by_id(get_authenticateUserID());
		$data['user_info']=$user_info;
		
		$site_setting=site_setting();
		
		
		$this->form_validation->set_rules('new_password', 'New Password', 'required|min_length[8]|max_length[12]|matches[confirm_password]');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|min_length[8]|max_length[12]');
	
		if($this->form_validation->run() == FALSE)
		{
			if(validation_errors())
			{													
				$data["error"] = validation_errors();
			}
			else
			{		
				$data["error"] = "";							
			}
			
			$data["new_password"] = '';
			$data["confirm_password"] = '';
				
		} else	{
							
			$reset_password = $this->user_model->change_password();
			$data['error']="update";

	   }	
			   
			   
	    $data["active_menu"] = "changepassword";
		$data['select']='password';
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		
		$meta_setting=meta_setting();
		
		$pageTitle= ucfirst($user_info->first_name).' '.ucfirst($user_info->last_name).' - Change Password '.$meta_setting->title;
		$metaDescription=ucfirst($user_info->first_name).' '.ucfirst($user_info->last_name).' - Change Password '.$meta_setting->meta_description;
		$metaKeyword=ucfirst($user_info->first_name).' '.ucfirst($user_info->last_name).' - Change Password '.$meta_setting->meta_keyword;
		
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/user/password',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
		
		
		
	}
	
	
	
		/*
	Function name :username_check()
	Parameter : $user_name (profile name)
	Return : boolen
	Use : check the user unique profile name
	Description : check the user unique profile name
	*/
		
	function username_check($profile_name)
	{
		$cuser_name = $this->home_model->usernameTaken($profile_name);
		
		if($cuser_name)
		{
			$this->form_validation->set_message('profile_name', 'There is an existing profile associated with this Username.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}	
	
	
	
	/*
	Function name :email_check()
	Parameter : $email (email id)
	Return : boolen
	Use : check the user unique email address
	Description : check the user unique email address
	*/
		
	function email_check($email)
	{
		
		$cemail = $this->home_model->emailTaken($email);
		
		if($cemail)
		{
			$this->form_validation->set_message('email_check', 'There is an existing account associated with this email.');
			return FALSE;
		}
		else
		{
				return TRUE;
		}
	}	
	
	function upload_ajax_image()
	{
		$user_info=get_user_profile_by_id(get_authenticateUserID());
		
		$image_settings = image_setting();
		if($_FILES['profileimage']['name']!='')
		{
			
				 
				 
				 $this->load->library('upload');
				 $rand=randomCode(); 
				  
				 $_FILES['userfile']['name']     =   $_FILES['profileimage']['name'];
				 $_FILES['userfile']['type']     =   $_FILES['profileimage']['type'];
				 $_FILES['userfile']['tmp_name'] =   $_FILES['profileimage']['tmp_name'];
				 $_FILES['userfile']['error']    =   $_FILES['profileimage']['error'];
				 $_FILES['userfile']['size']     =   $_FILES['profileimage']['size'];
	  
				 
				 $config['file_name'] = $rand;
				 $config['upload_path'] = base_path().'upload/user_orig/';
				 $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
					
					
				$this->upload->initialize($config);
 
				  if (!$this->upload->do_upload())
				  {
					$error =  $this->upload->display_errors();   
				  } 
				   
				   
				  $picture = $this->upload->data();		
				  $image_name=$picture['file_name'];
				 

					
				$this->load->library('image_lib');					
				$this->image_lib->clear();	
			
				$config['image_library'] = 'GD2';
				$config['thumb_marker'] = "";
				$config['maintain_ratio'] = TRUE;
				$config['create_thumb'] = TRUE;
				$config['quality'] = '100%';
				$config['master_dim'] = 'width';
				$config['source_image'] = $picture['full_path'];
				$config['new_image'] = base_path().'upload/user_thumb/'.$picture['file_name'];					
				$config['width'] = $image_settings->image_small_thumb_width;
				$config['height'] = $image_settings->image_small_thumb_height;
				$this->image_lib->initialize($config);
				
				
					
				if(!$this->image_lib->resize()){
					$error = $this->image_lib->display_errors();				
				}
							
					
					
				$this->image_lib->clear();
				
				$config['image_library'] = 'GD2';
				$config['thumb_marker'] = "";
				$config['maintain_ratio'] = TRUE;
				$config['create_thumb'] = TRUE;
				$config['quality'] = '100%';
				$config['master_dim'] = 'width';
				$config['source_image'] = $picture['full_path'];
				$config['new_image'] = base_path().'upload/user/'.$picture['file_name'];
				$config['width'] = $image_settings->image_medium_thumb_width;
				$config['height'] = $image_settings->image_medium_thumb_height;
				$this->image_lib->initialize($config);
		
				if(!$this->image_lib->resize()){
					$error = $this->image_lib->display_errors();				
				}
						
						
				$this->image_lib->clear();
				
				
				
				
				 if($user_info->profile_image != '') 
					{
						if(file_exists(base_path().'upload/user_orig/'.$user_info->profile_image))
						{
							unlink(base_path().'upload/user_orig/'.$user_info->profile_image);
						}
						if(file_exists(base_path().'upload/user_thumb/'.$user_info->profile_image))
						{
							unlink(base_path().'upload/user_thumb/'.$user_info->profile_image);
						}
						
						if(file_exists(base_path().'upload/user/'.$user_info->profile_image))
						{
							unlink(base_path().'upload/user/'.$user_info->profile_image);
						}
					}
							
				
				
			
				$data_img=array(
					'profile_image' => $picture['file_name'],
				);
				$this->db->where('user_id',get_authenticateUserID());
				$this->db->update('user_profile',$data_img);
				
				
				echo '<img src="'.base_url().'upload/user/'.$picture['file_name'].'" width="155" />';
				
				
			}
			
			exit;	
	}
	
	
	
	
	function edit($msg='')
	{
			
				
			if(!check_user_authentication()) { redirect('login'); }
			$data = array();
			$data['msg']=$msg;
			$data["error"]='';
			$site_setting=site_setting();
			$data['site_setting']=$site_setting;
			$theme = getThemeName();
			$this->template->set_master_template($theme .'/template.php');
			$data['active_menu']='edit_account';
			$data['theme']=$theme;
			
			$user_info=get_user_profile_by_id(get_authenticateUserID());
			
			if(!$user_info)
			{
				redirect('home');
			}
			
			$data['user_info']=$user_info;
			
			//$this->form_validation->set_rules('profile_name', 'User Name', 'required|alpha_numeric|callback_username_check');
			$this->form_validation->set_rules('first_name', 'First Name', 'required|alpha');
			$this->form_validation->set_rules('last_name', 'Last Name', 'required|alpha');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');
			//$this->form_validation->set_rules('zip_code', 'Postal Code', 'alpha_numeric|min_length['.$site_setting->zipcode_min.']|max_length['.$site_setting->zipcode_max.']');
		//	$this->form_validation->set_rules('mobile_no', 'Mobile Number', 'numeric|exact_length[10]');	
		  //  $this->form_validation->set_rules('phone_no', 'Phone Number', 'numeric|min_length[9]|max_length[12]');		
			
			//$this->form_validation->set_rules('user_location', 'Location', 'trim|required');
			//$this->form_validation->set_rules('about_user', 'About Me', 'trim|required');
			
			//$this->form_validation->set_rules('own_site_link', 'Personal Website', 'trim|valid_url');
			//$this->form_validation->set_rules('facebook_link', 'Facebook Profile Link', 'trim|valid_url');
			//$this->form_validation->set_rules('twitter_link', 'Twiiter Profile Link', 'trim|valid_url');
			//$this->form_validation->set_rules('linkedin_link', 'Linkedin Profile Link', 'trim|valid_url');
			
			
			
			
			if($this->form_validation->run() == FALSE )
			{
				if(validation_errors())
				{
					$data["error"] = validation_errors();
				}
				else
				{
				$data["error"] = "";
				}
				if($_POST)
				{
					$data['user_id']=$this->input->post('user_id ');
					$data['profile_name']=$this->input->post('profile_name');
					$data['first_name']=$this->input->post('first_name');
					$data['last_name']=$this->input->post('last_name');
					$data['email']=$this->input->post('email');		
					$data['zip_code']=$this->input->post('zip_code');
					$data['mobile_no']=$this->input->post('mobile_no');
					$data['phone_no']=$this->input->post('phone_no');
					
					
					$data['user_location']=$this->input->post('user_location');
					$data['about_user']=$this->input->post('about_user');
					
					$data['own_site_link']=$this->input->post('own_site_link');
					$data['facebook_link']=$this->input->post('facebook_link');
					$data['twitter_link']=$this->input->post('twitter_link');
					$data['linkedin_link']=$this->input->post('linkedin_link');
					
					
					
				}
				else
				{
					$data['user_id']=$user_info->user_id;
					$data['profile_name']=$user_info->profile_name;
					$data['first_name']=$user_info->first_name;
					$data['last_name']=$user_info->last_name;
					$data['email']=$user_info->email;
					$data['zip_code']=$user_info->zip_code;
					$data['mobile_no']=$user_info->mobile_no;
					$data['phone_no']=$user_info->phone_no;
					
					$data['user_location']=$user_info->user_location;
					$data['about_user']=$user_info->about_user;
					
					$data['own_site_link']=$user_info->own_site_link;
					$data['facebook_link']=$user_info->facebook_link;
					$data['twitter_link']=$user_info->twitter_link;
					$data['linkedin_link']=$user_info->linkedin_link;
					
				}
				
			} 
			else
			{		
				
				$reset_account = $this->user_model->edit_account();
				$msg="update";
				redirect('user/profile/'.$msg);
				
				
			}
			
			$meta_setting=meta_setting();
			$pageTitle=ucfirst($user_info->first_name).' '.ucfirst(substr($user_info->last_name,0,1)).' - Edit Account '.$meta_setting->title;
			$metaDescription=ucfirst($user_info->first_name).' '.ucfirst(substr($user_info->last_name,0,1)).' - Edit Account '.$meta_setting->meta_description;
			$metaKeyword=ucfirst($user_info->first_name).' '.ucfirst(substr($user_info->last_name,0,1)).' - Edit Account '.$meta_setting->meta_keyword;

			$this->template->write('pageTitle',$pageTitle,TRUE);
			$this->template->write('metaDescription',$metaDescription,TRUE);
			$this->template->write('metaKeyword',$metaKeyword,TRUE);
			$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
			$this->template->write_view('content_center',$theme .'/layout/user/edit_account',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
			$this->template->render();
			

	}


	function notifications($limit='20',$offset=0)
	{

		if(!check_user_authentication()) {  redirect('login'); } 
		

		$user_info=get_user_profile_by_id(get_authenticateUserID());
		$data['user_info']=$user_info;
		$data['active_menu'] = 'user_notification';
		
		$site_setting=site_setting();
		$data['site_setting']=$site_setting;
		$data['error']="";
		
		$update_notification = $this->user_model->update_notification(get_authenticateUserID());
		
	    $this->load->library('pagination');
		$config['uri_segment']='4';
		$config['base_url'] = base_url().'user/notifications/'.$limit.'/';
		$config['total_rows']= $this->user_model->get_user_notification_count(get_authenticateUserID());
	
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->user_model->get_user_notification(get_authenticateUserID(),$limit,$offset);
		
	 //  $user_notification=$this->user_model->get_user_notification(get_authenticateUserID());
	   //$data['user_notification']=$user_notification;

			
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		
		$meta_setting=meta_setting();
		$pageTitle=ucfirst($user_info->first_name).' '.ucfirst(substr($user_info->last_name,0,1)).' - Notifications '.$meta_setting->title;
		$metaDescription=ucfirst($user_info->first_name).' '.ucfirst(substr($user_info->last_name,0,1)).' - Notifications '.$meta_setting->meta_description;
		$metaKeyword=ucfirst($user_info->first_name).' '.ucfirst(substr($user_info->last_name,0,1)).' - Notifications '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/user/notification',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	

	}

	
	public function profile($msg='')
		{	
			if(!check_user_authentication()) {  redirect('login'); } 
			$data=array();
			$theme = getThemeName();
			
			$data['active_menu']='dashboard';
			$this->template->set_master_template($theme .'/template.php');
			
			$meta_setting=meta_setting();
			$site_setting=site_setting();
			
			$pageTitle=$meta_setting->title;
			$metaDescription=$meta_setting->meta_description;
			$metaKeyword=$meta_setting->meta_keyword;
			$data['site_setting']=$site_setting;
			$data['msg']=$msg;
			
				$data['user_info'] = get_user_profile_by_id(get_authenticateUserID());

			$this->template->write('pageTitle',$pageTitle,TRUE);
			$this->template->write('metaDescription',$metaDescription,TRUE);
			$this->template->write('metaKeyword',$metaKeyword,TRUE);
			
			$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
			$this->template->write_view('campaign_box',$theme .'/layout/user/profile_detail',$data,TRUE);		
			$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
			$this->template->render();
			
	
		}
	function reviews($profile_name,$offset=0)
		{        
			   
			$user_profile=get_user_profile_by_profile_name($profile_name);
			if(!$user_profile) { redirect('home'); }
			$data['user_info']=$user_profile;
			
			$agent_info = check_user_agent($user_profile->user_id);
			$data['agent_info']=$agent_info;
			$data['profile_name']=$profile_name;
			
			$review_limit = 5;
			$data['reviews'] = get_user_reviews($user_profile->user_id,$review_limit);	
				
			$this->load->library('pagination');
			
			$limit=10;
			
			if($offset>0)
			{
				$config['uri_segment']='4';
			}
			else
			{
				$config['uri_segment']='3';
			}
			
			
			$config['base_url'] = site_url('user/'.$profile_name.'/reviews');
			$config['total_rows'] = $this->user_model->get_user_total_reviews($user_profile->user_id);
			$config['per_page'] = $limit;
					
			$this->pagination->initialize($config);		
			
			$data['page_link'] = $this->pagination->create_links();
			$data['result'] = $this->user_model->get_user_review_list($user_profile->user_id,$limit,$offset);
			
			
			
			$data['total_rows']=$config['total_rows'];				
			$data['limit']=$limit;
	
			$theme = getThemeName();
			$this->template->set_master_template($theme .'/template.php');
		   
		   
			$data['theme']=$theme;
			$meta_setting=meta_setting();
		   
			$pageTitle='Reviews for - '.$user_profile->first_name.' '.substr($user_profile->last_name,0,1);
			$metaDescription='Reviews for - '.$user_profile->first_name.' '.substr($user_profile->last_name,0,1);
			$metaKeyword='Reviews for - '.$user_profile->first_name.' '.substr($user_profile->last_name,0,1);
		   
			$this->template->write('pageTitle',$pageTitle,TRUE);
			$this->template->write('metaDescription',$metaDescription,TRUE);
			$this->template->write('metaKeyword',$metaKeyword,TRUE);
			$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
			$this->template->write_view('content_center',$theme .'/layout/user/reviews',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();        
			   
	   }
	   
	   function airlinequote($profile_name='')
	   {
	   		
			$quote_user_data = get_user_profile_by_profile_name($profile_name);
			$quote_user_id = $quote_user_data->user_id;
			$data['airquote'] = $this->user_model->insert_air_quote($quote_user_id);
			$airquote = $data['airquote'];
			
			
			/*if($airquote == 1)
			{
				
					
				$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='AirLine Quote'");
				$email_temp=$email_template->row();
				
				if($email_temp)
				{
				
				$user_info=get_user_profile_by_profile_name($profile_name);
				$quote_user_data = get_user_profile_by_id(get_authenticateUserID());
				
				$user_name = $user_info->full_name;
				$ailinequote_user_name = $quote_user_data->full_name;
				$quote_user_link = site_url('user/'.$quote_user_data->profile_name);
				$air_line_from = $this->input->post('airline_from');
				$air_line_to = $this->input->post('airline_quote_to');
				$airline_message = $this->input->post('airline_message');
				
				$email_address_from=$email_temp->from_address;
				$email_address_reply=$email_temp->reply_address;
				
				$email_subject=$email_temp->subject;				
				$email_message=$email_temp->message;
				
				$email = $user_info->email;
				$email_to=$user_info->email;
				
				
				$email_message=str_replace('{break}','<br/>',$email_message);
				$email_message=str_replace('{user_name}',$user_name,$email_message);
				$email_message=str_replace('{quote_user_link}',$quote_user_link,$email_message);
				$email_message=str_replace('{air_line_from}',$air_line_from,$email_message);
				$email_message=str_replace('{air_line_to}',$air_line_to,$email_message);
				$email_message=str_replace('{airline_message}',$airline_message,$email_message);
				$email_temp_url=base_url().'email/';
				$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
				$str=$email_message;
				/** custom_helper email function **/
				/*email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str);
			}
				
				redirect('user/'.$profile_name.'/airline');
			}
			else
			{
				redirect('user/'.$profile_name);
			}*/
			
			redirect('travelagent/'.$profile_name.'/airline');
	   }
	   
	   function tripquote($profile_name='')
	   {
			$quote_user_data = get_user_profile_by_profile_name($profile_name);
			$quote_user_id = $quote_user_data->user_id;
			$data['tripquote'] = $this->user_model->insert_trip_quote($quote_user_id);
			redirect('travelagent/'.$profile_name.'/tripquote');
	   }
	   
	   function load_air_trip($profile_name='',$deal_id='')
	   {
	   		$data = array();
			$theme = getThemeName();
			$this->template->set_master_template($theme .'/template.php');
			$data['theme']=$theme;
			$data['profile_name']=$profile_name;
			$data['deal_id']=$deal_id;
			$this->load->view($theme.'/layout/quote/air_trip',$data);
			
	   }
	   function load_trip_quote($profile_name='',$deal_id='')
	   {
	   		$data = array();
			$theme = getThemeName();
			$this->template->set_master_template($theme .'/template.php');
			$data['theme']=$theme;
			$data['profile_name']=$profile_name;
			$data['deal_id']=$deal_id;
			$this->load->view($theme.'/layout/quote/trip_quote',$data);
	   }
	  
	   function getagnetemail($user_id=0)
	   {
	   		
			//$this->session->sess_destroy();
			
	   		if($user_id=='' || $user_id==0)
			{
				?>
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal">&times;</a>
                        <h3 class="fontred">Not Found</h3>
                    </div>
                    <div class="modal-body">
                        <p>User record does not exists.</p>
                    </div>
                    <div class="modal-footer">
                        <a class="btn" data-dismiss="modal">Close</a>
                    </div>
                    <?php
                    
			}
			else
			{
				$user_info=check_user_agent($user_id);
				
				if(!$user_info)
				{
					?>
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal">&times;</a>
                        <h3 class="fontred">Not Found</h3>
                    </div>
                    <div class="modal-body">
                        <p>User record does not exists.</p>
                    </div>
                    <div class="modal-footer">
                        <a class="btn" data-dismiss="modal">Close</a>
                    </div>
                    <?php
				}
				else
				{
				?>
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal">&times;</a>
                        <h3 class="fontblue"><?php echo ucwords($user_info->agency_name).' ('.ucwords($user_info->agency_name).')';?></h3>
                    </div>
                    <div class="modal-body">
                        
                           <strong>E-mail&nbsp; :</strong>                    
						<span>&nbsp;&nbsp;<?php echo $user_info->email; ?></span>
                        
                        <br /><br />

					<?php if($user_info->agency_contact_email!='') { ?>
						  <strong>Support Email&nbsp; :</strong>                    
						<span>&nbsp;&nbsp;<?php echo $user_info->agency_contact_email; ?></span>
                        <?php } ?>
                       
                    </div>
                    <div class="modal-footer">
                        <a class="btn" data-dismiss="modal">Close</a>
                    </div>
                    <?php
				}
				
				
			}
			
	   }

 		function getagnetphone($user_id=0)
	   {
	   		if($user_id=='' || $user_id==0)
			{
				?>
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal">&times;</a>
                        <h3 class="fontred">Not Found</h3>
                    </div>
                    <div class="modal-body">
                        <p>User record does not exists.</p>
                    </div>
                    <div class="modal-footer">
                        <a class="btn" data-dismiss="modal">Close</a>
                    </div>
                    <?php
                    
			}
			else
			{
				$user_info=check_user_agent($user_id);
				
				if(!$user_info)
				{
					?>
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal">&times;</a>
                        <h3 class="fontred">Not Found</h3>
                    </div>
                    <div class="modal-body">
                        <p>User record does not exists.</p>
                    </div>
                    <div class="modal-footer">
                        <a class="btn" data-dismiss="modal">Close</a>
                    </div>
                    <?php
				}
				else
				{
					
					?>
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal">&times;</a>
                        <h3 class="fontblue"><?php echo ucwords($user_info->agency_name).' ('.ucwords($user_info->agency_name).')';?></h3>
                    </div>
                    <div class="modal-body">
                        
                        <?php if($user_info->agency_phone!=''){?>
                        
                        <strong>Contact &nbsp; :</strong>
						<span class="support">&nbsp; &nbsp;<?php echo $user_info->agency_phone; ?></span>
                        <div class="clear"></div>
                        <?php }?>
                        
                        <?php if($user_info->agency_customer_support_no != ''){?>
                        <br />
                        <strong>Support &nbsp; :</strong>
						<span class="support">&nbsp; &nbsp;<?php echo $user_info->agency_customer_support_no; ?>(24X7, 365 days - help) for travel</span>
                        <div class="clear"></div>
                        <?php }?>
                        
                        
                        
                    </div>
                    <div class="modal-footer">
                       <!-- <a class="btn btn-primary" onclick="$('.modal-body > form').submit();">Save Changes</a>-->
                        <a class="btn" data-dismiss="modal">Close</a>
                    </div>
                    <?php
				}
				
				
			}
			
	   }
	   
	   
	   
	   function showcaptcha($type='',$user_id='')
	   {
	   		 if($this->session->userdata('show34meAgent')!='') {
			 
			 ///======
             
             
             if($user_id=='' || $user_id==0)
			{
				?>
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal">&times;</a>
                        <h3 class="fontred">Not Found</h3>
                    </div>
                    <div class="modal-body">
                        <p>User record does not exists.</p>
                    </div>
                    <div class="modal-footer">
                        <a class="btn" data-dismiss="modal">Close</a>
                    </div>
                    <?php
                    
			}
			else
			{
				$user_info=check_user_agent($user_id);
				
				if(!$user_info)
				{
					?>
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal">&times;</a>
                        <h3 class="fontred">Not Found</h3>
                    </div>
                    <div class="modal-body">
                        <p>User record does not exists.</p>
                    </div>
                    <div class="modal-footer">
                        <a class="btn" data-dismiss="modal">Close</a>
                    </div>
                    <?php
				}
				else
				{
				?>
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal">&times;</a>
                        <h3 class="fontblue"><?php echo ucwords($user_info->agency_name).' ('.ucwords($user_info->agency_name).')';?></h3>
                    </div>
                    <div class="modal-body">
                        
                        
                        
                        <?php if($type=='em') { ?>
                        
                        
                           <strong>E-mail&nbsp; :</strong>                    
						<span>&nbsp;&nbsp;<?php echo $user_info->email; ?></span>
                        
                        <br /><br />

					<?php if($user_info->agency_contact_email!='') { ?>
						  <strong>Support Email&nbsp; :</strong>                    
						<span>&nbsp;&nbsp;<?php echo $user_info->agency_contact_email; ?></span>
                        <?php } ?>
                        
                     
                     <?php } if($type=='ph') { ?>
                     
                      <?php if($user_info->agency_phone!=''){?>
                        
                        <strong>Contact &nbsp; :</strong>
						<span class="support">&nbsp; &nbsp;<?php echo $user_info->agency_phone; ?></span>
                        <div class="clear"></div>
                        <?php }?>
                        
                        <?php if($user_info->agency_customer_support_no != ''){?>
                        <br />
                        <strong>Support &nbsp; :</strong>
						<span class="support">&nbsp; &nbsp;<?php echo $user_info->agency_customer_support_no; ?>(24X7, 365 days - help) for travel</span>
                        <div class="clear"></div>
                        <?php }?>
                     
                     <?php } ?>
                        
                        
                       
                    </div>
                    <div class="modal-footer">
                        <a class="btn" data-dismiss="modal">Close</a>
                    </div>
                    <?php
				}
				
				
			}
             
             
           ///===
			 
			 } else {
		?>
                <div class="modal-header">
                <a class="close" data-dismiss="modal">&times;</a>
                <h3 class="fontblue">Enter Captcha Code</h3>
                </div>
                <div class="modal-body">
                    <form id="frmcookie" name="frmcookie" action="" method="post">
                    <?php	
                    $site_setting=site_setting();
                    
                    $captcha_result='';
                    
                    
                    if($site_setting->captcha_enable==1) { ?>
                     <img src="<?php echo base_url().'captcha_code_file.php?rand='.rand();?>" id="captchaimg"><br />
                    <input id="6_letters_code" name="6_letters_code" type="text">
                    Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh<br />
                    
                    <?php }   ?> 
                    </form>
                </div>
                <div class="modal-footer">
                <?php if($type=='em') { ?>
                <a class="btn btn-primary" onclick="checkcaptcha();">Save Changes</a>
                <?php } if($type=='ph') { ?>
                <a class="btn btn-primary" onclick="checkcaptcha2();">Save Changes</a>
                <?php } ?>
                <a class="btn" data-dismiss="modal">Close</a>
                </div>
                <?php      
            }            
	   
	   }
	   
	function mypackage($limit='20',$offset=0,$msg='')
	{
		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="mypackage";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['msg']=$msg;
		
		$this->load->library('pagination');
		$config['uri_segment']='4';
		$config['base_url'] = base_url().'user/mypackage/'.$limit.'/';
		$config['total_rows']= $this->user_model->get_total_user_package();
	
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->user_model->get_user_package($limit,$offset);
		
		$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$pageTitle='Package - '.$meta_setting->title;
		$metaDescription='Package - '.$meta_setting->meta_description;
		$metaKeyword='Package  - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/user/list_package',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();		
	}   
	
	function videodownload($limit='20',$offset=0,$msg='')
	{
		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="videodownload";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['msg']=$msg;
		
		$this->load->library('pagination');
		$config['uri_segment']='4';
		$config['base_url'] = base_url().'user/videodownload/'.$limit.'/';
		$config['total_rows']= $this->user_model->get_total_user_videodownload();
	
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->user_model->get_user_videodownload($limit,$offset);
		
		$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		$pageTitle='Video Download - '.$meta_setting->title;
		$metaDescription='Video Download - '.$meta_setting->meta_description;
		$metaKeyword='Video Download  - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/user/list_video_download',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();		
	} 
	
	function designdownload($limit='20',$offset=0,$msg='')
	{
		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="designdownload";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['msg']=$msg;
		
		$this->load->library('pagination');
		$config['uri_segment']='4';
		$config['base_url'] = base_url().'user/designdownload/'.$limit.'/';
		$config['total_rows']= $this->user_model->get_total_user_designdownload();
	
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->user_model->get_user_designdownload($limit,$offset);
		
		$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		$pageTitle='Design Download - '.$meta_setting->title;
		$metaDescription='Design Download - '.$meta_setting->meta_description;
		$metaKeyword='Design Download  - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/user/list_design_download',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();		
	}   
	  
	
	
	function checkcaptcha()
	{
		if(empty($_SESSION['6_letters_code'] ) || strcasecmp($_SESSION['6_letters_code'], $_POST['entercode']) != 0)
		{  
			$msg="fail";
		}else{
			$msg="success";
			$show_agent_data=array(
			'show34meAgent'=>randomCode()
			);
			$this->session->set_userdata($show_agent_data);
			
		}
		echo $msg;
		die;
	}

}

?>