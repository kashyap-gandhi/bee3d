<?php
	
	
	/*
	Function name :get_user_profile_by_id()
	Parameter : $user_id(user id) 
	Return : array of user profile details
	Use : get user profile information
	*/
	
	function get_user_profile_by_id($user_id)
	{
		
		
		$CI =& get_instance();
		
		$query=$CI->db->query("select * from ".$CI->db->dbprefix('user')." usr LEFT JOIN ".$CI->db->dbprefix('user_profile')." pr on usr.user_id=pr.user_id where  usr.user_id='".$user_id."'  and (usr.user_status=1 or usr.user_status=2)");	
		if($query->num_rows()>0)
		{		
			return $query->row();
		}
		
		return 0;
	}
	
	
	function get_user_without_status_by_id($user_id)
	{
		$CI =& get_instance();
		
		$query=$CI->db->query("select * from ".$CI->db->dbprefix('user')." usr, ".$CI->db->dbprefix('user_profile')." pr where usr.user_id=pr.user_id and usr.user_id='".$user_id."' ");	
		
		if($query->num_rows()>0)
		{		
			return $query->row();
		}
		
		return 0;
	}
	
	
	function get_user_without_status_by_profile_name($profile_name)
	{
		$CI =& get_instance();
		
		$query=$CI->db->query("select * from ".$CI->db->dbprefix('user')." usr, ".$CI->db->dbprefix('user_profile')." pr where usr.user_id=pr.user_id and usr.profile_name='".$profile_name."' ");	
		
		if($query->num_rows()>0)
		{		
			return $query->row();
		}
		
		return 0;
	}
	
	
	
	function get_user_profile_by_profile_name($profile_name)
	{
		$CI =& get_instance();
		
		$query=$CI->db->query("select * from ".$CI->db->dbprefix('user')." usr, ".$CI->db->dbprefix('user_profile')." pr where usr.user_id=pr.user_id and usr.profile_name='".$profile_name."' and (usr.user_status=1 or usr.user_status=2)");
		
		if($query->num_rows()>0)
		{
			return $query->row();
		}
		else
		{
			return 0;
		}
	}
	
		
	function get_user_profile_by_email($email)
	{
		$CI =& get_instance();
		
		$query=$CI->db->query("select * from ".$CI->db->dbprefix('user')." usr, ".$CI->db->dbprefix('user_profile')." pr where usr.user_id=pr.user_id and usr.email='".$email."' ");	
		
		if($query->num_rows()>0)
		{		
			return $query->row();
		}
		
		return 0;
	}
	
	
	/*
	Function name :get_user_profile_by_fbid()
	Parameter : $user_id(user id) 
	Return : array of user profile details
	Use : get user profile information
	*/
	
	function get_user_profile_by_fbid($fb_id)
	{
		$CI =& get_instance();
		
		$query=$CI->db->query("select * from ".$CI->db->dbprefix('user')." usr, ".$CI->db->dbprefix('user_profile')." pr where usr.user_id=pr.user_id and usr.fb_id='".$fb_id."' and (usr.user_status=1 or usr.user_status=2)");	
		
		if($query->num_rows()>0)
		{		
			return $query->row();
		}
		
		return 0;
	}
	
	
	/****get user profile name
	***
	***/
	
	function getUserProfileName( $id = 0)
	{
		$CI =& get_instance();
		
		if($id >0)
		{
			$query = $CI->db->get_where("user",array('user_id'=>$id));
		}
		else {
			$query = $CI->db->get_where("user",array('user_id'=>get_authenticateUserID()));
		}
		
		if($query->num_rows()>0)
		{
			$result=$query->row();
		
			return $result->profile_name;
		}
		
		return 0;
		
	}
	
	/****chekc_user_verify_email
	***
	***/
	
	function chekc_user_verify_email()
	{
		$CI =& get_instance();
		
		if(get_authenticateUserID()>0)
		{
		
			$query = $CI->db->get_where("user",array('user_id'=>get_authenticateUserID()));
			
			if($query->num_rows()>0)
			{
				$result=$query->row();
				
				if($result->verify_email==1)
				{
					return 0; ///already verify
				}
				else
				{
					return 1; ///need to verify
				}
			
				
			}
			else
			{
				return 3; ///not exists
			}
			
			
		}
		else {
			return 2;  //==not login
		}
		
	}
	
	
	
	
	/*** load user notification setting
	*  return single record array
	**/
	
	function notification_setting($user_id)
	{		
		$CI =& get_instance();
		$query = $CI->db->get_where("user_notification", array('user_id'=>$user_id));
		return $query->row();
	
	}

	
	
	/****** get user current wallet amount
	*** return double 
	***/
	
	function my_wallet_amount()
	{
	
	  	 $CI =& get_instance();
		 
			 $query = $CI->db->query("SELECT SUM(debit) as sumd,SUM(credit) as sumc FROM  ".$CI->db->dbprefix('wallet')." where user_id='".get_authenticateUserID()."'"); 
			 
			
	 
	 		if($query->num_rows()>0)
			{
			
				 $result = $query->row();
			
				 $debit=$result->sumd;
				 $credit=$result->sumc;
				
				 $total=$credit-$debit;
				
				return $total;
			
			}
			
			return 0;
		
	}
	
		/********get withrawal user last withdraw type details
	** integer uid (user id), $type (bank, check, gateway)
	** return array
	***/
	
	function get_detail($uid='',$type)
	{
	  $CI =& get_instance();
	  if($type=="bank")
	  {
	    $query=$CI->db->query("SELECT * FROM trc_wallet_bank b,trc_wallet_withdraw w WHERE b.withdraw_id = w.withdraw_id AND w.user_id = '$uid' AND b.bank_withdraw_type='bank' ORDER BY b.bank_id DESC LIMIT 0 , 1");
		}
	  if($type=="gateway")
		{
		   $query=$CI->db->query("SELECT * FROM trc_wallet_withdraw_gateway b, trc_wallet_withdraw w WHERE b.withdraw_id = w.withdraw_id AND w.user_id = '$uid' ORDER BY b.gateway_withdraw_id desc LIMIT 0 , 1");
		}
	  if($type=="check")
	  {
	       $query=$CI->db->query("SELECT * FROM trc_wallet_bank b, trc_wallet_withdraw w WHERE b.withdraw_id = w.withdraw_id AND w.user_id = '$uid' AND b.bank_withdraw_type='check' ORDER BY b.bank_id desc LIMIT 0 , 1");
	  }
	  
	    if($query->num_rows()>0)
		{
			return $query->row();
		}
		
		return 0;
	}
	
	/****  get user total rate
	*	var integer total
	***/
	
	function get_user_total_rate($user_id)
	{
		$CI =& get_instance();
		
		$CI->db->select('SUM(user_rating) as total_rate,COUNT(*) as total_trip');
		$CI->db->from('user_review');
		$CI->db->where('review_to_user_id',$user_id);		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			$result= $query->row();
			
			$total_div=0;
			
			if($result->total_trip>0)
			{
				$total_div=number_format($result->total_rate/$result->total_trip,2);
			}
			
			return $total_div;
			
		}
		return 0;
		
	}
	
	function get_user_total_review($user_id)
	{
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('user_review');
		$CI->db->where('review_to_user_id',$user_id);		
		$query=$CI->db->get();
		return $query->num_rows();
		
	}
	
	/*Top trip of agent*/
	function top_trip_user($user_id)
	{
		$CI =& get_instance();
		
		$sql="select count(package_type) as total_trip,package_type from ".$CI->db->dbprefix('trip')." where trip_agent_assign_id='".$user_id."' group by package_type";
		
		$query = $CI->db->query($sql);
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return 0;
		}
	}
	
	function unique_user_code($rand,$length=12)
	{		
		$chk=check_user_code($rand);
		
		if($chk==1)
		{
			$rand=getrandomCode($length);
			unique_user_code($rand);		
		}
				
		return $rand;  	
		
	}
	function check_user_code($rand)
	{
		$CI =& get_instance();
		$query=$CI->db->get_where('user',array('unique_code'=>$rand));
		if($query->num_rows()>0)
		{
			return 1;
		}
		return 0;
	}
	
	
	
	function get_total_agents()
	{
		
		
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('user us');
		$CI->db->join('user_profile up','us.user_id=up.user_id');
		$CI->db->join('agent_profile ag','us.user_id=ag.user_id');
		$CI->db->where('us.user_status',1);		
		//$CI->db->where('ag.agent_status',1);	
		$CI->db->where('ag.agent_app_approved',1);	
		$CI->db->where('us.verify_email',1);	
			
		
		$query=$CI->db->get();
		
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		return 0;
	}
	
	
	
	function get_top_agent($limit)
	{
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('user us');
		$CI->db->join('user_profile up','us.user_id=up.user_id');
		$CI->db->join('agent_profile ag','us.user_id=ag.user_id');
		$CI->db->where('us.user_status',1);
		//$CI->db->where('ag.agent_status',1);
		$CI->db->where('ag.agent_app_approved',1);
		$CI->db->where('us.verify_email',1);
		$CI->db->order_by('ag.agent_level','desc');
		$CI->db->limit($limit);
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		return 0;
	}
	
	
	function check_user_applied_for_agent($user_id)
	{
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('user us');
		$CI->db->join('user_profile up','us.user_id=up.user_id');
		$CI->db->join('agent_profile ag','us.user_id=ag.user_id');
		$CI->db->where('us.user_id',$user_id);		
		
		$query=$CI->db->get();

		if($query->num_rows()>0)
		{
			return $query->row();
		}
		return 0;
	}
	
	
	function check_user_agent($user_id)
	{
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('user us');
		$CI->db->join('user_profile up','us.user_id=up.user_id');
		$CI->db->join('agent_profile ag','us.user_id=ag.user_id');
		$CI->db->where('us.user_id',$user_id);		
		$CI->db->where('us.user_status',1);		
		//$CI->db->where('ag.agent_status',1);	
		$CI->db->where('us.verify_email',1);
		$CI->db->where('ag.agent_app_approved',1);			
		
		$query=$CI->db->get();

		if($query->num_rows()>0)
		{
			return $query->row();
		}
		return 0;
	}
	
	
	
	function get_agent_document($user_id)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('agent_document');
		$CI->db->where('user_id',$user_id);	
		
		$query = $CI->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
	}


	/*
	Function name :get_user_card_info()
	Parameter : none 
	Return : array of user card information
	Use : get user credit card information
	*/
	
	function get_user_card_info($user_id)
	{
		$CI =& get_instance();
		$query=$CI->db->get_where('user_card_info',array('user_id'=>$user_id));
		
		if($query->num_rows()>0)
		{
			return $query->row();
		}
		
		return 0;
		
	}

	function get_user_agent_detail($user_id)
	{
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('user us');
		$CI->db->join('user_profile up','us.user_id=up.user_id');
		$CI->db->join('agent_profile ag','us.user_id=ag.user_id');
		$CI->db->where('us.user_id',$user_id);		
		$CI->db->where('us.user_status',1);		
		//$CI->db->where('ag.agent_status',1);	
			
		$query=$CI->db->get();

		if($query->num_rows()>0)
		{
			return $query->row();
		}
		return 0;
	}
	
	
	function check_user_profile_exists($profile_name)
	{
		$CI =& get_instance();
		$query=$CI->db->get_where('user',array('profile_name'=>$profile_name, 'user_id !=' =>get_authenticateUserID()));
		if($query->num_rows()>0)
		{
			return 1;
		}
		else
		{
			return 0;		
		}
		
	
	}

	function get_user_total_trip_rate($user_id,$package_type)
	{
		
		
		$CI =& get_instance();
		
		$CI->db->select('trip_id');
		$CI->db->from('trip');
		$CI->db->where_in('package_type',$package_type);
		$CI->db->where('trip_agent_assign_id',$user_id);
		
		$get_trip=$CI->db->get();
		
		if($get_trip->num_rows()>0)
		{
			$result2=$get_trip->result();
			
			$trip_id='';
			
			
			foreach($result2 as $res)
			{
				$trip_id.=$res->trip_id.',';
			}
			
			if($trip_id!='')
			{
				$im_trip_id=substr($trip_id,0,-1);
				
				
				///////======
				
				$CI->db->select('SUM(user_rating) as total_rate,COUNT(*) as total_trip');
				$CI->db->from('user_review');
				$CI->db->where('review_to_user_id',$user_id);		
				$CI->db->where_in('trip_id',$im_trip_id);		
				$query=$CI->db->get();
				
				if($query->num_rows()>0)
				{
					$result= $query->row();
					
					$total_div=0;
					
					if($result->total_trip>0)
					{
						$total_div=number_format($result->total_rate/$result->total_trip,2);
					}
					
					return $total_div;
					
				}
				return 0;
					
					////=====
						
						
						
					}
					
			
			return 0;
						
		}
		
		return 0;
		
	}
	
	function get_user_reviews($user_id,$reviews_limit)
	{
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('user_review ur');
		$CI->db->join('trip tp','ur.trip_id=tp.trip_id');
		$CI->db->join('user us','ur.review_by_user_id=us.user_id');
		$CI->db->join('user_profile up','us.user_id=up.user_id');
		$CI->db->where('ur.review_to_user_id',$user_id);
		$CI->db->order_by('ur.user_review_date','desc');
		$CI->db->limit($reviews_limit);
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}

		return 0;

	}
	
	function agent_total_trip($user_id='')
	{
	
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('egg_trip');
		$CI->db->where('user_id',$user_id);	
		$query = $CI->db->get();
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;
	}
	
	
	function check_message_reply($trip_conversation_id='')
	{
		$CI =& get_instance();
		$query = $CI->db->get_where("trip_coversation",array('reply_trip_conversation_id'=>$trip_conversation_id));
		if($query->num_rows()>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	
	function get_trip_review($trip_id,$user_id){
	
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('user_review ur');
		$CI->db->where('ur.trip_id',$trip_id);
		$CI->db->where('ur.review_to_user_id',$user_id);
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->row();
		}

		return 0;
	}
	
	/*19-11-2012*/
	function member_association()
	{
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('member_association ma');
		$CI->db->where('ma.association_status',1);
		$query=$CI->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}

		return 0;
	}
	
	function get_user_deal($user_id='')
	{	
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('deal');
		$CI->db->where('user_id',$user_id);	
		$CI->db->where('deal_status',2);	
		$query = $CI->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
	}
	function total_trip_user($user_id='')
	{
	
		$CI =& get_instance();
		$query=$CI->db->get_where('trip',array('user_id'=>$user_id));
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;
	}
	function total_quote_user($user_id='')
	{
	
		$CI =& get_instance();
		$query=$CI->db->get_where('trip_quote',array('user_id'=>$user_id));
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;
	}
	
	/* End of file user_helper.php */
/* Location: ./system/application/helpers/user_helper.php */
	
	
?>