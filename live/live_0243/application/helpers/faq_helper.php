<?php
	/*
	Function name :get_faq_category()
	Parameter : none
	Return : array of faq category
	Use : get all active faq category
	*/
	
	function get_faq_category()
	{
		$CI =& get_instance();
		
		$query=$CI->db->query("select * from ".$CI->db->dbprefix('faq_category')." where (active=1 or active='1') order by faq_category_order asc");	
		
		if($query->num_rows()>0)
		{		
			return $query->result();
		}
		
		return 0;
	}
	
	
	/*
	Function name :get_faq_category()
	Parameter : $faq_category_id
	Return : array of faq question
	Use : get all active faq question
	*/
	
	function get_category_faq($faq_category_id)
	{
		$CI =& get_instance();
		
		$query=$CI->db->query("select * from ".$CI->db->dbprefix('faq')." where (active=1 or active='1') and faq_category_id='".$faq_category_id."' order by faq_order asc");	
		
		if($query->num_rows()>0)
		{		
			return $query->result();
		}
		
		return 0;
	}
	
	
?>