<?php
class Favourite_model extends IWEB_Model 
{

	/*
	Function name :User_model
	Description :its default constuctor which called when user_model object initialzie.its load necesary parent constructor
	*/
	function Favourite_model()
    {
        parent::__construct();	
    } 
	
	function get_total_favourite_design_count()
	{
		$user_id = get_authenticateUserID();
		$this->db->select('d.*,c.category_name');
		$this->db->from('design d');
		$this->db->join('design_like dl','dl.design_id=d.design_id');
		//$this->db->where('d.design_status','1');
		$this->db->join('design_category_rel dr','d.design_id=dr.design_id');
		$this->db->join('design_category c','c.category_id=dr.category_id','LEFT');
		$this->db->where('dl.user_id',$user_id);
		$this->db->where('dl.like_status',1);
		$this->db->order_by('d.design_id','desc');
		$this->db->group_by('dl.like_id');
		//$this->db->limit($limit);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_favourite_design_result($limit,$offset)
	{
		$user_id = get_authenticateUserID();
		$this->db->select('d.*,c.category_name');
		$this->db->from('design d');
		$this->db->join('design_like dl','dl.design_id=d.design_id');
		//$this->db->where('d.design_status','1');
		$this->db->join('design_category_rel dr','d.design_id=dr.design_id');
		$this->db->join('design_category c','c.category_id=dr.category_id','LEFT');
		$this->db->where('dl.user_id',$user_id);
		$this->db->where('dl.like_status',1);
		$this->db->order_by('d.design_id','desc');
		$this->db->group_by('dl.like_id');
		//$this->db->limit($limit);
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		//echo $this->db->last_query();
		//die;
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	function get_total_favourite_video_count()
	{
		$user_id = get_authenticateUserID();
		$this->db->select('v.*,c.category_name');
		$this->db->from('video v');
		$this->db->join('video_like dl','dl.video_id=v.video_id');
		$this->db->join('video_category_rel dr','v.video_id=dr.video_id');
		$this->db->join('video_category c','c.category_id=dr.category_id','LEFT');
		$this->db->where('dl.user_id',$user_id);
		$this->db->where('dl.like_status',1);
		$this->db->group_by('dl.video_id');
		$this->db->order_by('dl.like_id','desc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	
	
	function get_favourite_video_result($limit,$offset)
	{
		$user_id = get_authenticateUserID();
		$this->db->select('v.*,c.category_name');
		$this->db->from('video v');
		$this->db->join('video_like dl','dl.video_id=v.video_id');
		$this->db->join('video_category_rel dr','v.video_id=dr.video_id');
		$this->db->join('video_category c','c.category_id=dr.category_id','LEFT');
		$this->db->where('dl.user_id',$user_id);
		$this->db->where('dl.like_status',1);
		$this->db->group_by('dl.video_id');
		$this->db->order_by('dl.like_id','desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	function get_total_category_video_count($category_id,$keyword)
	{
		$this->db->select('d.*,c.category_name');
		$this->db->from('video d');
		$this->db->join('video_category_rel dr','d.video_id=dr.video_id');
		$this->db->join('video_category c','c.category_id=dr.category_id','LEFT');
		$this->db->join('video_tags vd','vd.video_id=d.video_id','LEFT');
		$this->db->join('tags tg','tg.tag_id=vd.tag_id','LEFT');
		$this->db->group_by('video_id');
		$this->db->order_by('video_id','desc');
		if($category_id > 0)
		{
			$this->db->where('c.category_id',$category_id);
		}
		if($keyword !='')
		{
			$this->db->like('d.video_title',$keyword);
			
			if(substr_count($keyword,' ')>=1)
			{
				$ex=explode(' ',$keyword);
				
				foreach($ex as $val)
				{
					$this->db->like('d.video_title',$val);
				}
			}
			$this->db->or_like('tg.tag_name',$keyword);	

		}
		//$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_category_video_result($limit,$offset,$category_id,$keyword)
	{
		$this->db->select('d.*,c.category_name');
		$this->db->from('video d');
		$this->db->join('video_category_rel dr','d.video_id=dr.video_id');
		$this->db->join('video_category c','c.category_id=dr.category_id','LEFT');
		$this->db->join('video_tags vd','vd.video_id=d.video_id','LEFT');
		$this->db->join('tags tg','tg.tag_id=vd.tag_id','LEFT');
		$this->db->group_by('video_id');
		$this->db->order_by('video_id','desc');
		if($category_id > 0)
		{
			$this->db->where('c.category_id',$category_id);
		}
		if($keyword !='')
		{
			$this->db->like('d.video_title',$keyword);
			
			if(substr_count($keyword,' ')>=1)
			{
				$ex=explode(' ',$keyword);
				
				foreach($ex as $val)
				{
					$this->db->like('d.video_title',$val);
				}	
			}
			$this->db->or_like('tg.tag_name',$keyword);	

		}
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
		
	}
	
		
}
?>