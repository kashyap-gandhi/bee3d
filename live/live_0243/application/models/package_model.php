<?php
class Package_model extends IWEB_Model 
{

	/*
	Function name :Home_model
	Description :its default constuctor which called when Home_model object initialzie.its load necesary parent constructor
	*/
	function Package_model()
    {
        parent::__construct();	
    } 
	
	
	function get_package_list()
	{
	    $qry = $this->db->get_where("package",array("package_type"=>1));
		
		if($qry->num_rows()>0)
		{
		      return $qry->result();
		}
		
		return 0;
	}
	
	
	/*
	Function name :get_gateway_result()
	Parameter : $id (payment gateway id), $name (payment gateway setting key pair name)
	Return :  array payment gateway setting key pair value
	Use : get one payment gateway setting detail 
	*/
	
	function get_gateway_result($id,$name)
	{
		 $query = $this->db->query("SELECT * FROM ".$this->db->dbprefix('gateways_details')." where payment_gateway_id='".$id."' and name='".$name."' order by payment_gateway_id desc");  
	     if($query->num_rows()>0)
		 {
			return $query->row();
		  }
		return 0;	
	}
	
	
	function get_one_package_detail($id = 0)
	{
	    $qry = $this->db->get_where("package",array("package_type"=>1,"package_id"=>$id));
		
		if($qry->num_rows()>0)
		{
		      return $qry->row_array();
		}
		
		return 0;
	}
	
		/*
	Function name :check_unique_transaction()
	Parameter : $txn_id (transaction id)
	Return :  1 or 0
	Use : check for unique transaction ID in the database and stop double entry in the system 
	*/
	
	function check_unique_transaction($txn_id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('wallet')." where wallet_transaction_id='".$txn_id."'");
		
		if($query->num_rows()>0)
		{
			return 1;
		}
		
		return 0;
	
	}
		
}	