<?php
class Challenge_model extends IWEB_Model 
{

	/*
	Function name :User_model
	Description :its default constuctor which called when user_model object initialzie.its load necesary parent constructor
	*/
	function Challenge_model()
    {
        parent::__construct();	
    } 
	function check_exist_challenge($challenge_id,$user_id)
	{
		$this->db->select('*');
		$this->db->from('challenge');
		$this->db->where('challenge_id',$challenge_id);
		if($user_id > 0)
		{
			//For check in view also
			$this->db->where('user_id',$user_id);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	function insert_challenge()
	{
		$data_insert = $_POST;
		$challenge_slug = get_challenge_slug($this->input->post('challenge_title'),0);
		$data_merge = array('challenge_title'=>$this->input->post('challenge_title'),
					  'challenge_slug'=>$challenge_slug,
					  'user_id'=>get_authenticateUserID(),
					  'challenge_date'=>date('Y-m-d h:i:s'),
					  'challenge_ip'=>$_SERVER['REMOTE_ADDR'],
					  'challenge_status'=>1,
					
					  );
				
				
		$main_insert_data = array_merge($data_insert,$data_merge);
	     unset($main_insert_data["challenge_category"]);
		 unset($main_insert_data["tag_name"]);
		 unset($main_insert_data["submit"]);
		$this->db->insert('challenge',$main_insert_data);
		$challenge_id = mysql_insert_id();
				
				
		//Update slug
		$data_slug = array('challenge_slug'=>$challenge_slug.'-'.$challenge_id);
		$this->db->where('challenge_id',$challenge_id);
		$this->db->update('challenge',$data_slug);		
		
		/*Insert data in challenge_category_rel*/
		$data_challenge_category_rel= array('category_id'=>$this->input->post('challenge_category'),
										    'challenge_id'=>$challenge_id);
		$this->db->insert('challenge_category_rel',$data_challenge_category_rel);
		
		
	
		
		/*Insert Tags*/
		$tags = explode(",",$this->input->post('tag_name'));
		if($tags)
		{
			for($i=0;$i<count($tags);$i++)
			 {
				$tag_id = check_tag($tags[$i]);
				$data_tag = array('challenge_id'=>$challenge_id,
								'tag_id'=>$tag_id,
								'tag_date'=>date('Y-m-d h:i:s'),
								'tag_ip'=>$_SERVER['REMOTE_ADDR']);
				$this->db->insert('challenge_tags',$data_tag);				
			 }
			 
			 //challenge_tags
		}
		
		
		
	}
	
	function update_challenge()
	{
		$data_update = $_POST; 
		$challenge_slug = get_challenge_slug($this->input->post('challenge_title'),0);
		$challenge_id = $this->input->post('challenge_id');
		$challenge_slug = $challenge_slug.'-'.$challenge_id;
		
		$data_merge = array('challenge_title'=>$this->input->post('challenge_title'),
					  'challenge_slug'=>$challenge_slug,
					  'user_id'=>get_authenticateUserID(),
					  'challenge_date'=>date('Y-m-d h:i:s'),
					  'challenge_ip'=>$_SERVER['REMOTE_ADDR'],
					  'challenge_status'=>1,
					
					  );
		
		
		$main_update_data = array_merge($data_update,$data_merge);
	     unset($main_update_data["challenge_category"]);
		 unset($main_update_data["tag_name"]);
		 unset($main_update_data["submit"]);
		
		$this->db->where('challenge_id',$challenge_id);
		$this->db->update('challenge',$main_update_data);
		
		/*Insert data in challenge_category_rel*/
		$data_challenge_category_rel= array('category_id'=>$this->input->post('challenge_category'),
										 'challenge_id'=>$challenge_id);
		$this->db->where('challenge_id',$challenge_id);
		$this->db->update('challenge_category_rel',$data_challenge_category_rel);
		
		
		
		/*Insert Tags*/
		//Delete challenge tag
		$delete_tag=$this->db->query("delete from ".$this->db->dbprefix('challenge_tags')." where challenge_id='".$challenge_id."'");
		
		$tags = explode(",",$this->input->post('tag_name'));
		if($tags)
		{
			for($i=0;$i<count($tags);$i++)
			 {
				$tag_id = check_tag($tags[$i]);
				$data_tag = array('challenge_id'=>$challenge_id,
								'tag_id'=>$tag_id,
								'tag_date'=>date('Y-m-d h:i:s'),
								'tag_ip'=>$_SERVER['REMOTE_ADDR']);
				$this->db->insert('challenge_tags',$data_tag);				
			 }
			 
			 //challenge_tags
		}
	
		
	}
	
	function get_popular_challenge($limit)
	{
		$this->db->select('d.*,c.category_name');
		$this->db->from('challenge d');
		$this->db->where('d.challenge_status','1');
		$this->db->join('challenge_category_rel dr','d.challenge_id=dr.challenge_id');
		$this->db->join('challenge_category c','c.category_id=dr.category_id');
	//	$this->db->where('d.user_id',$user_id);
		$this->db->order_by('challenge_like_count','desc');
		$this->db->limit($limit);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	function get_user_challenge($user_id,$limit)
	{
	
		$this->db->select('d.*,c.category_name');
		$this->db->from('challenge d');
		$this->db->where('d.challenge_status','1');
		$this->db->join('challenge_category_rel dr','d.challenge_id=dr.challenge_id');
		$this->db->join('challenge_category c','c.category_id=dr.category_id');
		$this->db->where('d.user_id',$user_id);
		$this->db->order_by('d.challenge_id','desc');
		//$this->db->order_by('challenge_like_count','desc');
		$this->db->limit($limit);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
	function get_total_user_challenge_running_count($user_id)
	{
		$this->db->select('d.*,c.category_name');
		$this->db->from('challenge_bid bd');
		$this->db->join('challenge d','bd.challenge_id=d.challenge_id');
		$this->db->join('challenge_category_rel dr','d.challenge_id=dr.challenge_id');
		$this->db->join('challenge_category c','c.category_id=dr.category_id','LEFT');
		$this->db->where('bd.user_id',$user_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_challenge_running_result($user_id,$limit,$offset)
	{

		$this->db->select('bd.*,d.*,c.category_name');
		$this->db->from('challenge_bid bd');
		$this->db->join('challenge d','bd.challenge_id=d.challenge_id');
		$this->db->join('challenge_category_rel dr','d.challenge_id=dr.challenge_id');
		$this->db->join('challenge_category c','c.category_id=dr.category_id','LEFT');
		$this->db->where('bd.user_id',$user_id);
		$this->db->order_by('challenge_bid_id','desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
	function get_toala_user_challenge_count($user_id)
	{
		$this->db->select('d.*,c.category_name');
		$this->db->from('challenge d');
		$this->db->join('challenge_category_rel dr','d.challenge_id=dr.challenge_id');
		$this->db->join('challenge_category c','c.category_id=dr.category_id','LEFT');
		$this->db->where('d.user_id',$user_id);
		$this->db->order_by('challenge_id','desc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_challenge_result($user_id,$limit,$offset)
	{

		$this->db->select('d.*,c.category_name');
		$this->db->from('challenge d');
		$this->db->join('challenge_category_rel dr','d.challenge_id=dr.challenge_id');
		$this->db->join('challenge_category c','c.category_id=dr.category_id','LEFT');
		$this->db->where('d.user_id',$user_id);
		$this->db->order_by('challenge_id','desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
	function get_one_challenge($challenge_id)
	{
		$this->db->select('d.*,dr.category_id');
		$this->db->from('challenge d');
		$this->db->join('challenge_category_rel dr','d.challenge_id=dr.challenge_id');
		//$this->db->join('challenge_category c','c.category_id=dr.category_id');
		$this->db->where('d.challenge_id',$challenge_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else
		{
			return 0;
		}
	}	
	
	function get_one_challenge_tag($challenge_id)
	{
		$this->db->select('tg.challenge_tags_id,t.tag_name,tg.tag_id');
		$this->db->from('challenge_tags tg');
		$this->db->join('tags t','t.tag_id=tg.tag_id');
		$this->db->where('tg.challenge_id',$challenge_id);
		$this->db->group_by('tg.tag_id');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
		//$query=$this->db->get_where('challenge_tags',array('challenge_id'=>$challenge_id));
		return $query->result();	
		}
	}
	
	
	
	function get_one_challenge_attachment($attachment_id)
	{
		$this->db->select('*');
		$this->db->from('challenge_attachment');
		$this->db->where('attachment_id',$attachment_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
		//$query=$this->db->get_where('challenge_tags',array('challenge_id'=>$challenge_id));
		return $query->row();	
		}
	}
	
	function get_one_main_challenge_image($challenge_id)
	{
		$this->db->select('*');
		$this->db->from('challenge_attachment');
		$this->db->where('attachment_is_main',1);
		$this->db->where('challenge_id',$challenge_id);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
		//$query=$this->db->get_where('challenge_tags',array('challenge_id'=>$challenge_id));
		$res =  $query->row();	
		return $res->attachment_name;
		}
	}
	
	function get_total_challenge_count()
	{
		$this->db->select('d.*,c.category_name');
		$this->db->from('challenge d');
		$this->db->join('challenge_category_rel dr','d.challenge_id=dr.challenge_id');
		$this->db->join('challenge_category c','c.category_id=dr.category_id','LEFT');
		$this->db->order_by('challenge_id','desc');
		//$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}	
	
	function get_all_challenge_result($limit,$offset)
	{
		$this->db->select('d.*,c.category_name');
		$this->db->from('challenge d');
		$this->db->join('challenge_category_rel dr','d.challenge_id=dr.challenge_id');
		$this->db->join('challenge_category c','c.category_id=dr.category_id','LEFT');
		$this->db->order_by('challenge_id','desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	function get_total_category_challenge_count($category_id,$keyword)
	{
		$this->db->select('d.*,c.category_name');
		$this->db->from('challenge d');
		$this->db->join('challenge_category_rel dr','d.challenge_id=dr.challenge_id');
		$this->db->join('challenge_category c','c.category_id=dr.category_id','LEFT');
		$this->db->join('challenge_tags dt','dt.challenge_id=d.challenge_id','LEFT');
		$this->db->join('tags tg','tg.tag_id=dt.tag_id','LEFT');
		
		$this->db->order_by('challenge_id','desc');
		if($category_id > 0)
		{
			 $cond = "dr.category_id in ('".str_replace("-",",",$category_id)."')";
			  
			  $this->db->where($cond);
		}
		if($keyword !='' && $keyword != "0")
		{
			$this->db->like('d.challenge_title',$keyword);
			
			if(substr_count($keyword,' ')>=1)
			{
				$ex=explode(' ',$keyword);
				
				foreach($ex as $val)
				{
					$this->db->like('d.challenge_title',$val);
				}	
			}

			$this->db->or_like('tg.tag_name',$keyword);
		}
		//$this->db->limit($limit,$offset);
		$this->db->group_by('d.challenge_id');
		$query = $this->db->get();
		//echo $this->db->last_query();
		//die;
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_category_challenge_result($limit,$offset,$category_id,$keyword)
	{
		$this->db->select('d.*,c.category_name');
		$this->db->from('challenge d');
		$this->db->join('challenge_category_rel dr','d.challenge_id=dr.challenge_id');
		$this->db->join('challenge_category c','c.category_id=dr.category_id','LEFT');
		$this->db->join('challenge_tags dt','dt.challenge_id=d.challenge_id','LEFT');
		$this->db->join('tags tg','tg.tag_id=dt.tag_id','LEFT');
		$this->db->order_by('challenge_id','desc');
		if($category_id > 0)
		{
			
			 $cond = "dr.category_id in ('".str_replace("-",",",$category_id)."')";
			  
			  $this->db->where($cond);
		}
		
		
		if($keyword !=''  && $keyword != "0")
		{
			
			$this->db->like('d.challenge_title',$keyword);
			
			if(substr_count($keyword,' ')>=1)
			{
				$ex=explode(' ',$keyword);
				
				foreach($ex as $val)
				{
					$this->db->like('d.challenge_title',$val);
				}	
			}
			$this->db->or_like('tg.tag_name',$keyword);	

		}
		$this->db->group_by('d.challenge_id');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
		
	}
	
	/*
	Function name :get_open_task_list()
    Return array of all  open challenge
   */
	function get_open_challenge_list($user_id = 0 ,$limit =0 ,$offset = 0)
	{

	
		$this->db->select('d.*,c.category_name');
		$this->db->from('challenge d');
		
		$this->db->join('challenge_category_rel dr','d.challenge_id=dr.challenge_id');
		$this->db->join('challenge_category c','c.category_id=dr.category_id');
		$this->db->where('d.user_id',$user_id);
	     $this->db->where('d.challenge_status',1);
		$this->db->where('d.challenge_activity_status',0);
	    $this->db->order_by('d.challenge_id','desc');
		$this->db->limit($limit,$offset);
		
		
		$query = $this->db->get();
		
		if($query->num_rows > 0) {
			return $query->result();
		}
		 return 0;
	}
	
	/*
	Function name :get_total_open_task()
	Return array of all  open challenge count
	*/
	function get_total_open_challenge_count($user_id = 0)
	{

		$this->db->select('d.challenge_id');
		$this->db->from('challenge d');
	
		$this->db->join('challenge_category_rel dr','d.challenge_id=dr.challenge_id');
		$this->db->join('challenge_category c','c.category_id=dr.category_id');
		$this->db->where('d.user_id',$user_id);
		$this->db->where('d.challenge_status',1);
		$this->db->where('d.challenge_activity_status',0);
	   $query = $this->db->get();
		
		if($query->num_rows > 0) {
		return $query->num_rows();
		}
		else
		{
			return 0;
		}
	
	}
	
	
	/*
	Function name :get_assigned_challenge_list()
    Return array of all  assigned tasks
   */
	function get_assigned_challenge_list($user_id = 0 ,$limit =0 ,$offset = 0)
	{

	
		$this->db->select('d.*,c.category_name');
	    $this->db->from('challenge d');
	    $this->db->join('challenge_bid cb','d.challenge_id=cb.challenge_id');
		$this->db->join('challenge_category_rel dr','d.challenge_id=dr.challenge_id');
		$this->db->join('challenge_category c','c.category_id=dr.category_id');
		$this->db->where('cb.user_id',$user_id);
		$this->db->where('d.challenge_status',1);
		$this->db->where('cb.is_won',1);
		$this->db->where_in('d.challenge_activity_status',array('1','2'));
	    $this->db->order_by('d.challenge_id','desc');
		$this->db->limit($limit,$offset);
		
		
		$query = $this->db->get();
		
		if($query->num_rows > 0) {
			return $query->result();
		}
		 return 0;
	}
	
	/*
	Function name :get_total_assigned_challenge_count()
	Return array of all  assigned challenge count
	*/
	function get_total_assigned_challenge_count($user_id = 0)
	{

		$this->db->select('d.challenge_id');
		$this->db->from('challenge d');
	    $this->db->join('challenge_bid cb','d.challenge_id=cb.challenge_id');
		$this->db->join('challenge_category_rel dr','d.challenge_id=dr.challenge_id');
		$this->db->join('challenge_category c','c.category_id=dr.category_id');
		$this->db->where('cb.user_id',$user_id);
		$this->db->where('cb.is_won',1);
		$this->db->where('d.challenge_status',1);
		$this->db->where_in('d.challenge_activity_status',array('1','2'));
		$query = $this->db->get();
		
		return $query->num_rows();
	
	}
	
	
	/*
	Function name :get_closed_challenge_list()
    Return array of all  closed tasks
   */
	function get_closed_challenge_list($user_id = 0 ,$limit =0 ,$offset = 0)
	{

	
		$this->db->select('d.*,c.category_name');
	    $this->db->from('challenge d');
	    $this->db->join('challenge_category_rel dr','d.challenge_id=dr.challenge_id');
		$this->db->join('challenge_category c','c.category_id=dr.category_id');
		$this->db->where('d.user_id',$user_id);
		$this->db->where('d.challenge_status',1);
		$this->db->where('d.challenge_activity_status','3');
		$this->db->or_where('d.challenge_activity_status','2');
	    $this->db->order_by('d.challenge_id','desc');
		$this->db->limit($limit,$offset);
		
		
		$query = $this->db->get();
		
		if($query->num_rows > 0) {
			return $query->result();
		}
		 return 0;
	}
	
	/*
	Function name :get_total_closed_challenge_count()
	Return array of all  closed tasks count
	*/
	function get_total_closed_challenge_count($user_id = 0)
	{

		$this->db->select('d.challenge_id');
		$this->db->from('challenge d');
	    $this->db->join('challenge_category_rel dr','d.challenge_id=dr.challenge_id');
		$this->db->join('challenge_category c','c.category_id=dr.category_id');
		$this->db->where('d.user_id',$user_id);
		$this->db->where('d.challenge_status',1);
		$this->db->where('d.challenge_activity_status','3');
		$this->db->or_where('d.challenge_activity_status','2');
	    $query = $this->db->get();
		
		return $query->num_rows();
	
	}
	
	
	
		/*
	Function name :get_loss_challenge_list()
    Return array of all  loss tasks
   */
	function get_loss_challenge_list($user_id = 0 ,$limit =0 ,$offset = 0)
	{

	
		$this->db->select('d.*,c.category_name');
	    $this->db->from('challenge d');
	    $this->db->join('challenge_bid cb','d.challenge_id=cb.challenge_id');
		$this->db->join('challenge_category_rel dr','d.challenge_id=dr.challenge_id');
		$this->db->join('challenge_category c','c.category_id=dr.category_id');
		$this->db->where('cb.user_id',$user_id);
		$this->db->where('cb.is_won','0');
		$this->db->where('d.challenge_status',1);
		$this->db->where('d.challenge_activity_status >',0);
	    $this->db->order_by('d.challenge_id','desc');
		$this->db->limit($limit,$offset);
		
		
		$query = $this->db->get();
		
		if($query->num_rows > 0) {
			return $query->result();
		}
		 return 0;
	}
	
	/*
	Function name :get_total_loss_challenge_count()
	Return array of all  loss tasks count
	*/
	function get_total_loss_challenge_count($user_id = 0)
	{

	    $this->db->from('challenge d');
	    $this->db->join('challenge_bid cb','d.challenge_id=cb.challenge_id');
		$this->db->join('challenge_category_rel dr','d.challenge_id=dr.challenge_id');
		$this->db->join('challenge_category c','c.category_id=dr.category_id');
		$this->db->where('cb.user_id',$user_id);
		$this->db->where('cb.is_won','0');
		$this->db->where('d.challenge_status',1);
		$this->db->where('d.challenge_activity_status >',0);
	    $query = $this->db->get();
		
		return $query->num_rows();
	
	}
	
	

	function chk_track_view($challenge_id,$user_id)
	{
		$cur_date = date('Y-m-d');
		
		$query = $this->db->query("select * from ".$this->db->dbprefix('challenge_view')." where challenge_id = ".$challenge_id." and user_id = ".$user_id." and DATE_FORMAT(view_date,'%Y-%m-%d') = '".$cur_date."'");

		if($query->num_rows()>0)
		{
			 return 1;
		}
		else
		{
			return 0;
		}
	}
	
	function chk_track_other_view($challenge_id)
	{
		$cur_date = date('Y-m-d');
		
		$query = $this->db->query("select * from ".$this->db->dbprefix('challenge_view')." where challenge_id = ".$challenge_id." and view_ip = '".getRealIP()."' and DATE_FORMAT(view_date,'%Y-%m-%d') = '".$cur_date."'");

		if($query->num_rows()>0)
		{
			 return 1;
		}
		else
		{
			return 0;
		}
	}

	function set_challenge_view($challenge_id)
	{
		
		
		$user_id=0;
		$chk_track_view  = 1;
		if(get_authenticateUserID() > 0)
		{
			$user_id = get_authenticateUserID();
		}
		if($user_id > 0 )
		{
			$chk_track_view = $this->chk_track_view($challenge_id,$user_id);
		}
		if($chk_track_view == 0)
		{
			
			$sql_res = "update ".$this->db->dbprefix('challenge')." set `challenge_view_count` = `challenge_view_count` + 1 where challenge_id =".$challenge_id;
			$this->db->query($sql_res);
			
			$data = array('challenge_id'=>$challenge_id,
						  'user_id'=>$user_id,
						  'view_date'=>date('Y-m-d h:i:s'),	
						  'view_ip'=>getRealIP()
						);
			$this->db->insert('challenge_view',$data);
		}
		else
		{
			$chk_track_other_view = $this->chk_track_other_view($challenge_id);
		
			if($chk_track_other_view == 0)
			{
			$sql_res = "update ".$this->db->dbprefix('challenge')." set `challenge_view_count` = `challenge_view_count` + 1 where challenge_id =".$challenge_id;
			$this->db->query($sql_res);
			
			$data = array('challenge_id'=>$challenge_id,
						  'user_id'=>0,
						  'view_date'=>date('Y-m-d h:i:s'),	
						  'view_ip'=>getRealIP()
						);
			$this->db->insert('challenge_view',$data);
			}
		}
		
	}	
	
	function check_like_track($user_id,$challenge_id)
	{

		$query = $this->db->query("select * from ".$this->db->dbprefix('challenge_like')." where challenge_id = ".$challenge_id." and user_id = ".$user_id);

		if($query->num_rows()>0)
		{
			 return $query->row();
		}
		else
		{
			return 0;
		}
	}
	
	function check_like_status($like_status,$user_id,$challenge_id)
	{
		$query = $this->db->query("select * from ".$this->db->dbprefix('challenge_like')." where challenge_id = ".$challenge_id." and like_status = ".$like_status." and user_id = ".$user_id);

		if($query->num_rows()>0)
		{
			 return $query->row();
		}
		else
		{
			return 0;
		}
	}
	
	function set_like_dislike($like_status,$challenge_id)
	{
		
		//$chk_track_like  = 1;
		//like
		$chk_like = $this->check_like_track(get_authenticateUserID(),$challenge_id);
		
		if($chk_like)
		{
			 $data = array('challenge_id'=>$challenge_id,
						  'user_id'=>get_authenticateUserID(),
						  'like_date'=>date('Y-m-d h:i:s'),	
						  'like_status'=>$like_status,
						  'like_ip'=>getRealIP()
						);
				$this->db->where(array('user_id'=>get_authenticateUserID(),'challenge_id'=>$challenge_id));		
				$this->db->update('challenge_like',$data);
				
				
			 if($like_status == 1)
			 {
				  $sql_res = "update ".$this->db->dbprefix('challenge')." set `challenge_like_count` = `challenge_like_count` + 1 where challenge_id =".$challenge_id;
			 }
			 else
			 {
				   $sql_res = "update ".$this->db->dbprefix('challenge')." set `challenge_like_count` = `challenge_like_count` - 1 where challenge_id =".$challenge_id;
			 }  
			 
			 $this->db->query($sql_res);	
		}
		
		else
		{
			
		     $data = array('challenge_id'=>$challenge_id,
						  'user_id'=>get_authenticateUserID(),
						  'like_date'=>date('Y-m-d h:i:s'),	
						  'like_status'=>$like_status,
						  'like_ip'=>getRealIP()
						);
			$this->db->insert('challenge_like',$data);
			$sql_res = "update ".$this->db->dbprefix('challenge')." set `challenge_like_count` = `challenge_like_count` + 1 where challenge_id =".$challenge_id;
			$this->db->query($sql_res);	
		}
		
		
	}

   
   function get_total_bid_count($challenge_id = 0)
   {
   	    $this->db->select("cb.challenge_bid_id");
	    $this->db->from('challenge_bid cb');
	    $this->db->join('challenge d','cb.challenge_id=d.challenge_id');
	    $this->db->join('user u','cb.user_id=u.user_id');
		$this->db->where('cb.challenge_id',$challenge_id);
	    $this->db->where('d.challenge_status',1);
	
	    $query = $this->db->get();
		
		return $query->num_rows();
	
   }


   function get_bid_list($challenge_id = 0 , $limit = 0 , $offset = 0)
	{
	      $this->db->select("*");
	    $this->db->from('challenge_bid cb');
	    $this->db->join('challenge d','cb.challenge_id=d.challenge_id');
	    $this->db->join('user u','cb.user_id=u.user_id');
		$this->db->where('cb.challenge_id',$challenge_id);
	    $this->db->where('d.challenge_status',1);
	    $this->db->order_by('cb.challenge_bid_id','desc');
		$this->db->limit($limit,$offset);
		
		
		$query = $this->db->get();
		
		if($query->num_rows > 0) {
			return $query->result();
		}
		 return 0;
	}
	
	
	function place_bid($challenge_id,$amount,$comment)
   {
	$data =  array('challenge_id'=>$challenge_id,
				   'user_id'=>get_authenticateUserID(),
				   'bid_date'=>date('Y-m-d h:i:s'),
				   'bid_ip'=>getRealIP(),
				   'bid_description'=>$comment,
				   'bid_price'=>$amount
				   );
	$this->db->insert('challenge_bid',$data);
	return 1;
   }
   
   
   function get_conversation_list( $id = 0 , $uid = 0)
   {
   	
       
	   $qry = $this->db->query("select cbc.* , fromuser.full_name as from_name ,fromuser.email as from_email ,touser.full_name as to_name ,touser.email as to_email 
	                             from ".$this->db->dbprefix('challenge_bid_conversation')." cbc
	                             LEFT join ".$this->db->dbprefix('user')." touser on cbc.conversation_to_user_id = touser.user_id 
	                             LEFT join ".$this->db->dbprefix('user')." fromuser on cbc.conversation_from_user_id = fromuser.user_id 
	                             where challenge_bid_id = '".$id."' order by 
	                             bid_conversation_id ASC 
	                             ");
								 
	   if($qry->num_rows()>0)
	   {
	   	  return $qry->result();
	   }			
	   
	   return 0;				 
   	  
   }
	
	
	function get_bid_details($challenge_bid_id = 0)
	{
		$qry = $this->db->get_where("challenge_bid",array("challenge_bid_id"=>$challenge_bid_id));
		
		if($qry->num_rows()>0)
		{
			 return $qry->row_array();
		}
		
		return 0;
	}
	
	
	function inseret_comment($data_insert = array())
	{
		 $data_insert["conversation_ip"] = $_SERVER['REMOTE_ADDR'];
		 $data_insert["conversation_date"] = date("Y-m-d H:i:s");
		 $this->db->insert("challenge_bid_conversation",$data_insert);
		 
		 return "success";
	}
	
	
	
	function complete_challenge($data_review = array(),$challenge_bid_id =0)
	{
		  $bid_detail = $this->get_bid_details($challenge_bid_id);
		   $challenge_details = $this->get_one_challenge($bid_detail["challenge_id"]);
		
		
		  unset($data_review["submit"]);
		  //insert inti user review///
		  $data_review["challenge_id"] = $bid_detail["challenge_id"];
		  $data_review["review_to_user_id"] = $bid_detail["user_id"];
		  $data_review["review_by_user_id"] = get_authenticateUserID();
		  $data_review["user_review_date"] = date("Y-m-d H:i:s");
		  
		  
		  $this->db->insert("user_review",$data_review);
		  //end of insert into user review///
		  
		  
		  ///update challenge///
		  $data_challenge = array("challenge_complete_date"=>date("Y-m-d H:i:s"),"challenge_activity_status"=>"2");
		  $this->db->where("challenge_id",$bid_detail["challenge_id"]);
		  $this->db->update("challenge",$data_challenge);
		  /// end//	
		  
		  ///update bid///
		  $data_bid = array("is_complete_by_owner"=>"1","is_complete_by_bidder"=>"1");
		  $this->db->where("challenge_bid_id",$challenge_bid_id);
		  $this->db->update("challenge_bid",$data_bid);
		  /// end//	
		  
		  ///get parent transaction id///
		  $parent_transaction_id = get_parent_transaction_challenge($bid_detail["challenge_id"]);
		  //// end///
		  
		  
		  	//// insert entry into transaction table/////
		$data_trans  = array("user_id"=>get_authenticateUserID(),
		                     "challenge_id"=>$bid_detail["challenge_id"],
							 "pay_points"=>$bid_detail["bid_price"],
							 "host_ip"=>$_SERVER['REMOTE_ADDR'],
							 "parent_transaction_id"=>$parent_transaction_id,
							 "transaction_date_time"=>date("Y-m-d H:i:s"));
		$this->db->insert("transaction",$data_trans);	
		
		
		
	   // end of insert entry in transaction table..//
	   
	   ////// update wallate////
	   $this->db->query("update  ".$this->db->dbprefix('wallet')." set is_authorize = '2' where challenge_id = '".$bid_detail["challenge_id"]."' 
	                   and transaction_id = '".$parent_transaction_id."'");
	   ///end ///
	   
	   
	   /////admin commission//
	   $admin_commission = commission_setting();
	   $admin_challenge_commission = $admin_commission->admin_commission_challange;
	   
	   $admin_points = $bid_detail["bid_price"] / $admin_challenge_commission;
	   $user_points = $bid_detail["bid_price"] - $admin_points;
	   ///end ///
	   
	   ///insert into transaction///
	     
		$data_trans_user  = array("user_id"=>$bid_detail["user_id"],
		                     "challenge_id"=>$bid_detail["challenge_id"],
							 "pay_points"=>$user_points,
							 "host_ip"=>$_SERVER['REMOTE_ADDR'],
							 "transaction_date_time"=>date("Y-m-d H:i:s"));
		$this->db->insert("transaction",$data_trans_user);
		
		$user_trans_id = mysql_insert_id();	
	   ///end of insert into transaction///
	   
	   ///insert into wallet///
	   $wallet_user = array("credit"=>$user_points,
		                     "user_id"=>$bid_detail["user_id"],
		                     "challenge_id"=>$bid_detail["challenge_id"],
		                     "total_user_price" =>$bid_detail["bid_price"],
		                     "total_cut_price"=>$admin_points,
		                     "wallet_date"=>date("Y-m-d H:i:s"),
		                     "transaction_id"=>$user_trans_id,
		                     "wallet_ip"=>$_SERVER['REMOTE_ADDR'],
							 );
			$this->db->insert("wallet",$wallet_user);	
			
			$wallet_transaction_id = mysql_insert_id();
	   /// end of insert into wallet//
	   
	   
	    ///insert into admin wallet///
	   $wallet_admin = array("credit"=>$admin_points,
		                     "wallet_transaction_id"=>$wallet_transaction_id,
		                     "wallet_date"=>date("Y-m-d H:i:s"),
		                     "user_id"=>$bid_detail["user_id"],
		                      "challenge_id"=>$bid_detail["challenge_id"],
		                     "transaction_id"=>$user_trans_id,
		                     "wallet_ip"=>$_SERVER['REMOTE_ADDR'],
							 );
	  $this->db->insert("wallet_admin",$wallet_admin);	
	   /// end of insert into admin wallet//
	}
}
?>