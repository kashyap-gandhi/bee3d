<?php
/*********************************************************************************
 * This the taskrabbitclone.com  by Rockers Technology. is paid software. It is released under the terms of 
 * the following BSD License.
 * 
 *  Rockers Technologies (Head Office)
 *    5038,Berthpage Dr
 *    suwanee, GA. Zip Code : 30024
    
 *    E-mail Address : nishu@rockersinfo.com
 * 
 * Copyright � 2012-2020 by Rockers Technology , INC a domestic profit corporation has been duly incorporated under
the laws of the state of georiga , USA. www.rockersinfo.com
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this 
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this 
 *   list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.

 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Myoffer_model extends IWEB_Model 
{

	/*
	Function name :Myoffer_model
	Description :its default constuctor which called when user_model object initialzie.its load necesary parent constructor
	*/
	function Myoffer_model()
    {
        parent::__construct();	
    } 

	function get_all_total_trips(){
	
		$this->db->select('*');
		$this->db->from('trip');
		$this->db->join('trip_offer','trip.trip_id=trip_offer.trip_id');
		$this->db->join('user','trip.user_id=user.user_id');
		$this->db->join('user_profile','trip.user_id=user_profile.user_id','left');
		$this->db->join('currency_code','trip.currency_code_id=currency_code.currency_code_id','left');
		$this->db->where('trip_offer.user_id',get_authenticateUserID());
		$this->db->where('trip.trip_status',1);
		$query = $this->db->get();
		
		
		return $query->num_rows();
		
	}
	
	function get_all_trips_list($limit,$offset){

		$this->db->select('*');
		$this->db->from('trip');
		$this->db->join('trip_offer','trip.trip_id=trip_offer.trip_id');
		$this->db->join('user','trip.user_id=user.user_id');
		$this->db->join('user_profile','trip.user_id=user_profile.user_id','left');
		$this->db->join('currency_code','trip.currency_code_id=currency_code.currency_code_id','left');
		$this->db->where('trip_offer.user_id',get_authenticateUserID());
		$this->db->where('trip.trip_status',1);
		$this->db->order_by('trip.trip_id desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		//echo $this->db->last_query();
		if($query->num_rows > 0) {
			return $query->result();
		}
		 return 0;
	}
	
	
	function get_open_total_trips(){
	
		$this->db->select('*');
		$this->db->from('trip');
		$this->db->join('trip_offer','trip.trip_id=trip_offer.trip_id');
		$this->db->join('user','trip.user_id=user.user_id');
		$this->db->join('user_profile','trip.user_id=user_profile.user_id','left');
		$this->db->join('currency_code','trip.currency_code_id=currency_code.currency_code_id','left');
		$this->db->where('trip_offer.user_id',get_authenticateUserID());
		$this->db->where('trip.trip_activity_status',0);
		$this->db->where('trip.trip_agent_assign_id =',0);
		$this->db->where('trip.trip_confirm_date >=',date('Y-m-d H:i:s'));
		$this->db->where('trip.trip_status',1);
		$query = $this->db->get();
		return $query->num_rows();
		
	}
	
	function get_open_trips_list($limit,$offset){
	
		$this->db->select('*');
		$this->db->from('trip');
		$this->db->join('trip_offer','trip.trip_id=trip_offer.trip_id');
		$this->db->join('user','trip.user_id=user.user_id');
		$this->db->join('user_profile','trip.user_id=user_profile.user_id','left');
		$this->db->join('currency_code','trip.currency_code_id=currency_code.currency_code_id','left');
		$this->db->where('trip_offer.user_id',get_authenticateUserID());
		$this->db->where('trip.trip_activity_status',0);
		$this->db->where('trip.trip_agent_assign_id =',0);
		$this->db->where('trip.trip_confirm_date >=',date('Y-m-d H:i:s'));
		$this->db->where('trip.trip_status',1);
		$this->db->order_by('trip.trip_id desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		
		if($query->num_rows > 0) {
			return $query->result();
		}
		 return 0;
	} 
	
	
	function get_assigned_total_trips(){
	
		$this->db->select('*');
		$this->db->from('trip');
		$this->db->join('trip_offer','trip.trip_id=trip_offer.trip_id');
		$this->db->join('user','trip.user_id=user.user_id');
		$this->db->join('user_profile','trip.user_id=user_profile.user_id','left');
		$this->db->join('currency_code','trip.currency_code_id=currency_code.currency_code_id','left');
		$this->db->where('trip_offer.user_id',get_authenticateUserID());
		$this->db->where('trip.trip_agent_assign_id',get_authenticateUserID());
		$this->db->where('trip.trip_activity_status',1);
		$this->db->where('trip.trip_status',1);
		$query = $this->db->get();
		return $query->num_rows();
		
	}
	
	function get_assigned_trips_list($limit,$offset){
	
		$this->db->select('*');
		$this->db->from('trip');
		$this->db->join('trip_offer','trip.trip_id=trip_offer.trip_id');
		$this->db->join('user','trip.user_id=user.user_id');
		$this->db->join('user_profile','trip.user_id=user_profile.user_id','left');
		$this->db->join('currency_code','trip.currency_code_id=currency_code.currency_code_id','left');
		$this->db->where('trip_offer.user_id',get_authenticateUserID());
		$this->db->where('trip.trip_agent_assign_id',get_authenticateUserID());
		$this->db->where('trip.trip_activity_status',1);
		$this->db->where('trip.trip_status',1);
		$this->db->order_by('trip.trip_id desc');
		$this->db->limit($limit,$offset);

		$query = $this->db->get();
		
		if($query->num_rows > 0) {
			return $query->result();
		}
		 return 0;
	}
	
	
	function get_completed_total_trips(){
	
		$this->db->select('*');
		$this->db->from('trip');
		$this->db->join('trip_offer','trip.trip_id=trip_offer.trip_id');
		$this->db->join('user','trip.user_id=user.user_id');
		$this->db->join('user_profile','trip.user_id=user_profile.user_id','left');
		$this->db->join('currency_code','trip.currency_code_id=currency_code.currency_code_id','left');
		$this->db->where('trip_offer.user_id',get_authenticateUserID());
		$this->db->where('trip.trip_agent_assign_id',get_authenticateUserID());
		$this->db->where('trip.trip_activity_status',2);
		$this->db->where('trip.trip_confirm_date <=',date('Y-m-d H:i:s'));
		$this->db->where('trip.trip_status',1);
		$query = $this->db->get();
		return $query->num_rows();
		
	}
	
	function get_completed_trips_list($limit,$offset){

		$this->db->select('*');
		$this->db->from('trip');
		$this->db->join('trip_offer','trip.trip_id=trip_offer.trip_id');
		$this->db->join('user','trip.user_id=user.user_id');
		$this->db->join('user_profile','trip.user_id=user_profile.user_id','left');
		$this->db->join('currency_code','trip.currency_code_id=currency_code.currency_code_id','left');
		$this->db->where('trip_offer.user_id',get_authenticateUserID());
		$this->db->where('trip.trip_agent_assign_id',get_authenticateUserID());
		$this->db->where('trip.trip_activity_status',2);
		$this->db->where('trip.trip_confirm_date <=',date('Y-m-d H:i:s'));
		$this->db->where('trip.trip_status',1);
		$this->db->order_by('trip.trip_id desc');
		$this->db->limit($limit,$offset);

		$query = $this->db->get();
		
		
		if($query->num_rows > 0) {
			return $query->result();
		}
		 return 0;
	}
	
	
	function get_close_total_trips(){
	
		$this->db->select('*');
		$this->db->from('trip');
		$this->db->join('trip_offer','trip.trip_id=trip_offer.trip_id');
		$this->db->join('user','trip.user_id=user.user_id');
		$this->db->join('user_profile','trip.user_id=user_profile.user_id','left');
		$this->db->join('currency_code','trip.currency_code_id=currency_code.currency_code_id','left');
		$this->db->where('trip_offer.user_id',get_authenticateUserID());
		$this->db->where('trip.trip_agent_assign_id !=',get_authenticateUserID());
		$this->db->where('trip.trip_confirm_date <=',date('Y-m-d H:i:s'));
		$this->db->where('trip.trip_status',1);
		$query = $this->db->get();
		return $query->num_rows();
		
	}
	
	function get_close_trips_list($limit,$offset){
		
		$this->db->select('*');
		$this->db->from('trip');
		$this->db->join('trip_offer','trip.trip_id=trip_offer.trip_id');
		$this->db->join('user','trip.user_id=user.user_id');
		$this->db->join('user_profile','trip.user_id=user_profile.user_id','left');
		$this->db->join('currency_code','trip.currency_code_id=currency_code.currency_code_id','left');
		$this->db->where('trip_offer.user_id',get_authenticateUserID());
		$this->db->where('trip.trip_agent_assign_id !=',get_authenticateUserID());
		$this->db->where('trip.trip_confirm_date <=',date('Y-m-d H:i:s'));
		$this->db->where('trip.trip_status',1);
		$this->db->order_by('trip.trip_id desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		
		
		
		
		
		
		
		if($query->num_rows > 0) {
			return $query->result();
		}
		 return 0;
	}
	
	
}
?>