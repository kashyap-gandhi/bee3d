<?php
class Design_model extends IWEB_Model 
{

	/*
	Function name :User_model
	Description :its default constuctor which called when user_model object initialzie.its load necesary parent constructor
	*/
	function Design_model()
    {
        parent::__construct();	
    } 
	
	function check_exist_design($design_id,$user_id)
	{
		$this->db->select('*');
		$this->db->from('design');
		$this->db->where('design_id',$design_id);
		if($user_id > 0)
		{
			//For check in view also
			$this->db->where('user_id',$user_id);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function insert_design_category()
	{
		
		$image_settings = image_setting();
		if($_FILES['category_image']['name']!='')
		{
				 $this->load->library('upload');
				 $rand=randomCode(); 
				  
				 $_FILES['userfile']['name']     =   $_FILES['category_image']['name'];
				 $_FILES['userfile']['type']     =   $_FILES['category_image']['type'];
				 $_FILES['userfile']['tmp_name'] =   $_FILES['category_image']['tmp_name'];
				 $_FILES['userfile']['error']    =   $_FILES['category_image']['error'];
				 $_FILES['userfile']['size']     =   $_FILES['category_image']['size'];
	   
				 $config['file_name'] = $rand;
				 $config['upload_path'] = base_path().'upload/category_orig/';
				 $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
		  		 $this->upload->initialize($config);
 
				  if (!$this->upload->do_upload())
				  {
					$error =  $this->upload->display_errors();   
				  } 
				   
				  $picture = $this->upload->data();		
				  $image_name=$picture['file_name'];
				 	
				$this->load->library('image_lib');					
				$this->image_lib->clear();	
			
				$config['image_library'] = 'GD2';
				$config['thumb_marker'] = "";
				$config['maintain_ratio'] = TRUE;
				$config['create_thumb'] = TRUE;
				$config['quality'] = '100%';
				$config['master_dim'] = 'width';
				$config['source_image'] = $picture['full_path'];
				$config['new_image'] = base_path().'upload/category_orig/'.$picture['file_name'];					
				$config['width'] = $image_settings->category_width;
				$config['height'] = $image_settings->category_height;
				$this->image_lib->initialize($config);
				
				if(!$this->image_lib->resize()){
					$error = $this->image_lib->display_errors();				
				}
				$this->image_lib->clear();
			}
			
			
				$data = array('category_name'=>$this->input->post('category_name'),
							  'category_url_name'=>get_category_slug($this->input->post('category_name'),0),
							  'category_parent_id'=>$this->input->post('category_parent_id'),
							  'category_description'=>$this->input->post('category_description'),
							  'category_image'=>$picture['file_name'],
							  'category_status'=>$this->input->post('category_status')
							  );
				
				$this->db->insert('design_category',$data);
	}
	
	function insert_design()
	{
		
		$new_design_title=BadWords($this->input->post('design_title'));
		$design_title= $new_design_title['str'];
		
		$new_design_content=BadWords($this->input->post('design_content'));
		$design_content= $new_design_content['str'];
		
		$new_design_meta_keyword = BadWords($this->input->post('design_meta_keyword'));
		$design_meta_keyword= $new_design_meta_keyword['str'];
		
		$new_design_meta_description= BadWords($this->input->post('design_meta_description'));
		$design_meta_description= $new_design_meta_description['str'];

		$design_slug = get_design_slug($this->input->post('design_title'),0);
		$data = array(	'design_title'=>$design_title,
						'design_is_modified'=>$this->input->post('design_is_modified'),
					   'design_slug'=>$design_slug,
					   'design_content'=>$design_content,
					   'design_point'=>$this->input->post('design_point'),
					  'design_ref_id'=>$this->input->post('design_ref_id'),
					  'design_by'=>$this->input->post('design_by'),
					  'parent_design_id'=>$this->input->post('parent_design_id'),
					  'design_meta_keyword'=>$design_meta_keyword,
					  'design_meta_description'=>$design_meta_description,
					  'user_id'=>get_authenticateUserID(),
					  'design_date'=>date('Y-m-d h:i:s'),
					  'design_ip'=>$_SERVER['REMOTE_ADDR'],
					  'design_status'=>1,
					  'design_gallery_type'=>$this->input->post('design_gallery_type')
					  );
				
				$this->db->insert('design',$data);
				$design_id = mysql_insert_id();
				
				
		//Update slug
		$data_slug = array('design_slug',$design_slug.'-'.$design_id);
		$this->db->where('design_id',$design_id);
		$this->db->update('design',$data);		
		
		/*Insert data in design_category_rel*/
		$data_design_category_rel= array('category_id'=>$this->input->post('design_category'),
										 'design_id'=>$design_id);
		$this->db->insert('design_category_rel',$data_design_category_rel);
		
		
		$image_settings = image_setting();
		$this->load->library('upload');
		 if($_FILES['attachment_name']['name']!='')
		 {
			 for($i=0;$i<count($_FILES['attachment_name']['name']);$i++)
			 {
						if($_FILES['attachment_name']['name'][$i]!='')
						{
							
						$rand=rand(0,100000);
						
						$_FILES['userfile']['name']    =   $_FILES['attachment_name']['name'][$i];
						$_FILES['userfile']['type']    =   $_FILES['attachment_name']['type'][$i];
						$_FILES['userfile']['tmp_name'] =   $_FILES['attachment_name']['tmp_name'][$i];
						$_FILES['userfile']['error']       =   $_FILES['attachment_name']['error'][$i];
						$_FILES['userfile']['size']    =   $_FILES['attachment_name']['size'][$i]; 
						  
						
						$config['file_name']     = $rand.'design_'.$design_id.'_'.$i;
						$config['upload_path'] = base_path().'upload/design_orig/';					
						//$config['allowed_types'] = 'doc|docx|jpg|jpeg|gif|png|bmp';
						$config['allowed_types'] = '*';
						
					   $this->upload->initialize($config);
						if (!$this->upload->do_upload())
						{		
							 $error =  $this->upload->display_errors();
						} 
			
			
			$picture = $this->upload->data();
            $this->load->library('image_lib');
            $this->image_lib->clear();
            $gd_var='gd2';
            
            if ($_FILES['attachment_name']['type'][$i]!= "image/png" and $_FILES['attachment_name']['type'][$i] != "image/x-png") {
            $gd_var='gd2';
            }
            
            if ($_FILES['attachment_name']['type'][$i] != "image/gif") {
            $gd_var='gd2';
            }
            
            if ($_FILES['attachment_name']['type'][$i] != "image/jpeg" and $_FILES['attachment_name']['type'][$i] != "image/pjpeg" ) {
            $gd_var='gd2';
            }
			
			$this->image_lib->initialize(array(
			'image_library' => $gd_var,
			'source_image' => base_path().'upload/design_orig/'.$picture['file_name'],
			'new_image' => base_path().'upload/design/'.$picture['file_name'],
			'maintain_ratio' => FALSE,
			'quality' => '100%',
			'width' => $image_settings->design_width,
			'height' => $image_settings->design_height
			));
			if(!$this->image_lib->resize())
            {
            $error = $this->image_lib->display_errors();
            }
			$this->image_lib->clear();
			
			$image=$picture['file_name'];
			
								
					if($i==0)
					{
						$attachment_is_main = 1;
					}		
					else
					{
						$attachment_is_main = 0;
					}
							$data_gallery=array(
							'design_id'=>$design_id,
							'attachment_is_main'=>$attachment_is_main,
							'attachment_name'=>$image,
							'attachment_type'=>1,
							'attachment_order'=>$i,
							'date_added' => date('Y-m-d H:i:s')
							);
							$this->db->insert('design_attachment',$data_gallery);
						
						}
			}
		}	
		
		/*Insert Tags*/
		$tags = explode(",",$this->input->post('tag_name'));
		if($tags)
		{
			for($i=0;$i<count($tags);$i++)
			 {
				$tag_id = check_tag($tags[$i]);
				$data_tag = array('design_id'=>$design_id,
								'tag_id'=>$tag_id,
								'tag_date'=>date('Y-m-d h:i:s'),
								'tag_ip'=>$_SERVER['REMOTE_ADDR']);
				$this->db->insert('design_tags',$data_tag);				
			 }
			 
			 //design_tags
		}
		
		
		
	}
	
	function update_design()
	{
		$design_slug = get_design_slug($this->input->post('design_title'),0);
		$design_id = $this->input->post('design_id');
		$design_slug = $design_slug.'-'.$design_id;
		
		
		$new_design_title=BadWords($this->input->post('design_title'));
		$design_title= $new_design_title['str'];
		
		$new_design_content=BadWords($this->input->post('design_content'));
		$design_content= $new_design_content['str'];
		
		$new_design_meta_keyword = BadWords($this->input->post('design_meta_keyword'));
		$design_meta_keyword= $new_design_meta_keyword['str'];
		
		$new_design_meta_description= BadWords($this->input->post('design_meta_description'));
		$design_meta_description= $new_design_meta_description['str'];
		
		$data = array(	'design_title'=>$design_title,
						'design_is_modified'=>$this->input->post('design_is_modified'),
					   'design_slug'=>$design_slug,
					   'design_content'=>$design_content,
					   'design_point'=>$this->input->post('design_point'),
					  'design_ref_id'=>$this->input->post('design_ref_id'),
					  'design_by'=>$this->input->post('design_by'),
					  'parent_design_id'=>$this->input->post('parent_design_id'),
					  'design_meta_keyword'=>$design_meta_keyword,
					  'design_meta_description'=>$design_meta_description,
					  'design_ip'=>$_SERVER['REMOTE_ADDR'],
					  'design_status'=>1,
					  'design_gallery_type'=>$this->input->post('design_gallery_type')
					  );
		$this->db->where('design_id',$design_id);
		$this->db->update('design',$data);
		
		/*Insert data in design_category_rel*/
		$data_design_category_rel= array('category_id'=>$this->input->post('design_category'),
										 'design_id'=>$design_id);
		$this->db->where('design_id',$design_id);
		$this->db->update('design_category_rel',$data_design_category_rel);
		
		$image_settings = image_setting();
		$this->load->library('upload');
		 if($_FILES['attachment_name']['name']!='')
		 {
			 for($i=0;$i<count($_FILES['attachment_name']['name']);$i++)
			 {
						if($_FILES['attachment_name']['name'][$i]!='')
						{
							
						$rand=rand(0,100000);
						
						$_FILES['userfile']['name']    =   $_FILES['attachment_name']['name'][$i];
						$_FILES['userfile']['type']    =   $_FILES['attachment_name']['type'][$i];
						$_FILES['userfile']['tmp_name'] =   $_FILES['attachment_name']['tmp_name'][$i];
						$_FILES['userfile']['error']       =   $_FILES['attachment_name']['error'][$i];
						$_FILES['userfile']['size']    =   $_FILES['attachment_name']['size'][$i]; 
						  
						
						$config['file_name']     = $rand.'design_'.$design_id.'_'.$i;
						$config['upload_path'] = base_path().'upload/design_orig/';					
						//$config['allowed_types'] = 'doc|docx|jpg|jpeg|gif|png|bmp';
						$config['allowed_types'] = '*';
						
					   $this->upload->initialize($config);
						if (!$this->upload->do_upload())
						{		
							 $error =  $this->upload->display_errors();
						} 
			
			
			$picture = $this->upload->data();
            $this->load->library('image_lib');
            $this->image_lib->clear();
            $gd_var='gd2';
            
            if ($_FILES['attachment_name']['type'][$i]!= "image/png" and $_FILES['attachment_name']['type'][$i] != "image/x-png") {
            $gd_var='gd2';
            }
            
            if ($_FILES['attachment_name']['type'][$i] != "image/gif") {
            $gd_var='gd2';
            }
            
            if ($_FILES['attachment_name']['type'][$i] != "image/jpeg" and $_FILES['attachment_name']['type'][$i] != "image/pjpeg" ) {
            $gd_var='gd2';
            }
			
			$this->image_lib->initialize(array(
			'image_library' => $gd_var,
			'source_image' => base_path().'upload/design_orig/'.$picture['file_name'],
			'new_image' => base_path().'upload/design/'.$picture['file_name'],
			'maintain_ratio' => FALSE,
			'quality' => '100%',
			'width' => $image_settings->design_width,
			'height' => $image_settings->design_height
			));
			if(!$this->image_lib->resize())
            {
            $error = $this->image_lib->display_errors();
            }
			$this->image_lib->clear();
			
			$image=$picture['file_name'];
			
								
					if($i==1)
					{
						$attachment_is_main = 1;
					}		
					else
					{
						$attachment_is_main = 0;
					}
							$data_gallery=array(
							'design_id'=>$design_id,
							'attachment_is_main'=>$attachment_is_main,
							'attachment_name'=>$image,
							'attachment_type'=>1,
							'attachment_order'=>$i,
							'date_added' => date('Y-m-d H:i:s')
							);
							$this->db->insert('design_attachment',$data_gallery);
						
						}
			}
		}	
		
		/*Insert Tags*/
		//Delete design tag
		$delete_tag=$this->db->query("delete from ".$this->db->dbprefix('design_tags')." where design_id='".$design_id."'");
		
		$tags = explode(",",$this->input->post('tag_name'));
		if($tags)
		{
			for($i=0;$i<count($tags);$i++)
			 {
				$tag_id = check_tag($tags[$i]);
				$data_tag = array('design_id'=>$design_id,
								'tag_id'=>$tag_id,
								'tag_date'=>date('Y-m-d h:i:s'),
								'tag_ip'=>$_SERVER['REMOTE_ADDR']);
				$this->db->insert('design_tags',$data_tag);				
			 }
			 
			 //design_tags
		}
	
		
	}
	
	function get_popular_design($limit,$design_id = 0)
	{
		$this->db->select('d.*,c.category_name');
		$this->db->from('design d');
		$this->db->where('d.design_status','1');
		$this->db->join('design_category_rel dr','d.design_id=dr.design_id');
		$this->db->join('design_category c','c.category_id=dr.category_id');
		$this->db->where('d.design_id !=',$design_id);
		$this->db->order_by('design_like_count','desc');
		$this->db->limit($limit);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	function get_user_design($user_id,$limit,$design_id = 0)
	{
	
		$this->db->select('d.*,c.category_name');
		$this->db->from('design d');
		$this->db->where('d.design_status','1');
		$this->db->join('design_category_rel dr','d.design_id=dr.design_id');
		$this->db->join('design_category c','c.category_id=dr.category_id');
		
		$this->db->where('d.user_id',$user_id);
		$this->db->where('d.design_id !=',$design_id);
		$this->db->order_by('d.design_id','desc');
		//$this->db->order_by('design_like_count','desc');
		$this->db->limit($limit);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	function get_user_design_gallery($user_id,$design_id)
	{
		$this->db->select('d.*,c.category_name,at.attachment_name');
		$this->db->from('design d');
		
		$this->db->join('design_category_rel dr','d.design_id=dr.design_id',"LEFT");
		$this->db->join('design_category c','c.category_id=dr.category_id',"LEFT");
		$this->db->join('design_attachment at','at.design_id=d.design_id',"LEFT");
		 /*if($user_id > 0)
         {
               //For home page gallery
              $this->db->where('d.user_id',$user_id);
         }*/
         $this->db->where('d.design_status','1');
		 $this->db->where("at.attachment_is_main",1);
		$this->db->where('d.design_id !=',$design_id);
		$this->db->order_by('d.design_id','desc');
		//$this->db->order_by('design_like_count','desc');
	//	$this->db->limit($limit);
		$query = $this->db->get();
		
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	function get_toala_user_design_count($user_id)
	{
		$this->db->select('d.*,c.category_name');
		$this->db->from('design d');
		$this->db->join('design_category_rel dr','d.design_id=dr.design_id');
		$this->db->join('design_category c','c.category_id=dr.category_id','LEFT');
		$this->db->where('d.user_id',$user_id);
		$this->db->order_by('design_id','desc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_design_result($user_id,$limit,$offset)
	{

		$this->db->select('d.*,c.category_name');
		$this->db->from('design d');
		$this->db->join('design_category_rel dr','d.design_id=dr.design_id');
		$this->db->join('design_category c','c.category_id=dr.category_id','LEFT');
		$this->db->where('d.user_id',$user_id);
		$this->db->order_by('design_id','desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
	function get_one_design($design_id)
	{
		$this->db->select('d.*,dr.category_id');
		$this->db->from('design d');
		$this->db->join('design_category_rel dr','d.design_id=dr.design_id');
		//$this->db->join('design_category c','c.category_id=dr.category_id');
		$this->db->where('d.design_id',$design_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else
		{
			return 0;
		}
	}	
	
	function get_one_design_tag($design_id)
	{
		$this->db->select('tg.design_tags_id,t.tag_name,tg.tag_id');
		$this->db->from('design_tags tg');
		$this->db->join('tags t','t.tag_id=tg.tag_id');
		$this->db->where('tg.design_id',$design_id);
		$this->db->group_by('tg.tag_id');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
		//$query=$this->db->get_where('design_tags',array('design_id'=>$design_id));
		return $query->result();	
		}
	}
	
	function get_one_design_images($design_id)
	{
		$this->db->select('*');
		$this->db->from('design_attachment');
		$this->db->where('design_id',$design_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
		//$query=$this->db->get_where('design_tags',array('design_id'=>$design_id));
		return $query->result();	
		}
	}
	
	function get_one_design_attachment($attachment_id)
	{
		$this->db->select('*');
		$this->db->from('design_attachment');
		$this->db->where('attachment_id',$attachment_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
		//$query=$this->db->get_where('design_tags',array('design_id'=>$design_id));
		return $query->row();	
		}
	}
	
	function get_one_main_design_image($design_id)
	{
		$this->db->select('*');
		$this->db->from('design_attachment');
		$this->db->where('attachment_is_main',1);
		$this->db->where('design_id',$design_id);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
		//$query=$this->db->get_where('design_tags',array('design_id'=>$design_id));
		$res =  $query->row();	
		return $res->attachment_name;
		}
	}
	
	function get_total_design_count()
	{
		$this->db->select('d.*,c.category_name');
		$this->db->from('design d');
		$this->db->join('design_category_rel dr','d.design_id=dr.design_id');
		$this->db->join('design_category c','c.category_id=dr.category_id','LEFT');
		$this->db->order_by('design_id','desc');
		//$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}	
	
	function get_all_design_result($limit,$offset)
	{
		$this->db->select('d.*,c.category_name');
		$this->db->from('design d');
		$this->db->join('design_category_rel dr','d.design_id=dr.design_id');
		$this->db->join('design_category c','c.category_id=dr.category_id','LEFT');
		$this->db->order_by('design_id','desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	function get_total_category_design_count($category_id,$keyword)
	{
		$this->db->select('d.*,c.category_name');
		$this->db->from('design d');
		$this->db->join('design_category_rel dr','d.design_id=dr.design_id');
		$this->db->join('design_category c','c.category_id=dr.category_id','LEFT');
		$this->db->join('design_tags dt','dt.design_id=d.design_id','LEFT');
		$this->db->join('tags tg','tg.tag_id=dt.tag_id','LEFT');
		
		$this->db->order_by('design_id','desc');
		if($category_id > 0)
		{
			 $cond = "dr.category_id in ('".str_replace("-",",",$category_id)."')";
			  
			  $this->db->where($cond);
		}
		if($keyword !='' && $keyword != "0")
		{
			$this->db->like('d.design_title',$keyword);
			
			if(substr_count($keyword,' ')>=1)
			{
				$ex=explode(' ',$keyword);
				
				foreach($ex as $val)
				{
					$this->db->like('d.design_title',$val);
				}	
			}

			$this->db->or_like('tg.tag_name',$keyword);
		}
		//$this->db->limit($limit,$offset);
		$this->db->group_by('d.design_id');
		$query = $this->db->get();
		//echo $this->db->last_query();
		//die;
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_category_design_result($limit,$offset,$category_id,$keyword)
	{
		$this->db->select('d.*,c.category_name');
		$this->db->from('design d');
		$this->db->join('design_category_rel dr','d.design_id=dr.design_id');
		$this->db->join('design_category c','c.category_id=dr.category_id','LEFT');
		$this->db->join('design_tags dt','dt.design_id=d.design_id','LEFT');
		$this->db->join('tags tg','tg.tag_id=dt.tag_id','LEFT');
		$this->db->order_by('design_id','desc');
		if($category_id > 0)
		{
			
			 $cond = "dr.category_id in ('".str_replace("-",",",$category_id)."')";
			  
			  $this->db->where($cond);
		}
		
		
		if($keyword !=''  && $keyword != "0")
		{
			
			$this->db->like('d.design_title',$keyword);
			
			if(substr_count($keyword,' ')>=1)
			{
				$ex=explode(' ',$keyword);
				
				foreach($ex as $val)
				{
					$this->db->like('d.design_title',$val);
				}	
			}
			$this->db->or_like('tg.tag_name',$keyword);	

		}
		$this->db->group_by('d.design_id');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
		
	}
	
	
	
	
	
function design_buy_process($design_id){


		$cur_date=date('Y-m-d H:i:s');	
		$user_ip=getRealIP();  ///===use custom helper function  getrealIP
		
		//$picture_sql=$this->db->query(("SELECT * FROM ".$this->db->dbprefix('iweb_design')." where design_id=".$design_id);
		
		///===Notification
		$data_notification = array(	'to_user_id'=>get_authenticateUserID(),
		'design_id'=>$design_id,
		'act'=>'DESIGN_DOWNLOAD',
		'is_read'=>1,
		'notification_date'=>$cur_date
		);
		$this->db->insert('system_notification',$data_notification);
		
		$this->db->select('*');
		$this->db->from($this->db->dbprefix('design'));
		$this->db->where('design_id',$design_id);
		$picture_sql = $this->db->get();
		
		if ($picture_sql->num_rows() > 0) {
		$picture_detail =  $picture_sql->row();
		
		//echo "image points <br /><br />";
		
		$image_points=$picture_detail->design_point;
		$picture_owner_id=$picture_detail->user_id;
		
		//		echo "<br /><br />";
		
		$cur_login_user_id=get_authenticateUserID(); ///==replace with session user id
		
		
		//==check balance of user
		$user_cur_balance=getuserpoints($cur_login_user_id);
		//echo "cure bal".$user_cur_balance."imag".$image_points;die;
		if($user_cur_balance>=$image_points){
		
		///===buyer user debit from wallet
		$data_trans = array('user_id'=>$cur_login_user_id,
		'design_id'=>$design_id,
		'pay_points'=>$image_points,
		'host_ip'=>$user_ip,
		'transaction_date_time'=>$cur_date
		);
		$this->db->insert('transaction',$data_trans);
		
		//$do_transaction=mysql_query("insert into iweb_transaction(user_id,design_id,pay_points,host_ip,transaction_date_time)values('".$cur_login_user_id."','".$design_id."','".$image_points."','".$user_ip."','".$cur_date."')");
		
		$transaction_id=mysql_insert_id();
		
		//$do_wallet_transaction=mysql_query("insert into iweb_wallet(user_id,design_id,debit,transaction_id,wallet_ip,wallet_date)values('".$cur_login_user_id."','".$design_id."','".$image_points."','".$transaction_id."','".$user_ip."','".$cur_date."')");
		
		$data_wallet= array('user_id'=>$cur_login_user_id,
		'design_id'=>$design_id,
		'debit'=>$image_points,
		'transaction_id'=>$transaction_id,
		'wallet_ip'=>$user_ip,
		'wallet_date'=>$cur_date
		);
		$this->db->insert('wallet',$data_wallet);
		///===end buying
		//====get user tree for recursion payment
		$files_user_ids=array();
		
		if($image_points>0) {	
		if($picture_detail->parent_design_id > 0 && $picture_detail->design_is_modified == 1) {
		
		$files_user_ids=get_all_picture_tree($picture_detail->parent_design_id);
		
		}
		}
		
		//echo "files_user_ids <br /><br />";
		//print_r($files_user_ids); //die;
		///===add current design user id
		$all_ids[]=$picture_owner_id;
		$final_ids=array_merge($all_ids,$files_user_ids);
		///===total users id===
		//echo "<br /><br />total users ids<br /><br />";
		//print_r($final_ids);
		///====commission settings
		$admin_design_point_commission=20; ///in %
		$commission_setting=commission_setting();
		
		if(!empty($commission_setting)){
		$admin_design_point_commission=$commission_setting->admin_commission_image;
		}
		
		$user_total_point_commission=100-$admin_design_point_commission;
		
		
		///=====assign and split percentage to user
		//echo "total pay to users<br /><br />";
		$pay_to_user=splitampunt($user_total_point_commission,$final_ids);	
		//print_r($pay_to_user);
		////====total pay to individual users=
		//echo "total pay to all individual users not admin<br /><br />";
		$final_pay_to_user=$pay_to_user;
		//print_r($final_pay_to_user);  ///die;
		//====count admin points===
		$admin_points=countpoints($image_points,$admin_design_point_commission);
		
		foreach($final_pay_to_user as $user_id=>$user_perc){
		
		$user_image_points=countpoints($image_points,$user_perc);
		
		
		//====insert into db=====
		
		//$check_user_exit=mysql_query("select * from iweb_user where user_id=".$user_id);
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('user_id',$user_id);
		$check_user_exit = $this->db->get();
		
		if($check_user_exit->num_rows() > 0){
		
		//$do_transaction=mysql_query("insert into iweb_transaction(parent_transaction_id,user_id,design_id,pay_points,host_ip,transaction_date_time)values('".$transaction_id."','".$user_id."','".$design_id."','".$user_image_points."','".$user_ip."','".$cur_date."')");
		$do_transaction= array('parent_transaction_id'=>$transaction_id,
		'user_id'=>$user_id,
		'design_id'=>$design_id,
		'pay_points'=>$user_image_points,
		'host_ip'=>$user_ip,
		'transaction_date_time'=>$cur_date
		);
		$this->db->insert('transaction',$do_transaction);
		
		$last_transaction_id=mysql_insert_id();
		
		//$do_wallet_transaction=mysql_query("insert into iweb_wallet(user_id,design_id,credit,transaction_id,wallet_ip,wallet_date)values('".$user_id."','".$design_id."','".$user_image_points."','".$last_transaction_id."','".$user_ip."','".$cur_date."')");
		
		$do_wallet_transaction= array('user_id'=>$user_id,
		'design_id'=>$design_id,
		'credit'=>$user_image_points,
		'transaction_id'=>$last_transaction_id,
		'wallet_ip'=>$user_ip,
		'wallet_date'=>$cur_date
		);
		$this->db->insert('wallet',$do_wallet_transaction);
		
		} else {	
		///==if user not found than transaction points to admin		
		$user_id=$admin_id;
		$admin_points=$admin_points+$user_image_points;
		}	
		} //===foreach users
		
		
		
		///===pay to admin wallet====
		if($admin_points!=''){				
		$is_point=1;
		
		//mysql_query("insert into iweb_wallet_admin(credit,is_point,wallet_date,transaction_id,design_id)values('".$admin_points."','".$is_point."','".$cur_date."','".$transaction_id."','".$design_id."')");	
		
		$pay_admin_wallet_transaction= array('credit'=>$admin_points,
		'is_point'=>$is_point,
		'wallet_date'=>$cur_date,
		'transaction_id'=>$transaction_id,
		'design_id'=>$design_id
		);
		$this->db->insert('wallet_admin',$pay_admin_wallet_transaction);		

}///===pay to admin wallet====

return 1;

} //==if balance
	else
	{
		return "NO_BALANCE";
		die;
	}


	} //===if design get
}

	function chk_track_view($design_id,$user_id)
	{
		$cur_date = date('Y-m-d');
		
		$query = $this->db->query("select * from ".$this->db->dbprefix('design_view')." where design_id = ".$design_id." and user_id = ".$user_id." and DATE_FORMAT(view_date,'%Y-%m-%d') = '".$cur_date."'");

		if($query->num_rows()>0)
		{
			 return 1;
		}
		else
		{
			return 0;
		}
	}
	
	function chk_track_other_view($design_id)
	{
		$cur_date = date('Y-m-d');
		
		$query = $this->db->query("select * from ".$this->db->dbprefix('design_view')." where design_id = ".$design_id." and view_ip = '".getRealIP()."' and DATE_FORMAT(view_date,'%Y-%m-%d') = '".$cur_date."'");

		if($query->num_rows()>0)
		{
			 return 1;
		}
		else
		{
			return 0;
		}
	}

	function set_design_view($design_id)
	{
		
		
		$user_id=0;
		$chk_track_view  = 1;
		if(get_authenticateUserID() > 0)
		{
			$user_id = get_authenticateUserID();
		}
		if($user_id > 0 )
		{
			$chk_track_view = $this->chk_track_view($design_id,$user_id);
		}
		if($chk_track_view == 0)
		{
			
			$sql_res = "update ".$this->db->dbprefix('design')." set `design_view_count` = `design_view_count` + 1 where design_id =".$design_id;
			$this->db->query($sql_res);
			
			$data = array('design_id'=>$design_id,
						  'user_id'=>$user_id,
						  'view_date'=>date('Y-m-d h:i:s'),	
						  'view_ip'=>getRealIP()
						);
			$this->db->insert('design_view',$data);
		}
		else
		{
			$chk_track_other_view = $this->chk_track_other_view($design_id);
		
			if($chk_track_other_view == 0)
			{
			$sql_res = "update ".$this->db->dbprefix('design')." set `design_view_count` = `design_view_count` + 1 where design_id =".$design_id;
			$this->db->query($sql_res);
			
			$data = array('design_id'=>$design_id,
						  'user_id'=>0,
						  'view_date'=>date('Y-m-d h:i:s'),	
						  'view_ip'=>getRealIP()
						);
			$this->db->insert('design_view',$data);
			}
		}
		
	}	
	
	function check_like_track($user_id,$design_id)
	{

		$query = $this->db->query("select * from ".$this->db->dbprefix('design_like')." where design_id = ".$design_id." and user_id = ".$user_id);

		if($query->num_rows()>0)
		{
			 return $query->row();
		}
		else
		{
			return 0;
		}
	}
	
	function check_like_status($like_status,$user_id,$design_id)
	{
		$query = $this->db->query("select * from ".$this->db->dbprefix('design_like')." where design_id = ".$design_id." and like_status = ".$like_status." and user_id = ".$user_id);

		if($query->num_rows()>0)
		{
			 return $query->row();
		}
		else
		{
			return 0;
		}
	}
	
	function set_like_dislike($like_status,$design_id)
	{
		
		//$chk_track_like  = 1;
		//like
		$chk_like = $this->check_like_track(get_authenticateUserID(),$design_id);
		
		if($chk_like)
		{
			 $data = array('design_id'=>$design_id,
						  'user_id'=>get_authenticateUserID(),
						  'like_date'=>date('Y-m-d h:i:s'),	
						  'like_status'=>$like_status,
						  'like_ip'=>getRealIP()
						);
				$this->db->where(array('user_id'=>get_authenticateUserID(),'design_id'=>$design_id));		
				$this->db->update('design_like',$data);
				
				
			 if($like_status == 1)
			 {
				  $sql_res = "update ".$this->db->dbprefix('design')." set `design_like_count` = `design_like_count` + 1 where design_id =".$design_id;
			 }
			 else
			 {
				   $sql_res = "update ".$this->db->dbprefix('design')." set `design_like_count` = `design_like_count` - 1 where design_id =".$design_id;
			 }  
			 
			 $this->db->query($sql_res);	
		}
		
		else
		{
			
		     $data = array('design_id'=>$design_id,
						  'user_id'=>get_authenticateUserID(),
						  'like_date'=>date('Y-m-d h:i:s'),	
						  'like_status'=>$like_status,
						  'like_ip'=>getRealIP()
						);
			$this->db->insert('design_like',$data);
			$sql_res = "update ".$this->db->dbprefix('design')." set `design_like_count` = `design_like_count` + 1 where design_id =".$design_id;
			$this->db->query($sql_res);	
		}
		
		
	}

function get_all_images_of_design($design_id = 0)
{
	    $this->db->select('*');
		$this->db->from('design_attachment');
        $this->db->where('design_id',$design_id);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
		return  $query->result();	
		
		}
	
	return 0;
}
	
}
?>