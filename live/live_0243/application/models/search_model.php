<?php
/*********************************************************************************
 * This the taskrabbitclone.com  by Rockers Technology. is paid software. It is released under the terms of 
 * the following BSD License.
 * 
 *  Rockers Technologies (Head Office)
 *    5038,Berthpage Dr
 *    suwanee, GA. Zip Code : 30024
    
 *    E-mail Address : nishu@rockersinfo.com
 * 
 * Copyright © 2012-2020 by Rockers Technology , INC a domestic profit corporation has been duly incorporated under
the laws of the state of georiga , USA. www.rockersinfo.com
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this 
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this 
 *   list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.

 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
 
class Search_model extends IWEB_Model 
{

	/*
	Function name :Search_model
	Description :its default constuctor which called when Search_model object initialzie.its load necesary parent constructor
	*/
	function Search_model()
    {
        parent::__construct();	
    }
	
	function get_total_trip($from_place,$to_place,$min_amount,$max_amount,$package_type,$time_left_hours,$post_date,$sort_type)
	{
		$this->db->select('*');
		$this->db->from('trip tp');
		$this->db->join('user us','tp.user_id=us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->where('tp.trip_status',1);
		$this->db->where('tp.trip_activity_status',0);
		$this->db->where('tp.trip_agent_assign_id',0);
		$this->db->where('tp.trip_confirm_date >= ',date('Y-m-d H:i:s'));
		
		
		if($from_place!='')
		{
			$this->db->like('tp.trip_from_place',$from_place);
		}
		
		if($to_place!='')
		{
			$this->db->like('tp.trip_to_place',$to_place);
		}
		
		
		if($min_amount!='' && $min_amount>0)
		{
			$this->db->where('tp.trip_min_budget >= ',$min_amount);
		}
		
		if($max_amount!='' && $max_amount>0)
		{
			$this->db->where('tp.trip_max_budget <= ',$max_amount);
		}
		
		
		if($package_type!='')
		{
				$package_str='';
			
				if(substr_count($package_type,',')>=1)
				{
					$ex=explode(',',$package_type);
					
					foreach($ex as $val)
					{
						if($val>0)
						{
							$package_str .= '"'.$val.'",';
						}
					}	
					
					$package_str = substr($package_str,0,-1);
					
					$this->db->where('tp.package_type in ('.$package_str.')');
					
				}	
				else
				{
					$package_str=$package_type;
					
					if($package_str>0)
					{
						$this->db->where('tp.package_type in ('.$package_str.')');
					}
					
				}	
			
			
			
		}
		
		
		if($time_left_hours!='' && $time_left_hours>0)
		{
			
			//SELECT TIME_FORMAT(TIMEDIFF(`trip_confirm_date`,NOW()),'%H') from egg_trip;
			
			$this->db->where("TIME_FORMAT(TIMEDIFF(tp.trip_confirm_date,'".date('Y-m-d H:i:s')."'),'%i') > ",15);
			$this->db->where("TIME_FORMAT(TIMEDIFF(tp.trip_confirm_date,'".date('Y-m-d H:i:s')."'),'%H') <= ",$time_left_hours);
		}
		
		if($post_date!='' && $post_date>0)
		{
			$cur_date=date('Y-m-d');
			$minus_date=date('Y-m-d',strtotime('-'.$post_date.' day',strtotime($cur_date)));
			
			$this->db->where('DATE(tp.trip_added_date)',$minus_date);
		}
		
		
				
		$this->db->order_by('tp.trip_added_date',$sort_type);
		
		$query=$this->db->get();
		
		///echo $this->db->last_query();
		
		
		return $query->num_rows();
		
		
	}
	
	
	function get_trip_result($limit,$offset,$from_place,$to_place,$min_amount,$max_amount,$package_type,$time_left_hours,$post_date,$sort_type)
	{
		$this->db->select('*');
		$this->db->from('trip tp');
		$this->db->join('user us','tp.user_id=us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->where('tp.trip_status',1);
		$this->db->where('tp.trip_activity_status',0);
		$this->db->where('tp.trip_agent_assign_id',0);
		$this->db->where('tp.trip_confirm_date >= ',date('Y-m-d H:i:s'));		
		
		
		
		if($from_place!='')
		{
			$this->db->like('tp.trip_from_place',$from_place);
		}
		
		if($to_place!='')
		{
			$this->db->like('tp.trip_to_place',$to_place);
		}
		
		
		if($min_amount!='' && $min_amount>0)
		{
			$this->db->where('tp.trip_min_budget >= ',$min_amount);
		}
		
		if($max_amount!='' && $max_amount>0)
		{
			$this->db->where('tp.trip_max_budget <= ',$max_amount);
		}
		
		
		if($package_type!='')
		{
				$package_str='';
			
				if(substr_count($package_type,',')>=1)
				{
					$ex=explode(',',$package_type);
					
					foreach($ex as $val)
					{
						if($val>0)
						{
							$package_str .= '"'.$val.'",';
						}
					}	
					
					$package_str = substr($package_str,0,-1);
					
					$this->db->where('tp.package_type in ('.$package_str.')');
					
				}	
				else
				{
					$package_str=$package_type;
					
					if($package_str>0)
					{
						$this->db->where('tp.package_type in ('.$package_str.')');
					}
					
				}	
			
			
			
		}
		
		
		if($time_left_hours!='' && $time_left_hours>0)
		{
			
			//SELECT TIME_FORMAT(TIMEDIFF(`trip_confirm_date`,NOW()),'%H') from egg_trip;
			
			$this->db->where("TIME_FORMAT(TIMEDIFF(tp.trip_confirm_date,'".date('Y-m-d H:i:s')."'),'%i') > ",15);
			$this->db->where("TIME_FORMAT(TIMEDIFF(tp.trip_confirm_date,'".date('Y-m-d H:i:s')."'),'%H') <= ",$time_left_hours);
		}
		
		if($post_date!='' && $post_date>0)
		{
			$cur_date=date('Y-m-d');
			$minus_date=date('Y-m-d',strtotime('-'.$post_date.' day',strtotime($cur_date)));
			
			$this->db->where('DATE(tp.trip_added_date)',$minus_date);
		}
		
		
				
		$this->db->order_by('tp.trip_added_date',$sort_type);
		
		
		$this->db->limit($limit,$offset);
		
		$query=$this->db->get();
		//echo $query->num_rows();
		//echo $this->db->last_query();
		//die;
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		return 0;
	}
	
}?>