<?php
class User_model extends IWEB_Model 
{

	/*
	Function name :User_model
	Description :its default constuctor which called when user_model object initialzie.its load necesary parent constructor
	*/
	function User_model()
    {
        parent::__construct();	
    } 
	
	
	
	
	/*
	Function name :edit_account()
	Parameter : none
	Return : integer 1
	Use : update user account
	*/
	
	function edit_account()
	{	
		
		$user_info=get_user_profile_by_id(get_authenticateUserID());
		
		$image_settings = image_setting();
		
		
		if($_FILES['profileimage']['name']!='')
		{
			
				 $this->load->library('upload');
				 $rand=randomCode(); 
				  
				 $_FILES['userfile']['name']     =   $_FILES['profileimage']['name'];
				 $_FILES['userfile']['type']     =   $_FILES['profileimage']['type'];
				 $_FILES['userfile']['tmp_name'] =   $_FILES['profileimage']['tmp_name'];
				 $_FILES['userfile']['error']    =   $_FILES['profileimage']['error'];
				 $_FILES['userfile']['size']     =   $_FILES['profileimage']['size'];
	  
				 
				 $config['file_name'] = $rand;
				 $config['upload_path'] = base_path().'upload/user_orig/';
				 $config['allowed_types'] = '*';
					
 				$this->upload->initialize($config);
  
				  if (!$this->upload->do_upload())
				  {
					$error =  $this->upload->display_errors();   
				  } 
				   
				   
				  $picture = $this->upload->data();		
				  $image_name=$picture['file_name'];
				 

					
				$this->load->library('image_lib');					
				$this->image_lib->clear();	
			
				$config['image_library'] = 'GD2';
				$config['thumb_marker'] = "";
				$config['maintain_ratio'] = TRUE;
				$config['create_thumb'] = TRUE;
				$config['quality'] = '100%';
				$config['master_dim'] = 'width';
				$config['source_image'] = $picture['full_path'];
				$config['new_image'] = base_path().'upload/user_thumb/'.$picture['file_name'];					
				$config['width'] = $image_settings->user_width;
				$config['height'] = $image_settings->user_height;

				$this->image_lib->initialize($config);
				
				
					
				if(!$this->image_lib->resize()){
					$error = $this->image_lib->display_errors();				
				}
							
					
					
				$this->image_lib->clear();
				
				$config['image_library'] = 'GD2';
				$config['thumb_marker'] = "";
				$config['maintain_ratio'] = TRUE;
				$config['create_thumb'] = TRUE;
				$config['quality'] = '100%';
				$config['master_dim'] = 'width';
				$config['source_image'] = $picture['full_path'];
				$config['new_image'] = base_path().'upload/user/'.$picture['file_name'];
				$config['width'] = $image_settings->user_width;
				$config['height'] = $image_settings->user_height;
				$this->image_lib->initialize($config);
		
				if(!$this->image_lib->resize()){
					$error = $this->image_lib->display_errors();				
				}
					
				$this->image_lib->clear();
				
				
				 if($user_info->profile_image != '') 
					{
						if(file_exists(base_path().'upload/user_orig/'.$user_info->profile_image))
						{
							unlink(base_path().'upload/user_orig/'.$user_info->profile_image);
						}
						if(file_exists(base_path().'upload/user_thumb/'.$user_info->profile_image))
						{
							unlink(base_path().'upload/user_thumb/'.$user_info->profile_image);
						}
						
						if(file_exists(base_path().'upload/user/'.$user_info->profile_image))
						{
							unlink(base_path().'upload/user/'.$user_info->profile_image);
						}
					}
							
				
				
			
				$data_img=array(
					'profile_image' => $picture['file_name'],
				);
				$this->db->where('user_id',get_authenticateUserID());
				$this->db->update('user_profile',$data_img);
			}
		
		
		$first_name=$this->input->post('first_name');
		$last_name=$this->input->post('last_name');

		$data = array(		
			'full_name' => $first_name.' '.$last_name,		
			'first_name' => $first_name,
			'last_name' => $last_name,	
			'email' => $this->input->post('email'),			
			'zip_code'=>$this->input->post('zip_code'),
			'mobile_no'=>$this->input->post('mobile_no'),						
			'phone_no' =>$this->input->post('phone_no'),
		); 

		$this->db->where('user_id',get_authenticateUserID());
		$this->db->update('user', $data);
		
		
		$data_profile = array(		
			'about_user' => $this->input->post('about_user'),			
			'own_site_link'=>$this->input->post('own_site_link'),
			'facebook_link'=>$this->input->post('facebook_link'),						
			'twitter_link' =>$this->input->post('twitter_link'),
			'linkedin_link' =>$this->input->post('linkedin_link')
		); 
		
		
		$this->db->where('user_id',get_authenticateUserID());
		$this->db->update('user_profile', $data_profile);
	
	}
	
	/*
	Function name :change_password()
	Parameter : none
	Return : none
	Use : update user current password
	*/
	
	function change_password()
	{
		
		$data=array(
		   'password'=>md5($this->input->post('new_password'))		
		);
		
		$this->db->where('user_id',get_authenticateUserID());		
		$this->db->update('user', $data);
		
	}
	
	
	
	

	function deactivateuser($user_id)
	{
		
		///===user own things
		
		////=== User profile image
			$user_info=get_user_profile_by_id($user_id);
		
				if($user_info->profile_image != '') 
					{
						if(file_exists(base_path().'upload/user_orig/'.$user_info->profile_image))
						{
							unlink(base_path().'upload/user_orig/'.$user_info->profile_image);
						}
						if(file_exists(base_path().'upload/user_thumb/'.$user_info->profile_image))
						{
							unlink(base_path().'upload/user_thumb/'.$user_info->profile_image);
						}
						
						if(file_exists(base_path().'upload/user_medium/'.$user_info->profile_image))
						{
							unlink(base_path().'upload/user_medium/'.$user_info->profile_image);
						}
					}
		
		
		
		
		
	$datauser=array(
		'user_status'=>3,
		'tw_id'=>'',
		'twitter_screen_name'=>'',
		'tw_oauth_token'=>'',
		'tw_oauth_token_secret'=>'',
		'fb_id'=>''
	);
	
	
	$this->db->where('user_id',$user_id);
	$this->db->update('user',$datauser);
	
	
					
					////==destroy cache====	
					$this->load->driver('cache');			
					
					$supported_cache=check_supported_cache_driver();
					
					////==destroy now====		
					if(isset($supported_cache))
					{
						if($supported_cache!='' && $supported_cache!='none')
						{	
							if($this->cache->$supported_cache->get('user_login'.$user_id))
							{								
								$this->cache->$supported_cache->delete('user_login'.$user_id);						
							}
						}
						
					}
					
			
	
			
			
			////========
		
	}
	
	/*
	Function name :get_user_notification()
	Parameter : $user_id(user id)
	Return : array of user notification information
	Use : get user notification details
	*/
	
	function update_notification($user_id)
	{
		$data = array('is_read'=>0);
		$this->db->where('to_user_id',$user_id);
		$this->db->update('system_notification',$data);
	}
	
	function get_user_notification_count($user_id)
	{
		$this->db->select('*');
		$this->db->from('system_notification');
		$this->db->where('to_user_id',$user_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_user_notification($user_id,$limit,$offset)
	{
		$this->db->select('*');
		$this->db->from('system_notification');
		$this->db->where('to_user_id',$user_id);
		$this->db->order_by('notification_id','desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
		
	}	
	
	
	/*
	Function name :change_notification()
	Parameter : none
	Return : none
	Use : update user notification details
	*/
	
	function change_notification()
	{
	
		$data=array(
			'on_new_public_conversation'=>$this->input->post('on_new_public_conversation'),
			'on_new_offer'=>$this->input->post('on_new_offer'),
			'on_new_offer_conversation'=>$this->input->post('on_new_offer_conversation'),
			'on_offer_declined'=>$this->input->post('on_offer_declined'),
			'on_offer_accpet'=>$this->input->post('on_offer_accpet'),
			'on_trip_complete'=>$this->input->post('on_trip_complete'),
		);
		
		$this->db->where('user_id',get_authenticateUserID());		
		$this->db->update('user_notification', $data);
		
	}
	
	function get_user_total_reviews($user_id)
	{
		$this->db->select('*');
		$this->db->from('user_review ur');
		$this->db->join('trip tp','ur.trip_id=tp.trip_id');
		$this->db->join('user us','ur.review_to_user_id=us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->where('ur.review_to_user_id',$user_id);
		
		$query=$this->db->get();
		
		return $query->num_rows();

	}
	
	function get_user_review_list($user_id,$limit,$offset)
	{

		$this->db->select('*');
		$this->db->from('user_review ur');
		$this->db->join('trip tp','ur.trip_id=tp.trip_id');
		$this->db->join('user us','ur.review_by_user_id	=us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->where('ur.review_to_user_id',$user_id);
		$this->db->order_by('ur.user_review_date','desc');
		$this->db->limit($limit,$offset);
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}

		return 0;

	}
	
	function insert_air_quote($quote_user_id='')
	{
		$airline_from = explode(',',$this->input->post('airline_from'));
		$airline_to = explode(',',$this->input->post('airline_quote_to'));
		$data = array(		
				'quote_type'=>1,
				'user_id'=>$quote_user_id,	
				'airline_from' =>$airline_from[0],
				'airline_to' => $airline_to[0],
				'trip_ip'=>getRealIP(),
				'trip_quote_email'=>$this->input->post('airline_quote_email'),
				'quote_message'=>$this->input->post('airline_message'),
				'deal_id'=>$this->input->post('airline_deal_id'),
				'quote_date'=>date('Y-m-d H:i:s')
				);
				
		
		if($this->db->insert('trip_quote',$data))
		{
			return 1;
		}
		else
		{
			return 0;
		}		
	}
	
	function insert_trip_quote($quote_user_id='')
	{
		$trip_from = explode(',',$this->input->post('trip_from'));
		$trip_to = explode(',',$this->input->post('trip_to'));
		$data = array(		
				'user_id' => $quote_user_id,	
				'quote_type'=>1,	
				'trip_from_place' =>$trip_from[0],
				'trip_to_place' => $trip_to[0],
				'trip_ip'=>getRealIP(),
				'trip_days'=>$this->input->post('total_day'),
				'quote_type'=>2,
				'trip_quote_email'=>$this->input->post('trip_email'),
				'quote_message'=>$this->input->post('trip_quote_msg'),
				'deal_id'=>$this->input->post('trip_deal_id'),
				'quote_date'=>date('Y-m-d H:i:s')
				);
		if($this->db->insert('trip_quote',$data))
		{
			return 1;
		}
		else
		{
			return 0;
		}		
	}
	
	function get_total_user_package()
	{
		$this->db->select('up.*,p.package_name');
		$this->db->from('package p');
		$this->db->join('user_package up','up.package_id=p.package_id');
		//$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}	
	
	function get_user_package($limit,$offset)
	{
		
		$this->db->select('up.*,p.package_name');
		$this->db->from('package p');
		$this->db->join('user_package up','up.package_id=p.package_id');
		$this->db->where('up.user_id',get_authenticateUserID());
		$this->db->order_by('up.subscription_date','desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	function get_total_user_videodownload()
	{
		$this->db->select('tr.*,v.*');
		$this->db->from('transaction tr');
		$this->db->join('video v','tr.video_id=v.video_id');
		$this->db->where('tr.parent_transaction_id',0);
		$this->db->where('tr.video_id > ',0);
		$this->db->where('tr.user_id',get_authenticateUserID());
		//$this->db->order_by('tr.transaction_id','desc');
		//$this->db->limit($limit,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
		return $query->num_rows();
		}
		return 0;

	}	
	
	function get_user_videodownload($limit,$offset)
	{
		$this->db->select('tr.*,v.*');
		$this->db->from('transaction tr');
		$this->db->join('video v','tr.video_id=v.video_id');
		$this->db->where('tr.parent_transaction_id',0);
		$this->db->where('tr.video_id > ',0);
		$this->db->where('tr.user_id',get_authenticateUserID());
		$this->db->order_by('tr.transaction_id','desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
		return $query->result();
		}
		return 0;
	}
	
	
	
	function get_total_user_designdownload()
	{
		
		$this->db->select('tr.*,d.*');
		$this->db->from('transaction tr');
		$this->db->join('design d','tr.design_id=d.design_id');
		$this->db->where('tr.parent_transaction_id',0);
		$this->db->where('tr.design_id > ',0);
		$this->db->where('tr.user_id',get_authenticateUserID());
		//$this->db->order_by('tr.transaction_id','desc');
		//$this->db->limit($limit,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
		return $query->num_rows();
		}
		return 0;
		
		
	}	
	
	function get_user_designdownload($limit,$offset)
	{
		$this->db->select('tr.*,d.*');
		$this->db->from('transaction tr');
		$this->db->join('design d','tr.design_id=d.design_id');
		$this->db->where('tr.parent_transaction_id',0);
		$this->db->where('tr.design_id > ',0);
		$this->db->where('tr.user_id',get_authenticateUserID());
		$this->db->order_by('tr.transaction_id','desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
		return $query->result();
		}
		return 0;
	}
	
	
	
}
?>