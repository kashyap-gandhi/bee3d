<?php
class Printer_model extends IWEB_Model 
{

	/*
	Function name :User_model
	Description :its default constuctor which called when user_model object initialzie.its load necesary parent constructor
	*/
	function Printer_model()
    {
        parent::__construct();	
    } 
    
    
    function get_printer_geo_result($distance_range,$postal_code='',$visitor_lat='',$visitor_lon='')
    {
        
        $visitor_postal_code=$this->session->userdata('visitor_user_postal_code');
    
        $do_postal=0;
        if($visitor_postal_code!=$postal_code && $postal_code!='') {        
            $do_postal=1;
        }
        
        
        $fields='*';
        
        $latlong_field='';
        
        $search_by_miles = 3959;
        $search_by_kms = 6371;
        
        $site_setting=site_setting();        
        
         $system_lat=$site_setting->default_latitude;
         $system_long=$site_setting->default_longitude;
         
         
         if ($visitor_lat == '' && $visitor_lon == ''){
             
            $visitor_lat=$system_lat;
            $visitor_lon=$system_long;
             
         }
         
//         if($do_postal==1){
//             $visitor_lat=$system_lat;
//             $visitor_lon=$system_long;
//         }
         
         
        
        
        if ($visitor_lat != '' && $visitor_lon != '') {

                $latlong_field = "(
                    $search_by_kms * acos (
                      cos ( radians(" . $visitor_lat . ") )
                      * cos( radians( s.store_lat ) )
                      * cos( radians( s.store_long ) - radians(" . $visitor_lon . ") )
                      + sin ( radians(" . $visitor_lat . ") )
                      * sin( radians( s.store_lat ) )
                    )
                  ) AS distance";
            }
            
            
            if($latlong_field!=''){
                $fields.=','.$latlong_field;
            }
            
            
        $this->db->select($fields);
        $this->db->from('store s');
        $this->db->join('store_category_rel scrl','scrl.store_id=s.store_id','left');
        $this->db->join('store_category sc','sc.category_id=scrl.category_id','left');
        $this->db->where('s.store_status',1);
        $this->db->where('sc.category_status',1);
        
        //==&& $do_postal==1
        if($postal_code!='' ){
            $this->db->where('s.store_postal_code',$postal_code);
        }
        
        $this->db->order_by('s.store_id','desc');
        
        if($latlong_field!=''){
            $this->db->having('distance <= ',$distance_range);  
        }
        
     
        $query = $this->db->get();
        
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
                return $query->result_array();
        }
        return 0;
    }
	
	
	
	function get_printer_result()
	{
		$this->db->select('*');
		$this->db->from('store s');
		$this->db->join('store_category_rel scrl','scrl.store_id=s.store_id','left');
		$this->db->join('store_category sc','sc.category_id=scrl.category_id','left');
                $this->db->where('s.store_status',1);
                $this->db->where('sc.category_status',1);
		$this->db->order_by('s.store_id','desc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		}
		return 0;
	}
        
        
        ///========manage part=================
        
        function get_user_location_count($user_id)
	{
		$this->db->select('*');
		$this->db->from('store s');
		$this->db->join('store_category_rel scrl','scrl.store_id=s.store_id','left');
		$this->db->join('store_category sc','sc.category_id=scrl.category_id','left');
		$this->db->where('s.user_id',$user_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_user_location($user_id,$limit,$offset)
	{
		$this->db->select('*');
		$this->db->from('store s');
		$this->db->join('store_category_rel scrl','scrl.store_id=s.store_id','left');
		$this->db->join('store_category sc','sc.category_id=scrl.category_id','left');
		$this->db->where('s.user_id',$user_id);
		$this->db->order_by('s.store_id','desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
		
	}	
	
	function insert_location()
	{
		$data = array(		
				'user_id'=>get_authenticateUserID(),
				'store_date'=>date('Y-m-d H:i:s'),
				'store_ip' =>getRealIP(),
				//'store_status' => $this->input->post('store_status'),
				'store_name' => $this->input->post('store_name'),
				'store_address' => $this->input->post('store_address'),
				'store_address2' => $this->input->post('store_address2'),
				'store_ctiy' => $this->input->post('store_ctiy'),
				'store_state' => $this->input->post('store_state'),
				'store_postal_code' => $this->input->post('store_postal_code'),
				'store_country' => $this->input->post('store_country'),
				'store_lat' => $this->input->post('store_lat'),
				'store_long'=>$this->input->post('store_long'),
				'store_email'=>$this->input->post('store_email'),
				'store_phone'=>$this->input->post('store_phone'),
				'store_url'=>$this->input->post('store_url'),
				);
			$this->db->insert('store',$data);
			
			$store_id = mysql_insert_id();	
			$data_rel = array('store_id'=>$store_id,
							  'category_id'=>$this->input->post('category_id'));
			$this->db->insert('store_category_rel',$data_rel);
		
	}
	
	function update_location()
	{
		$data = array(		
				'user_id'=>get_authenticateUserID(),
				'store_date'=>date('Y-m-d H:i:s'),
				'store_ip' =>getRealIP(),
				//'store_status' => $this->input->post('store_status'),
				'store_name' => $this->input->post('store_name'),
				'store_address' => $this->input->post('store_address'),
				'store_address2' => $this->input->post('store_address2'),
				'store_ctiy' => $this->input->post('store_ctiy'),
				'store_state' => $this->input->post('store_state'),
				'store_postal_code' => $this->input->post('store_postal_code'),
				'store_country' => $this->input->post('store_country'),
				'store_lat' => $this->input->post('store_lat'),
				'store_long'=>$this->input->post('store_long'),
				'store_email'=>$this->input->post('store_email'),
				'store_phone'=>$this->input->post('store_phone'),
				'store_url'=>$this->input->post('store_url'),
				);
			$this->db->where('store_id',$this->input->post('store_id'));
			$this->db->update('store',$data);
			
			$data_rel = array('category_id'=>$this->input->post('category_id'));
			$this->db->where('store_id',$this->input->post('store_id'));
			$this->db->update('store_category_rel',$data_rel);			
			
		
	}
	
	
	
	function emailTaken($store_email)
	{
	
		 $query = $this->db->query("select * from ".$this->db->dbprefix('store')." where store_email='".$store_email."'");
		 
		 if($query->num_rows()>0)
		 {
			return true;
		 }
		 else 
		 {
		 	
			return false;
			
		 }		
	}
	
		
	function get_all_store_category()
	{
		$query=$this->db->get_where('store_category',array('category_status'=>1));
		
		if($query->num_rows()>0)
		{			
			return $query->result();
			
		} else {
		
			return false;		
		}
	}
	
	function check_exist_store($store_id,$user_id)
	{
		$this->db->select('*');
		$this->db->from('store');
		$this->db->where('store_id',$store_id);
		if($user_id > 0)
		{
			$this->db->where('user_id',$user_id);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function get_one_store($store_id)
	{
		$this->db->select('*');
		$this->db->from('store s');
		$this->db->join('store_category_rel scrl','scrl.store_id=s.store_id','left');
		$this->db->join('store_category sc','sc.category_id=scrl.category_id','left');
		$this->db->where('s.store_id',$store_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else
		{
			return 0;
		}
	}	
	
	
}
?>