<?php
/*********************************************************************************
 * This the taskrabbitclone.com  by Rockers Technology. is paid software. It is released under the terms of 
 * the following BSD License.
 * 
 *  Rockers Technologies (Head Office)
 *    5038,Berthpage Dr
 *    suwanee, GA. Zip Code : 30024
    
 *    E-mail Address : nishu@rockersinfo.com
 * 
 * Copyright © 2012-2020 by Rockers Technology , INC a domestic profit corporation has been duly incorporated under
the laws of the state of georiga , USA. www.rockersinfo.com
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this 
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this 
 *   list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.

 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
 
class Conversation_model extends IWEB_Model 
{

	/*
	Function name :Conversation_model
	Description :its default constuctor which called when Home_model object initialzie.its load necesary parent constructor
	*/
	function Conversation_model()
    {
        parent::__construct();	
    } 
	
	function agent_offer_details($trip_id='',$user_id='')
	{
	
	   $this->db->select('*');
	   $this->db->from('user us');
	   $this->db->join('user_profile up','us.user_id=up.user_id');
	   $this->db->join('agent_profile ap','ap.user_id=up.user_id');
	   $this->db->join('trip_offer tf','tf.user_id=ap.user_id');
	   $this->db->join('trip tp','tf.trip_id=tp.trip_id');
	   $this->db->where('us.user_id',$user_id);
	   $this->db->where('tp.trip_id',$trip_id);                 
	   $this->db->where('tp.trip_status',1);    
	   //$this->db->where('tp.trip_activity_status',0);
	   //$this->db->where('tp.trip_agent_assign_id',0);
	   $this->db->where('ap.agent_app_approved',1);
	   //$this->db->where('ap.agent_status',1);
	   $this->db->group_by('tf.trip_offer_id');                  
	   
	   $query=$this->db->get();
	  
	   if($query->num_rows()>0)
	   {
	   	   return $query->result();
	   }
	   else
	   {
			return 0;
		}
	}
	
	function get_all_conversation($trip_id='',$trip_user_id='',$offer_user_id='')
	{
	
	
		$ids= $trip_user_id.','.$offer_user_id;

		$query = $this->db->query("select * from ".$this->db->dbprefix('trip_offer_conversation')." tp, ".$this->db->dbprefix('user')." ur, ".$this->db->dbprefix('user_profile')." up  where tp.trip_id = '".$trip_id."' and ur.user_id = tp.post_by_user_id and ur.user_id = up.user_id and tp.trip_id = '".$trip_id."' and (tp.post_to_user_id in (".$ids.") and tp.post_by_user_id in (".$ids.") ) order by tp.trip_offer_conversation_id asc");
		
		return $query->result();

	  
	}
	
	
	function user_new_conversation()
	{
	
		$data = array(					
			'trip_id' => $this->input->post('trip_id'),
			'post_to_user_id' => $this->input->post('post_to_user_id'),
			'post_by_user_id' => get_authenticateUserID(),
			'conversation_message' => $this->input->post('conversation_message'),
			'conversation_date' => date('Y-m-d H:i:s'),	
			'conversation_ip' => getRealIP()		
		);
		//echo '<pre>'; print_r($data); die();
		$this->db->insert('trip_offer_conversation', $data); 
		
	
	}
	
	
	function accept_offer($offer_user_id,$trip_id)
	{
		$trip_data = array(
						'trip_assign_date' =>date('Y-m-d H:i:s'),
						'trip_agent_assign_id' =>$offer_user_id,
						'trip_activity_status' => 1
					);
		$this->db->where('trip_id',$trip_id);
		$this->db->update('trip',$trip_data);
		
		$offer_data = array(
						'offer_status_for_agent' =>1
					);
		$this->db->where(array('trip_id'=>$trip_id,'user_id'=>$offer_user_id));
		$this->db->update('trip_offer',$offer_data);

	
	}
	
	function complete_trip()
	{
	
		$data = array(					
			'trip_id' => $this->input->post('trip_id'),
			'review_to_user_id' => $this->input->post('trip_offer_user_id'),
			'review_by_user_id' => get_authenticateUserID(),
			'user_rating' => $this->input->post('user_rating'),				
			'user_review' => $this->input->post('user_review'),
			'user_review_date' => date('Y-m-d H:i:s')		
		);
		//echo '<pre>'; print_r($data); die();
		if($this->db->insert('user_review', $data)) 
		{
			$trip_data = array(
							'trip_completed_date' =>date('Y-m-d H:i:s'),
							'trip_close_date' =>date('Y-m-d H:i:s'),
							'trip_activity_status' => 2
						);
			$this->db->where('trip_id',$this->input->post('trip_id'));
			$this->db->update('trip',$trip_data);
			
			$offer_data = array(
							'offer_status_for_agent' =>2
						);
			$this->db->where(array('trip_id'=>$this->input->post('trip_id'),'user_id'=>$this->input->post('trip_offer_user_id')));
			$this->db->update('trip_offer',$offer_data);
		}
		
	
	}
	
	
	function get_offer_detail($trip_offer_id){
	
	   $this->db->select('*,tp.user_id as trip_user_id');
	   $this->db->from('trip_offer tof');
	   $this->db->join('trip tp','tof.trip_id=tp.trip_id');
	   $this->db->join('user us','tof.user_id=us.user_id');
	   $this->db->join('user_profile up','us.user_id=up.user_id');
	   $this->db->where('tof.trip_offer_id',$trip_offer_id);
	   $query=$this->db->get();
	 
	 
	   if($query->num_rows()>0)
	   {
	     return $query->row();
	   }
	   
		return 0;
		
	}
	
}

?>