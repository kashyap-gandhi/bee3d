<?php
class Video_model extends IWEB_Model 
{

	/*
	Function name :User_model
	Description :its default constuctor which called when user_model object initialzie.its load necesary parent constructor
	*/
	function Video_model()
    {
        parent::__construct();	
    } 
	
	function check_exist_video($video_id,$user_id)
	{
		$this->db->select('*');
		$this->db->from('video');
		$this->db->where('video_id',$video_id);
		if($user_id > 0)
		{
			//For check in view also
			$this->db->where('user_id',$user_id);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function insert_design_category()
	{
		
		$image_settings = image_setting();
		if($_FILES['category_image']['name']!='')
		{
				 $this->load->library('upload');
				 $rand=randomCode(); 
				  
				 $_FILES['userfile']['name']     =   $_FILES['category_image']['name'];
				 $_FILES['userfile']['type']     =   $_FILES['category_image']['type'];
				 $_FILES['userfile']['tmp_name'] =   $_FILES['category_image']['tmp_name'];
				 $_FILES['userfile']['error']    =   $_FILES['category_image']['error'];
				 $_FILES['userfile']['size']     =   $_FILES['category_image']['size'];
	   
				 $config['file_name'] = $rand;
				 $config['upload_path'] = base_path().'upload/category_orig/';
				 $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
		  		 $this->upload->initialize($config);
 
				  if (!$this->upload->do_upload())
				  {
					$error =  $this->upload->display_errors();   
				  } 
				   
				  $picture = $this->upload->data();		
				  $image_name=$picture['file_name'];
				 	
				$this->load->library('image_lib');					
				$this->image_lib->clear();	
			
				$config['image_library'] = 'GD2';
				$config['thumb_marker'] = "";
				$config['maintain_ratio'] = TRUE;
				$config['create_thumb'] = TRUE;
				$config['quality'] = '100%';
				$config['master_dim'] = 'width';
				$config['source_image'] = $picture['full_path'];
				$config['new_image'] = base_path().'upload/category_orig/'.$picture['file_name'];					
				$config['width'] = $image_settings->category_width;
				$config['height'] = $image_settings->category_height;
				$this->image_lib->initialize($config);
				
				if(!$this->image_lib->resize()){
					$error = $this->image_lib->display_errors();				
				}
				$this->image_lib->clear();
			}
			
			
				$data = array('category_name'=>$this->input->post('category_name'),
							  'category_url_name'=>get_category_slug($this->input->post('category_name'),0),
							  'category_parent_id'=>$this->input->post('category_parent_id'),
							  'category_description'=>$this->input->post('category_description'),
							  'category_image'=>$picture['file_name'],
							  'category_status'=>$this->input->post('category_status')
							  );
				
				$this->db->insert('design_category',$data);
	}
	
	function insert_video()
	{
		
		$video_slug = get_design_slug($this->input->post('video_title'),0);
		$video_url='';
		if($this->input->post('video_type') == '1')
		{
			$video_url = $this->input->post('video_url');
		}
		
		
		$new_video_title=BadWords($this->input->post('video_title'));
		$video_title= $new_video_title['str'];
		
		$new_video_content=BadWords($this->input->post('video_content'));
		$video_content= $new_video_content['str'];
		
		$new_video_meta_keyword = BadWords($this->input->post('video_meta_keyword'));
		$video_meta_keyword= $new_video_meta_keyword['str'];
		
		$new_video_meta_description= BadWords($this->input->post('video_meta_description'));
		$video_meta_description= $new_video_meta_description['str'];
		
		$data = array(	'video_title'=>$video_title,
						'video_is_modified'=>$this->input->post('video_is_modified'),
					   'video_slug'=>$video_slug,
					   'video_content'=>$video_content,
					   'video_point'=>$this->input->post('video_point'),
					  'parent_video_id'=>$this->input->post('parent_video_id'),
					  'video_url'=>$video_url,
					  'video_meta_keyword'=>$video_meta_keyword,
					  'video_meta_description'=>$video_meta_description,
					  'comment_allow'=>$this->input->post('comment_allow'),
					  'video_type'=>$this->input->post('video_type'),
					  'user_id'=>get_authenticateUserID(),
					  'video_date'=>date('Y-m-d h:i:s'),
					  'video_ip'=>$_SERVER['REMOTE_ADDR'],
					  'video_status'=>1,
					  'comment_allow'=>$this->input->post('comment_allow')
					  );
				$this->db->insert('video',$data);
				$video_id = mysql_insert_id();
				
				
		//Update slug
		$data_slug = array('video_slug',$video_slug.'-'.$video_id);
		$this->db->where('video_id',$video_id);
		$this->db->update('video',$data);		
		
		/*Insert data in design_category_rel*/
		$data_video_category_rel= array('category_id'=>$this->input->post('video_category'),
										 'video_id'=>$video_id);
		$this->db->insert('video_category_rel',$data_video_category_rel);
		
		
		//$image_settings = image_setting();
		$this->load->library('upload');
		 if($_FILES['video_file']['name']!='')
		 {
						if($_FILES['video_file']['name'][$i]!='')
						{
							
						$rand=rand(0,100000);
						
						$_FILES['userfile']['name']    =   $_FILES['video_file']['name'][$i];
						$_FILES['userfile']['type']    =   $_FILES['video_file']['type'][$i];
						$_FILES['userfile']['tmp_name'] =   $_FILES['video_file']['tmp_name'][$i];
						$_FILES['userfile']['error']       =   $_FILES['video_file']['error'][$i];
						$_FILES['userfile']['size']    =   $_FILES['video_file']['size'][$i]; 
						  
						
						$config['file_name']     = $rand.'video_'.$video_id.'_'.$i;
						$config['upload_path'] = base_path().'upload/video_file/';					
						//$config['allowed_types'] = 'doc|docx|jpg|jpeg|gif|png|bmp';
						$config['allowed_types'] = '*';
						
					   $this->upload->initialize($config);
						if (!$this->upload->do_upload())
						{		
							 $error =  $this->upload->display_errors();
						} 
			
			
			$picture = $this->upload->data();
            $image=$picture['file_name'];
			
								
					if($i==1)
					{
						$attachment_is_main = 1;
					}		
					else
					{
						$attachment_is_main = 0;
					}
							$data_gallery=array(
							'video_id'=>$video_id,
							'attachment_is_main'=>$attachment_is_main,
							'attachment_name'=>$image,
							'attachment_type'=>1,
							'attachment_order'=>$i,
							'date_added' => date('Y-m-d H:i:s')
							);
							$this->db->insert('video_attachment',$data_gallery);
						
						}
			
		}	
		
		/*Insert Tags*/
		$tags = explode(",",$this->input->post('tag_name'));
		if($tags)
		{
			for($i=0;$i<count($tags);$i++)
			 {
				$tag_id = check_tag($tags[$i]);
				$data_tag = array('video_id'=>$video_id,
								'tag_id'=>$tag_id,
								'tag_date'=>date('Y-m-d h:i:s'),
								'tag_ip'=>$_SERVER['REMOTE_ADDR']);
				$this->db->insert('video_tags',$data_tag);				
			 }
			 
		}
		
		
		
	}
	
	function update_video()
	{
		$video_slug = get_video_slug($this->input->post('video_title'),0);
		$video_id = $this->input->post('video_id');
		$video_slug = $video_slug.'-'.$video_id;
		$video_url='';
		if($this->input->post('video_type') == '1')
		{
			$video_url = $this->input->post('video_url');
		}
		
		$new_video_title=BadWords($this->input->post('video_title'));
		$video_title= $new_video_title['str'];
		
		$new_video_content=BadWords($this->input->post('video_content'));
		$video_content= $new_video_content['str'];
		
		$new_video_meta_keyword = BadWords($this->input->post('video_meta_keyword'));
		$video_meta_keyword= $new_video_meta_keyword['str'];
		
		$new_video_meta_description= BadWords($this->input->post('video_meta_description'));
		$video_meta_description= $new_video_meta_description['str'];
		
		$data = array(	'video_title'=>$video_title,
						'video_is_modified'=>$this->input->post('video_is_modified'),
					   'video_slug'=>$video_slug,
					   'video_content'=>$video_content,
					   'video_point'=>$this->input->post('video_point'),
					  'parent_video_id'=>$this->input->post('parent_video_id'),
					  'video_url'=>$video_url,
					  'video_meta_keyword'=>$video_meta_keyword,
					  'video_meta_description'=>$video_meta_description,
					  'comment_allow'=>$this->input->post('comment_allow'),
					  'video_type'=>$this->input->post('video_type'),
					  'user_id'=>get_authenticateUserID(),
					  'video_date'=>date('Y-m-d h:i:s'),
					  'video_ip'=>$_SERVER['REMOTE_ADDR'],
					  'video_status'=>1,
					  'comment_allow'=>$this->input->post('comment_allow')
					  );
		
		$this->db->where('video_id',$video_id);
		$this->db->update('video',$data);
		
		/*Insert data in design_category_rel*/
		$data_video_category_rel= array('category_id'=>$this->input->post('video_category'),
										 'video_id'=>$video_id);
		$this->db->where('video_id',$video_id);
		$this->db->update('video_category_rel',$data_video_category_rel);
		
		/*Insert Tags*/
		//Delete design tag
		$delete_tag=$this->db->query("delete from ".$this->db->dbprefix('video_tags')." where video_id='".$video_id."'");
		
		$tags = explode(",",$this->input->post('tag_name'));
		if($tags)
		{
			for($i=0;$i<count($tags);$i++)
			 {
				$tag_id = check_tag($tags[$i]);
				$data_tag = array('video_id'=>$video_id,
								'tag_id'=>$tag_id,
								'tag_date'=>date('Y-m-d h:i:s'),
								'tag_ip'=>$_SERVER['REMOTE_ADDR']);
				$this->db->insert('video_tags',$data_tag);				
			 }
			 
			 //design_tags
		}
	
		
	}
	
	function get_user_video($user_id,$limit,$video_id = 0)
	{
		$this->db->select('v.*,c.category_name');
		$this->db->from('video v');
		$this->db->join('video_category_rel dr','v.video_id=dr.video_id');
		$this->db->join('video_category c','c.category_id=dr.category_id');
		$this->db->where('v.user_id',$user_id);
		$this->db->where('v.video_status','1');
		$this->db->where('v.video_id !=',$video_id);
		$this->db->order_by('v.video_id','desc');
		$this->db->limit($limit);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	function get_popular_video($limit,$video_id = 0)
	{
		$this->db->select('v.*,c.category_name');
		$this->db->from('video v');
		
		$this->db->join('video_category_rel dr','v.video_id=dr.video_id');
		$this->db->join('video_category c','c.category_id=dr.category_id','LEFT');
		$this->db->where('v.video_status','1');
		//$this->db->where('v.user_id',$user_id);
		$this->db->where('v.video_id !=',$video_id);
		$this->db->order_by('video_id','video_view_count');
		$this->db->limit($limit);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	function get_total_user_video_count($user_id)
	{
		$this->db->select('v.*,c.category_name');
		$this->db->from('video v');
		$this->db->join('video_category_rel dr','v.video_id=dr.video_id');
		$this->db->join('video_category c','c.category_id=dr.category_id','LEFT');
		$this->db->where('v.user_id',$user_id);
		$this->db->order_by('video_id','desc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_video_result($user_id,$limit,$offset)
	{

		$this->db->select('v.*,c.category_name');
		$this->db->from('video v');
		$this->db->join('video_category_rel dr','v.video_id=dr.video_id');
		$this->db->join('video_category c','c.category_id=dr.category_id','LEFT');
		$this->db->where('v.user_id',$user_id);
		$this->db->order_by('video_id','desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
	function get_one_video($video_id)
	{
		$this->db->select('d.*,dr.category_id');
		$this->db->from('video d');
		$this->db->join('video_category_rel dr','d.video_id=dr.video_id');
		//$this->db->join('design_category c','c.category_id=dr.category_id');
		$this->db->where('d.video_id',$video_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else
		{
			return 0;
		}
	}	
	
	function get_one_video_tag($video_id)
	{
		$this->db->select('tg.video_tags_id,t.tag_name,t.tag_id');
		$this->db->from('video_tags tg');
		$this->db->join('tags t','t.tag_id=tg.tag_id');
		$this->db->where('tg.video_id',$video_id);
		$this->db->group_by('tg.tag_id');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
		//$query=$this->db->get_where('design_tags',array('design_id'=>$design_id));
		return $query->result();	
		}
	}
	
	function get_one_design_images($design_id)
	{
		$this->db->select('*');
		$this->db->from('design_attachment');
		$this->db->where('design_id',$design_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
		//$query=$this->db->get_where('design_tags',array('design_id'=>$design_id));
		return $query->result();	
		}
	}
	
	function get_one_design_attachment($attachment_id)
	{
		$this->db->select('*');
		$this->db->from('design_attachment');
		$this->db->where('attachment_id',$attachment_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
		//$query=$this->db->get_where('design_tags',array('design_id'=>$design_id));
		return $query->row();	
		}
	}
	
	function get_one_main_video_image($video_id)
	{
		$this->db->select('*');
		$this->db->from('video_attachment');
		$this->db->where('attachment_is_main',1);
		$this->db->where('video_id',$video_id);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
		//$query=$this->db->get_where('design_tags',array('design_id'=>$design_id));
		$res =  $query->row();	
		return $res->attachment_name;
		}
	}
	
	function get_total_video_count()
	{
		$this->db->select('d.*,c.category_name');
		$this->db->from('video d');
		$this->db->join('video_category_rel dr','d.video_id=dr.video_id');
		$this->db->join('video_category c','c.category_id=dr.category_id','LEFT');
		$this->db->order_by('video_id','desc');
		//$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}	
	
	function get_all_video_result($limit,$offset)
	{
		$this->db->select('d.*,c.category_name');
		$this->db->from('video d');
		$this->db->join('video_category_rel dr','d.video_id=dr.video_id');
		$this->db->join('video_category c','c.category_id=dr.category_id','LEFT');
		$this->db->order_by('video_id','desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	function get_total_category_video_count($category_id,$keyword)
	{
		$this->db->select('d.*,c.category_name');
		$this->db->from('video d');
		$this->db->join('video_category_rel dr','d.video_id=dr.video_id');
		$this->db->join('video_category c','c.category_id=dr.category_id','LEFT');
		$this->db->order_by('video_id','desc');
		if($category_id > 0)
		{
			//$this->db->where('c.category_id',$category_id);
			 $cond = "dr.category_id in ('".str_replace("-",",",$category_id)."')";
			  
			  $this->db->where($cond);
		}
		if($keyword !='' && $keyword != "0")
		{
			$this->db->like('d.video_title',$keyword);
			
			if(substr_count($keyword,' ')>=1)
			{
				$ex=explode(' ',$keyword);
				
				foreach($ex as $val)
				{
					$this->db->like('d.video_title',$val);
				}	
			}

		}
		//$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_category_video_result($limit,$offset,$category_id,$keyword)
	{
		$this->db->select('d.*,c.category_name');
		$this->db->from('video d');
		$this->db->join('video_category_rel dr','d.video_id=dr.video_id');
		$this->db->join('video_category c','c.category_id=dr.category_id','LEFT');
		$this->db->order_by('video_id','desc');
		if($category_id > 0)
		{
			//$this->db->where('c.category_id',$category_id);
			 $cond = "dr.category_id in ('".str_replace("-",",",$category_id)."')";
			  
			  $this->db->where($cond);
		}
		if($keyword !='' && $keyword != "0")
		{
			$this->db->like('d.video_title',$keyword);
			
			if(substr_count($keyword,' ')>=1)
			{
				$ex=explode(' ',$keyword);
				
				foreach($ex as $val)
				{
					$this->db->like('d.video_title',$val);
				}	
			}

		}
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
	
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
		
	}
	
	
	
	
function video_buy_process($video_id){

		
		$cur_date=date('Y-m-d H:i:s');	
		$user_ip=getRealIP();  ///===use custom helper function  getrealIP
		//$picture_sql=$this->db->query(("SELECT * FROM ".$this->db->dbprefix('iweb_design')." where design_id=".$design_id);
		
		$this->db->select('*');
		$this->db->from($this->db->dbprefix('video'));
		$this->db->where('video_id',$video_id);
		$picture_sql = $this->db->get();
		
		///===Notification
		$data_notification = array(	'to_user_id'=>get_authenticateUserID(),
		'video_id'=>$video_id,
		'act'=>'VIDEO_DOWNLOAD',
		'is_read'=>1,
		'notification_date'=>$cur_date
		);
		$this->db->insert('system_notification',$data_notification);
		
		if ($picture_sql->num_rows() > 0) {
		$video_detail =  $picture_sql->row();
		
		//echo "image points <br /><br />";
		
		$video_points=$video_detail->video_point;
		$picture_owner_id=$video_detail->user_id;
		
		//		echo "<br /><br />";
		
		$cur_login_user_id=get_authenticateUserID(); ///==replace with session user id
		
		
		//==check balance of user
		$user_cur_balance=getuserpoints($cur_login_user_id);
		
		if($user_cur_balance>=$image_points){
		
		///===buyer user debit from wallet
		$data_trans = array(	'user_id'=>$cur_login_user_id,
		'video_id'=>$video_id,
		'pay_points'=>$video_points,
		'host_ip'=>$user_ip,
		'transaction_date_time'=>$cur_date
		);
		$this->db->insert('transaction',$data_trans);
		
		//$do_transaction=mysql_query("insert into iweb_transaction(user_id,design_id,pay_points,host_ip,transaction_date_time)values('".$cur_login_user_id."','".$design_id."','".$image_points."','".$user_ip."','".$cur_date."')");
		
		$transaction_id=mysql_insert_id();
		
		//$do_wallet_transaction=mysql_query("insert into iweb_wallet(user_id,design_id,debit,transaction_id,wallet_ip,wallet_date)values('".$cur_login_user_id."','".$design_id."','".$image_points."','".$transaction_id."','".$user_ip."','".$cur_date."')");
		
		$data_wallet= array('user_id'=>$cur_login_user_id,
		'video_id'=>$video_id,
		'debit'=>$video_points,
		'transaction_id'=>$transaction_id,
		'wallet_ip'=>$user_ip,
		'wallet_date'=>$cur_date
		);
		$this->db->insert('wallet',$data_wallet);
		///===end buying
		//====get user tree for recursion payment
		$files_user_ids=array();
		
		if($image_points>0) {	
		if($picture_detail->parent_video_id > 0 && $picture_detail->video_is_modified == 1) {
		
		$files_user_ids=get_all_picture_tree($picture_detail->parent_video_id);
		
		}
		}
		
		//echo "files_user_ids <br /><br />";
		//print_r($files_user_ids); //die;
		///===add current design user id
		$all_ids[]=$picture_owner_id;
		$final_ids=array_merge($all_ids,$files_user_ids);
		///===total users id===
		//echo "<br /><br />total users ids<br /><br />";
		//print_r($final_ids);
		///====commission settings
		$admin_video_point_commission=20; ///in %
		$commission_setting=commission_setting();
		
		if(!empty($commission_setting)){
		$admin_video_point_commission=$commission_setting->admin_commission_video;
		}
		
		$user_total_point_commission=100-$admin_video_point_commission;
		
		
		///=====assign and split percentage to user
		//echo "total pay to users<br /><br />";
		$pay_to_user=splitampunt($user_total_point_commission,$final_ids);	
		//print_r($pay_to_user);
		////====total pay to individual users=
		//echo "total pay to all individual users not admin<br /><br />";
		$final_pay_to_user=$pay_to_user;
		//print_r($final_pay_to_user);  ///die;
		//====count admin points===
		$admin_points=countpoints($image_points,$admin_video_point_commission);
		
		foreach($final_pay_to_user as $user_id=>$user_perc){
		
		$user_video_points=countpoints($image_points,$user_perc);
		
		
		//====insert into db=====
		
		//$check_user_exit=mysql_query("select * from iweb_user where user_id=".$user_id);
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('user_id',$user_id);
		$check_user_exit = $this->db->get();
		
		if($check_user_exit->num_rows() > 0){
		
		//$do_transaction=mysql_query("insert into iweb_transaction(parent_transaction_id,user_id,design_id,pay_points,host_ip,transaction_date_time)values('".$transaction_id."','".$user_id."','".$design_id."','".$user_image_points."','".$user_ip."','".$cur_date."')");
		$do_transaction= array('parent_transaction_id'=>$transaction_id,
		'user_id'=>$user_id,
		'video_id'=>$video_id,
		'pay_points'=>$user_video_points,
		'host_ip'=>$user_ip,
		'transaction_date_time'=>$cur_date
		);
		$this->db->insert('transaction',$do_transaction);
		
		$last_transaction_id=mysql_insert_id();
		
		//$do_wallet_transaction=mysql_query("insert into iweb_wallet(user_id,design_id,credit,transaction_id,wallet_ip,wallet_date)values('".$user_id."','".$design_id."','".$user_image_points."','".$last_transaction_id."','".$user_ip."','".$cur_date."')");
		
		$do_wallet_transaction= array('user_id'=>$user_id,
		'video_id'=>$video_id,
		'credit'=>$user_video_points,
		'transaction_id'=>$last_transaction_id,
		'wallet_ip'=>$user_ip,
		'wallet_date'=>$cur_date
		);
		$this->db->insert('wallet',$do_wallet_transaction);
		
		} else {	
		///==if user not found than transaction points to admin		
		$user_id=$admin_id;
		$admin_points=$admin_points+$user_video_points;
		}	
		} //===foreach users
		
		
		
		///===pay to admin wallet====
		if($admin_points!=''){				
		$is_point=1;
		
		//mysql_query("insert into iweb_wallet_admin(credit,is_point,wallet_date,transaction_id,design_id)values('".$admin_points."','".$is_point."','".$cur_date."','".$transaction_id."','".$design_id."')");	
		
		$pay_admin_wallet_transaction= array('credit'=>$admin_points,
		'is_point'=>$is_point,
		'wallet_date'=>$cur_date,
		'transaction_id'=>$transaction_id,
		'video_id'=>$video_id
		);
		$this->db->insert('wallet_admin',$pay_admin_wallet_transaction);		

}///===pay to admin wallet====

return 1;

} //==if balance
	else
	{
		echo "NO_BALANCE";
	}


	} //===if design get
}

	function chk_track_view($design_id,$user_id)
	{
		$cur_date = date('Y-m-d');
		
		$query = $this->db->query("select * from ".$this->db->dbprefix('design_view')." where design_id = ".$design_id." and user_id = ".$user_id." and DATE_FORMAT(view_date,'%Y-%m-%d') = '".$cur_date."'");

		if($query->num_rows()>0)
		{
			 return 1;
		}
		else
		{
			return 0;
		}
	}

function chk_video_track_view($video_id,$user_id)
	{
		$cur_date = date('Y-m-d');
		
		$query = $this->db->query("select * from ".$this->db->dbprefix('video_view')." where video_id = ".$video_id." and user_id = ".$user_id." and DATE_FORMAT(view_date,'%Y-%m-%d') = '".$cur_date."'");

		if($query->num_rows()>0)
		{
			 return 1;
		}
		else
		{
			return 0;
		}
	}


	function set_video_view($video_id)
	{
		$user_id=0;
		$chk_video_track_view  = 1;
		if(get_authenticateUserID() > 0)
		{
			$user_id = get_authenticateUserID();
		}
		if($user_id > 0 )
		{
			$chk_video_track_view = $this->chk_video_track_view($video_id,$user_id);
		}
		if($chk_video_track_view == 0)
		{
			
			$sql_res = "update ".$this->db->dbprefix('video')." set `video_view_count` = `video_view_count` + 1 where video_id =".$video_id;
			$this->db->query($sql_res);
			
			$data = array('video_id'=>$video_id,
						  'user_id'=>$user_id,
						  'view_date'=>date('Y-m-d h:i:s'),	
						  'view_ip'=>getRealIP()
						);
			$this->db->insert('video_view',$data);
	
		}
		else
		{
			
			
			$chk_track_other_view = $this->chk_track_other_view($video_id);
			
			if($chk_track_other_view  == 0)
			{
			$sql_res = "update ".$this->db->dbprefix('video')." set `video_view_count` = `video_view_count` + 1 where video_id =".$video_id;
			$this->db->query($sql_res);
			
			$data = array('video_id'=>$video_id,
						  'user_id'=>0,
						  'view_date'=>date('Y-m-d h:i:s'),	
						  'view_ip'=>getRealIP()
						);
			$this->db->insert('video_view',$data);
			}
		}
		
	}	
	
	function check_like_track($user_id =0,$video_id = 0)
	{

		$query = $this->db->query("select * from ".$this->db->dbprefix('video_like')." where video_id = ".$video_id." and user_id = ".$user_id);

		if($query->num_rows()>0)
		{
			 return $query->row();
		}
		else
		{
			return 0;
		}
	}
	
	function chk_track_other_view($video_id)
	{
		$cur_date = date('Y-m-d');
		
		$query = $this->db->query("select * from ".$this->db->dbprefix('video_view')." where video_id = ".$video_id." and view_ip = '".getRealIP()."' and DATE_FORMAT(view_date,'%Y-%m-%d') = '".$cur_date."'");

		if($query->num_rows()>0)
		{
			 return 1;
		}
		else
		{
			return 0;
		}
	}

	
	function check_like_status($like_status,$user_id,$video_id)
	{
		$query = $this->db->query("select * from ".$this->db->dbprefix('video_like')." where video_id = ".$video_id." and like_status = ".$like_status." and user_id = ".$user_id);

		if($query->num_rows()>0)
		{
			 return $query->row();
		}
		else
		{
			return 0;
		}
	}
	
	function set_like_dislike($like_status=0,$video_id = 0)
	{
		
		//$chk_track_like  = 1;
		//like
		
		$chk_like = $this->check_like_track(get_authenticateUserID(),$video_id);
		
		
		/// new code//
		if($chk_like)
		{
			 $data = array('video_id'=>$video_id,
						  'user_id'=>get_authenticateUserID(),
						  'like_date'=>date('Y-m-d h:i:s'),	
						  'like_status'=>$like_status,
						  'like_ip'=>getRealIP()
						);
				$this->db->where(array('user_id'=>get_authenticateUserID(),'video_id'=>$video_id));		
				$this->db->update('video_like',$data);
				
				
			 if($like_status == 1)
			 {
				  $sql_res = "update ".$this->db->dbprefix('video')." set `video_like_count` = `video_like_count` + 1 where video_id =".$video_id;
			 }
			 else
			 {
				   $sql_res = "update ".$this->db->dbprefix('video')." set `video_like_count` = `video_like_count` - 1 where video_id =".$video_id;
			 }  
			 
			 $this->db->query($sql_res);	
		}
		
		else
		{
			
		     $data = array('video_id'=>$video_id,
						  'user_id'=>get_authenticateUserID(),
						  'like_date'=>date('Y-m-d h:i:s'),	
						  'like_status'=>$like_status,
						  'like_ip'=>getRealIP()
						);
			$this->db->insert('video_like',$data);
			$sql_res = "update ".$this->db->dbprefix('video')." set `video_like_count` = `video_like_count` + 1 where video_id =".$video_id;
			$this->db->query($sql_res);	
		}
		// end of new code//
		
	}
	
	
	
}
?>