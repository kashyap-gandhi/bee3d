<?php
class Home_model extends IWEB_Model 
{

	/*
	Function name :Home_model
	Description :its default constuctor which called when Home_model object initialzie.its load necesary parent constructor
	*/
	function Home_model()
    {
        parent::__construct();	
    } 
	
	
	/*
	Function name :usernameTaken()
	Parameter : $user_name (profile name)
	Return : boolen
	Use : check the user unique profile name
	*/
	
	function usernameTaken($user_name)
	{
		if(get_authenticateUserID()!=''){
			$query = $this->db->query("select * from ".$this->db->dbprefix('user')." where profile_name='".$user_name."' and user_id!='".get_authenticateUserID()."'");
		} else {
			$query = $this->db->query("select * from ".$this->db->dbprefix('user')." where profile_name='".$user_name."'");
		}		 
		 
		 if($query->num_rows()>0)
		 {
			return true;
		 }
		 else 
		 {
			return false;
		 }		
	}
	

	/*
	Function name :requestemailTaken()
	Parameter : $email (email address)
	Return : boolen
	Use : check the user unique email address
	*/
	
	function requestemailTaken($email)
	{
	
		 $query = $this->db->query("select * from ".$this->db->dbprefix('user')." where email='".$email."'");
		 
		 if($query->num_rows()>0)
		 {
			return true;
		 }
		 else 
		 {
		 	 $query1 = $this->db->query("select * from ".$this->db->dbprefix('user_request')." where request_email='".$email."'");
		 
			 if($query1->num_rows()>0)
			 {
				return true;
			 }
			 else
			 {
				return false;
			}
		 }		
	}
	
	
	/*
	Function name :emailTaken()
	Parameter : $email (email address)
	Return : boolen
	Use : check the user unique email address
	*/
	
	function emailTaken($email)
	{
		
		if(get_authenticateUserID()!=''){
			 $query = $this->db->query("select * from ".$this->db->dbprefix('user')." where email='".$email."' and user_id!='".get_authenticateUserID()."'");
		} else {
			 $query = $this->db->query("select * from ".$this->db->dbprefix('user')." where email='".$email."'");
		}
		
		 
		 if($query->num_rows()>0)
		 {
			return true;
		 }
		 else 
		 {
			return false;
		 }		
	}
	
	
	

	
	/*
	Function name :check_spam_register()
	Parameter : none
	Return : 1 or 0
	Use : check new register user ip address for spamming
	*/
	
	function check_spam_register()
	{
		$spam_control=$this->db->query("select * from ".$this->db->dbprefix('spam_control')." ");
		$control=$spam_control->row();
		
		$total_register=$control->total_register;
		$register_expire=date('Y-m-d', strtotime('+'.$control->register_expire.' days'));
		
		
		$chk_spam=$this->db->query("select * from ".$this->db->dbprefix('user')." where sign_up_ip='".$this->input->ip_address()."' and DATE(sign_up_date)='".date('Y-m-d')."'");
		
		if($chk_spam->num_rows()>0)
		{	
			$total_posted_register=$chk_spam->num_rows();
			
			if($total_posted_register>=$total_register)
			{
								
				$make_spam=$this->db->query("insert into ".$this->db->dbprefix('spam_ip')."(`spam_ip`,`start_date`,`end_date`)values('".$this->input->ip_address()."','".date('Y-m-d')."','".$register_expire."')");
						
				return 1;				
			}
			else
			{
				return 0;
			}
		
		}
		
		return 0;		
	
	}	

	
	/*
	Function name :check_spam_inquiry()
	Parameter : none
	Return : 1 or 0
	Use : check the inquiry user ip address for spamming
	*/
	
	function check_spam_inquiry()
	{
		$spam_control=$this->db->query("select * from ".$this->db->dbprefix('spam_control')."");
		$control=$spam_control->row();
		
		$total_contact=$control->total_contact;
		$contact_expire=date('Y-m-d', strtotime('+'.$control->contact_expire.' days'));
		
		
		$chk_spam=$this->db->query("select * from ".$this->db->dbprefix('spam_inquiry')." where inquire_spam_ip='".getRealIP()."' and inquire_date='".date('Y-m-d')."'");
		
		if($chk_spam->num_rows()>0)
		{	
			$total_posted_inquire=$chk_spam->num_rows();
			
			if($total_posted_inquire>=$total_contact)
			{
								
				$make_spam=$this->db->query("insert into ".$this->db->dbprefix('spam_ip')."(`spam_ip`,`start_date`,`end_date`)values('".getRealIP()."','".date('Y-m-d')."','".$contact_expire."')");
				
				$delete_inquiry=$this->db->query("delete from ".$this->db->dbprefix('spam_inquiry')." where inquire_spam_ip='".getRealIP()."'");
						
				return 1;				
			}
			else
			{
				return 0;
			}
		
		}
		
		return 0;		
	
	}		
	
	
	
	/*
	Function name :insert_inquiry()
	Parameter : none
	Return : none
	Use : add inquire user ip address for spamming protection
	*/
	function insert_inquiry()
	{
		$query=$this->db->query("insert into ".$this->db->dbprefix('spam_inquiry')."(`inquire_spam_ip`,`inquire_date`)values('".$this->input->ip_address()."','".date('Y-m-d')."')");
	}
	
	
	
	
	/*
	Function name :is_login()
	Parameter : none
	Return : 1 or 0
	Use :  check user login information
	*/
	
	function is_login()
	{
		$this->load->helper('cookie');
		$username = $this->input->post('login_email');
		$password = $this->input->post('login_password');
		
	$query = $this->db->query("select * from ".$this->db->dbprefix('user')." where email='".$username."' and  password='".md5($password)."' ");
		
		if($query->num_rows() > 0)
		{
			$user = $query->row();
			
			
			if($user->user_status==0 && $user->verify_email==0)
			{
				return 4;
			}
			
			
			if($user->user_status==0)
			{
				return 3;
			}
			
			
			
			if($user->verify_email==0)
			{
				return 2;
			}
			else
			{
			
			$data1=array(
					'user_id'=>$user->user_id,
					'login_date_time'=> date('Y-m-d H:i:s'),
					'login_ip'=>getRealIP()
					); 
			$this->db->insert('user_login',$data1);
			
			$login_id=mysql_insert_id();
			
			
			
							$temp=get_cookie('tripdetail');
							if(!empty($temp))
							{
								
								$trip_id=$temp;
								if($trip_id)
								{ 
									
									$data['update_trip'] = $this->trip_model->update_cookie_trip($user->user_id,$trip_id);
									
								}
								
							}
			
			
			
			////===load cache driver===
		$login_details='';
		
		$supported_cache=check_supported_cache_driver();	
			
		$this->load->driver('cache');
		
		if(isset($supported_cache))
		{
			if($supported_cache!='' && $supported_cache!='none')
			{
				
				$get_user_login=$this->db->query("select * from ".$this->db->dbprefix('user_login')." where login_id='".$login_id."'");
		
					if($get_user_login->num_rows()>0)
					{					
						$login_details=$get_user_login->row();
						$this->cache->$supported_cache->save('user_login'.$user->user_id, $login_details,CACHE_VALID_SEC);								
					}			
				
			}
		}
		
				////===load cache driver===
				
				
			
			if($this->input->post('remember')=="1")
			{
				set_cookie(true);
			}else{
				set_cookie(false);
			}
			
			$data=array(
					'user_id' => $user->user_id,
					'full_name' => $user->full_name,
					'first_name'=> $user->first_name,
					'profile_name' => $user->profile_name,
					'email'=>$user->email,
					);
			$this->session->set_userdata($data);
			
	
			
			
									
			return 1;			
			
			}
			
		}
		else{
			return 0;
		}		
	}
	
	
	/*
	Function name :check_email()
	Parameter : none
	Return : 1 or 0
	Use :  check user forget password request 
	*/
	
	function check_email()
	{
		$email = $this->input->post('forget_email');
		$query = $this->db->query("select * from ".$this->db->dbprefix('user')." where email='".$email."' or profile_name='".$email."'");
		//echo $query->num_rows(); die();
		if($query->num_rows()>0)
		{
			$row = $query->row();			
			
			$forget_password_code=randomCode();
				
				
		
			if($row->email != "")
			{
			
				
				
				$username =$row->full_name;
				$user_id = $row->user_id;
				$email = $row->email;
				
				
				$update_data=array(
				'forget_password_code'=>$forget_password_code,
				'forget_password_request'=>1				
				);	
				
				$this->db->where('user_id',$user_id);
				$this->db->update('user',$update_data);
			
			
				$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Forgot Password'");
				$email_temp=$email_template->row();				
			
				$email_from_name=$email_temp->from_name;

				$email_address_from=$email_temp->from_address;
				$email_address_reply=$email_temp->reply_address;
				
				$email_subject=$email_temp->subject;				
				$email_message=$email_temp->message;
				
				
				
				$email_to =$email;
				
				
				//$reset_password_link=base_url().'reset_password/'.$user_id.'/'.$forget_password_code;
				    $reset_link = "<a href='".base_url()."reset_password/".$user_id."/".$forget_password_code."'>here</a>";
				//$reset_password_link=site_url('signin/forgotpassword/'.$forget_password_code.'/'.$user_id);
				
				$email_message=str_replace('{break}','<br/>',$email_message);
				$email_message=str_replace('{username}',$username,$email_message);
				$email_message=str_replace('{email}',$email,$email_message);
				$email_message=str_replace('{reset_link}',$reset_link,$email_message);
				$email_temp_url=base_url().'email/';
				$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
				$str=$email_message;
				
				/** custom_helper email function **/
					//echo $str; die;				
				email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
				
					
							
				return '1';
				
			}
			else
			{
				return '0';
			}
		}
		else
		{
			return '0';
		}
		
	}
	
	function check_active_profile_request($user_id,$code)
	{
		$query=$this->db->get_where('user',array('user_id'=>$user_id,'email_verification_code'=>$code,'verify_email'=>0));
		
		if($query->num_rows()>0)
		{
			$user_info = $query->row();
			
			
			$data=array(
			'verify_email'=>1,
			'user_status'=>1,
			'is_invite'=>1			
			);
			$this->db->where('user_id',$user_id);
			$this->db->update('user',$data);
			
			$data_agent=array(
			'agent_status'=>1,
			'agent_app_approved'=>1		
			);
			$this->db->where('user_id',$user_id);
			$this->db->update('agent_profile',$data_agent);
			
			
			
			$data1=array(
			'user_id'=>$user_id,
			'login_date_time'=> date('Y-m-d H:i:s'),
			'login_ip'=>getRealIP()
			); 
			$this->db->insert('user_login',$data1);
			
			$login_id=mysql_insert_id();
			
			
			
			////===load cache driver===
			$login_details='';
			
			$supported_cache=check_supported_cache_driver();	
				
			$this->load->driver('cache');
			
			if(isset($supported_cache))
			{
				if($supported_cache!='' && $supported_cache!='none')
				{
					
					$get_user_login=$this->db->query("select * from ".$this->db->dbprefix('user_login')." where login_id='".$login_id."'");
			
						if($get_user_login->num_rows()>0)
						{					
							$login_details=$get_user_login->row();
							$this->cache->$supported_cache->save('user_login'.$user->user_id, $login_details,CACHE_VALID_SEC);								
						}			
					
				}
			}
		
			////===load cache driver===

				
					$sess_data=array(
					'user_id' => $user_info->user_id,
					'full_name' => $user_info->full_name,
					'first_name'=> $user_info->first_name,
					'profile_name' => $user_info->profile_name,
					'email'=>$user_info->email,
					);
					$this->session->set_userdata($sess_data);
						
			
			return $user_info->profile_name;
			
		} else {
		
			return 'none';		
		}
		
	}
	
	
	function check_preview_profile_request($user_id,$code)
	{
		
		$query=$this->db->get_where('user',array('user_id'=>$user_id,'email_verification_code'=>$code,'verify_email'=>0));
		
		if($query->num_rows()>0)
		{
			$user_info = $query->row();
			
				$sess_data=array(
				'email_verification_code' => $user_info->email_verification_code,
				'email_invite_user_id'=>$user_id
				);
				$this->session->set_userdata($sess_data);
						
			
			return $user_info->profile_name;
			
		} else {
		
			return 'none';		
		}
	
	}
	
	
	
	
	function check_valid_request($code,$user_id)
	{
		
		$query=$this->db->get_where('user',array('user_id'=>$user_id,'forget_password_code'=>$code,'forget_password_request'=>1));
		
		if($query->num_rows()>0)
		{			
			return true;
			
		} else {
		
			return false;		
		}
	
	}
	
	/*
	Function name :check_valid_request()
	Parameter : $user_id (user_id), $code (code)
	Return : boolen
	Use :  reset password
	*/
	function reset_password($code,$user_id)
	{
			
			$data=array(
			'forget_password_request'=>0,
			'forget_password_code'=>'',
			'password'=>md5($this->input->post('new_password'))			
			);
			
			
			$this->db->where('user_id',$user_id);
			$this->db->update('user',$data);
			
			
			$userinfo  = get_user_profile_by_id($user_id);
			
			///////// change password mail//////////////
			$email_template = $this->db->query("select * from " . $this->db->dbprefix('email_template') . " where task='change password'");
			$email_temp = $email_template->row();

			$email_address_from = $email_temp->from_address;
			$email_address_reply = $email_temp->reply_address;

			$email_subject = $email_temp->subject;
			$email_message = $email_temp->message;

		
           $login_link = "<a href='".base_url()."home/login/'>click here</a>";


			
			

			$email_to = $userinfo->email;

			$email_message = str_replace('{break}', '<br/>', $email_message);
			$email_message = str_replace('{user_name}', $userinfo->full_name, $email_message);
			$email_message = str_replace('{email}', $userinfo->email, $email_message);
			$email_message = str_replace('{password}', $this->input->post('new_password'), $email_message);
			$email_message = str_replace('{login_link}', $login_link, $email_message);

			 $str = $email_message;
			
			/** custom_helper email function **/

			email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);
	

			/////// end of change password mail////////////////
			
			return true;
	}
	
	
	/*
	Function name :register()
	Parameter : none
	Return : 1
	Use :  user sign up or register
	*/
	
	function register_user()
	{	
		
		$email_verification_code=randomCode();
		
		$verify_email=0;
		
		$user_setting=user_setting();
		
		$google='';
		$yahoo='';
		
		$user_status=$user_setting->sign_up_auto_active;
		
		$user_status=0;
		
		
		
		///=================track ip =========
		/* $continent_code='';
		 $country_code='';
		 $country_code3='';
		 $country_name='';
		 $region='';
		 $city='';
		 $postal_code='';
		 $latitude='';
		 $longitude='';
		 $dma_code='';
		 $area_code='';
		 
		$get_visitor_loc=getip2location(getRealIP());	
		if($get_visitor_loc) {
			if(isset($get_visitor_loc->response)) {
				
			$vislog=$get_visitor_loc->response;
				
			if(isset($vislog->continent_code)) { if($vislog->continent_code!='') {  $continent_code=$vislog->continent_code;  } } 
			if(isset($vislog->country_code)) { if($vislog->country_code!='') {  $country_code=$vislog->country_code;  } }
			if(isset($vislog->country_code3)) { if($vislog->country_code3!='') {  $country_code3=$vislog->country_code3;  } }
			if(isset($vislog->country_name)) { if($vislog->country_name!='') {  $country_name=$vislog->country_name;  } }
			if(isset($vislog->region)) { if($vislog->region!='') {  $region=$vislog->region;  } }
			if(isset($vislog->city)) { if($vislog->city!='') {  $city=$vislog->city;  } }
			if(isset($vislog->postal_code)) { if($vislog->postal_code!='') {  $postal_code=$vislog->postal_code;  } }
			if(isset($vislog->latitude)) { if($vislog->latitude!='') {  $latitude=$vislog->latitude;  } }
			if(isset($vislog->longitude)) { if($vislog->longitude!='') {  $longitude=$vislog->longitude;  } }
			if(isset($vislog->dma_code)) { if($vislog->dma_code!='') {  $dma_code=$vislog->dma_code;  } }
			if(isset($vislog->area_code)) { if($vislog->area_code!='') {  $area_code=$vislog->area_code;  } }	 
								
			}
		}
		
		
		'continent_code'=>$continent_code,
		'country_code'=>$country_code,
		'country_code3'=>$country_code3,
		'country_name'=>$country_name,
		'region'=>$region,
		'city'=>$city,
		'postal_code'=>$postal_code,
		'latitude'=>$latitude,
		'longitude'=>$longitude,
		'dma_code'=>$dma_code,
		'area_code'=>$area_code
		*/
		
		///=================track ip =========
		
		
		
		$password= md5($this->input->post('password'));		
		
		if($this->input->post('fb_id') || $this->input->post('tw_id'))
		{
			$user_status=1;
			$password= '';
			$verify_email=1;
		}
		
		
		if($this->input->post('yahoo')==1)
		{
			$user_status=1;
			$verify_email=1;
			$yahoo=1;
		}
		if($this->input->post('google')==1)
		{
			$user_status=1;
			$verify_email=1;
			$google=1;
		}
		
		
		$full_name=$this->input->post('firstname').' '.$this->input->post('lastname');
		$firstname=$this->input->post('firstname');
		$lastname=$this->input->post('lastname');
		
		
		
		/*$profile_name =$this->input->post('firstname').' '.$this->input->post('lastname');
		$profile_name=clean_url($profile_name);
			
	    $chk_url_exists=$this->db->query("select MAX(profile_name) as profile_name from ".$this->db->dbprefix('user')." where profile_name like '".$profile_name."%'");
		
		if($chk_url_exists->num_rows()>0)
		{			
				$get_pr=$chk_url_exists->row();					
				
				$strre='0';
				if($get_pr->profile_name!='')
				{
					$strre=str_replace($profile_name,'',$get_pr->profile_name);
				}
				
				if($strre=='0') 
				{					
					$newcnt=''; 
					  				
				} 
				elseif($strre=='') 
				{					
					$newcnt='1';   				
				}
				else
				{				
					$newcnt=(int)$strre+1;			
				}
				
			 	$profile_name=$profile_name.$newcnt;
						
		}*/
		
		
		
		$unique_code=unique_user_code(getrandomCode(12));
		$reference_user_id='';
		$invite_code='';
	
		if($this->input->post('invite_code')!='')
		{	
			$invite_code=$this->input->post('invite_code');	
		}
		else
		{	
			$this->load->helper('cookie');
			$referral_cookie=get_cookie('invite_code',TRUE);
			
			if(isset($referral_cookie))
			{
				$invite_code=$referral_cookie;	
			}
			else
			{	
				if(isset($_COOKIE['invite_code']))
				{
					$invite_code=$_COOKIE['invite_code'];
				}
				elseif(isset($HTTP_COOKIE_VARS['invite_code']))
				{
					$invite_code=$HTTP_COOKIE_VARS['invite_code'];
				}	
			}	
			}
			if($reference_user_id=='' || $reference_user_id==0)
			{
				if($this->session->userdata('invite_code')!='')
				{	
					$invite_code=$this->session->userdata('invite_code');	
				}	
			}
		///
		if($invite_code!='')
		{
			$check_reference_user=$this->db->get_where('user',array('unique_code'=>$invite_code));
			if($check_reference_user->num_rows()>0)
			{
				$get_reference_user=$check_reference_user->row();
				$reference_user_id=$get_reference_user->user_id;	
			}
		}	
		////



		
		
		$data = array(		
				'full_name' => $full_name,		
				'first_name' => $firstname,
				'last_name' => $lastname,	
				'profile_name'=>$this->input->post('profilename'),			
				'email' => $this->input->post('email'),			
				'password' => $password,	
				'sign_up_ip' => getRealIP(),
				'email_verification_code'=>$email_verification_code,
				'verify_email'=>$verify_email,					
				'sign_up_date' => date('Y-m-d H:i:s'),
				'fb_id' => $this->input->post('fb_id'),		
				'tw_id' => $this->input->post('tw_id'),		
				'twitter_screen_name' => $this->input->post('tw_screen_name'),	
				'tw_oauth_token'=>$this->input->post('oauth_token'),
				'tw_oauth_token_secret'=>$this->input->post('oauth_token_secret'),
				'request_code' => $this->input->post('invite_code'),
				'yahoo'=>$yahoo,
				'google'=>$google,
				'user_status'=>$user_status,
				'unique_code'=>$unique_code,
				'reference_user_id'=>$reference_user_id			
				); 
				
		//echo '<pre>'; print_r($data); die();
		$this->db->insert('user', $data);
		$user_id = mysql_insert_id();
		
		/*Thisid is user for resend request to user in thank you page*/
		$resend_user_id = $user_id;
		
	
		
		/*****create profile****/
		$image = '';
		
		if($_FILES) 
			{
			
				if($_FILES['file1']['name']!="")
				{
					$image = $this->upload->file_name;
				}
			}
			$active=0;
			if($this->input->post('fb_img')){$image=$this->input->post('fb_img');$active=1;}
			if($this->input->post('twiter_img')){$image=$this->input->post('twiter_img');$active=1;}
			
			
		
		$data_profile=array(
			'user_id'=>$user_id,
			'profile_image' =>$image
		);

		$this->db->insert('user_profile',$data_profile);
		

		
		
			/*** user notification ****/
	
		$user_notification=mysql_query("SHOW COLUMNS FROM ".$this->db->dbprefix('user_notification'));
		$res=mysql_fetch_array($user_notification);
		$fields="";
		$values="";
				
		while($res=mysql_fetch_array($user_notification)){
		//print_r($res['Field']);echo '<br>';
		if($fields==""){$fields.="(`".$res['Field']."`"; $values.="('".$user_id."'";}
		else {$fields.=",`".$res['Field']."`";	 $values.=",'1'";}
		}
		$fields.=")";
		 $values.=")";								   
		$insert_val= $fields.' values '.$values;
		
		$this->db->query("insert into ".$this->db->dbprefix('user_notification')." ".$insert_val."");
		
	
		/*******************/
	
		
					
			
		return $resend_user_id;
		
		
	}
	
	
	function register_agent()
	{
			
		
		$email_verification_code=randomCode();
		
		$verify_email=0;
		
		$user_setting=user_setting();
		
		$google='';
		$yahoo='';
		
		$user_status=$user_setting->sign_up_auto_active;
		
		$user_status=0;
		
		
		
		///=================track ip =========
		/* $continent_code='';
		 $country_code='';
		 $country_code3='';
		 $country_name='';
		 $region='';
		 $city='';
		 $postal_code='';
		 $latitude='';
		 $longitude='';
		 $dma_code='';
		 $area_code='';
		 
		$get_visitor_loc=getip2location(getRealIP());	
		if($get_visitor_loc) {
			if(isset($get_visitor_loc->response)) {
				
			$vislog=$get_visitor_loc->response;
				
			if(isset($vislog->continent_code)) { if($vislog->continent_code!='') {  $continent_code=$vislog->continent_code;  } } 
			if(isset($vislog->country_code)) { if($vislog->country_code!='') {  $country_code=$vislog->country_code;  } }
			if(isset($vislog->country_code3)) { if($vislog->country_code3!='') {  $country_code3=$vislog->country_code3;  } }
			if(isset($vislog->country_name)) { if($vislog->country_name!='') {  $country_name=$vislog->country_name;  } }
			if(isset($vislog->region)) { if($vislog->region!='') {  $region=$vislog->region;  } }
			if(isset($vislog->city)) { if($vislog->city!='') {  $city=$vislog->city;  } }
			if(isset($vislog->postal_code)) { if($vislog->postal_code!='') {  $postal_code=$vislog->postal_code;  } }
			if(isset($vislog->latitude)) { if($vislog->latitude!='') {  $latitude=$vislog->latitude;  } }
			if(isset($vislog->longitude)) { if($vislog->longitude!='') {  $longitude=$vislog->longitude;  } }
			if(isset($vislog->dma_code)) { if($vislog->dma_code!='') {  $dma_code=$vislog->dma_code;  } }
			if(isset($vislog->area_code)) { if($vislog->area_code!='') {  $area_code=$vislog->area_code;  } }	 
								
			}
		}
		
		
		'continent_code'=>$continent_code,
		'country_code'=>$country_code,
		'country_code3'=>$country_code3,
		'country_name'=>$country_name,
		'region'=>$region,
		'city'=>$city,
		'postal_code'=>$postal_code,
		'latitude'=>$latitude,
		'longitude'=>$longitude,
		'dma_code'=>$dma_code,
		'area_code'=>$area_code
		*/
		
		///=================track ip =========
		
		
		
		$password= md5($this->input->post('agentpassword'));		
		
		if($this->input->post('fb_id') || $this->input->post('tw_id'))
		{
			$user_status=1;
			$password= '';
			$verify_email=1;
		}
		
		
		if($this->input->post('yahoo')==1)
		{
			$user_status=1;
			$verify_email=1;
			$yahoo=1;
		}
		if($this->input->post('google')==1)
		{
			$user_status=1;
			$verify_email=1;
			$google=1;
		}
		
		
		$full_name=$this->input->post('agentfirstname').' '.$this->input->post('agentlastname');
		$firstname=$this->input->post('agentfirstname');
		$lastname=$this->input->post('agentlastname');
		
		
		//$profile_name =$this->input->post('agentfirstname').' '.$this->input->post('agentfirstname');
		
		$profile_name =$this->input->post('agencyname');
		$profile_name=clean_url($profile_name);
		
			
	    $chk_url_exists=$this->db->query("select MAX(profile_name) as profile_name from ".$this->db->dbprefix('user')." where profile_name like '".$profile_name."%'");
		
		if($chk_url_exists->num_rows()>0)
		{			
				$get_pr=$chk_url_exists->row();					
				
				$strre='0';
				if($get_pr->profile_name!='')
				{
					$strre=str_replace($profile_name,'',$get_pr->profile_name);
				}
				
				if($strre=='0') 
				{					
					$newcnt=''; 
					  				
				} 
				elseif($strre=='') 
				{					
					$newcnt='1';   				
				}
				else
				{				
					$newcnt=(int)$strre+1;			
				}
				
			 	$profile_name=$profile_name.$newcnt;
						
		}
		
		
		
		$unique_code=unique_user_code(getrandomCode(12));
		$reference_user_id='';
		$invite_code='';
	
		if($this->input->post('invite_code')!='')
		{	
			$invite_code=$this->input->post('invite_code');	
		}
		else
		{	
			$this->load->helper('cookie');
			$referral_cookie=get_cookie('invite_code',TRUE);
			
			if(isset($referral_cookie))
			{
				$invite_code=$referral_cookie;	
			}
			else
			{	
				if(isset($_COOKIE['invite_code']))
				{
					$invite_code=$_COOKIE['invite_code'];
				}
				elseif(isset($HTTP_COOKIE_VARS['invite_code']))
				{
					$invite_code=$HTTP_COOKIE_VARS['invite_code'];
				}	
			}	
			}
			if($reference_user_id=='' || $reference_user_id==0)
			{
				if($this->session->userdata('invite_code')!='')
				{	
					$invite_code=$this->session->userdata('invite_code');	
				}	
			}
		///
		if($invite_code!='')
		{
			$check_reference_user=$this->db->get_where('user',array('unique_code'=>$invite_code));
			if($check_reference_user->num_rows()>0)
			{
				$get_reference_user=$check_reference_user->row();
				$reference_user_id=$get_reference_user->user_id;	
			}
		}	
		////

		
		
		$data_user = array(		
				'full_name' => $full_name,		
				'first_name' => $firstname,
				'last_name' => $lastname,	
				'profile_name'=>$profile_name,			
				'email' => $this->input->post('agentemail'),			
				'password' => $password,	
				'sign_up_ip' => getRealIP(),
				'email_verification_code'=>$email_verification_code,
				'verify_email'=>$verify_email,					
				'sign_up_date' => date('Y-m-d H:i:s'),
				'fb_id' => $this->input->post('fb_id'),		
				'tw_id' => $this->input->post('tw_id'),		
				'twitter_screen_name' => $this->input->post('tw_screen_name'),	
				'tw_oauth_token'=>$this->input->post('oauth_token'),
				'tw_oauth_token_secret'=>$this->input->post('oauth_token_secret'),
				'request_code' => $this->input->post('invite_code'),
				'yahoo'=>$yahoo,
				'user_status'=>$user_status,
				'google'=>$google,
				'unique_code'=>$unique_code,
				'reference_user_id'=>$reference_user_id			
				); 
				
		//echo '<pre>'; print_r($data); die();
		$this->db->insert('user', $data_user);
		$user_id = mysql_insert_id();

		/*Thisid is user for resend request to user in thank you page*/
		$resend_user_id = $user_id;
		
		
		/*****create profile****/
		$image = '';
		
		if($_FILES) 
			{
			
				if($_FILES['file1']['name']!="")
				{
					$image = $this->upload->file_name;
				}
			}
			$active=0;
			if($this->input->post('fb_img')){$image=$this->input->post('fb_img');$active=1;}
			if($this->input->post('twiter_img')){$image=$this->input->post('twiter_img');$active=1;}
			
			
		
		$data_profile=array(
			'user_id'=>$user_id,
			'profile_image' =>$image
		);

		$this->db->insert('user_profile',$data_profile);
			/*** user notification ****/
	
		$user_notification=mysql_query("SHOW COLUMNS FROM ".$this->db->dbprefix('user_notification'));
		$res=mysql_fetch_array($user_notification);
		$fields="";
		$values="";
				
		while($res=mysql_fetch_array($user_notification)){
		//print_r($res['Field']);echo '<br>';
		if($fields==""){$fields.="(`".$res['Field']."`"; $values.="('".$user_id."'";}
		else {$fields.=",`".$res['Field']."`";	 $values.=",'1'";}
		}
		$fields.=")";
		 $values.=")";								   
		$insert_val= $fields.' values '.$values;
		
		$this->db->query("insert into ".$this->db->dbprefix('user_notification')." ".$insert_val."");
		
	
		/*******************/
	
		
		
		
		
		$memberassociate = $this->input->post('memberassociate');
		if($memberassociate)
		{
			$memberassociate = implode(',',$this->input->post('memberassociate'));
		}
		else
		{
			$memberassociate = '';
		}
		
		$agency_location = explode(',',$this->input->post('agency_location'));
		
		$agency_city='';
		$agency_state='';
		$agency_country='';
		
		if(isset($agency_location[0]))
		{
			$agency_city=$agency_location[0];
		}
		if(isset($agency_location[1]))
		{
			$agency_state=$agency_location[1];
		}
		if(isset($agency_location[2]))
		{
			$agency_country=$agency_location[2];
		}
		
		
		$data_agent = array(
				'user_id'=>$user_id,	
				'agency_name'=>$this->input->post('agencyname'),		
				'contact_person_name'=>$this->input->post('contactperson'),
				'agency_location'=>$this->input->post('agency_location'),	
				'agency_city'=>$agency_city,
				'agency_state'=>$agency_state,
				'agent_status'=>0,
				'agent_app_approved'=>0,
				'agency_country'=>$agency_country,
				'agency_contact_email'=>$this->input->post('agentemail'),
				'agency_phone'=>$this->input->post('agency_phone'),
				'agency_customer_support_no'=>$this->input->post('agency_customer_support_no'),
				'agency_website'=>seturl($this->input->post('agency_website')),
				'agent_member_of_association'=>$memberassociate,
				'agent_date'=>date('Y-m-d H:i:s'),
				'agent_ip'=>getRealIP(),
				); 
				
		//echo '<pre>'; print_r($data); die();
		$this->db->insert('agent_profile', $data_agent);
		$user_id = mysql_insert_id();
		
		//echo $user_id; die();
		
	
		$data_reset_captcha=array('register34Agent'=>'');
		
		$this->session->set_userdata($data_reset_captcha);
		
					
			
		return $resend_user_id;
		
		
	
	}
	
	
	/*
	Function name :verify_user()
	Parameter : $user_id (user id), $code (email verification code)
	Return : boolen
	Use :  check email account verification
	*/
	function verify_user($user_id,$code)
	{
		
		$query=$this->db->get_where('user',array('user_id'=>$user_id,'email_verification_code'=>$code,'verify_email'=>0));
		
		if($query->num_rows()>0)
		{
			
			$data=array(
			'verify_email'=>1
			);
			$this->db->where('user_id',$user_id);
			$this->db->update('user',$data);
			
			return true;
			
		} else {
		
			return false;		
		}
		
	}
	
	
	
	
	/////////////////============facebook============
   
   /*
	Function name :add_facebook()
	Parameter : $fb_uid (facebook id)
	Return : none
	Use :  update facebook user details
	*/
	function add_facebook($fb_uid)
	{
		$data=array(
			'fb_id'=>$fb_uid
		);
		
		$this->db->where('user_id',get_authenticateUserID());
		$this->db->update('user',$data);  
	}
	
   /*
	Function name :remove_fb()
	Parameter : none
	Return : none
	Use :  remove facebook connection
	*/
   function remove_fb()
   {
   		$data=array(
			'fb_id'=>''
		);
		
   		$this->db->where('user_id',get_authenticateUserID());
   		$this->db->update('user',$data);   
   }
   
	/*
	Function name :validate_user_facebook()
	Parameter : $uid (user id), $email (email id)
	Return : boolean or 2
	Use :  check facebook details validate or not
	*/
	
	function validate_user_facebook($uid = 0,$email='') {
		//confirm that facebook session data is still valid and matches
		$this->load->library('fb_connect');
		
   		//see if the facebook session is valid and the user id in the sesison is equal to the user_id you want to validate
		//$session_uid = $this->fb_connect->fbSession['uid'];
		if(!$this->fb_connect->fbSession) {
   	  		return false;
		}
        
   	  	//Receive Data
      	 $this->user_id    = $uid;

      
	  if($email!=''){
	  
		 $query = $this->db->get_where('user',array('email'=>$email,'user_status'=>'1'));
		
		if($query->num_rows() > 0)
		{
			$this->db->query("Update ".$this->db->dbprefix('user')." set fb_id='".$this->user_id."' where email='".$email."' and user_status='1'");
			$user = $query->row();
			
			$data1=array(
					'user_id'=>$user->user_id,
					'login_date_time'=> date('Y-m-d H:i:s'),
					'login_ip'=>$this->input->ip_address()
					); 
			$this->db->insert('user_login',$data1);
					
			
					
			
			
			$data=array(
					'user_id' => $user->user_id,
					'profile_name' => $user->profile_name,
					'full_name' => $user->full_name,
					'first_name'=> $user->first_name,
					'facebook_id' => $this->user_id,
					'email'=>$user->email,	
					);
			$this->session->set_userdata($data);

			
			return "2";			
		}	
		else{
				
		  //See if User exists     
		 
			 $query = $this->db->get_where('user',array('fb_id'=>$this->user_id,'user_status'=>'1'));
		
			if($query->num_rows() > 0)
			{
			$user = $query->row();
			
			
			
			
			
			$data1=array(
					'user_id'=>$user->user_id,
					'login_date_time'=> date('Y-m-d H:i:s'),
					'login_ip'=>$this->input->ip_address()
					); 
			$this->db->insert('user_login',$data1);		

			
			$data=array(
					'user_id' => $user->user_id,
					'profile_name'=> $user->profile_name,
					'full_name' => $user->full_name,
					'first_name'=> $user->first_name,
					'facebook_id' => $this->user_id,
					'email'=>$user->email,	
					);
			$this->session->set_userdata($data);

					
						
			return "2";			
		
		
		
		 
      }

    	  //no user exists
    	  return false;
	  
	  
		}
	
	  }
	  else{
	  
	 	 //See if User exists    
		 
		$query = $this->db->getwhere('user',array('fb_id'=>$this->user_id,'user_status'=>'1'));
		
		if($query->num_rows() > 0)
		{
			$user = $query->row();
			
			
			
			
			
			$data1=array(
					'user_id'=>$user->user_id,
					'login_date_time'=> date('Y-m-d H:i:s'),
					'login_ip'=>$this->input->ip_address()
					); 
			$this->db->insert('user_login',$data1);
			
			
			
					
			$data=array(
					'user_id' => $user->user_id,
					'profile_name'=> $user->profile_name,
					'full_name' => $user->full_name,
					'first_name'=> $user->first_name,
					'facebook_id' => $this->user_id,
					'email'=>$user->email,	
					);
			$this->session->set_userdata($data);

					
						
			return "2";			
		}	
		
		
      //no user exists
      return false;
	  
	  }
   }
   
   
   	/*
	Function name :get_user_by_fb_uid()
	Parameter : $fb_id (facebook id), $email (email id)
	Return : array of user details
	Use :  get array of user details
	*/
    function get_user_by_fb_uid($fb_id = 0,$email='') {
	
	   	//returns the facebook user as an array.
	   		$sql = " SELECT * FROM ".$this->db->dbprefix('user')." WHERE fb_id ='".$fb_id."'";
		
		if($email != ''){
			$sql = " SELECT * FROM ".$this->db->dbprefix('user')." WHERE fb_id ='".$fb_id."' or email='".$email."'";
		}
		
	
		
	   	$usr_qry = $this->db->query($sql);
		
	   	if($usr_qry->num_rows() > 0) {
		//yes, a user exists
			$user = $usr_qry->row();
			
			
			
			
			if($user->fb_id == 0){
				$data2 = array(					
						'fb_id' => $fb_id	
				);
						
				$this->db->where('email', $email);
				$this->db->update('user', $data2);
				
			}
			
		
	   		return $user;
	   	} else {
	   		// no user exists
	   		return false;
	   	}
	
	   		
   }
   
 
	
	
	
	
	///////////////============twitter ================
   
   /*
	Function name :check_twitter_exists()
	Parameter : $twitter_id (twitter id),
	Return : boolen
	Use :  check tweeter user account
	*/
   function check_twitter_exists($twitter_id)
   {
   		$query=$this->db->query("select * from ".$this->db->dbprefix('user')." where tw_id='".$twitter_id."'  ");
		
		
		
		if($query->num_rows()>0)
		{
			return true;
		}
		else
		{ 
			return false;
		}
   
   }
   
   /*
	Function name :get_twitter_user_detail()
	Parameter : $twitter_id (twitter id),
	Return : array of tweeter user details
	Use :  get tweeter user details
	*/
   function get_twitter_user_detail($twitter_id)
   {
   		$query=$this->db->query("select * from ".$this->db->dbprefix('user')." where tw_id='".$twitter_id."'  ");
		
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		
		return 0;
   
   
   }
   
	/*
	Function name :add_twitter()
	Parameter : $twitter_id (twitter id), $screen_name (tweeter screen name),$oauth_token,$oauth_token_secret
	Return : none
	Use :  update tweeter user details
	*/
	function add_twitter($twitter_id,$screen_name,$oauth_token,$oauth_token_secret)
	{
		$data=array(
			'tw_id'=>$twitter_id,
			'twitter_screen_name'=>$screen_name,
			'tw_oauth_token'=>$oauth_token,
			'tw_oauth_token_secret'=>$oauth_token_secret
		);
		
		$this->db->where('user_id',get_authenticateUserID());
		$this->db->update('user',$data);  
	}
	
	
	/*
	Function name :remove_tw()
	Parameter : none
	Return : none
	Use :  remove tweeter connection
	*/
	function remove_tw()
   {
		$data=array(
			'tw_id'=>'',
			'twitter_screen_name'=>'',
			'tw_oauth_token'=>'',
			'tw_oauth_token_secret'=>''
		);
		
		$this->db->where('user_id',get_authenticateUserID());
		$this->db->update('user',$data);   
   }
   
  
  
 
	
	function openid_register(){
	
		$email_verification_code=randomCode();
		
		$user_setting=user_setting();
		
		
		$user_status=$user_setting->sign_up_auto_active;
			
		
		if($this->input->post('fb_id') || $this->input->post('tw_id') || $this->input->post('google') || $this->input->post('yahoo'))
		{
			$user_status=1;
		}
		
		$full_name=$this->input->post('first_name').' '.$this->input->post('last_name');
		$first_name=$this->input->post('first_name');
		$last_name=$this->input->post('last_name');
		
		
		
		$profile_name =$this->input->post('first_name').' '.$this->input->post('last_name');
		
		
		
		
	
		$profile_name=clean_url($profile_name);
		$user_id = $this->input->post('user_id');
		
		$data = array(		
				'full_name' => $full_name,		
				'first_name' => $first_name,
				'last_name' => $last_name,	
				'profile_name'=>$profile_name,			
				'email' => $this->input->post('email'),				
				'sign_up_ip' => getRealIP() ,
				'email_verification_code'=>$email_verification_code,					
				'user_status' => $user_status,
				'sign_up_date' => date('Y-m-d H:i:s'),
				'fb_id' => $this->input->post('fb_id'),		
				'tw_id' => $this->input->post('tw_id'),		
				'twitter_screen_name' => $this->input->post('tw_screen_name'),	
				'tw_oauth_token'=>$this->input->post('oauth_token'),
				'tw_oauth_token_secret'=>$this->input->post('oauth_token_secret'),
				'request_code' => $this->input->post('invite_code')
				); 
				
				//echo '<pre>'; print_r($data); die();
		$this->db->where('user_id', $user_id);		
		$this->db->update('user', $data);
		
	
		
		/*****create profile****/
		$image = '';

		$data_profile=array(
			'user_id'=>$user_id,
			'profile_image' =>$image
		);

		$this->db->insert('user_profile',$data_profile);
		
	
		
			/*** user notification ****/
	
		$user_notification=mysql_query("SHOW COLUMNS FROM ".$this->db->dbprefix('user_notification'));
		$res=mysql_fetch_array($user_notification);
		$fields="";
		$values="";
				
		while($res=mysql_fetch_array($user_notification)){
		//print_r($res['Field']);echo '<br>';
		if($fields==""){$fields.="(`".$res['Field']."`"; $values.="('".$user_id."'";}
		else {$fields.=",`".$res['Field']."`";	 $values.=",'1'";}
		}
		$fields.=")";
		 $values.=")";								   
		$insert_val= $fields.' values '.$values;
		
		$this->db->query("insert into ".$this->db->dbprefix('user_notification')." ".$insert_val."");
		
	
		/*******************/
		
	
	
	
		return 1;
		
	}
	
	
 function get_exists_user_profile($email = '')
   {
           $qry = $this->db->get_where("user",array("email"=>$email));
		   
		   if($qry->num_rows()>0)
		   {
			    return $qry->row();
		   }
		   
		   return 0;
   }
   
   function get_page_content($slug='')
   {
   		  $qry = $this->db->get_where("pages",array("slug"=>$slug,'active'=>1));
		   if($qry->num_rows()>0)
		   {
			    return $qry->row();
		   }
		   return 0;
   }

function send_spam_mail()
{
		$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Spam Email'");
		$email_temp=$email_template->row();				
	
		$email_from_name=$email_temp->from_name;

		$email_address_from=$email_temp->from_address;
		$email_address_reply=$email_temp->reply_address;
		
		$email_subject=$email_temp->subject;				
		$email_message=$email_temp->message;
		
		$spam_ip = getRealIP();
		
		$email_to =SPAM_EMAIL;
		
		$email_message=str_replace('{break}','<br/>',$email_message);
		$email_message=str_replace('{spam_ip}',$spam_ip,$email_message);
		$str=$email_message;
		
		email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
				
}

   function latest_design($limit)
	{
		$this->db->select('d.*,at.attachment_name');
		$this->db->from('design d');
		$this->db->join('design_category_rel dr','d.design_id=dr.design_id');
		$this->db->join('design_category c','c.category_id=dr.category_id');
		$this->db->join('design_attachment at','at.design_id=d.design_id');
		$this->db->where('d.design_status','1');
		$this->db->where("at.attachment_is_main",'1');
		$this->db->order_by('d.design_id','desc');
		//$this->db->order_by('design_like_count','desc');
		$this->db->limit($limit);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	function latest_printer($limit)
	{
		$this->db->select('*');
		$this->db->from('store s');
		$this->db->join('store_category_rel scrl','scrl.store_id=s.store_id');
		$this->db->join('store_category sc','sc.category_id=scrl.category_id');
		$this->db->order_by('s.store_id','desc');
		$this->db->limit($limit);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
}

?>