//$(document).ready(function(){	
	
	
	var email_reg_exp= /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
	var LetNumSpec=/^[0-9a-zA-Z_-]+$/;
	var number=/^[0-9]+$/;
	var amount=/^[0-9.]+$/;
	var alpha=/^[a-zA-Z]+$/;
	var alphanum=/^[a-zA-Z0-9]+$/;
	var alphaspace=/^[a-z A-Z]+$/;
	var profilename=/^[a-z0-9]+$/;
	var LetNumSpaceSpec=/^[0-9a-z A-Z_-]+$/;
	var content_email=/^\b\w+\@\w+[\.\w+]+\b$/;
	
	var url_reg_exp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
	
	//validate_request();
	//validate_socialsignup();
	
	/* validate_signup();
	 validate_login();
	 validate_forget();
	 validate_reset();
	  validate_edit_account();
	   validate_change_passsword();
	   validate_offer_task();
	   validate_askquestion();
	   
	     validate_postmessage();*/
	   
	
	//global vars
	
	
	
$('#trademarksubmitbtn').click(function(){
	
	validate_trademarkform();
	
});
	
$('#copyrightsubmitbtn').click(function(){
	
	validate_copyrightform();
	
});

$('#requestbtn').click( function() {		
		
		 validate_request();	
 });
	
	
$('#socialsignupbtn').click( function() {		
		
		 validate_socialsignup();
 });
		
	
		
$('#sign_up').click( function() {		
	
		 validate_signup();	
 });

$('#loginbtn').click( function() {		
		
		 validate_login();	
 });
$('#forgetbtn').click( function() {		
		
		 validate_forget();	
 });	
	
$('#resetbtn').click( function() {		
		
		 validate_reset();	
 });	
	
	
$('#changepasswordbtn').click( function() {		
		
		 validate_change_passsword();	
 });	
	
	
$('#editbtn').click( function() {		
		
		 validate_edit_account();	
 });	
	


$('#offerTaskbtn').click( function() {		
		
		 validate_offer_task();	
 });	

$('#askQuestionbtn').click( function() {		
		
		 validate_askquestion();	
 });

$('#posMessagebtn').click( function() {		
		
		 validate_postmessage();	
 });



function validate_trademarkform()
{
	
	
	
		
		var form = $("#trademark_notification");	
		
		var infringing_user_name = $("#infringing_user_name");
		var infringing_user_nameInfo = $("#infringing_user_nameInfo");		
		
		var tradmark_word_or_symbol = $("#tradmark_word_or_symbol");
		var tradmark_word_or_symbolInfo = $("#tradmark_word_or_symbolInfo");
		
		
		var detail = $("#detail");
		var detailInfo = $("#detailInfo");
		
		var registration_number = $("#registration_number");
		var registration_numberInfo = $("#registration_numberInfo");
		
		var registration_office = $("#registration_office");
		var registration_officeInfo = $("#registration_officeInfo");
		
		var link_to_trademark_record = $("#link_to_trademark_record");
		var link_to_trademark_recordInfo = $("#link_to_trademark_recordInfo");
		
		
		var date_of_first_use = $("#date_of_first_use");
		var date_of_first_useInfo = $("#date_of_first_useInfo");
		
		var date_of_app = $("#date_of_app");
		var date_of_appInfo = $("#date_of_appInfo");
		
		
		var date_of_reg = $("#date_of_reg");
		var date_of_regInfo = $("#date_of_regInfo");
		
		
		var full_name = $("#full_name");
		var full_nameInfo = $("#full_nameInfo");
		
		var email = $("#email");
		var emailInfo = $("#emailInfo");
		
		
		var relation_to_trademark = $("#relation_to_trademark");
		var relation_to_trademarkInfo = $("#relation_to_trademarkInfo");
		
		
		var company_name = $("#company_name");
		var company_nameInfo = $("#company_nameInfo");
		
		
		var address = $("#address");
		var addressInfo = $("#addressInfo");
		
		var city = $("#city");
		var cityInfo = $("#cityInfo");
		
		var state = $("#state");
		var stateInfo = $("#stateInfo");
		
		var zip_code = $("#zip_code");
		var zip_codeInfo = $("#zip_codeInfo");
		
		var country = $("#country");
		var countryInfo = $("#countryInfo");
		
		var phone_no = $("#phone_no");
		var phone_noInfo = $("#phone_noInfo");
		
		
		
		var username_action = $("#username_action");
		var username_actionInfo = $("#username_actionInfo");		
		
		var condition1 = $("#condition1");
		var condition1Info = $("#condition1Info");
		
		var condition2 = $("#condition2");
		var condition2Info = $("#condition2Info");
		
		
		
		
		
		
		
		
		//On Submitting
		form.submit(function(){
			if(validateinfringing_user_name() & validatetradmark_word_or_symbol() & validatecompany_name() & validaterelation_to_trademark() & validatedate_of_reg() & validatedate_of_app() & validatedate_of_first_use() & validatelink_to_trademark_record() & validateregistration_office() & validateregistration_number() & validatedetail() & validatetradmark_word_or_symbol() &  validatefull_name() & validateaddress() & validatecity() & validatestate() & validatecountry() & validatephone_no() & validateEmail() & validateZipcode() & validateusername_action() & validatecondition1() & validatecondition2() )
				return true
			else
				return false;
		});
		
	
	
	
	
	
	
	//validation functions
	
	function validateinfringing_user_name()
	{
		
		
		var a = $("#infringing_user_name").val();
		var filter = profilename;
		
	
		if(infringing_user_name.val()=='')
		{
			infringing_user_name.addClass("error1");
			infringing_user_nameInfo.text("This field is required.");
			infringing_user_nameInfo.addClass("error1");
			
			return false;
		}
		else {
			
			//if it's valid email
			if(filter.test(a)){
				infringing_user_name.removeClass("error1");			
				infringing_user_nameInfo.text(" ");
				infringing_user_nameInfo.removeClass("error1");
				infringing_user_nameInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				infringing_user_name.addClass("error1");
				infringing_user_nameInfo.text("Infringing username is not valid.");
				infringing_user_nameInfo.addClass("error1");
				return false;
				
			}
			
		}
		
		
	
	}	
	
	function validatetradmark_word_or_symbol()
	{
			
		if(tradmark_word_or_symbol.val()=='')
		{
			
			tradmark_word_or_symbol.addClass("error1");
			tradmark_word_or_symbolInfo.text("This field is required.");
			tradmark_word_or_symbolInfo.addClass("error1");
		
			return false;
		}
		else {
			
			tradmark_word_or_symbol.removeClass("error1");			
			tradmark_word_or_symbolInfo.text(" ");
			tradmark_word_or_symbolInfo.removeClass("error1");
			tradmark_word_or_symbolInfo.addClass("success");
			return true;
		}
	}	
	
	function validatecompany_name()
	{
		
		
		
		var a = $("#company_name").val();
		var filter = LetNumSpaceSpec;
		
		
		if(company_name.val()=='')
		{
			company_name.addClass("error1");
			company_nameInfo.text("This field is required.");
			company_nameInfo.addClass("error1");
			return false;
		}
		else {
			
			//if it's valid email
			if(filter.test(a)){
				company_name.removeClass("error1");			
				company_nameInfo.text(" ");
				company_nameInfo.removeClass("error1");
				company_nameInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				company_name.addClass("error1");
				company_nameInfo.text("Company Name is not valid.");
				company_nameInfo.addClass("error1");
			
				return false;
			}
			
		}
		
		
	
	
	}
	
	function validaterelation_to_trademark()
	{
		
		var a = $("#relation_to_trademark").val();
		var filter = LetNumSpaceSpec;
		
		
		if(relation_to_trademark.val()=='')
		{
			relation_to_trademark.addClass("error1");
			relation_to_trademarkInfo.text("This field is required.");
			relation_to_trademarkInfo.addClass("error1");
			return false;
		}
		else {
			
			//if it's valid email
			if(filter.test(a)){
				relation_to_trademark.removeClass("error1");			
				relation_to_trademarkInfo.text(" ");
				relation_to_trademarkInfo.removeClass("error1");
				relation_to_trademarkInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				relation_to_trademark.addClass("error1");
				relation_to_trademarkInfo.text("Relation to trademark holder is not valid.");
				relation_to_trademarkInfo.addClass("error1");
				
				return false;
			}
			
		}
		
		
	
	
	
	}
	
	function validatedetail()
	{
		
		if(detail.val()=='')
		{
			detail.addClass("error1");
			detailInfo.text("This field is required.");
			detailInfo.addClass("error1");
			return false;
		}
		else {
			
			detail.removeClass("error1");			
			detailInfo.text(" ");
			detailInfo.removeClass("error1");
			detailInfo.addClass("success");
			return true;
		}
	
	}
	
	function validateregistration_number()
	{
		
		
		
		var a = $("#registration_number").val();
		var filter = LetNumSpaceSpec;
		
		
		if(registration_number.val()=='')
		{
			registration_number.addClass("error1");
			registration_numberInfo.text("This field is required.");
			registration_numberInfo.addClass("error1");
			return false;
		}
		else {
			
			//if it's valid email
			if(filter.test(a)){
				registration_number.removeClass("error1");			
				registration_numberInfo.text(" ");
				registration_numberInfo.removeClass("error1");
				registration_numberInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				registration_number.addClass("error1");
				registration_numberInfo.text("Registration number is not valid.");
				registration_numberInfo.addClass("error1");
				
				return false;
			}
			
		}
		
		
	
	
	}	
	
	function validateregistration_office()
	{
		
		if(registration_office.val()=='')
		{
			registration_office.addClass("error1");
			registration_officeInfo.text("This field is required.");
			registration_officeInfo.addClass("error1");
			
			return false;
		}
		else {
			
			registration_office.removeClass("error1");			
			registration_officeInfo.text(" ");
			registration_officeInfo.removeClass("error1");
			registration_officeInfo.addClass("success");
			return true;
		}
	
	}
	
	
	function validatelink_to_trademark_record()
	{
		var a = $("#link_to_trademark_record").val();
		var filter = url_reg_exp;
		
		
		if(link_to_trademark_record.val()!='')
		{
			
			
			//if it's valid email
			if(filter.test(a)){
				link_to_trademark_record.removeClass("error1");			
				link_to_trademark_recordInfo.text(" ");
				link_to_trademark_recordInfo.removeClass("error1");
				link_to_trademark_recordInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				link_to_trademark_record.addClass("error1");
				link_to_trademark_recordInfo.text("Url address is not valid.");
				link_to_trademark_recordInfo.addClass("error1");
			
				return false;
			}
			
		} else { return true;  }
		
	}
	
	
	
	function validatedate_of_first_use()
	{
		
		// regular expression to match required date format
		re = /^(\d{4})\-(\d{1,2})\-(\d{1,2})$/;
	
		if(date_of_first_use.val() != '') {
		 
		 if(regs = date_of_first_use.val().match(re)) {
			 
		
			// day value between 1 and 31
			if(regs[3] < 1 || regs[3] > 31) {
			  //alert("Invalid value for day: " + regs[3]);
			  
			    date_of_first_use.addClass("error1");
				date_of_first_useInfo.text('Invalid value for day.');
				date_of_first_useInfo.addClass("error1");
			
				  return false;
			}
			// month value between 1 and 12
			else if(regs[2] < 1 || regs[2] > 12) {
			 // alert("Invalid value for month: " + regs[2]);
			 
				date_of_first_use.addClass("error1");
				date_of_first_useInfo.text('Invalid value for month.');
				date_of_first_useInfo.addClass("error1");
			
				
			  return false;
			}
			// year value between 1902 and 2012
			else if(regs[1] < 1950 || regs[1] > (new Date()).getFullYear()) {
			  //alert("Invalid value for year: " + regs[1] + " - must be between 1902 and " + (new Date()).getFullYear());
			  
			  date_of_first_use.addClass("error1");
		date_of_first_useInfo.text("Invalid value for year : " + regs[1] + " - Year must be between 1950 and " + (new Date()).getFullYear());
				date_of_first_useInfo.addClass("error1");
			
			  return false;
			}
			
			else
			{
					date_of_first_use.removeClass("error1");
					date_of_first_useInfo.text(' ');
					date_of_first_useInfo.addClass("success");
					return true;
			}
		
			
		  } else {
			//alert("Invalid date format: " + date_of_first_use.val());
				date_of_first_use.addClass("error1");
				date_of_first_useInfo.text('Invalid date format.');
				date_of_first_useInfo.addClass("error1");
			
			return false;
		  }
		}
		else
		{
				date_of_first_use.removeClass("error1");
				date_of_first_useInfo.text(' ');
				date_of_first_useInfo.addClass("success");
				return true;
		}
	
	}
	
	
	function validatedate_of_app()
	{
		
		
		// regular expression to match required date format
		re = /^(\d{4})\-(\d{1,2})\-(\d{1,2})$/;
	
		if(date_of_app.val() != '') {
		  if(regs = date_of_app.val().match(re)) {
			// day value between 1 and 31
			if(regs[3] < 1 || regs[3] > 31) {
			 // alert("Invalid value for day: " + regs[3]);
			  
			    date_of_app.addClass("error1");
				date_of_appInfo.text('Invalid value for day.');
				date_of_appInfo.addClass("error1");
			
				  return false;
			}
			// month value between 1 and 12
			else if(regs[2] < 1 || regs[2] > 12) {
			  //alert("Invalid value for month: " + regs[2]);
			 
				date_of_app.addClass("error1");
				date_of_appInfo.text('Invalid value for month.');
				date_of_appInfo.addClass("error1");
				
			  return false;
			}
			// year value between 1902 and 2012
			else if(regs[1] < 1950 || regs[1] > (new Date()).getFullYear()) {
			  //alert("Invalid value for year: " + regs[1] + " - must be between 1902 and " + (new Date()).getFullYear());
			  
			  date_of_app.addClass("error1");
		date_of_appInfo.text("Invalid value for year : " + regs[1] + " - Year must be between 1950 and " + (new Date()).getFullYear());
				date_of_appInfo.addClass("error1");
			
			  return false;
			}
			
			
			else
			{
					date_of_app.removeClass("error1");
					date_of_appInfo.text(' ');
					date_of_appInfo.addClass("success");
					return true;
			}
			
			
		  } else {
			//alert("Invalid date format: " + date_of_first_use.val());
				date_of_app.addClass("error1");
				date_of_appInfo.text('Invalid date format.');
				date_of_appInfo.addClass("error1");
				
			return false;
		  }
		}
			else
			{
					date_of_app.removeClass("error1");
					date_of_appInfo.text(' ');
					date_of_appInfo.addClass("success");
					return true;
			}
	
	}
	
	function validatedate_of_reg()
	{
		
		
		
		// regular expression to match required date format
		re = /^(\d{4})\-(\d{1,2})\-(\d{1,2})$/;
	
		if(date_of_reg.val() != '') {
		  if(regs = date_of_reg.val().match(re)) {
			// day value between 1 and 31
			if(regs[3] < 1 || regs[3] > 31) {
			 // alert("Invalid value for day: " + regs[3]);
			  
			    date_of_reg.addClass("error1");
				date_of_regInfo.text('Invalid value for day.');
				date_of_regInfo.addClass("error1");
			
				  return false;
			}
			// month value between 1 and 12
			else if(regs[2] < 1 || regs[2] > 12) {
			  //alert("Invalid value for month: " + regs[2]);
			 
				date_of_reg.addClass("error1");
				date_of_regInfo.text('Invalid value for month.');
				date_of_regInfo.addClass("error1");
				
			  return false;
			}
			// year value between 1902 and 2012
			else if(regs[1] < 1950 || regs[1] > (new Date()).getFullYear()) {
			  //alert("Invalid value for year: " + regs[1] + " - must be between 1902 and " + (new Date()).getFullYear());
			  
			  date_of_reg.addClass("error1");
		date_of_regInfo.text("Invalid value for year : " + regs[1] + " - Year must be between 1950 and " + (new Date()).getFullYear());
				date_of_regInfo.addClass("error1");
			
			  return false;
			}
			
			
			
			else
			{
					date_of_reg.removeClass("error1");
					date_of_regInfo.text(' ');
					date_of_regInfo.addClass("success");
					return true;
			}
			
			
		  } else {
			//alert("Invalid date format: " + date_of_first_use.val());
				date_of_reg.addClass("error1");
				date_of_regInfo.text('Invalid date format.');
				date_of_regInfo.addClass("error1");
				
			return false;
		  }
		}
	
		else
			{
					date_of_reg.removeClass("error1");
					date_of_regInfo.text(' ');
					date_of_regInfo.addClass("success");
					return true;
			}
	
	}
	
	
	function validateZipcode()
	{
		
		//testing regular expression
		var a = $("#zip_code").val();
		var filter = alphanum;
		
		if(zip_code.val()=='')
		{
			zip_code.addClass("error1");
			zip_codeInfo.text('This field is required.');
			zip_codeInfo.addClass("error1");
			
			return false;
			
		}
	
		else if(zip_code.val().length < 4 )
		{		
			zip_code.addClass("error1");
			zip_codeInfo.text('Zip/Postal Code at-least 4 digit required.');
			zip_codeInfo.addClass("error1");
			
			return false;
		} 
		
		else if(zip_code.val().length > 9 )
		{		
			zip_code.addClass("error1");
			zip_codeInfo.text('Zip/Postal Code maximum 9 digit required.');
			zip_codeInfo.addClass("error1");
				
			return false;
		} 
		
		else {
			
			//if it's valid number
			if(filter.test(a)){
				zip_code.removeClass("error1");
				zip_codeInfo.text(' ');
				zip_codeInfo.removeClass("error1");	
				zip_codeInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				zip_code.addClass("error1");
				zip_codeInfo.text('Zip/Postal Code at-least 4 digit required.');
				zip_codeInfo.addClass("error1");
					
				return false;
			}
		}
	}	
	
	function validatefull_name()
	{
		
		var a = $("#full_name").val();
		var filter = alphaspace;
		
		
		if(full_name.val()=='')
		{
			full_name.addClass("error1");
			full_nameInfo.text("This field is required.");
			full_nameInfo.addClass("error1");
			return false;
		}
		else {
			
			//if it's valid email
			if(filter.test(a)){
				full_name.removeClass("error1");			
				full_nameInfo.text(" ");
				full_nameInfo.removeClass("error1");
				full_nameInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				full_name.addClass("error1");
				full_nameInfo.text("Full name is not valid.");
				full_nameInfo.addClass("error1");
			
				return false;
			}
			
		}
		
		
	}
	
	
	function validateaddress()
	{			
		
		if(address.val()=='')
		{
			address.addClass("error1");
			addressInfo.text("This field is required.");
			addressInfo.addClass("error1");
			return false;
		}
		else {
			
			//if it's valid email
			if(address.val().length>4){
				address.removeClass("error1");			
				addressInfo.text(" ");
				addressInfo.removeClass("error1");
				addressInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				address.addClass("error1");
				addressInfo.text("Address field must required at-least 5 character.");
				addressInfo.addClass("error1");
					
				return false;
			}
			
		}
		
		
	}
	
		function validatecity()
	{			
		
		if(city.val()=='')
		{
			city.addClass("error1");
			cityInfo.text("This field is required.");
			cityInfo.addClass("error1");
			return false;
		}
		else {
			
			//if it's valid email
			if(city.val().length>3){
				city.removeClass("error1");			
				cityInfo.text(" ");
				cityInfo.removeClass("error1");
				cityInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				city.addClass("error1");
				cityInfo.text("City field must required at-least 3 character.");
				cityInfo.addClass("error1");
					
				return false;
			}
			
		}
		
		
	}
	
	
		function validatestate()
	{			
		
		if(state.val()=='')
		{
			state.addClass("error1");
			stateInfo.text("This field is required.");
			stateInfo.addClass("error1");
			return false;
		}
		else {
			
			//if it's valid email
			if(state.val().length>=2){
				state.removeClass("error1");			
				stateInfo.text(" ");
				stateInfo.removeClass("error1");
				stateInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				state.addClass("error1");
				stateInfo.text("State field must required at-least 2 character.");
				stateInfo.addClass("error1");
				
				return false;
			}
			
		}
		
		
	}
	
	
	function validatecountry()
	{
					
		
		if(country.val()=='')
		{
			country.addClass("error1");
			countryInfo.text("This field is required.");
			countryInfo.addClass("error1");
			return false;
		}
		else {
			
			
				country.removeClass("error1");			
				countryInfo.text(" ");
				countryInfo.removeClass("error1");
				countryInfo.addClass("success");
					
				return true;
		
			
		}
		
		
		
	}
	
	
	function validatephone_no()
	{
		
		
		//testing regular expression
		var a = $("#phone_no").val();
		var filter = number;
		
		
		
		
		if(phone_no.val()=='')
		{
			phone_no.addClass("error1");
			phone_noInfo.text('This field is required.');
			phone_noInfo.addClass("error1");
			
			return false;
			
		}
	
		else if(phone_no.val().length <= 9 )
		{		
			phone_no.addClass("error1");
			phone_noInfo.text('Phone No. must be 10 digit.');
			phone_noInfo.addClass("error1");
			
			return false;
		} 
		
		else if(phone_no.val().length > 10 )
		{		
			phone_no.addClass("error1");
			phone_noInfo.text('Phone No. must be 10 digit.');
			phone_noInfo.addClass("error1");
			
			return false;
		} 
		
		else {
			
			//if it's valid number
			if(filter.test(a)){
				phone_no.removeClass("error1");
				phone_noInfo.text(' ');
				phone_noInfo.removeClass("error1");		
				phone_noInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				phone_no.addClass("error1");
				phone_noInfo.text('Phone No. must be 10 digit.');
				phone_noInfo.addClass("error1");
				
				return false;
			}
		}
		
		
		
	
	}
	
	
	function validateEmail(){
		//testing regular expression
		var a = $("#email").val();
		var filter = email_reg_exp;
		
		if(email.val()=='')
		{
			email.addClass("error1");
			emailInfo.text("This field is required.");
			emailInfo.addClass("error1");
			return false;
		}
		else {
			//if it's valid email
			if(filter.test(a)){
				email.removeClass("error1");			
				emailInfo.text(" ");
				emailInfo.removeClass("error1");
				emailInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				email.addClass("error1");
				emailInfo.text("Type a valid e-mail.");
				emailInfo.addClass("error1");
				
				return false;
			}
		}
	}
	
	
	
	function validateusername_action()
	{
		
		 var oBtnsGender = document.getElementsByName('username_action');
                var isGenderChecked = false;
                for(i=0; i < oBtnsGender.length; i++){
                    if(oBtnsGender[i].checked){
                        isGenderChecked = true;
                        i = oBtnsGender.length;
                    }
                }
			
                if(!isGenderChecked) {
					
					 username_action.addClass("error1");
					username_actionInfo.text("This field is required.");
					username_actionInfo.addClass("error1");
					return false;
				
				
				}
				else
				{
						username_action.removeClass("error1");
						username_actionInfo.text(" ");
						username_actionInfo.removeClass("error1");
						return true;
					}
				
				
	}
	
	
	function validatecondition2()
	{
		
		
		if($('#condition2:checked').size()==1)
		{
			condition2.removeClass("error1");			
			condition2Info.text(" ");
			condition2Info.removeClass("error1");
			condition2Info.addClass("success");
			return true;
		}
		else
		{
			condition2.addClass("error1");
			condition2Info.text("This field is required.");
			condition2Info.addClass("error1");
			return false;
		}
		
	}
	
	
	function validatecondition1()
	{
		
		
		if($('#condition1:checked').size()==1)
		{
			condition1.removeClass("error1");			
			condition1Info.text(" ");
			condition1Info.removeClass("error1");
			condition1Info.addClass("success");
			return true;
		}
		else
		{
			condition1.addClass("error1");
			condition1Info.text("This field is required.");
			condition1Info.addClass("error1");
			return false;
		}
		
	}
	
	
	
	


}


function validate_copyrightform()
	{
	
	
		
		var form = $("#copyright_complaint");	
		
		var infringed_work_url = $("#infringed_work_url");
		var infringedworkurlInfo = $("#infringedworkurlInfo");		
		
		var own_website = $("#own_website");
		var ownwebsiteInfo = $("#ownwebsiteInfo");
		
		var full_name = $("#full_name");
		var full_nameInfo = $("#full_nameInfo");
		
		var address = $("#address");
		var addressInfo = $("#addressInfo");
		
		var city = $("#city");
		var cityInfo = $("#cityInfo");
		
		var state = $("#state");
		var stateInfo = $("#stateInfo");
		
		var zip_code = $("#zip_code");
		var zip_codeInfo = $("#zip_codeInfo");
		
		var country = $("#country");
		var countryInfo = $("#countryInfo");
		
		var phone_no = $("#phone_no");
		var phone_noInfo = $("#phone_noInfo");
		
		var email = $("#email");
		var emailInfo = $("#emailInfo");
		
		var disputed_use_prevent = $("#disputed_use_prevent");
		var disputepreventInfo = $("#disputepreventInfo");		
		
		var authorized_to_act = $("#authorized_to_act");
		var authorizedtoactInfo = $("#authorizedtoactInfo");
		
		var information_accurate = $("#information_accurate");
		var informationaccurateInfo = $("#informationaccurateInfo");
		
		var electronic_signature = $("#electronic_signature");
		var electronic_signatureInfo = $("#electronic_signatureInfo");
		
		
		
		
		
		
		//On Submitting
		form.submit(function(){
			if(validateinfringed_work_url() & validateown_website() & validatefull_name() & validateaddress() & validatecity() & validatestate() & validatecountry() & validatephone_no() & validateEmail() & validatedisputed_use_prevent() & validateauthorized_to_act() & validateinformation_accurate() & validateelectronic_signature() & validateZipcode())
				return true
			else
				return false;
		});
		
	
	
	
	
	
	
	//validation functions
	function validateZipcode()
	{
		
		//testing regular expression
		var a = $("#zip_code").val();
		var filter = alphanum;
		
		if(zip_code.val()=='')
		{
			zip_code.addClass("error1");
			zip_codeInfo.text('This field is required.');
			zip_codeInfo.addClass("error1");
			
			return false;
			
		}
	
		else if(zip_code.val().length < 4 )
		{		
			zip_code.addClass("error1");
			zip_codeInfo.text('Zip/Postal Code at-least 4 digit required.');
			zip_codeInfo.addClass("error1");
			return false;
		} 
		
		else if(zip_code.val().length > 9 )
		{		
			zip_code.addClass("error1");
			zip_codeInfo.text('Zip/Postal Code maximum 9 digit required.');
			zip_codeInfo.addClass("error1");
			return false;
		} 
		
		else {
			
			//if it's valid number
			if(filter.test(a)){
				zip_code.removeClass("error1");
				zip_codeInfo.text(' ');
				zip_codeInfo.removeClass("error1");	
				zip_codeInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				zip_code.addClass("error1");
				zip_codeInfo.text('Zip/Postal Code at-least 4 difit required.');
				zip_codeInfo.addClass("error1");
				return false;
			}
		}
	}
	
	
	
	function validateinfringed_work_url()
	{
		var a = $("#infringed_work_url").val();
		var filter = url_reg_exp;
		
		
		if(infringed_work_url.val()=='')
		{
			infringed_work_url.addClass("error1");
			infringedworkurlInfo.text("This field is required.");
			infringedworkurlInfo.addClass("error1");
			return false;
		}
		else {
			
			//if it's valid email
			if(filter.test(a)){
				infringed_work_url.removeClass("error1");			
				infringedworkurlInfo.text(" ");
				infringedworkurlInfo.removeClass("error1");
				infringedworkurlInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				infringed_work_url.addClass("error1");
				infringedworkurlInfo.text("Url address is not valid.");
				infringedworkurlInfo.addClass("error1");
				return false;
			}
			
		}
		
	}
	
	
	function validateown_website()
	{
		var a = $("#own_website").val();
		var filter = url_reg_exp;
		
		
		if(own_website.val()=='')
		{
			own_website.addClass("error1");
			ownwebsiteInfo.text("This field is required.");
			ownwebsiteInfo.addClass("error1");
			return false;
		}
		else {
			
			//if it's valid email
			if(filter.test(a)){
				own_website.removeClass("error1");			
				ownwebsiteInfo.text(" ");
				ownwebsiteInfo.removeClass("error1");
				ownwebsiteInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				own_website.addClass("error1");
				ownwebsiteInfo.text("Url address is not valid.");
				ownwebsiteInfo.addClass("error1");
				return false;
			}
			
		}
		
	}
	
	
	function validatefull_name()
	{
		
		var a = $("#full_name").val();
		var filter = alphaspace;
		
		
		if(full_name.val()=='')
		{
			full_name.addClass("error1");
			full_nameInfo.text("This field is required.");
			full_nameInfo.addClass("error1");
			return false;
		}
		else {
			
			//if it's valid email
			if(filter.test(a)){
				full_name.removeClass("error1");			
				full_nameInfo.text(" ");
				full_nameInfo.removeClass("error1");
				full_nameInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				full_name.addClass("error1");
				full_nameInfo.text("Full name is not valid.");
				full_nameInfo.addClass("error1");
				return false;
			}
			
		}
		
		
	}
	
	
	function validateaddress()
	{			
		
		if(address.val()=='')
		{
			address.addClass("error1");
			addressInfo.text("This field is required.");
			addressInfo.addClass("error1");
			return false;
		}
		else {
			
			//if it's valid email
			if(address.val().length>4){
				address.removeClass("error1");			
				addressInfo.text(" ");
				addressInfo.removeClass("error1");
				addressInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				address.addClass("error1");
				addressInfo.text("Address field must required at-least 5 character.");
				addressInfo.addClass("error1");
				return false;
			}
			
		}
		
		
	}
	
		function validatecity()
	{			
		
		if(city.val()=='')
		{
			city.addClass("error1");
			cityInfo.text("This field is required.");
			cityInfo.addClass("error1");
			return false;
		}
		else {
			
			//if it's valid email
			if(city.val().length>3){
				city.removeClass("error1");			
				cityInfo.text(" ");
				cityInfo.removeClass("error1");
				cityInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				city.addClass("error1");
				cityInfo.text("City field must required at-least 3 character.");
				cityInfo.addClass("error1");
				return false;
			}
			
		}
		
		
	}
	
	
		function validatestate()
	{			
		
		if(state.val()=='')
		{
			state.addClass("error1");
			stateInfo.text("This field is required.");
			stateInfo.addClass("error1");
			return false;
		}
		else {
			
			//if it's valid email
			if(state.val().length>=2){
				state.removeClass("error1");			
				stateInfo.text(" ");
				stateInfo.removeClass("error1");
				stateInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				state.addClass("error1");
				stateInfo.text("State field must required at-least 2 character.");
				stateInfo.addClass("error1");
				return false;
			}
			
		}
		
		
	}
	
	
	function validatecountry()
	{
					
		
		if(country.val()=='')
		{
			country.addClass("error1");
			countryInfo.text("This field is required.");
			countryInfo.addClass("error1");
			return false;
		}
		else {
			
			
				country.removeClass("error1");			
				countryInfo.text(" ");
				countryInfo.removeClass("error1");
				countryInfo.addClass("success");
				return true;
		
			
		}
		
		
		
	}
	
	
	function validatephone_no()
	{
		
		
		//testing regular expression
		var a = $("#phone_no").val();
		var filter = number;
		
		
		
		
		if(phone_no.val()=='')
		{
			phone_no.addClass("error1");
			phone_noInfo.text('This field is required.');
			phone_noInfo.addClass("error1");
			
			return false;
			
		}
	
		else if(phone_no.val().length <= 9 )
		{		
			phone_no.addClass("error1");
			phone_noInfo.text('Phone No. must be 10 digit.');
			phone_noInfo.addClass("error1");
			return false;
		} 
		
		else if(phone_no.val().length > 10 )
		{		
			phone_no.addClass("error1");
			phone_noInfo.text('Phone No. must be 10 digit.');
			phone_noInfo.addClass("error1");
			return false;
		} 
		
		else {
			
			//if it's valid number
			if(filter.test(a)){
				phone_no.removeClass("error1");
				phone_noInfo.text(' ');
				phone_noInfo.removeClass("error1");		
				phone_noInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				phone_no.addClass("error1");
				phone_noInfo.text('Phone No. must be 10 digit.');
				phone_noInfo.addClass("error1");
				return false;
			}
		}
		
		
		
	
	}
	
	
	function validateEmail(){
		//testing regular expression
		var a = $("#email").val();
		var filter = email_reg_exp;
		
		if(email.val()=='')
		{
			email.addClass("error1");
			emailInfo.text("This field is required.");
			emailInfo.addClass("error1");
			return false;
		}
		else {
			//if it's valid email
			if(filter.test(a)){
				email.removeClass("error1");			
				emailInfo.text(" ");
				emailInfo.removeClass("error1");
				emailInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				email.addClass("error1");
				emailInfo.text("Type a valid e-mail.");
				emailInfo.addClass("error1");
				return false;
			}
		}
	}
	
	
	
	function validatedisputed_use_prevent()
	{
			
		if($('#disputed_use_prevent:checked').size()==1)
		{
			disputed_use_prevent.removeClass("error1");			
			disputepreventInfo.text(" ");
			disputepreventInfo.removeClass("error1");
			disputepreventInfo.addClass("success");
			return true;
		}
		else
		{
			disputed_use_prevent.addClass("error1");
			disputepreventInfo.text("This field is required.");
			disputepreventInfo.addClass("error1");
			return false;
		}
		
	}
	
	
	function validateauthorized_to_act()
	{
		
		
		if($('#authorized_to_act:checked').size()==1)
		{
			authorized_to_act.removeClass("error1");			
			authorizedtoactInfo.text(" ");
			authorizedtoactInfo.removeClass("error1");
			authorizedtoactInfo.addClass("success");
			return true;
		}
		else
		{
			authorized_to_act.addClass("error1");
			authorizedtoactInfo.text("This field is required.");
			authorizedtoactInfo.addClass("error1");
			return false;
		}
		
	}
	
	
	function validateinformation_accurate()
	{
		
		
		if($('#information_accurate:checked').size()==1)
		{
			information_accurate.removeClass("error1");			
			informationaccurateInfo.text(" ");
			informationaccurateInfo.removeClass("error1");
			informationaccurateInfo.addClass("success");
			return true;
		}
		else
		{
			information_accurate.addClass("error1");
			informationaccurateInfo.text("This field is required.");
			informationaccurateInfo.addClass("error1");
			return false;
		}
		
	}
	
	
	function validateelectronic_signature()
	{
		
		
		var a = $("#electronic_signature").val();
		var filter = LetNumSpaceSpec;
		
		
		if(electronic_signature=='')
		{
			electronic_signature.addClass("error1");
			electronic_signatureInfo.text("This field is required.");
			electronic_signatureInfo.addClass("error1");
			return false;
		}
		else {
			
			//if it's valid email
			if(filter.test(a)){
				electronic_signature.removeClass("error1");			
				electronic_signatureInfo.text(" ");
				electronic_signatureInfo.removeClass("error1");
				electronic_signatureInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				electronic_signature.addClass("error1");
				electronic_signatureInfo.text("Electronic Signature is not valid.");
				electronic_signatureInfo.addClass("error1");
				return false;
			}
			
		}
		
		
	
	}
	

}
	
	
	
	

function validate_request()
{
	var form = $("#frmlanding");
	
	var email = $("#email");
	var emailInfo = $("#emailInfo");
	
	//On click		
		email.focus(function() {  			
			emailInfo.addClass('field_main'); 
		} );
		
	
	//On blur
		email.blur(validateRequestEmail);
	
	//On key up
		//email.keyup(validateRequestEmail);
		
	//On Submitting
		form.submit(function(){
			if(validateRequestEmail())
				return true
			else
				return false;
		});
		
		
		//validation functions
	
	
	
	
	
	function checkRequestEmailAvailability()
	{	
		
		var email = $("#email");
		var emailInfo = $("#emailInfo");
	
           
                // show our holding text in the validation message space
                emailInfo.removeClass('error1');
				emailInfo.html('<img src="'+baseThemeUrl+'/images/ajax-loader.gif" height="16" width="16" /> checking availability...');
				
			
          var res = $.ajax({						
						type: 'POST',
                        url: baseUrl+'home/checkrequestemailavailability',
                        data: 'email=' + email.val(),
                        dataType: 'json', 
						cache: false,
						async: false                     
                    }).responseText;
		  
	
				return res;	
              
        
	}
	
	
	function validateRequestEmail()
	{
	
		//testing regular expression
		var a = $("#email").val();
		var filter = email_reg_exp;
		//if it's valid email
		if(filter.test(a)){
			email.removeClass("error1");
			//emailInfo.text("E-mail address is valid!");
			emailInfo.text("");
			emailInfo.removeClass("error1");
			emailInfo.addClass("success");
			
			
			var chk=checkRequestEmailAvailability();
			
			var obj = jQuery.parseJSON(chk);

			if(obj.ok==true)
			{				
				
				email.removeClass("error1");
				//emailInfo.text("E-mail address is available!");
				emailInfo.text("");
				emailInfo.addClass("success");
				
				return true;
			}
			else
			{
				email.addClass("error1");
				emailInfo.text("This E-mail address is already registered.");
				emailInfo.addClass("error1");
				return false;
			}
			
			
		}
		
		//if it's NOT valid
		else{
			email.addClass("error1");
			emailInfo.text("That doesn't look like a valid address. Is there an extra space?");
			emailInfo.addClass("error1");
			return false;
		}
	}
	
	
	
}

function validate_socialsignup()
	{
	
		var form = $("#signupForm");
		
		var user_name = $("#user_name");
		var user_nameInfo = $("#user_nameInfo");
	
		var email = $("#email");
		var emailInfo = $("#emailInfo");
	
		
		var password = $("#password");
		var passwordInfo = $("#passwordInfo");
		
		
		//On blur
		user_name.blur(validateUserName);		
		email.blur(validateEmail);
		password.blur(validatePassword);
		
		
		//On key up
		//user_name.keyup(validateUserName);		
		//email.keyup(validateEmail);
		password.keyup(validatePassword);
		
		
		
		
		//On Submitting
		form.submit(function(){
			if(validateUserName() & validateEmail() & validatePassword())
				return true
			else
				return false;
		});
		
	
	
	
	
	//validation functions
	
	function checkAvailability()
	{	
		
		var email = $("#email");
		var emailInfo = $("#emailInfo");
	
           
                // show our holding text in the validation message space
                emailInfo.removeClass('error1');
				emailInfo.html('<img src="'+baseThemeUrl+'/images/ajax-loader.gif" height="16" width="16" /> checking availability...');
				
			
          var res = $.ajax({						
						type: 'POST',
                        url: baseUrl+'home/checkemailavailability',
                        data: 'email=' + email.val(),
                        dataType: 'json', 
						cache: false,
						async: false                     
                    }).responseText;
		  
	
				return res;	
              
        
	}
	
	
	function validateEmail()
	{
	
		//testing regular expression
		var a = $("#email").val();
		var filter = email_reg_exp;
		
		
		if(a=='')
		{
			email.addClass("error1");
			emailInfo.text("Sorry, you must specify an E-mail address");
			emailInfo.addClass("error1");
			
			return false;
			
		}
		
		
		//if it's valid
		else{
			
				//if it's valid email
				if(filter.test(a)){
			email.removeClass("error1");
			emailInfo.text("E-mail address is valid!");
			emailInfo.removeClass("error1");
			emailInfo.addClass("success");
			
			
			var chk=checkAvailability();
			
			var obj = jQuery.parseJSON(chk);

			if(obj.ok==true)
			{				
				
				email.removeClass("error1");
				emailInfo.text("E-mail address is available!");
				emailInfo.addClass("success");
				
				return true;
			}
			else
			{
				email.addClass("error1");
				emailInfo.text("E-mail address is not available!");
				emailInfo.addClass("error1");
				return false;
			}
			
			
		}
		
				//if it's NOT valid
			else{
					email.addClass("error1");
					emailInfo.text("Type a valid E-mail");
					emailInfo.addClass("error1");
					return false;
				}
		}
		
	}
	
	
	
	function validatePassword()
	{
		
		var a = $("#password");	

		//it's NOT valid
		if(password.val().length <8){
			password.addClass("error1");
			passwordInfo.text("At least 8 character are required.");
			passwordInfo.addClass("error1");
			return false;
		}
		//it's valid
		else{			
			password.removeClass("error1");
			passwordInfo.text("Ey! Remember: your password");
			passwordInfo.removeClass("error1");
			passwordInfo.addClass("success");
			return true;
		}
	}
	
	
	function checkUsernameAvailability()
	{	
		
		var user_name = $("#user_name");
		var user_nameInfo = $("#user_nameInfo");
	
           
              // show our holding text in the validation message space
             user_nameInfo.removeClass('error1');
			user_nameInfo.html('<img src="'+baseThemeUrl+'/images/ajax-loader.gif" height="16" width="16" /> checking availability...');

          var res = $.ajax({						
						type: 'POST',
                        url: baseUrl+'home/checkusernameavailability',
                        data: 'user_name=' + user_name.val(),
                        dataType: 'json', 
						cache: false,
						async: false                     
                    }).responseText;
		  
				return res;	
	}
	
	
	function validateUserName()
	{	
		
		var a = $("#user_name").val();
		var filter = profilename;
			
		if(user_name.val()=='')
		{
			user_name.addClass("error1");
			user_nameInfo.text("Please use at least 3 characters");
			user_nameInfo.addClass("error1");
			
			return false;
			
		}
		//if it's NOT valid
		else if(user_name.val().length < 4){
			user_name.addClass("error1");
			user_nameInfo.text("Please use at least 4 characters");
			user_nameInfo.addClass("error1");
			return false;
		}
		
		
		//if it's valid
		else{			
			//if it's valid number
			if(filter.test(a)){
				
				var chk=checkUsernameAvailability();			
				var obj = jQuery.parseJSON(chk);			
			
				
				if(obj.ok==true)
				{						
					user_name.removeClass("error1");
					//user_nameInfo.text("Username is available!");
					user_nameInfo.html("<br/>");
					user_nameInfo.removeClass("error1");
					user_nameInfo.addClass("success");					
					return true;
				}
				else
				{				
					user_name.removeClass("error1");
					user_nameInfo.text("Username is not available!");
					user_nameInfo.addClass("error1");
					return false;
				}			
				
			}
			//if it's NOT valid
			else{
				user_name.addClass("error1");
				user_nameInfo.text("Enter valid user name.");
				user_nameInfo.addClass("error1");
				return false;
			}
			
			
		}
	}
	
	
	

	
	
	}
	
	
	
	
	function validate_signup()
	{
		
		var form = $("#signupForm");
		
		var full_name = $("#full_name");
		var full_nameInfo = $("#full_nameInfo");
			
		
		var email = $("#email");
		var emailInfo = $("#emailInfo");
		
		
		var password = $("#password");
		var passwordInfo = $("#passwordInfo");
		//var passwordTR = $("#passwordTR");
		
		var retypePassword = $("#retypePassword");
		var retypePasswordInfo = $("#retypePasswordInfo");
		//var retypePasswordTR = $("#retypepasswordTR");
		
		
		//On click		
		email.focus(function() {  
			
			//emailTR.addClass('field_main'); 
			//retypepasswordTR.removeClass('retypepassword');
			//passwordTR.removeClass('field_main');
			//full_nameTR.removeClass('field_main');
			//mobile_noTR.removeClass('field_main'); 
		} );
		
		password.focus(function() {  
			///passwordTR.addClass('field_main'); 
			//retypepasswordTR.removeClass('retypepassword');
			//emailTR.removeClass('field_main');	
			//full_nameTR.removeClass('field_main');
			//mobile_noTR.removeClass('field_main');  
		} );
		
		
		
		full_name.focus(function() {  
			//full_nameTR.addClass('field_main'); 
			//zip_codeTR.removeClass('field_main');
			//emailTR.removeClass('field_main');
			//passwordTR.removeClass('field_main');
			//mobile_noTR.removeClass('field_main');  
		} );
		
		
		
		//On blur
		full_name.blur(validateUserName);		
		
		email.blur(validateEmail);
		password.blur(validatePassword);
		retypePassword.blur(retypePassword);
		
		
		//On key up
		//full_name.keyup(validateUserName);		
		
		//email.keyup(validateEmail);
		password.keyup(validatePassword);
		retypePassword.keyup(validateRetypePassword);
		
		
		
		
		//On Submitting
		form.submit(function(){
			if(validateUserName() & validateEmail() & validatePassword() & validateRetypePassword())
				return true
			else
				return false;
		});
		
	
	
	
	
	//validation functions
	
	
	
	
	
	function checkAvailability()
	{	
		
		var email = $("#email");
		var emailInfo = $("#emailInfo");
	
           
                // show our holding text in the validation message space
                emailInfo.removeClass('error1');
				emailInfo.html('<img src="'+baseThemeUrl+'/images/ajax-loader.gif" height="16" width="16" /> checking availability...');
				
			
          var res = $.ajax({						
						type: 'POST',
                        url: baseUrl+'home/checkemailavailability',
                        data: 'email=' + email.val(),
                        dataType: 'json', 
						cache: false,
						async: false                     
                    }).responseText;
		  
	
				return res;	
              
        
	}
	
	
	function validateEmail()
	{
	
		//testing regular expression
		var a = $("#email").val();
		var filter = email_reg_exp;
		//if it's valid email
		if(filter.test(a)){
			email.removeClass("error1");
			emailInfo.text("E-mail address is valid!");
			emailInfo.removeClass("error1");
			emailInfo.addClass("success");
			
			
			var chk=checkAvailability();
			
			var obj = jQuery.parseJSON(chk);

			if(obj.ok==true)
			{				
				
				email.removeClass("error1");
				emailInfo.text("E-mail address is available!");
				emailInfo.addClass("success");
				
				return true;
			}
			else
			{
				email.addClass("error1");
				emailInfo.text("E-mail address is not available!");
				emailInfo.removeClass("success");
				emailInfo.addClass("error1");
				return false;
			}
			
			
		}
		
		//if it's NOT valid
		else{
			email.addClass("error1");
			emailInfo.text("Type a valid e-mail please :P");
			emailInfo.addClass("error1");
			return false;
		}
	}
	
	
	
	function validatePassword()
	{
		
		var a = $("#password");	

		//it's NOT valid
		if(password.val().length <8){
			password.addClass("error1");
			passwordInfo.text("At least 8 character are required.");
			passwordInfo.addClass("error1");
			return false;
		}
		//it's valid
		else{			
			password.removeClass("error1");
			passwordInfo.text("Ey! Remember: your password");
			passwordInfo.removeClass("error1");
			passwordInfo.addClass("success");
			return true;
		}
	}
	

	
	
	function validateRetypePassword()
	{
		
		var a = $("#password");	
		var b = $("#retypePassword");	
	
		//it's NOT valid
		if(retypePassword.val().length <8){
			retypePassword.addClass("error1");
			retypePasswordInfo.text("At least 8 character are required.");
			retypePasswordInfo.addClass("error1");
			return false;
		}
		
		else if(password.val() != retypePassword.val()){
			retypePassword.addClass("error1");
			retypePasswordInfo.text("Please enter same password");
			retypePasswordInfo.addClass("error1");
			return false;
		}
		//it's valid
		else{			
			retypePassword.removeClass("error1");
			retypePasswordInfo.text("Ey! Your password is match.");
			retypePasswordInfo.removeClass("error1");
			retypePasswordInfo.addClass("success");
			return true;
		}
	}
	
	
	
	function checkUsernameAvailability()
	{	
		
		var full_name = $("#full_name").val();
		full_name= full_name.replace('.'+domain_name,'');
		var full_nameInfo = $("#full_nameInfo");
	
           
              // show our holding text in the validation message space
          full_nameInfo.removeClass('error1');
		full_nameInfo.html('<img src="'+baseThemeUrl+'/images/ajax-loader.gif" height="16" width="16" /> checking availability...');

          var res = $.ajax({						
						type: 'POST',
                        url: baseUrl+'home/checkusernameavailability',
                        data: 'user_name=' + full_name,
                        dataType: 'json', 
						cache: false,
						async: false                     
                    }).responseText;
		  
				return res;	
	}
	
	
	function validateUserName()
	{	
		
		
		var a = $("#full_name").val();
		a= a.replace('.'+domain_name,'');
		
		var filter = profilename;
			
		if(full_name.val()=='')
		{
			full_name.addClass("error1");
			full_nameInfo.text("Enter Username / Account URL!");
			full_nameInfo.addClass("error1");
			
			return false;
			
		}
		//if it's NOT valid
		else if(full_name.val().length < 4){
			full_name.addClass("error1");
			full_nameInfo.text("Please use at least 4 characters");
			full_nameInfo.addClass("error1");
			return false;
		}
		
		
		//if it's valid
		else{			
			//if it's valid number
			if(filter.test(a)){
				
				var chk=checkUsernameAvailability();			
				var obj = jQuery.parseJSON(chk);			
			
				
				if(obj.ok==true)
				{						
					full_name.removeClass("error1");
					//user_nameInfo.text("Username is available!");
					full_nameInfo.html("<br/>");
					full_nameInfo.removeClass("error1");
					full_nameInfo.addClass("success");					
					return true;
				}
				else
				{				
					full_name.removeClass("error1");
					full_nameInfo.text("Username / Account is not available!");
					full_nameInfo.addClass("error1");
					return false;
				}			
				
			}
			//if it's NOT valid
			else{
				full_name.addClass("error1");
				full_nameInfo.text("Enter valid Username / Account.");
				full_nameInfo.addClass("error1");
				return false;
			}
			
			
		}
	}
	
	
	
	

	
	
	
	}
	
	
	function validate_login()
	{
	
	
		
		var form = $("#loginForm");	
		
	
		var login_email = $("#login_email");
		var loginemailInfo = $("#loginemailInfo");
		
		
		var login_password = $("#login_password");
		var loginPasswordInfo = $("#loginPasswordInfo");
		
		
		
		
		//On click		
		login_email.focus(function() {  
			
		
			
		} );
		
		login_password.focus(function() {  
			
		} );
		
		
		
		
		//On blur
		
		login_email.blur(validateEmail);
		login_password.blur(validatePassword);
	
		login_password.keyup(validatePassword);
		
		
		//On Submitting
		form.submit(function(){
			if(validateEmail() & validatePassword())
				return true
			else
				return false;
		});
		
	
	
	
	
	
	
	//validation functions
	function validateEmail(){
		//testing regular expression
		var a = $("#login_email").val();
		var filter = email_reg_exp;
		//if it's valid email
		if(filter.test(a)){
			login_email.removeClass("error1");			
			loginemailInfo.text("E-mail address is valid!");
			loginemailInfo.removeClass("error1");
			loginemailInfo.addClass("success");
			return true;
		}
		//if it's NOT valid
		else{
			login_email.addClass("error1");
			loginemailInfo.text("Type a valid e-mail please");
			loginemailInfo.addClass("error1");
			return false;
		}
	}
	
	function validatePassword(){
		var a = $("#login_password");
	

		//it's NOT valid
		if(login_password.val().length <8){
			login_password.addClass("error1");
			loginPasswordInfo.text("At least 8 character are required.");
			loginPasswordInfo.addClass("error1");
			return false;
		}
		//it's valid
		else{			
			login_password.removeClass("error1");
			loginPasswordInfo.text("Password is valid.");
			loginPasswordInfo.removeClass("error1");
			loginPasswordInfo.addClass("success");
			return true;
		}
	}
	
	
	
	
	}
	
	
	function validate_forget()
	{
	
	
		
		var form = $("#forgetForm");	
		
	
		var forget_email = $("#forget_email");
		var forgetEmailInfo = $("#forgetEmailInfo");
	
		
	
		
		//On click		
		forget_email.focus(function() {  
			
			
		
		} );
		
		
		
		//On blur
		
		forget_email.blur(validateEmail);
		
	
	
		forget_email.keyup(validateEmail);
		
		
		//On Submitting
		form.submit(function(){
			if(validateEmail())
				return true
			else
				return false;
		});
		
	
	
	
	
	
	
	//validation functions
	function validateEmail(){
		//testing regular expression
		var a = $("#forget_email").val();
		var filter = email_reg_exp;
		//if it's valid email
		if(filter.test(a)){
			forget_email.removeClass("error1");			
			forgetEmailInfo.text("E-mail address is valid!");
			forgetEmailInfo.removeClass("error1");
			forgetEmailInfo.addClass("success");
			return true;
		}
		//if it's NOT valid
		else{
			forget_email.addClass("error1");
			forgetEmailInfo.text("Type a valid e-mail please");
			forgetEmailInfo.addClass("error1");
			return false;
		}
	}
	

	
	}
	
	
	
	function validate_reset()
	{
		
		
		
		
		var form = $("#resetForm");	
		
	
		var new_password = $("#new_password");
		var newpasswordInfo = $("#newpasswordInfo");
	
		
		
		var confirm_password = $("#confirm_password");
		var confirmpasswordInfo = $("#confirmpasswordInfo");
		
		
	
		
		//On click		
		
	
		
		new_password.focus(function() {  
		
		} );
		
		confirm_password.focus(function() {  
			
		} );
		
		
		
		//On blur
		
		new_password.blur(validatePass1);
		confirm_password.blur(validatePass2);
		
	
	
		new_password.keyup(validatePass1);
		confirm_password.keyup(validatePass2);
		
		
		//On Submitting
		form.submit(function(){
			if(validatePass1() & validatePass2())
				return true
			else
				return false;
		});
		
	

		
		function validatePass1(){
		var a = $("#new_password");
		var b = $("#confirm_password");

		//it's NOT valid
		if(new_password.val().length <7){
			new_password.addClass("error1");
			newpasswordInfo.text("At least 8 character are required.");
			newpasswordInfo.addClass("error1");
			return false;
		}
		//it's valid
		else{			
			new_password.removeClass("error1");
			newpasswordInfo.text("New Password is valid.");
			newpasswordInfo.removeClass("error1");
			newpasswordInfo.addClass("success");
			validatePass2();
			return true;
		}
	}
	function validatePass2(){
		var a = $("#new_password");
		var b = $("#confirm_password");
		//are NOT valid
		
		if(confirm_password.val().length <7){
			confirm_password.addClass("error1");
			confirmpasswordInfo.text("At least 8 character are required.");
			confirmpasswordInfo.addClass("error1");
			return false;
		}
		//it's valid
		else{	
					if( new_password.val() != confirm_password.val() ){
						confirm_password.addClass("error1");
						confirmpasswordInfo.text("Passwords doesn't match!");
						confirmpasswordInfo.addClass("error1");
						return false;
					}
					//are valid
					else{
						confirm_password.removeClass("error1");
						confirmpasswordInfo.text("Confirm password is match.");
						confirmpasswordInfo.removeClass("error1");
						confirmpasswordInfo.addClass("success");
						return true;
					}
		}
		
	}
	
	
		
	}
	
	
	function validate_change_passsword()
	{
		
		
		
		
		var form = $("#changepasswordForm");	
		
	
		//var old_password = $("#old_password");
		//var oldpasswordInfo = $("#oldpasswordInfo");
		
		
		var new_password = $("#new_password");
		var newpasswordInfo = $("#newpasswordInfo");
	
		
		
		var confirm_password = $("#confirm_password");
		var confirmpasswordInfo = $("#confirmpasswordInfo");
		
		
	
		
		//On click		
		
		//old_password.focus(function() {  
		
		//} );
		
		new_password.focus(function() {  
		
		} );
		
		confirm_password.focus(function() {  
			
		} );
		
		
		
		//On blur
		//old_password.blur(validatePass3);
		new_password.blur(validatePass1);
		confirm_password.blur(validatePass2);
		
	
		//old_password.keyup(validatePass3);
		new_password.keyup(validatePass1);
		confirm_password.keyup(validatePass2);
		
		
		//On Submitting
		form.submit(function(){
			if(validatePass1() & validatePass2())
				return true
			else
				return false;
		});
		
	
	
	
/*	function validatePass3(){
		var a = $("#old_password");
		

		//it's NOT valid
		if(old_password.val().length <7){
			old_password.addClass("error1");
			oldpasswordInfo.text("At least 8 characters is required.");
			oldpasswordInfo.addClass("error1");
			return false;
		}
		//it's valid
		else{			
			old_password.removeClass("error1");
			oldpasswordInfo.text("Old Password is valid.");
			oldpasswordInfo.removeClass("error1");
			oldpasswordInfo.addClass("success");
			validatePass1();
			return true;
		}
	}*/
	
	
	
		
		function validatePass1(){
		var a = $("#new_password");
		var b = $("#confirm_password");

		//it's NOT valid
		if(new_password.val().length <7){
			new_password.addClass("error1");
			newpasswordInfo.text("At least 8 character are required.");
			newpasswordInfo.addClass("error1");
			return false;
		}
		//it's valid
		else{			
			new_password.removeClass("error1");
			newpasswordInfo.text("New Password is valid.");
			newpasswordInfo.removeClass("error1");
			newpasswordInfo.addClass("success");
			validatePass2();
			return true;
		}
	}
	function validatePass2(){
		var a = $("#new_password");
		var b = $("#confirm_password");
		//are NOT valid
		
		
		if(confirm_password.val().length <7){
			confirm_password.addClass("error1");
			confirmpasswordInfo.text("At least 8 character are required.");
			confirmpasswordInfo.addClass("error1");
			return false;
		}
		//it's valid
		else{	
		
		
				if( new_password.val() != confirm_password.val() ){
					confirm_password.addClass("error1");
					confirmpasswordInfo.text("Passwords doesn't match!");
					confirmpasswordInfo.addClass("error1");
					return false;
				}
				//are valid
				else{
					confirm_password.removeClass("error1");
					confirmpasswordInfo.text("Confirm password is match.");
					confirmpasswordInfo.removeClass("error1");
					confirmpasswordInfo.addClass("success");
					return true;
				}
				
		}
		
		
	}
	
	
		
	}
	
	
	
	function validate_edit_account()
	{
	
	
		
		var form = $("#editForm");
		
		var user_name = $("#user_name");
		var user_nameInfo = $("#user_nameInfo");
		
		var first_name = $("#first_name");
		var first_nameInfo = $("#first_nameInfo");
		
		
		var last_name = $("#last_name");
		var last_nameInfo = $("#last_nameInfo");
	
		var email = $("#email");
		var emailInfo = $("#emailInfo");
		
	
		
		//On click		
		email.focus(function() {  
		
		} );
		
		last_name.focus(function() {  
		
		} );
		
		first_name.focus(function() {  
			
		} );
		
		user_name.focus(function() {  
		
 		} );
		
		
	
		
		
		
		
		//On blur
		first_name.blur(validateFirstName);		
		last_name.blur(validateLastName);		
		user_name.blur(validateUserName);
		email.blur(validateEmail);
		
		
		
		//On key up
		first_name.keyup(validateFirstName);		
		last_name.keyup(validateLastName);		
		//user_name.keyup(validateUserName);
		//email.keyup(validateEmail);
	
		
		
		
		
		//On Submitting
		form.submit(function(){
			if(validateFirstName() & validateLastName()  & validateEmail() & validateUserName())
				return true
			else
				return false;
		});
		
	
	
	
	
	//validation functions
	
	
	function checkAvailability()
	{	
		
		var email = $("#email");
		var emailInfo = $("#emailInfo");
	
           
                // show our holding text in the validation message space
                emailInfo.removeClass('error1');
				emailInfo.html('<img src="'+baseThemeUrl+'/images/ajax-loader.gif" height="16" width="16" /> checking availability...');
				
			
          var res = $.ajax({						
						type: 'POST',
                        url: baseUrl+'home/checkemailavailability',
                        data: 'email=' + email.val(),
                        dataType: 'json', 
						cache: false,
						async: false                     
                    }).responseText;
		  
	
				return res;	
              
        
	}
	
	
	function validateEmail()
	{
	
		//testing regular expression
		var a = $("#email").val();
		var filter = email_reg_exp;
		
		
		if(a=='')
		{
			email.addClass("error1");
			emailInfo.text("Sorry, you must specify an E-mail address");
			emailInfo.addClass("error1");
			
			return false;
			
		}
		
		
		//if it's valid
		else{
			
				//if it's valid email
				if(filter.test(a)){
			email.removeClass("error1");
			//emailInfo.text("E-mail address is valid!");
			emailInfo.html("<br/>");
			emailInfo.removeClass("error1");
			emailInfo.addClass("success");
			
			
			var chk=checkAvailability();
			
			var obj = jQuery.parseJSON(chk);

			if(obj.ok==true)
			{				
				
				email.removeClass("error1");
				//emailInfo.text("E-mail address is available!");
				emailInfo.html("<br/>");
				emailInfo.addClass("success");
				
				return true;
			}
			else
			{
				email.addClass("error1");
				emailInfo.text("E-mail address is not available!");
				emailInfo.addClass("error1");
				return false;
			}
			
			
		}
		
				//if it's NOT valid
			else{
					email.addClass("error1");
					emailInfo.text("Type a valid E-mail");
					emailInfo.addClass("error1");
					return false;
				}
		}
		
	}
	
	
	
	
	
	function validateFirstName()
	{	
		
		var a = $("#first_name").val();
		var filter = alpha;
			
		if(first_name.val()=='')
		{
			first_name.addClass("error1");
			first_nameInfo.text("Enter First name!");
			first_nameInfo.addClass("error1");
			
			return false;
			
		}
		//if it's NOT valid
		else if(first_name.val().length < 4){
			first_name.addClass("error1");
			first_nameInfo.text("We want names with more than 4 letters!");
			first_nameInfo.addClass("error1");
			return false;
		}
		
		
		//if it's valid
		else{
			
			
			
			//if it's valid number
			if(filter.test(a)){
				first_name.removeClass("error1");
				first_nameInfo.text("First name is valid");
				first_nameInfo.removeClass("error1");
				first_nameInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				first_name.addClass("error1");
				first_nameInfo.text("Enter valid first name.");
				first_nameInfo.addClass("error1");
				return false;
			}
			
			
		}
	}
	
	
	
	function validateLastName()
	{	
		
		var a = $("#last_name").val();
		var filter = alpha;
			
		if(last_name.val()=='')
		{
			last_name.addClass("error1");
			last_nameInfo.text("Enter Last name!");
			last_nameInfo.addClass("error1");
			
			return false;
			
		}
		//if it's NOT valid
		else if(last_name.val().length < 4){
			last_name.addClass("error1");
			last_nameInfo.text("We want names with more than 4 letters!");
			last_nameInfo.addClass("error1");
			return false;
		}
		
		
		//if it's valid
		else{
			
			
			
			//if it's valid number
			if(filter.test(a)){
				last_name.removeClass("error1");
				last_nameInfo.text("Last name is valid");
				last_nameInfo.removeClass("error1");
				last_nameInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				last_name.addClass("error1");
				last_nameInfo.text("Enter valid Last name.");
				last_nameInfo.addClass("error1");
				return false;
			}
			
			
		}
	}
	
	
	
	function checkUsernameAvailability()
	{	
		
		var user_name = $("#user_name");
		var user_nameInfo = $("#user_nameInfo");
	
           
              // show our holding text in the validation message space
             user_nameInfo.removeClass('error1');
			user_nameInfo.html('<img src="'+baseThemeUrl+'/images/ajax-loader.gif" height="16" width="16" /> checking availability...');

          var res = $.ajax({						
						type: 'POST',
                        url: baseUrl+'home/checkusernameavailability',
                        data: 'user_name=' + user_name.val(),
                        dataType: 'json', 
						cache: false,
						async: false                     
                    }).responseText;
		  
				return res;	
	}
	
	
	function validateUserName()
	{	
		
		var a = $("#user_name").val();
		var filter = profilename;
			
		if(user_name.val()=='')
		{
			user_name.addClass("error1");
			user_nameInfo.text("Please use at least 3 characters");
			user_nameInfo.addClass("error1");
			
			return false;
			
		}
		//if it's NOT valid
		else if(user_name.val().length < 4){
			user_name.addClass("error1");
			user_nameInfo.text("Please use at least 4 characters");
			user_nameInfo.addClass("error1");
			return false;
		}
		
		
		//if it's valid
		else{			
			//if it's valid number
			if(filter.test(a)){
				
				var chk=checkUsernameAvailability();			
				var obj = jQuery.parseJSON(chk);			
			
				
				if(obj.ok==true)
				{						
					user_name.removeClass("error1");
					//user_nameInfo.text("Username is available!");
					user_nameInfo.html("<br/>");
					user_nameInfo.removeClass("error1");
					user_nameInfo.addClass("success");					
					return true;
				}
				else
				{				
					user_name.removeClass("error1");
					user_nameInfo.text("Username is not available!");
					user_nameInfo.addClass("error1");
					return false;
				}			
				
			}
			//if it's NOT valid
			else{
				user_name.addClass("error1");
				user_nameInfo.text("Enter valid user name.");
				user_nameInfo.addClass("error1");
				return false;
			}
			
			
		}
	}
	
	
	

	
	}
//
//	
//	function validateMobile()
//	{
//		
//		//testing regular expression
//		var a = $("#mobile_no").val();
//		var filter = number;
//		
//		if(mobile_no.val()!='')
//		{
//		/*if(mobile_no.val()=='')
//		{
//			mobile_no.addClass("error1");
//			mobile_noInfo.text("Enter valid 10 digit mobile number!");
//			mobile_noInfo.addClass("error1");
//			
//			return false;
//			
//		}
//	
//		else*/ if(mobile_no.val().length <= 9 )
//		{		
//			mobile_no.addClass("error1");
//			mobile_noInfo.text("Enter valid 10 digit mobile number!");
//			mobile_noInfo.addClass("error1");
//			return false;
//		} 
//		
//		else if(mobile_no.val().length > 10 )
//		{		
//			mobile_no.addClass("error1");
//			mobile_noInfo.text("Enter valid 10 digit mobile number!");
//			mobile_noInfo.addClass("error1");
//			return false;
//		} 
//		
//		else {
//			
//			//if it's valid number
//			if(filter.test(a)){
//				mobile_no.removeClass("error1");
//				mobile_noInfo.text("Valid mobile number.");
//				mobile_noInfo.removeClass("error1");		
//				mobile_noInfo.addClass("success");
//				return true;
//			}
//			//if it's NOT valid
//			else{
//				mobile_no.addClass("error1");
//				mobile_noInfo.text("Enter valid 10 digit mobile number.");
//				mobile_noInfo.addClass("error1");
//				return false;
//			}
//		}
//		
//		} else { return true; }
//	}
//	
//	function validatePhone()
//	{
//		
//		//testing regular expression
//		var a = $("#phone_no").val();
//		var filter = number;
//		
//		
//	
//		
//		
//		
//	 if(phone_no.val().length > 1 )
//	 {
//			//if it's valid number
//			if(filter.test(a))
//			{
//				
//				
//				
//				 if(phone_no.val().length > 15 )
//				{		
//					phone_no.addClass("error1");
//					phone_noInfo.text("Enter valid and less than 15 digit phone number!");
//					phone_noInfo.addClass("error1");
//					return false;
//				} 
//				
//				
//				 else { phone_no.removeClass("error1");
//						phone_noInfo.text("Valid phone number.");
//						phone_noInfo.removeClass("error1");		
//						phone_noInfo.addClass("success");
//						return true;
//						
//				 }
//		
//		   }
//		   //if it's NOT valid
//			else{
//				phone_no.addClass("error1");
//				phone_noInfo.text("Enter valid phone number.");
//				phone_noInfo.addClass("error1");
//				return false;
//			}
//		   
//		
//		}
//		
//		else
//		{
//			return true;	
//		}
//			
//		
//	}
//	
//	
//	function validateZipcode()
//	{
//		
//		//testing regular expression
//		var a = $("#zip_code").val();
//		var filter = alphanum;
//		
//		if(zip_code.val()=='')
//		{
//			zip_code.addClass("error1");
//			zip_codeInfo.text("Enter valid postal code number!");
//			zip_codeInfo.addClass("error1");
//			
//			return false;
//			
//		}
//	
//		else if(zip_code.val().length < 4 )
//		{		
//			zip_code.addClass("error1");
//			zip_codeInfo.text("Enter valid postal code number!");
//			zip_codeInfo.addClass("error1");
//			return false;
//		} 
//		
//		else {
//			
//			//if it's valid number
//			if(filter.test(a)){
//				zip_code.removeClass("error1");
//				zip_codeInfo.text("Valid postal code number.");
//				zip_codeInfo.removeClass("error1");	
//				zip_codeInfo.addClass("success");
//				return true;
//			}
//			//if it's NOT valid
//			else{
//				zip_code.addClass("error1");
//				zip_codeInfo.text("Enter valid postal code number.");
//				zip_codeInfo.addClass("error1");
//				return false;
//			}
//		}
//	}
//	
//
//	
	



	
	function validate_offer_task()
	{
		
		
		
		var form = $("#offerTaskForm");	
		
	
		var offer_amount = $("#offer_amount");
		var offer_amountInfo = $("#offer_amountInfo");
		var offer_amountTR = $("#offer_amountTR");
		
		var task_comment = $("#task_comment");
		var task_commentInfo = $("#task_commentInfo");
		var task_commentTR = $("#task_commentTR");
		
		
		
		//On click		
		offer_amount.focus(function() {  
			
			offer_amountTR.addClass('field_main'); 
			task_commentTR.removeClass('field_main');
		} );
		
		task_comment.focus(function() {  
			offer_amountTR.removeClass('field_main'); 
			task_commentTR.addClass('field_main');
		} );
		
		
		
		
		//On blur
		
		offer_amount.blur(validateAmount);
		//task_comment.blur(validateComment);
	
		offer_amount.keyup(validateAmount);
		//task_comment.keyup(validateComment);
		
		
		
		//On Submitting
		form.submit(function(){
			if(validateAmount() & validateComment())
				return true
			else
				return false;
		});
		
	
	
		
	
	
	
		function validateAmount()
		{
		
		//testing regular expression
		var a = $("#offer_amount").val();
		var filter = amount;
		
		if(offer_amount.val()=='')
		{
			offer_amount.addClass("error1");
			offer_amountInfo.text("Enter valid offer amount!");
			offer_amountInfo.addClass("error1");
			
			return false;
			
		}
	
		else if(offer_amount.val().length < 1 )
		{		
			offer_amount.addClass("error1");
			offer_amountInfo.text("Enter valid offer amount!");
			offer_amountInfo.addClass("error1");
			return false;
		} 
		
		
		
		else {
			
			//if it's valid number
			if(filter.test(a)){
				offer_amount.removeClass("error1");
				offer_amountInfo.text("Valid offer amount.");
				offer_amountInfo.removeClass("error1");		
				offer_amountInfo.addClass("success");
				return true;
			}
			//if it's NOT valid
			else{
				offer_amount.addClass("error1");
				offer_amountInfo.text("Enter valid offer amount.");
				offer_amountInfo.addClass("error1");
				return false;
			}
		}
	}
	
	
	
		function checkcontentemail()
		{	
			
				var task_comment = $("#task_comment");
				var task_commentInfo = $("#task_commentInfo");
		
			   
					// show our holding text in the validation message space
					
				
			  var res = $.ajax({						
							type: 'POST',
							url: baseUrl+'task/checkcontentemail',
							data: 'task_comment=' + task_comment.val(),
							dataType: 'json', 
							cache: false,
							async: false                     
						}).responseText;
			  
		
					return res;	
				  
			
		}
	
	
	
		function validateComment()
		{
			//it's NOT valid
			if(trim(task_comment.val()).length < 10){
				task_commentInfo.addClass("error1");
				task_commentInfo.text("Enter valid Description.");
				return false;
			}
			//it's valid
			else{		
			
			
					   	
			var chk=checkcontentemail();
			
			var obj = jQuery.parseJSON(chk);
			
			
			
				if(obj.ok==false)
				{	
				
						task_commentInfo.addClass("error1");
						task_commentInfo.text("You can not write email address in message!");
						task_commentInfo.addClass("error1");					
						
						return false;
					
					
					
				}
				else
				{
					
					task_commentInfo.removeClass("error1");
					task_commentInfo.text("Offer Description is valid.");
					task_commentInfo.addClass("success");
					return true;
				}
				
				
				
			}
		}
		
	}
	
	
	function validate_askquestion()
	{
		
		
		
		var form = $("#askQuestionForm");	
		
	
	
		
		var task_comment = $("#task_comment");
		var task_commentInfo = $("#task_commentInfo");
		var task_commentTR = $("#task_commentTR");
		
		
		
	
		task_comment.focus(function() {  
			task_commentTR.addClass('field_main');
		} );
		
		
		
		
		//On blur
		
		
		//task_comment.blur(validateComment);
	
		
		//task_comment.keyup(validateComment);
		
		
		
		//On Submitting
		form.submit(function(){
			if(validateComment())
				return true
			else
				return false;
		});
		
		
		
		
		function checkcontentemail()
		{	
			
			var task_comment = $("#task_comment");
			var task_commentInfo = $("#task_commentInfo");
		
			   
					// show our holding text in the validation message space
					
				
			  var res = $.ajax({						
							type: 'POST',
							url: baseUrl+'task/checkcontentemail',
							data: 'task_comment=' + task_comment.val(),
							dataType: 'json', 
							cache: false,
							async: false                     
						}).responseText;
			  
		
					return res;	
				  
			
		}
	
	
		function validateComment()
		{
			
			var a = task_comment.val();
			var filter = content_email;
		
			
			//it's NOT valid
			if(trim(task_comment.val()).length < 10){
				task_commentInfo.addClass("error1");
				task_commentInfo.text("Enter valid Comment.");
				return false;
			}
		
			
			//it's valid
			else{		
			
			
			   	
			var chk=checkcontentemail();
			
			var obj = jQuery.parseJSON(chk);
			
			
			
				if(obj.ok==false)
				{	
				
					task_commentInfo.addClass("error1");
						task_commentInfo.text("You can not write email address in message!");
						task_commentInfo.addClass("error1");
						
						
						return false;
					
					
					
				}
				else
				{
					
						task_commentInfo.removeClass("error1");
						task_commentInfo.text("Comment is valid.");
						task_commentInfo.addClass("success");
						return true;
						
				}
			}
		}
		
	}
	
	
	function validate_postmessage()
	{
		
		
		
		var form = $("#posMessageForm");	
		
	
	
		
		var task_comment = $("#task_comment");
		var task_commentInfo = $("#task_commentInfo");
		var task_commentTR = $("#task_commentTR");
		
		
		
	
		task_comment.focus(function() {  
			task_commentTR.addClass('field_main');
		} );
		
		
		
		
		//On blur
		
		
	//	task_comment.blur(validateComment);
	
		
		//task_comment.keyup(validateComment);
		
		
		
		//On Submitting
		form.submit(function(){
			if(validateComment())
				return true
			else
				return false;
		});
		
	
	
	
		function validateComment()
		{
			//it's NOT valid
			if(trim(task_comment.val()).length < 10){
				task_commentInfo.addClass("error1");
				task_commentInfo.text("Enter valid Comment.");
				return false;
			}
			//it's valid
			else{			
				task_commentInfo.removeClass("error1");
				task_commentInfo.text("Comment is valid.");
				task_commentInfo.addClass("success");
				return true;
			}
		}
		
	}
	
	

//});