<?php
# AuthWordpress.php
# Copyright (C) 2007 Heriniaina Eugene <hery@serasera.org>
# Inspired by the AuthPress of Robla 
# Originally from http://codex.wordpress.org/User:RobLa/bbPress_Auth_for_MediaWiki
# Version 0.9.0 February 2007
# Authenticate MediaWiki users against WordPress
#

/***
The one done by Robla had a problem with table prefix so I had to change
the class to add a new query function

Installation:
	Just drop this file in extensions
	and add these few lines in your LocalSettings.php


require_once( 'extensions/AuthWordpress.php' );
$wgAuth = new AuthWordpress();
$wgAuth->setAuthWordpressTablePrefix('wp_');
$wgAuth->setAuthWordpressDBServer ('CHANGEME');    // wordpress host (eg. localhost)
$wgAuth->setAuthWordpressDBName('DBNAME');         // wordpress database
$wgAuth->setAuthWordpressUser('DBUSER');           // wordpress db username
$wgAuth->setAuthWordpressPassword('DBPASS');       // wordpress db password




Todo: 
	shared session with wordpress



**/

# To use this
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# http://www.gnu.org/copyleft/gpl.html





require_once('AuthPlugin.php');


class AuthWordpress extends AuthPlugin {
	
	var $_AuthWordpressTablePrefix="wp_";
	var	$_AuthWordpressDBServer;
	var	$_AuthWordpressDBName;
	var	$_AuthWordpressUser;
	var	$_UseSeparateAuthWordpressDB = false;
	var	$_AuthWordpressPassword;
	var $_AuthWordpressDBconn = -1;
	
	function AuthWordpress () {
		global $wgDBserver, $wgDBname, $wgDBuser, $wgDBpassword;

		$this->_AuthWordpressDBServer=$wgDBserver;
		$this->_AuthWordpressDBName=$wgDBname;
		$this->_AuthWordpressUser=$wgDBuser;
		$this->_AuthWordpressPassword=$wgDBpassword;
	}

	function setAuthWordpressTablePrefix ( $prefix ) {
		$this->_AuthWordpressTablePrefix=$prefix;
	}

	function getAuthWordpressUserTableName () {
		
		return $this->_AuthWordpressTablePrefix."users";
	}

	function setAuthWordpressDBServer ($server) {
		$this->_UseSeparateAuthWordpressDB=true;
		$this->_AuthWordpressDBServer=$server;
	}

	function setAuthWordpressDBName ($dbname) {
		$this->_UseSeparateAuthWordpressDB=true;
		$this->_AuthWordpressDBName=$dbname;
	}

	function setAuthWordpressUser ($user) {
		$this->_UseSeparateAuthWordpressDB=true;
		$this->_AuthWordpressUser=$user;
	}

	function setAuthWordpressPassword ($password) {
		$this->_UseSeparateAuthWordpressDB=true;
		$this->_AuthWordpressPassword=$password;
	}

	function &getAuthWordpressDB () {
		Return $this->getAuthWordpressDB =
			new Database($this->_AuthWordpressDBServer,
						$this->_AuthWordpressUser,
						$this->_AuthWordpressPassword, 
						$this->_AuthWordpressDBName	); 
	}

	/* Interface documentation copied in from AuthPlugin */
	/**
	 * Check whether there exists a user account with the given name.
	 * The name will be normalized to MediaWiki's requirements, so
	 * you might need to munge it (for instance, for lowercase initial
	 * letters).
	 *
	 * @param string $username
	 * @return bool
	 * @access public
	 */
	function userExists( $username ) {
		$dbr =& $this->getAuthWordpressDB();

		/* I had to use query instead of selectrow because of prefix */

		$sql = "SELECT * FROM " . $this->getAuthWordpressUserTableName() . " WHERE user_login=".$dbr->addQuotes($username);

		//$sql = "SELECT * FROM " . $this->getAuthWordpressUserTableName();

		$res = $this->query($sql,
				       "AuthWordpress::userExists" );

		if($res) {

			return true;
		} else {
			return false;
		}
	}
	

	function query($sql, $fname) {
		$dbr =& $this->getAuthWordpressDB();
		$res = $dbr->query( $sql, $fname );

		$obj = $dbr->fetchObject( $res );
		$dbr->freeResult( $res );
		return $obj;
	}
	/**
	 * Check if a username+password pair is a valid login.
	 * The name will be normalized to MediaWiki's requirements, so
	 * you might need to munge it (for instance, for lowercase initial
	 * letters).
	 *
	 * @param string $username
	 * @param string $password
	 * @return bool
	 * @access public
	 */
	function authenticate( $username, $password ) {
		$dbr =& $this->getAuthWordpressDB();

		$sql = "SELECT user_pass FROM " . $this->getAuthWordpressUserTableName() . 
				       " WHERE user_login=".$dbr->addQuotes($username);
		$res = $this->query($sql,
				       "AuthWordpress::authenticate" );
		if( $res && ( $res->user_pass == MD5( $password ))) {
			return true;
		} else {
			return false;
		}
	}
		    
	
	/**
	 * Modify options in the login template.
	 *
	 * @param UserLoginTemplate $template
	 * @access public
	 */
	function modifyUITemplate( &$template ) {
		$template->set( 'usedomain', false );
		$template->set( 'useemail', false );
		$template->set( 'create', false );
	}

	/**
	 * Set the domain this plugin is supposed to use when authenticating.
	 *
	 * @param string $domain
	 * @access public
	 */
	function setDomain( $domain ) {
		$this->domain = $domain;
	}

	/**
	 * Check to see if the specific domain is a valid domain.
	 *
	 * @param string $domain
	 * @return bool
	 * @access public
	 */
	function validDomain( $domain ) {
		# Override this!
		return true;
	}

	/**
	 * When a user logs in, optionally fill in preferences and such.
	 * For instance, you might pull the email address or real name from the
	 * external user database.
	 *
	 * The User object is passed by reference so it can be modified; don't
	 * forget the & on your function declaration.
	 *
	 * @param User $user
	 * @access public
	 */
	function updateUser( &$user ) {

		$dbr =& $this->getAuthWordpressDB();
		
		$res = $this->query("SELECT user_nicename, user_email FROM " . $this->getAuthWordpressUserTableName() . 
				       " WHERE user_login=".
				         $dbr->addQuotes($user->mName),
				       "AuthWordpress::updateUser" );
		
		if($res) {
			$user->setEmail( $res->user_email );
			$user->setRealName( $res->user_nicename );
		}

		return true;
	}


	/**
	 * Return true if the wiki should create a new local account automatically
	 * when asked to login a user who doesn't exist locally but does in the
	 * external auth database.
	 *
	 * If you don't automatically create accounts, you must still create
	 * accounts in some way. It's not possible to authenticate without
	 * a local account.
	 *
	 * This is just a question, and shouldn't perform any actions.
	 *
	 * @return bool
	 * @access public
	 */
	function autoCreate() {
		return true;
	}
	
	/**
	 * Set the given password in the authentication database.
	 * Return true if successful.
	 *
	 * @param string $password
	 * @return bool
	 * @access public
	 */
	function setPassword( $password ) {
		# we probably don't want users using MW to change password
		return false;
	}

	/**
	 * Update user information in the external authentication database.
	 * Return true if successful.
	 *
	 * @param User $user
	 * @return bool
	 * @access public
	 */
	function updateExternalDB( $user ) {
		# we probably don't want users using MW to change other stuff
		return false;
	}

	/**
	 * Add a user to the external authentication database.
	 * Return true if successful.
	 *
	 * @param User $user
	 * @param string $password
	 * @return bool
	 * @access public
	 */
	function addUser( $user, $password ) {
		# disabling
		return false;
	}


	/**
	 * Return true to prevent logins that don't authenticate here from being
	 * checked against the local database's password fields.
	 *
	 * This is just a question, and shouldn't perform any actions.
	 *
	 * @return bool
	 * @access public
	 */
	function strict() {
		return true;
	}

	/**
	 * Check to see if external accounts can be created.
	 * Return true if external accounts can be created.
	 * @return bool
	 * @access public
	 */
	function canCreateAccounts() {
		return false;
	}

	
	/**
	 * When creating a user account, optionally fill in preferences and such.
	 * For instance, you might pull the email address or real name from the
	 * external user database.
	 *
	 * The User object is passed by reference so it can be modified; don't
	 * forget the & on your function declaration.
	 *
	 * @param User $user
	 * @access public
	 */
	function initUser( &$user ) {
		/* User's email is already authenticated, because:
		 * A.  They have valid bbPress account
		 * B.  bbPress emailed them the password
		 * C.  They are logged in (presumably using that password
		 * If something changes about the bbPress email verification,
		 * then this function might need changing, too
		 */
		$user->mEmailAuthenticated = wfTimestampNow();

		/* Everything else is in updateUser */
		$this->updateUser( $user );
	}

	/**
	 * If you want to munge the case of an account name before the final
	 * check, now is your chance.
	 */


	function getCanonicalName ( $username ) {

		// connecting to MediaWiki database for this check 		
		$dbr =& wfGetDB( DB_SLAVE );
		
		$res = $dbr->selectRow('user',
				       array("user_name"),
				       "lower(user_name)=lower(".
				         $dbr->addQuotes($username).")",
				       "AuthWordpress::getCanonicalName" );
		
		if($res) {
			return $res->user_name;
		} else {
			return $username;
		}

/*
		$dbr =& $this->getAuthWordpressDB();
		
		$sql = "SELECT user_login FROM " . $this->getAuthWordpressUserTableName() . 
				       " WHERE lower(user_login)=lower(".
				         $dbr->addQuotes($username) . ")";
		$res = $this->query($sql,
				       "AuthWordpress::getCanonicalName" );
		if($res) {
			return $res->user_login;
		} else {
			return $username;
		}
*/
	}
}


// this action is executed just before the invocation of the WordPress authentication process
add_action('wp_authenticate','checkTheUserAuthentication');

function checkTheUserAuthentication() {

     $username=$_POST['log'];
     $password=$_POST['pwd'];

    // try to log into the external service or database with username and password
    $ext_auth = try2AuthenticateExternalService($username,$password);

    // if external authentication was successful
    if($ext_auth) {

         // find a way to get the user id
         $user_id = username_exists($username);
         // userdata will contain all information about the user
         $userdata = get_userdata($user_id);
         $user = set_current_user($user_id,$username);

         // this will actually make the user authenticated as soon as the cookie is in the browser
         wp_set_auth_cookie($user_id);
         // the wp_login action is used by a lot of plugins, just decide if you need it
        do_action('wp_login',$userdata->ID);

        // you can redirect the authenticated user to the "logged-in-page", define('MY_PROFILE_PAGE',1); f.e. first
        header("Location:".get_page_link(MY_PROFILE_PAGE));
    }

}
?>