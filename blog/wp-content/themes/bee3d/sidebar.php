<?php
/**
 * The sidebar containing the main widget area
 *
 * If no active widgets are in the sidebar, hide it completely.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>

	<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
		<div class="v-left-listing"> 
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
                </div>
	<?php endif; ?>

        <?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
            <div class="v-left-listing"> 
                    <?php dynamic_sidebar( 'sidebar-2' ); ?>
            </div><!-- .first -->
	<?php endif; ?>

	<?php if ( is_active_sidebar( 'sidebar-3' ) ) : ?>
            <div class="v-left-listing"> 
                    <?php dynamic_sidebar( 'sidebar-3' ); ?>
            </div><!-- .second -->
	<?php endif; ?>
