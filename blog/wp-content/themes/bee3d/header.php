<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>


 <!-- Bootstrap -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/default.css" rel="stylesheet">
     <link href="<?php echo get_template_directory_uri(); ?>/css/reset.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
   
    
    
    <?php /* <link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]--> */ ?>
<?php wp_head(); ?>
    
     <!-- jQuery (necessary for Bootstrap's JavaScript plugins)
    <script src="https://code.jquery.com/jquery.js"></script> -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
    
</head>

<body <?php //body_class(); ?>>
    
    
    
    <!-- Top Navigation -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
         <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri(); ?>/images/header-logo.png" alt="<?php bloginfo( 'name' ); ?>" width="103"  height="35"/></a>
        </div>
          
          
          
          
          
        <div class="navbar-collapse collapse">
         
            <?php wp_nav_menu( array( 'menu_class' => 'nav navbar-nav',  'theme_location' => 'primary','container' => false ) ); ?>
            <?php wp_nav_menu( array( 'menu_class' => 'nav navbar-nav navbar-right',  'theme_location' => 'toprightmenu','container' => false ) ); ?>
            
            
        </div><!--/.nav-collapse -->
      </div>
    </div>
<!-- End Top Navigation -->



<!-- *************************************************************************************** -->

<?php if ( (is_front_page() && is_home()) || is_front_page() || is_home() ) { ?>
<div class="wrapper row8 page-padding-top">
   
   <div class="container">
   		<div class="inner-header">
            <div class="row">
            <div class=" margin-bottom-20 col-md-6 col-sm-6 br-black "> 
              <div class="text-center margin-top-20"> 
                <img src="<?php echo get_template_directory_uri(); ?>/images/inner_logo.png" alt="Logo" class="img-responsive" />
              </div>
               <div class="text-center head-social"	>
                    <ul class="social-link">
                        <li class="fb"> <a href="#"> &nbsp;</a></li>
                        <li class="tw"> <a href="#" > &nbsp;</a></li>
                        <li class="gm"> <a href="#" > &nbsp;</a></li>
                        <li class="in"> <a href="#"> &nbsp;</a></li>
                    </ul>
                </div>
            
            </div>
            <div class="col-md-6 col-sm-6"> 
            	<div class="inner_top_link">
                	<ul>
                    	<li> <a href="#"> <img src="<?php echo get_template_directory_uri(); ?>/images/learning_hive.png" alt="Learing HIVE" class="img-responsive" /> </a> </li>
                        <li> <a href="#"> <img src="<?php echo get_template_directory_uri(); ?>/images/design_hive.png" alt="Learing HIVE" class="img-responsive" /> </a> </li>
                        <li> <a href="#"> <img src="<?php echo get_template_directory_uri(); ?>/images/challenge_hive.png" alt="Learing HIVE"  class="img-responsive" /> </a> </li>
                    </ul>
                </div>
                <div class="newletter">
                	<div class="title">SIGN UP FOR iHIVE 3D NEWS </div>
                    <div class="title_sub">   COMMUNICATE, COLLABORATE ALL THINGS 3D </div>
                    <div> 
                    	<input type="text" class="m-wrap medium pull-left" />
                    	<a href="#"  class="btn yellow-btn"> Submit</a>
                    </div>
                </div>
            
            </div>
            </div>
        </div>
    
   </div>
   
</div>
<?php } else { ?>
    
<div style="padding-top: 37px;">&nbsp;</div>

<?php } ?>
<!-- *************************************************************************************** -->


<!-- *************************************************************************************** -->
<div class="wrapper row9">
	<div class="container">
    	 <div class="video-detail">
         
    
    
