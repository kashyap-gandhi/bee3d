<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bee3d_dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5=sXMAZYkbLTW[%y):$Vxi[0[[_q->A.K7a]zgiG|74lM(~r;,4dS0t!sLr4>{5z');
define('SECURE_AUTH_KEY',  'uq&Nev9}#5kVz-yB?$bEZNCc[RRk)Cs=hux1OjKa[^R&@k]>T:9/HT8IroMEJCpd');
define('LOGGED_IN_KEY',    'p! &1x[/J;#D*4RjjAqV&o?]VAsr{Jzh.%*Oo1sKZ)^FS$>EZxocgcz|4P_kUNk#');
define('NONCE_KEY',        '>TN;{fAoYsUW%md?n:.k{unC$B{|z1f1i|@VY}f|4(MrO=T*h``2&_.QPA/?</Py');
define('AUTH_SALT',        'h*o/h*J|I].GLgju|g6UO5NSYNl@T6#z:DCpJVsNJfyO$nD~7A;L,_B{;]^@B$D<');
define('SECURE_AUTH_SALT', '3f/AUN*woY-1bbsNWnGI]}04-`eLcnV&IMR%:H,]c1-Qgp*k;H%_<Dx{e4Q$Dj^F');
define('LOGGED_IN_SALT',   '3yAo5q:j=2%Xi6JYzBw1n42F|ZTD7go#)eR-o(YW@}m*1IUKAn= `tyf,I*zN`+]');
define('NONCE_SALT',       'u~8;_7aiIxjVr`<m_n1>2i42lzV5(?J|/,{n:{8{S;o!OYJ^%2`3x+PX.gqxd&-C');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'iweb_xblog_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
