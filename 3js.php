<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>three.js webgl - loaders - OBJ loader</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
		<style>
			body {
				font-family: Monospace;
				background-color: #000;
				color: #fff;
				margin: 0px;
				overflow: hidden;
			}
			#info {
				color: #fff;
				position: absolute;
				top: 10px;
				width: 100%;
				text-align: center;
				z-index: 100;
				display:block;
			}
			#info a, .button { color: #f00; font-weight: bold; text-decoration: underline; cursor: pointer }
		</style>
	<style type="text/css"></style></head>

	<body>
		<div id="info">
			<br><a href="http://threejs.org/" target="_blank">three.js</a>
			<br><br>Female Croupier 2013-03-26 by <a href="http://www.manuelwieser.com/">Manuel Wieser</a>
		</div>

		<script src="http://public.manuelwieser.com/viewer/three.min.js"></script>
		<script src="http://public.manuelwieser.com/viewer/TrackballControls.js"></script>
		<script src="http://public.manuelwieser.com/viewer/OBJLoader.js"></script>
		<script src="http://public.manuelwieser.com/viewer/Detector.js"></script>
		<script src="http://public.manuelwieser.com/viewer/stats.min.js"></script>

		<script>

			var container, stats;

			var camera, scene, renderer;

			var mouseX = 0, mouseY = 0;

			var windowHalfX = window.innerWidth / 2;
			var windowHalfY = window.innerHeight / 2;


			init();
			animate();


			function init() {

				container = document.createElement( 'div' );
				document.body.appendChild( container );

				camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 2000 );
				camera.position.z = 100;

				controls = new THREE.TrackballControls( camera );
			controls.rotateSpeed = 5.0;
			controls.zoomSpeed = 5;
			controls.panSpeed = 2;
			controls.noZoom = false;
			controls.noPan = false;
			controls.staticMoving = true;
			controls.dynamicDampingFactor = 0.3;

				// scene

				scene = new THREE.Scene();

				// var ambient = new THREE.AmbientLight( 0x101030 );
				var ambient = new THREE.AmbientLight( 0xffffff );
				scene.add( ambient );

				// var directionalLight = new THREE.DirectionalLight( 0xffeedd );
				// directionalLight.position.set( 0, 0, 1 ).normalize();
				// scene.add( directionalLight );

				// texture

				var texture = new THREE.Texture();

				var loader = new THREE.ImageLoader();
				loader.addEventListener( 'load', function ( event ) {

					texture.image = event.content;
					texture.needsUpdate = true;
					texture.magFilter = THREE.NearestFilter;
					texture.minFilter = THREE.NearestMipMapLinearFilter;

				} );
				loader.load( 'http://public.manuelwieser.com/female_croupier_2013-03-26.png' );

				// model

				var loader = new THREE.OBJLoader();
				loader.addEventListener( 'load', function ( event ) {

					var object = event.content;

					object.traverse( function ( child ) {

						if ( child instanceof THREE.Mesh ) {

							child.material.map = texture;

						}

					} );

					object.scale = new THREE.Vector3( 25, 25, 25 );
					scene.add( object );

				});
				//loader.load( 'http://localhost/bee3dhive/upload/design/SW3DPS-M10S1000FS-DEFAULT11.wrl' );
				loader.load('http://ihive3d.com/upload/design_orig/66782design_9_1.wrl');

				//

				renderer = new THREE.WebGLRenderer();
				renderer.setSize( window.innerWidth, window.innerHeight );
				container.appendChild( renderer.domElement );

				document.addEventListener( 'mousemove', onDocumentMouseMove, false );

				//

				window.addEventListener( 'resize', onWindowResize, false );

			}

			function onWindowResize() {

				windowHalfX = window.innerWidth / 2;
				windowHalfY = window.innerHeight / 2;

				camera.aspect = window.innerWidth / window.innerHeight;
				camera.updateProjectionMatrix();

				renderer.setSize( window.innerWidth, window.innerHeight );

			}

			function onDocumentMouseMove( event ) {

				mouseX = ( event.clientX - windowHalfX ) / 2;
				mouseY = ( event.clientY - windowHalfY ) / 2;

			}

			//

			function animate() {

				requestAnimationFrame( animate );
				render();

			}

			function render() {

				controls.update();

				camera.lookAt( scene.position );

				renderer.render( scene, camera );

			}

		</script><div><canvas width="1920" height="936" style="width: 1920px; height: 936px;"></canvas></div>



<div id="window-resizer-tooltip" style="display: block;"><span class="tooltipTitle">Window size: </span><span class="tooltipWidth" id="winWidth">1920</span> x <span class="tooltipHeight" id="winHeight">1040</span><br><span class="tooltipTitle">Viewport size: </span><span class="tooltipWidth" id="vpWidth">1920</span> x <span class="tooltipHeight" id="vpHeight">936</span></div></body></html>