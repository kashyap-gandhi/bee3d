<?php
$themurl = base_url() . getThemeName();
?>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53b182c86f415a5c"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->


<link href="<?php echo $themurl; ?>/css/owl.carousel.css" rel="stylesheet">
<link href="<?php echo $themurl; ?>/css/owl.theme.css" rel="stylesheet">
<script src="<?php echo $themurl; ?>/js/jquery-1.9.1.min.js"></script> 
<script src="<?php echo $themurl; ?>/js/owl.carousel.js"></script>




<!-- second rotation code -->

<link href="<?php echo $themurl; ?>/rotation1/css/rotate.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $themurl; ?>/rotation1/js/jquery.ui.touch-punch.min.js"></script>

<?php
if ($design_gallery_type == 1) {
    ?>
    <script type="text/javascript" src="<?php echo $themurl; ?>/rotation1/js/script.js"></script>

<?php } ?>
<!-- end of second rotation code-->

<script>
    $(document).ready(function() {

        $("#owl-demo").owlCarousel({
            autoPlay: 3000,
            items : 6,
            itemsDesktop : [1199,2],
            itemsDesktopSmall : [979,2]
        });



    });

    function like_dislike(like_status,design_id)
    {
        if(design_id)
        {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>design/like_dislike",         //the script to call to get data          
                data: {like_status : like_status,design_id:design_id},
                // data: $("#member_form").serialize(),
                dataType: '',                //data format      
                success: function(data)          //on receive of reply
                {
                    if(data == 'success')
                    {
                        var orig_total_likes = $("#total_likes").val();

                        if(like_status == "0")
                        {

                            var new_like_count =  parseFloat(orig_total_likes) - parseFloat(1);
                            //alert(new_like_count);
                            $(".like-cls").html("Like");
                            $(".like-cls").attr("onclick","like_dislike('1','<?php echo $design_id; ?>')");
                        }
                        else
                        {
                            var new_like_count =  parseFloat(orig_total_likes) + parseFloat(1);
                            $(".like-cls").html("Dislike");
                            $(".like-cls").attr("onclick","like_dislike('0','<?php echo $design_id; ?>')");
                        }

                        $("#total_likes").val(new_like_count);
                        $("#total_likes_count").html(new_like_count);
                        // alert(like_status);



                    }
                } 
            });
        }
        else
        {
            //alert('rr');
        }
    }
</script>

    
    
    <script>


function loadScript(url, callback) {
 
    var script = document.createElement("script")
    script.type = "text/javascript";

    if (script.readyState) { //IE
        script.onreadystatechange = function () {
            if (script.readyState == "loaded" || script.readyState == "complete") {
                script.onreadystatechange = null;
                callback();
            }
        };
    } else { //Others
        script.onload = function () {
            callback();
        };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}

function hidef()
{
    setTimeout(function(){
        
         $("div.sculpteo_viewer iframe").addClass('xcd');
                 
                 
         var iBody = $("iframe.xcd").contents().find("body");
         alert(iBody.find("module-remotectrl-content").html());
         
         
         
         $("div.sculpteo_viewer iframe.xcd div.viewer-logo").css({'display':'none !important','visibility': 'hidden','background': 'none'});
       
         alert($("div.sculpteo_viewer iframe.xcd").contents().find("div.viewer-logo").attr('title'));
        
       
    }, 10000);
    
   
   
}
//hidef();
</script>

<!-- *************************************************************************************** -->
<style>
    .row8{
        background: none !important;
    }
   div.sculpteo_viewer iframe html body div.viewer-logo, .logog {
        display:none !important;
                 visibility: hidden;
                 background: none;}
</style>
<div class="wrapper row8 page-padding-top">



</div>

<!-- *************************************************************************************** -->
<div class="wrapper row4">
    <div class="container">
        <div class="main-contain">
            <h1 class="text-center white"><span class="font-light">BROWSE, SHARE, SELL AND CUSTOMIZE DESIGNS  </h1>
            <div class="video-gallery">
                <div class="row">
                    <div class="col-md-3 col-sm-3 text-center"> 
                        <div class="margin-bottom-15">
                        <!-- <img src="<?php echo $themurl; ?>/images/upload_download.png" alt="Logo" class="img-responsive" />-->
                            <div class="gallery_link">
                                <h1 class="top-link"> <a href="<?php echo site_url("design/add_me") ?>"> Upload your Design </a> </h1>
                                <?php
                                if (get_authenticateUserID() > 0) {
                                    if (get_authenticateUserID() != $user_id) {
                                        ?>
                                        <h1> <a href="<?php echo site_url("design/buy/" . base64_encode($design_id)) ?>"> Download a Design </a> </h1>
                                        <?php
                                    } else {
                                        ?>
                                        <h1> <a href="javascript://"> Download a Design </a> </h1>
                                        <?php
                                    }
                                } else {
                                    ?>
                                        <h1> <a href="<?php echo site_url("login/a/". base64_encode(current_url())) ?>"> Download a Design </a> </h1>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9">
                        <div id="owl-demo" class="owl-carousel">
                            <?php
                            if ($user_design_galley) {
                                $i = 1;
                                $j = 0;
                                foreach ($user_design_galley as $udg) {
                                    if ($j == 0) {
                                        echo '<div class="item">';
                                        $j = 1;
                                    }


                                    $is_gallery_image = 0;

                                    $counter_id = $udg->design_counter_id;
                                    if ($counter_id > 0) {

                                        $counter_detail = get_counter_detail($counter_id);

                                        if ($counter_detail) {
                                            $main_counter_image = $counter_detail->gallery_image;
                                            if ($main_counter_image != '' && file_exists(base_path() . 'upload/gallery/' . $main_counter_image)) {
                                                $is_gallery_image = 1;
                                                ?>
                                                <span> <a href="<?php echo site_url("design/" . $udg->design_slug); ?>"  style="height: 127px;">
                                                        <img alt="" src="<?php echo base_url() ?>upload/gallery/<?php echo $main_counter_image; ?>"  />
                                                 </a>
                                                <a href="<?php echo site_url("design/" . $udg->design_slug); ?>">
                                                        
                                                <?php echo $udg->design_title; ?> </a></span>
                                                <?php
                                            }
                                        }
                                    } else {

                                        if ($udg->attachment_name != "" && file_exists(base_path() . "upload/design/" . $udg->attachment_name)) {
                                            $is_gallery_image = 1;
                                            ?>
                            <span> <a href="<?php echo site_url("design/" . $udg->design_slug); ?>" style="height: 127px;"> 
                                                    <img src="<?php echo base_url() . "upload/design/" . $udg->attachment_name; ?>" alt="" /> 
                                              </a>
                                            <a href="<?php echo site_url("design/" . $udg->design_slug); ?>"> 
                                                   
                                            <?php echo $udg->design_title; ?>  </a></span>
            <?php
            }
        }



        if ($is_gallery_image = 1) {
            if ($i >= 2) {
                echo "</div>";
                $j = 0;
                $i = 0;
            }
            $i++;
        }
    }
}
?>


                            <!--                            <div class="item"> 
                            <span> <a href="#"> <img src="<?php echo $themurl; ?>/images/120x85.jpg" alt="Image"> AGP6OR ORING GRIPPER LOREM IPSUM </a> </span> 
                            <span> <a href="#"> <img src="<?php echo $themurl; ?>/images/120x85.jpg" alt="Image"> AGP6OR ORING GRIPPER LOREM IPSUM</a> </span>  
                            </div>
                            <div class="item"> 
                            <span> <a href="#"> <img src="<?php echo $themurl; ?>/images/120x85.jpg" alt="Image"> DUAL VEE </a> </span> 
                            <span> <a href="#"> <img src="<?php echo $themurl; ?>/images/120x85.jpg" alt="Image"> DUAL VEE</a> </span>  
                            </div>
                            <div class="item"> 
                            <span> <a href="#"> <img src="<?php echo $themurl; ?>/images/120x85.jpg" alt="Image"> LOVE ANIMATION  </a> </span> 
                            <span> <a href="#"> <img src="<?php echo $themurl; ?>/images/120x85.jpg" alt="Image">  LOVE ANIMATION </a> </span>  
                            </div>
                            <div class="item"> 
                            <span> <a href="#"> <img src="<?php echo $themurl; ?>/images/120x85.jpg" alt="Image"> AGP6OR ORING GRIPPER LOREM IPSUM  </a> </span> 
                            <span> <a href="#"> <img src="<?php echo $themurl; ?>/images/120x85.jpg" alt="Image"> AGP6OR ORING GRIPPER LOREM IPSUM </a> </span>  
                            </div>
                            <div class="item"> 
                            <span> <a href="#"> <img src="<?php echo $themurl; ?>/images/120x85.jpg" alt="Image"> DUAL VEE </a> </span> 
                            <span> <a href="#"> <img src="<?php echo $themurl; ?>/images/120x85.jpg" alt="Image"> DUAL VEE</a> </span>  
                            </div>
                            <div class="item"> 
                            <span> <a href="#"> <img src="<?php echo $themurl; ?>/images/120x85.jpg" alt="Image"> AGP6OR ORING GRIPPER LOREM IPSUM   </a> </span> 
                            <span> <a href="#"> <img src="<?php echo $themurl; ?>/images/120x85.jpg" alt="Image"> AGP6OR ORING GRIPPER LOREM IPSUM </a> </span>  
                            </div>
                            <div class="item"> 
                            <span> <a href="#"> <img src="<?php echo $themurl; ?>/images/120x85.jpg" alt="Image"> DUAL VEE </a> </span> 
                            <span> <a href="#"> <img src="<?php echo $themurl; ?>/images/120x85.jpg" alt="Image"> DUAL VEE</a> </span>  
                            </div>
                            -->                         </div>

                    </div>
                </div>
            </div> 
        </div>	
    </div>
</div>
<!-- *************************************************************************************** -->
<div class="wrapper row9">

    <div class="video-detail v-detail-bg">
        <div class="container">
            
            
            <div id="ajax_msg_error1" style="display: none;"></div>
             
            <?php 
		if($msg!= '' )
		{
			if($msg == 'successr')
			{?>
			<div class="alert alert-success" style="margin: 16px 21px 16px 0px;">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<?php  echo "Thank you for adding rating."; ?>
			
		</div>
			<?php }
                         
		}
		?>
            
            
           
            
            
            
            
            
            
            <div class="col-md-12" > 
                <div class="v-detail-title">
                    <h2><?php echo $design_title; ?> </h2>
                    <h3> BY <strong> <?php if($design_by_profile_name!='' ) { echo anchor('bee/'.$design_by_profile_name,$design_by,' style="color: #333333;" '); } else { echo $design_by; } ?></strong> - <?php echo date($site_setting->date_format, strtotime($design_date)); ?></h3>
                </div>	
            </div>
        </div>

        <div class="container">
            <div class="col-md-9"> 

                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="video-box">
                                    
                                    
                                    <?php
                                    $is_image = 0;


                                    $counter_id = $design_counter_id;
                                    if ($counter_id > 0) {
                                        $counter_detail = get_counter_detail($counter_id);

                                        if ($counter_detail) {
                                            $main_counter_image = $counter_detail->gallery_image;
                                            if ($main_counter_image != '' && file_exists(base_path() . 'upload/gallery/' . $main_counter_image)) {
                                                $is_image = 1;
                                                ?>
                                                <img alt="<?php echo $main_counter_image; ?>" src="<?php echo base_url() ?>upload/gallery/<?php echo $main_counter_image; ?>" class="video-border img-responsive" />
                                                <?php
                                            }
                                        }
                                    } else {



                                        if ($design_gallery_type == 1) {

                                            $desing_image_list = $this->design_model->get_all_images_of_design($design_id);


                                            if ($desing_image_list) {
                                                $i = 0;
                                                echo "<div class='menu'><ul class='list'>";
                                                foreach ($desing_image_list as $dl) {
                                                    if ($dl->attachment_name != "" && is_file(base_path() . "upload/design/" . $dl->attachment_name)) {

                                                        $is_image = 1;

                                                        if ($i == 0) {
                                                            echo "<li>";
                                                        } else {
                                                            echo "<li style='display:none'>";
                                                        }
                                                        echo "<img src='" . base_url() . "upload/design/" . $dl->attachment_name . "' class='video-border img-responsive' />";
                                                        echo "</li>";
                                                    }
                                                    $i++;
                                                }
                                                echo "</ul></div>";
                                            }
                                        } else if ($design_gallery_type == 2) {




                                            if ($main_3d_image != '') {
                                                $is_image = 1;
                                                echo "<div>";

                                                $ext = pathinfo(base_path() . 'upload/design_orig/' . $main_3d_image, PATHINFO_EXTENSION);
                                                $extra_file = array('fbx', 'c4d', 'max', 'dae', 'ma', 'mb', 'lwo', 'lws', 'lxo', 'x', 'dxf', 'xsi', '3ds', 'obj', 'ply', 'skp', 'stl', 'iges', 'x3d', 'ac', 'pov', 'rwx', 'sldasm', 'sldprt', 'wrl', 'prt', 'asm');
                                                if (in_array($ext, $extra_file)) {
                                                    ?>
                    
                    
                                                 <script type="text/javascript" src="http://www.sculpteo.com/en/js/sculpteo.widget.js"></script>
                                                    
                                                    
                                                    
                                               
                                                        
                                                    <div class="sculpteo_viewer" src="<?php echo base_url() ?>upload/design_orig/<?php echo $main_3d_image; ?>" style="width: 535px; height: 400px; position: relative"></div>
                                                        
                                                <?php } else { ?>
                                                    <img src="<?php echo base_url() ?>upload/design/<?php echo $main_3d_image; ?>" alt="" class="video-border img-responsive">
                                                    <?php
                                                }

                                                echo "</div>";
                                            }
                                        } else {


                                            if ($main_image != '') {
                                                echo "<div>";
                                                $is_image = 1;

                                                $ext = pathinfo(base_path() . 'upload/design_orig/' . $main_image, PATHINFO_EXTENSION);
                                                $extra_file = array('fbx', 'c4d', 'max', 'dae', 'ma', 'mb', 'lwo', 'lws', 'lxo', 'x', 'dxf', 'xsi', '3ds', 'obj', 'ply', 'skp', 'stl', 'iges', 'x3d', 'ac', 'pov', 'rwx', 'sldasm', 'sldprt', 'wrl', 'prt', 'asm');
                                                if (in_array($ext, $extra_file)) {
                                                    ?>
                    
                    
                                                     <script type="text/javascript" src="http://www.sculpteo.com/en/js/sculpteo.widget.js"></script>
                                                    
                                                    
                                                   
                                                        
                                                    <div class="sculpteo_viewer" src="<?php echo base_url() ?>upload/design_orig/<?php echo $main_image; ?>" style="width: 535px; height: 400px; position: relative"></div>
                                                        
                                                <?php } else {
                                                    ?>
                                                    <img src="<?php echo base_url() ?>upload/design/<?php echo $main_image; ?>" alt="" class="video-border img-responsive">
                                                    <?php
                                                }

                                                echo "</div>";
                                            }
                                        }
                                    }

                                    if ($is_image == 0) {
                                        ?>
                                        <div>
                                            <img src="<?php echo base_url(); ?>upload/no_userimage.png" class="img1 video-border img-responsive" alt="" />
                                        </div>
                                    <?php } ?>
    




                                    <div  class="text-center downlaod-bnt">
                                    <?php
                                    if (get_authenticateUserID() > 0) {
                                        if (get_authenticateUserID() != $user_id) {

                                            $check_already_buy = check_user_buy_design($design_id, get_authenticateUserID());

                                            if ($check_already_buy) {
//echo "already buy";
                                            } else {
                                                ?>
                                                    <a href="<?php echo site_url('design/buy/' . base64_encode($design_id)); ?>"><img src="<?php echo $themurl; ?>/images/download_file.png" alt="Download File" class="img-responsive" /> </a>
                                                    <?php
                                                }
                                            } else {
//echo own design
                                            }
                                        } else {
                                            ?>
                                            <a href="<?php echo site_url('login/a/'. base64_encode(current_url())); ?>"><img src="<?php echo $themurl; ?>/images/download_file.png" alt="Download File" class="img-responsive" /> </a>
                                        <?php } ?>		
                                    </div>
                                </div>


                            </div>

                            <div class="col-md-3 row">
                                <div class="v-detail-list" >
                                    <ul>
                                        <li><?php echo $design_point; ?> CREDITS </li>
                                        <li> <span id="total_likes_count"><?php echo $design_like_count; ?></span> LIKES 

<?php
//if(get_authenticateUserID() != $user_id)
//{
?>
                                            <div class="pull-right">
<?php
if (get_authenticateUserID() > 0) {
    $chk_like = $this->design_model->check_like_track(get_authenticateUserID(), $design_id);
    if ($chk_like) {
        if ($chk_like->like_status == 1) {
            ?>
                                                            <a class="like-cls" id="dislike" href="javascript://" onclick="like_dislike('0','<?php echo $design_id; ?>');">Dislike &nbsp;</a>
                                                        <?php
                                                    } else {
                                                        ?>
                                                            <a class="like-cls" id="like" href="javascript://" onclick="like_dislike('1','<?php echo $design_id; ?>');">Like &nbsp;</a>
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <a class="like-cls" href="javascript://" onclick="like_dislike('1','<?php echo $design_id; ?>');">Like &nbsp;</a>
                                                    <?php }
                                                    ?>

                                                <?php }
//}?>				
                                                <input type="hidden" name="total_likes" id="total_likes" value="<?php echo $design_like_count; ?>" />				
                                        </li>
                                        
                                        
                                         
                                         
                                         
                                          <?php
                                          $show_label_star=0;
                                    if (get_authenticateUserID() > 0) {
                                        if (get_authenticateUserID() != $user_id) {

                                            $check_already_buy = check_user_buy_design($design_id, get_authenticateUserID());

                                            if ($check_already_buy) { 
                                                
                                                
                                                $check_already_rate = check_user_rate_design($design_id, get_authenticateUserID());
                                                
                                                if($check_already_rate){
                                                    //already rated
                                                } else {
                                                
                                                $show_label_star=1;
                                                
                                                ?>
                                         <li>
                                             <div class="rate-ex2-cnt">
                                                <div id="1" class="rate-btn-1 rate-btn"></div>
                                                <div id="2" class="rate-btn-2 rate-btn"></div>
                                                <div id="3" class="rate-btn-3 rate-btn"></div>
                                                <div id="4" class="rate-btn-4 rate-btn"></div>
                                                <div id="5" class="rate-btn-5 rate-btn"></div>
                                            </div> 
                                             <div class="clearfix" style="padding-top: 8px;padding-bottom: 7px;">
                                             Rate it now: <button type="button" class="btn yellow-btn" style="padding: 3px 15px;height: 26px;font-size: 18px;" onclick="dorate();">Place</button></div>
                                         </li>
                                         <?php } } } } ?>
                                         
                                         <?php if($show_label_star==0){?>
                                         
                                                <li><?php $total_rating=roundDownToHalf($design_total_rating);   ?>
                                                <img src="<?php echo $themurl;?>/rating/<?php echo $total_rating;?>.png" />
                                                
                                        <div style="padding-top: 5px;"><?php echo get_design_total_user_rate($design_id);?> Customer Rating</div></li>
                                         <?php } ?>
                                        
                                        
                                        <li><?php echo $design_view_count; ?> VIEWS</li>

                                        <li><!-- Go to www.addthis.com/dashboard to customize your tools -->
                                            <div class="addthis_sharing_toolbox"></div></li>
                                    </ul>
                                </div>
                                                <?php
                                                if ($get_one_design_tag) {
                                                    ?>
                                    <div class="v-tags">
                                        <h2> Tags </h2>
                                        <dl>          
    <?php
    foreach ($get_one_design_tag as $ot) {
        ?>

                                                <dt><?php echo $ot->tag_name; ?> </dt>
                                                <dd><?php echo get_tag_rel_count($ot->tag_id, 'design'); ?></dd>
                                    <?php } ?>
                                        </dl>

                                    </div>	
<?php } ?>


                            </div>



                        </div>

                    </div>





                    <style>
                        .tree_view ul{
                            list-style: none;
                            padding: 0px;
                        }
                        .tree_view ul li {
                            list-style: none;
                        }
                        .tree_view ul li a{
                            text-transform: capitalize;
                            padding: 10px 0px;
                            font-size: 15px;
                            color: #615858;
                        }
                    </style>

                    <!---tree part--->
<?php if ($design_tree != '') { ?>
                        <div class="v-comment"> 
                            <div class="">
                                <h3>REBOUND</h3>
                                <div class="tree_view"><?php echo $design_tree; ?></div>

                            </div>

                        </div>
<?php } ?>
                    <!---tree part--->



                    <!-- vcommetnt-->
                    <div class="v-comment"> 
                        <div class="">
                            <h3>COMMENTS</h3>
                            <div class="media">
                                <div class="fb-comments" data-href="<?php echo current_url(); ?>" data-numposts="5" data-colorscheme="light"></div>
                            </div>

                        </div>
                    </div>
                    <style>
                        .fb_iframe_widget iframe {
                            height: 165px !important;
                        }
                    </style>
                    <!-- end of commnet-->
                </div>
            </div>   	 
            <div class="col-md-3">
                <div class="v-left-listing"> 

                    <h1>  CHECK OUT THESE DESIGNS TOO!  </h1>
                    <div>
                        <ul>	
                            <?php
                            if ($user_design) {
                                foreach ($user_design as $pd) {


                                    $is_image = 0;
                                    ?>
            
                                    <li>
                                        <a href="<?php echo site_url('design/' . $pd->design_slug); ?>"> <h2> <?php echo $pd->design_title; ?></h2>
                                            <?php
                                            $counter_id = $pd->design_counter_id;
                                            if ($counter_id > 0) {

                                                $counter_detail = get_counter_detail($counter_id);

                                                if ($counter_detail) {
                                                    $main_counter_image = $counter_detail->gallery_image;
                                                    if ($main_counter_image != '' && file_exists(base_path() . 'upload/gallery/' . $main_counter_image)) {
                                                        $is_image = 1;
                                                        ?>
                                                        <img alt="" src="<?php echo base_url() ?>upload/gallery/<?php echo $main_counter_image; ?>" class="img-responsive minheight20 imgcenter" />
                                                            
                                                    <?php
                                                    }
                                                }
                                            } else {

                                                $main_design_image = $this->design_model->get_one_main_design_image($pd->design_id);
                                                if ($main_design_image != '' && file_exists(base_path() . 'upload/design/' . $main_design_image)) {
                                                    $is_image = 1;
                                                    ?>
                                                    <img src="<?php echo base_url() ?>upload/design/<?php echo $main_design_image; ?>" alt="" class="img-responsive minheight20 imgcenter"  />     
                                                <?php }
                                            } ?>
            
                                            <?php if ($is_image == 0) { ?>
                                                <img src="<?php echo base_url(); ?>upload/no_userimage.png" alt="" class="img-responsive minheight20 imgcenter" />     
                                            <?php } ?>		
                                        </a>
                                    </li>
                                    <?php
                                } //==foreach
                            } else {
                                
                                if ($popular_design) {
                                    foreach ($popular_design as $pd) {
                                        $is_image = 0;
                                        ?>
                                        <li>
                                            <a href="<?php echo site_url('design/' . $pd->design_slug); ?>"> <h2> <?php echo $pd->design_title; ?></h2> 
                                                <?php
                                                $counter_id = $pd->design_counter_id;
                                                if ($counter_id > 0) {

                                                    $counter_detail = get_counter_detail($counter_id);

                                                    if ($counter_detail) {
                                                        $main_counter_image = $counter_detail->gallery_image;
                                                        if ($main_counter_image != '' && file_exists(base_path() . 'upload/gallery/' . $main_counter_image)) {
                                                            $is_image = 1;
                                                            ?>
                                                            <img alt="" src="<?php echo base_url() ?>upload/gallery/<?php echo $main_counter_image; ?>" class="img-responsive minheight20 imgcenter" />
                                                                
                                                        <?php
                                                        }
                                                    }
                                                } else {
                                                    $main_design_image = $this->design_model->get_one_main_design_image($pd->design_id);

                                                    if ($main_design_image != '' && file_exists(base_path() . 'upload/design/' . $main_design_image)) {
                                                        $is_image = 1;
                                                        ?>
                                                        <img src="<?php echo base_url() ?>upload/design/<?php echo $main_design_image; ?>" alt="" class="img-responsive minheight20 imgcenter" />     
                                                    <?php }
                                                } ?>
                
                
            <?php if ($is_image == 0) { ?>
                                                    <img src="<?php echo base_url(); ?>upload/no_userimage.png" alt="" class="img-responsive minheight20 imgcenter" />     
                                                <?php } ?>		
                                            </a>
                                        </li>
            <?php
        }
    }
}
?>
    
    
                        </ul>
                            
                    </div>

                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div> 
    

    <link href="<?php echo $themurl;?>/rating/rating.css" rel="stylesheet" />

    <input type="hidden" id="edit_rating" value="0" />


<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


 <script>
var design_id='<?php echo $design_id; ?>';
        
    $(document).ready(function(){    

                $(".rateme").show();
                

                $('.rate-btn').hover(function(){
                    $('.rate-btn').removeClass('rate-btn-hover');
                    var therate = $(this).attr('id');
                    $("#edit_rating").val(therate);
                    for (var i = therate; i >= 0; i--) {
                        $('.rate-btn-'+i).addClass('rate-btn-hover');
                    };
                });
                
    });            
                function dorate(){
                
                $("#ajax_msg_error1").hide();
                
                design_id=parseInt(design_id);
                
            var therate = parseInt($("#edit_rating").val());
            
            if(therate>5){
                therate=5;
            } else if(therate<0){
                therate=0;
            } 
            
            
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('design/post_rating')?>",
                    data: {design_id:design_id,rating:therate},
                    dataType: 'json',
                    success: function(response, textStatus, xhr) {
                        if(response.error!="")
                        {
                                              $("#ajax_msg_error1").html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+response.error+'</div>').show();
                            return false;
                        }
                        else
                        {	
                             window.location.href = '<?php echo site_url('design/'.$design_slug);?>'+'/'+response.msg;
                        }
                    }
                });
                
                }
                
                
                
    
 </script>