
<!-- *************************************************************************************** -->
<div class="wrapper row4 page-padding-top">
    <div class="container">
    	<div class="main-contain">
        	<h1 class="text-center font-light white"> <strong><a href="<?php echo site_url('design/search');?>" class="white"> BROWSE AND DOWNLOAD </strong>DESIGNS FROM OUR...  </a>  </h1>
            <h2 class="text-center white"> LOOKING FOR A GREAT 3D DESIGN TO PRINT?  SELECT A CATEGORY AND START BROWSING! </h2>
     	</div>	
    </div>
</div>
<!-- *************************************************************************************** -->
<div class="wrapper row5">
    <div class="container main-contain">
    	<div class=" print-block ">
        	<div class="center-block text-center">
            	<a href="<?php echo site_url('design/search');?>"><img src="<?php echo base_url().getThemeName(); ?>/images/3d-img.png" alt="hexagon"  class="img-responsive"/></a>
            </div>
<!--            <div>
            	<hr>
            	<h1 class="text-center font-light white"><a href="<?php echo site_url('printer/search');?>" class="white"> 3D PRINTERS  </a> </h1>
            </div>-->
            <div class="product-listing">
            	<ul>
				<?php 
					/*if($latest_printer)
					{
						foreach($latest_printer as $prn)
						{
						$get_user_image = get_user_profile_by_id($prn->user_id);
						if($get_user_image->profile_image  != '' && file_exists(base_path().'upload/user/'.$get_user_image->profile_image))
						{?>
						<li>
                         <a href="<?php echo site_url('printer/search');?>">
							<img alt="<?php echo $prn->store_name; ?>" src="<?php echo base_url()?>upload/user/<?php echo $get_user_image->profile_image;?>" width="152" height="152" >
						</a>
						</li>
						<?php }else{?>
						 <li><a href="<?php echo site_url('printer/search');?>">	<img alt="No Image" src="<?php echo base_url().getThemeName(); ?>/images/152x152.png"></a></li>
                             <?php }?>
						<?php }
					}*/
					?>
				
				<?php /*?>
                	<li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li><?php */?>
				
                </ul>
            </div>
        </div>
    </div>
</div>    
   
<!-- *************************************************************************************** -->
<div class="wrapper row4">
    <div class="container">
		<div class="main-contain margin-bottom-40">
        	<h1 class="text-center font-light  yellow">GOT YOUR OWN DESIGN?   </h1>
            <h4 class="text-center yellow"> 
				<?php if(get_authenticateUserID()!='') {?>
			<a href="<?php echo site_url('video/add_me');?>" class="yellow">
			<strong class="font-itcbold"> UPLOAD </strong> YOUR 3D DESIGN HERE! 
			<?php }
			else
			{?>
			<a href="<?php echo site_url('login');?>" class="yellow"><strong class="font-itcbold"> UPLOAD </strong> YOUR 3D DESIGN HERE! </a>
			<?php }?>
			</h4>
        </div>	
    </div>
</div>