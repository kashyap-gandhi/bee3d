<style>
.error{ color:#FF0000!important;}
</style>
		<!-- responsive table -->
			<link href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>
		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Dashboard</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
						
						<!-- main content -->
						<div class="row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Add Category</h4>
									</div>
									
									<?php if($error!='') { ?>
											<div class="alert alert-danger">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
													<?php  echo $error; ?>
												
											</div>
									 <?php  } ?>
									
									<?php
												$attributes = array('name'=>'design_category','id'=>'design_category','class'=>'');
												echo form_open_multipart('design/add_design_category',$attributes);
											?>
											
									<div class="panel-body">
											
											<div class="form_sep">
												<label for="reg_input_name" class="req">Category Name</label>
												<input type="text" id="category_name" name="category_name" class="form-control" data-required="true" value="<?php echo $category_name;?>">
											</div>
											
											
											<div class="form_sep">
												<label for="reg_select">Parent Category</label>
												<select name="category_parent_id" id="category_parent_id" class="form-control" data-required="true">
													<option value="">--Select Parent Category--</option>
													<?php 
													if($category)
													{
														foreach($category as $cat)
														{?>
														<option value="<?php echo $cat->category_id;?>"><?php echo $cat->category_name;?></option>
														
														<?php }
														
													}
													?>
												</select>
											</div>
											
											<div class="form_sep">
												<label for="reg_textarea_message">Description</label>
												<textarea name="category_description" id="category_description" cols="30" rows="4" class="form-control" ></textarea>
											</div>
											
											<div class="form_sep">
												<label for="reg_input_name">Category Image</label>
												<input type="file" name="category_image" id="category_image" class=""/>
											</div>
											
											<div class="form_sep">
												<label for="reg_select">Status</label>
												<select name="category_status" id="category_status" class="form-control" data-required="true">
													<option value="">--Select Status--</option>
													<option value="1">Active</option>
													<option value="0">Inactive</option>
												</select>
											</div>
											
											<div class="form_sep">
												<input type="submit" name="submit" id="submit" class="btn btn-default" value="Submit"/>
											</div>
										
									</div>
									</form>
								</div>
							</div>
							
						</div>
						
					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
			<div id="footer_space"></div>
		</div>

       
<!-- parsley -->
<script src="<?php echo base_url().getThemeName(); ?>/js/jquery.validate.js"></script>

<script type="text/javascript">
$(document).ready(function(){
//===== Usual validation engine=====//
	$("#design_category").validate({
		rules: {
			category_name: "required"
		}

	});
});	
	
</script>