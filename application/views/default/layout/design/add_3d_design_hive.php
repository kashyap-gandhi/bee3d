<!-- Add fancyBox main JS and CSS files -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . getThemeName(); ?>/js/tag/jquery.tagsinput.css" />
<!-- responsive lightbox -->
<?php /* ?> <link rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/magnific-popup/magnific-popup.css">
  <?php */ ?><script type="text/javascript" src="<?php echo base_url() ?>tinymce/tinymce.min.js"></script>
<!-- aditional stylesheets -->
<script type="text/javascript">
    $(document).ready(function(){

        $("#3dmainimage").hide();

<?php if ($design_gallery_type == 1) { ?>
            $("#addimg").show();
<?php } elseif ($design_gallery_type == 2) { ?>
            $("#addimg").hide();
            $("#3dmainimage").show();
<?php } else { ?>
            $("#addimg").hide();
<?php } ?>
    
    
    <?php if($design_id=='' || $design_id==0) { ?>
        
        $("#design_gallery_type option[value=2]").prop( "selected", true );
        $("#addimg").hide();
            $("#3dmainimage").show();
        
        <?php } ?>


        $("#design_gallery_type").on('change',function(){

            $("#3dmainimage").hide();

            var type=$("#design_gallery_type option:selected").val();
            
            
            var count_image=$("#gallery_grid li a img").length;
            
            var dpros=0;
            if(count_image>1 && type!=1){
                
               dpros=1;
            } 
            
            if(dpros==0){
                if(type==1){
                    $("#addimg").show();
                } else {
                    $("#addimg").hide();
                }

                if(type==2){
                    $("#3dmainimage").show();
                } 
            
            } else {
                $("#design_gallery_type option[value=<?php echo $design_gallery_type; ?>]").prop( "selected", true );
                alert('Only one image is allowed in selected gallery type, before you go further please delete extra images.');
            }


        });

        ///====

        $("#design_form").submit(function(){

            var check=true;

            document.getElementById('err1').style.display='none';
            document.getElementById('err2').style.display='none';
            document.getElementById('err3').style.display='none';



            var design_gallery_type = document.getElementById("design_gallery_type").value;



            if(design_gallery_type=='')
            {

                check=false;
                var dv = document.getElementById('err1');
                dv.style.display='block';
                dv.className = "error";
                dv.style.clear = "both";
                dv.style.minWidth = "110px";
                dv.style.margin = "5px";
                dv.innerHTML = 'Please select Gallery Type.';
                return false;

            }




            if(design_gallery_type==2)
            {



                //====for 3D File=====

                if(document.getElementById('prev_3dattachment_name').value == "")
                {

                    if(document.getElementById('3dattachment_name').value == "")
                    {
                        check=false;

                        var dv = document.getElementById('err3');
                        dv.style.display='block';
                        dv.className = "error";
                        dv.style.clear = "both";
                        dv.style.minWidth = "110px";
                        dv.style.margin = "5px";
                        dv.innerHTML = 'Please upload 3D File.';
                        return false;


                    } else {

                        value = document.getElementById('3dattachment_name').value;
                        var t1 = value.substring(value.lastIndexOf('.') + 1,value.length);
                        t1=t1.toLowerCase();

                        if( t1=='fbx' || t1=='c4d' || t1=='max' || t1=='dae' || t1=='ma' || t1=='mb' || t1=='lwo' || t1=='lws' || t1=='lxo' || t1=='x' || t1=='dxf' || t1=='xsi' || t1=='3ds' || t1=='obj' || t1=='ply' || t1=='skp' || t1=='stl' || t1=='iges' || t1=='x3d' || t1=='ac' || t1=='pov' || t1=='rwx' || t1=='sldasm' || t1=='sldprt' || t1=='wrl' || t1=='prt' || t1=='asm'){



                            check=true;

                        } else {

                            check=false;

                            var dv = document.getElementById('err3');
                            dv.style.display='block';
                            dv.className = "error";
                            dv.style.clear = "both";
                            dv.style.minWidth = "110px";
                            dv.style.margin = "5px";
                            dv.innerHTML = 'Please upload valid 3D File.';
                            return false;
                        }
                    }


                    ////===end 3D File



                }
            }



            if(check==true)
            {
                return true;
            }


        });

        ///==

    });


    tinymce.init({selector:'#design_content'});
    function append_div2()
    {
        var design_gallery_type = document.getElementById("design_gallery_type").value;



        if(design_gallery_type=='')
        {

            check=false;
            var dv = document.getElementById('err1');
            dv.style.display='block';
            dv.className = "error";
            dv.style.clear = "both";
            dv.style.minWidth = "110px";
            dv.style.margin = "5px";
            dv.innerHTML = 'Please select Gallery Type.';
            return false;

        }




        if(design_gallery_type==1)
        {


            var tmp_div2 = document.createElement("div");
            tmp_div2.className = "";

            var glry_cnt=document.getElementById('glry_cnt').value;

            /*if(glry_cnt<5)
{*/
            document.getElementById('glry_cnt').value=parseInt(glry_cnt)+1;
            var num=parseInt(glry_cnt)+1;

            tmp_div2.id='galry'+num;
            var content=document.getElementById('more2').innerHTML;
            var str = '<div onclick="remove_gallery_div('+num+')" style="font-weight:normal;cursor:pointer;color:#ff0000; width:100px;">Remove</div><div class="clear"></div>';
            tmp_div2.innerHTML = content +str;

            document.getElementById('add_more2').appendChild(tmp_div2);

    } else {
            alert('You cannot add more than one image. Please change Gallery Type to add more images.')
    }

    }


    function remove_gallery_div(id)
    {
        var element = document.getElementById('galry'+id);
        var add_more = parent.document.getElementById('add_more2');

        element.parentNode.removeChild(element);


        var glry_cnt=document.getElementById('glry_cnt').value;
        document.getElementById('glry_cnt').value=parseInt(glry_cnt)-1;

        document.getElementById('addimg').style.display='block';

    }

    function show_parent_design()
    {
        $('#parent_design_div').slideToggle();
    }

    function delete_design_attachment(id)
    {
        var y = confirm('Are you sure to delete image?');
        if(y)
        {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>design/delete_design_attachment",         //the script to call to get data
                data: {attachment_id: id},
                // data: $("#member_form").serialize(),
                dataType: '',                //data format
                success: function(data)          //on receive of reply
                {
                    if(data == 'success')
                    {
                        //$('#coupon_gallery_span_'+id).removeClass('img_upload');
                        $('#image_attach_'+id).html('');
                    }
                }
            });
        }
        else
        {
            //alert('rr');
        }
    }


</script>
<style>
    #err1,#err2,#err3,#err4 {
        color: red;
    }
</style>
<!-- parsley -->
<!-- responsive table -->
<link href="<?php echo base_url() . getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>
<div id="wrapper_all">
    <?php
    $theme = getThemeName();
    echo $this->load->view($theme . '/layout/common/header_menu', TRUE);
    ?>
    <!-- mobile navigation -->
    <nav id="mobile_navigation"></nav>

    <section id="breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="<?php echo site_url('user/dashboard') ?>">Home</a></li>
                <li><span>Dashboard</span></li>
            </ul>
        </div>
    </section>

    <section class="container clearfix main_section">
        <div id="main_content_outer" class="clearfix">
            <div id="main_content">

                <!-- main content -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <?php if ($design_id > 0) { ?>
                                        Edit
                                        <?php
                                    } else {
                                        ?>
                                        Add
                                    <?php } ?>
                                    Description of 3D Design
                                </h4>
                            </div>
                            <?php if ($error != '') { ?>
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php echo $error; ?>

                                </div>
                            <?php } ?>

                            <div class="panel-body">
                                <?php
                                $attributes = array('name' => 'design_form', 'id' => 'design_form', 'class' => '');
                                echo form_open_multipart('design/add_me', $attributes);
                                ?>
                                <div class="form_sep">
                                    <label for="reg_input_name" class="req">Name of 3D Design</label>
                                    <input type="text" id="design_title" name="design_title" class="form-control" data-required="true" value="<?php echo $design_title; ?>">
                                </div>
                                <div class="form_sep">
                                    <label for="reg_input_email" class="req">Detailed Description of Design</label>
                                    <textarea name="design_content" id="design_content" cols="30" rows="4" class="form-control"><?php echo $design_content; ?></textarea>

                                </div>

                                <div class="form_sep">
                                    <label for="reg_select" ><span class="req" data-toggle="tooltip" data-placement="right auto" title="Category & Sub-Categories For Storage of This Design">Design Category  </span>  <a style="padding-left: 10px;" data-toggle="modal" href="#designcategory" ><span class="glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="left auto" title="Add Category"></span></a> </label>
                                    <select name="design_category" id="design_category" class="form-control" data-required="true">
                                        <option value="">--Select Category--</option>
                                        <?php
                                        echo $category;
                                        /* if($category)
                                          {

                                          foreach($category as $cat)
                                          {?>
                                          <option value="<?php echo $cat->category_id;?>" <?php if($design_category == $cat->category_id){?> selected="selected"<?php }?>><?php echo $cat->category_name;?></option>

                                          <?php }

                                          } */
                                        ?>
                                    </select>
                                </div>

                                <div class="form_sep">
                                    <label for="reg_select" class="req"><span data-toggle="tooltip" data-placement="right auto" title="Select Image type">Select Gallery Type</span></label> <span id="err1"></span>
                                    <select name="design_gallery_type" id="design_gallery_type" class="form-control" data-required="true">
                                        <option value=" ">--Select Gallery Type--</option>
                                        <option value="0" <?php if ($design_gallery_type == 0) { ?> selected="selected"<?php } ?>><?php echo "Normal"; ?></option>
                                        <option value="2" <?php if ($design_gallery_type == 2) { ?> selected="selected"<?php } ?>><?php echo "3D File"; ?></option>
                                        <option value="1" <?php if ($design_gallery_type == 1) { ?> selected="selected"<?php } ?>><?php echo "360 Rotate"; ?></option>


                                    </select>
                                </div>

                                <div class="form_sep">
                                    <label for="reg_textarea_message"><span data-toggle="tooltip" data-placement="right auto" title="An Image to Help  Identify This Design">Design Image</span></label> <span id="err2"></span>
                                    <div id="add_more2">
                                        <div id="more2">
                                            <input type="file" class="form-control" name="attachment_name[]" value=""></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>











                                <div id="addimg" class="add_icon" style="display:none;" ><img src="<?php echo base_url() . getThemeName(); ?>/images/add.png" height="16" width="16" border="0" title="add more" onclick="append_div2();" style="cursor:pointer;" /></div>

                                <nav class="panel_controls" style="display:none;">
                                    <div class="row" style="display:none;">
                                        <div class="col-sm-4">
                                            <span class="gal_lay_change lay_active" id="gal_toGrid"><i class="icon-th"></i></span>
                                            <span class="gal_lay_change" id="gal_toList"><i class="icon-align-justify"></i></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <select name="gal_filter_a" id="gal_filter_a" class="form-control" multiple>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <select name="gal_filter_b" id="gal_filter_b" class="form-control" multiple>
                                            </select>
                                        </div>
                                    </div>
                                </nav>


                                <?php if ($design_images) { ?>
                                    <div class="form_sep">
                                        <ul id="gallery_grid" class="" style="list-style:none;">
                                            <?php foreach ($design_images as $di) { ?>

                                                <li class="mix user_1 nature travel" data-name="Image 11" data-timestamp="1379912400" id="image_attach_<?php echo $di->attachment_id; ?>">

                                                    <?php
                                                    
                                                     if(file_exists(base_path() . 'upload/design_orig/' . $di->attachment_name)){
                      
                    
                                                    
                                                    $ext = pathinfo(base_path() . 'upload/design_orig/' . $di->attachment_name, PATHINFO_EXTENSION);
                                                    $extra_file = array('fbx', 'c4d', 'max', 'dae', 'ma', 'mb', 'lwo', 'lws', 'lxo', 'x', 'dxf', 'xsi', '3ds', 'obj', 'ply', 'skp', 'stl', 'iges', 'x3d', 'ac', 'pov', 'rwx', 'sldasm', 'sldprt', 'wrl', 'prt', 'asm');
                                                    if (in_array($ext, $extra_file)) {
                                                        ?>
                                                    <?php } else { 
                                                        
                                                         if(file_exists(base_path() . 'upload/design/' . $di->attachment_name)){
                                                        ?>
                                                        <a href="#myModal" data-toggle="modal" data-img-url="<?php echo base_url() ?>upload/design_orig/<?php echo $di->attachment_name; ?>">

                                                            <img src="<?php echo base_url() ?>upload/design/<?php echo $di->attachment_name; ?>" class="img-thumbnail" alt="">
                                                        </a>

                                                    <?php } } } ?>

                                                    <br/>
                                                    <a href="javascript://" onclick="delete_design_attachment('<?php echo $di->attachment_id; ?>');">Delete</a>


                                                </li>

                                            <?php } ?>

                                        </ul>
                                    </div>
                                    <div class="clear"></div>
                                <?php } ?>





                                <div class="form_sep" id="3dmainimage">
                                    <label for="reg_textarea_message">3D File</label> <span id="err3"></span>

                                    <input type="file" class="form-control" name="3dattachment_name" id="3dattachment_name" value="">

                                    <div class="clear"></div>
                                </div>


                                <input type="hidden" name="prev_3dattachment_name" id="prev_3dattachment_name" value="<?php echo $prev_3dattachment_name; ?>" />

                                <div class="form_sep" style=" padding-top: 10px;">
                                    <label for="reg_textarea_message" class="req">Number of credits to buy the design</label>
                                    <input type="text" id="design_point" name="design_point" class="form-control" value="<?php echo $design_point; ?>">
                                </div>

                                <div class="form_sep">
                                    <label for="reg_textarea_message"><span data-toggle="tooltip" data-placement="right auto" title="A Unique ID Number For This Design">Reference ID</span></label>
                                    <input type="text" id="design_ref_id" name="design_ref_id" class="form-control" value="<?php echo $design_ref_id; ?>">
                                </div>

                                <div class="form_sep">
                                    <label for="reg_textarea_message"><span data-toggle="tooltip" data-placement="right auto" title="Person Who Originally Created This Design">Design By</span></label>
                                    <input type="text" id="design_by" name="design_by" class="form-control" value="<?php echo $design_by; ?>">
                                </div>

                                <div class="form_sep">
                                    <label for="reg_textarea_message"><span data-toggle="tooltip" data-placement="right auto" title="Check Here if Design Has Been Modified">Design is modified</span></label>
                                    <input type="checkbox" id="design_is_modified" name="design_is_modified" class="form-control" value="1" onclick="show_parent_design();" <?php if ($design_is_modified == 1) { ?> checked="checked"<?php } ?>>
                                </div>

                                <?php
                                if ($design_is_modified == 1) {
                                    $prd = 'style="display:block;"';
                                } else {
                                    $prd = 'style="display:none;"';
                                }
                                ?>
                                <div class="form_sep" id="parent_design_div" <?php echo $prd; ?>>
                                    <label for="reg_select">Parent Design</label>
                                    <select name="parent_design_id" id="parent_design_id" class="form-control" data-required="true">
                                        <option value="">--Select Parent Design--</option>
                                        <?php
                                        if ($alldesign) {
                                            foreach ($alldesign as $dg) {
                                                ?>
                                                <option value="<?php echo $dg->design_id; ?>" <?php if ($parent_design_id == $dg->design_id) { ?> selected="selected"<?php } ?>><?php echo $dg->design_title; ?></option>

                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="form_sep">
                                    <label for="reg_textarea_message"><span data-toggle="tooltip" data-placement="right auto" title="Keywords Used When Searching for Design">Meta Keywords</span></label>
                                    <textarea rows="3" cols="70" class="form-control autosize_textarea" name="design_meta_keyword" id="design_meta_keyword" ><?php echo $design_meta_keyword; ?></textarea>

                                </div>

                                <div class="form_sep" style="display:none;">
                                    <label for="reg_textarea_message">Meta Description</label>
                                    <textarea rows="3" cols="70" class="form-control autosize_textarea" name="design_meta_description" id="design_meta_description" ><?php echo $design_meta_description; ?></textarea>

                                </div>

                                <div class="form_sep" style="display:none;">
                                    <label for="reg_textarea_message">Design Tag</label>
                                    <div id="add_more_tag">
                                        <div id="more_tag">
                                            <input id="tags_1" type="text" class="tags" name="tag_name" value="<?php echo $design_tag; ?>" />
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                
                                
                                
                                <div class="form_sep">
                                    <input type="hidden" name="glry_cnt" id="glry_cnt" value="1" />
                                    <input type="hidden" name="design_id" id="design_id" value="<?php echo $design_id; ?>"/>
                                    <input type="submit" name="submit" id="submit" value="submit" class="btn btn-default"/>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <?php echo $this->load->view($theme . '/layout/common/sidebar', TRUE); ?>
    </section>
    <div id="footer_space"></div>

    <!-- Modal -->
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="text-align:center;" >
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
            <img src="#"/>
        </div>
        <!--<div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>-->
    </div>
</div>



<script type="text/javascript" src="<?php echo base_url() . getThemeName(); ?>/js/tag/jquery.tagsinput.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#tags_1').tagsInput({width:'auto'});

        //===== Usual validation engine=====//

        $('li a').click(function(e) {
            $('#myModal img').attr('src', $(this).attr('data-img-url'));
        });
    });

</script>
