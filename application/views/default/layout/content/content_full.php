<!-- *************************************************************************************** -->
<style>
.font-itcbold a {
    color: #333333!important;
    text-decoration: none;
}
</style>
<div class="wrapper row1 page-padding-top">
   
       <div id="carousel-example-generic" class="carousel slide main-slider" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
  
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="<?php echo base_url().getThemeName(); ?>/images/banner1.jpg" alt="" >
    </div>
     <div class="item">
      <img src="<?php echo base_url().getThemeName(); ?>/images/banner1.jpg" alt="">
    </div>
    
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>
     <!-- /container -->
</div>

<!-- *************************************************************************************** -->
<div class="wrapper row2">
    <div class="container">
		<div class="main-contain">
        	<h1 class="text-center dark-gery"><span class="font-light"> PICK A SUBJECT OF OUR  </span>3D LEARNING VIDEO LIBRARY </h1>
        	<h2 class="text-center dark-gery"> HAS THE AMAZING WORLD OF 3D PRINTING GOT YOU A LITTLE OVERWHELMED? <br/> WE CAN HELP!  SELECT A CATEGORY AND START LEARNING!  </h2>
        </div>	
    </div>
</div>
<!-- *************************************************************************************** -->
<div class="wrapper row3">
	<div class="container">
    	 <div class="video-block">
            	<div class="video-listing">
                	<ul>
                    	<li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                        <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                        <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                        <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                        <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                        <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                        <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                        <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                        <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                        <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                        <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                     </ul>
                 </div>
            </div>
    </div>
</div> 

<!-- *************************************************************************************** -->
<div class="wrapper row2">
    <div class="container">
		<div class="main-contain margin-bottom-20">
        	<h1 class="text-center font-light dark-gery"> GOT A VIDEO TO SHARE?    </h1>
            <h4 class="text-center dark-gery"> <strong class="font-itcbold">
			<?php if(get_authenticateUserID()!='') {?>
			<a href="<?php echo site_url('video/add_me');?>"> UPLOAD </a>
			<?php }
			else{?>
			<a href="<?php echo site_url('login');?>"> UPLOAD </a>
			<?php }?>
			</strong> YOUR OWN VIDEO HERE! </h4>
        </div>	
    </div>
</div>
<!-- *************************************************************************************** -->
<div class="wrapper row4">
    <div class="container">
    	<div class="main-contain">
        	<h1 class="text-center font-light white"> <strong><a href="<?php echo site_url('design/search');?>" class="white"> BROWSE AND DOWNLOAD </strong>DESIGNS FROM OUR...  </a>  </h1>
            <h2 class="text-center white"> LOOKING FOR A GREAT 3D DESIGN TO PRINT?  SELECT A CATEGORY ANS START BROWSING! </h2>
     	</div>	
    </div>
</div>
<!-- *************************************************************************************** -->
<div class="wrapper row5">
    <div class="container main-contain">
    	<div class=" print-block ">
        	<div class="center-block text-center">
            	<a href="<?php echo site_url('design/search');?>"><img src="<?php echo base_url().getThemeName(); ?>/images/3d-img2.png" alt="hexagon"  class="img-responsive"/></a>
            </div>
            <div>
            	<hr>
            	<h1 class="text-center font-light white"><a href="<?php echo site_url('printer/search');?>" class="white"> 3D PRINTERS  </a> </h1>
            </div>
            <div class="product-listing">
            	<ul>
				<?php 
					if($latest_printer)
					{
						foreach($latest_printer as $prn)
						{
						$get_user_image = get_user_profile_by_id($prn->user_id);
						if($get_user_image->profile_image  != '' && file_exists(base_path().'upload/user/'.$get_user_image->profile_image))
						{?>
						<li>
                         <a href="<?php echo site_url('printer/search');?>">
							<img alt="<?php echo $prn->store_name; ?>" src="<?php echo base_url()?>upload/user/<?php echo $get_user_image->profile_image;?>" width="152" height="152" >
						</a>
						</li>
						<?php }else{?>
						 <li><a href="<?php echo site_url('printer/search');?>">	<img alt="No Image" src="<?php echo base_url().getThemeName(); ?>/images/152x152.png"></a></li>
                             <?php }?>
						<?php }
					}
					?>
				
				<?php /*?>
                	<li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li><?php */?>
				
                </ul>
            </div>
        </div>
    </div>
</div>    
   
<!-- *************************************************************************************** -->
<div class="wrapper row4">
    <div class="container">
		<div class="main-contain margin-bottom-40">
        	<h1 class="text-center font-light  yellow">GOT YOUR OWN DESIGN?   </h1>
            <h4 class="text-center yellow"> 
				<?php if(get_authenticateUserID()!='') {?>
			<a href="<?php echo site_url('video/add_me');?>" class="yellow">
			<strong class="font-itcbold"> UPLOAD </strong> YOUR 3D DESIGN  HERE! 
			<?php }
			else
			{?>
			<a href="<?php echo site_url('login');?>" class="yellow"><strong class="font-itcbold"> UPLOAD </strong> YOUR 3D DESIGN  HERE! </a>
			<?php }?>
			</h4>
        </div>	
    </div>
</div>

<!-- *************************************************************************************** -->
<div class="wrapper row6">
    <div class="container main-contain">
    	<div class="challenge-block">
        	<h1 class="text-center font-light  dark-gery">  
				<a href="<?php echo site_url('challenge/search');?>" class="dark-gery">TRY YOUR BRAIN<strong class="dark-gery"> 3D CHALLENGE  </strong></a>
			 </h1>
             <h2 class="text-center dark-gery"> THINK YOU'VE GOT WHAT IT TAKES TO SOLVE ANOTHER MEMBER&lsquo;S 3D CHALLENGE? SELECT A CHALLENGE AND BID ON SOLVING IT! </h2>
             <div>
            	<hr>
                <div class="text-center margin-bottom-30"> <img src="<?php echo base_url().getThemeName(); ?>/images/bullate.png" alt="" /></div>
            	<h1 class="text-center font-light dark-gery"> TOYS + MODELS   </h1>
            </div>
            <div class="product-listing">
            	<ul>
					<?php 
					if($latest_design)
					{
						foreach($latest_design as $res)
						{
						if($res->attachment_name != '' && file_exists(base_path().'upload/design/'.$res->attachment_name))
						{?>
						<li>
                         <a href="<?php echo site_url('design/'.$res->design_slug);?>">
											<img alt="<?php echo $res->design_slug; ?>" src="<?php echo base_url()?>upload/design/<?php echo $res->attachment_name;?>" width="152" height="152">
						</a>
						</li>
						<?php }else{?>
						 <li><a href="<?php echo site_url('design/'.$res->design_slug);?>">	<img alt="No Image" src="<?php echo base_url().getThemeName(); ?>/images/152x152.png"></a></li>
                             <?php }?>
						<?php }
					}
					?>
<?php /*?>                	<li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li><?php */?>
                </ul>
            </div>
            <hr class="wd80">
            <h1 class="text-center font-light dark-gery"> GOT A 3D CHALLENGE?  
				<?php if(get_authenticateUserID()!='') {?>
			<a href="<?php echo site_url('challenge/add_me');?>" class="dark-gery"><span class="font-itcbold"> POST IT HERE! </span></a>
			<?php }
			else
			{?>
			<a href="<?php echo site_url('login');?>" class="dark-gery"><span class="font-itcbold"> POST IT HERE! </span></a>
			<?php }?>
			</h1>
        </div>    
    </div>
</div>    
