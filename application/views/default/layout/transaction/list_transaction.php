	<!-- aditional stylesheets -->
		<!-- responsive table -->
			<link href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>

		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Transaction</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
					
						<!-- main content -->
						<div class="row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Transaction</h4>
									</div>
									<!--<div class="panel_controls">
										<div class="row">
											
											
											<div class="col-sm-12 col-xs-6">
												<label>&nbsp;</label>
												<a href="<?php //echo site_url('design/add_me');?>" class="btn btn-info">Add Design</a>&nbsp;
												<a href="<?php //echo site_url('design/add_design_category');?>" class="btn btn-warning">Add Category</a>
											</div>
										</div>
									</div>-->
									<table id="resp_table" class="table toggle-square" data-filter="#table_search" data-page-size="40">
										<thead>
											<tr>
												<th>Date</th>
												<th>Pay For</th>
												<th>Pay Points</th>
												<!--<th>Action</th>-->
											</tr>
										</thead>
										<tbody>
											<?php if($result)
											{
												foreach($result as $res)
												{?>
											<tr>
												<td><?php echo date($site_setting->date_format,strtotime($res->transaction_date_time));?></td>
												<td>
												<?php 
												if($res->design_id	> 0)
												{
													echo "Design : ".get_design_name($res->design_id);
												}
												else if($res->video_id	> 0)
												{
													echo "Video :".get_video_name($res->video_id);
												}
												else if($res->challenge_id	> 0)
												{
													$release= "";
													if($res->parent_transaction_id>0) {
														$release= " Release";
													}
													$challenge_detail = $this->challenge_model->get_one_challenge($res->challenge_id);
													if($challenge_detail) {
														echo "Challenge".$release." :".$challenge_detail->challenge_title;
													} else{
														echo "Challenge".$release;
													}
													
													
												}
												else
												{
													echo "Buy Credit";
												}
												?>
												</td>
												<td><?php echo $res->pay_points;?></td>
												<!--<td>
													<a class="btn btn-primary" href="<?php echo site_url('design/view_me/'.$res->design_id);?>">View</a>
													<a class="btn btn-success" href="<?php echo site_url('design/edit_me/'.$res->design_id);?>">Edit</a>
													<a class="btn btn-danger" href="javascript://" onclick="delete_design('<?php echo $res->design_id;?>');">Delete</a>
												</td>-->
											</tr>	
											<?php }}
											else
											{?>
											<tr>
												<td colspan="3">No Records Found!!</td>
												
											</tr>
											<?php }?>
											
											
										</tbody>
									
											<tr>
												<td colspan="6" class="text-center">
													<ul class="">
													<?php echo $page_link;?>
													</ul>
												</td>
											</tr>
									
									</table>
								</div>
							</div>
						</div>
						
					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
			<div id="footer_space"></div>
		</div>

       <!-- responsive table -->
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.sort.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.filter.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.paginate.js"></script>

			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/pages/ebro_responsive_table.js"></script>
