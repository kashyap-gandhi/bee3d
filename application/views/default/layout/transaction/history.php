    <div id="bidcontener"><!--start_#bidcontener-->
                <div class="bid_form_center">
                	<div class="left_side" style="border:none; width:970px;">
                    	<div class="describe_requirements" style="width:945px;">
                            <h1 class="lt">Transaction History</h1>
                            <h1 class="rt">Total : <?php echo $this->transaction_model->get_total_transaction_sum(); ?></h1>
                        </div>
                        
                        <table class="tr_history" width="100%" cellpadding="0" cellspacing="0" border="0" align="left">
                          
                          
                          <tr>
                            <th></th>
                            <th>Amount</th>
                            
                            <th>TRANSACTION ID</th>
                            <th>Trip</th>                          
                            <th>DATE</th>
                            <th>STATUS</th>
                          </tr>
                          
                          
                          <?php if($result) { 
						  			foreach($result as $row) { ?>
                          
                          <tr class="debit">
                           <td valign="middle" align="center">Debit</td>
                           <td valign="middle" align="right">- <?php echo $row->pay_amount; ?>  </td>
                          
                           <td valign="middle" align="center"><?php echo $row->pay_transaction_id; ?> </td>
                           <td valign="middle" align="left">
                           <?php if($row->trip_id>0) { 
						   	$trip_info=get_one_trip($row->trip_id);
							if($trip_info) { 
						   ?>
                                                     
<a class="fpass" href="<?php echo site_url('from/'.$trip_info->trip_from_place.'/to/'.$trip_info->trip_to_place.'/'.$trip_info->trip_id);?>"><?php echo ucwords($trip_info->trip_from_place.' > '.$trip_info->trip_to_place); ?></a>
                       
                           
                           <?php } } ?>
                           </td>                           
						   <td valign="middle" align="center"><?php echo date($site_setting->date_time_format,strtotime($row->transaction_date_time)); ?></td>  
                           
                           <td valign="middle" align="center"><span style="color:#009900; font-weight:bold;"><?php if($row->transaction_approved==1) { ?>Confirm<?php } else { ?> Reject<?php } ?> </span> </td>                        </tr>
                           
                           <?php } } else { ?>
                           
                           
                           <tr class="debit">
                           
                           <td colspan="6" align="center" valign="middle">No Transaction has been created yet.</td>
                           
                           </tr>
                           
                           <?php } ?>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </table>
                       
                        <div class="clear"></div>
                        <?php if($total_rows>10) { ?>
							<div class="gonext">
								<?php echo $page_link; ?>
							</div>
                             <div class="clear"></div>
						<?php } ?>
                        
                        
                        
                    <div class="clear"></div>
                    </div>
                    
                    </div>
            <div class="clear"></div>
            </div><!--end_#bidcontener-->
            