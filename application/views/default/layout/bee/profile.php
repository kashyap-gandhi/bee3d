<?php
$themurl = base_url() . getThemeName();
?>
<!-- *************************************************************************************** -->
<style>
    .row8{
        background: none !important;
    }
</style>
<div class="wrapper row8 page-padding-top">

</div>

<!-- *************************************************************************************** -->

<!-- *************************************************************************************** -->
<div class="wrapper row11">
    <div class="container">
        <div class="search-block"> 
            <div class="row">
              
                    <div class="col-md-3">
                        <div class="advsearch-block"> 
                            <div class="min-height" style="min-height: 200px;">
                                
                                
                                
                                
                                <?php
                                
                                $user_image= base_url().'upload/no_userimage.png';
				 if($user_profile->profile_image!='') {  
					if(file_exists(base_path().'upload/user/'.$user_profile->profile_image)) {
						$user_image=base_url().'upload/user/'.$user_profile->profile_image;	
					}					
				}
                                
                                ?>
                                
                                <img src="<?php echo $user_image; ?>" class="img-responsive imgcenter" />
                                
                                <h3 class="search-title" style="padding-top: 20px;"> <?php echo ucfirst(strtolower($user_profile->first_name)).' '.ucfirst(strtolower($user_profile->last_name));?> </h3>
                               
                        
                                <div class="text-center head-social">
                     <ul class="social-link">
                         
                        
                         
                         <?php if ($user_profile->own_site_link != '') { ?> <li class="site"> <a href="<?php echo addhttp($user_profile->own_site_link); ?>" target="_blank"> &nbsp;</a></li> <?php } ?>
                         
                         <?php if ($user_profile->facebook_link != '') { ?> <li class="fb"> <a href="<?php echo addhttp($user_profile->facebook_link); ?>" target="_blank"> &nbsp;</a></li> <?php } ?>
                         
                         
                         <?php if ($user_profile->twitter_link != '') { ?> <li class="tw"> <a href="<?php echo addhttp($user_profile->twitter_link); ?>" target="_blank"> &nbsp;</a></li> <?php } ?>
                         
                         <?php if ($user_profile->linkedin_link != '') { ?> <li class="in"> <a href="<?php echo addhttp($user_profile->linkedin_link); ?>" target="_blank"> &nbsp;</a></li> <?php } ?>
                         
<!--                          <li class="gm"> <a href="#" > &nbsp;</a></li>-->
                         
                         
                                            
                         
                     </ul>
                 </div>
                                 


                            </div>
                            
                              
                               	
                               
                            

                        </div>


                    </div>
                    <div class="col-md-9">
                        <div class="searchlist-block">
                            <div class="searchlist-top">
                                <div class="row">
                                    <div class="col-md-5 col-sm-5"> 
                                        <h3 class="search-title"> 3D Designs </h3>
                                    </div>
                                    <div class="col-md-7 col-sm-7"> 
                                        <div class="form-horizontal col-lg-12" role="form">
                                            <div class="form-group">
                                                <label for="inputEmail3" class=" col-sm-9 control-label"></label>
                                                <div class="col-sm-3 ">
                                                   
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div> 
                                </div>
                            </div>
                             <div class="searchlist-detail">

                           
<!--                                <div class="related-search">
                                    
                                    <ul class="list-unstyled inline" >
                                        <li> Related Searches   </li>
                                        <li> <a href="javascript:void(0);">cat 1 </a>  </li>
                                    </ul>
                                     
                                </div> -->

                                <div>
                                    <ul class="search-listing columns-4">

                                        <?php
                                        if ($user_design) {
                                            foreach ($user_design as $res) {
                                                $res=(object) $res;
                                                ?>
                                         <li>
                                         <a href="<?php echo site_url('design/' . $res->design_slug); ?>">

                                                <?php 
                                                
                                                $is_image=0;
                                                $main_counter_image='';
                                                $main_image='';
                                                
                                                
                                                $counter_id = $res->design_counter_id;
                                                if ($counter_id > 0) {
                                                    $counter_detail=  get_counter_detail($counter_id);
                                                    
                                                    if($counter_detail){
                                                        $main_counter_image=$counter_detail->gallery_image;
                                                         if ($main_counter_image != '' && file_exists(base_path() . 'upload/gallery/' . $main_counter_image)) {
                                                             $is_image=1; ?>
                                              <img alt="<?php echo $main_counter_image; ?>" src="<?php echo base_url() ?>upload/gallery/<?php echo $main_counter_image; ?>" class="img-responsive minheight20 imgcenter" />
                                             <?php
                                                         }
                                                        
                                                        
                                                    } 
                                                    
                                                } else {
                                                    $main_image = $this->design_model->get_one_main_design_image($res->design_id); 
                                                   

                                                    if ($main_image != '' && file_exists(base_path() . 'upload/design/' . $main_image)) {
                                                        $is_image=1;
                                                    ?>
                                                           
                                                                <img alt="<?php echo $main_image; ?>" src="<?php echo base_url() ?>upload/design/<?php echo $main_image; ?>" class="img-responsive minheight20 imgcenter" />
                                                            
                                                        <?php }  }?>
                                                                
                                                              <?php 
                                                              
                                                              if($is_image==0){
                                                              
                                                              ?>
                                                                 <img alt="No Image" src="<?php echo base_url() ?>upload/no_userimage.png" class="img-responsive minheight20 imgcenter" />
                                                                 <?php } ?>
                                                                
                                                                
                                                        
                                                          </a>
                                                        
                                                        
                                                        <p><a href="<?php echo site_url('design/' . $res->design_slug); ?>"><?php echo substr($res->design_title, 0, 35); ?></a></p>
                                                        <h4> <strong><?php echo $res->design_point; ?> </strong> </h4>
                                                        <div class="rig-brd clearfix">
                                                            
                                                            <div class="pull-left"> </div>
                                                            
                                                            
                                                            <div class="pull-right">
                                                           <!-- <a href="#"> <i class="sprit-icon comment "> </i> </a> -->
                                                                <a href="<?php echo site_url('design/' . $res->design_slug); ?>"> <i class="sprit-icon like "> </i> </a> 
                                                            </div> 
                                                        </div>
                                                    </li>
                                                    <?php
                                                
                                            
                                            }} else {
                                            ?>
                                            <li style=" width: 100%; text-align: center;"><?php echo $user_profile->first_name; ?> have not added any 3D designs yet.</li>
                                        <?php } ?>                               

                                    </ul>
                                </div>

                              

                            </div>


                        </div>		  


                    </div>
               
            </div>
        </div>

    </div>
</div> 


<!-- *************************************************************************************** -->