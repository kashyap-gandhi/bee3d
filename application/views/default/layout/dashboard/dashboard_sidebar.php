<div class="right_side">

	<?php 
		$agent_profile = get_user_agent_detail(get_authenticateUserID());
		
		if($agent_profile) {
			$verify_msg = '';
			
			$user_card_info = get_user_card_info(get_authenticateUserID());
			$agent_document = get_agent_document(get_authenticateUserID());
			
			if($user_card_info) {
				if($user_card_info->card_verify_status == 0){
					$verify_msg .= '<li>'.anchor('my_card','Please Verify Credit Card Details','style="color:red;font-size: 14px;font-weight: bold;"').'</li>';
				}
			} else {
				$verify_msg .= '<li>'.anchor('my_card','Please Verify Credit Card Details','style="color:red;font-size: 14px;font-weight: bold;"').'</li>';
			}
			
			if(!$agent_document){
				$verify_msg .= '<li>'.anchor('travelagent/profile#document','Please Add your Background Proof','style="color:red;font-size: 14px;font-weight: bold;"').'</li>';	
			}
			
			if($verify_msg != '') {
				//echo '<ul style="margin-left: 5px;">'.$verify_msg.'</ul><br />';
			}
		}
	?>
	
	<div class="deal_btn_dash fl"><?php echo anchor('deal/adddeal','Add deal')?></div>
    <div class="clear"></div><br />

    
    <div class="estim marB5">Profile</div>
   	<ul class="accr">
		<li><a href="<?php echo site_url('user/'.getUserProfileName());?>">See Profile</a></li>
    </ul><div class="clear"></div>
	
	
	<div class="estim marB5 martop20">Your Account</div>
	<ul class="accr">
		<li><?php echo anchor('user/edit','My Account')?></li>
		
		
		
		<li><?php echo anchor('user/change_password','Change password')?></li>
		<li><?php echo anchor('notifications','Notifications')?></li>
		<?php /*?><li><?php echo anchor('mytrip/all','MyTrips')?></li><?php */?>
		
		
        
        <?php if($agent_profile) { ?>
			<li><?php echo anchor('travelagent/profile','Agent Profile')?></li>
           <?php /*?> <li><?php echo anchor('myoffer/all','MyOffers')?></li>
            <li><?php echo anchor('my_card','My Credit Card')?></li>
			<li><?php echo anchor('transaction','Transaction History')?></li><?php */?>
             <li><?php echo anchor('deal/mydeal','My Deal')?></li>
            
		<?php } else { ?>
			<li><?php echo anchor('travelagent/became','Became a Travel Agent')?></li>
		<?php } ?>
       
        
        
        
  	</ul><div class="clear"></div>
  
<?php /*?>  	<div class="marB5 martop20"><a href="#" class="estim">Alerts(25)</a></div>
			
			<ul class="alerts">
			  <li>
				<a href="#">
				
				<div class="alert_img"> <img src="images/man1.png" /></div>
				  <p>
				   
					Your offer accepted by 
					<span>software</span> on
				   <span>testing purpose 2222</span>
				  </p>
				</a>
			  </li>
			  
			  <li>
				<a href="#">
				
				<div class="alert_img"> <img src="images/man1.png" /></div>
				  <p>
				   
					Your offer accepted by 
					<span>software</span> on
				   <span>testing purpose 2222</span>
				  </p>
				</a>
			  </li>
			  
			  
			</ul>
			<?php */?>
			<div class="clear"></div>
			
			<?php /*?><div class="add" style="padding-left: 10px;">
			 <h3>View Tasks with our new Task Map!</h3>
			 <a href="#" class="fr">
					  <img src="images/close.png">
			 </a>
			 
			 <div class="add_cmnt">
			   <h4>Want a way to see all the current Tasks on a fun map?</h4>
			   <div class="imgst">
				 <img src="images/add_demo.png" />
			   </div>
			   
			   
			 </div><div class="clear"></div>
			 
			 
			 </div><?php */?> 
			 
			 
			 <?php if($top_agents) { ?>
			 <div class="add">
			 
			 	<div class="posted" style="width:257px;">
					<h2>EggTrip Agents</h2>
				</div>
				
				<?php foreach($top_agents as $top_agent) { 
				
					////////======= trip post usser image 
					$agent_image= base_url().'upload/no_image.png';
 
					 if($top_agent->profile_image!='') {  
						if(file_exists(base_path().'upload/user_thumb/'.$top_agent->profile_image)) {
							$agent_image=base_url().'upload/user_thumb/'.$top_agent->profile_image;	
						}					
					}
				?>
				
				<div class="egg_trip1">
					<?php echo anchor('travelagent/'.$top_agent->profile_name,'<img src="'.$agent_image.'" alt=""class="img" />');?>
					
					<?php /*?><div class="orenge_box1" <?php if($top_agent->agent_level>9) { ?> style="padding:5px 0px 0px 5px;" <?php } else { ?> style="padding:5px 0px 0px 8px;" <?php } ?>>
						<h6><?php echo $top_agent->agent_level;?></h6>
					</div><?php */?>
					
					<span><?php echo anchor('travelagent/'.$top_agent->profile_name,ucwords($top_agent->first_name.' '.$top_agent->last_name),' style="color:#0979B2;"');?> <!--<br> <strong>selected by Egg Trip</strong>--></span>



				</div>
				
				<?php } ?>

			</div>
   			<?php } ?>
	
			<div class="clear"></div>
</div>
<div class="clear"></div>                   