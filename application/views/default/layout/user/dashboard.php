<link rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/fronttheme/css/linecons/style.css">
		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Dashboard</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
						
						<!-- main content -->
						
						<h2>Personal Information Center</h2>
							<!-- main content -->
						<div class="row">
							<div class="col-sm-6 col-md-3">
								<div class="box_stat box_ico">
									<span class="stat_ico stat_ico_1"><i class="icon-picture"></i></span>
									<h4><?php echo get_total_design();?></h4>
									<small>Designs</small>
								</div>
							</div>
							<div class="col-sm-6 col-md-3">
								<div class="box_stat box_ico">
									<span class="stat_ico stat_ico_2"><i class="icon-play-circle"></i></span>
									<h4><?php echo get_total_video();?></h4>
									<small>Videos</small>
								</div>
							</div>
							<div class="col-sm-6 col-md-3">
								<div class="box_stat box_ico">
									<span class="stat_ico stat_ico_3"><i class="icon-tasks"></i></span>
									<h4><?php echo get_total_challenge();?></h4>
									<small>Challenges</small>
								</div>
							</div>
							<div class="col-sm-6 col-md-3">
								<div class="box_stat box_ico">
									<span class="stat_ico stat_ico_4"><i class="icon-map-marker"></i></span>
									<h4><?php echo get_total_store();	?></h4>
									<small>3D Printers</small>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4">
								<div class="panel_alt">
									<div class="panel_alt_body">
                                        <p class="panel_alt_title">
                                            <i class="li_note"></i>
											<span>Blog</span>
                                        </p>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In amet obcaecati sequi! Ipsum dolores adipisci inventore placeat&hellip;</p>
										<a href="<?php echo site_url('blog');?>" class="btn btn-primary btn-sm" target="_blank">Read More&hellip;</a>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="panel_alt">
									<div class="panel_alt_body">
                                        <p class="panel_alt_title">
                                            <i class="li_bulb"></i>
											<span>Forum</span>
                                        </p>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In amet obcaecati sequi! Ipsum dolores adipisci&hellip;</p>
										<a href="<?php echo site_url('forums');?>" class="btn btn-primary btn-sm" target="_blank">Read More&hellip;</a>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="panel_alt">
									<div class="panel_alt_body">
                                        <p class="panel_alt_title">
                                            <i class="li_mail"></i>
											<span>Contact Us</span>
                                        </p>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In amet obcaecati sequi! Ipsum dolores adipisci </p>
										<a href="<?php echo site_url('contact');?>" class="btn btn-primary btn-sm">Read More&hellip;</a>
									</div>
								</div>
							</div>
						</div>
						
					

					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
			<div id="footer_space"></div>
		</div>
		
