<!-- aditional stylesheets -->
<!-- responsive table -->
<link href="<?php echo base_url() . getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>

<div id="wrapper_all">
    <?php
    $theme = getThemeName();
    echo $this->load->view($theme . '/layout/common/header_menu', TRUE);
    ?>
    <!-- mobile navigation -->
    <nav id="mobile_navigation"></nav>

    <section id="breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="<?php echo site_url('user/dashboard') ?>">Home</a></li>
                <li><span>Design Download</span></li>
            </ul>
        </div>
    </section>
    
    <script>
    function categoryfilter(cat)
	{
		
            window.location.href='<?php echo site_url('user/designdownload/'.$limit);?>/'+cat;
		
	
	}
    </script>

    <section class="container clearfix main_section">
        <div id="main_content_outer" class="clearfix">
            <div id="main_content">

                <!-- main content -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">My Design Download
                                <!--<span class="span_right"><a href="<?php //echo site_url('package/buy_credit'); ?>" class="">Add Credit</a></span>-->
                                </h4>
                            </div>




                            <div class="panel_controls">
                                <div class="row">

                                    <div class="col-sm-3 col-xs-6">
                                        <label for="category_id">Filter By Category:</label>
                                        <select name="category_id" id="category_id" class="form-control" onchange="categoryfilter(this.value)">
                                            <option value="0">---Select---</option>
                                            <?php echo $category_filter; ?>

                                        </select>
                                    </div>
<!--                                    <div class="col-sm-3 col-xs-6">
                                        <label>&nbsp;</label>
                                        <button class="btn btn-default btn-sm" >Search</button>
                                    </div>-->
                                </div>
                            </div>

                            <table id="resp_table" class="table toggle-square" data-filter="#table_search" data-page-size="40">
                                <thead>
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Date</th>
                                        <th>Design</th>
                                        <th>ip</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($result) {
                                        $i = 1;
                                        foreach ($result as $res) {
                                            ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo date($site_setting->date_format, strtotime($res->transaction_date_time)); ?></td>
                                                <td><?php echo anchor('design/' . $res->design_slug, ucfirst($res->design_title)); ?></td>
                                                <td><?php echo $res->design_ip; ?></td>
                                                <td><a class="btn btn-warning" href="<?php echo site_url('design/download_image_zip/' . $res->design_id); ?>"><span data-toggle="tooltip" data-placement="top auto" title="Downloa design in zip">Download</span></a></td>
                                                <?php /* <td>
                                                  <a class="btn btn-primary" href="<?php echo site_url('video/view_me/'.$res->video_id);?>">View</a>
                                                  <a class="btn btn-success" href="<?php echo site_url('video/edit_me/'.$res->video_id);?>">Edit</a>
                                                  <a class="btn btn-danger" href="javascript://" onclick="delete_video('<?php echo $res->video_id;?>');">Delete</a>
                                                  </td> */ ?>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="3">No Records Found!!</td>

                                        </tr>
<?php } ?>


                                </tbody>

                                <tr>
                                    <td colspan="6" class="text-center">
                                        <ul class="">
<?php echo $page_link; ?>
                                        </ul>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

<?php echo $this->load->view($theme . '/layout/common/sidebar', TRUE); ?>
    </section>
    <div id="footer_space"></div>
</div>

<!-- responsive table -->
<script src="<?php echo base_url() . getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.js"></script>
<script src="<?php echo base_url() . getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.sort.js"></script>
<script src="<?php echo base_url() . getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.filter.js"></script>
<script src="<?php echo base_url() . getThemeName(); ?>/fronttheme/js/lib/FooTable/js/footable.paginate.js"></script>

<script src="<?php echo base_url() . getThemeName(); ?>/fronttheme/js/pages/ebro_responsive_table.js"></script>
