<?php 
$themurl = base_url().getThemeName(); 
?>
    <!-- *************************************************************************************** -->
    <style>
    .row8{
        background: none !important;
    }
</style>
<div class="wrapper row8 page-padding-top">
 </div>

<!-- *************************************************************************************** -->
    

    <?php
    
    ////==save first time taken postal code and lat long to session
    ///====and if user search some another postal code then use posted code for search
    //===maintain searchable parameters like range, postal code ...lat and long
    ///==== check direction link
    
    
    
	$sotre_data = $result;

	?>
<link src="<?php echo $themurl; ?>/css/3d-map.css" type="text/css" />

   
<script>   
   
    
   function rad(x) {
      return x * Math.PI / 180;
    };

    function getDistance(system_lat,system_long,store_lat,store_long) {
       // alert(system_lat+","+system_long+","+store_lat+","+store_long);
       //var R = 6378137; // Earth’s mean radius in meter
      var R = 3961.3; // Earth’s mean radius in kilometer
      var dLat = rad(store_lat - system_lat);
      var dLong = rad(store_long - system_long);
      var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(rad(system_lat)) * Math.cos(rad(store_lat)) *
        Math.sin(dLong / 2) * Math.sin(dLong / 2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      var d = R * c;
      return d.toFixed(2); // returns the distance in meter
    }

function stripslashes(str) {
  return (str + '')
    .replace(/\\(.?)/g, function(s, n1) {
      switch (n1) {
        case '\\':
          return '\\';
        case '0':
          return '\u0000';
        case '':
          return '';
        default:
          return n1;
      }
    });
}
 </script>

<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<!--<script type="text/javascript" src="http://www.google.com/jsapi?kay=AIzaSyAE3Vx4HJoGXk2AEF0WA6nlUy_c_L_1fKY"></script>-->
   
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyAE3Vx4HJoGXk2AEF0WA6nlUy_c_L_1fKY"></script>-->


<script>
        
    var placeSearch, autocomplete,map, infowindow, service;
    var marker,geocoder;


    var gmarkers = []; 


    var system_lat=<?php echo $site_setting->default_latitude;?>;
    var system_long=<?php echo $site_setting->default_longitude;?>;
            
    var sotre_data;
    
    var default_distance_range=10000;
    
    
          function initmap(){  



                geocoder = new google.maps.Geocoder();

                var myLatlng = new google.maps.LatLng( system_lat, system_long);

                var mapOptions = {
                    center: myLatlng,
                    zoom: 13,
                    mapTypeId: google.maps.MapTypeId.HYBRID
                };
                var map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);


                infowindow = new google.maps.InfoWindow();

        
<?php /*if( $this->session->userdata('visitor_user_lat')!='' && $this->session->userdata('visitor_user_lon')!='' ){
    
    if($this->session->userdata('visitor_user_postal_code')!='') { ?>
          getstores('<?php echo $this->session->userdata('visitor_user_postal_code'); ?>','<?php echo $this->session->userdata('visitor_user_lat'); ?>','<?php echo $this->session->userdata('visitor_user_lon'); ?>',default_distance_range);
            <?php } else { ?>
    getgeolocate('<?php echo $this->session->userdata('visitor_user_lat'); ?>','<?php echo $this->session->userdata('visitor_user_lon'); ?>');
        
        <?php } } else { */ ?>
       if(navigator.geolocation.getCurrentPosition(show_map)){} else { googleloader(); }
<?php /*}*/ ?>
        
     
     }
     
     

     function show_map(position) {
           //alert(position.coords.latitude+","+ position.coords.longitude);
           getgeolocate(position.coords.latitude,position.coords.longitude);
        }

      function googleloader(){
            if(google.loader.ClientLocation)
            {
                visitor_lat = google.loader.ClientLocation.latitude;
                visitor_lon = google.loader.ClientLocation.longitude;
                visitor_city = google.loader.ClientLocation.address.city;
                visitor_region = google.loader.ClientLocation.address.region;
                visitor_country = google.loader.ClientLocation.address.country;
                visitor_countrycode = google.loader.ClientLocation.address.country_code;
               // alert('<p>Lat/Lon: ' + visitor_lat + ' / ' + visitor_lon + '</p><p>Location: ' + visitor_city + ', ' + visitor_region + ', ' + visitor_country + ' (' + visitor_countrycode + ')</p>');


                getgeolocate(visitor_lat,visitor_lon);


            }
            else
            {
                getapplocate(system_lat, system_long);
            }
        }
        
        function getgeolocate(visitor_lat,visitor_lon){
            
            $.ajax({
                url: "<?php echo site_url('printer/getgeolocate');?>",
                type:'post',         
                async: false,  
                data:{lat:visitor_lat,lon:visitor_lon},             
                success: function( response ) {
                  //alert(response);
                  var res=$.parseJSON(response);
                  
                  if(res.postal_code==''){
                    getapplocate(visitor_lat,visitor_lon);
                  } else {
                     getstores(res.postal_code,res.lat,res.lon,default_distance_range);
                  }
                }
            });
            
            
        }
       
        function getapplocate(visitor_lat,visitor_lon){
            $.ajax({
                url: "<?php echo site_url('printer/getapplocate');?>",
                type:'post',         
                async: false,  
                data:{lat:visitor_lat,lon:visitor_lon},  
                success: function( response ) {                  
                  var res=$.parseJSON(response);           
                  getstores(res.postal_code,res.lat,res.lon,default_distance_range);                  
                }
            });
        }
        
        function getstores(postal_code,lat,lon,distance_range,type)
        {
            //alert(postal_code+','+lat+','+lon);
            if(lat!='' && lon!=''){
                var visitorLatlng = new google.maps.LatLng( lat,lon);   
              

                var mapOptions = {
                    center: visitorLatlng,
                    zoom: 13,
                    mapTypeId: google.maps.MapTypeId.HYBRID
                };
                map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);
               
                 infowindow = new google.maps.InfoWindow();
            }
            
            if(distance_range==''){
                distance_range=default_distance_range;
            }
            
            
            
            $.ajax({
                url: "<?php echo site_url('printer/getstore');?>",
                type:'post',         
                async: false,  
                data:{lat:lat,lon:lon,postal_code:postal_code,distance_range:distance_range},             
                success: function( response ) {
                  //alert(response);
                  var res=$.parseJSON(response);
                  
                    sotre_data=res;
                    
                    ///==
                    
                    if(sotre_data.length>0){
                        
                        $("#marker_list").empty();
                        
                        $.each(sotre_data,function(skey,sval){

                            var store_id = sval.store_id;
                            var store_address = stripslashes(sval.store_address);
                            var store_address2 =    stripslashes(sval.store_address2);
                            var store_city =    stripslashes(sval.store_ctiy);                          
                            var store_state =   stripslashes(sval.store_state);
                            var store_postal_code =   stripslashes(sval.store_postal_code);
                            var store_country =    stripslashes(sval.store_country);
                            var store_lat =    sval.store_lat;
                            var store_long =    sval.store_long;
                            var store_name =    stripslashes(sval.store_name);
                            var store_email =    sval.store_email;
                            var store_phone =    sval.store_phone;
                            var store_url =    sval.store_url;


                               var marker= createMarker(store_id,store_address,store_address2,store_city,store_state,store_postal_code,store_country,store_lat,store_long,store_name,store_email,store_phone,store_url);

                        });
                    } else {
                        if(type=='s'){
                        alert("No 3D printers found. Please refine your search criteria.");
                        }
                    }
                    
                    //===
                   
                  
                }
            });
        }
        

            

                function createMarker(store_id,store_address,store_address2,store_city,store_state,store_postal_code,store_country,store_lat,store_long,store_name,store_email,store_phone,store_url) {


                            
                             <?php if( $this->session->userdata('visitor_user_lat')!='' && $this->session->userdata('visitor_user_lon')!='' ){ ?>
                                     var distance = getDistance(<?php echo $this->session->userdata('visitor_user_lat');?>,<?php echo $this->session->userdata('visitor_user_lon'); ?>,store_lat, store_long); 
                            <?php } else { ?>
                                var distance = getDistance(system_lat,system_long,store_lat, store_long); 
<?php } ?>

                                var storeLatlng = new google.maps.LatLng( store_lat, store_long);

                                var marker = new google.maps.Marker({
                                    map: map,
                                    position: storeLatlng
                                });
                                
                               
                                marker.setIcon(({
                                    url: '<?php echo $themurl; ?>/images/orange_university.png',
                                    size: new google.maps.Size(71, 71),
                                    origin: new google.maps.Point(0, 0),
                                    anchor: new google.maps.Point(17, 34),
                                    scaledSize: new google.maps.Size(35, 35)
                                }));

                                
                               


                                var pin_content='';
                                var sideHtml = '';
                                
                               // sideHtml += '<li data-storeid="'+store_id+'">';
                               
                               sideHtml +='<div class="storefirst">';
                               
                                if(store_name!=''){
                                    pin_content+='<strong style="font-size:1.2em">'+store_name+'</strong>';
                                    sideHtml += '<div class="store_name"><b>'+store_name+'</b></div>';
                                    
                                }
                                sideHtml += '<div class="store_name"><b>'+distance+'km</b></div>';
                                
                                
                                 sideHtml +='</div>';
                                 
                                  sideHtml +='<div class="storesec">';
                                 
                                 
                                  if(store_address!=''){
                                    pin_content+= '<br/><strong>Address:</strong>'+store_address;
                                    sideHtml += '<p>'+store_address+'</p>';
                                }
                                if(store_address2!=''){
                                    pin_content+= '<br/><strong>Address2:</strong>'+store_address2;
                                    sideHtml += '<p>'+store_address2+'</p>';
                                }
                                if(store_city!=''){
                                    pin_content+= '<br/><strong>City:</strong>'+store_city;
                                    sideHtml += '<p>'+store_city+'</p>';
                                }
                                if(store_state!=''){
                                    pin_content+= '<br/><strong>State:</strong>'+store_state;
                                   
                                   if(store_postal_code!=''){ 
                                      sideHtml += '<p>'+store_state+''; 
                                   
                                   } else {
                                       sideHtml += '<p>'+store_state+'</p>';
                                   }
                                   
                                   
                                   
                                }
                                if(store_postal_code!=''){
                                    pin_content+= '<br/><strong>Postal Code:</strong>'+store_postal_code;
                                    
                                    if(store_state!=''){
                                        sideHtml += ', '+store_postal_code+'</p>';
                                    } else {
                                          sideHtml += '<p>'+store_postal_code+'</p>';
                                    }
                                  
                                }
                                if(store_country!=''){
                                    pin_content+= '<br/><strong>Country:</strong>'+store_country;
                                    sideHtml += '<p>'+store_country+'</p>';
                                }
                                
                                 
                                 if(store_phone!=''){
                                    pin_content+= '<br/><strong>Phone:</strong>'+store_phone;
                                    sideHtml += '<div class="store_phone"><span>Phone:</span> '+store_phone+'</div>';
                                }
                                
                                
                                  sideHtml +='</div>';
                                 
                                 sideHtml +='<div class="storethird">';
                                 
                                 if(store_url!=''){
                                    pin_content+= '<br/><strong>Website:</strong>'+store_url;
                                    sideHtml += '<p class="store_url"><a href="'+store_url+'">Website</a></p>';
                                }
                                 
                                 if(store_email!=''){
                                    pin_content+= '<br/><strong>Email:</strong>'+store_email;
                                    sideHtml += '<p class="store_email"><a href="mailto:'+store_email+'">'+store_email+'</a></p>';
                                }
                                
                                
                                sideHtml +='<p class="storelocatorlink"><a href="http://maps.google.com/maps?saddr='+store_country+'&amp;daddr='+store_address+','+store_address2+','+store_city+','+store_state+','+store_postal_code+','+store_country+'" target="_blank">Directions</a></p>';
                                
                                  sideHtml +='</div>';
                                  
                                
                                
                                
                              sideHtml +='<div class="clear"></div>';
                                
                                
                                
                                
                                //sideHtml += '</li>';

                                google.maps.event.addListener(marker, 'click', function(){          
                                  infowindow.setContent(pin_content);
                                  infowindow.open(map, this);
                                });
                                
                                
                                
                               
                                
                                // save the info we need to use later for the side_bar
                                gmarkers.push(marker);

                                
                                var ul = document.getElementById("marker_list");
                                var li = document.createElement("li");    
                                li.setAttribute('data-storeid',store_id);
                                li.innerHTML = sideHtml;
                                ul.appendChild(li);
                                
                              
                               
                                 //Trigger a click event to marker when the button is clicked.
                                  google.maps.event.addDomListener(li, "click", function(){
                                    google.maps.event.trigger(marker, "click");
                                  });
//                                  google.maps.event.addDomListener(li, "mouseover", function(){
//                                    google.maps.event.trigger(marker, "click");
//                                  });
                                 
                                

             

                }

            
$(document).ready(function(){
        
        initmap();
        
        $("#find_stores").on('click',function(){
        
            
            var search_postal_code=$("#postal_code").val();
            var search_distance_range = $("#distance_range option:selected").val();
            
            
            var lat=system_lat;
            var lon=system_long;
            
         
            <?php if($this->session->userdata('visitor_user_lat')!='' && $this->session->userdata('visitor_user_lon')!='' ){ ?>
            	var lat='<?php echo $this->session->userdata("visitor_user_lat"); ?>';
            	var lon='<?php echo $this->session->userdata("visitor_user_lon"); ?>';
            	//alert(lat+"==="+lon);
            <?php } ?>
            
            
            
            getstores(search_postal_code,lat,lon,search_distance_range,'s');
            
            
        });

});



           




           

        </script>
        
        <style>
            
#map-canvas {
	width: 100%; height: 500px;
	float:none;
	margin: 0px;
	padding: 0px;
}
#locs {
	margin: 10px 0px 0px 0px;
	padding: 0px;	
        clear: both;
	float:none;
	height: 500px;
        width:100%;
	overflow-y: scroll;
}
#marker_list {
    list-style: none;
    margin:0px !important;
    padding: 0px !important;	
}
#marker_list li {
        list-style: none;
	border: 1px solid #CCC;
	width: 100%;
	padding: 5px;
	cursor:pointer;
	margin:0px 0px 10px 0px;
}
#marker_list li:hover {
    background: #F8F8F8;
}

.storefirst {
    width:35%;
    float: left;
}
.storesec {
    width:50%;
    float: left;
}

.storethird {
    width:15%;
    float: left;
}
.clear { clear: both;}

.store_name { color:#333333; font-size: 16px; }


.storesec p {
    line-height: 10px;
}

.store_phone span { font-weight: bold; }

.store_url {
    
}

.store_url a {
    font-weight: bold;
}

.store_email {
    
}

.storelocatorlink {
    
}
.storelocatorlink a {
    font-weight: bold;
}

.label-control{
    color: #333333;
    font-family: 'conduit_itclight';
    font-size: 36px;
    text-transform: uppercase;
}
.form-control{
    font-size: 25px;
    height: 48px;
}

        </style>
<!-- *************************************************************************************** -->
<div class="wrapper row11">
	<div class="container">
    	<div class="search-block"> 
            
            
            <div class="col-md-2">
                <lable class="label-control">Zipcode</lable>
            </div>
                <div class="col-md-2">
                <input type="text" maxlength="10" name="postal_code" id="postal_code" class="form-control" />
                </div>
            <div class="col-md-2">
                <lable class="label-control">Within</lable>
            </div>
            <div class="col-md-2">
                <select name="distance_range" id="distance_range" class="form-control">
                    <option value="10">10 km</option>
                    <option value="25">25 km</option>
                    <option value="50">50 km</option>
                    <option value="100">100 km</option>
                    <option value="200">200 km</option>
                    <option value="500">500 km</option>
                    <option value="1000">1000 km</option>
                </select>
            </div>
            <div class="col-md-2">
                <input type="button" name="find_stores" id="find_stores" value="Find Locations" class="btn" />
            </div>
            
            <div class="clearfix" style="height:15px;" ></div>
            
		
        <div id="map-canvas"></div>
        <div id="locs"><ul id="marker_list"></ul></div>

    
        
        
        <div class="clear"></div>
        
        </div>
    	 
    </div>
</div> 
<div class="clear"></div>
  
<!-- *************************************************************************************** -->