<?php
$themurl = base_url() . getThemeName();
?>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53b182c86f415a5c"></script>
<!-- *************************************************************************************** -->
<style>
    .row8{
        background: none !important;
    }
</style>
<div class="wrapper row8 page-padding-top">

</div>

<!-- *************************************************************************************** -->
<div class="wrapper row4">
    <div class="container">
        <div class="main-contain">
            <h1 class="text-center white"><span class="font-light">BROWSE, SHARE, AND REVIEW 3D PRINTER STORE  </h1>

        </div>	
    </div>
</div>
<!-- *************************************************************************************** -->
<div class="wrapper row9">

    <div class="video-detail v-detail-bg">
        <div class="container">
            
            
            
            <?php 
		if($msg!= '' )
		{
			if($msg == 'success')
			{?>
			<div class="alert alert-success" style="margin: 16px 21px 16px 0px;">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<?php  echo "Thank you for adding review."; ?>
			
		</div>
			<?php }
                        if($msg == 'delete')
			{?>
			<div class="alert alert-success" style="margin: 16px 21px 16px 0px;">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<?php  echo "Review has been deleted successfully."; ?>
			
		</div>
			<?php }
		}
		?>
            
            
            
            <div class="col-md-12" > 
                <div class="v-detail-title">
                    <h2><?php echo stripslashes($store_detail->store_name); ?> </h2>
                    <h3> BY <strong> <?php if ($design_by_profile_name != '') {
    echo anchor('bee/' . $design_by_profile_name, $design_by, ' style="color: #333333;" ');
} else {
    echo $design_by;
} ?></strong> - <?php echo date($site_setting->date_format, strtotime($store_detail->store_date)); ?></h3>
                </div>	
            </div>
        </div>

        <div class="container">
            <div class="col-md-9"> 

                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="video-box">



                                    <div>
                                        <!--map--->



                    <script>   


                        function rad(x) {
                            return x * Math.PI / 180;
                        };

                        function getDistance(system_lat,system_long,store_lat,store_long) {
                            // alert(system_lat+","+system_long+","+store_lat+","+store_long);
                            //var R = 6378137; // Earth’s mean radius in meter
                            var R = 3961.3; // Earth’s mean radius in kilometer
                            var dLat = rad(store_lat - system_lat);
                            var dLong = rad(store_long - system_long);
                            var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                                Math.cos(rad(system_lat)) * Math.cos(rad(store_lat)) *
                                Math.sin(dLong / 2) * Math.sin(dLong / 2);
                            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                            var d = R * c;
                            return d.toFixed(2); // returns the distance in meter
                        }

                   
    
    //var google_key='AIzaSyAE3Vx4HJoGXk2AEF0WA6nlUy_c_L_1fKY';
  var google_key='';
  
    var placeSearch, autocomplete,map, infowindow, service;
    var marker,geocoder;


    var gmarkers = []; 
    var ib = [];


    var system_lat=<?php echo $site_setting->default_latitude; ?>;
    var system_long=<?php echo $site_setting->default_longitude; ?>;


    var store_lat =   '<?php echo $store_detail->store_lat; ?>';
    var store_long =    '<?php echo $store_detail->store_long; ?>';
    
   

    var vlat='';
    var vlon='';

var icon_path='<?php echo $themurl;?>/map/';




function loadScript()
{
    if ( typeof map == 'undefined') {
        
        var script1 = document.createElement('script');
        script1.type = 'text/javascript';
        if(google_key!=''){
        script1.src = 'http://www.google.com/jsapi?key='+google_key;
        } else {
         script1.src = 'http://www.google.com/jsapi';
        }
        document.body.appendChild(script1);
        
        var script = document.createElement('script');
        script.type = 'text/javascript';
        if(google_key!=''){
            script.src = 'http://maps.google.com/maps/api/js?key='+google_key+'&v=3.exp&sensor=false&callback=initmap';
        } else {
            script.src = 'http://maps.google.com/maps/api/js?v=3.exp&sensor=false&callback=initmap';
        }
        document.body.appendChild(script);
        
        
       
    } 
}



function loadScript2(url, callback) {
 
    var script = document.createElement("script")
    script.type = "text/javascript";

    if (script.readyState) { //IE
        script.onreadystatechange = function () {
            if (script.readyState == "loaded" || script.readyState == "complete") {
                script.onreadystatechange = null;
                callback();
            }
        };
    } else { //Others
        script.onload = function () {
            callback();
        };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}



    function initmap(){ 
        

    
    loadScript2(icon_path+'js/infobox.js',function(){
    
        
        geocoder = new google.maps.Geocoder();

        var myLatlng = new google.maps.LatLng( store_lat, store_long);

        var mapOptions = {
            center: myLatlng,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        
        map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);


        //infowindow = new google.maps.InfoWindow();



<?php if ($this->session->userdata('visitor_user_lat') != '' && $this->session->userdata('visitor_user_lon') != '') { ?>
            vlat = '<?php echo $this->session->userdata('visitor_user_lat'); ?>';
            vlon = '<?php echo $this->session->userdata('visitor_user_lon'); ?>';
<?php } ?>


        getstore(vlat,vlon);
        
        
         if(navigator.geolocation.getCurrentPosition(show_map)){} 

        //else { googleloader(); }

        });

    }



    function show_map(position) {

        getgeolocate(position.coords.latitude,position.coords.longitude);
        
    }

    function googleloader(){
        if(google.loader.ClientLocation)
        {
            visitor_lat = google.loader.ClientLocation.latitude;
            visitor_lon = google.loader.ClientLocation.longitude;
            visitor_city = google.loader.ClientLocation.address.city;
            visitor_region = google.loader.ClientLocation.address.region;
            visitor_country = google.loader.ClientLocation.address.country;
            visitor_countrycode = google.loader.ClientLocation.address.country_code;
            // alert('<p>Lat/Lon: ' + visitor_lat + ' / ' + visitor_lon + '</p><p>Location: ' + visitor_city + ', ' + visitor_region + ', ' + visitor_country + ' (' + visitor_countrycode + ')</p>');


            getgeolocate(visitor_lat,visitor_lon);


        }

    }

    function getgeolocate(visitor_lat,visitor_lon){

        $.ajax({
            url: "<?php echo site_url('printer/getgeolocate'); ?>",
            type:'post',         
            async: false,  
            data:{lat:visitor_lat,lon:visitor_lon},             
            success: function( response ) {
                //alert(response);
                var res=$.parseJSON(response);

                clearmap();
                getstore(res.lat,res.lon);

            }
        });


    }






    function getstore(lat,lon)
    {         

        var store_id = '<?php echo $store_detail->store_id; ?>';
        var store_address = '<?php echo stripslashes($store_detail->store_address); ?>';
        var store_address2 =    '<?php echo stripslashes($store_detail->store_address2); ?>';
        var store_city =    '<?php echo stripslashes($store_detail->store_ctiy); ?>';                        
        var store_state =   '<?php echo stripslashes($store_detail->store_state); ?>';
        var store_postal_code =   '<?php echo stripslashes($store_detail->store_postal_code); ?>';
        var store_country =    '<?php echo stripslashes($store_detail->store_country); ?>';
        var store_lat =   '<?php echo $store_detail->store_lat; ?>';
        var store_long =    '<?php echo $store_detail->store_long; ?>';
        var store_name =    '<?php echo $store_detail->store_name; ?>';
        var store_email =    '<?php echo $store_detail->store_email; ?>';
        var store_phone =    '<?php echo $store_detail->store_phone; ?>';
        var store_url =    '<?php echo addhttp($store_detail->store_url) ?>';
        var store_domain ='<?php echo get_domain_name($store_detail->store_url); ?>';
        
        var store_rating ='<?php echo roundDownToHalf($store_detail->store_total_rating); ?>';
        var store_review ='<?php echo $store_detail->store_total_review; ?>';

        var store_pin ='';
<?php if (file_exists(base_path() . 'upload/category_orig/' . $store_detail->category_image) && $store_detail->category_image != '') { ?>
            var store_pin ='<?php echo base_url() . 'upload/category_orig/' . $store_detail->category_image; ?>';
<?php } ?>



        var marker= createMarker(store_id,store_address,store_address2,store_city,store_state,store_postal_code,store_country,store_lat,store_long,store_name,store_email,store_phone,store_rating,store_review,store_url,store_domain,store_pin,lat,lon);


    }




    function createMarker(store_id,store_address,store_address2,store_city,store_state,store_postal_code,store_country,store_lat,store_long,store_name,store_email,store_phone,store_rating,store_review,store_url,store_domain,store_pin,vlat,vlong) {



        if(vlat=='' && vlong==''){
<?php if ($this->session->userdata('visitor_user_lat') != '' && $this->session->userdata('visitor_user_lon') != '') { ?>
                var distance = getDistance(<?php echo $this->session->userdata('visitor_user_lat'); ?>,<?php echo $this->session->userdata('visitor_user_lon'); ?>,store_lat, store_long); 
<?php } else { ?>
                var distance = getDistance(system_lat,system_long,store_lat, store_long);
<?php } ?>

        } else {
            var distance = getDistance(vlat,vlong,store_lat, store_long); 
        }

        var storeLatlng = new google.maps.LatLng( store_lat, store_long);

        var marker = new google.maps.Marker({
            map: map,
            position: storeLatlng,
            visible:true
        });


        var pin_thumb='<?php echo $themurl; ?>/map/bee-3d.png';

        if(store_pin!='') {
            pin_thumb=store_pin;
        }



        marker.setIcon(({
            url: pin_thumb,
            size: new google.maps.Size(39,51),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(19.5, 25.5),
            scaledSize: new google.maps.Size(39,51)
        }));





        var pin_content='';


        if(store_name!=''){
            pin_content+='<strong style="font-size:22px;"><a>'+store_name+'</a></strong>';
        }
        
         pin_content+='<p class="pinrating"><img src="<?php echo $themurl;?>/rating/'+store_rating+'.png" /><span> REVIEWS('+store_review+')</span></p>';
       
        if(store_address!=''){
            //pin_content+= '<p style="clear:both;">'+store_address+'</p>';
        }
        if(store_address2!=''){
           // pin_content+= '<p style="clear:both;">'+store_address2+'</p>';
        }
         pin_content+= '<p style="clear:both;">';
        if(store_city!=''){
            pin_content+= ''+store_city;
        }
        if(store_state!=''){
            if(store_city!=''){
                pin_content+= ','+store_state;
            } else {
                pin_content+= ''+store_state;
            }
        }
        if(store_postal_code!=''){
            pin_content+= '-'+store_postal_code;
        }
         pin_content+= '</p>';
        if(store_country!=''){
            pin_content+= '<p style="clear:both;">';
            pin_content+= ''+store_country;
            pin_content+= '</p>';
        }
      
       
        if(store_phone!=''){
             pin_content+= '<p style="clear:both;">';
            pin_content+= ''+store_phone;
             pin_content+= '</p>';
        }
       
        
        if(store_url!=''){
            pin_content+= '<p style="clear:both;">';
            pin_content+= '<a href="'+store_url+'">'+store_domain+'</a>';
            pin_content+= '</p>';
        }
        
        
        if(store_email!=''){
            pin_content+= '<p style="clear:both;">';
            pin_content+= ''+store_email;
            pin_content+= '</p>';
        }
        
        pin_content+= '<p style="clear:both;">';
        pin_content+= '<strong>Distance: </strong>'+distance+'km';
         pin_content+= '</p>';

        //alert(pin_content);
        
        
        
        
         var info_html='<div class="mapTip"><div class="tipInfoBox">';
                                info_html+=pin_content;
                                 info_html += '</div>';          
                                info_html += '<div class="mapArrow"><img src="'+icon_path+'mapTipArrow.png" /></div>';
                                info_html += '</div>';    
                                
                           var left_side = -168;
                            var top_height = -234;     
                            
                            var boxText = document.createElement("div");
                            //boxText.style.cssText = "border: 1px solid black; margin-top: 8px; background: #999; border-radius:5px; padding: 5px; color:#FFF;";
                            boxText.innerHTML = info_html;

                            var myOptions1 = {
                                alignBottom:false,
                                position:storeLatlng,
                                content: boxText,
                                disableAutoPan: false,
                                maxWidth: 0,
                                pixelOffset: new google.maps.Size(left_side,top_height),
                                zIndex: null,
                                boxStyle: { 
                                    // background: "url('"+icon_path+"images/tipbox.gif') no-repeat",
                                    opacity: 1,
                                    width: "340px"
                                },
                                closeBoxMargin: "2px",
                                closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                                infoBoxClearance: new google.maps.Size(1, 1),
                                isHidden: false,
                                pane: "floatPane", //mapPane
                                enableEventPropagation: false
                            };
                            ib[store_id] = new InfoBox(myOptions1);
    
    
    

                                google.maps.event.addListener(marker, 'click', function(){          
                                  //infowindow.setContent(pin_content);
                                  //infowindow.open(map, this);
                                   $(".infoBox").hide();
                                    ib[store_id].open(map, marker); 
                                });
                                

       
       


        // save the info we need to use later for the side_bar
        gmarkers.push(marker);


    }



    function clearmap() {        
        $.each(gmarkers, function(index) {
            deleteMarker(gmarkers[index]);       
        });
        gmarkers = [];


    }

    function deleteMarker(marker) {
        marker.setMap(null); 
    }


    $(document).ready(function(){
        if(store_long!='' && store_lat!=''){ loadScript(); }
    });

</script>

<style>

#map-canvas {
width: 100%; height: 500px;
float:none;
margin: 0px;
padding: 0px;
}
   p.pinrating {
                border-bottom: 1px solid #ccc;
padding-bottom: 31px;

            }
            p.pinrating img {
  float: left;  
  width: 125px;
}
p.pinrating span {
    font-size: 20px;
float: left;
padding-left: 5px;
}


    /*-- mapTip --*/
.mapTip{
	position:relative;
	width:340px;
        height: auto;
/*	font-family: 'proxima_nova_rgregular';*/
	color:#000;
}

.mapTip [class*="col-"]{padding:0;}
.mapTip .tipInfoBox{
	padding:7px !important;
	min-height: 200px;
	border-radius:1px;
	-webkit-border-radius:1px;
	background: #fff; /* url('/images/mapTipInfoBG.png') left center repeat-x;*/
}
.mapTip .tipInfoBox p{
    margin: 0 0 5px;
    font-size: 12px;

}
.mapTip .mapArrow{
	position:absolute;
	left:46%;
	bottom:-12px;
	margin-left:-12px;
}
.infoclose img { z-index: 9;  }

</style>


                                        <div id="map-canvas"></div>      

                                        <!--end map--> 
                                    </div>





                                </div>


                            </div>

                            <div class="col-md-3 row">
                                <div class="v-detail-list" >
                                    <ul>


                                        
                            <li><?php $total_rating=roundDownToHalf($store_detail->store_total_rating);   ?>
                                <img src="<?php echo $themurl;?>/rating/<?php echo $total_rating;?>.png" /></li>
                            <li><?php echo $store_detail->store_total_review; ?> REVIEWS</li>
                            
<!--                            <li><?php //echo $store_detail->store_view_count; ?> VIEWS</li>-->
                                        

                                        <li><!-- Go to www.addthis.com/dashboard to customize your tools -->
                                            <div class="addthis_sharing_toolbox"></div></li>
                                    </ul>
                                </div>


                            </div>



                        </div>

                    </div>






                    <style>
                        ul#comments {
                            list-style: none;
                            padding: 0px;
                            margin: 0px;
                        }
                        ul#comments li {
                            list-style: none;
                            padding: 0px;
                            margin: 0px;
                        }
                        ul#comments li ul {
                            list-style: none;
                            padding: 0px;
                            margin: 0px;
                            padding-left: 47px;
                        }
                        .writetext {
                            font-size: 22px;
                            height: 39px;
                        }
                        .commenttitle {
                            padding-top: 10px;
                            font-size: 22px !important;
                        }
                        #obj-loader{
                            display: none;
                            margin: 10px 0px 10px 0px;
                            text-align: center;
                        }
                    </style>


                    <!-- vcommetnt-->
                    <div class="v-comment"> 
                        <div class="">
                            <h3 class="pull-left commenttitle">COMMENTS</h3>
                            
                            
                            <?php 
                            
                            
                            if(isset($store_reviews) && !empty($store_reviews)) {                            
                                $review_text='Write a Review';
                            } else {
                                $review_text='Be the first to write a Review';
                            }
                            
                            $store_user_id=$store_detail->user_id;
                            $user_id=0;
                            
                            if (get_authenticateUserID() > 0) {
                                $user_id=get_authenticateUserID(); ?>
                            <a href='javascript:void(0)' class="pull-right btn yellow-btn writetext doreview" data-id="0" data-pid="0">
                            <?php } else { ?>
                            <a href='<?php echo site_url("login/a/". base64_encode(current_url())) ?>' class="pull-right btn yellow-btn writetext">
                            <?php } 
                            
                            echo $review_text; ?></a>
                            
                            
                                <div class="clearfix"></div>
                            
                            
                            <?php if(isset($store_reviews) && !empty($store_reviews)) { ?>
                            <ul id="comments">
                                <?php foreach($store_reviews as $review){ 
                                    
                                    
                                    
                                    $profile_url='javascript:void(0)';
                                    $user_image= base_url().'upload/no_userimage.png';
                                    
                                    $first_name=$review->user_profile_name;
                                    $last_name='';
                                    
                                        
                                   
                                        
                                    
                                    if($review->user_profile_name!='guest' && $review->user_profile_name!='') {
                                        $user_exp=explode('####',$review->user_profile_name);

                                        $first_name=$user_exp[0];
                                        $last_name=$user_exp[1];
                                        $profile_name=$user_exp[2];
                                        $profile_image=$user_exp[3];

                                        

                                        if($profile_name!=''){
                                            $profile_url=site_url('bee/'.$profile_name);
                                        }


                                      
                                         if($profile_image!='') {  
                                                if(file_exists(base_path().'upload/user/'.$profile_image)) {
                                                        $user_image=base_url().'upload/user/'.$profile_image;	
                                                }					
                                        }
                                    
                                    }
                                    
                                    
                                    ?>
                                <li>
                                    <div class="media">
                                        
                                        
                                        <a class="pull-left" href="<?php echo $profile_url; ?>">
                                            
                                            <img class="media-object" src="<?php echo $user_image; ?>" width="36" height="36">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"> 
                                                <div class="pull-left">         
                                                    <a href="<?php echo $profile_url; ?>" style="color: #333333;"><?php echo strtoupper($first_name.' '.$last_name);?></a>
                                                    <br/><img style="width: 90px;" src="<?php echo $themurl;?>/rating/<?php echo (int) $review->store_rating;?>.png" />
                                                </div>
                                                <div class="pull-right">
                                                    
                                                    
                                                    <?php if($user_id>0) { ?>
                                                  <a href='javascript:void(0)' class="doreply" data-pid="<?php echo $review->parent_review_id; ?>" data-id="<?php echo $review->store_review_id; ?>">  
                                                    <?php } else { ?>
                                                    <a href='<?php echo site_url("login/a/". base64_encode(current_url())) ?>'>
                                                        <?php } ?>
                                                        <img src="<?php echo $themurl;?>/images/icon/reply28.png" border="0" alt="Reply" title="Reply" /></a>
                                                    
                                                      <?php if($user_id>0 && ($user_id==$store_user_id || $user_id==$review->user_id)) { ?>
                                                      
                                                    <a href='javascript:void(0)' class="doreview" data-pid="<?php echo $review->parent_review_id; ?>" data-id="<?php echo $review->store_review_id; ?>"><img src="<?php echo $themurl;?>/images/icon/edit20.png" border="0" alt="Edit" title="Edit" /></a> 
                                                    <?php } ?>
                                                    
                                                     <?php if($user_id>0 && $user_id==$store_user_id) { ?>
                                                        <a href='javascript:void(0)'  onclick="doaction(<?php echo $review->store_review_id; ?>)" class="doaction" data-pid="<?php echo $review->parent_review_id; ?>" data-id="<?php echo $review->store_review_id; ?>"><img src="<?php echo $themurl;?>/images/icon/trash24.png" border="0" alt="Delete" title="Delete" /></a> 
                                                        <?php } ?>
                                                    
                                                    
                                                </div>
                                                <div class="clearfix"></div>
                                       
                                            </h4>
                                           
                                            <h5 class="media-heading-sub rv<?php echo $review->store_review_id; ?>" data-rt="<?php echo (int) $review->store_rating;?>"> <?php echo ucfirst(stripslashes($review->store_review_description)); ?> </h5>
                                            about <?php echo getDuration($review->store_review_date); ?>
                                        </div>
                                    </div> 
                                    <?php 
                                   
                                    $review_reply=$this->printer_model->get_store_review_reply($store_detail->store_id,$review->store_review_id);
                                    
                                    if(isset($review_reply) && !empty($review_reply)) {
                                        
                                        $store_replys=$review_reply;
                                        
                                       
                                    
                                     ?>
                                    <ul>
                                         <?php foreach($store_replys as $reply){ 
                                             
                                             
                                    $profile_url='javascript:void(0)';
                                    $user_image= base_url().'upload/no_userimage.png';
                                    
                                    $first_name=$reply->user_profile_name;
                                   $last_name='';
                                        
                                    
                                    if($reply->user_profile_name!='guest' && $reply->user_profile_name!='') {
                                        $user_exp=explode('####',$reply->user_profile_name);

                                        $first_name=$user_exp[0];
                                        $last_name=$user_exp[1];
                                        $profile_name=$user_exp[2];
                                        $profile_image=$user_exp[3];

                                        

                                        if($profile_name!=''){
                                            $profile_url=site_url('bee/'.$profile_name);
                                        }


                                      
                                         if($profile_image!='') {  
                                                if(file_exists(base_path().'upload/user/'.$profile_image)) {
                                                        $user_image=base_url().'upload/user/'.$profile_image;	
                                                }					
                                        }
                                    
                                    }
                                    
                                             
                                             
                                             ?>
                                        <li>
                                   <div class="media">
                                        
                                        
                                        <a class="pull-left" href="<?php echo $profile_url; ?>">
                                            
                                            <img class="media-object" src="<?php echo $user_image; ?>" width="36" height="36">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"> 
                                                <div class="pull-left">         
                                                    <a href="<?php echo $profile_url; ?>" style="color: #333333;"><?php echo strtoupper($first_name.' '.$last_name);?></a>
                                                </div>
                                                <div class="pull-right">
                                                    
                                                    <?php if($user_id>0 && ($user_id==$store_user_id || $user_id==$reply->user_id)) { ?>   
                                                    <a href='javascript:void(0)' class="doreview" data-pid="<?php echo $reply->parent_review_id; ?>" data-id="<?php echo $reply->store_review_id; ?>"><img src="<?php echo $themurl;?>/images/icon/edit20.png" border="0" alt="Edit" title="Edit" /></a> 
                                                     <?php } ?>
                                                    
                                                     <?php if($user_id>0 && $user_id==$store_user_id) { ?>
                                                    <a href='javascript:void(0)' onclick="doaction(<?php echo $reply->store_review_id; ?>)" class="doaction" data-pid="<?php echo $reply->parent_review_id; ?>" data-id="<?php echo $reply->store_review_id; ?>"><img src="<?php echo $themurl;?>/images/icon/trash24.png" border="0" alt="Delete" title="Delete" /></a> 
                                                    <?php } ?>
                                                    
                                                </div>
                                                <div class="clearfix"></div>
                                       
                                            </h4>
                                            <h5 class="media-heading-sub rv<?php echo $reply->store_review_id; ?>"> <?php echo ucfirst(stripslashes($reply->store_review_description)); ?> </h5>
                                            about <?php echo getDuration($reply->store_review_date); ?>
                                        </div>
                                    </div> 
                                </li>
                                <?php } ?>
                               
                                    </ul>
                                    <?php } ?>
                                </li>
                                 <?php } ?>
                               
                                    </ul>
                                
                                <div id="obj-loader"><img src="<?php echo $themurl;?>/images/loader.gif" border="0" /></div>
                                
                                    <?php }  ?>

                        </div>
                    </div>

                    <!-- end of commnet-->
                </div>
            </div>   	 
            <div class="col-md-3">
                <div class="v-left-listing"> 

                    <h1> Location  </h1>
                    <div class="addressloc margin-bottom-15">

<?php if($store_detail->store_address!='') { ?><p><?php echo stripslashes($store_detail->store_address); ?></p><?php } ?>
<?php if($store_detail->store_address2!='') { ?><p><?php echo stripslashes($store_detail->store_address2); ?></p><?php } ?>
<?php if($store_detail->store_ctiy!='') { ?><p><?php echo ucfirst(stripslashes($store_detail->store_ctiy)); ?></p><?php } ?>
<?php if($store_detail->store_state!='') { ?><p><?php echo ucfirst(stripslashes($store_detail->store_state)); ?></p><?php } ?>
<?php if($store_detail->store_postal_code!='') { ?><p><?php echo ucfirst(stripslashes($store_detail->store_postal_code)); ?></p><?php } ?>
<?php if($store_detail->store_country!='') { ?><p><?php echo ucfirst(stripslashes($store_detail->store_country)); ?></p><?php } ?>
        
       </div> 
                    
                    <style>
                       
                       
                        .addressloc p {
                            margin: 5px 0px 0px 0px;
                        }
                    </style>
                        
                    
              
                    <?php if($store_detail->store_email!='' || $store_detail->store_email!='' || $store_detail->store_url!=''){?>
      <h1> Other Information  </h1>
      <div>

<?php if($store_detail->store_email!=''){?> <p><a href="mailto:<?php echo $store_detail->store_email; ?>"><?php echo $store_detail->store_email; ?></a></p> <?php } ?>
<?php if($store_detail->store_email!=''){?> <p><?php echo $store_detail->store_phone; ?></p><?php } ?>
<?php if($store_detail->store_url!=''){?> <p><a href="<?php echo addhttp($store_detail->store_url); ?>" target="_blank"><?php echo get_domain_name($store_detail->store_url);?></a></p><?php } ?>

                </div>
      <?php } ?>
      
      
      

                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div> 



 <script>
        var store_id='<?php echo $store_detail->store_id; ?>';
        
    $(document).ready(function(){    
        
    
    if($("#comments").length>0){
        
    var $container = $('#comments');

     var pagesize = 10;
     var pageno = pagesize;
     
    
   
     
     var isPreviousEventComplete = true, isDataAvailable = true;
     
  
    $(window).scroll(function(){ 
        if($(window).scrollTop() + $(window).height() > ( ($(document).height() * 3 ) / 4)){
         
      if (isPreviousEventComplete && isDataAvailable) {
          
        isPreviousEventComplete = false;
        $("#obj-loader").css("display", "block");
        
        $.ajax({
          type: "GET",
          cache:true,
          async:false,
          dataType:'html',
          url: '<?php echo site_url('printer/loadmore/'.$store_detail->store_id);?>/'+ pageno,
          success: function (result) {
            if($.trim(result)=='nothing'){
                
            } else {
                $container.append(result).fadeIn(300);       
                bindaction();
            }
            
            
            
             pageno = parseInt(pageno) + parseInt(pagesize);
            
            
            isPreviousEventComplete = true;
          
            if ($.trim(result) == '' || $.trim(result)=='nothing') { 
                isDataAvailable = false;
            }

            $("#obj-loader").css("display", "none");
          },
          error: function (error) {
              alert(error);
          }
        });

      }
     }
    });
    
    }
        
        
        function bindaction(){
                $(".doreview").on("click",function(){

            var review_id=parseInt($(this).attr('data-id'));
            var preview_id=parseInt($(this).attr('data-pid'));
            var review_description=$(".rv"+review_id).html();
            var rating=parseInt($(".rv"+review_id).attr('data-rt'));
            
            if(rating == undefined){
                rating=0;
            }
            if(isNaN(rating)){
                rating=0;
            }
            
            if(review_id>0){
                $(".rateme").hide();
            } else if(preview_id>0){
                 rating=0;
                 $(".rateme").hide();
             } else {
                 $(".rateme").show();
                

                for (var i = rating; i >= 0; i--) {
                    $('.rate-btn-'+i).addClass('rate-btn-hover');
                };
             }

            $("#edit_review_description").val(review_description);
            $("#edit_review_id").val(review_id);
            $("#edit_preview_id").val(preview_id);
            $("#edit_rating").val(rating);

            $("#myModal").modal('show');
        });
        
        
        
        $(".doreply").on("click",function(){

           
            var preview_id=$(this).attr('data-id');
           
           $("#edit_review_id").val(0);
            $("#edit_preview_id").val(preview_id);
            $("#edit_rating").val(0);
            
            $(".rateme").hide();

            $("#myModal").modal('show');
        });
        }
       
        $(".doreview").on("click",function(){

            var review_id=parseInt($(this).attr('data-id'));
            var preview_id=parseInt($(this).attr('data-pid'));
            var review_description=$(".rv"+review_id).html();
            var rating=parseInt($(".rv"+review_id).attr('data-rt'));
            
            if(rating == undefined){
                rating=0;
            }
            if(isNaN(rating)){
                rating=0;
            }
            
            if(review_id>0){
                $(".rateme").hide();
            } else if(preview_id>0){
                 rating=0;
                 $(".rateme").hide();
             } else {
                 $(".rateme").show();
                

                for (var i = rating; i >= 0; i--) {
                    $('.rate-btn-'+i).addClass('rate-btn-hover');
                };
             }

            $("#edit_review_description").val(review_description);
            $("#edit_review_id").val(review_id);
            $("#edit_preview_id").val(preview_id);
            $("#edit_rating").val(rating);

            $("#myModal").modal('show');
        });
        
        
        
        $(".doreply").on("click",function(){

           
            var preview_id=$(this).attr('data-id');
           
           $("#edit_review_id").val(0);
            $("#edit_preview_id").val(preview_id);
            $("#edit_rating").val(0);
            
            $(".rateme").hide();

            $("#myModal").modal('show');
        });


        $('#myModal').on('shown.bs.modal', function(){
           
        });


        $('#myModal').on("hidden.bs.modal", function(){    
            $("#edit_review_description").val('');
            $("#edit_review_id").val(0);
            $("#edit_preview_id").val(0);
            $("#edit_rating").val(0);
        });
        
       
  
  $('.rate-btn').hover(function(){
    $('.rate-btn').removeClass('rate-btn-hover');
    var therate = $(this).attr('id');
    $("#edit_rating").val(therate);
    for (var i = therate; i >= 0; i--) {
        $('.rate-btn-'+i).addClass('rate-btn-hover');
    };
});

    });
    
    
function doaction(id)
{
	var ans = confirm("Are you sure, you want to delete Review?");
	if(ans)
	{
		location.href = "<?php echo site_url('printer/delete_review_me/'.$store_detail->store_id); ?>/"+id;
	}else{
		return false;
	}
}
    
function getauth()
{


            var review_description=$.trim($("#edit_review_description").val());
            var review_num=parseInt($("#edit_review_id").val());
            var preview_num = parseInt($("#edit_preview_id").val());
            
            
            var therate = parseInt($("#edit_rating").val());
            
            if(therate>5){
                therate=5;
            } else if(therate<0){
                therate=0;
            } 
            
            
            
            
            
          
            
    
    $.ajax({
        type: "POST",
        url: "<?php echo site_url('printer/post_review')?>",
        data: {review_desc:review_description,review_id:review_num,preview_id:preview_num,rating:therate,store_id:store_id},
        dataType: 'json',
        success: function(response, textStatus, xhr) {
	    if(response.error!="")
            {
				  $("#ajax_msg_error1").html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+response.error+'</div>');
                return false;
            }
            else
            {	
		 window.location.href = '<?php echo site_url('printer/'.$store_detail->store_slug);?>'+'/'+response.msg;
            }
        }
    });
  }
    </script>  


    <link href="<?php echo $themurl;?>/rating/rating.css" rel="stylesheet" />

<!--review-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Write a Review</h4>
      </div>
      <div class="modal-body">
	 <div id="ajax_msg_error1"></div>
         <form name="frm_ajax_auth" method="post" id="frm_ajax_auth" class="">
     
		 
		 <div class="form-group rateme">
			<label for="exampleInputEmail1" class="label-control">Rate it:</label>
			<div class="rate-ex1-cnt">
                            <div id="1" class="rate-btn-1 rate-btn"></div>
                            <div id="2" class="rate-btn-2 rate-btn"></div>
                            <div id="3" class="rate-btn-3 rate-btn"></div>
                            <div id="4" class="rate-btn-4 rate-btn"></div>
                            <div id="5" class="rate-btn-5 rate-btn"></div>
                        </div>
		  </div>
		  
		 <div class="form-group">
			<label for="edit_review_description" class="label-control">Comment:</label>
                        <textarea rows="8" class="form-control" id="edit_review_description" name="edit_review_description" placeholder="Please enter an comment"></textarea>
		  </div>
		<div class="form-group text-center margin-top-20">
                    <input type="hidden" name="edit_rating" id="edit_rating" value="0" />
			<input type="hidden" name="edit_review_id" id="edit_review_id" />	
                        <input type="hidden" name="edit_preview_id" id="edit_preview_id" />	
			<button type="button" class="btn yellow-btn" onclick="getauth();">Send</button>
		 </div>
			   
		</form>
      </div>
      
    </div>
  </div>
</div>
