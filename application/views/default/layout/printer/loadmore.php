<?php
$themurl = base_url() . getThemeName();


$store_user_id = $store_detail->user_id;
$user_id = 0;

if (get_authenticateUserID() > 0) {
    $user_id = get_authenticateUserID();
   } 
   if (isset($store_reviews) && !empty($store_reviews)) { 
    
    foreach ($store_reviews as $review) {

       

        $profile_url = 'javascript:void(0)';
        $user_image = base_url() . 'upload/no_userimage.png';

        $first_name = $review->user_profile_name;
        $last_name = '';





        if ($review->user_profile_name != 'guest' && $review->user_profile_name != '') {
            $user_exp = explode('####', $review->user_profile_name);

            $first_name = $user_exp[0];
            $last_name = $user_exp[1];
            $profile_name = $user_exp[2];
            $profile_image = $user_exp[3];



            if ($profile_name != '') {
                $profile_url = site_url('bee/' . $profile_name);
            }



            if ($profile_image != '') {
                if (file_exists(base_path() . 'upload/user/' . $profile_image)) {
                    $user_image = base_url() . 'upload/user/' . $profile_image;
                }
            }
        } 
        ?>
        <li>
            <div class="media">


                <a class="pull-left" href="<?php echo $profile_url; ?>">

                    <img class="media-object" src="<?php echo $user_image; ?>" width="36" height="36">
                </a>
                <div class="media-body">
                    <h4 class="media-heading">
                        <div class="pull-left">
                            <a href="<?php echo $profile_url; ?>" style="color: #333333;"><?php echo strtoupper($first_name . ' ' . $last_name); ?></a>
                            <br/><img style="width: 90px;" src="<?php echo $themurl; ?>/rating/<?php echo (int) $review->store_rating; ?>.png" />
                        </div>
                        <div class="pull-right">


                            <?php if ($user_id > 0) { ?>
                                <a href='javascript:void(0)' class="doreply" data-pid="<?php echo $review->parent_review_id; ?>" data-id="<?php echo $review->store_review_id; ?>">
                                <?php } else { ?>
                                    <a href='<?php echo site_url("login/a/" . base64_encode(current_url())) ?>'>
                                    <?php } ?>
                                    <img src="<?php echo $themurl; ?>/images/icon/reply28.png" border="0" alt="Reply" title="Reply" /></a>

                                <?php if ($user_id > 0 && ($user_id == $store_user_id || $user_id == $review->user_id)) { ?>

                                    <a href='javascript:void(0)' class="doreview" data-pid="<?php echo $review->parent_review_id; ?>" data-id="<?php echo $review->store_review_id; ?>"><img src="<?php echo $themurl; ?>/images/icon/edit20.png" border="0" alt="Edit" title="Edit" /></a>
                                <?php } ?>

                                <?php if ($user_id > 0 && $user_id == $store_user_id) { ?>
                                    <a href='javascript:void(0)'  onclick="doaction(<?php echo $review->store_review_id; ?>)" class="doaction" data-pid="<?php echo $review->parent_review_id; ?>" data-id="<?php echo $review->store_review_id; ?>"><img src="<?php echo $themurl; ?>/images/icon/trash24.png" border="0" alt="Delete" title="Delete" /></a>
                                <?php } ?>


                        </div>
                        <div class="clearfix"></div>

                    </h4>

                    <h5 class="media-heading-sub rv<?php echo $review->store_review_id; ?>" data-rt="<?php echo (int) $review->store_rating; ?>"> <?php echo ucfirst(stripslashes($review->store_review_description)); ?> </h5>
                    about <?php echo getDuration($review->store_review_date); ?>
                </div>
            </div>
            <?php
           $review_reply=$this->printer_model->get_store_review_reply($store_detail->store_id,$review->store_review_id);
                                    
            if(isset($review_reply) && !empty($review_reply)) {

                $store_replys=$review_reply;
                                        
                ?>
                <ul>
                    <?php
                    foreach ($store_replys as $reply) {


                        $profile_url = 'javascript:void(0)';
                        $user_image = base_url() . 'upload/no_userimage.png';

                        $first_name = $reply->user_profile_name;
                        $last_name = '';


                        if ($reply->user_profile_name != 'guest' && $reply->user_profile_name != '') {
                            $user_exp = explode('####', $reply->user_profile_name);

                            $first_name = $user_exp[0];
                            $last_name = $user_exp[1];
                            $profile_name = $user_exp[2];
                            $profile_image = $user_exp[3];



                            if ($profile_name != '') {
                                $profile_url = site_url('bee/' . $profile_name);
                            }



                            if ($profile_image != '') {
                                if (file_exists(base_path() . 'upload/user/' . $profile_image)) {
                                    $user_image = base_url() . 'upload/user/' . $profile_image;
                                }
                            }
                        }
                        ?>
                        <li>
                            <div class="media">


                                <a class="pull-left" href="<?php echo $profile_url; ?>">

                                    <img class="media-object" src="<?php echo $user_image; ?>" width="36" height="36">
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">
                                        <div class="pull-left">
                                            <a href="<?php echo $profile_url; ?>" style="color: #333333;"><?php echo strtoupper($first_name . ' ' . $last_name); ?></a>
                                        </div>
                                        <div class="pull-right">

                                            <?php if ($user_id > 0 && ($user_id == $store_user_id || $user_id == $reply->user_id)) { ?>
                                                <a href='javascript:void(0)' class="doreview" data-pid="<?php echo $reply->parent_review_id; ?>" data-id="<?php echo $reply->store_review_id; ?>"><img src="<?php echo $themurl; ?>/images/icon/edit20.png" border="0" alt="Edit" title="Edit" /></a>
                                            <?php } ?>

                                            <?php if ($user_id > 0 && $user_id == $store_user_id) { ?>
                                                <a href='javascript:void(0)'  onclick="doaction(<?php echo $reply->store_review_id; ?>)" class="doaction" data-pid="<?php echo $reply->parent_review_id; ?>" data-id="<?php echo $reply->store_review_id; ?>"><img src="<?php echo $themurl; ?>/images/icon/trash24.png" border="0" alt="Delete" title="Delete" /></a>
                                            <?php } ?>

                                        </div>
                                        <div class="clearfix"></div>

                                    </h4>
                                    <h5 class="media-heading-sub rv<?php echo $reply->store_review_id; ?>"> <?php echo ucfirst(stripslashes($reply->store_review_description)); ?> </h5>
                                    about <?php echo getDuration($reply->store_review_date); ?>
                                </div>
                            </div>
                        </li>
                    <?php } ?>

                </ul>
            <?php } ?>
        </li>
    <?php } ?>
<?php } ?>

