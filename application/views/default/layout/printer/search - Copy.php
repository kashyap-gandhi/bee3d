<?php 
$themurl = base_url().getThemeName(); 
?>
    <!-- *************************************************************************************** -->
    <style>
    .row8{
        background: none !important;
    }
</style>
<div class="wrapper row8 page-padding-top">
 </div>

<!-- *************************************************************************************** -->
    

<?php $sotre_data = $result; ?>
<link src="<?php echo $themurl; ?>/css/3d-map.css" type="text/css" />

<input type="hidden" id="visitor_user_lat" value="<?php if(isset($_SESSION['visitor_user_lat'])) { echo $_SESSION['visitor_user_lat']; } ?>" />
<input type="hidden" id="visitor_user_lon" value="<?php if(isset($_SESSION['visitor_user_lon'])) { echo $_SESSION['visitor_user_lon']; } ?>" />
<input type="hidden" id="visitor_user_postal_code" value="<?php if(isset($_SESSION['visitor_user_postal_code'])) { echo $_SESSION['visitor_user_postal_code']; } ?>" />
   
 <script src="<?php echo $themurl.'/map/js/markerclusterer.js';?>"></script>
<script>   
   
    
   function rad(x) {
      return x * Math.PI / 180;
    };

    function getDistance(system_lat,system_long,store_lat,store_long) {
       // alert(system_lat+","+system_long+","+store_lat+","+store_long);
       //var R = 6378137; // Earth’s mean radius in meter
      var R = 3961.3; // Earth’s mean radius in kilometer
      var dLat = rad(store_lat - system_lat);
      var dLong = rad(store_long - system_long);
      var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(rad(system_lat)) * Math.cos(rad(store_lat)) *
        Math.sin(dLong / 2) * Math.sin(dLong / 2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      var d = R * c;
      return d.toFixed(2); // returns the distance in meter
    }

function stripslashes(str) {
  return (str + '')
    .replace(/\\(.?)/g, function(s, n1) {
      switch (n1) {
        case '\\':
          return '\\';
        case '0':
          return '\u0000';
        case '':
          return '';
        default:
          return n1;
      }
    });
}

  
  //var google_key='AIzaSyAE3Vx4HJoGXk2AEF0WA6nlUy_c_L_1fKY';
  var google_key='';
 
var placeSearch, autocomplete,map, infowindow, service;
var marker,geocoder,markerCluster,circle_radius;

var oms;
var shadow;

var default_distance_range=10000;
var minZoomForDisplayMarkers=14;
var mapZoom = 13;
var maxZoom= 18;
var minZoom=11;

var draw_circle=null;
var gmarkers = [];
var ib = [];

var system_lat=<?php echo $site_setting->default_latitude;?>;
var system_long=<?php echo $site_setting->default_longitude;?>;
            
    var sotre_data;
    
var default_distance_range=10000;

var LoadedNeLat;
var LoadedSwLat;
var LoadedNeLng;
var LoadedSwLng;

var visitor_user_lat='';
var visitor_user_lon='';
var visitor_user_postal_code='';

var icon_path='<?php echo $themurl;?>/map/';
  
  
function loadScript()
{
    if ( typeof map == 'undefined') {
        
        var script1 = document.createElement('script');
        script1.type = 'text/javascript';
        if(google_key!=''){
        script1.src = 'http://www.google.com/jsapi?key='+google_key;
        } else {
         script1.src = 'http://www.google.com/jsapi';
        }
        document.body.appendChild(script1);
        
        var script = document.createElement('script');
        script.type = 'text/javascript';
        if(google_key!=''){
            script.src = 'http://maps.google.com/maps/api/js?key='+google_key+'&v=3.exp&sensor=false&callback=initmap';
        } else {
            script.src = 'http://maps.google.com/maps/api/js?v=3.exp&sensor=false&callback=initmap';
        }
        document.body.appendChild(script);
        
        
       
    } 
}


function loadScript2(url, callback) {
 
    var script = document.createElement("script")
    script.type = "text/javascript";

    if (script.readyState) { //IE
        script.onreadystatechange = function () {
            if (script.readyState == "loaded" || script.readyState == "complete") {
                script.onreadystatechange = null;
                callback();
            }
        };
    } else { //Others
        script.onload = function () {
            callback();
        };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}



    
    
function initmap(){  
              
           
    var script2 = document.createElement('script');
    script2.type = 'text/javascript';
    script2.src = icon_path+'js/infobox.js';
    //script2.src ='http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js';
    document.body.appendChild(script2);
    




loadScript2(icon_path+'js/oms.min.js',function(){
    
                geocoder = new google.maps.Geocoder();

                var myLatlng = new google.maps.LatLng( system_lat, system_long);

                var mapOptions = {
                    center: myLatlng,                    
                    zoom: mapZoom,
                    maxZoom:maxZoom,
                    minZoom: minZoom,
                    //mapTypeControl:false,             
                    streetViewControl: false,
                    /*disableDefaultUI: true,
                     zoomControl:false,  
                    streetViewControl: false,
                    scrollwheel: false,
                    scaleControl: true,*/
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
             map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);


        oms = new OverlappingMarkerSpiderfier(map, {markersWontMove: true, markersWontHide: true, keepSpiderfied: true, nearbyDistance: 1, legWeight: 2, circleSpiralSwitchover: Infinity, circleFootSeparation:56, usualLegZIndex: 100001, spiderfiedZIndex: 100000, highlightedLegZIndex: 100002});  
        
        
    
       oms.legColors.usual.sepia = '#ffffff';
        oms.legColors.highlighted.sepia = '#ed7620';
        
       /* var mti = google.maps.MapTypeId;
oms.legColors.usual[mti.HYBRID] = oms.legColors.usual[mti.SATELLITE] = '#fff';
oms.legColors.usual[mti.TERRAIN] = oms.legColors.usual[mti.ROADMAP] = '#444';
oms.legColors.highlighted[mti.HYBRID] = oms.legColors.highlighted[mti.SATELLITE] = 
  oms.legColors.highlighted[mti.TERRAIN] = oms.legColors.highlighted[mti.ROADMAP] = '#f00';*/
        
      
     
//   shadow = new google.maps.MarkerImage(
//        'https://www.google.com/intl/en_ALL/mapfiles/shadow50.png',
//        new google.maps.Size(37, 34),  // size   - for sprite clipping
//        new google.maps.Point(0, 0),   // origin - ditto
//        new google.maps.Point(10, 34)  // anchor - where to meet map location
//      );
     
 

    /*infowindow = new google.maps.InfoWindow(info_config); */  
    
    google.maps.event.addListener(map, 'click', function() {
        //infowindow.close();
        $(".infoBox").hide();
    });
    
    var mcOptions = {
        zoomOnClick:false,
        averageCenter:true,  
        ignoreHidden: true,
        maxZoom: minZoomForDisplayMarkers,
        minimumClusterSize:2,
        gridSize: 150,
        styles: [{
            height: 52,
            width: 53,
            textSize: 10,
            textColor: '#ffffff',
            backgroundPosition:'0 0',  
            url: icon_path+"img/m1.png"
        },
        {
            height: 55,
            width: 56,
            textSize: 10,
            textColor: '#ffffff',
            backgroundPosition:'0 0',
            url: icon_path+"img/m2.png"            
        },
        {
            height: 65,
            width: 66,
            textSize: 10,
            textColor: '#ffffff',
            backgroundPosition:'0 0',
            url: icon_path+"img/m3.png"
        },
        {
            height: 77,
            width: 78,
            textSize: 10,
            textColor: '#ffffff',
            backgroundPosition:'0 0',
            url: icon_path+"img/m4.png"
        },
        {
            height: 89,
            width: 90,
            textSize: 10,
            textColor: '#ffffff',
            backgroundPosition:'0 0',
            url: icon_path+"img/m5.png"
        }]
    };
    
    markerCluster = new MarkerClusterer(map, [], mcOptions);
    
    
    google.maps.event.addListener(markerCluster, 'clusteringbegin', function(){
        $(".infoBox").hide();
    });
    google.maps.event.addListener(markerCluster, 'clusteringend', function(){
        $(".infoBox").hide();
    });
    
    
    google.maps.event.addListener(markerCluster, "clusterclick", function (c) {
            
        //alert("&mdash;Center of cluster: " + c.getCenter()+"&mdash;Number of managed markers in cluster: " + c.getSize());
        //          var m = c.getMarkers();
        //          var p = [];
        //          for (var i = 0; i < m.length; i++ ){
        //            p.push(m[i].getPosition());
        //          }
        //alert("&mdash;Locations of managed markers: " + p.join(", "));
          
         
        if(c!=undefined){   
            map.setZoom(map.getZoom() + 1);
            map.setCenter(c.getCenter());
        }
          
    });
    /*google.maps.event.addListener(markerCluster, "mouseover", function (c) {
         
          //alert("&mdash;Center of cluster: " + c.getCenter()+"&mdash;Number of managed markers in cluster: " + c.getSize());
        });
        google.maps.event.addListener(markerCluster, "mouseout", function (c) {
         //alert("&mdash;Center of cluster: " + c.getCenter()+"&mdash;Number of managed markers in cluster: " + c.getSize());
        });*/
    
    var gridOptions = {
        gridSize: 150,
        maxZoom: 8
    };

    //google.maps.event.addListener(map, 'idle', checkZoom);

    // Add listeners for our custom zoom controls
    /*google.maps.event.addDomListener(zoomout, 'click', function() {
        var zoomLevel = map.getZoom();
        if(zoomLevel>maxZoom){
            map.setZoom(zoomLevel - 1);
        }
    });
    google.maps.event.addDomListener(zoomin, 'click', function() {       
       var zoomLevel = map.getZoom();       
       if(zoomLevel<minZoom){
            map.setZoom(map.getZoom() + 1);
        }
    });*/
    
    

        
<?php /*if( $this->session->userdata('visitor_user_lat')!='' && $this->session->userdata('visitor_user_lon')!='' ){
    
    if($this->session->userdata('visitor_user_postal_code')!='') { ?>
          getstores('<?php echo $this->session->userdata('visitor_user_postal_code'); ?>','<?php echo $this->session->userdata('visitor_user_lat'); ?>','<?php echo $this->session->userdata('visitor_user_lon'); ?>',default_distance_range);
            <?php } else { ?>
    getgeolocate('<?php echo $this->session->userdata('visitor_user_lat'); ?>','<?php echo $this->session->userdata('visitor_user_lon'); ?>');
        
        <?php } } else { */ ?>
       if(navigator.geolocation.getCurrentPosition(show_map)){} else { googleloader(); }
<?php /*}*/ ?>
        
      });
      
     }
     
     
     
function checkZoom(){
        
    var zoom = map.getZoom(); 
    
    //change the maxZoom of the markerclusterer
    var bounds = map.getBounds();
    var ne = bounds.getNorthEast();
    var sw = bounds.getSouthWest();
        
    var swlat=sw.lat();
    var swlng=sw.lng();
    var nelat=ne.lat();
    var nelng=ne.lng();
        
      
    if(markerCluster.getTotalMarkers() === 0){
            
    //alert('if not markers to show');
    } else {
        if((swlat<LoadedSwLat) || (swlng<LoadedSwLng) || (nelat>LoadedNeLat) || (nelng>LoadedNeLng) || (LoadedSwLat === undefined)){
    // We load more places
    //getboundCells(swlat, swlng, nelat, nelng);
    }
    }
    
/* //remove al the infoboxes
    var zoom = map.getZoom();
   
        if(zoom<=minZoomForDisplayMarkers && overlaysDisplayed){
            clearOverlays();
        }
        if(zoom>minZoomForDisplayMarkers && !overlaysDisplayed){
            if(lastCat === null)
                showOverlays();
            else
                showOverlay(lastCat,"checkZoom");
        }
        //change the maxZoom of the markerclusterer
//        var bounds = map.getBounds();
//        var ne = bounds.getNorthEast();
//        var sw = bounds.getSouthWest();

        if(markerCluster.getTotalMarkers() === 0){
            //showAllCategories();
        }
        if((swlat<LoadedSwLat) || (swlng<LoadedSwLng) || (nelat>LoadedNeLat) || (nelng>LoadedNeLng) || (LoadedSwLat === undefined)){
                // We load more places
                getboundCells(swlat, swlng, nelat, nelng);
            }
    
     */
}



     

     function show_map(position) {
           //alert(position.coords.latitude+","+ position.coords.longitude);
           
           
           if(position.coords.latitude!='' && position.coords.longitude!=''){
                           
                getgeolocate(position.coords.latitude,position.coords.longitude);

            } 
           
           
        }

      function googleloader(){
          if(google.loader!= undefined) {
                if(google.loader.ClientLocation != undefined)
                {
                    visitor_lat = google.loader.ClientLocation.latitude;
                    visitor_lon = google.loader.ClientLocation.longitude;
                    visitor_city = google.loader.ClientLocation.address.city;
                    visitor_region = google.loader.ClientLocation.address.region;
                    visitor_country = google.loader.ClientLocation.address.country;
                    visitor_countrycode = google.loader.ClientLocation.address.country_code;
                   // alert('<p>Lat/Lon: ' + visitor_lat + ' / ' + visitor_lon + '</p><p>Location: ' + visitor_city + ', ' + visitor_region + ', ' + visitor_country + ' (' + visitor_countrycode + ')</p>');




                    if(visitor_lat!='' && visitor_lon!=''){

                        $("#visitor_user_lat").val(visitor_lat);
                        $("#visitor_user_lon").val(visitor_lon);

                        getapplocate(visitor_lat,visitor_lon);
                    } else{        
                        getstores('',system_lat,system_long);
                    }



                }
                else
                {
                    getapplocate();
                }
            } else {
                    getapplocate();
            }
        }
        
        function getgeolocate(visitor_lat,visitor_lon){
            
            $.ajax({
                url: "<?php echo site_url('printer/getgeolocate');?>",
                type:'post',         
                async: false,  
                data:{lat:visitor_lat,lon:visitor_lon},             
                success: function( response ) {
                  //alert(response);
                  var res=$.parseJSON(response);
                  
                  if(res.postal_code==''){
                    getapplocate(visitor_lat,visitor_lon);
                  } else {
                      
                    $("#visitor_user_lat").val(res.lat);
                    $("#visitor_user_lon").val(res.lon);
                     getstores(res.postal_code,res.lat,res.lon,default_distance_range);
                  }
                }
            });
            
            
        }
       
        function getapplocate(visitor_lat,visitor_lon){
            $.ajax({
                url: "<?php echo site_url('printer/getapplocate');?>",
                type:'post',         
                async: false,  
                data:{lat:visitor_lat,lon:visitor_lon},  
                success: function( response ) {                  
                  var res=$.parseJSON(response);           
                  $("#visitor_user_lat").val(res.lat);
                  $("#visitor_user_lon").val(res.lon);
                  getstores(res.postal_code,res.lat,res.lon,default_distance_range);                  
                }
            });
        }
        
        function getstores(postal_code,lat,lon,distance_range,type)
        {
            //alert(postal_code+','+lat+','+lon);
            if(lat!='' && lon!=''){
               var visitorLatlng = new google.maps.LatLng( lat,lon);   
              
                 map.setCenter(visitorLatlng);
            }
            
            if(distance_range==''){
                distance_range=default_distance_range;
            }
            
             clearmap();   
            
            $.ajax({
                url: "<?php echo site_url('printer/getstore');?>",
                type:'post',         
                async: false,  
                data:{lat:lat,lon:lon,postal_code:postal_code,distance_range:distance_range},             
                success: function( response ) {
                  //alert(response);
                  var res=$.parseJSON(response);
                  
                    sotre_data=res;
                    
                    ///==
                    
                    if(sotre_data.length>0){
                        
                        $("#marker_list").empty();
                        
                        $.each(sotre_data,function(skey,sval){

                            var store_id = sval.store_id;
                            var store_address = stripslashes(sval.store_address);
                            var store_address2 =    stripslashes(sval.store_address2);
                            var store_city =    stripslashes(sval.store_ctiy);                          
                            var store_state =   stripslashes(sval.store_state);
                            var store_postal_code =   stripslashes(sval.store_postal_code);
                            var store_country =    stripslashes(sval.store_country);
                            var store_lat =    sval.store_lat;
                            var store_long =    sval.store_long;
                            var store_name =    stripslashes(sval.store_name);
                            var store_email =    sval.store_email;
                            var store_phone =    sval.store_phone;
                            var store_url =    sval.store_url;
                            var store_slug = sval.store_slug;
                            var store_category = sval.category_id;
                            
                            var store_pin =sval.category_image;
                            
                            var store_domain =sval.store_domain;
        
                            var store_rating =sval.store_total_rating;
                            var store_review =sval.store_total_review;


                               var marker_obj= createMarker(store_id,store_address,store_address2,store_city,store_state,store_postal_code,store_country,store_lat,store_long,store_name,store_email,store_phone,store_rating,store_review,store_url,store_domain,store_slug,store_category,store_pin,lat,lon);

                        });
                        
                        
                        
                               markerCluster.addMarkers(gmarkers);
                
                
                       
                               oms.addListener('spiderfy', function(gmarkers) {
                                //for(var i = 0; i < gmarkers.length; i ++) {
                                  //gmarkers[i].setIcon(iconWithColor(spiderfiedColor));
                                  //gmarkers[i].setShadow(null);
                                //}                 

                                $(".infoBox").hide();
                              });
                              oms.addListener('unspiderfy', function(gmarkers) {
                                //for(var i = 0; i < gmarkers.length; i ++) {
                                  //gmarkers[i].setIcon(iconWithColor(usualColor));
                                  //gmarkers[i].setShadow(shadow);
                                //}
                              });
                              
                              centerMap();
              
                        
                    } else {
                        if(type=='s'){
                        alert("No 3D printers found. Please refine your search criteria.");
                        }
                    }
                    
                    //===
                   
                  
                }
            });
        }
        
function centerMap() {
      map.setCenter(gmarkers[gmarkers.length-1].getPosition());
    }
            

                function createMarker(store_id,store_address,store_address2,store_city,store_state,store_postal_code,store_country,store_lat,store_long,store_name,store_email,store_phone,store_rating,store_review,store_url,store_domain,store_slug,store_category,store_pin,vlat,vlong) {


                             if(vlat=='' && vlong==''){
                             <?php if( $this->session->userdata('visitor_user_lat')!='' && $this->session->userdata('visitor_user_lon')!='' ){ ?>
                                     var distance = getDistance(<?php echo $this->session->userdata('visitor_user_lat');?>,<?php echo $this->session->userdata('visitor_user_lon'); ?>,store_lat, store_long); 
                            <?php } else { ?>
                                var distance = getDistance(system_lat,system_long,store_lat, store_long); 
                            <?php } ?>
                                 } else {
                                    var distance = getDistance(vlat,vlong,store_lat, store_long); 
                                }


                                var storeLatlng = new google.maps.LatLng( store_lat, store_long);

                                var marker = new google.maps.Marker({
                                    map: map,
                                    position: storeLatlng
                                });
                                
                               if(store_pin!=''){
                                   
                                } else {
                                    store_pin=icon_path+'bee-3d.png';
                                }
                                
                                
                                
                                 marker.setIcon(({
                                        url: store_pin,
                                        size: new google.maps.Size(39,51),
                                        origin: new google.maps.Point(0, 0),
                                        anchor: new google.maps.Point(19.5,25.5),
                                        scaledSize: new google.maps.Size(39,51)
                                    }));

                                marker.storecategory=store_category;
                               


                                var pin_content='';
                                var sideHtml = '';
                                
                               // sideHtml += '<li data-storeid="'+store_id+'">';
                               
                               sideHtml +='<div class="storefirst">';
                               
            if(store_name!=''){
                pin_content+='<strong style="font-size:22px;"><a href="'+store_slug+'">'+store_name+'</a></strong>';
                sideHtml += '<div class="store_name"><b><a href="'+store_slug+'">'+store_name+'</a></b></div>';

            }
            
            pin_content+='<p class="pinrating"><img src="<?php echo $themurl;?>/rating/'+store_rating+'.png" /><span> <a href="'+store_slug+'">REVIEWS('+store_review+')</a></span></p>';
        
                               
                               
                               sideHtml +='<div class="pinrating"><img src="<?php echo $themurl;?>/rating/'+store_rating+'.png" /></div>';
                               sideHtml +='<div class="pinreviews"><a href="'+store_slug+'">REVIEWS('+store_review+')</a></div>';
                                
                                sideHtml += '<div class="store_name"><b>'+distance+'km</b></div>';
                                
                                
                                 sideHtml +='</div>';
                                 
                                  sideHtml +='<div class="storesec">';
                                 
                                 
                                  if(store_address!=''){
                                    //pin_content+= '<p>'+store_address+'</p>';
                                    sideHtml += '<p style="clear:both;">'+store_address+'</p>';
                                }
                                if(store_address2!=''){
                                   // pin_content+= '<p>'+store_address2+'</p>';
                                    sideHtml += '<p style="clear:both;">'+store_address2+'</p>';
                                }
                                
                                pin_content+= '<p style="clear:both;">';
                                if(store_city!=''){
                                    pin_content+= ''+store_city;
                                    sideHtml += '<p>'+store_city+'</p>';
                                }
                                if(store_state!=''){
                                    
                                    if(store_city!=''){
                                        pin_content+= ','+store_state;
                                    } else {
                                        pin_content+= ''+store_state;
                                    }
                                   
                                   if(store_postal_code!=''){ 
                                      sideHtml += '<p>'+store_state+''; 
                                   
                                   } else {
                                       sideHtml += '<p>'+store_state+'</p>';
                                   }
                                   
                                   
                                   
                                }
                                if(store_postal_code!=''){
                                    pin_content+= '-'+store_postal_code;
                                    
                                    if(store_state!=''){
                                        sideHtml += ', '+store_postal_code+'</p>';
                                    } else {
                                          sideHtml += '<p>'+store_postal_code+'</p>';
                                    }
                                  
                                }
                                 pin_content+= '</p>';
                                
                                if(store_country!=''){
                                    pin_content+= '<p style="clear:both;">';
                                    pin_content+= ''+store_country;
                                    pin_content+= '</p>';
                                    
                                    sideHtml += '<p>'+store_country+'</p>';
                                }
                                 
                                 
                                 if(store_phone!=''){
                                    
                                    pin_content+= '<p style="clear:both;">';
                                    pin_content+= ''+store_phone;
                                    pin_content+= '</p>';
                                    sideHtml += '<div class="store_phone"><span>Phone:</span> '+store_phone+'</div>';
                                }
                                
                                
                                  sideHtml +='</div>';
                                 
                                 sideHtml +='<div class="storethird">';
                                 
                                 if(store_url!=''){
                                     pin_content+= '<p style="clear:both;">';                                   
                                    pin_content+= '<a href="'+store_url+'">'+store_domain+'</a>';
                                    pin_content+= '</p>';
                                    sideHtml += '<p class="store_url"><a href="'+store_url+'">Website</a></p>';
                                }
                                 
                                 if(store_email!=''){
                                      pin_content+= '<p style="clear:both;">';    
                                    pin_content+= ''+store_email;
                                     pin_content+= '</p>';
                                    sideHtml += '<p class="store_email"><a href="mailto:'+store_email+'">'+store_email+'</a></p>';
                                }
                                
                                
                                 pin_content+= '<p style="clear:both;">';
                                    pin_content+= '<strong>Distance: </strong>'+distance+'km';
                                     pin_content+= '</p>';
                                
                                
                                sideHtml +='<p class="storelocatorlink"><a href="http://maps.google.com/maps?saddr='+store_country+'&amp;daddr='+store_address+','+store_address2+','+store_city+','+store_state+','+store_postal_code+','+store_country+'" target="_blank">Directions</a></p>';
                                
                                  sideHtml +='</div>';
                                  
                                
                                
                                
                              sideHtml +='<div class="clear"></div>';
                                
                                //sideHtml += '</li>';
                             
                             
                             
                             var info_html='<div class="mapTip"><div class="tipInfoBox">';
                                info_html+=pin_content;
                                 info_html += '</div>';          
                                info_html += '<div class="mapArrow"><img src="'+icon_path+'mapTipArrow.png" /></div>';
                                info_html += '</div>';    
                                
                           var left_side = -168;
                            var top_height = -218;     
                            
                            var boxText = document.createElement("div");
                            //boxText.style.cssText = "border: 1px solid black; margin-top: 8px; background: #999; border-radius:5px; padding: 5px; color:#FFF;";
                            boxText.innerHTML = info_html;

                            var myOptions1 = {
                                alignBottom:false,
                                position:storeLatlng,
                                content: boxText,
                                disableAutoPan: false,
                                maxWidth: 0,
                                pixelOffset: new google.maps.Size(left_side,top_height),
                                zIndex: null,
                                boxStyle: { 
                                    // background: "url('"+icon_path+"images/tipbox.gif') no-repeat",
                                    opacity: 1,
                                    width: "340px"
                                },
                                closeBoxMargin: "2px",
                                closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                                infoBoxClearance: new google.maps.Size(1, 1),
                                isHidden: false,
                                pane: "floatPane", //mapPane
                                enableEventPropagation: false
                            };
                            ib[store_id] = new InfoBox(myOptions1);
    
    
    

                                google.maps.event.addListener(marker, 'click', function(){          
                                  //infowindow.setContent(pin_content);
                                  //infowindow.open(map, this);
                                   $(".infoBox").hide();
                                    ib[store_id].open(map, marker); 
                                });
                                
                                
                                
                               
                                
                                // save the info we need to use later for the side_bar
                                gmarkers.push(marker);
                                oms.addMarker(marker);

                                
                                var ul = document.getElementById("marker_list");
                                var li = document.createElement("li");    
                                li.setAttribute('data-storeid',store_id);
                                li.setAttribute('data-storecatid',store_category);
                                li.innerHTML = sideHtml;
                                ul.appendChild(li);
                                
                              
                               
                                 //Trigger a click event to marker when the button is clicked.
                                  google.maps.event.addDomListener(li, "click", function(){
                                    google.maps.event.trigger(marker, "click");
                                  });
//                                  google.maps.event.addDomListener(li, "mouseover", function(){
//                                    google.maps.event.trigger(marker, "click");
//                                  });
                                 
                                

             

                }





function togglecategory(cid) {
    $(".infoBox").hide();
   
    $(".sidecat ul li").removeClass('active');
   
    if(cid==0){
         for (var i=0; i<gmarkers.length; i++) {         
            gmarkers[i].setVisible(true); 
            $("ul#marker_list li").show();
        }  
    } else {
        
          $(".sidecat ul li#"+cid).addClass('active');
          
         for (var i=0; i<gmarkers.length; i++) {
          if (gmarkers[i].storecategory == cid) {
            gmarkers[i].setVisible(true);             
          } else {
              gmarkers[i].setVisible(false);
          }
        } 
        
         $('ul#marker_list li').each(function(){
            var self = $(this),
                licid = self.attr('data-storecatid');

               if(licid==cid){
                   self.show();
               } else {
                   self.hide();
               }

        });
        
    }
    
   //markerCluster.setMap( map );
   // markerCluster.resetViewport();
//markerCluster.redraw();

markerCluster.repaint();
   
}
function drawCircle(cent) {

    circle_radius = 40; // convert to meters if in miles
    if (draw_circle !== null) {
        draw_circle.setMap(null);
    }
    draw_circle = new google.maps.Circle({
        center: cent,
        radius: circle_radius,
        strokeColor: "#0099FF",
        strokeOpacity: 1,
        strokeWeight: 1,
        fillColor: "#333333",
        fillOpacity: 0.50,
        //map: map,
        zIndex: 9999
    });
    
    draw_circle.setMap(map);
}
    
    
      
function clearmap() {        
    $.each(gmarkers, function(index) {
        deleteMarker(gmarkers[index]);
        //oms.removeMarker(gmarkers[index]);
    });
    gmarkers = [];
    markerCluster.clearMarkers();
    oms.clearMarkers();
    //draw_circle.setMap(null); 
    $(".infoBox").hide();
   
}

function deleteMarker(marker) {
    marker.setMap(null); 
}
 
    
function getboundCells(swlat, swlng, nelat, nelng){
    // the width of the cell with 400 columns 0.8, so for 600 => 0.6
    var w = 0.6;
    // the height of the cell with 200 rows 0.85, so for 400 => 0.425
    var h = 0.425;
    //find in which cell is the sw corner
    var swColResult = Math.round(((swlng + 180) / w));
    var swRowResult = Math.round((400-((swlat + 85) / h)) + 1);
    var neColResult = Math.round(((nelng + 180) / w)+1);
    var neRowResult = Math.round((400-((nelat + 85) / h)));

    cellsToLoad["RequestMinCol"]=swColResult;
    cellsToLoad["RequestMaxCol"]=neColResult;
    cellsToLoad["RequestMinRow"]=neRowResult;
    cellsToLoad["RequestMaxRow"]=swRowResult;

    if(!jQuery('#LoadingDiv').is(':visible')){
        jQuery('#LoadingDiv').show();
    }
    //load the places
    loadCells(swlat, swlng, nelat, nelng);
}

function loadCells(swlat, swlng, nelat, nelng){
    var margin = (nelng - swlng)/5; // the margin is 1/5 of the width of the screen
    LoadedNeLat = (nelat+margin);
    LoadedSwLat = (swlat-margin);
    LoadedNeLng = (nelng+margin);
    LoadedSwLng = (swlng-margin);
    jQuery.ajax({
        dataType: 'json',
        url: 'action=getMarkersAround&minlat='+LoadedSwLat+'&maxlat='+LoadedNeLat+'&minlng='+LoadedSwLng+'&maxlng='+LoadedNeLng,
        success: function(places) {
        
        }
    });
}

            
$(document).ready(function(){
        
        loadScript();
        
        $("#find_stores").on('click',function(){
        
            
            var search_postal_code=$("#postal_code").val();
            var search_distance_range = $("#distance_range option:selected").val();
            
            
            var lat=system_lat;
            var lon=system_long;
            
         
            <?php if($this->session->userdata('visitor_user_lat')!='' && $this->session->userdata('visitor_user_lon')!='' ){ ?>
            	var lat='<?php echo $this->session->userdata("visitor_user_lat"); ?>';
            	var lon='<?php echo $this->session->userdata("visitor_user_lon"); ?>';
            	//alert(lat+"==="+lon);
            <?php } ?>
            
            
            
            getstores(search_postal_code,lat,lon,search_distance_range,'s');
            
            
        });

});



           




           

        </script>
       
        
        <style>
          
            p.pinrating {
                border-bottom: 1px solid #ccc;
padding-bottom: 31px;

            }
            p.pinrating img {
  float: left;  
  width: 125px;
}
p.pinrating span {
    font-size: 20px;
float: left;
padding-left: 5px;
}

div.pinrating img {
  
  width: 105px;
}
div.pinreviews {
    padding-top: 5px;
}


    /*-- mapTip --*/
.mapTip{
	position:relative;
	width:340px;
        height: auto;
/*	font-family: 'proxima_nova_rgregular';*/
	color:#000;
}

.mapTip [class*="col-"]{padding:0;}
.mapTip .tipInfoBox{
	padding:7px !important;
	min-height: 200px;
	border-radius:1px;
	-webkit-border-radius:1px;
	background: #fff; /* url('/images/mapTipInfoBG.png') left center repeat-x;*/
}
.mapTip .tipInfoBox p{
    margin: 0 0 5px;
    font-size: 12px;

}
.mapTip .mapArrow{
	position:absolute;
	left:46%;
	bottom:-12px;
	margin-left:-12px;
}
.infoclose img { z-index: 9;  }
#map-canvas {
	width: 100%; height: 500px;
	float:none;
	margin: 0px;
	padding: 0px;
}
#locs {
	margin: 10px 0px 0px 0px;
	padding: 0px;	
        clear: both;
	float:none;
	height: 500px;
        width:100%;
	overflow-y: scroll;
}
#marker_list {
    list-style: none;
    margin:0px !important;
    padding: 0px !important;	
}
#marker_list li {
        list-style: none;
	border: 1px solid #CCC;
	width: 100%;
	padding: 5px;
	cursor:pointer;
	margin:0px 0px 10px 0px;
}
#marker_list li:hover {
    background: #F8F8F8;
}

.storefirst {
    width:35%;
    float: left;
}
.storesec {
    width:50%;
    float: left;
}

.storethird {
    width:15%;
    float: left;
}
.clear { clear: both;}

.store_name { color:#333333; font-size: 16px; }


.storesec p {
    line-height: 10px;
}

.store_phone span { font-weight: bold; }

.store_url {
    
}

.store_url a {
    font-weight: bold;
}

.store_email {
    
}

.storelocatorlink {
    
}
.storelocatorlink a {
    font-weight: bold;
}

.label-control{
    color: #333333;
    font-family: 'conduit_itclight';
    font-size: 36px;
    text-transform: uppercase;
}
.form-control{
    font-size: 25px;
    height: 48px;
}


.sidecat {
    position: absolute;
    top: 65px;
    right: 0px;
    width: 225px;
    padding: 0px;
    background: #fff;
    -webkit-box-shadow: -2px -4px 19px 0px rgba(97, 96, 107, 0.75);
    -moz-box-shadow:    -2px -4px 19px 0px rgba(97, 96, 107, 0.75);
    box-shadow:         -2px -4px 19px 0px rgba(97, 96, 107, 0.75);
}

.sidecat ul {
    list-style: none;
    margin: 0px;
    padding: 0px;
}
.sidecat ul li {
    list-style: none;
    clear: both;
    line-height: 30px;
    padding: 7px 5px;
    cursor: pointer;
}
.sidecat ul li.active {
    background-color: #e8e8e8;
}

.sidecat ul li img {
    float:left;
    width: 25px;
}
.sidecat ul li p {
    float:left;
    font-size: 14px;
    margin: 0px;
    padding: 0px 0px 0px 7px;
}

        </style>
<!-- *************************************************************************************** -->
<div class="wrapper row11">
	<div class="container">
    	<div class="search-block"> 
            
            
            <div class="col-md-2">
                <lable class="label-control">Zipcode</lable>
            </div>
                <div class="col-md-2">
                <input type="text" maxlength="10" name="postal_code" id="postal_code" class="form-control" />
                </div>
            <div class="col-md-2">
                <lable class="label-control">Within</lable>
            </div>
            <div class="col-md-2">
                <select name="distance_range" id="distance_range" class="form-control">
                    <option value="10">10 km</option>
                    <option value="25">25 km</option>
                    <option value="50">50 km</option>
                    <option value="100">100 km</option>
                    <option value="200">200 km</option>
                    <option value="500">500 km</option>
                    <option value="1000">1000 km</option>
                </select>
            </div>
            <div class="col-md-2">
                <input type="button" name="find_stores" id="find_stores" value="Find Locations" class="btn" />
            </div>
            
            <div class="clearfix" style="height:15px;" ></div>
            
	
            
            <div style="position: relative;">
        <div id="map-canvas"></div>
        
       
        <div class="sidecat">
                        <ul>
                           <li onclick="togglecategory(this.id)" id="0">
                                <img src="<?php echo $themurl;?>/map/bee-3d.png"> <p>All</p>
                                <div class="clear"></div>
                            </li>
                             <?php if(isset($store_category) && !empty($store_category)) { ?>
                            <?php foreach($store_category as $cres) {
                                $cat_pin_url='';
                 if (file_exists(base_path() . 'upload/category_orig/' . $cres->category_image) && $cres->category_image != '') {
                         $cat_pin_url=base_url() . 'upload/category_orig/' . $cres->category_image; 
                 } 
            
                                
                                ?>
                            <li onclick="togglecategory(this.id)" id="<?php echo $cres->category_id; ?>">
                                <img src="<?php echo $cat_pin_url;?>"> <p><?php echo ucfirst($cres->category_name); ?></p>
                                <div class="clear"></div>
                            </li>
                            <?php } ?>
                             <?php } ?>
                        </ul>
        </div>
        
       
        </div>
        
        
        
        <div id="locs"><ul id="marker_list"></ul></div>

    
        
        
        <div class="clear"></div>
        
        </div>
    	 
    </div>
</div> 
<div class="clear"></div>
  
<!-- *************************************************************************************** -->