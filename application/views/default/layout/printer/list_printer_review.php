<script type="text/javascript">
function delete_printer_review(id)
{
	var ans = confirm("Are you sure, you want to delete Review?");
	if(ans)
	{
		location.href = "<?php echo site_url('printer/delete_review_me/'.$store_detail->store_id); ?>/"+id;
	}else{
		return false;
	}
}
</script>
	<!-- aditional stylesheets -->
		<!-- responsive table -->
			<link href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>

		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><a href="<?php echo site_url('printer/all')?>">3D Printer</a></li>	
                                                <li><span>Review</span></li>	
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
					<?php if($msg!='') { 
					 if($msg == 'success')
					 {?>
					 <div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "Review has been inserted successfully."; ?>
								
							</div>
					 <?php }
					 if($msg == 'update')
					 {?>
					  <div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "Review has been updated successfully."; ?>
								
							</div>
					 <?php }
					 if($msg == 'delete')
					 {?>
					  <div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php  echo "Review has been deleted successfully."; ?>
								
							</div>
					 <?php }
					 } ?>
                                            <input type="hidden" id="store_id" value="<?php echo $store_detail->store_id; ?>" />

						<!-- main content -->
						<div class="row">
							<div class="col-sm-12">
								<div class="panel panel-default">
                                            <div class="panel-heading">
                                                    <h4 class="panel-title">Review of <a target="_blank" href="<?php echo site_url('printer/'.$store_detail->store_slug);?>"><span data-toggle="tooltip" data-placement="top auto" title="View 3D Printer"><b><?php echo ucfirst(stripslashes($store_detail->store_name)); ?> </b></span></a>
                                                    </h4>
                                            </div>

                                            <table id="resp_table" class="table toggle-square" data-filter="#table_search" data-page-size="40">
                                                    <thead>
                    <tr>
                            <th>Review</th>
                            <th>User</th>
                            <th>Date</th>												
                            <th>Rating</th>
                            <th>Action</th>
                    </tr>
            </thead>
            <tbody>
                    <?php if($result)
                    {
                       
                            foreach($result as $res)
                            {?>
                    <tr>
                        <td class="rv<?php echo $res->store_review_id; ?>" style="width: 50%;"><?php echo stripslashes($res->store_review_description);?></td>
                            <td><?php
                            
                            
                            if($res->profile_name!='' && ($res->first_name!='' || $res->last_name!='')) {                            
                                echo anchor('bee/'.$res->profile_name,$res->first_name.' '.$res->last_name,' target="_blank" ');
                            } else{
                                
                                if($res->first_name!='' || $res->last_name!=''){
                                    echo $res->first_name.' '.$res->last_name;
                                } else {
                                    echo "Unknown";
                                }
                                
                            }
                            
                            
                            ?></td>
                            <td><?php echo date($site_setting->date_format, strtotime($res->store_review_date)); ?></td>
                           
                           


                            <td><img src="<?php echo base_url().$theme;?>/rating/<?php echo roundDownToHalf($res->store_rating);?>.png" border="0" height="20" /></td>
                            <td>
                            
                                
            <a class="btn btn-success editinforeview" data-id="<?php echo $res->store_review_id; ?>" title="Edit review description of 3D Printer" ><span data-toggle="tooltip" data-placement="top auto" title="Edit review description of 3D Printer">Edit</span></a>
                                    <a class="btn btn-danger" title="Delete this 3D Printer Review from personal data base" href="javascript://" onclick="delete_printer_review('<?php echo $res->store_review_id;?>');"><span data-toggle="tooltip" data-placement="top auto" title="Delete this 3D Printer Review from database">Delete</span></a>
                            </td>
                    </tr>	
                    <?php }}
                    else
                    {?>
                    <tr>
                            <td colspan="5">No one have added review on this 3D Printer yet!!</td>

                    </tr>
                    <?php }?>


            </tbody>

                    <tr>
                            <td colspan="5" class="text-center">
                                    <ul class="">
                                    <?php echo $page_link;?>
                                    </ul>
                            </td>
                    </tr>

    </table>
</div>
</div>
						</div>
						
					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
			<div id="footer_space"></div>
		</div>

       <!-- responsive table -->
			

		
                        

<div class="modal fade" id="editreview">
       <div class="modal-dialog">
            <div class="modal-content">
                    <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Review</h4>
                    </div>
                    <div class="modal-body">
                      
                       
                       
                        <textarea id="edit_review_description" class="form-control autosize_textarea" cols="70" rows="12"></textarea>
                        <input type="hidden" id="edit_review_id" />
                        
                    
                       
                        <div class="clearfix"></div>
                        
                    </div>
                <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary updatereview">Save changes</button>
                </div>
                   
            </div>
    </div>
</div>
    
                        <script>
                            //store_id
                    $(document).ready(function(){    
                       
                       
                        $(".editinforeview").on("click",function(){
                            
                            var review_id=$(this).attr('data-id');
                            var review_description=$("td.rv"+review_id).html();
                            
                            $("#edit_review_description").val(review_description);
                            $("#edit_review_id").val(review_id);
                            
                            $("#editreview").modal('show');
                        });
                        
                        
                        $('#editreview').on('shown.bs.modal', function(){
                            
                        });
    
                       
                        $('#editreview').on("hidden.bs.modal", function(){    
                            $("#edit_review_description").val('');
                            $("#edit_review_id").val('');
                        });
                        
                    });
                    </script>                        
    