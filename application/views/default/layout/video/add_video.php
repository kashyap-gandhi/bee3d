<!-- Add fancyBox main JS and CSS files -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . getThemeName(); ?>/js/tag/jquery.tagsinput.css" />
<!-- responsive lightbox -->
<?php /* ?> <link rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/magnific-popup/magnific-popup.css">
  <?php */ ?><script type="text/javascript" src="<?php echo base_url() ?>tinymce/tinymce.min.js"></script>
<!-- aditional stylesheets -->


<style>
    #err1,#err2,#err3,#err4 {
        color: red;
    }
</style>
<script type="text/javascript">
    tinymce.init({selector:'#video_content'});
    function append_div2()
    {
        var tmp_div2 = document.createElement("div");
        tmp_div2.className = "";

        var glry_cnt=document.getElementById('glry_cnt').value;

        /*if(glry_cnt<5)
{*/
        document.getElementById('glry_cnt').value=parseInt(glry_cnt)+1;
        var num=parseInt(glry_cnt)+1;

        tmp_div2.id='galry'+num;
        var content=document.getElementById('more2').innerHTML;
        var str = '<div onclick="remove_gallery_div('+num+')" style="font-weight:normal;cursor:pointer;color:#ff0000; width:100px;">Remove</div><div class="clear"></div>';
        tmp_div2.innerHTML = content +str;

        document.getElementById('add_more2').appendChild(tmp_div2);



    }


    function remove_gallery_div(id)
    {
        var element = document.getElementById('galry'+id);
        var add_more = parent.document.getElementById('add_more2');

        element.parentNode.removeChild(element);


        var glry_cnt=document.getElementById('glry_cnt').value;
        document.getElementById('glry_cnt').value=parseInt(glry_cnt)-1;

        document.getElementById('addimg').style.display='block';

    }

    function show_parent_video()
    {
        $('#parent_video_div').slideToggle();
    }

    function show_video(str)
    {
        if(str == 'external')
        {
            $('#external_video').show();
            $('#internal_video').hide();
            $('#video_custom_image').hide();
        }
        else
        {
            $('#internal_video').show();
            $('#video_custom_image').show();
            $('#external_video').hide();
        }
    }

    $(document).ready(function(){


<?php if ($video_id > 0) { ?>

            $('#video_custom_image').show();

<?php } else { ?>

            $('#video_custom_image').hide();

<?php } ?>

        $("#video_form").submit(function(){

            var check=true;

            document.getElementById('err1').style.display='none';
            document.getElementById('err2').style.display='none';
            document.getElementById('err3').style.display='none';
            document.getElementById('err4').style.display='none';



            var video_type = document.getElementById("video_type").value;



            if(video_type=='')
            {

                check=false;
                var dv = document.getElementById('err1');
                dv.style.display='block';
                dv.className = "error";
                dv.style.clear = "both";
                dv.style.minWidth = "110px";
                dv.style.margin = "5px";
                dv.innerHTML = 'Please select Video Type.';
                return false;

            }




            if(video_type==1)
            {



                //====for video=====

                if(document.getElementById('prev_video_image').value == "")
                {

                    if(document.getElementById('video_file').value == "")
                    {
                        check=false;

                        var dv = document.getElementById('err3');
                        dv.style.display='block';
                        dv.className = "error";
                        dv.style.clear = "both";
                        dv.style.minWidth = "110px";
                        dv.style.margin = "5px";
                        dv.innerHTML = 'Please upload video.';
                        return false;


                    } else {

                        value = document.getElementById('video_file').value;
                        var t1 = value.substring(value.lastIndexOf('.') + 1,value.length);

                        if( t1=='3gp' || t1=='avi' || t1=='flv' || t1=='mp4' || t1=='mov' || t1=='mpg' || t1=='mpeg' || t1=='qt' || t1=='wmv' || t1=='webm' || t1=='ogg' || t1=='ogv' || t1=='3GP' || t1=='AVI' || t1=='FLV' || t1=='MP4' || t1=='MOV' || t1=='MPG' || t1=='MPEG' || t1=='QT' || t1=='WMV' || t1=='WEBM' || t1=='OGG' || t1=='OGV'){





                            check=true;

                        } else {

                            check=false;

                            var dv = document.getElementById('err3');
                            dv.style.display='block';
                            dv.className = "error";
                            dv.style.clear = "both";
                            dv.style.minWidth = "110px";
                            dv.style.margin = "5px";
                            dv.innerHTML = 'Please enter valid video.';
                            return false;
                        }
                    }
                }

                ////===end video


                ///====for image==
<?php if ($video_id > 0) { ?>
                    if(document.getElementById('prev_video_image').value == "")
                    {

                        if(document.getElementById('video_image').value == "")
                        {

                            check=false;
                            var dv = document.getElementById('err2');
                            dv.style.display='block';
                            dv.className = "error";
                            dv.style.clear = "both";
                            dv.style.minWidth = "110px";
                            dv.style.margin = "5px";
                            dv.innerHTML = 'Please upload video image.';
                            return false;

                        } else {

                            value = document.getElementById('video_image').value;
                            t1 = value.substring(value.lastIndexOf('.') + 1,value.length);

                            if( t1=='jpg' || t1=='jpeg' || t1=='gif' || t1=='png' || t1=='JPG' || t1=='JPEG' || t1=='GIF' || t1=='PNG'){

                                check=true;

                            } else {

                                check=false;

                                var dv = document.getElementById('err2');
                                dv.style.display='block';
                                dv.className = "error";
                                dv.style.clear = "both";
                                dv.style.minWidth = "110px";
                                dv.style.margin = "5px";
                                dv.innerHTML = 'Please upload valid image.';
                                return false;
                            }
                        }

                    } else {
                        if(document.getElementById('video_image').value != "")
                        {
                            value = document.getElementById('video_image').value;
                            t1 = value.substring(value.lastIndexOf('.') + 1,value.length);

                            if( t1=='jpg' || t1=='jpeg' || t1=='gif' || t1=='png' || t1=='JPG' || t1=='JPEG' || t1=='GIF' || t1=='PNG'){

                                check=true;

                            } else {

                                check=false;

                                var dv = document.getElementById('err2');
                                dv.style.display='block';
                                dv.className = "error";
                                dv.style.clear = "both";
                                dv.style.minWidth = "110px";
                                dv.style.margin = "5px";
                                dv.innerHTML = 'Please upload valid image.';
                                return false;
                            }
                        }
                    }
<?php } ?>



            } else if (video_type==2) {

                if(document.getElementById('video_url').value == "")
                {
                    check=false;

                    var dv = document.getElementById('err4');
                    dv.style.display='block';
                    dv.className = "error";
                    dv.style.clear = "both";
                    dv.style.minWidth = "110px";
                    dv.style.margin = "5px";
                    dv.innerHTML = 'Video URL is required.';
                    return false;

                }else{

                    check=true;

                }

            }



            if(check==true)
            {
                return true;
            }


        });
    });


</script>
<!-- parsley -->
<!-- responsive table -->
<link href="<?php echo base_url() . getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>
<div id="wrapper_all">
    <?php
    $theme = getThemeName();
    echo $this->load->view($theme . '/layout/common/header_menu', TRUE);
    ?>
    <!-- mobile navigation -->
    <nav id="mobile_navigation"></nav>

    <section id="breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="<?php echo site_url('user/dashboard') ?>">Home</a></li>
                <li><span>Video</span></li>
            </ul>
        </div>
    </section>

    <section class="container clearfix main_section">
        <div id="main_content_outer" class="clearfix">
            <div id="main_content">

                <!-- main content -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><?php if ($video_id > 0) { ?>Edit<?php } else { ?>Add<?php } ?> Video</h4>
                            </div>
                            <?php if ($error != '') { ?>
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php echo $error; ?>

                                </div>
                            <?php } ?>

                            <div class="panel-body">
                                <?php
                                $attributes = array('name' => 'video_form', 'id' => 'video_form', 'class' => '');
                                echo form_open_multipart('video/add_me', $attributes);
                                ?>

                                <?php if ($video_type == 2) { ?><script>show_video('external');</script><?php } else { ?><script>show_video('internal');</script><?php } ?>

                                <input type="hidden" name="video_type" id="video_type" class="form-control" value="1"/>
                                <input type="hidden" name="comment_allow" id="comment_allow" value="1"/>
                                <div id="err1"></div>

                                <div class="form_sep">
                                    <label for="reg_input_name" class="req">Name</label>
                                    <input type="text" id="video_title" name="video_title" class="form-control" data-required="true" value="<?php echo $video_title; ?>">
                                </div>
                                <div class="form_sep">
                                    <label for="reg_input_email" class="req">Detail Description of Video</label>
                                    <textarea name="video_content" id="video_content" cols="30" rows="4" class="form-control"><?php echo $video_content; ?></textarea>

                                </div>

                                <div class="form_sep">
                                    <label for="reg_select"> <span class="req" data-toggle="tooltip" data-placement="right auto" title="Category & Sub-Categories For Storage of This Video">Video Category  </span> <a style="padding-left: 10px;" data-toggle="modal" href="#videocategory" ><span class="glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="left auto" title="Add Category"></span></a></label>

                                    <select name="video_category" id="video_category" class="form-control" data-required="true">
                                        <option value="">--Select Category--</option>
                                        <?php
                                        echo $category;
                                        /*if ($category) {
                                            foreach ($category as $cat) {
                                                ?>
                                                <option value="<?php echo $cat->category_id; ?>" <?php if ($video_category == $cat->category_id) { ?> selected="selected"<?php } ?>><?php echo $cat->category_name; ?></option>

                                                <?php
                                            }
                                        }*/
                                        ?>
                                    </select>
                                </div>




                                <?php /* ?><div class="form_sep">
                                  <label for="reg_textarea_message">Video Type</label>
                                  <div class="radio">
                                  <input type="radio" name="video_type" class="form-control" value="1" onclick="show_video('internal');"/>	External
                                  </div>
                                  <div class="radio">
                                  <input type="radio" name="video_type" class="form-control" value="2" onclick="show_video('external');"/>	Internal
                                  </div>
                                  </div>

                                  <div class="form_sep">
                                  <label for="reg_textarea_message">Comment Allow</label>
                                  <div class="radio">
                                  <input type="radio" name="comment_allow" class="form-control" value="0" <?php if($comment_allow == 0){?> checked="checked"<?php }?>/>	No
                                  </div>
                                  <div class="radio">
                                  <input type="radio" name="comment_allow" class="form-control" value="1" <?php if($comment_allow == 1){?> checked="checked"<?php }?>/>	Yes								</div>
                                  </div><?php */ ?>




                                <div class="form_sep" id="internal_video" style="display: block;">
                                    <label for="reg_textarea_message" class="req">Upload Video</label> <span id="err3"></span>
                                    <input type="file" class="form-control" id="video_file" name="video_file" />
                                    <input type="hidden" name="prev_video_file" id="prev_video_file" value="<?php echo $prev_video_file; ?>" />

                                </div>




                                <div class="form_sep" id="video_custom_image" style="display: block;">
                                    <label for="reg_textarea_message" class="req">Upload Custom Image</label> <span id="err2"></span>
                                    <input type="file" class="form-control" id="video_image" name="video_image" />

                                    <input type="hidden" name="prev_video_image" id="prev_video_image" value="<?php echo $prev_video_image; ?>" />

                                </div>



                                <div class="form_sep" id="external_video" style="display: none;">
                                    <label for="reg_textarea_message" class="req">3rd Party Video URL</label> <span id="err4"></span>
                                    <input type="text" class="form-control" id="video_url" name="video_url"  value="<?php echo $video_url; ?>" />
                                </div>










                                <div class="form_sep">
                                    <label for="reg_textarea_message" class="req">Number of credits to buy the video</label>
                                    <input type="text" id="video_point" name="video_point" class="form-control" value="<?php echo $video_point; ?>">
                                </div>

                                <?php /* ?><div class="form_sep">
                                  <label for="reg_textarea_message">Video Referal ID</label>
                                  <input type="text" id="video_ref_id" name="video_ref_id" class="form-control" value="<?php echo $video_ref_id;?>">
                                  </div><?php */ ?>




                                <div class="form_sep">
                                    <label for="reg_textarea_message">Video is modified</label>
                                    <input type="checkbox" id="video_is_modified" name="video_is_modified" class="form-control" value="1" onclick="show_parent_video();" <?php if ($video_is_modified == 1) { ?> checked="checked"<?php } ?>>
                                </div>

                                <?php
                                if ($video_is_modified == 1) {
                                    $prd = 'style="display:block;"';
                                } else {
                                    $prd = 'style="display:none;"';
                                }
                                ?>
                                <div class="form_sep" id="parent_video_div" <?php echo $prd; ?>>
                                    <label for="reg_select">Parent Video</label>
                                    <select name="parent_video_id" id="parent_video_id" class="form-control" data-required="true">
                                        <option value="">--Select Parent Video--</option>
                                        <?php
                                        if ($allvideo) {
                                            foreach ($allvideo as $dg) {
                                                ?>
                                                <option value="<?php echo $dg->video_id; ?>" <?php if ($parent_video_id == $dg->video_id) { ?> selected="selected"<?php } ?>><?php echo $dg->video_title; ?></option>

                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="form_sep">
                                    <label for="reg_textarea_message">Meta Keywords</label>
                                    <textarea rows="3" cols="70" class="form-control autosize_textarea" name="video_meta_keyword" id="video_meta_keyword" ><?php echo $video_meta_keyword; ?></textarea>

                                </div>

                                <div class="form_sep">
                                    <label for="reg_textarea_message">Meta Description</label>
                                    <textarea rows="3" cols="70" class="form-control autosize_textarea" name="video_meta_description" id="video_meta_description" ><?php echo $video_meta_description; ?></textarea>

                                </div>

                                <div class="form_sep">
                                    <label for="reg_textarea_message">Video Tag</label>
                                    <div id="add_more_tag">
                                        <div id="more_tag">
                                            <input id="tags_1" type="text" class="tags" name="tag_name" value="<?php echo $video_tag; ?>" />
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="form_sep">
                                    <input type="hidden" name="video_id" id="video_id" value="<?php echo $video_id; ?>"/>
                                    <input type="submit" name="submit" id="submit" value="submit" class="btn btn-default"/>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <?php echo $this->load->view($theme . '/layout/common/sidebar', TRUE); ?>
    </section>
    <div id="footer_space"></div>

    <!-- Modal -->
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="text-align:center;" >
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
            <img src="#"/>
        </div>
        <!--<div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>-->
    </div>
</div>



<script type="text/javascript" src="<?php echo base_url() . getThemeName(); ?>/js/tag/jquery.tagsinput.js"></script>



<script type="text/javascript">
    $(document).ready(function(){
        $('#tags_1').tagsInput({width:'auto'});
    });

</script>
