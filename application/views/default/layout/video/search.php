<?php
$themurl = base_url() . getThemeName();
?>
<link href="<?php echo $themurl; ?>/css/owl.carousel.css" rel="stylesheet">
<link href="<?php echo $themurl; ?>/css/owl.theme.css" rel="stylesheet">

<script src="<?php echo $themurl; ?>/js/owl.carousel.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#owl-demo").owlCarousel({
            autoPlay: 3000,
            items : 6,
            itemsDesktop : [1199,2],
            itemsDesktopSmall : [979,2]
        });
	  
	  
        $('#category-list input').click(updatecate);
	   
        $("#refine-search").click(function(){
            $("#video-search").submit();
        });
		   
        $("#limit").change(function(){
            $("#video-search").submit();
        });
		   
		   

    });
	
    function updatecate() {
        var allVals = [];
        $('#category-list  :checked').each(function () {
            allVals.push($(this).val());
        });
          
        // alert(allVals);
        // allVals = allVals.replace(",", "-");
        //  allVals = allVals.replace(",","-")
        //  alert(allVals);
        var final_cat_str = allVals.join("-");
        if(final_cat_str == "") {final_cat_str = 0;}
        var str_keyword = $("#keyword").val();
        //alert(final_cat_sting);
		
        document.location.href = '<?php echo site_url("video/search/" . $limit) ?>/'+final_cat_str+'/'+str_keyword;
    }

</script>

<!-- *************************************************************************************** -->
<style>
    .row8{
        background: none !important;
    }
     #category-list li ul {
        padding-left: 15px;
    }
    
</style>
<div class="wrapper row8 page-padding-top">
    <!--
    <div class="container">
                 <div class="inner-header">
             <div class="row">
             <div class=" margin-bottom-20 col-md-6 col-sm-6 br-black "> 
               <div class="text-center margin-top-20"> 
                 <img src="<?php echo $themurl; ?>/images/inner_logo.png" alt="Logo" class="img-responsive" />
               </div>
                <div class="text-center head-social"	>
                     <ul class="social-link">
                         <li class="fb"> <a href="#"> &nbsp;</a></li>
                         <li class="tw"> <a href="#" > &nbsp;</a></li>
                         <li class="gm"> <a href="#" > &nbsp;</a></li>
                         <li class="in"> <a href="#"> &nbsp;</a></li>
                     </ul>
                 </div>
             
             </div>
             <div class="col-md-6 col-sm-6"> 
                 <div class="inner_top_link">
                         <ul>
                         <li> <a href="#"> <img src="<?php echo $themurl; ?>/images/learning_hive.png" alt="Learing HIVE" class="img-responsive" /> </a> </li>
                         <li> <a href="#"> <img src="<?php echo $themurl; ?>/images/design_hive.png" alt="Learing HIVE" class="img-responsive" /> </a> </li>
                         <li> <a href="#"> <img src="<?php echo $themurl; ?>/images/challenge_hive.png" alt="Learing HIVE"  class="img-responsive" /> </a> </li>
                     </ul>
                 </div>
                 <div class="newletter">
                         <div class="title">SIGN UP FOR iHIVE 3D NEWS </div>
                     <div class="title_sub">   COMMUNICATE, COLLABORATE ALL THINGS 3D </div>
                     <div> 
                         <input type="text" class="m-wrap medium pull-left" />
                         <a href="#"  class="btn yellow-btn"> Submit</a>
                     </div>
                 </div>
             
             </div>
             </div>
         </div>
     
    </div>
    --> 
</div>

<!-- *************************************************************************************** -->

<!-- *************************************************************************************** -->

<div class="wrapper row11">
    <div class="container">
        <div class="search-block"> 
            <div class="row">
                <form method="post" id="video-search" name="video-search" action="<?php echo site_url("video/search") ?>">   
                    <div class="col-md-3">
                        <div class="advsearch-block"> 
                            <div class="min-height">
                                 <h3 class="search-title"> CATEGORIES </h3>
<!--                                <h3 class="search-title"> Advanced Search </h3>
                                <h4 class="sub-title"> Reset Search </h4>-->
                                <div class="search-filter">
<!--                                    <h3 class="filter-title"> FRESHNESS</h3>-->
                                    <ul class="list-unstyled" id="category-list">
                                        <?php echo $search_category_menu; ?>
                                        <?php
                                        $category_selected_list = explode("-", $category_id);
                                        //print_r($category_selected_list);
                                        /*if ($category) {
                                            foreach ($category as $ca) {
                                                ?>
                                                <li><div class="radio">
                                                        <label> <input type="checkbox" name="category[]" id="category<?php echo $ca->category_id; ?>" value="<?php echo $ca->category_id; ?>" <?php if (in_array($ca->category_id, $category_selected_list, true)) { ?> checked="checked" <?php } ?>>  <?php echo $ca->category_name; ?> </label>
                                                    </div>
                                                </li>
                                            <?php
                                            }
                                        } */
                                        ?>



                                    </ul>
                                </div> 


                            </div>
                            <div class="search-filter">
<!--                                <h4 class="sub-title"> Do not include these words</h4>-->
                                <input type="text" class="form-control" placeholder="Text input" name="keyword" id="keyword" value="<?php if ($keyword != "0") {
                                            echo $keyword;
                                        } ?>">	
                                <button id="refine-search" type="button" class="btn yellow-btn">SEARCH BY KEYWORD</button>
                            </div>

                        </div>


                    </div>
                    <div class="col-md-9">
                        <div class="searchlist-block">
                            <div class="searchlist-top">
                                <div class="row">
                                    <div class="col-md-5 col-sm-5"> 
                                        <h3 class="search-title"> <!--Search Title Here--> </h3>
                                    </div>
                                    <div class="col-md-7 col-sm-7"> 
                                        <div class="form-horizontal col-lg-12" role="form">
                                            <div class="form-group">
                                                <label for="inputEmail3" class=" col-sm-9 control-label">Display Preferences :</label>
                                                <div class="col-sm-3 ">
                                                    <select class="form-control" name="limit" id="limit">
                                                        <option value="1" <?php if ($limit == "1") { ?> selected="selected" <?php } ?>>1</option>
                                                        <option value="2" <?php if ($limit == "2") { ?> selected="selected" <?php } ?>>2</option>
                                                        <option value="10" <?php if ($limit == "10") { ?> selected="selected" <?php } ?>>10</option>
                                                        <option value="20" <?php if ($limit == "20") { ?> selected="selected" <?php } ?>>20</option>
                                                        <option value="30" <?php if ($limit == "30") { ?> selected="selected" <?php } ?>>30</option>
                                                        <option value="40" <?php if ($limit == "40") { ?> selected="selected" <?php } ?>>40</option>
                                                        <option value="50" <?php if ($limit == "50") { ?> selected="selected" <?php } ?>>50</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <!--<div class="col-md-12"> 
                                                <ul>
                                                <li> page </li>
                                                <li> 1 </li>
                                                <li> of 450 </li>
                                                <li> <a href="#"> </a> </li>
                                            </ul>
                                        </div>-->
                                    </div> 
                                </div>
                            </div>
                            
                            <div class="searchlist-detail">
                             <?php
                                        /* $category_selected_list = explode("-", $category_id);
                                        //print_r($category_selected_list);
                                        if ($category) { ?>
                           
                                <div class="related-search">
                                   
                                    <ul class="list-unstyled inline" >
                                        <li> Related Searches   </li>
                                        <?php 
                                            foreach ($category as $ca) {
                                                if (in_array($ca->category_id, $category_selected_list, true)) {
                                                    echo '<li> <a href="javascript:void(0);"> ' . $ca->category_name . ' </a> </li>';
                                                }
                                            }
                                       
                                        ?>
                                    </ul>
                                    
                                </div>
<?php  } */?>
                                <div>
                                    <ul class="search-listing columns-4">

                                        <?php
                                        if ($result) {
                                            foreach ($result as $res) { ?>
                                                <li>
                                                    
                                                    <a href="<?php echo site_url('video/' . $res->video_slug); ?>">
                                                    
                                                    
                                                    
                                                <?php
                                                if ($res->video_type == 2) {

                                                    $get_video_url_image = $this->video_model->get_video_url_image($res->video_id, 2);

                                                    if ($get_video_url_image != '' && file_exists(base_path() . 'upload/video_thumb/' . $get_video_url_image)) {
                                                        ?>
                                                        <img src="<?php echo base_url(); ?>upload/video_thumb/<?php echo $get_video_url_image; ?>" alt="" class="img-responsive minheight20 imgcenter" /> 
                <?php } else { ?>
                                                        <img src="<?php echo base_url(); ?>upload/no_userimage.png" alt="" class="img-responsive minheight20 imgcenter"  /> 
                                                    <?php } ?>



                                                <?php
                                                } elseif ($res->video_type == 1) {

                                                    $get_video_image = $this->video_model->get_video_image($res->video_id, 1);

                                                    if ($get_video_image != '' && file_exists(base_path() . 'upload/video_thumb/' . $get_video_image)) {
                                                        ?>
                                                        <img src="<?php echo base_url(); ?>upload/video_thumb/<?php echo $get_video_image; ?>" alt="" class="img-responsive minheight20 imgcenter" /> 
                <?php } else { ?>
                                                        <img src="<?php echo base_url(); ?>upload/no_userimage.png" alt="" class="img-responsive minheight20 imgcenter"  /> 
                                                    <?php } ?>



            <?php } else { ?>
                                                    <img src="<?php echo base_url(); ?>upload/no_userimage.png" alt="" class="img-responsive minheight20 imgcenter"  />   
            <?php } ?>

                                                    
                                                    
                                                    
                                                     </a>
                                                    
                                                    
                                                    
                                                    <p><a href="<?php echo site_url('video/' . $res->video_slug); ?>"><?php echo substr($res->video_title, 0, 35); ?></a></p>
                                                    <h4> <strong><?php echo $res->video_point; ?> </strong> </h4>
                                                    <div class="rig-brd clearfix">
                                                        
                                                         <div class="pull-left"><?php if($res->profile_name!='' && $res->first_name!='') { ?>
                                                                <a class="lineH23" href="<?php echo site_url('bee/' . $res->profile_name); ?>"> <?php echo $res->first_name; ?> </a> <?php } ?>
                                        
                                                            </div>
                                                        <div class="pull-right">
                                                            <a href="#"> <i class="sprit-icon like "> </i> </a> 
                                                        </div> 
                                                    </div>
                                                </li>
    <?php
    }
} else {
    ?>
                                            <li>No Record(s)</li>
<?php } ?>                               

                                    </ul>
                                </div>

                                <ul class="">
                                        <?php echo $page_link; ?>
                                </ul>

                            </div>


                        </div>		  


                    </div>

                </form>


            </div>
        </div>

    </div>
</div> 


<!-- *************************************************************************************** -->