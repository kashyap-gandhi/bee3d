<script>

function frm_check_valid()
{
	
	var check=true;
		
	document.getElementById('err1').style.display='none';
	document.getElementById('err2').style.display='none';
	document.getElementById('err3').style.display='none';
	document.getElementById('err4').style.display='none';
			
	
	
	var video_type = document.getElementsByName("video_type");
     
	
	if(video_type=='')
	{
			
			check=false;
			var dv = document.getElementById('err1');
			dv.className = "error";
			dv.style.clear = "both";
			dv.style.minWidth = "110px";
			dv.style.margin = "5px";
			dv.innerHTML = 'Please select Video Type.';
			return false;
			
	}
	
	


		if(video_type==1)
		{
			
			
			///====for image==
			if(document.getElementById('prev_video_image').value == "")
			{
				if(document.getElementById('video_image').value == "")
				{
											
						check=false;
						var dv = document.getElementById('err2');
						dv.className = "error";
						dv.style.clear = "both";
						dv.style.minWidth = "110px";
						dv.style.margin = "5px";
						dv.innerHTML = 'Please upload video image.';
						return false;
					
				} else {
				
						value = document.getElementById('video_image').value;
						t1 = value.substring(value.lastIndexOf('.') + 1,value.length);
						
						if( t1=='jpg' || t1=='jpeg' || t1=='gif' || t1=='png' || t1=='JPG' || t1=='JPEG' || t1=='GIF' || t1=='PNG'){
					
							check=true;
							
						} else {
						
							check=false;
							
							var dv = document.getElementById('err2');
		
							dv.className = "error";
							dv.style.clear = "both";
							dv.style.minWidth = "110px";
							dv.style.margin = "5px";
							dv.innerHTML = 'Please upload valid image.';
							return false;		
						}
				 }

			}
			
		
		 
			//====for video=====
						
			if(document.getElementById('video_file').value == "")
			{
				check=false;
				i=0;												
				var dv = document.getElementById('err3');
				dv.className = "error";
				dv.style.clear = "both";
				dv.style.minWidth = "110px";
				dv.style.margin = "5px";
				dv.innerHTML = 'Please upload video.';
				return false;	
				
				
			} else {
				
				value = document.getElementById('video_file').value;
				t1 = value.substring(value.lastIndexOf('.') + 1,value.length);
				
				if( t1=='avi' || t1=='mp3' || t1=='flv' || t1=='mp4' || t1=='mov' || t1=='AVI' || t1=='MP3' || t1=='FLV' || t1=='MP4' || t1=='MOV'){
				
				check=true;
				
				} else {
				
					check=false;
																
					var dv = document.getElementById('err3');
					dv.className = "error";
					dv.style.clear = "both";
					dv.style.minWidth = "110px";
					dv.style.margin = "5px";
					dv.innerHTML = 'Please enter valid video.';
					return false;	
				}
			}
			
			////===end video
			
			} else if (video_type==2) {

				if(document.getElementById('video_url').value == "")
				{
					check=false;
															
					var dv = document.getElementById('err4');
					dv.className = "error";
					dv.style.clear = "both";
					dv.style.minWidth = "110px";
					dv.style.margin = "5px";
					dv.innerHTML = 'Video URL is required.';
					return false;	
					
				}else{
				
					check=true;
					
				}
	
		}
		
					
		
		if(check==true)
		{
			document.getElementById("frm_project").submit();
		} 
	 
	 
}