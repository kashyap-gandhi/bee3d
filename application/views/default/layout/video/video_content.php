
<!-- *************************************************************************************** -->
<div class="wrapper row2 page-padding-top">
    <div class="container">
		<div class="main-contain">
        	<h1 class="text-center dark-gery"><span class="font-light"> PICK A SUBJECT OF OUR  </span><a href="<?php echo site_url('video/search');?>" class="dark-gery">3D LEARNING VIDEO LIBRARY</a> </h1>
        	<h2 class="text-center dark-gery"> HAS THE AMAZING WORLD OF 3D PRINTING GOT YOU A LITTLE OVERWHELMED? <br/> WE CAN HELP!  SELECT A CATEGORY AND START LEARNING!  </h2>
        </div>	
    </div>
</div>
<!-- *************************************************************************************** -->
<style>
    .maxvideohw {
        width: 215px !important;
        height: 130px !important;
    }
    </style>
<div class="wrapper row3">
	<div class="container">
    	 <div class="video-block">
            	<div class="video-listing">
                	<?php if(!empty($popular_video)){ ?>
                    
                    <ul>
                        <?php foreach($popular_video as $pd){ ?>
                        
                    	                      
                        
                         <li>
                                            <a href="<?php echo site_url('video/' . $pd->video_slug); ?>" title="<?php echo $pd->video_title; ?>"> 



                                                <?php
                                                if ($pd->video_type == 2) {

                                                    $get_video_url_image = $this->video_model->get_video_url_image($pd->video_id, $pd->video_type);

                                                    if ($get_video_url_image != '' && file_exists(base_path() . 'upload/video_thumb/' . $get_video_url_image)) {
                                                        ?>
                                                        <img src="<?php echo base_url(); ?>upload/video_thumb/<?php echo $get_video_url_image; ?>" alt="" class="img-responsive maxvideohw imgcenter" /> 
                                                    <?php } else { ?>
                                                        <img src="<?php echo base_url(); ?>upload/no_userimage.png" alt="" class="img-responsive maxvideohw imgcenter" /> 
                                                    <?php } ?>



                                                    <?php
                                                } elseif ($pd->video_type == 1) {

                                                    $get_video_image = $this->video_model->get_video_image($pd->video_id, $pd->video_type);

                                                    if ($get_video_image != '' && file_exists(base_path() . 'upload/video_thumb/' . $get_video_image)) {
                                                        ?>
                                                        <img src="<?php echo base_url(); ?>upload/video_thumb/<?php echo $get_video_image; ?>" alt="" class="img-responsive maxvideohw imgcenter" /> 
                                                    <?php } else { ?>
                                                        <img src="<?php echo base_url(); ?>upload/no_userimage.png" alt="" class="img-responsive maxvideohw imgcenter" /> 
                                                    <?php } ?>



                                                <?php } else { ?>
                                                    <img src="<?php echo base_url(); ?>upload/no_userimage.png" alt="" class="img-responsive maxvideohw imgcenter" />   
                                                <?php } ?>




                                            </a>
                                        </li>
                        
                        
                       
                        <?php } ?>
                     </ul>
                    <?php } ?>
<!--                     <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                        <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                        <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                        <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                        <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                        <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                        <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                        <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                        <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>
                        <li> <a  href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/215x130.png" alt="215x130" /> </a> </li>-->
                    
                 </div>
            </div>
    </div>
</div> 

<!-- *************************************************************************************** -->
<div class="wrapper row2">
    <div class="container">
		<div class="main-contain margin-bottom-20">
        	<h1 class="text-center font-light dark-gery"> GOT A VIDEO TO SHARE?    </h1>
            <h4 class="text-center dark-gery"> 
			
			<a href="<?php if(get_authenticateUserID()!='') { echo site_url('video/add_me'); } else{ echo site_url('login'); } ?>" class="dark-gery"> <strong class="font-itcbold"> UPLOAD </strong> YOUR OWN VIDEO HERE! </a> </h4>
        </div>	
    </div>
</div>