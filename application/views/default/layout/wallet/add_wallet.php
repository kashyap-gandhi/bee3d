<script type="text/javascript">
	function numericFilter(txb) {	 
		 txb.value = txb.value.replace(/[^0-9\.]/g,'');
	}
</script>

<!-- parsley -->
		<!-- responsive table -->
<link href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>
		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Wallet</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
						
						<!-- main content -->
						<div class="row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Add Amount to Your Wallet
										<span class="span_right">
										<?php echo anchor('wallet/','Wallet History'.'('.$site_setting->currency_symbol.$total_wallet_amount.')','class="fpass"'); ?> &nbsp;|&nbsp;
									  
											<?php if($total_wallet_amount>$wallet_setting->wallet_minimum_amount) { ?>
											
												<?php echo anchor('wallet/withdraw_wallet','Withdraw','class="fpass"'); ?> &nbsp;|&nbsp;
												  <?php echo anchor('wallet/my_withdraw','Withdrawal History','class="fpass"'); ?> 
											 <?php } ?>
</span>
										</h4>
									</div>
									<?php if($error!='') { ?>
											<div class="alert alert-danger">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
													<?php  echo $error; ?>
												
											</div>
									 <?php  } ?>
									
									<div class="panel-body">
											<?php
											$attributes = array('name'=>'frmAddWallet','id'=>'frmAddWallet','class'=>'form_design');
											echo form_open('wallet/add_wallet',$attributes);
										?>
										<div id="detail-bg2" class="padTB10"><b style="color:#F00;">
										<?php 
												$var = sprintf("Note : </b><p>Transaction Fees <b class='colblack'>%s(&#37;)</b> is added on the Total Wallet or Transaction Amount. Minimum <b class='colblack'>%s</b> is required.</p>",$wallet_setting->wallet_add_fees,$site_setting->currency_symbol.$wallet_setting->wallet_minimum_amount); echo $var;
											?>
										</div>
										
											<div class="form_sep">
												<label for="reg_input_name" class="req">Add Amount (<?php echo $site_setting->currency_symbol;?>)</label>
												<input type="text" value="<?php echo $credit ;?>" id="credit" name="credit" class="form-control" onKeyUp="numericFilter(this);">
											</div>
											
											<div class="radio">
											<label>
												 <?php	
											
												if($payment)
												{
													$i=0;
													foreach($payment as $row)
													{
													
														if($row->name != 'By Cash') {
														$check='';
														//var_dump($payment);exit;
														 if($gateway_type==$row->id)$check='checked=checked';
														else if($i==0)$check='checked=checked';
															?>
										<input type="radio" name="gateway_type" id="gateway_type" value="<?php echo $row->id; ?>" <?php echo $check;?> /><?php echo $row->name; ?>
													<?php }
													}
												}
												?>    
											</label>
											</div>
											
											<div class="form_sep">
											<input type="submit" name="submit" id="submit" value="submit" class="btn btn-default"/>
											</div>	
										</form>
									</div>
								</div>
							</div>
							
						</div>
						
					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
		</div>


