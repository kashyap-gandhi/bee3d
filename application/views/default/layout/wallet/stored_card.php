<script type="text/javascript">
jQuery(document).ready(function() {	
	
	jQuery("#learnmore").fancybox({
				'titlePosition'		: 'inside',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			});
	
  
});

function filldata()
{
	document.getElementById("card_address").value = "";
	document.getElementById("card_city").value = "";
	document.getElementById("card_state").value = "";
	document.getElementById("card_zipcode").value = "";
	document.getElementById("sub_step2").value = "Save";
}
  
  
  
function filldata_ready(val){
  
  var id = val;
  var url = "<?php echo site_url('stored_card/get_location'); ?>"+'/'+id; // the script where you handle the form input.
	$.ajax({
	   type: "POST",
	   url: url,
	   dataType:'json',
	   data: {user_location_id: id}, 
	   success: function(data)
	   {
			document.getElementById("card_address").value = data.card_address;
			document.getElementById("card_state").value = data.card_state;
			document.getElementById("card_zipcode").value = data.card_zipcode;
			document.getElementById("card_city").value = data.card_city;
			document.getElementById("sub_step2").value = "Update";
	   }
	 });
					 
   }
   
   



</script>
<div class="main">
<div class="incon">
<?php
$site_setting=site_setting();
$data['site_setting']=$site_setting;

  ?>
<?php
	$attributes = array('name'=>'frmCardInfo','id'=>'frmCardInfo');
	echo form_open('stored_card/',$attributes);
?>

    	<div class="mconleft">

<?php if($error != ''){ 

		if($error=='fail') { ?> 
			<div class="errmsgcl"> 
				<div class="follfi">
				<?php echo $this->lang->line('wallet.storedcard_msg5');?>
				</div>
			</div>
		<?php } elseif($error=='update') { ?>


				<div class="errmsgcl"> 
					<div class="follfi"><?php echo $this->lang->line('wallet.storedcard_msg2');?></div>
				</div> 

	   <?php }else{?>
				<div class="errmsgcl"> 
				<div class="follfi"><?php echo $this->lang->line('wallet.storedcard_msg');?></div>
				<?php echo $error; ?>
				</div> 
	   <?php }}?>

      
           
           <div id="s1post"><?php echo $this->lang->line('wallet.storedcard_setup');?></div>
        
<div class="tabs2">
             

            <div class="borrdercol">
            	<h1 id="bilt"><?php echo $this->lang->line('wallet.your_billing_info');?></h1>
            <h3 class="crec fs14"><?php echo $this->lang->line('wallet.credit_card');?> <span id="req" class="crec1" ><?php echo $this->lang->line('wallet.credit_card_msg');?>
            <a href="#learnmoreinfo" id="learnmore"><?php echo $this->lang->line('wallet.storedcard_learnmore');?></a></span> </h3>
			
						
						
<div style="display: none;">
		<div id="learnmoreinfo" style="width:500px;height:140px;overflow:auto;">
        <div class="clear"></div>
			<div class="fl padTB3 marL5"><h2><?php echo $this->lang->line('wallet.storedcard_whycc');?></h2></div>
			
            
        
            <div class="clear"></div>


 <ol class="ordlist fs11 LH18">
	<li><?php 
			$var = sprintf($this->lang->line('wallet.storedcard_msg3'),$site_setting->site_name,$site_setting->site_name,$site_setting->site_name); echo $var;
	?></li>
    
    <li><?php
		$var2 = sprintf($this->lang->line('wallet.storedcard_msg4'),$site_setting->site_name); echo $var2; 
	?></li>
 </ol>
		</div>
	</div>
    
    
<div class="wrap4"></div>             
                
                
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td><h4 class="fs13"><?php echo $this->lang->line('wallet.storedcard_fname');?></h4></td>
                <td><h4 class="fs13"><?php echo $this->lang->line('wallet.storedcard_lname');?></h4></td>
              </tr>
              <tr>
                <td><input name="card_first_name" id="card_first_name" type="text" class="ntext" value="<?php echo $card_first_name;?>" /></td>
                <td><input name="card_last_name" id="card_last_name" type="text"  class="ntext" value="<?php echo $card_last_name;?>"  /></td>
              </tr>

              <tr>
                <td><h4 class="fs13"><?php echo $this->lang->line('wallet.storedcard_cardnum');?></h4></td>
                <td><h4 class="fs13"><?php echo $this->lang->line('wallet.storedcard_cardtype');?> <span class="marL57">
				<?php echo $this->lang->line('wallet.storedcard_exdate');?></span></h4></td>
              </tr>
              <tr>
                <td><input name="cardnumber" id="cardnumber" type="text" value="<?php echo $cardnumber; ?>" class="ntext"  size="19" maxlength="19"  /></td>
                <td>
                    <select name="cardtype" id="cardtype" class="wid120 fs11"  onChange="javascript:generateCC(); return false;">
						<option value='Visa' <?php if($cardtype=='Visa') { ?> selected <?php } ?>><?php echo $this->lang->line('wallet.visa');?></option>
                        <option value='MasterCard'  <?php if($cardtype=='MasterCard') { ?> selected <?php } ?>><?php echo $this->lang->line('wallet.master_card');?></option>
                        <option value='Discover'  <?php if($cardtype=='Discover') { ?> selected <?php } ?>><?php echo $this->lang->line('wallet.discover');?></option>
                        <option value='Amex'  <?php if($cardtype=='Amex') { ?> selected <?php } ?>><?php echo $this->lang->line('wallet.american_express');?></option>
                    </select>
                    
                    <select name="card_expiration_month" id="card_expiration_month" class="fs11">
						<option value="1" <?php if($card_expiration_month==1) { ?> selected <?php } ?>>1</option>
						<option value="2"  <?php if($card_expiration_month==2) { ?> selected <?php } ?>>2</option>
						<option value="3"  <?php if($card_expiration_month==3) { ?> selected <?php } ?>>3</option>
						<option value="4"  <?php if($card_expiration_month==4) { ?> selected <?php } ?>>4</option>
						<option value="5"  <?php if($card_expiration_month==5) { ?> selected <?php } ?>>5</option>
						<option value="6"  <?php if($card_expiration_month==6) { ?> selected <?php } ?>>6</option>
						<option value="7"  <?php if($card_expiration_month==7) { ?> selected <?php } ?>>7</option>
						<option value="8"  <?php if($card_expiration_month==8) { ?> selected <?php } ?>>8</option>
						<option value="9"  <?php if($card_expiration_month==9) { ?> selected <?php } ?>>9</option>
						<option value="10"  <?php if($card_expiration_month==10) { ?> selected <?php } ?>>10</option>
						<option value="11"  <?php if($card_expiration_month==11) { ?> selected <?php } ?>>11</option>
						<option value="12"  <?php if($card_expiration_month==12) { ?> selected <?php } ?>>12</option>
                    </select>
                    
                    <select name="card_expiration_year" id="card_expiration_year" class="fs11">
						<?php for($i=date('Y');$i<=date('Y')+7;$i++) 
						{ ?>
                                              
                        <option value="<?php echo $i;?>" <?php if($card_expiration_year==$i) { ?> selected <?php } ?>><?php echo $i;?></option>
						<?php } ?>
                    </select>
				</td>
              </tr>
				<tr>
                 <td colspan="2"><h4 class="fs13"><?php echo $this->lang->line('temp.card_verification_number');?></h4></td> 
              </tr>
              <tr>
                <td colspan="2"><input name="cvv2Number" id="cvv2Number" type="password" class="ntext" size="3" maxlength="3" value="<?php echo $cvv2Number;?>" /></td> 
              </tr>
            </table>
 
			<h3 class="crec fs14"><?php echo $this->lang->line('wallet.storedcard_billadd');?> <span id="req" class="crec1" >
			<?php echo $this->lang->line('wallet.storedcard_require');?></span> </h3>


<table width="100%" border="0" cellspacing="1" cellpadding="1">
<?php if($user_location) { ?>

  
  <?php foreach($user_location as $location) { ?>
  <tr>
    <td width="20"><input type="radio" name="user_location_id" value="<?php echo $location->user_location_id; ?>" id="user_location_id" <?php if(($location->is_home==1) || $user_location_id==$location->user_location_id ) { ?>checked="checked" <?php } ?> onclick="return filldata_ready(this.value)" /></td>
                <td><label><?php echo ucfirst($location->location_name);?></label></td>
               </tr>
                <tr>
                <td>&nbsp;</td>
                <td><span>(<?php if($location->location_address!='') { echo $location->location_address.','; }

   if($location->location_city!='') { echo $location->location_city.','; }
   
    if($location->location_state!='') { echo $location->location_state.','; }
	
	 if($location->location_zipcode!='') { echo $location->location_zipcode; }
   
   ?>)</span></td>                
  </tr>
 
 
 
 <?php } ?>

<?php } ?>

 <tr><td colspan="2" height="10">&nbsp;</td></tr>
 
 
 	<tr><td> <input type="radio" name="user_location_id" id="user_location_id" value="other" <?php if($user_location_id=='other' || $card_address!='') {  echo 'checked'; } else { if(!$user_location) {  echo 'checked'; } } ?> onclick="return filldata()" /></td>
                <td><label><?php echo $this->lang->line('wallet.storedcard_fillout');?></label></td>
                </tr>
                
                
</table>



<table width="100%" border="0" cellspacing="1" cellpadding="3">



  <tr>
    <td><h4 class="fs13"><?php echo $this->lang->line('wallet.storedcard_address');?></h4></td>
    <td></td>
    <td></td>
   </tr>
  <tr>
    <td><input name="card_address" id="card_address" type="text"  class="ntext" value="<?php echo $card_address; ?>"  /></td>
    <td></td>
    <td></td>
   </tr>
   
  <tr>
    <td><h4 class="fs13"><?php echo $this->lang->line('wallet.storedcard_city');?></h4></td>
    <td><h4 class="fs13"><?php echo $this->lang->line('wallet.storedcard_state');?></h4></td>
    <td><h4 class="fs13"><?php echo $this->lang->line('wallet.storedcard_postalcode');?></h4></td>
   </tr>
  <tr>
    <td><input name="card_city"  id="card_city" type="text"  class="ntext" value="<?php echo $card_city; ?>" /></td>
    <td><input name="card_state" id="card_state" type="text"   size="15" value="<?php echo $card_state;?>" /></td>
    <td><input name="card_zipcode" id="card_zipcode" type="text"  size="15"  value="<?php echo $card_zipcode; ?>" /></td>
   </tr>
   
   <?php if($card_verify_status==0 || $card_verify_status=='') { ?>
    <tr>
  
 
    <td align="left" valign="top" colspan="3"><h4><input name="save_location" type="checkbox" value="1" id="save_location"  <?php if($save_location==1) { ?> checked <?php } ?>/> <?php echo $this->lang->line('temp.store_this_location');?></h4></td>
  </tr>
  <?php  } ?>
   
</table>   
<div class="clear"></div> <br>
<?php /*?><?php if() { ?>

 <input type="submit" value="<?php echo $this->lang->line('temp.store');?>" class="submbg2 fl" name="sub_step2">
 <?php } else {<?php */?>
 
 <input type="submit" value="<?php echo $this->lang->line('temp.update');?>" class="submbg2 fl" name="sub_step2" id="sub_step2">
 <?php //} ?>
              <div class="clear"></div>     
            </div>
              	
            
			
             
   
              






			</div>                
		</div>
         <?php 
		
		 echo $this->load->view($theme.'/layout/user/user_sidebar',$data); ?>  
        <div class="clear"></div>

</form>

    </div>
</div>



</section>

<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>  
<script> 
	var options = {		
	types: ['(cities)']
	//componentRestrictions: {country: 'us'}
	};
	
	var card_address = new google.maps.places.Autocomplete($("#card_address")[0], options);
	google.maps.event.addListener(card_address, 'place_changed', function() {
		var place_start = card_address.getPlace();
		console.log(card_address.address_components);
	}); 
</script>