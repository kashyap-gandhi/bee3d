<!-- parsley -->
<script type="text/javascript">
function change_div_method(str)
{
	if(str=='bank')
	{
		document.getElementById('bank_div').style.display='block';
		document.getElementById('check_div').style.display='none';
		document.getElementById('gateway_div').style.display='none';
	}
	if(str=='check')
	{
		document.getElementById('check_div').style.display='block';
		document.getElementById('bank_div').style.display='none';
		document.getElementById('gateway_div').style.display='none';
	}
	if(str=='gateway')
	{
		document.getElementById('gateway_div').style.display='block';
		document.getElementById('bank_div').style.display='none';
		document.getElementById('check_div').style.display='none';
	}
	
}
</script>

<script>
function numericFilter(txb) {	 
	 txb.value = txb.value.replace(/[^0-9\.]/g,'');
}
</script>

		<!-- responsive table -->
<link href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>
		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Wallet</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
						
						<!-- main content -->
						<div class="row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Wallet (<?php echo $total_wallet_amount;	?>)										<span>Authorize Balance :( <?php echo getuserpoints_auth(get_authenticateUserID());	?>)</span>
										<span class="span_right">
										<?php echo anchor('wallet/','Wallet History'.'('.$total_wallet_amount.')','class="fpass"'); ?> &nbsp;|&nbsp;
									  
											<?php if($total_wallet_amount>$wallet_setting->wallet_minimum_amount) { ?>
											
												<?php echo anchor('wallet/withdraw_wallet','Withdraw','class="fpass"'); ?> 											 <?php } ?>
										</span>
										</h4>
									</div>
									<?php 
									if($error != '') { ?>
											<div class="alert alert-danger">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
													<?php  echo $error; ?>
												
											</div>
									 <?php  } ?>
									
									<div class="panel-body">
											 <?php
												$attributes = array('name'=>'frm_withdrawwallet','id'=>'frm_withdrawwallet','class'=>'form_design');
												echo form_open_multipart('wallet/withdraw_wallet',$attributes);
											
											?>
	<?php 
		$var = sprintf("<b style='color:#F00;'>Note : </b> Administrator Transaction Fees <b class='colblack'> %s (&#37;)</b> is added on the Total Withdrawal Amount. Also Transaction Charges cut               							from the withdrawal Points .Minimum <b class='colblack'> %s </b> is required.",$wallet_setting->wallet_donation_fees,$wallet_setting->wallet_minimum_amount);
			//echo $var;
                        
                        
                        
                        
	?>
                                                                            <b style="color:#F00;">Note : </b> Administrator Transaction Fees <b class="colblack"> <?php echo $wallet_setting->wallet_donation_fees; ?> (%)</b> is added on the Total Withdrawal amount also Transaction Charges cut from the withdrawal Points. Minimum <b class="colblack"> <?php echo $wallet_setting->wallet_minimum_amount; ?> </b> withdraw points is required.	
											<br/><br/><br/>
											<div class="form_sep">
												<label for="reg_input_name">Enter withdraw Points</label>
												<input type="text" value="<?php echo $amount; ?>" id="amount" name="amount" class="form-control" onKeyUp="numericFilter(this);">
											</div>
											
											<div class="form_sep">
												<label for="reg_input_email">Withdraw Method</label>
												<div class="radio">
												<label>
												 <input type="radio" name="withdraw_method" onclick="change_div_method(this.value)" value="bank"  <?php if($withdraw_method=='bank') { ?> checked="checked" <?php } ?> id="withdraw_method" class="" /><?php echo 'By Net Banking';?>
												</label>
											</div>
												<div class="radio">
												<label>
												 <input type="radio" name="withdraw_method" onclick="change_div_method(this.value)" value="check" <?php if($withdraw_method=='check') { ?> checked="checked" <?php } ?> id="withdraw_method" /><?php echo 'By Cheque';?>
												</label>
											</div>
												<div class="radio">
											<label>
												  <input type="radio" name="withdraw_method" onclick="change_div_method(this.value)" value="gateway" id="withdraw_method" <?php if($withdraw_method=='gateway') { ?> checked="checked" <?php } ?>  /><?php echo 'By Payment Gateway';?></label>
											</div>
											</div>
											
								
											
											
<div style="display: <?php if($withdraw_method=='bank') { echo "block"; } else { echo "none"; } ?>;" id="bank_div">

<div id="detail-bg1" class="title2"><?php echo 'Bank Detail'?></div>

<table width="100%" cellspacing="4" cellpadding="4" border="0">

<tbody><tr>
<td width="20%" valign="middle" align="left" class="lab1"><?php echo 'Bank Name';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td width="80%" valign="top" align="left"><input type="text" value="<?php echo $bank_name; ?>" id="bank_name" name="bank_name" class="form-control"></td>
</tr>


<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Account Holder Name';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $bank_account_holder_name; ?>" id="bank_account_holder_name" name="bank_account_holder_name" class="form-control"></td>
</tr>





<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Bank Account Number'?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $bank_account_number; ?>" id="bank_account_number" name="bank_account_number" class="form-control"></td>
</tr>




<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Bank Branch'?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $bank_branch; ?>" id="bank_branch" name="bank_branch" class="form-control"></td>
</tr>



<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Bank IFSC Code';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $bank_ifsc_code; ?>" id="bank_ifsc_code" name="bank_ifsc_code" class="form-control"></td>
</tr>



<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Bank Address';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $bank_address; ?>" id="bank_address" name="bank_address" class="form-control"></td>
</tr>




<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Bank City';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $bank_city; ?>" id="bank_city" name="bank_city" class="form-control"></td>
</tr>




<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Bank State';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $bank_state; ?>" id="bank_state" name="bank_state" class="form-control"></td>
</tr>


<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Bank Country ';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $bank_country; ?>" id="bank_country" name="bank_country" class="form-control"></td>
</tr>


<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Bank Postal Code';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $bank_zipcode; ?>" id="bank_zipcode" name="bank_zipcode" class="form-control"></td>
</tr>

</tbody></table>

</div>
<div style="display: <?php if($withdraw_method=='check') { echo "block"; } else { echo "none"; } ?>;" id="check_div">
<div  id="detail-bg1" class="title2"><?php echo 'Cheque Bank Detail';?></div>
<table width="100%" cellspacing="4" cellpadding="4" border="0">

<tbody>

<tr>
<td width="20%" align="left" valign="middle" class="lab1"><?php echo 'Bank Name';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td width="80%" align="left" valign="top"><input type="text" value="<?php echo $check_name; ?>" id="check_name" name="check_name" class="form-control"></td>
</tr>


<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Account Holder Name';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $check_account_holder_name; ?>" id="check_account_holder_name" name="check_account_holder_name" class="form-control"></td>
</tr>


<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Bank Account Number';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $check_account_number; ?>" id="check_account_number" name="check_account_number" class="form-control"></td>
</tr>


<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Bank Branch';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $check_branch; ?>" id="check_branch" name="check_branch" class="form-control"></td>
</tr>



<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Bank IFSC Code';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $check_unique_id; ?>" id="check_unique_id" name="check_unique_id" class="form-control"></td>
</tr>


<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Bank Address';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $check_address; ?>" id="check_address" name="check_address" class="form-control"></td>
</tr>






<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Bank City';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $check_city; ?>" id="check_city" name="check_city" class="form-control"></td>
</tr>


<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Bank State';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $check_state; ?>" id="check_state" name="check_state" class="form-control"></td>
</tr>


<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Bank Country';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $check_country; ?>" id="check_country" name="check_country" class="form-control"></td>
</tr>


<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Bank Postal Code';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $check_zipcode; ?>" id="check_zipcode" name="check_zipcode" class="form-control"></td>
</tr>

</tbody></table>
</div>
<div style="display: <?php if($withdraw_method=='gateway') { echo "block"; } else { echo "none"; } ?>;" id="gateway_div">
<div id="detail-bg1" class="title2"><?php echo 'Payment Gateway Detail';?></div>
<table width="100%" cellspacing="4" cellpadding="4" border="0">

<tbody><tr>
<td width="20%"  align="left" valign="middle" class="lab1"><?php echo 'Gateway Name';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td width="80%"  align="left" valign="top"><input type="text" value="<?php echo $gateway_name; ?>" id="gateway_name" name="gateway_name" class="form-control"></td>
</tr>


<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Gateway Account';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $gateway_account; ?>" id="gateway_account" name="gateway_account" class="form-control"></td>
</tr>



<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Gateway City';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $gateway_city; ?>" id="gateway_city" name="gateway_city" class="form-control"></td>
</tr>


<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Gateway State';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $gateway_state; ?>" id="gateway_state" name="gateway_state" class="form-control"></td>
</tr>


<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Gateway Country';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $gateway_country; ?>" id="gateway_country" name="gateway_country" class="form-control"></td>
</tr>


<tr>
<td valign="middle" align="left" class="lab1"><?php echo 'Gateway Postal Code';?><span style="color:#F00; font-weight:bold;">&nbsp;*</span></td>

<td valign="top" align="left"><input type="text" value="<?php echo $gateway_zip; ?>" id="gateway_zip" name="gateway_zip" class="form-control"></td>
</tr>

</tbody></table>

</div>

											
											
<div class="form_sep">
											<input type="hidden" name="withdraw_id" id="withdraw_id" value="<?php echo $withdraw_id; ?>" />
											<input type="submit" name="submit" id="submit" value="submit" class="btn btn-default"/>
											</div>	
										</form>
									</div>

								</div>
							</div>
							
						</div>
						
					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
		</div>
<div id="footer_space"></div>

