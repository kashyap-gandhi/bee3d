<link href="<?php echo base_url().getThemeName(); ?>/css/screen.css" rel="stylesheet" type="text/css" media="screen" /> 
<div id="bidcontener">

  
  <!--start_#bidcontener-->
  <div class="bid_form_center">
  
   	   <?php if($msg != '') {
			  if($msg =='success') {?>     
				 		 <div id="error" class="msgsuccess" style="margin-top:10px;margin-bottom:10px;"><?php  echo "Conversation message posted successfully"; ?></div>
                         <div class="clear"></div>
	   <?php } if($msg =='accept') {?>     
				 		 <div id="error" class="msgsuccess" style="margin-top:10px;margin-bottom:10px;"><?php  echo "Offer accepted successfully"; ?></div>
                         <div class="clear"></div>
	   <?php } } ?>
	   
	   <?php if($error!='') { ?>
			<div class="errmsgcl" id="error" style="margin-top:10px;">
				<?php  echo $error; ?>
			</div>
	   <?php } ?>
	   
	   
    <div class="left_side">
      <div class="center_top">
	  
	  
	  	<div class="trip_requirements">
			<h1>Conversation with:
			 <?php echo anchor('user/'.$offer_user_info->profile_name,ucfirst($offer_user_info->first_name).' '.ucfirst(substr($offer_user_info->last_name,0,1)),' class="dhan" '); ?>
			</h1>
		</div>
						
						
        <div class="center_left">
          <h1>Trip: <?php echo anchor('from/'.$trip_info->trip_from_place.'/to/'.$trip_info->trip_to_place.'/'.$trip_info->trip_id,$trip_info->trip_from_place.' &gt '.$trip_info->trip_to_place);?></h1>
         
         
      
<p>posted <span>about <?php echo getDuration($trip_info->trip_added_date);?></span> by <?php echo anchor('user/'.$user_info->profile_name,$user_info->profile_name); ?></p>
        </div>
       
        <div class="center_right">
          <div class="photo" style="margin:10px 31px 0 0;"> 
          <?php 
          	 	$user_image= base_url().'upload/no_image.png';
				if($user_info->profile_image!='') {  
			
					if(file_exists(base_path().'upload/user_thumb/'.$user_info->profile_image)) {
				
						$user_image=base_url().'upload/user_thumb/'.$user_info->profile_image;
						
					}
					
				}
			?>
            <?php echo anchor('user/'.$user_info->profile_name,'<img src="'.$user_image.'" alt="" width="65" height="67"  />'); ?>
           
            
        </div>
        </div>
         <div class="clear"></div>
         
      </div>
      <div class="trip_requirements"> <img src="<?php echo base_url().getThemeName(); ?>/images/pen.png" alt="" class="img" />
        <h1>Trip Requirements</h1>
      </div>
      <div class="center_middle">
        <div class="middle_left">
          <div class="packages_box">
            <div class="packages_box_center">
              <div class="packages_text">
                <p>Packages &nbsp;&nbsp;:&nbsp;
                	<span>
                    	<?php 
						$package_name='';
						if($trip_info->package_type == 0){
							$package_name = 'All';
						}
						else if($trip_info->package_type == 1)
						{
							$package_name = 'Family';
						}
						else if($trip_info->package_type == 2)
						{
							$package_name = 'Group';
						}
						else if($trip_info->package_type == 3)
						{
							$package_name = 'Honeymoon';
						}
						else if($trip_info->package_type == 4)
						{
							$package_name = 'Weekend';
						}
						else
						{
							$package_name = 'Start';
							
						}
						?>
                		<?php echo $package_name.' Package';?>
                    </span>
                </p>
                <p>Start Trip&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;<span><?php echo $trip_info->trip_from_place;?></span></p>
                <p>End Trip&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<span><?php echo $trip_info->trip_to_place;?></span></p>
                <p>Trip Month&nbsp;&nbsp;:&nbsp;
                	<span><?php
					$month_name='';
					if($trip_info->trip_month == 1)
					{
						$month_name ='January';
					}
					else if($trip_info->trip_month == 2)
					{
						$month_name ='February';
					}
					else if($trip_info->trip_month == 3)
					{
						$month_name ='March';
					}
					else if($trip_info->trip_month == 4)
					{
						$month_name ='April';
					}
					else if($trip_info->trip_month == 5)
					{
						$month_name ='May';
					}
					else if($trip_info->trip_month == 6)
					{
						$month_name ='June';
					}
					else if($trip_info->trip_month == 7)
					{
						$month_name ='July';
					}
					else if($trip_info->trip_month == 8)
					{
						$month_name ='August';
					}
					else if($trip_info->trip_month == 9)
					{
						$month_name ='September';
					}
					else if($trip_info->trip_month == 10)
					{
						$month_name ='October';
					}
					else if($trip_info->trip_month == 11)
					{
						$month_name ='November';
					}
					else
					{
						$month_name ='December';
					}
					
					 
					?> 
                   <?php echo $month_name.' '.$trip_info->trip_month;?>
                    </span>
                  </p>
                <p>Duration&nbsp;&nbsp;:&nbsp;<span><?php echo $trip_info->trip_days;?>-<?php echo $trip_info->trip_nights;?> Nights/Day</span></p>
              </div>
            </div>
          </div>
          <div class="packages_box">
            <div class="packages_box_top"></div>
            <div class="packages_box_center">
              <div class="packages_text">
                <span >Rooms</span>&nbsp;&nbsp;&nbsp;&nbsp;
                <span>Adults</span>&nbsp;&nbsp;&nbsp;&nbsp;
                <span>Children</span> 
                
                <span>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $trip_info->total_room;?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span><?php echo $trip_info->total_adult;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;
                <span><?php echo $trip_info->total_child;?></span> </div>
            </div>
            <div class="packages_box_bottom"></div>
          </div>
          <div class="packages_box">
            <div class="packages_box_top"></div>
            <div class="packages_box_center">
              <div class="packages_text">
                <p>Budget : <strong><?php echo get_currency_symbol($trip_info->currency_code_id);?><?php echo $trip_info->trip_min_budget;?> - <?php echo get_currency_symbol($trip_info->currency_code_id);?><?php echo $trip_info->trip_max_budget;?></strong></p>
              </div>
            </div>
            <div class="packages_box_bottom"> </div>
          </div>
        </div>
        <div class="middle_right">
        	<?php if($trip_info->trip_special_comment != '')
			{?>
              <div class="special_comments">
                <h1>Special Comments</h1>
              </div>
              <div class="view_more_prw"> <span><?php echo  $trip_info->trip_special_comment;?></span>
              </div>
               <?php }?>
          <div class="trip_places">
            <h1>Trip Places</h1>
          </div>
          <div class="trp_plc">
            <div style="padding:10px;"> 
            	<?php 
				$place = explode(',',$trip_info->trip_destination_places);
				if($place)
				{
				foreach($place as $pl){
				?>
                	<span><?php echo $pl;?></span>
				<?php }}
				?>
              </div>
           <!-- <div class="viewmore_fnr"> <a href="#"><b class="create"></b> view more...</a> </div>-->
          </div>
          
        
         
          
          <div class="trip_must_include">
            <h1>Trip must include</h1>
          </div>
          
          <?php 
	   $atts = array(
		  'width'      => '500',
		  'height'     => '500',
		  'scrollbars' => 'yes',
		  'status'     => 'yes',
		  'resizable'  => 'yes',
		  'screenx'    => '0',
		  'screeny'    => '0'
		);
		  	if($trip_info->trip_build_package == 1)
			{
		  ?>
          	<div class="icons">
                                    <div class="flight">
                                        <img src="<?php echo base_url().getThemeName(); ?>/images/plane.png" alt="" class="img" />
                                        <a href="#">Flight</a>
                                    </div>
                                    <div class="hotel">
                                        <img src="<?php echo base_url().getThemeName(); ?>/images/hotel.png" alt="" class="img" />
                                        <a href="#">Hotel</a>
                                    </div>
                                    <div class="car">
                                        <img src="<?php echo base_url().getThemeName(); ?>/images/car.png" alt="" class="img" /><br />
                                        <a href="#">Car</a>
                                    </div>
                                    <div class="map">
                                         <?php 
										
										 echo anchor_popup('trip/map/'.$trip_info->trip_id,'<span>Trip Map</span>', $atts); ?>
                                         <br />
                                       <?php  echo anchor_popup('trip/map/'.$trip_info->trip_id,' <img src="'.base_url().getThemeName().'/images/map.png" alt="" class="img" />', $atts); ?>
                                       <br />
                                    </div>
                                </div>
          <?php  }else if($trip_info->trip_build_package == 2){?>
          
         	 <div class="icons">
                                    <div class="flight">
                                        <img src="<?php echo base_url().getThemeName(); ?>/images/plane.png" alt="" class="img" />
                                        <a href="#">Flight</a>
                                    </div>
                                    <div class="hotel">
                                        <img src="<?php echo base_url().getThemeName(); ?>/images/hotel.png" alt="" class="img" />
                                        <a href="#">Hotel</a>
                                    </div>
                                    <div class="map" style="float:none;">
                                         <?php 
										
										 echo anchor_popup('trip/map/'.$trip_info->trip_id,'<span>Trip Map</span>', $atts); ?>
                                         <br />
                                     <?php  echo anchor_popup('trip/map/'.$trip_info->trip_id,' <img src="'.base_url().getThemeName().'/images/map.png" alt="" class="img" />', $atts); ?>
                                       <br />
                                    </div>
                                </div>
          <?php }else{?>
         	<div class="icons">
                                    <div class="flight">
                                        <img src="<?php echo base_url().getThemeName(); ?>/images/plane.png" alt="" class="img" />
                                        <a href="#">Flight</a>
                                    </div>
                                    <div class="car">
                                        <img src="<?php echo base_url().getThemeName(); ?>/images/car.png" alt="" class="img" /><br />
                                        <a href="#">Car</a>
                                    </div>
                                    <div class="map" style="float:none;">
                                         <?php 
										
										 echo anchor_popup('trip/map/'.$trip_info->trip_id,'<span>Trip Map</span>', $atts); ?>
                                         <br />
                                      
                                       
                                       <?php  echo anchor_popup('trip/map/'.$trip_info->trip_id,' <img src="'.base_url().getThemeName().'/images/map.png" alt="" class="img" />', $atts); ?>
                                       
                                       <br />
                                    </div>
                                </div>
         <?php } ?>
        </div>
        <div class="clear"></div>
         
        <div class="conservations">
          <h2>Confirm Egg Trip by : </h2>
          <span>&nbsp;&nbsp;&nbsp;<?php echo date($site_setting->date_format,strtotime($trip_info->trip_confirm_date));?></span><br />
          <h2>Complete Trip by : </h2>
          <span>&nbsp;&nbsp;&nbsp;<?php echo date($site_setting->date_format,strtotime($trip_info->trip_must_complete_date));?></span> </div>
      </div>
      
      
	  	<div class="trip_requirements">
			<img src="<?php echo base_url().getThemeName(); ?>/images/ques.png" alt="" class="img" />
			<h1>Conservations</h1>
		</div>
		<div class="conserva">
		
		 <!--Conversation Start-->
    
         <?php if($trip_conversation){ foreach($trip_conversation as $trc){
	  
			 
			 $post_user_image= base_url().'upload/no_image.png';					
				if($trc->profile_image!='') {  
				if(file_exists(base_path().'upload/user_thumb/'.$trc->profile_image)) {
					$post_user_image=base_url().'upload/user_thumb/'.$trc->profile_image;
				}
			}
			
			$post_user_info = check_user_agent($trc->post_by_user_id);
		?>
      
        <div class="conservations"><?php echo anchor('user/'.$trc->profile_name,'<img src="'. $post_user_image.'" alt="" class="img" />');?>
		
		  <?php /*?><?php if($post_user_info) {  ?>
          <div class="orenge_box conver_ie">
            <h6><?php echo $post_user_info->agent_level; ?></h6>
          </div>
		  <?php } ?><?php */?>
		  
          <h1><?php echo anchor('user/'.$trc->profile_name,$trc->first_name.' '.$trc->last_name);?></h1>
          <p><?php echo $trc->conversation_message;?></p>
		   <p><?php echo getDuration($trc->conversation_date);?></p>
        </div>
        <div class="clear"></div>
        	
     
         <div class="clear"></div>
       <?php } } ?>      
     
         <!--End Conversation Start-->
			
		<?php 
			
				if(((strtotime($trip_info->trip_confirm_date)>=strtotime(date('Y-m-d H:i:s'))) &&($trip_info->trip_agent_assign_id == 0)) || (($trip_info->trip_agent_assign_id > 0) && ($trip_info->user_id == $user_info->user_id) && ($offer_user_id == $trip_info->trip_agent_assign_id) && ($trip_info->trip_activity_status == 1))) {
	
				$attributes = array('name'=>'frm_conversation');
				echo form_open('conversation/user/'.$offer_trip_id,$attributes);

					$user_image= base_url().'upload/no_image.png';
					 
					 if($user_info->profile_image!='') {  
						if(file_exists(base_path().'upload/user_thumb/'.$user_info->profile_image)) {
					
							$user_image=base_url().'upload/user_thumb/'.$user_info->profile_image;
						}
					}
			?>  
			<div class="conservations">
				<a href="#"><img src="<?php echo $user_image;?>" alt="" style="width: 65px; "class="fl"  /></a>
				<div class="talk" style="padding-left: 12px;">
				  <textarea name="conversation_message" rows="5" cols="67" style="resize:none; margin:0; float:left;"></textarea>
				  
				  <input type="hidden" id="offer_user_id" name="offer_user_id" value="<?php echo $offer_user_id;?>">
				  <input type="hidden" id="trip_id" name="trip_id" value="<?php echo $trip_id;?>">
				  <input type="hidden" id="post_to_user_id" name="post_to_user_id" value="<?php echo $offer_user_id;?>">
				  <input type="hidden" id="trip_user_id" name="trip_user_id" value="<?php echo $trip_user_id;?>">
				  

				  <div style="margin:0; padding:0;">
					<div style="margin:0; padding:0; float:left; width:350px; height:100px;"><!--<a class="conversation_task marT5" href="#">Dispute Task</a>-->
				  
				  <?php if($trip_info->trip_activity_status == 0 && $trip_info->trip_confirm_date >= date('Y-m-d H:i:s')) { ?>
				  
				    <a class="conversation marT5" href="<?php echo site_url('conversation/accept_offer/'.$offer_trip_id) ?>" >Accept Offer</a>
					
				  <?php } elseif($trip_info->trip_activity_status == 1 && $offer_user_id == $trip_info->trip_agent_assign_id && $trip_info->trip_confirm_date <= date('Y-m-d H:i:s') ) {?>
				  
				  	 <a class="conversation marT5" href="<?php echo site_url('conversation/complete/'.$trip_id) ?>" >Complete Trip</a>
					 
				  <?php } ?>
					</div>
					<div style="margin:0; padding:0; float:right; width:100px; height:100px;"><input type="submit"  class="conversation right marT5" border="0" value="Send" />
					</div>
				  </div>
				</div>
				
			</div>
			</form>
			<?php } ?>
		   
		</div> <div class="clear"></div>

      
      
    </div>
    
      <?php $this->load->view($theme .'/layout/conversation/conversation_sidebar');?>
    
    <div class="clear"></div>
  </div>
  
  <div class="clear"></div>
  <div class="clear"></div>
</div>