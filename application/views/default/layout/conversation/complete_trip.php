<div id="bidcontener">

  
  <!--start_#bidcontener-->
  <div class="bid_form_center">

	   <?php if($error!='') { ?>
			<div class="errmsgcl" id="error" style="margin-top:10px;">
				<?php  echo $error; ?>
			</div>
	   <?php } ?>
	   
	   
    <div class="left_side">
	
		<div class="trip_requirements">
				<h1>Complete Trip</h1>
			</div>
		  
			<div class="clear"></div>
			
			
      <div class="center_top">

        <div class="center_left">
          <h1>Trip: <?php echo anchor('from/'.$trip_info->trip_from_place.'/to/'.$trip_info->trip_to_place.'/'.$trip_info->trip_id,$trip_info->trip_from_place.' &gt '.$trip_info->trip_to_place);?></h1>
		  <p>Budget : <strong><?php echo get_currency_symbol($trip_info->currency_code_id);?><?php echo $trip_info->trip_min_budget;?> - <?php echo get_currency_symbol($trip_info->currency_code_id);?><?php echo $trip_info->trip_max_budget;?></strong></p>
		  <p>posted <span>about <?php echo getDuration($trip_info->trip_added_date);?></span> by <?php echo anchor('user/'.$user_info->profile_name,$user_info->profile_name); ?></p>
        </div>
       
        <div class="center_right">
          <div class="photo" style="margin:10px 31px 0 0;"> 
          <?php 
          	 	$user_image= base_url().'upload/no_image.png';
				if($user_info->profile_image!='') {  
			
					if(file_exists(base_path().'upload/user_thumb/'.$user_info->profile_image)) {
				
						$user_image=base_url().'upload/user_thumb/'.$user_info->profile_image;
						
					}
					
				}
			?>
            <?php echo anchor('user/'.$user_info->profile_name,'<img src="'.$user_image.'" alt="" width="65" height="67"  />'); ?>
           
            
        </div>
        </div>
         <div class="clear"></div>
         
      </div>
	  
	  <br /><br />
	  
	<div class="center_top">
		<?php 
			$agent_offer_details = $this->conversation_model->agent_offer_details($trip_info->trip_id,$offer_user_info->user_id);
			$site_setting = site_setting();
		
		?>
        <div class="center_left">
          <h1>Trip Assign to: <?php echo anchor('user/'.$offer_user_info->profile_name,$offer_user_info->profile_name); ?></h1>
		  <p>Offor Amount : <strong><?php echo get_currency_symbol($trip_info->currency_code_id);?><?php echo $agent_offer_details[0]->final_offer_amount;?></strong></p>
		  <p>Assigned: <span><?php echo date($site_setting->date_time_format, strtotime($trip_info->trip_assign_date)); ?></p>
		   
        </div>
       
        <div class="center_right">
          <div class="photo" style="margin:10px 31px 0 0;"> 
          <?php 
          	 	$user_image= base_url().'upload/no_image.png';
				if($offer_user_info->profile_image!='') {  
			
					if(file_exists(base_path().'upload/user_thumb/'.$offer_user_info->profile_image)) {
				
						$user_image=base_url().'upload/user_thumb/'.$offer_user_info->profile_image;
						
					}
					
				}
			?>
            <?php echo anchor('user/'.$offer_user_info->profile_name,'<img src="'.$user_image.'" alt="" width="65" height="67"  />'); ?>
           
            
        </div>
        </div>
         <div class="clear"></div>
         
      </div>
	  

			
			
			<div class="edt_account">
				<?php 
					$attributes = array('name'=>'frm_conversation');
					echo form_open('conversation/complete/'.$trip_id,$attributes);	
				?>
			
					<label><strong>User's Rating</strong></label>
					<div class="creditcard_date">
					 <select name="user_rating" id="user_rating"> 
							<option value="1" <?php if($user_rating==1) { ?> selected="selected" <?php } ?>>1 - Poor</option>
							<option value="2" <?php if($user_rating==2) { ?> selected="selected" <?php } ?>>2 - Fair</option>
							<option value="3" <?php if($user_rating==3) { ?> selected="selected" <?php } ?>>3 - Average</option>
							<option value="4" <?php if($user_rating==4) { ?> selected="selected" <?php } ?>>4 - Very Good</option>  
							<option value="5" <?php if($user_rating==5) { ?> selected="selected" <?php } ?>>5 - Excellent</option>
					</select>
					
					</div>
					<div class="clear"></div>
					
					<label><strong>User Review</strong></label>
					<div class="textbox" style="height: 80px;margin-left: 5px;"><textarea name="user_review" rows="5" cols="50" style="resize:none; height:inherit;"></textarea></div>
					<div class="clear"></div>
					
					
					
					<div class="btn fr" style="margin-right:115px; margin-top:10px;">   
						<input type="hidden" id="trip_id" name="trip_id" value="<?php echo $trip_id;?>">   
						<input type="hidden" id="trip_user_id" name="trip_user_id" value="<?php echo $trip_user_id;?>"> 
						<input type="hidden" id="trip_offer_user_id" name="trip_offer_user_id" value="<?php echo $trip_offer_user_id;?>">          
						<input type="submit" value="Complete" id="complete" name="complete">
					</div>
				
				</form>
				
			</div>
			
			 <div class="clear"></div>

    </div>
    
    <?php echo $this->load->view($theme.'/layout/user/user_sidebar'); ?>
    
    <div class="clear"></div>
  </div>
  
  
  <div class="clear"></div>
</div>