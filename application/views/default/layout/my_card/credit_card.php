	<div id="bidcontener"><!--start_#bidcontener-->
                
		<div class="bid_form_center">
		
        	<div class="left_side">
			
				<div class="describe_requirements">
					<h1>My Credit Card</h1>
				</div>
				
				<!--show error or update messages start-->
				
				<?php if($error != ''){ 

				if($error=='fail') { ?> 
					<div class="errmsgcl" id="error" style="margin-top:10px;"> 
						<p> Credit Card Information not Added Successfully.</p>
					</div>
				<?php } elseif($error=='update') { ?>
						<div  class="msgsuccess" id="error" style="margin-top:10px;"> 
							<p>Credit Card Information Added Successfully.</p>
						</div> 
		
			   <?php }else{?>
						<div class="errmsgcl" style="margin-top:10px;"> 
						<?php echo $error; ?>
						</div> 
			   <?php }}?>

				<div class="clear"></div> 
				
               <!--show error or update messages end-->  
			        
			   <div class="edt_account">
			   
					<label class="rt">*Required Fields</label><div class="clear"></div>

					<?php
						$attributes = array('name'=>'frmCardInfo','id'=>'frmCardInfo');
						echo form_open('my_card',$attributes);
					?>
					
					<label>First Name*</label>
					<div class="textbox"><input type="text" placeholder="First Name" name="card_first_name" id="card_first_name" value="<?php echo $card_first_name; ?>" /></div><div class="clear"></div>
					
					
					<label>Last Name *</label>
					<div class="textbox"><input type="text" placeholder="Last Name" name="card_last_name" id="card_last_name" value="<?php echo $card_last_name; ?>" /></div><div class="clear"></div>
					
					<label>Credit card number *</label>
					<div class="textbox"><input type="text" placeholder="Credit card number" name="cardnumber" id="cardnumber" maxlength="19" value="<?php echo $cardnumber; ?>" /></div><div class="clear"></div>
					
					<label>Card type</label>
					<div class="creditcard_type">
						 <select name="cardtype" id="cardtype"  onChange="javascript:generateCC(); return false;">
							<option value='Visa' <?php if($cardtype=='Visa') { ?> selected <?php } ?>>Visa</option>
							<option value='MasterCard'  <?php if($cardtype=='MasterCard') { ?> selected <?php } ?>>MasterCard</option>
							<option value='Discover'  <?php if($cardtype=='Discover') { ?> selected <?php } ?>>Discover</option>
							<option value='Amex'  <?php if($cardtype=='Amex') { ?> selected <?php } ?>>Amex</option>
						</select>
					</div><div class="clear"></div>

                    <label>Expiry</label>
					<div class="creditcard_date">
						<select name="card_expiration_month" id="card_expiration_month" >
							<option value="1" <?php if($card_expiration_month==1) { ?> selected <?php } ?>>1</option>
							<option value="2"  <?php if($card_expiration_month==2) { ?> selected <?php } ?>>2</option>
							<option value="3"  <?php if($card_expiration_month==3) { ?> selected <?php } ?>>3</option>
							<option value="4"  <?php if($card_expiration_month==4) { ?> selected <?php } ?>>4</option>
							<option value="5"  <?php if($card_expiration_month==5) { ?> selected <?php } ?>>5</option>
							<option value="6"  <?php if($card_expiration_month==6) { ?> selected <?php } ?>>6</option>
							<option value="7"  <?php if($card_expiration_month==7) { ?> selected <?php } ?>>7</option>
							<option value="8"  <?php if($card_expiration_month==8) { ?> selected <?php } ?>>8</option>
							<option value="9"  <?php if($card_expiration_month==9) { ?> selected <?php } ?>>9</option>
							<option value="10"  <?php if($card_expiration_month==10) { ?> selected <?php } ?>>10</option>
							<option value="11"  <?php if($card_expiration_month==11) { ?> selected <?php } ?>>11</option>
							<option value="12"  <?php if($card_expiration_month==12) { ?> selected <?php } ?>>12</option>
						</select>
						
						<select name="card_expiration_year" id="card_expiration_year" style="margin-left:54px;">
							<?php for($i=date('Y');$i<=date('Y')+7;$i++)  { ?>					  
							<option value="<?php echo $i;?>" <?php if($card_expiration_year==$i) { ?> selected <?php } ?>><?php echo $i;?></option>
							<?php } ?>
						</select>
					</div><div class="clear"></div>
					
					<label>CVC number *</label>
					<div class="textbox"><input type="password" placeholder="CVC numbere" name="cvv2Number" id="cvv2Number" value="<?php //echo $cvv2Number; ?>" /></div><div class="clear"></div>
                    
					
					<h3 style="margin-left:25px;margin-top: 25px;">Card Address</h3>
					<div class="clear"></div>
					
					<label>Address *</label>
					<div class="textbox"><input type="text" placeholder="Address" name="card_address" id="card_address" value="<?php echo $card_address; ?>" /></div><div class="clear"></div>
					
					
					<label>City *</label>
					<div class="textbox"><input type="text" placeholder="City" name="card_city" id="card_city" value="<?php echo $card_city; ?>" /></div><div class="clear"></div>
					
					<label>State *</label>
					<div class="textbox"><input type="text" placeholder="State" name="card_state" id="card_state" maxlength="19" value="<?php echo $card_state; ?>" /></div><div class="clear"></div>
					
					<label>Zipcode *</label>
					<div class="textbox"><input type="text" placeholder="Zipcode" name="card_zipcode" id="card_zipcode" maxlength="19" value="<?php echo $card_zipcode; ?>" /></div><div class="clear"></div>
					

	
					<div class="btn fr" style="margin-right:175px; margin-top:10px;">              
						<input type="submit" value="Update" id="updatebtn" name="updatebtn">
					</div>
					
					</form>

			   </div>
               <div class="clear"></div>
			   
			</div>
            
			<?php echo $this->load->view($theme.'/layout/user/user_sidebar'); ?>

		</div>
        <div class="clear"></div>
		
	</div>