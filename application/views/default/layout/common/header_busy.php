<?php $site_setting=site_setting(); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
   <title><?php echo $pageTitle; ?></title>   
    <meta name="description" content="<?php echo $metaDescription; ?>" />
	<meta name="keywords" content="<?php echo $metaKeyword; ?>" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="text/javascript">
		var baseUrl='<?php if(strstr(site_url(),'index.php')) { echo site_url().'/'; } else { echo base_url(); } ?>';
		var baseThemeUrl='<?php echo base_url().getThemeName(); ?>';
	</script>
<script type="text/javascript" src="<?php echo base_url().getThemeName(); ?>/js/lib/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<?php echo base_url().getThemeName(); ?>/js/function.js"></script>
<script src="<?php echo base_url().getThemeName(); ?>/js/jquery.placeholder.js"></script> 
  
<link rel="stylesheet" type="text/css" href="<?php echo base_url().getThemeName(); ?>/css/style.css">
<link media="screen" type="text/css" rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/css/style.php?p=<?php echo base_url(); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url().getThemeName(); ?>/css/common.css">

<link href='http://fonts.googleapis.com/css?family=Signika:400,300,600,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

<!--[if IE]>
<link media="all" type="text/css" rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/css/iestyle.css"/>
<![endif]-->

<!--[if IE 7]>
<link media="all" type="text/css" rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/css/ie7.css"/>
<![endif]-->

<!--[if IE 8]>
<link media="all" type="text/css" rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/css/ie8.css"/>
<![endif]-->

<!--[if IE 9]>
<link media="all" type="text/css" rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/css/ie9.css"/>
<![endif]-->

<!--[if lt IE 9]>
        <script src="<?php echo base_url().getThemeName(); ?>/js/html5.js"></script>
<![endif]-->

</head>
<body><?php $data['site_setting']=$site_setting; echo $this->load->view($theme .'/layout/common/header_home',$data);  ?>