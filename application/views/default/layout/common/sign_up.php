<script type="text/javascript">
function accept_legal() {
		$('#myModal').modal('show');
		$(this).attr('data-toggle','modal');
        }
    function accept_term() {
		$('#myModalterm').modal('show');
		$(this).attr('data-toggle','modal');
        }    
        
</script>
<div class="wrapper row10">
    <div class="container main-contain">
    <div class="login-block">
        <h1 class="text-center font-light white">SIGN UP HERE  </h1>
            <div class="login_wrapper">
            <div class="login_panel">
                <!--<div class="login_head">
                        <h1>Log In</h1>
                    </div>-->
                <div class="master-form"> 
                   <?php
	$attributes = array('name'=>'signup_form','id'=>'signup_form','class'=>'');
	echo form_open('home/register',$attributes);
?>

<?php if($error!='') { ?>
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php  echo $error; ?>
					
				</div>
  		 <?php  } ?>

                              <div class="form-group">
                                <label for="exampleInputEmail1" class="label-control">Profile Name :</label>
                                <input type="text" class="form-control" id="profilename" name="profilename" placeholder="Please enter a valid profile name" value="<?php echo $profilename;?>">
                              </div>
							  
							  <div class="form-group">
                                <label for="exampleInputEmail1" class="label-control">First Name :</label>
                                <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Please enter a valid first name" value="<?php echo $firstname; ?>">
                              </div>
							  
							  <div class="form-group">
                                <label for="exampleInputEmail1" class="label-control">Last Name :</label>
                                <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Please enter a valid last name" value="<?php echo $lastname; ?>">
                              </div>
							  
							  <div class="form-group">
                                <label for="exampleInputEmail1" class="label-control">Email :</label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Please enter a valid email" value="<?php echo $lastname; ?>">
                              </div>
							  
                               <div class="form-group">
                                <label for="password" class="label-control">Password  :</label>
                                <input type="password" class="form-control" id="password"  name="password" placeholder="Please enter a valid password">
                              </div>
							  
							  <div class="form-group">
                                <label for="confirmpassword" class="label-control">Confirm Password  :</label>
                                <input type="password" class="form-control" id="confirmpassword"  name="confirmpassword" placeholder="Please enter a valid confirm password">
                              </div>
							  
							  
                              <div class="form-group">
                                   <div class="checkbox">
                             <label class="label-control">  <input type="checkbox" class="chkbox" id="terms_condition" name="terms_condition"></label><a href="javascript://" onclick="accept_term();" class="morelink"> <label class="label-control"> Accept Terms & Conditions </label></a>
                                   </div>     
                              </div>
							  
							  <div class="form-group">
                                   <div class="checkbox">
                                        <label class="label-control">  <input type="checkbox" class="chkbox" id="legal_desc" name="legal_desc"></label>
										<a href="javascript://" onclick="accept_legal();" class="morelink"> <label class="label-control"> Legal Disclaimers for uploading/downloading </label></a>
                                   </div>     
                              </div>
                              <div class="form-group text-center margin-top-20">
								<input type="submit" name="registeruser" id="submit" value="Register" class="btn yellow-btn"/>
                              </div>
                              <div class="text-center">
							  <a href="<?php echo site_url("login") ?>" class="morelink">Already have an account? Login</a>
                              </div>
                         </form> 
                     </div>
                    
                    </div>
                </div> 
            </div>
        </div>    
    </div>
</div>  




<div class="modal fade" id="myModalterm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
      </div>
      <div class="label-control modal-body">
          
          <div style="height: 400px;overflow-x: hidden;overflow-y: scroll; ">
	    <?php $res=$this->home_model->get_page_content('terms-of-use');
           
           if(!empty($res)){
               
               echo $res->description;
               }?>
          
 </div>
        
      </div>
      <div class="modal-footer">
      <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Legal Disclaimer</h4>
      </div>
      <div class="label-control modal-body">
            <div style="height: 400px; overflow-x: hidden;overflow-y: scroll; ">
	   <?php $res2=$this->home_model->get_page_content('legal-disclaimer');
           
           if(!empty($res2)){
               
               echo $res2->description;
               }?>
                </div>
        
      </div>
      <div class="modal-footer">
      <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
    </div>
  </div>
</div>