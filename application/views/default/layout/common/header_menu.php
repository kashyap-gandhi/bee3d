<?php 
$user_info = get_user_profile_by_id(get_authenticateUserID());
$user_notification = get_user_unread_notification(get_authenticateUserID());
?>
<header id="top_header">
	<div class="container">
		<div class="row">
		
			<div class="col-xs-6 col-sm-2">
				<a href="<?php echo site_url('home');?>" class="logo_main" title=""><img src="<?php echo base_url().getThemeName()?>/images/footer-logo.png" height="50"/></a>
			</div>
			
                  
                      
			<div class="col-sm-push-7 col-sm-2 text-right ">
                            
                             <div style="float:left;padding: 12px 0;"> Welcome, <?php echo $user_info->first_name;?>...</div>           
                            
		<div class="notification_dropdown">
					<a href="<?php echo site_url('user/notifications');?>" class="notification_icon">
						<?php if($user_notification)
						{?>
						<span class="label label-danger"><?php echo $user_notification;?></span>
						<?php }?>
						<i class="icon-comment-alt icon-2x"></i>
					</a>
			</div>		
				<div class="notification_separator"></div>	
				<?php /*<div class="notification_separator"></div>
				<div class="notification_dropdown dropdown">
					<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
						<span class="label label-danger">12</span>
						<i class="icon-envelope-alt icon-2x"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-wide">
						<li>
							<div class="dropdown_heading">Messages</div>
							<div class="dropdown_content">
								<ul class="dropdown_items">
									<li>
										<h3><a href="#">Lorem ipsum dolor sit amet</a></h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
										<p class="large_info">Sean Walter, 24.05.2014</p>
										<i class="icon-exclamation-sign indicator"></i>
									</li>
									<li>
										<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam assumenda&hellip;</p>
										<p class="large_info">Sean Walter, 24.05.2014</p>
									</li>
									<li>
										<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
										<p>Lorem ipsum dolor sit amet, consectetur&hellip;</p>
										<p class="large_info">Sean Walter, 24.05.2014</p>
										<i class="icon-exclamation-sign indicator"></i>
									</li>
								</ul>
							</div>
							<div class="dropdown_footer">
								<a href="#" class="btn btn-sm btn-default">Show all</a>
								<div class="pull-right dropdown_actions">
									<a href="#"><i class="icon-refresh"></i></a>
									<a href="#"><i class="icon-cog"></i></a>
								</div>
							</div>
						</li>
					</ul>
				</div><?php */?>
				
			</div>
                    
                   
                            
                   
			<?php
				 $user_image= base_url().'upload/no_userimage.png';
				 if($user_info->profile_image!='') {  
					if(file_exists(base_path().'upload/user/'.$user_info->profile_image)) {
						$user_image=base_url().'upload/user/'.$user_info->profile_image;	
					}					
				}
				?>
                    
			<div class="col-xs-6 col-sm-push-5 col-sm-3">
                             
                          
                            
				<div class="pull-right dropdown">
					<a href="#" class="user_info dropdown-toggle" title="" data-toggle="dropdown">
						<img src="<?php echo $user_image;?>" alt="">
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo site_url('user/profile');?>">Profile</a></li>
						<li><a href="<?php echo site_url('user/change_password');?>">Change Password</a></li>
						<li><a href="<?php echo site_url('home/logout');?>">Log Out</a></li>
					</ul>
				</div>
			</div>
			<!--<div class="col-xs-12 col-sm-pull-6 col-sm-4">
				<form class="main_search">
					<input type="text" id="search_query" name="search_query" class="typeahead form-control">
					<button type="submit" class="btn btn-primary btn-xs"><i class="icon-search icon-white"></i></button>
				</form> 
			</div>-->
		</div>
	</div>
</header>						
<nav id="top_navigation">
	<div class="container">
		<ul id="icon_nav_h" class="top_ico_nav clearfix">
			<li>
				<a href="<?php echo site_url('/');?>">
					<i class="icon-home icon-2x"></i>
					<span class="menu_label">Home</span>
				</a>
			</li>
			<!--<li>             
				<a href="#">
					<i class="icon-edit icon-2x"></i>
					<span class="menu_label">Learning Hive</span>
				</a>
			</li>-->
			<li>             
				<a href="<?php echo site_url('design/all');?>">
					<i class="icon-picture icon-2x"></i>
					<span class="menu_label">Design Hive</span>
				</a>
			</li>
			<li>             
				<a href="<?php echo site_url('video/all');?>">
					<!--<span class="label label-danger">12</span>-->
					<i class="icon-play-circle icon-2x"></i>
					<span class="menu_label">Videos</span>
				</a>
			</li>
			
			<li>             
				<a href="<?php echo site_url('challenge/all');?>">
					<!--<span class="label label-danger">12</span>-->
					<i class="icon-tasks icon-2x"></i>
					<span class="menu_label">Challenge Hive</span>
				</a>
			</li>
			
			<li>             
				<a href="<?php echo site_url('printer/search');?>">
					<i class="icon-map-marker icon-2x"></i>
					<span class="menu_label">3D Printers</span>
				</a>
			</li>
			
			<li>             
				<a href="<?php echo site_url('transaction');?>">
					<!--<span class="label label-danger">12</span>-->
					<i class="icon-file-text-alt icon-2x"></i>
					<span class="menu_label">Transaction</span>
				</a>
			</li>
			
			<li>             
				<a href="<?php echo site_url('wallet');?>">
					<!--<span class="label label-danger">12</span>-->
					<i class="icon-credit-card icon-2x"></i>
					<span class="menu_label">Wallet</span>
				</a>
			</li>
			
			<li>             
				<a href="<?php echo site_url('user/mypackage');?>">
					<!--<span class="label label-danger">12</span>-->
					<i class="icon-trello icon-2x"></i>
					<span class="menu_label">My Package</span>
				</a>
			</li>
			
			<li class="active">             
				<a href="<?php echo site_url('package/all');?>">
					<!--<span class="label label-danger">12</span>-->
					<i class="icon-bullseye icon-2x"></i>
					<span class="menu_label">Buy Credit</span>
				</a>
			</li>
			
                        <?php if(isset($active_menu)){ if($active_menu=='3ddesign') {?>
                        <li><a data-toggle="modal" href="#designcategory">
					
					<i class="icon-book icon-2x"></i>
					<span class="menu_label">Add Category</span>
				</a></li>
                                
                                <?php } } ?>
                                <?php if(isset($active_menu)){ if($active_menu=='video') {?>
                        <li><a data-toggle="modal" href="#videocategory">
					
					<i class="icon-book icon-2x"></i>
					<span class="menu_label">Add Category</span>
				</a></li>
                                
                                <?php } } ?>
						<!--class="active"-->
			
		</ul>
	</div>
</nav>


<?php if(isset($active_menu)){ if($active_menu=='3ddesign' || $active_menu=='video') {?>

<div class="modal fade" id="designcategory">
    <div class="modal-dialog">
            <div class="modal-content">
                    <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add Design Category</h4>
                    </div>
                    <div class="modal-body">
                      
                        <div class="adddiv">
                            <lable>Selected Category :</lable> <lable id="parentcatname">Main Category</lable>
                            </div>
                        
                        <div class="adddiv2">
                            
                            
                           <lable> Category Name : </lable>
                             <input type="text" id="new_cat_name" value="" />
                             <input type="hidden" id="parentcatid" value="0" />
                             <button type="button" class="btn btn-primary" id="submitdesigncat">ADD</button>
                             
                        </div>
                        
                         <div class="clearfix" style="border-bottom: 1px dashed #666;padding-top: 45px;"></div>
                        
                        <ul class="treeview" id="adddesignselect">
                       
                        </ul>
                        <div id="sidetreecontrol"> <a href="?#" class="controlcollapse">Collapse All</a> | <a href="?#" class="controlexpand">Expand All</a> </div>
                        
                       
                        <div class="clearfix"></div>
                        
                    </div>
                   
            </div>
    </div>
</div>


<link rel="stylesheet" href="<?php echo base_url() . getThemeName(); ?>/tree-view/jquery.treeview.css" /> 
<link rel="stylesheet" href="<?php echo base_url() . getThemeName(); ?>/tree-view/screen.css" /> 
 

<script src="<?php echo base_url() . getThemeName(); ?>/tree-view/jquery.cookie.js" type="text/javascript"></script> 
<script src="<?php echo base_url() . getThemeName(); ?>/tree-view/jquery.treeview.js" type="text/javascript"></script> 
<script>
    var cnt=0;
    $("#designcategory").on("show.bs.modal",function(){
       getdesigncategory();
    });
    
    
    function bindadd(){
        
        $("#adddesignselect").treeview({
                expandable: true,
                animated: "medium",
                control:"#sidetreecontrol",				
                prerendered: true,
                persist: "location"
        });
                        
                        
        $(".addselcategory").on("click",function(){
           var catname='Main Category'; 
           
           var id=$(this).attr('data-id');
           catname=$(this).text();
           
           $("#designcategory #parentcatid").val(id);
           if(id==0){
               catname='Main Category';
           }
           $("#parentcatname").html(catname);
        });
    }
    
    
    function getdesigncategory()
    {   
       
         $.ajax({						
                type: 'GET',
                url: '<?php echo site_url('design/designcattree/');?>',   
                dataType: 'html', 
                cache: false,
                async: false,
                success:function(data){                  
                    var edata='<li><a href="javascript:void(0)" class="addselcategory selmain" data-id="0">Click to Select Main Category</a></li>';
                    edata+=data;
                   $("#designcategory #adddesignselect").html(edata);
                   bindadd();
                }
        });
    }
    
    
    $("#submitdesigncat").on("click",function(){
        
        var new_name=$.trim($("#new_cat_name").val());
        
        if(new_name==''){
            alert('Please enter new category name.');
            return false;
        }
        
        var selected_category=$("#parentcatid").val();
        
        var rest= $.ajax({						
                type: 'POST',
                url: '<?php echo site_url('design/addcategory/');?>',   
                dataType: 'json', 
                cache: false,
                async: false,
                data:{'category_name':new_name,'selected_category':selected_category}                
        }).responseText;
        
        var res=jQuery.parseJSON(rest);
      
                    
                    if(res.status=='success'){
                        window.location.href=window.location;
                    } else {
                        if(res.msg=='login'){
                            window.location.href="<?php echo site_url('/');?>";
                        } else {
                            alert(res.msg);
                            return false;
                        }
                    }
        
    });
    
    
    </script>


<div class="modal fade" id="videocategory">
       <div class="modal-dialog">
            <div class="modal-content">
                    <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add Video Category</h4>
                    </div>
                    <div class="modal-body">
                      
                        <div class="adddiv">
                            <lable>Selected Category :</lable> <lable id="parentcatname2">Main Category</lable>
                            </div>
                        
                        <div class="adddiv2">
                            
                            
                           <lable> Category Name : </lable>
                             <input type="text" id="new_cat_name2" value="" />
                             <input type="hidden" id="parentcatid2" value="0" />
                             <button type="button" class="btn btn-primary" id="submitvideocat">ADD</button>
                             
                        </div>
                        
                         <div class="clearfix" style="border-bottom: 1px dashed #666;padding-top: 45px;"></div>
                        
                        <ul class="treeview" id="addvideoselect">
                       
                        </ul>
                        <div id="sidetreecontrol2"> <a href="?#" class="controlcollapse">Collapse All</a> | <a href="?#" class="controlexpand">Expand All</a> </div>
                        
                       
                        <div class="clearfix"></div>
                        
                    </div>
                   
            </div>
    </div>
</div>
    
    
    <script>
    var cnt=0;
    $("#videocategory").on("show.bs.modal",function(){
       getvideocategory();
    });
    
    
    function bindaddv(){
        
        $("#addvideoselect").treeview({
                expandable: true,
                animated: "medium",
                control:"#sidetreecontrol2",				
                prerendered: true,
                persist: "location"
        });
                        
                        
        $(".addselcategory2").on("click",function(){
           var catname='Main Category'; 
           
           var id=$(this).attr('data-id');
           catname=$(this).text();
           
           $("#videocategory #parentcatid2").val(id);
           if(id==0){
               catname='Main Category';
           }
           $("#parentcatname2").html(catname);
        });
    }
    
    
    function getvideocategory()
    {   
       
         $.ajax({						
                type: 'GET',
                url: '<?php echo site_url('video/videocattree/');?>',   
                dataType: 'html', 
                cache: false,
                async: false,
                success:function(data){                  
                    var edata='<li><a href="javascript:void(0)" class="addselcategory2 selmain" data-id="0">Click to Select Main Category</a></li>';
                    edata+=data;
                   $("#videocategory #addvideoselect").html(edata);
                   bindaddv();
                }
        });
    }
    
    
    $("#submitvideocat").on("click",function(){
        
        var new_name=$.trim($("#new_cat_name2").val());
        
        if(new_name==''){
            alert('Please enter new category name.');
            return false;
        }
        
        var selected_category=$("#parentcatid2").val();
        
        var rest= $.ajax({						
                type: 'POST',
                url: '<?php echo site_url('video/addcategory/');?>',   
                dataType: 'json', 
                cache: false,
                async: false,
                data:{'category_name':new_name,'selected_category':selected_category}                
        }).responseText;
        
        var res=jQuery.parseJSON(rest);
      
                    
                    if(res.status=='success'){
                        window.location.href=window.location;
                    } else {
                        if(res.msg=='login'){
                            window.location.href="<?php echo site_url('/');?>";
                        } else {
                            alert(res.msg);
                            return false;
                        }
                    }
        
    });
    
    
    </script>


    <?php } } ?>