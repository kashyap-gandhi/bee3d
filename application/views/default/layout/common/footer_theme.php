 <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                       <a href="<?php echo site_url();?>" target="_blank" style="color: #333333;"> &copy; Copyright <?php echo date('Y')?> iHive 3D</a>
                    </div>
                    <div class="col-sm-8">
                        <ul>
                            <li><a href="<?php echo site_url('user/dashboard');?>">Dashboard</a></li>
                            <li>&middot;</li>
                            <li><a href="<?php echo site_url('home/content/terms-of-use');?>">Terms of Service</a></li>
                            <li>&middot;</li>
                            <li><a href="<?php echo site_url('home/content/privacy-policy');?>">Privacy Policy</a></li>
                            <li>&middot;</li>
                            <li><a href="<?php echo site_url('home/contactus');?>">Contact us</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-1 text-right">
                        <small class="text-muted">v1.0</small>
                    </div>
                </div>
            </div>
        </footer>
        
	<!-- jQuery -->
		
	<!-- jQuery resize event -->
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/jquery.ba-resize.min.js"></script>
	<!-- retina ready -->
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/jquery_cookie.min.js"></script>
	<!-- retina ready -->
<!--		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/retina.min.js"></script>-->
	<!-- tinyNav -->
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/tinynav.js"></script>
	<!-- sticky sidebar -->
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/jquery.sticky.js"></script>
	<!-- Navgoco -->
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/navgoco/jquery.navgoco.min.js"></script>
	<!-- jMenu -->
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/jMenu/js/jMenu.jquery.js"></script>
	<!-- typeahead -->
        <script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/typeahead.js/typeahead.min.js"></script>
        <script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/typeahead.js/hogan-2.0.0.js"></script>
	
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/ebro_common.js"></script>


		<!-- peity (small charts) -->
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/peity/jquery.peity.min.js"></script>
		<!-- vector map -->
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
		<!-- charts -->
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/flot/jquery.flot.min.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/flot/jquery.flot.pie.min.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/flot/jquery.flot.time.min.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/flot/jquery.flot.tooltip.min.js"></script>
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/flot/jquery.flot.resize.js"></script>
		<!-- easy pie chart -->
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/easy-pie-chart/jquery.easy-pie-chart.js"></script>
			
			<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/pages/ebro_dashboard1.js"></script>
			<!-- responsive lightbox -->

<!-- mixItUp -->
<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/mixitup/jquery.mixitup.min.js"></script>

<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/magnific-popup/jquery.magnific-popup.min.js"></script>

<!--<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/pages/ebro_gallery.js"></script>-->



	<!--[if lte IE 9]>
		<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/ie/jquery.placeholder.js"></script>
		<script>
			$(function() {
				$('input, textarea').placeholder();
			});
		</script>
	<![endif]-->
	