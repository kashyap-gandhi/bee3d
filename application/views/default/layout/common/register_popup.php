<style>
.error1{
	color:#FF0000;
	float:left;
	margin-left:15px;
}
.success{
	color:#009900;
	float:left;
	margin-left:15px;
}
</style>

<?php 
	$data = array(
		'facebook'		=> $this->fb_connect->fb,
		'fbSession'		=> $this->fb_connect->fbSession,
		'user'			=> $this->fb_connect->user,
		'uid'			=> $this->fb_connect->user_id,
		'fbLogoutURL'	=> $this->fb_connect->fbLogoutURL,
		'fbLoginURL'	=> $this->fb_connect->fbLoginURL,	
		'base_url'		=> site_url('home/facebook'),
		'appkey'		=> $this->fb_connect->appkey,
	);
	
	$t_setting = twitter_setting();	            
    $f_setting = facebook_setting();
?>
<script type="text/javascript" src="<?php echo base_url().getThemeName(); ?>/js/validation.js"></script>
<script type="text/javascript">
$(document).ready(function(){	
 	 validate_login();
	 validate_signup();
	
});
</script>
<div class="clear"></div>
<div class="popupreglogin">
       <div class="left" style="float:left;">
      
      
      	<div class="register_left marl10 shadow_popup_none" style="height:325px;">
         
          
        	
           
             <h2 class="martop5">Log in</h2><div class="clear"></div>
             <hr />
			 
			 <?php
				$attributes = array('name'=>'loginForm','id'=>'loginForm','autocomplete'=>'off');
				echo form_open('login/'.$msg,$attributes);
			?>
			
				<label class="marbottom0 martop12">Username or Email Address: </label><div class="clear"></div>
				<input type="text" name="login_email" id="login_email" placeholder="Username or Email Address" value=""   />
				<span id="loginemailInfo"></span>
				<div class="clear"></div>
				
				<label class="marbottom0 martop12">Password: </label><div class="clear"></div>
				<input type="password" name="login_password" id="login_password" placeholder="password" value="" />    
				<span id="loginPasswordInfo"></span>
				<div class="clear"></div> 

				<div class="textbox_popup stay_popup ">
					<input type="checkbox" value="1" id="remember" name="remember" tabindex="9">
					<label for="remember_popup">Stay signed in on this computer</label>
				</div>
              
             
             	<input type="hidden" name="clogin" value="clogin" id="clogin" />
				<input type="hidden" name="msg" id="msg" value="<?php echo $msg; ?>" />
                <input type="submit" value="Sign In" class="marbottop" id="loginbtn" name="loginbtn" /><div class="clear"></div>
             
             </form>
        </div><div class="clear"></div>
        
        
        <div class="register_right_popup_account">
        <h2 class="martop5 paddingbottom0">Use another account</h2><div class="clear"></div>
        <hr/><div class="clear"></div>
        <div class="sharebuttons">
           
			<?php if($t_setting->twitter_login_enable == '1'){ ?>
				 <div class="twitterbtn"><em></em><?php echo anchor('home/twitter_auth/','Twitter');?></div>
			<?php } else {?>
				 <div class="twitterbtn"><em></em><?php echo anchor('#','Twitter');?></div>
			<?php } ?>
		
           
            <div class="googleplusbtn"><em></em><a onClick="window.parent.location.href='<?php echo site_url('home/google_signin');?>'" href="javascript:void(0)">Google</a></div>
           
           <?php if($f_setting->facebook_login_enable == '1'){ ?>
				<div class="facebookbtn"><em></em> <?php echo anchor($data['fbLoginURL'],'Facebook');?></div>
			<?php } else {?>
				<div class="facebookbtn"><em></em> <?php echo anchor('#','Facebook');?></div>
			<?php } ?>
           
           <div class="yahoobtn"><em></em><a onClick="window.parent.location.href='<?php echo site_url('home/yahoo_signin');?>'" href="javascript:void(0)">Yahoo</a></div>
           
        </div><div class="clear"></div>
        
        
   
        
        </div>
        <div class="clear"></div>
        </div>
        
        
        
	<div class="register_left_account">
        	
           
             <h2 class="martop5">Register</h2><div class="clear"></div>
             <hr>
              <?php
					$attributes = array('name'=>'signupForm','id'=>'signupForm','autocomplete'=>'off');
					echo form_open('sign_up/'.$msg,$attributes);
			  ?>
			  
			  
			   <label>Username / Account URL: </label><div class="clear"></div>
                <span id="full_nameInfo"></span><div class="clear"></div>
             	   <input type="text" class="sp_input" name="full_name" id="full_name" style="text-align:right;" value="<?php echo $full_name;  ?>" />
                <input type="text" class="sp_input_sec" readonly="readonly"  value=".<?php echo DOMAIN_NAME;  ?>" style="text-align:right;"  /><div class="clear"></div>
                
                
                <label>Email: </label><div class="clear"></div>
                <span id="emailInfo"></span>
             	<input type="text" name="email" id="email" value="<?php echo $email; ?>" /><div class="clear"></div>
                
                <label>Password: </label><div class="clear"></div>
                <span id="passwordInfo"></span>
             	<input type="password" name="password" id="password" value="<?php echo $password; ?>"  /><div class="clear"></div>
                
                <label>Retype Password: </label><div class="clear"></div>
                <span id="retypePasswordInfo"></span>
             	<input type="password" name="retypePassword" id="retypePassword" value=""  /><div class="clear"></div>
                
                 <?php	
					$site_setting=site_setting();
					if($site_setting->captcha_enable==1) { 
				?>
              <label>
                	<?php  dsp_crypt(0,1); ?>
               </label>
                <input type="text" name="captcha"  value=""  /><div class="clear"></div>
                
                
             <?php } ?>
                
                
                <input type="hidden" name="register" value="register" id="register" />
				<input type="hidden" name="msg" id="msg" value="<?php echo $msg; ?>" />
                <input type="submit" value="Register" name="sign_up" id="sign_up" /><div class="clear"></div>
				
				
             	
                
                 <!--<label style="height:30px; background-color:#000000; margin-bottom:5px;">&nbsp;</label><div class="clear"></div>-->
             	

             
             </form>
             
             <div class="clear"></div>
        </div>
     </div>
    <div class="clear"></div>