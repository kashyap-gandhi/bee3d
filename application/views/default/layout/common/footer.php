<!-- *************************************************************************************** -->
<?php 
$site_setting = site_setting();
?>

<div class="wrapper row7">
    <div class="container main-contain">
    <footer>
    	<div class="row">
        	<div class="col-md-5 brd-rt">
                    <ul class="links">
<!--                    <li> <a href="<?php //echo site_url('home')?>"> 3d learning  </a> </li>-->
                    <li> <a href="<?php echo site_url('design/search')?>">3d designs </a> </li>
                    <li><a href="<?php echo site_url('video/search');?>">3D VIDEOS</a></li>
                    <li> <a href="<?php echo site_url('challenge/search')?>">3d challenges </a> </li>
                    <li> <a href="<?php echo site_url('printer/search');?>">3D PRINTERS</a> </li>
                    
				  <?php if(get_authenticateUserID() > 0)
		  			{?>
                     <li ><a href="<?php echo site_url('user/dashboard');?>">Dashboard</a></li>
					<li> <a href="<?php echo site_url('home/logout')?>">Logout</a> </li>
               <?php }
				else
				{?>
				     <li> <a href="<?php echo site_url('home/register')?>">sign up </a> </li>
                    <li> <a href="<?php echo site_url('login')?>">login </a> </li>
				<?php }?>	
               	</ul>
                    
                    
                    
                    
                    <ul class="links padleft15">

                    <li> <a href="<?php echo site_url('home/content/about')?>">ABOUT iHIVE3D </a> </li>
                    <li><a href="<?php echo site_url('home/content/terms-of-use');?>">TERMS & CONDITIONS</a></li>
                    <li> <a href="<?php echo site_url('home/content/privacy-statement')?>">PRIVACY STATEMENT </a> </li>
                    <li> <a href="<?php echo site_url('home/content/legal-disclaimer');?>">LEGAL DISCLAIMER</a> </li>
                    
				 	
               	</ul>
                    <div class="clear"></div>
                    
            </div>
            <div class="col-md-3 brd-rt">
            	<div class="text-center">
                	<a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/footer-logo.png" alt="" /></a>
                </div>
                <div>
                	<ul class="social-link">
                    	<li class="fb"> <a href="<?php echo $site_setting->facebook_link;?>"> &nbsp;</a></li>
                        <li class="tw"> <a href="<?php echo $site_setting->twitter_link;?>" > &nbsp;</a></li>
                        <li class="gm"> <a href="<?php echo $site_setting->google_plus_link;?>" > &nbsp;</a></li>
                        <li class="in"> <a href="<?php echo $site_setting->linkedin_link;?>"> &nbsp;</a></li>
                    </ul>
                </div>
                <p class="text-center"> <a href="<?php echo site_url();?>" target="_blank" style="color: #333333;"> &copy; Copyright <?php echo date('Y')?> iHive 3D</a></p>
            </div>
           
             <div class="col-md-4">
             	<h1 class="font-light white">SIGN UP FOR iHIVE 3D NEWS </h1>
                <form class="form-horizontal" role="form">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-5 control-label">First Name</label>
                    <div class="col-sm-7">
                      <input type="email" class="form-control" id="inputEmail3" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-5 control-label">Email Address</label>
                    <div class="col-sm-7">
                      <input type="email" class="form-control" id="inputEmail3">
                    </div>
                  </div>
                  
                  
                  <div class="form-group">
                  	 <label for="inputEmail3" class="col-sm-5 control-label">Check which applies</label>
                    <div class="col-sm-offset-6">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox">3D Newbie
                        </label>
                      </div>
                       <div class="checkbox">
                        <label>
                          <input type="checkbox">3D Intermediate
                        </label>
                      </div>
                       <div class="checkbox">
                        <label>
                          <input type="checkbox">3D Pro
                        </label>
                      </div>
                    </div>
                  </div>
                </form>
             </div>
           
           </div>
     
    </footer>
    
    </div>
    
</div>       
