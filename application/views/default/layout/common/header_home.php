   <!-- Bootstrap -->
    <link href="<?php echo base_url().getThemeName(); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url().getThemeName(); ?>/css/default.css" rel="stylesheet">
     <link href="<?php echo base_url().getThemeName(); ?>/css/reset.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--    <script src="https://code.jquery.com/jquery.js"></script>-->
    <script src="<?php echo base_url().getThemeName(); ?>/js/jquery-1.9.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url().getThemeName(); ?>/js/bootstrap.min.js"></script>

    
    <?php if(isset($is_home) && $is_home==1){ ?>
<!-- Top Navigation -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
         <a class="navbar-brand" href="<?php echo site_url('home');?>"><img src="<?php echo base_url().getThemeName(); ?>/images/header-logo.png" alt="" /></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
<!--            <li class="active"><a href="<?php echo site_url('home');?>">3D LEARNING</a></li>-->
            <li><a href="<?php echo site_url('design/landing');?>">3D DESIGNS</a></li>
            <li><a href="<?php echo site_url('video/landing');?>">3D VIDEOS</a></li>
            <li><a href="<?php echo site_url('challenge/landing');?>">3D CHALLENGES</a></li>
            <li><a href="<?php echo site_url('printer/search');?>">LOCATE 3D PRINTER</a></li>
          
          </ul>
          <ul class="nav navbar-nav navbar-right">
		  <?php if(get_authenticateUserID() > 0)
		  {?>
		  	 <li class="active"><a href="<?php echo site_url('user/dashboard');?>">Dashboard</a></li>
            <li class="active"><a href="<?php echo site_url('home/logout');?>">Logout</a></li>
			
		  <?php 
		  }else
		  {?>
		    <li  class="active"><a href="<?php echo site_url('home/register');?>">SIGN UP</a></li>
            <li><a href="<?php echo site_url('login');?>">Log In</a></li>
		  <?php }?>
          
          </ul> 
        </div><!--/.nav-collapse -->
      </div>
    </div>
<!-- End Top Navigation -->
<?php } else {?>



<!-- Top Navigation -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
         <a class="navbar-brand" href="<?php echo site_url('home');?>"><img src="<?php echo base_url().getThemeName(); ?>/images/header-logo.png" alt="" /></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav header_logo"><li><a class="" href="<?php echo site_url('home');?>"><img src="<?php echo base_url().getThemeName(); ?>/images/inner_header-logo.png" alt="" /></a></li></ul>
          <ul class="nav navbar-nav main_header">
<!--            <li class="active"><a href="<?php echo site_url('home');?>">3D LEARNING</a></li>-->
            <li><a href="<?php echo site_url('design/landing');?>">3D DESIGNS</a></li>
            <li><a href="<?php echo site_url('video/landing');?>">3D VIDEOS</a></li>
            <li><a href="<?php echo site_url('challenge/landing');?>">3D CHALLENGES</a></li>
            <li><a href="<?php echo site_url('printer/search');?>">LOCATE 3D PRINTER</a></li>
          
          </ul>
          <ul class="nav navbar-nav right_header navbar-right">
		  <?php if(get_authenticateUserID() > 0)
		  {?>
		  	 <li class="active"><a href="<?php echo site_url('user/dashboard');?>">Dashboard</a></li>
            <li class="active"><a href="<?php echo site_url('home/logout');?>">Logout</a></li>
			
		  <?php 
		  }else
		  {?>
		    <li  class="active"><a href="<?php echo site_url('home/register');?>">SIGN UP</a></li>
            <li><a href="<?php echo site_url('login');?>">Log In</a></li>
		  <?php }?>
          
          </ul> 
        </div><!--/.nav-collapse -->
      </div>
    </div>
<!-- End Top Navigation -->

<?php } ?>

