<div class="wrapper row10">
    <div class="container main-contain">
    <div class="login-block">
        <h1 class="text-center font-light white">Forget Password  </h1>
            <div class="login_wrapper">
            <div class="login_panel">
                <!--<div class="login_head">
                        <h1>Log In</h1>
                    </div>-->
                <div class="master-form"> 
                    			<?php
								$attributes = array('name'=>'forgetForm','id'=>'forgetForm','class'=>'');
	                            echo form_open('forget_password',$attributes);
							?>
<?php if($error!='') { ?>
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php  echo $error; ?>
					
				</div>
  		 <?php  } ?>
                              <div class="form-group">
                                <label for="exampleInputEmail1" class="label-control">Email :</label>
                                <input type="text" class="form-control" id="forget_email" name="forget_email" placeholder="Please enter a valid Email">
                              </div>
                            
                              <div class="form-group text-center margin-top-20">
                                 <input type="hidden" name="cforget" value="cforget" id="cforget" />
							  <input type="submit" name="submit" value="Submit" class="btn yellow-btn"/>
                              </div>
                              <div class="text-center">
                                 <a href="<?php echo site_url("login") ?>" class="morelink">Login</a>
                              </div>
                         </form> 
                     </div>
                    
                    </div>
                </div> 
            </div>
        </div>    
    </div>
</div>  

<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/jquery.min.js"></script>