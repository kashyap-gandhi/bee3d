<div class="span3">
        	<div class="view-runner">
            	

    <?php                    
        $category_infos=get_category();
        
        if($category_infos) { ?>
        
        <div class="runner">
        	<h1>Search By Category</h1>
        
        	
			  <ul>
               <?php foreach($category_infos as $category_info) { ?>
                    
               		<li><a href="<?php echo site_url('challenge/category/'.$category_info->category_url_name.'/'.$search);?>" <?php if($cat_name==$category_info->category_url_name) { ?> style="font-weight:bold;" <?php } ?>><?php echo $category_info->category_name;?></a></li>
                    <?php
                    $sub_categories = sub_category($category_info->category_id);
                         if($sub_categories){ 
                           foreach($sub_categories as $sub_category) { ?>
                         <li><a href="<?php echo site_url('challenge/category/'.$sub_category->category_url_name.'/'.$search);?>" <?php if($cat_name==$sub_category->category_url_name) { ?> style="font-weight:bold;" <?php } ?>>&nbsp;-&nbsp;<?php echo $sub_category->category_name;?></li>
                    <?php } 
                     } ?>
                    
              <?php } ?>
            </ul>
            
        </div>
        
        <?php } ?>
        
		
		 
            </div>
        </div>