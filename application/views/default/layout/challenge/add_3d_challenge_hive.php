<!-- Add fancyBox main JS and CSS files -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url().getThemeName(); ?>/js/tag/jquery.tagsinput.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url().getThemeName(); ?>/css/datepicker.css" />
<!-- responsive lightbox -->
<?php /*?> <link rel="stylesheet" href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/magnific-popup/magnific-popup.css">
<?php */?><script type="text/javascript" src="<?php echo base_url()?>tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().getThemeName(); ?>/js/bootstrap-datepicker.js"></script>
	<!-- aditional stylesheets -->
	<script type="text/javascript">
	tinymce.init({selector:'#challenge_criteria,#challenge_requirements,#challenge_specification'});
	
  $(document).ready(function(){
  	
    	$('#challenge_start_date').datepicker({
       format: 'yyyy-mm-dd'
        });

     $('#challenge_end_date').datepicker({
       format: 'yyyy-mm-dd'
     });
     
      $('#challenge_start_date').datepicker({ format: "yyyy-mm-dd" }).on('changeDate', function(ev){
         $(this).datepicker('hide');
     });
     
     
      $('#challenge_end_date').datepicker({ format: "yyyy-mm-dd" }).on('changeDate', function(ev){
         $(this).datepicker('hide');
     });
  });



	function show_parent_challenge()
	{
		$('#parent_challenge_div').slideToggle();
	}
	
	function delete_challenge_attachment(id)
	{
		var y = confirm('Are you sure yo delete image?');
		if(y)
		{
			$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>challenge/delete_challenge_attachment",         //the script to call to get data          
			data: {attachment_id: id},
		   // data: $("#member_form").serialize(),
			dataType: '',                //data format      
			success: function(data)          //on receive of reply
				{
					if(data == 'success')
					{
						//$('#coupon_gallery_span_'+id).removeClass('img_upload');
						$('#image_attach_'+id).html('');
					}
				} 
			});
		}
		else
		{
			//alert('rr');
		}
	}
	</script>
<!-- parsley -->
		<!-- responsive table -->
			<link href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>
		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Dashboard</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
						
						<!-- main content -->
						<div class="row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Add 3D challenge</h4>
									</div>
									<?php if($error!='') { ?>
											<div class="alert alert-danger">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
													<?php  echo $error; ?>
												
											</div>
									 <?php  } ?>
									
									<div class="panel-body">
											<?php
												$attributes = array('name'=>'challenge_form','id'=>'challenge_form','class'=>'');
												echo form_open_multipart('challenge/add_me',$attributes);
											?>
											<div class="form_sep">
												<label for="reg_input_name" class="req">Title</label>
												<input type="text" id="challenge_title" name="challenge_title" class="form-control" data-required="true" value="<?php echo $challenge_title;?>">
											</div>
												<div class="form_sep">
													<label for="reg_input_email" class="req">Challenge Start Date</label>
													<input type="text" id="challenge_start_date" name="challenge_start_date" class="form-control" data-required="true" value="<?php echo $challenge_start_date;?>">
													
												</div>
												
												<div class="form_sep">
													<label for="reg_input_email" class="req">Challenge End Date</label>
													<input type="text" id="challenge_end_date" name="challenge_end_date" class="form-control" data-required="true" value="<?php echo $challenge_end_date;?>">
													
												</div>
											
											
											<div class="form_sep">
												<label for="reg_input_email" class="req">Challenge Criteria</label>
												<textarea name="challenge_criteria" id="challenge_criteria" cols="30" rows="4" class="form-control"><?php echo $challenge_criteria;?></textarea>
												
											</div>
											
											<div class="form_sep">
												<label for="reg_input_email" class="req">Challenge Requirements</label>
												<textarea name="challenge_requirements" id="challenge_requirements" cols="30" rows="4" class="form-control"><?php echo $challenge_requirements;?></textarea>
												
											</div>
											
											<div class="form_sep">
												<label for="reg_input_email" class="req">Challenge Specification</label>
												<textarea name="challenge_specification" id="challenge_specification" cols="30" rows="4" class="form-control"><?php echo $challenge_specification;?></textarea>
												
											</div>
											
											
											<div class="form_sep">
												<label for="reg_select" class="req">Challenge Category</label>
												<select name="challenge_category" id="challenge_category" class="form-control" data-required="true">
													<option value="">--Select Category--</option>
													<?php 
													if($category)
													{
														foreach($category as $cat)
														{?>
														<option value="<?php echo $cat->category_id;?>" <?php if($challenge_category == $cat->category_id){?> selected="selected"<?php }?>><?php echo $cat->category_name;?></option>
														
														<?php }
														
													}
													?>
												</select>
											</div>
											
										
									
								<div class="form_sep">
								<label for="reg_textarea_message" class="req">Challenge Points</label>
								<input type="text" id="challenge_point" name="challenge_point" class="form-control" value="<?php echo $challenge_point;?>">
								</div>
											
											
										<div class="form_sep">
												<label for="reg_textarea_message">Meta Keywords</label>
												<textarea rows="3" cols="70" class="form-control autosize_textarea" name="challenge_meta_keyword" id="challenge_meta_keyword" ><?php echo $challenge_meta_keyword;?></textarea>
												
											</div>
											
											<div class="form_sep">
												<label for="reg_textarea_message">Meta Description</label>
											<textarea rows="3" cols="70" class="form-control autosize_textarea" name="challenge_meta_description" id="challenge_meta_description" ><?php echo $challenge_meta_description;?></textarea>

											</div>
											
											<div class="form_sep">
												<label for="reg_textarea_message">Challenge Tag</label>
												<div id="add_more_tag">
											<div id="more_tag">
											<input id="tags_1" type="text" class="tags" name="tag_name" value="<?php echo $challenge_tag;?>" />
											</div>
											</div>
											<div class="clear"></div>
											</div>
											<div class="form_sep">
											<input type="hidden" name="challenge_id" id="challenge_id" value="<?php echo $challenge_id;?>"/>
											<input type="submit" name="submit" id="submit" value="submit" class="btn btn-default"/>
											</div>	
										</form>
									</div>
								</div>
							</div>
							
						</div>
						
					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
			<div id="footer_space"></div>
			
			<!-- Modal -->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="text-align:center;" >
<div class="modal-body">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <img src="#"/>
</div>
<!--<div class="modal-footer">
<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
</div>-->
</div>
		</div>

       
<script src="<?php echo base_url().getThemeName(); ?>/js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url().getThemeName(); ?>/js/tag/jquery.tagsinput.js"></script>
<?php /*?><!-- mixItUp -->
			<script src="js/lib/mixitup/jquery.mixitup.min.js"></script>
	<!-- responsive lightbox -->
      <script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/magnific-popup/jquery.magnific-popup.min.js"></script>
			
			
<!-- multiple select -->
<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/multiple-select/jquery.multiple.select.js"></script>
<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/pages/ebro_gallery.js"></script>
<?php */?>
<script type="text/javascript">
$(document).ready(function(){
$('#tags_1').tagsInput({width:'auto'});

//===== Usual validation engine=====//
	$("#challenge_form").validate({
		rules: {
			challenge_title: "required",
			challenge_point: "required",
			challenge_content: "required",
			challenge_category:"required"
			
		}

	});
	$('li a').click(function(e) {
    $('#myModal img').attr('src', $(this).attr('data-img-url')); 
});
});	
	
</script>
