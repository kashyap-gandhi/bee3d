<?php
$site_setting=site_setting();
$data['site_setting']=$site_setting;

$themurl = base_url().getThemeName(); 
?>
    <link href="<?php echo $themurl; ?>/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo $themurl; ?>/css/owl.theme.css" rel="stylesheet">
	 
    <script src="<?php echo $themurl; ?>/js/owl.carousel.js"></script>
   	<script type="text/javascript">
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
        autoPlay: 3000,
        items : 6,
        itemsDesktop : [1199,2],
        itemsDesktopSmall : [979,2]
      });
	  
	  
	$("#limit").change(function(){
		alert(this.value);
		var srch = $("#search").val();
		document.location.href = '<?php echo site_url("challenge/search/") ?>'+srch+'/'+this.value;
		});
});
    </script>
    <!-- *************************************************************************************** -->
    <style>
    .row8{
        background: none !important;
    }
</style>
<div class="wrapper row8 page-padding-top">
   <!--
   <div class="container">
   		<div class="inner-header">
            <div class="row">
            <div class=" margin-bottom-20 col-md-6 col-sm-6 br-black "> 
              <div class="text-center margin-top-20"> 
                <img src="<?php echo $themurl; ?>/images/inner_logo.png" alt="Logo" class="img-responsive" />
              </div>
               <div class="text-center head-social"	>
                    <ul class="social-link">
                        <li class="fb"> <a href="#"> &nbsp;</a></li>
                        <li class="tw"> <a href="#" > &nbsp;</a></li>
                        <li class="gm"> <a href="#" > &nbsp;</a></li>
                        <li class="in"> <a href="#"> &nbsp;</a></li>
                    </ul>
                </div>
            
            </div>
            <div class="col-md-6 col-sm-6"> 
            	<div class="inner_top_link">
                	<ul>
                    	<li> <a href="#"> <img src="<?php echo $themurl; ?>/images/learning_hive.png" alt="Learing HIVE" class="img-responsive" /> </a> </li>
                        <li> <a href="#"> <img src="<?php echo $themurl; ?>/images/design_hive.png" alt="Learing HIVE" class="img-responsive" /> </a> </li>
                        <li> <a href="#"> <img src="<?php echo $themurl; ?>/images/challenge_hive.png" alt="Learing HIVE"  class="img-responsive" /> </a> </li>
                    </ul>
                </div>
                <div class="newletter">
                	<div class="title">SIGN UP FOR iHIVE 3D NEWS </div>
                    <div class="title_sub">   COMMUNICATE, COLLABORATE ALL THINGS 3D </div>
                    <div> 
                    	<input type="text" class="m-wrap medium pull-left" />
                    	<a href="#"  class="btn yellow-btn"> Submit</a>
                    </div>
                </div>
            
            </div>
            </div>
        </div>
    
   </div>
   -->
</div>

<!-- *************************************************************************************** -->
    
<!-- *************************************************************************************** -->
<div class="wrapper row11">
	<div class="container">
    	<div class="search-block"> 
    	<div class="row">
        	<div class="col-md-3">
            	<div class="advsearch-block"> 
                	<div class="min-height">
                              <h3 class="search-title"> CATEGORIES </h3>
<!--                    	<h3 class="search-title"> Advanced Search </h3>
                        <h4 class="sub-title"> Reset Search </h4>-->
                        <div class="search-filter">
<!--                        	<h3 class="filter-title"> FRESHNESS</h3>-->
                            <ul class="list-unstyled" id="category-list">
                         
                            <?php
							 $category_infos=get_category();
							if($category_infos)
							{
						       foreach($category_infos as $category_info)
							   {?>
                               <li class="pad24">
                                     <a href="<?php echo site_url('challenge/category/'.$category_info->category_url_name.'/'.$search);?>" <?php if($cat_name==$category_info->category_url_name) { ?> style="font-weight:bold;" <?php } ?>><?php echo $category_info->category_name;?></a> </li>                                  
                                      <?php
                    $sub_categories = sub_category($category_info->category_id);
                         if($sub_categories){ 
                           foreach($sub_categories as $sub_category) { ?>
                        <li claaa="pad24">
                                      <a href="<?php echo site_url('challenge/category/'.$sub_category->category_url_name.'/'.$search);?>" <?php if($cat_name==$sub_category->category_url_name) { ?> style="font-weight:bold;" <?php } ?>>&nbsp;-&nbsp;<?php echo $sub_category->category_name;?></a></li>
                    <?php } 
                     } ?>
                                  
						 <?php }
							}
							?>
                            	
                              
                                 
                            </ul>
                        </div> 
                        
                     
                    </div>
                     <?php
			$attributes = array('name'=>'frm_search_task_worker','class'=>'fr marT10');
			
			if($cat_name!='')
			{
				echo form_open('challenge/category/'.$cat_name,$attributes);
			
			} else {
				
					echo form_open('challenge/search',$attributes);
				
			}		
				
	   ?>
                    <div class="search-filter">
<!--                    	 <h4 class="sub-title"> Do not include these words</h4>-->
                    	   <input type="text" class="form-control" name="search" id="search"   placeholder="<?php echo $this->lang->line('search.text_search')?>" value="<?php if($search!='' && $search!=0) { echo urldecode($search); } ?>">	
                          <button id="refine-search" type="submit" class="btn yellow-btn">SEARCH BY KEYWORD</button>
                    </div>
                </form>	
                </div>
            
            	
            </div>
            <div class="col-md-9">
            	<div class="searchlist-block">
                	<div class="searchlist-top">
                    	<div class="row">
                        	<div class="col-md-5 col-sm-5"> 
                            	<h3 class="search-title"> <!--Search Title Here --></h3>
                            </div>
                            <div class="col-md-7 col-sm-7"> 
                            	<div class="form-horizontal col-lg-12" role="form">
                                  <div class="form-group">
                                   <!-- <label for="inputEmail3" class=" col-sm-9 control-label">Display Preferences :</label>-->
                                    <div class="col-sm-3 ">
                                         <!--  <select class="form-control" name="limit" id="limit">
                                           <option value="1" <?php //if($limit == "1"){ ?> selected="selected" <?php //}?>>1</option>
                                             <option value="2" <?php //if($limit == "2"){ ?> selected="selected" <?php// }?>>2</option>
                                          <option value="10" <?php //if($limit == "10"){ ?> selected="selected" <?php //}?>>10</option>
                                          <option value="20" <?php //if($limit == "20"){ ?> selected="selected" <?php// }?>>20</option>
                                          <option value="30" <?php //if($limit == "30"){ ?> selected="selected" <?php //}?>>30</option>
                                          <option value="40" <?php// if($limit == "40"){ ?> selected="selected" <?php// }?>>40</option>
                                          <option value="50" <?php //if($limit == "50"){ ?> selected="selected" <?php// }?>>50</option>
                                        </select>-->
                                    </div>
                                  </div>
  								</div>
                                <!--<div class="col-md-12"> 
                                	<ul>
                                    	<li> page </li>
                                        <li> 1 </li>
                                        <li> of 450 </li>
                                        <li> <a href="#"> </a> </li>
                                    </ul>
                                </div>-->
                            </div> 
                        </div>
                    </div>
                    	
                    <div class="searchlist-detail">
<!--                    	<div class="related-search">
                        	<ul class="list-unstyled inline" >
                            	<li> Searches   </li>-->
                               
                                
                                  <?php
							/*$category_selected_list = explode("-",$category_id);
							//print_r($category_selected_list);
							if($category)
							{
						       foreach($category as $ca)
							   {
								 if(in_array($ca->category_id,$category_selected_list,true))
								 {
                                 echo '<li> <a href="javascript:void(0);"> '.$ca->category_name.' </a> </li>';
								 }
							  }
							}*/
							?>
<!--                            </ul>
                        </div>-->
                        
                        <div>
                        <ul class="search-listing columns-4">
                            
                            <?php 
							if($result)
							{
							   foreach($result as $res)
							   {
								  // $main_image = $this->design_model->get_one_main_design_image($res->design_id);
								  $main_image = $res->profile_image;
								   
								   ?>
                               <li>
                          
                                <?php 
														if($main_image != '' && file_exists(base_path().'upload/user_thumb/'.$main_image))
														{
														?>
                                                        <a href="<?php echo site_url('challenge/'.$res->challenge_slug);?>">
															<img alt="<?php echo $main_image; ?>" src="<?php echo base_url()?>upload/user_thumb/<?php echo $main_image;?>"  class="img-responsive minheight20 imgcenter" /></a><?php }else{?>
														 <a href="<?php echo site_url('challenge/'.$res->challenge_slug);?>">	<img alt="No Image" src="<?php echo base_url()?>upload/no_userimage.png" class="img-responsive minheight20 imgcenter" />	</a>
                                                            <?php }?>
                                <p><a href="<?php echo site_url('challenge/'.$res->challenge_slug);?>"><?php echo substr($res->challenge_title,0,30);?></a></p>
                                <h4> <strong><?php echo $res->challenge_point;?> </strong> </h4>
                                    <div class="rig-brd clearfix">
                                         <div class="pull-left"><?php if($res->profile_name!='' && $res->first_name!='') { ?>
                                                                <a class="lineH23" href="<?php echo site_url('bee/' . $res->profile_name); ?>"> <?php echo $res->first_name; ?> </a> <?php } ?>
                                        
                                                            </div>
                                        
                                        <div class="pull-right">
                                       <!-- <a href="#"> <i class="sprit-icon comment "> </i> </a> -->
                                        <!-- <a href="<?php echo site_url('challenge/'.$res->challenge_slug);?>"> <i class="sprit-icon like "> </i> </a> --> 
                                        <?php 
										  $challenge_status='';
										  if($res->challenge_activity_status == 0)
										  {
										  	$challenge_status='Posted';
											echo "<a href=".site_url('challenge/'.$res->challenge_slug)."> <img src='".$themurl."/images/icon/orange.png' style='margin:0px !important;' title='".$challenge_status."'  alt='".$challenge_status."'> </a>";
										  }
										  else if($res->challenge_activity_status == 1)
										  {
										  	$challenge_status='Assigned';
											echo "<a href=".site_url('challenge/'.$res->challenge_slug)."> <img src='".$themurl."/images/icon/blue.png' style='margin:0px !important;'  title='".$challenge_status."' alt='".$challenge_status."'> </a>";
										  }
										  else if($res->challenge_activity_status == 2)
										  {	
										  	$challenge_status='Completed';
											echo "<a href=".site_url('challenge/'.$res->challenge_slug)."> <img src='".$themurl."/images/icon/green.png' style='margin:0px !important;'  title='".$challenge_status."' alt='".$challenge_status."'> </a>";
										  }
										  else 
										  {
										  	$challenge_status='Closed';
											echo "<a href=".site_url('challenge/'.$res->challenge_slug)."> <img src='".$themurl."/images/icon/red.png' style='margin:0px !important;'  title='".$challenge_status."' alt='".$challenge_status."'> </a>";
										  }

						  				?>
                                        </div> 
                                    </div>
                            </li>
							<?php }	
							} else{
							?>
                                 <li>No Record(s)</li>
                          <?php }?>                               
                           
                		</ul>
                        </div>
                        
                           <ul class="">
													<?php echo $page_link;?>
													</ul>
                        
                    </div>
                    
                    
                </div>		  
            
            	
            </div>
        </div>
        </div>
    	 
    </div>
</div> 

  
<!-- *************************************************************************************** -->