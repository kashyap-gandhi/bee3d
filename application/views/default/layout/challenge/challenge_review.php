<!-- parsley -->
		<!-- responsive table -->
			<link href="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/FooTable/css/footable.core.css" rel="stylesheet" type="text/css"/>
		<div id="wrapper_all">
			<?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>
			
			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
						<li><span>Dashboard</span></li>						
					</ul>
				</div>
			</section>
                
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix">
					<div id="main_content">
						
						<!-- main content -->
						<div class="row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Challenge Review</h4>
									</div>
									<?php if($error!='') { ?>
											<div class="alert alert-danger">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
													<?php  echo $error; ?>
												
											</div>
									 <?php  } ?>
									
									<div class="panel-body">
											<?php
												$attributes = array('name'=>'challenge_form','id'=>'challenge_form','class'=>'');
												echo form_open_multipart('challenge/complete/'.$challenge_bid_id,$attributes);
											?>
											<div class="form_sep">
												<label for="reg_input_name" class="req">USer rating</label>
												<select id="user_rating" name="user_rating" class="form-control" data-required="true">
												<option value="">--select--</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												</select>
											</div>
												<div class="form_sep">
													<label for="reg_input_email" class="req">User Review</label>
													
													<textarea id="user_review" name="user_review" class="form-control" data-required="true"><?php echo $user_review;?></textarea>
													
												</div>
												
											
											<div class="form_sep">
											<input type="hidden" name="challenge_bid_id" id="challenge_bid_id" value="<?php echo $challenge_bid_id;?>"/>
											<input type="submit" name="submit" id="submit" value="submit" class="btn btn-default"/>
											</div>	
										</form>
									</div>
								</div>
							</div>
							
						</div>
						
					</div>
				</div>

				<?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?>
			</section>
			<div id="footer_space"></div>
			
			<!-- Modal -->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="text-align:center;" >
<div class="modal-body">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <img src="#"/>
</div>
<!--<div class="modal-footer">
<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
</div>-->
</div>
		</div>

       
<script src="<?php echo base_url().getThemeName(); ?>/js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url().getThemeName(); ?>/js/tag/jquery.tagsinput.js"></script>
<?php /*?><!-- mixItUp -->
			<script src="js/lib/mixitup/jquery.mixitup.min.js"></script>
	<!-- responsive lightbox -->
      <script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/magnific-popup/jquery.magnific-popup.min.js"></script>
			
			
<!-- multiple select -->
<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/lib/multiple-select/jquery.multiple.select.js"></script>
<script src="<?php echo base_url().getThemeName(); ?>/fronttheme/js/pages/ebro_gallery.js"></script>
<?php */?>
<script type="text/javascript">
$(document).ready(function(){
$('#tags_1').tagsInput({width:'auto'});

//===== Usual validation engine=====//
	$("#challenge_form").validate({
		rules: {
			challenge_title: "required",
			challenge_point: "required",
			challenge_content: "required",
			challenge_category:"required"
			
		}

	});
	$('li a').click(function(e) {
    $('#myModal img').attr('src', $(this).attr('data-img-url')); 
});
});	
	
</script>
