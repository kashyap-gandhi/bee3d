
<!-- *************************************************************************************** -->
<div class="wrapper row6 page-padding-top">
    <div class="container main-contain">
    	<div class="challenge-block">
        	<h1 class="text-center font-light  dark-gery">  
				<a href="<?php echo site_url('challenge/search');?>" class="dark-gery">TRY YOUR BRAIN<strong class="dark-gery"> 3D CHALLENGE  </strong></a>
			 </h1>
             <h2 class="text-center dark-gery"> THINK YOU'VE GOT WHAT IT TAKES TO SOLVE ANOTHER MEMBER&lsquo;S 3D CHALLENGE? SELECT A CHALLENGE AND BID ON SOLVING IT! </h2>
             <div>
            	<hr>
                <div class="text-center margin-bottom-30"> <img src="<?php echo base_url().getThemeName(); ?>/images/bullate.png" alt="" /></div>
            	<h1 class="text-center font-light dark-gery"> TOYS + MODELS   </h1>
            </div>
            <div class="product-listing">
            	<ul>
					<?php 
					if($latest_design)
					{
						foreach($latest_design as $res)
						{
						if($res->attachment_name != '' && file_exists(base_path().'upload/design/'.$res->attachment_name))
						{?>
						<li>
                         <a href="<?php echo site_url('design/'.$res->design_slug);?>">
											<img alt="<?php echo $res->design_slug; ?>" src="<?php echo base_url()?>upload/design/<?php echo $res->attachment_name;?>" width="152" height="152">
						</a>
						</li>
						<?php }else{?>
						 <li><a href="<?php echo site_url('design/'.$res->design_slug);?>">	<img alt="No Image" src="<?php echo base_url().getThemeName(); ?>/images/152x152.png"></a></li>
                             <?php }?>
						<?php }
					}
					?>
<?php /*?>                	<li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li>
                    <li><a href="#"> <img src="<?php echo base_url().getThemeName(); ?>/images/152x152.png" alt="152x152" /> </a> </li><?php */?>
                </ul>
            </div>
            <hr class="wd80">
            <h1 class="text-center font-light dark-gery"> GOT A 3D CHALLENGE?  
				<?php if(get_authenticateUserID()!='') {?>
			<a href="<?php echo site_url('challenge/add_me');?>" class="dark-gery"><span class="font-itcbold"> POST IT HERE! </span></a>
			<?php }
			else
			{?>
			<a href="<?php echo site_url('login');?>" class="dark-gery"><span class="font-itcbold"> POST IT HERE! </span></a>
			<?php }?>
			</h1>
        </div>    
    </div>
</div>    
