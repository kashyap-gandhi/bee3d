<script src="<?php echo base_url().getThemeName(); ?>/js/jquery-1.9.1.min.js"></script> 
<script type="text/javascript">

$(document).ready(function(){
	
	$("#comment-submit").click(function(){
		
	
	$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>challenge/add_comment",         //the script to call to get data          
			data: $("#add_comment").serialize(),
			dataType: '',                //data format      
			success: function(data)          //on receive of reply
				{
					
					$("#msg-add-success").html("Comment added Successfully");	
				
					location.reload();
				} 
			});
			
		});
		
});
		
</script>
<div id="wrapper_all">
  <?php 
			$theme = getThemeName();
			echo $this->load->view($theme .'/layout/common/header_menu',TRUE);?>
  <!-- mobile navigation -->
  <nav id="mobile_navigation"></nav>
  <section id="breadcrumbs">
    <div class="container">
      <ul>
        <li><a href="<?php echo site_url('user/dashboard')?>">Home</a></li>
        <li><span>Dashboard</span></li>
      </ul>
    </div>
  </section>
  <section class="container clearfix main_section">
    <div id="main_content_outer" class="clearfix">
      <div id="main_content"> 
        
        <!-- main content -->
        
        <h2>Dashboard</h2>
       
        
        <!-- ************************************************************** -->
      
        
          <!-- ************************************************************** -->
        <div class="row">
          <div class="col-sm-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                Conversation <!-- <span class="span_right">
                <a class="" href="#">All </a> | <a href="#" class="btn btn-primary">Post jobs</a> </span>  -->
                </h4>
              </div>
              <div class="panel-body">
                 <div class="chat_app">
							<div class="row">
								<div class="col-sm-11 col-md-12">
									<div class="panel panel-default">
										<!-- <div class="panel-heading">
											<h4 class="panel-title pull-left">Chatroom</h4>
											<div class="btn-group btn-group-sm pull-right">
												<button data-title="New Chat" data-container="body" data-toggle="tooltip" type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-comments"></span></button>
												<button data-title="Settings" data-container="body" data-toggle="tooltip" type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-cog"></span></button>
												<button data-title="Close Chat" data-container="body" data-toggle="tooltip" type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-remove"></span></button>
											</div>
										</div> -->
										<div class="chat_messages">
										
										<?php
										if($bid_detail)
										{?>
											<div class="chat_message">
												<!-- <img class="img-thumbnail" src="img/avatars/avatar_16.jpg" alt=""> -->
												<div class="chat_message_body">
													<p><?php echo $bid_detail["bid_description"]; ?></p>
													<p class="chat_message_date"><?php echo getDuration($bid_detail["bid_date"]); ?></p>
													<p><?php echo getUserProfileName($bid_detail["user_id"]); ?></p>
													
												</div>
											</div>
									<?php }
										?>
											<?php 
									if($conversation_list)
									{
										$i = 0;
										foreach($conversation_list as $cl)
										{
										     	
										?>
									     
									     <div class="chat_message">
												<!-- <img class="img-thumbnail" src="img/avatars/avatar_16.jpg" alt=""> -->
												<div class="chat_message_body">
													<p><?php echo $cl->conversation_msg; ?></p>
													<p class="chat_message_date"><?php echo getDuration($cl->conversation_date); ?></p>
													<p><?php echo $cl->from_name; ?></p>
													
												</div>
											</div>
									   		
								  <?php 
								  $i++;
										 }
									}
									?>
											
										</div>
										<b id="msg-add-success" style="color: #00ff00"></b>
										<?php
										
									    if((strtotime($challenge_details->challenge_end_date)>= strtotime(date("Y-m-d"))))
										{
										?>
											<form action="#" method="post" name="add_comment" id="add_comment">
										<div class="panel-footer">
											 <div class="form-group">
													<textarea class="form-control" rows="3" cols="10" id="conversation_msg" name="conversation_msg"></textarea>
													
												</div>
                                                <input type="hidden" name="challenge_bid_id" id="challenge_bid_id" value="<?php echo  $challenge_bid_id; ?>" />
							                    <input type="hidden" name="challenge_id" id="challenge_id" value="<?php echo  $bid_detail["challenge_id"]; ?>" />
							
												<?php
												$to_user_id = $bid_detail["user_id"];
												
												if($to_user_id ==  get_authenticateUserID())
												{
													$to_user_id = $challenge_details->user_id;
												}
												?>
							
							                    <input  type="hidden" name="conversation_to_user_id" id="conversation_to_user_id" value="<?php echo $to_user_id; ?>"  />
						                        <input  type="hidden" name="conversation_from_user_id" id="conversation_from_user_id" value="<?php echo get_authenticateUserID(); ?>"  />
                                               <div class="form-group">
                                               	
                                               	 <?php
                                                    if($challenge_details->challenge_activity_status == 1 && $bid_detail["is_won"] ==1  &&  $challenge_details->user_id == get_authenticateUserID() )
													{
                                                    ?>
													<button class="btn btn-primary" type="button" onclick="document.location.href='<?php echo site_url("challenge/complete/".$challenge_bid_id); ?>'">Approve Job</button>
                                                
                                                   <?php }?>
                                                   
                                                   <?php
                                                    if($challenge_details->challenge_activity_status == 1 && $bid_detail["is_won"] ==1  &&  $bid_detail["user_id"] == get_authenticateUserID()  )
													{
                                                    ?>
													<button class="btn btn-primary" type="button" onclick="document.location.href='<?php echo site_url("challenge/complete/".$challenge_bid_id); ?>'">Complete Job</button>
                                                
                                                   <?php }?>
                                                     <button class="btn btn-danger" type="button" id="comment-submit">Send</button>
                                                    
                                                    <?php
                                                    get_authenticateUserID();
                                                    if($challenge_details->challenge_activity_status == 0  && get_authenticateUserID() != $bid_detail["user_id"])
													{
                                                    ?>
                                                     <button class="btn btn-success" type="button" id="comment-submit" onclick="document.location.href='<?php echo site_url("challenge/invoice/".$challenge_bid_id); ?>'">Accept</button>
											
											      <?php }?>
												</div> 
												</form>
										</div>
										
										<?php }?>
										
									</div>
								</div>
								
							</div>
						</div>
              </div>
            </div>
          </div>
        </div>
        
        <!-- ************************************************************** --> 
        
      </div>
    </div>
    <?php echo $this->load->view($theme .'/layout/common/sidebar',TRUE);?> </section>
  <div id="footer_space"></div>
</div>