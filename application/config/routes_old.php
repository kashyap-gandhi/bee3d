<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = 'home/error_page';



$route['agentactivate/(.*)/(.*)']='home/agentactivate/$1/$2';
$route['previewprofile/(.*)/(.*)']='home/previewprofile/$1/$2';

$route['user/checkcaptcha']='user/checkcaptcha';
$route['user/showcaptcha/em/(:num)']='user/showcaptcha/em/$1';
$route['user/showcaptcha/ph/(:num)']='user/showcaptcha/ph/$1';

$route['user/getagnetemail']='user/getagnetemail';
$route['user/getagnetemail/(.*)']='user/getagnetemail/$1';

$route['user/getagnetphone']='user/getagnetphone';
$route['user/getagnetphone/(.*)']='user/getagnetphone/$1';


$route['user/tripquote/(.*)/(.*)']='user/tripquote/$1/$2';
$route['user/tripquote/(.*)']='user/tripquote/$1';
$route['user/tripquote']='user/tripquote';


$route['user/airlinequote/(.*)/(.*)']='user/airlinequote/$1/$2';
$route['user/airlinequote/(.*)']='user/airlinequote/$1';
$route['user/airlinequote']='user/airlinequote';

$route['user/edit/(.*)']='user/edit/$1';
$route['user/edit']='user/edit';

$route['user/upload_ajax_image']='user/upload_ajax_image';

$route['user/change_password']='user/change_password';
$route['user/notifications']='user/notifications';

$route['user/load_air_trip/(.*)']='user/load_air_trip/$1';
$route['user/load_air_trip']='user/load_air_trip';

$route['user/load_trip_quote/(.*)']='user/load_trip_quote/$1';
$route['user/load_trip_quote']='user/load_trip_quote';

$route['user/(.*)/reviews/(.*)']='user/reviews/$1/$2';
$route['user/(.*)/reviews']='user/reviews/$1';



$route['user/(.*)/(.*)']='user/profile_detail/$1/$2';
$route['user/(.*)']='user/profile_detail/$1';




$route['search']='search/index';
$route['search/trip/(.*)']='search/index/$1';
$route['search/trip/(.*)/(.*)']='search/index/$1/$2';
$route['search/trip']='search/index';


$route['conversation/offer/(.*)/(.*)']='conversation/offer_user_conversation/$1/$2';
$route['conversation/offer/(.*)']='conversation/offer_user_conversation/$1';


$route['conversation/user/(.*)/(.*)']='conversation/trip_user_conversation/$1/$2';
$route['conversation/user/(.*)']='conversation/trip_user_conversation/$1';
 
$route['conversation/complete/(.*)']='conversation/complete/$1';


/*Route for search from trip detail*/
$route['from']='search';
$route['from/([a-zA-Z%20]+)']='trip/trip_detail/$1';
$route['from/([a-zA-Z%20]+)/to']='trip/trip_detail/$1';
$route['from/([a-zA-Z%20]+)/to/([a-zA-Z%20]+)']='trip/trip_detail/$2';
$route['from/([a-zA-Z%20]+)/to/([a-zA-Z%20]+)/(:num)']='trip/trip_detail/$3';
$route['from/([a-zA-Z%20]+)/to/([a-zA-Z%20]+)/(:num)/([a-zA-Z%20]+)']='trip/trip_detail/$3/$4';


$route['sign_up/(.*)']='home/sign_up/$1';
$route['sign_up']='home/sign_up';

$route['login/(.*)']='home/login/$1';
$route['login']='home/login';

$route['verify/(.*)/(.*)']='home/verify/$1/$2';
$route['password/reset']='home/password_reset';
$route['reset_password/(.*)/(.*)']='home/reset_password/$1/$2';


$route['notifications']='user/notifications';


$route['logout']='home/logout';


$route['contact']='home/contact_us';



$route['content/(.*)/(:num)']='content/detail/$1/$2';
$route['traveller']='content/traveller';
$route['travel-agent']='content/travelagent';
$route['terms-and-condition']='content/terms';
$route['about-us']='content/aboutus';
$route['how-it-works']='content/howitworks';



$route['home/index']='home/index';
$route['home']='home/index';

 
/* End of file routes.php */
/* Location: ./application/config/routes.php */
?>