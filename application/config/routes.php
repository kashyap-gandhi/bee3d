<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = 'home/error_page';


$route['sign_up/(.*)']='home/sign_up/$1';
$route['sign_up']='home/sign_up';

$route['login/(.*)']='home/login/$1';
$route['login']='home/login';

$route['verify/(.*)/(.*)']='home/verify/$1/$2';
$route['password/reset']='home/password_reset';
$route['reset_password/(.*)/(.*)']='home/reset_password/$1/$2';




$route['notifications']='user/notifications';


$route['logout']='home/logout';


$route['contact']='home/contact_us';

$route['home/content/(.*)']='home/content/$1';
$route['home/content']='home/content';

$route['forget_password']='home/forget_password';
$route['home/index']='home/index';
$route['home']='home/index';


$route['bee/index']='bee/index';
$route['bee/(.*)']='bee/profile/$1';



$route['printer/([a-zA-Z0-9-]+)-([0-9]{0,6}+)/([a-zA-Z]+)']='printer/index/$1-$2/$3';
$route['printer/([a-zA-Z0-9-]+)-([0-9]{0,6}+)']='printer/index/$1-$2';



$route['design/post_rating']='design/post_rating';

$route['design/landing']='design/landing';
$route['design/addcategory']='design/addcategory';
$route['design/designcattree/(.*)']='design/designcattree/$1';
$route['design/designcattree']='design/designcattree';
$route['design/delete_design_attachment/(.*)']='design/delete_design_attachment/$1';
$route['design/delete_design_attachment']='design/delete_design_attachment';
$route['design/download_image_zip/(.*)']='design/download_image_zip/$1';
$route['design/download_image_zip']='design/download_image_zip';
$route['design/buy']='design/buy';
$route['design/buy/(.*)']='design/buy/$1';
$route['design/buypoint']='design/buypoint';
$route['design/search/(.*)/(.*)']='design/search/$1/$2';
$route['design/search/(.*)']='design/search/$1';
$route['design/search']='design/search';

$route['design/delete_me/(.*)']='design/delete_me/$1';
$route['design/delete_me']='design/delete_me';

$route['design/like_dislike/(.*)/(.*)']='design/like_dislike/$1/$2';
$route['design/like_dislike/(.*)']='design/like_dislike/$1';
$route['design/like_dislike']='design/like_dislike';


$route['design/edit_me/(.*)']='design/edit_me/$1';
$route['design/add_me']='design/add_me';
$route['design/all/(.*)/(.*)']='design/all/$1/$2/$3';
$route['design/all/(.*)/(.*)']='design/all/$1/$2';
$route['design/all/(.*)']='design/all/$1';
$route['design/all']='design/all';
$route['design/(.*)']='design/index/$1';
$route['design/index']='design/index';


$route['video/post_rating']='video/post_rating';

$route['video/landing']='video/landing';
$route['video/addcategory']='video/addcategory';
$route['video/videocattree/(.*)']='video/videocattree/$1';
$route['video/videocattree']='video/videocattree';
$route['video/buy']='video/buy';
$route['video/buy/(.*)']='video/buy/$1';
$route['video/search/(.*)/(.*)']='video/search/$1/$2';
$route['video/search/(.*)']='video/search/$1';
$route['video/search']='video/search';
$route['video/delete_me/(.*)']='video/delete_me/$1';
$route['video/delete_me']='video/delete_me';

$route['video/like_dislike/(.*)/(.*)']='video/like_dislike/$1/$2';
$route['video/like_dislike/(.*)']='video/like_dislike/$1';
$route['video/like_dislike']='video/like_dislike';


$route['video/edit_me/(.*)']='video/edit_me/$1';
$route['video/add_me']='video/add_me';
$route['video/all/(.*)/(.*)']='video/all/$1/$2/$3';
$route['video/all/(.*)/(.*)']='video/all/$1/$2';
$route['video/all/(.*)']='video/all/$1';
$route['video/all']='video/all';
$route['video/(.*)']='video/index/$1';
$route['video/index']='video/index';



$route['challenge/landing']='challenge/landing';
$route['challenge/conversation/(.*)/(.*)/(.*)']='challenge/conversation/$1/$2/$3';
$route['challenge/conversation/(.*)/(.*)']='challenge/conversation/$1/$2';
$route['challenge/conversation/(.*)']='challenge/conversation/$1';
$route['challenge/conversation']='challenge/conversation';
$route['challenge/offer/(.*)/(.*)/(.*)']='challenge/offer/$1/$2/$3';
$route['challenge/offer/(.*)/(.*)']='challenge/offer/$1/$2';
$route['challenge/offer/(.*)']='challenge/offer/$1';
$route['challenge/offer']='challenge/offer';

$route['challenge/invoice/(.*)/(.*)/(.*)']='challenge/invoice/$1/$2/$3';
$route['challenge/invoice/(.*)/(.*)']='challenge/invoice/$1/$2';
$route['challenge/invoice/(.*)']='challenge/invoice/$1';
$route['challenge/invoice']='challenge/invoice';


$route['challenge/pay/(.*)/(.*)/(.*)']='challenge/pay/$1/$2/$3';
$route['challenge/pay/(.*)/(.*)']='challenge/pay/$1/$2';
$route['challenge/pay/(.*)']='challenge/pay/$1';
$route['challenge/pay']='challenge/pay';

$route['challenge/complete/(.*)/(.*)/(.*)']='challenge/complete/$1/$2/$3';
$route['challenge/complete/(.*)/(.*)']='challenge/complete/$1/$2';
$route['challenge/complete/(.*)']='challenge/complete/$1';
$route['challenge/complete']='challenge/complete';



$route['challenge/add_comment/(.*)']='challenge/add_comment/$1';
$route['challenge/add_comment']='challenge/add_comment';


$route['challenge/post_challenge']='challenge/post_challenge';
$route['challenge/search/(.*)']='challenge/search/$1';
$route['challenge/search']='challenge/search';
$route['challenge/edit_me/(.*)']='challenge/edit_me/$1';
$route['challenge/add_me']='challenge/add_me';

$route['challenge/open/(.*)/(.*)']='challenge/open/$1/$2/$3';
$route['challenge/open/(.*)/(.*)']='challenge/open/$1/$2';
$route['challenge/open/(.*)']='challenge/open/$1';
$route['challenge/open']='challenge/open';

$route['challenge/assigned/(.*)/(.*)']='challenge/assigned/$1/$2/$3';
$route['challenge/assigned/(.*)/(.*)']='challenge/assigned/$1/$2';
$route['challenge/assigned/(.*)']='challenge/assigned/$1';
$route['challenge/assigned']='challenge/assigned';

$route['challenge/running/(.*)/(.*)']='challenge/running/$1/$2/$3';
$route['challenge/running/(.*)/(.*)']='challenge/running/$1/$2';
$route['challenge/running/(.*)']='challenge/running/$1';
$route['challenge/running']='challenge/running';

$route['challenge/completed/(.*)/(.*)']='challenge/completed/$1/$2/$3';
$route['challenge/completed/(.*)/(.*)']='challenge/completed/$1/$2';
$route['challenge/completed/(.*)']='challenge/completed/$1';
$route['challenge/completed']='challenge/completed';

$route['challenge/closed/(.*)/(.*)']='challenge/closed/$1/$2/$3';
$route['challenge/closed/(.*)/(.*)']='challenge/closed/$1/$2';
$route['challenge/closed/(.*)']='challenge/closed/$1';
$route['challenge/closed']='challenge/closed';


$route['challenge/search/(.*)/(.*)']='challenge/search/$1/$2/$3';
$route['challenge/search/(.*)/(.*)']='challenge/search/$1/$2';
$route['challenge/search/(.*)']='challenge/search/$1';
$route['challenge/search']='challenge/search';
$route['challenge/category/(.*)/(.*)']='challenge/category/$1/$2/$3';
$route['challenge/category/(.*)/(.*)']='challenge/category/$1/$2';
$route['challenge/category/(.*)']='challenge/category/$1';
$route['challenge/category']='challenge/category';



$route['challenge/all/(.*)/(.*)']='challenge/all/$1/$2/$3';
$route['challenge/all/(.*)/(.*)']='challenge/all/$1/$2';
$route['challenge/all/(.*)']='challenge/all/$1';
$route['challenge/all']='challenge/all';
$route['challenge/setchallenge/(.*)']='challenge/setchallenge/$1';
$route['challenge/setchallenge']='challenge/setchallenge';
$route['challenge/(.*)/(.*)']='challenge/index/$1/$2';
$route['challenge/(.*)']='challenge/index/$1';
$route['challenge/index']='challenge/index';


 $route['challenge/bidder_open/(.*)/(.*)']='challenge/bidder_open/$1/$2/$3';
$route['challenge/bidder_open/(.*)/(.*)']='challenge/bidder_open/$1/$2';
$route['challenge/bidder_open/(.*)']='challenge/bidder_open/$1';
$route['challenge/bidder_open']='challenge/bidder_open';


$route['challenge/bidder_assigned/(.*)/(.*)']='challenge/bidder_assigned/$1/$2/$3';
$route['challenge/bidder_assigned/(.*)/(.*)']='challenge/bidder_assigned/$1/$2';
$route['challenge/bidder_assigned/(.*)']='challenge/bidder_assigned/$1';
$route['challenge/bidder_assigned']='challenge/bidder_assigned';


$route['challenge/bidder_complete/(.*)/(.*)']='challenge/bidder_complete/$1/$2/$3';
$route['challenge/bidder_complete/(.*)/(.*)']='challenge/bidder_complete/$1/$2';
$route['challenge/bidder_complete/(.*)']='challenge/bidder_complete/$1';
$route['challenge/bidder_complete']='challenge/bidder_complete';
 
/* End of file routes.php */
/* Location: ./application/config/routes.php */
?>