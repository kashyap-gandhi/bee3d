<?php

class Video_model extends IWEB_Model {
    /*
      Function name :User_model
      Description :its default constuctor which called when user_model object initialzie.its load necesary parent constructor
     */

    function Video_model() {
        parent::__construct();
    }
    
    
    
    
    function place_rating(){     
        
        
        $video_rating =(int) $this->input->post('rating'); 
        $video_review_ip=getRealIP();
        $video_review_date=date('Y-m-d h:i:s');
        $user_id=get_authenticateUserID();
        
        $video_id =(int) $this->input->post('video_id');
        
        
        $final_video_rating=0;
        
        
        
            $data_review= array(
                'parent_review_id'=>0,
                'video_id'=>$video_id,
                'user_id'=>$user_id,
                'video_rating'=>$video_rating,            
                'video_review_description'=>'',
                'video_review_ip'=>$video_review_ip,
                'video_review_date'=>$video_review_date           
                );
            $this->db->insert('video_review',$data_review);
            
            
                $final_video_rating=$this->get_video_avg_rating($video_id);
                $this->db->trans_start();
                $sql_res = "update " . $this->db->dbprefix('video') . " set `video_total_rating` = ".$final_video_rating." where video_id =" . $video_id;
                $this->db->query($sql_res);
                $this->db->trans_complete();
            
        
        
        
         return 1;
        
        
    }
    
    
      function get_video_avg_rating($video_id)
    {
         $sql="SELECT AVG(video_rating) as final_video_rating from ".$this->db->dbprefix('video_review')." where parent_review_id=0  and video_id=".$video_id;
         
         $query = $this->db->query($sql);
        
        if($query->num_rows()>0){
        
            $result=$query->row();
            
            return $result->final_video_rating;
      
        }
        return 0;
      
      
    }
    
    
    
    function check_exist_video($video_id, $user_id) {
        $this->db->select('*');
        $this->db->from('video');
        $this->db->where('video_id', $video_id);
        if ($user_id > 0) {
            //For check in view also
            $this->db->where('user_id', $user_id);
        } else {
            $this->db->where('video_status', 1);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    
    
    function get_video_image($video_id, $video_type) {
        $this->db->select('*');
        $this->db->from('video_attachment');
        $this->db->where('video_id', $video_id);
        $this->db->where('attachment_is_main', 1);
        $this->db->where('attachment_type', $video_type);
        $this->db->where('attachment_name != ', '');
        $this->db->order_by('attachment_order', 'asc');
        $this->db->limit(1);

        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            $result = $query->row();

            return $result->attachment_name;
        }
        return false;
    }

    function get_video_file($video_id, $video_type) {
        $this->db->select('*');
        $this->db->from('video_attachment');
        $this->db->where('video_id', $video_id);
        $this->db->where('attachment_is_main', 1);
        $this->db->where('attachment_type', $video_type);
        $this->db->where('attachment_file != ', '');
        $this->db->order_by('attachment_order', 'asc');
        $this->db->limit(1);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();

            return $result->attachment_file;
        }
        return false;
    }

    function get_video_url($video_id, $video_type) {
        $this->db->select('*');
        $this->db->from('video_attachment');
        $this->db->where('video_id', $video_id);
        $this->db->where('attachment_is_main', 1);
        $this->db->where('attachment_type', $video_type);
        $this->db->where('attachment_url != ', '');
        $this->db->order_by('attachment_order', 'asc');
        $this->db->limit(1);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();

            return $result->attachment_url;
        }
        return false;
    }

    function get_video_url_image($video_id, $video_type) {
        $this->db->select('*');
        $this->db->from('video_attachment');
        $this->db->where('video_id', $video_id);
        $this->db->where('attachment_is_main', 1);
        $this->db->where('attachment_type', $video_type);
        $this->db->where('attachment_name != ', '');
        $this->db->order_by('attachment_order', 'asc');
        $this->db->limit(1);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();

            return $result->attachment_name;
        }
        return false;
    }

    // size input prevents buffer overrun exploits.
    function sizeinput($input, $len) {
        (int) $len;
        (string) $input;
        $n = substr($input, 0, $len);
        $ret = trim($n);
        $out = htmlentities($ret, ENT_QUOTES);
        return $out;
    }

    //Check the file is of correct format.
    function checkfile($input) {
        $ext = array('avi', 'flv', 'mp4', 'mov', '3gp');
        $extfile = substr($input['name'], -4);
        $extfile = explode('.', $extfile);
        $good = array();
        $extfile = $extfile[1];
        if (in_array($extfile, $ext)) {
            $good['safe'] = true;
            $good['ext'] = $extfile;
        } else {
            $good['safe'] = false;
        }
        return $good;
    }

    function insert_video() {

        //$video_slug = get_design_slug($this->input->post('video_title'), 0);
        $video_slug = clean_url($this->input->post('video_title'));
        


        $new_video_title = BadWords($this->input->post('video_title'));
        $video_title = $new_video_title['str'];

        $new_video_content = BadWords($this->input->post('video_content'));
        $video_content = $new_video_content['str'];

        $new_video_meta_keyword = BadWords($this->input->post('video_meta_keyword'));
        $video_meta_keyword = $new_video_meta_keyword['str'];

        $new_video_meta_description = BadWords($this->input->post('video_meta_description'));
        $video_meta_description = $new_video_meta_description['str'];
        
        
        $video_is_modified=$this->input->post('video_is_modified');
        $parent_video_id=$this->input->post('parent_video_id');
        
        if($video_is_modified==0 || $video_is_modified==''){
            $parent_video_id=0;
        }
        
        
         $video_status=0;
        
        $get_cat_detail=get_category_obj_by_id('video',$this->input->post('video_category'));
        
        if($get_cat_detail){
            if($get_cat_detail->category_status==1){
                $video_status=1;
            }
        }
        

        $data = array('video_title' => $video_title,
            'video_is_modified' => $video_is_modified,
            'parent_video_id' => $parent_video_id,
            'video_slug' => $video_slug,
            'video_content' => $video_content,
            'video_point' => $this->input->post('video_point'),            
            'video_meta_keyword' => $video_meta_keyword,
            'video_meta_description' => $video_meta_description,
            'comment_allow' => $this->input->post('comment_allow'),
            'video_type' => $this->input->post('video_type'),
            'user_id' => get_authenticateUserID(),
            'video_date' => date('Y-m-d h:i:s'),
            'video_ip' => getRealIP(),
            'video_status' => $video_status,
            'comment_allow' => $this->input->post('comment_allow')
        );
        $this->db->insert('video', $data);
        $video_id = mysql_insert_id();


        //Update slug
        $data_slug = array('video_slug'=> $video_slug . '-' . $video_id);
        $this->db->where('video_id', $video_id);
        $this->db->update('video', $data_slug);

        /* Insert data in design_category_rel */
        if($get_cat_detail){
        $data_video_category_rel = array('category_id' => $this->input->post('video_category'),
            'video_id' => $video_id);
        $this->db->insert('video_category_rel', $data_video_category_rel);
        }


        /* Insert Tags */
        $tags = explode(",", $this->input->post('tag_name'));
        if ($tags) {
            for ($i = 0; $i < count($tags); $i++) {
                $tag_name=trim(strip_tags($tags[$i]));
                if($tag_name!=''){
                $tag_id = check_tag($tag_name);
               if($tag_id>0){
                $data_tag = array('video_id' => $video_id,
                    'tag_id' => $tag_id,
                    'tag_date' => date('Y-m-d h:i:s'),
                    'tag_ip' => getRealIP());
                $this->db->insert('video_tags', $data_tag);
            }
                }
            }
        }


        ///====attachment code========

        if ($this->input->post('video_type') == '1' || $this->input->post('video_type') == 1) {
            //===
            ///====video upload=====

            $global_path = base_path();
            // if the form was submitted process request if there is a file for uploading
            if ($_POST && array_key_exists("video_file", $_FILES)) {

                if ($_FILES['video_file']['name'] != '') {

                    //$uploaddir is for videos before conversion
                    $uploaddir = $global_path . 'upload/video_orig/';
                    //$live_dir is for videos after converted to mp4
                    $live_dir = $global_path . 'upload/video/';
                    //$live_img is for the first frame thumbs.
                    $live_img = $global_path . 'upload/video_thumb/';
                    //$live_img is for the first frame medium.
                    $live_mid_img = $global_path . 'upload/video_medium/';

                    
                    
                    
                   
            $pathFileinfo = pathinfo($_FILES['video_file']['name']);
            $ext = ($pathFileinfo['extension']);  
            $mainfilename = $_FILES['video_file']['name'];
            $baseFilename  = preg_replace('/[^A-Za-z0-9\-]/', '',strtolower(str_replace(".".$ext, "",basename($mainfilename))));
            $video_file_name= $baseFilename.".".$ext;
        
                    
                  
                    
                    $seed = rand(1, 2009) * rand(1, 10);

                    $upload = $seed . '_video_' . $video_id . $video_file_name;
                    $uploadfile = $uploaddir . $upload;


                    $safe_file = $this->checkfile($_FILES['video_file']);
                    if ($safe_file['safe'] == 1) {
                        if (move_uploaded_file($_FILES['video_file']['tmp_name'], $uploadfile)) {

                            //echo "File is valid, and was successfully uploaded.<br/>";
                            $base = basename($uploadfile, $safe_file['ext']);

                            $new_file = $base . 'mp4';
                            $new_image = $base . 'jpg';

                            $new_image_path = $live_img . $new_image;
                            $new_image_mid_path = $live_mid_img . $new_image;
                            $new_flv = $live_dir . $new_file;

                            //ececute ffmpeg generate flv
                           
			//$cmd='/usr/bin/ffmpeg -y -i '.$uploadfile.' -vcodec mpeg4 -r 30 -b:v 32k -acodec aac -ab 128k -ar 44100 -ac 2 -strict -2 -s 528x297 '.$new_flv.' 2>&1';
                        $final_with_bad_qalu_cmd='/usr/bin/ffmpeg -y -i '.$uploadfile.' -vcodec libx264 -pass 2 -q 26 -r 30 -b:v 32k -q:v 1 -acodec aac -ab 128k -ar 48000 -ac 2 -strict -2 '.$new_flv.' 2>&1';
                            
                            $new_high_cmd='/usr/bin/ffmpeg -y -i '.$uploadfile.' -vcodec libx264 -q:v 1 -acodec aac -ab 128k -ar 22050 -qscale .1 -ac 2 -strict -2 '.$new_flv.' 2>&1';
					 
                            $cmd='/usr/bin/ffmpeg -y -i '.$uploadfile.' -vcodec libx264 -b:v 700k -r 25 -acodec aac -strict -2 -b:a 96k -ar 44100 '.$new_flv.' 2>&1';
					  
                            
                            shell_exec($cmd);	   
                            
                            
                            

                            //execute ffmpeg and create thumb
                            shell_exec('/usr/bin/ffmpeg  -i ' . $uploadfile . ' -f mjpeg -vframes 1 -s 500x435 -an ' . $new_image_path . '');

                            //execute ffmpeg and create medium
                            shell_exec('/usr/bin/ffmpeg  -i ' . $uploadfile . ' -f mjpeg -vframes 1 -s 528x297 -an ' . $new_image_mid_path . '');



                            $data_gallery = array(
                                'video_id' => $video_id,
                                'attachment_is_main' => 1,
                                'attachment_file' => $new_file,
                                'attachment_type' => 1,
                                'attachment_order' => 1
                            );
                            $this->db->insert('video_attachment', $data_gallery);

                            $data_gallery = array(
                                'video_id' => $video_id,
                                'attachment_is_main' => 1,
                                'attachment_name' => $new_image,
                                'attachment_type' => 1,
                                'attachment_order' => 1
                            );
                            $this->db->insert('video_attachment', $data_gallery);
                        } else {
                            //echo "Possible file upload attack!\n";
                            //print_r($_FILES); die;
                        }
                    } else {

                        //echo 'Invalid File Type Please Try Again. You file must be of type .mpg, .wma, .mov, .flv, .mp4, .avi, .qt, .wmv, .rm'; die;
                    }
                }
            }

            ///===video image upload==========
            $image_settings = image_setting();

            $this->load->library('upload');
            if ($_FILES['video_image']) {
                if ($_FILES['video_image']['name'] != '') {


                    $rand = rand(0, 100000);

                    $_FILES['userfile']['name'] = $_FILES['video_image']['name'];
                    $_FILES['userfile']['type'] = $_FILES['video_image']['type'];
                    $_FILES['userfile']['tmp_name'] = $_FILES['video_image']['tmp_name'];
                    $_FILES['userfile']['error'] = $_FILES['video_image']['error'];
                    $_FILES['userfile']['size'] = $_FILES['video_image']['size'];


                    $config['file_name'] = $rand . '_video_' . $video_id;
                    $config['upload_path'] = base_path() . 'upload/video_medium/';
                    //$config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
                    $config['allowed_types'] = '*';

                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload()) {
                        $error = $this->upload->display_errors();
                    }


                    $picture = $this->upload->data();
                    $image = $picture['file_name'];


                    $this->load->library('image_lib');
                    $this->image_lib->clear();

                    $gd_var = 'gd2';

                    if ($_FILES['video_image']['type'] != "image/png" and $_FILES['video_image']['type'] != "image/x-png") {
                        $gd_var = 'gd2';
                    }

                    if ($_FILES['video_image']['type'] != "image/gif") {
                        $gd_var = 'gd2';
                    }

                    if ($_FILES['video_image']['type'] != "image/jpeg" and $_FILES['video_image']['type'] != "image/pjpeg") {
                        $gd_var = 'gd2';
                    }

                    $this->image_lib->initialize(array(
                        'image_library' => $gd_var,
                        'thumb_marker' => "",
                        'create_thumb' => TRUE,
                        'master_dim' => 'width',
                        'maintain_ratio' => TRUE,
                        'quality' => '100%',
                        'width' => $image_settings->design_width,
                        'height' => $image_settings->design_height,
                        'source_image' => base_path() . 'upload/video_medium/' . $picture['file_name'],
                        'new_image' => base_path() . 'upload/video_thumb/' . $picture['file_name']
                    ));
                    if (!$this->image_lib->resize()) {
                        $error = $this->image_lib->display_errors();
                    }
                    $this->image_lib->clear();



                    $data_gallery = array(
                        'video_id' => $video_id,
                        'attachment_is_main' => 1,
                        'attachment_name' => $image,
                        'attachment_type' => 1,
                        'attachment_order' => 1
                    );
                    $this->db->insert('video_attachment', $data_gallery);
                }
            }

            //===end coding
            //==
        } elseif ($this->input->post('video_type') == '2' || $this->input->post('video_type') == 2) {
            $video_url = $this->input->post('video_url');
            if ($video_url != '') {
                $data_gallery = array(
                    'video_id' => $video_id,
                    'attachment_is_main' => 1,
                    'attachment_url' => $video_url,
                    'attachment_type' => 2,
                    'attachment_order' => 1
                );
                $this->db->insert('video_attachment', $data_gallery);
            }
        }


        ///===end attahcment
    }

    function update_video() {
        //$video_slug = get_video_slug($this->input->post('video_title'), 0);
         $video_slug = clean_url($this->input->post('video_title'));
        $video_id = $this->input->post('video_id');
        $video_slug = $video_slug . '-' . $video_id;


        $new_video_title = BadWords($this->input->post('video_title'));
        $video_title = $new_video_title['str'];

        $new_video_content = BadWords($this->input->post('video_content'));
        $video_content = $new_video_content['str'];

        $new_video_meta_keyword = BadWords($this->input->post('video_meta_keyword'));
        $video_meta_keyword = $new_video_meta_keyword['str'];

        $new_video_meta_description = BadWords($this->input->post('video_meta_description'));
        $video_meta_description = $new_video_meta_description['str'];
        
        
        $video_is_modified=$this->input->post('video_is_modified');
        $parent_video_id=$this->input->post('parent_video_id');
        
        if($video_is_modified==0 || $video_is_modified==''){
            $parent_video_id=0;
        }
        
        
        $video_status=0;
        
        $get_cat_detail=get_category_obj_by_id('video',$this->input->post('video_category'));
        
        if($get_cat_detail){
            if($get_cat_detail->category_status==1){
                $video_status=1;
            }
        }
        

        $data = array('video_title' => $video_title,
            'video_is_modified' => $video_is_modified,
            'parent_video_id' => $parent_video_id,
            'video_slug' => $video_slug,
            'video_content' => $video_content,
            'video_point' => $this->input->post('video_point'),            
            'video_meta_keyword' => $video_meta_keyword,
            'video_meta_description' => $video_meta_description,
            'comment_allow' => $this->input->post('comment_allow'),
            'video_type' => $this->input->post('video_type'),
            'user_id' => get_authenticateUserID(),
            'video_date' => date('Y-m-d h:i:s'),
            'video_ip' => getRealIP(),
            'video_status' => $video_status,
            'comment_allow' => $this->input->post('comment_allow')
        );

        $this->db->where('video_id', $video_id);
        $this->db->update('video', $data);

        /* Insert data in design_category_rel */
        if($get_cat_detail){
        $data_video_category_rel = array('category_id' => $this->input->post('video_category'),
            'video_id' => $video_id);
        $this->db->where('video_id', $video_id);
        $this->db->update('video_category_rel', $data_video_category_rel);
        }

        /* Insert Tags */
        //Delete design tag
        $delete_tag = $this->db->query("delete from " . $this->db->dbprefix('video_tags') . " where video_id='" . $video_id . "'");

        $tags = explode(",", $this->input->post('tag_name'));
        if ($tags) {
            for ($i = 0; $i < count($tags); $i++) {
                $tag_name=trim(strip_tags($tags[$i]));
                if($tag_name!=''){
                $tag_id = check_tag($tag_name);
               if($tag_id>0){
                $data_tag = array('video_id' => $video_id,
                    'tag_id' => $tag_id,
                    'tag_date' => date('Y-m-d h:i:s'),
                    'tag_ip' => getRealIP());
                $this->db->insert('video_tags', $data_tag);
            }
                }
            }
        }





        ///====attachment code========

        if ($this->input->post('video_type') == '1' || $this->input->post('video_type') == 1) {
            //===
            ///====video upload=====

            $global_path = base_path();
            // if the form was submitted process request if there is a file for uploading
            if ($_POST && array_key_exists("video_file", $_FILES)) {
                if ($_FILES['video_file']['name'] != '') {
                    //$uploaddir is for videos before conversion
                    $uploaddir = $global_path . 'upload/video_orig/';
                    //$live_dir is for videos after converted to mp4
                    $live_dir = $global_path . 'upload/video/';
                    //$live_img is for the first frame thumbs.
                    $live_img = $global_path . 'upload/video_thumb/';
                    //$live_img is for the first frame medium.
                    $live_mid_img = $global_path . 'upload/video_medium/';
                    
                    
                    
                    
                     $pathFileinfo = pathinfo($_FILES['video_file']['name']);
                    $ext = ($pathFileinfo['extension']);  
                    $mainfilename = $_FILES['video_file']['name'];
                    $baseFilename  = preg_replace('/[^A-Za-z0-9\-]/', '',strtolower(str_replace(".".$ext, "",basename($mainfilename))));
                    $video_file_name= $baseFilename.".".$ext;
            
            
                    $seed = rand(1, 2009) * rand(1, 10);

                    $upload = $seed . '_video_' . $video_id . $video_file_name;
                    $uploadfile = $uploaddir . $upload;


                    $safe_file = $this->checkfile($_FILES['video_file']);
                    if ($safe_file['safe'] == 1) {
                        if (move_uploaded_file($_FILES['video_file']['tmp_name'], $uploadfile)) {

                            //echo "File is valid, and was successfully uploaded.<br/>";
                            $base = basename($uploadfile, $safe_file['ext']);

                            $new_file = $base . 'mp4';
                            $new_image = $base . 'jpg';

                            $new_image_path = $live_img . $new_image;
                            $new_image_mid_path = $live_mid_img . $new_image;
                            $new_flv = $live_dir . $new_file;
                            
                            
                            //ececute ffmpeg generate flv
                            //$cmd='/usr/bin/ffmpeg -y -i '.$uploadfile.' -vcodec mpeg4 -r 30 -b:v 32k -acodec aac -ab 128k -ar 44100 -ac 2 -strict -2 -s 528x297 '.$new_flv.' 2>&1';
                            
                            $final_with_bad_qalu_cmd='/usr/bin/ffmpeg -y -i '.$uploadfile.' -vcodec libx264 -pass 2 -q 26 -r 30 -b:v 32k -q:v 1 -acodec aac -ab 128k -ar 48000 -ac 2 -strict -2 '.$new_flv.' 2>&1';
                            
                            $new_high_cmd='/usr/bin/ffmpeg -y -i '.$uploadfile.' -vcodec libx264 -q:v 1 -acodec aac -ab 128k -ar 22050 -qscale .1 -ac 2 -strict -2 '.$new_flv.' 2>&1';
					 
                            $cmd='/usr/bin/ffmpeg -y -i '.$uploadfile.' -vcodec libx264 -b:v 700k -r 25 -acodec aac -strict -2 -b:a 96k -ar 44100 '.$new_flv.' 2>&1';
				
					  
					   
                            shell_exec($cmd);	   
                            
                            
                            

                            //execute ffmpeg and create thumb
                            shell_exec('/usr/bin/ffmpeg  -i ' . $uploadfile . ' -f mjpeg -vframes 1 -s 500x435 -an ' . $new_image_path . '');

                            //execute ffmpeg and create medium
                            shell_exec('/usr/bin/ffmpeg  -i ' . $uploadfile . ' -f mjpeg -vframes 1 -s 528x297 -an ' . $new_image_mid_path . '');





                            $this->delete_video_attachment($video_id);





                            $data_gallery = array(
                                'video_id' => $video_id,
                                'attachment_is_main' => 1,
                                'attachment_file' => $new_file,
                                'attachment_type' => 1,
                                'attachment_order' => 1
                            );
                            $this->db->insert('video_attachment', $data_gallery);

                            $data_gallery = array(
                                'video_id' => $video_id,
                                'attachment_is_main' => 1,
                                'attachment_name' => $new_image,
                                'attachment_type' => 1,
                                'attachment_order' => 1
                            );
                            $this->db->insert('video_attachment', $data_gallery);
                        } else {
                            //echo "Possible file upload attack!\n";
                            //print_r($_FILES); die;
                        }
                    } else {

                        //echo 'Invalid File Type Please Try Again. You file must be of type .mpg, .wma, .mov, .flv, .mp4, .avi, .qt, .wmv, .rm'; die;
                    }
                }
            }

            ///===video image upload==========
            $image_settings = image_setting();



            $this->load->library('upload');
            if ($_FILES['video_image']) {
                if ($_FILES['video_image']['name'] != '') {


                    $rand = rand(0, 100000);

                    $_FILES['userfile']['name'] = $_FILES['video_image']['name'];
                    $_FILES['userfile']['type'] = $_FILES['video_image']['type'];
                    $_FILES['userfile']['tmp_name'] = $_FILES['video_image']['tmp_name'];
                    $_FILES['userfile']['error'] = $_FILES['video_image']['error'];
                    $_FILES['userfile']['size'] = $_FILES['video_image']['size'];


                    $config['file_name'] = $rand . '_video_' . $video_id;
                    $config['upload_path'] = base_path() . 'upload/video_medium/';
                    //$config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
                    $config['allowed_types'] = '*';

                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload()) {
                        $error = $this->upload->display_errors();
                    }


                    $picture = $this->upload->data();
                    $image = $picture['file_name'];


                    $this->load->library('image_lib');
                    $this->image_lib->clear();

                    $gd_var = 'gd2';

                    if ($_FILES['video_image']['type'] != "image/png" and $_FILES['video_image']['type'] != "image/x-png") {
                        $gd_var = 'gd2';
                    }

                    if ($_FILES['video_image']['type'] != "image/gif") {
                        $gd_var = 'gd2';
                    }

                    if ($_FILES['video_image']['type'] != "image/jpeg" and $_FILES['video_image']['type'] != "image/pjpeg") {
                        $gd_var = 'gd2';
                    }

                    $this->image_lib->initialize(array(
                        'image_library' => $gd_var,
                        'thumb_marker' => "",
                        'create_thumb' => TRUE,
                        'master_dim' => 'width',
                        'maintain_ratio' => TRUE,
                        'quality' => '100%',
                        'width' => $image_settings->design_width,
                        'height' => $image_settings->design_height,
                        'source_image' => base_path() . 'upload/video_medium/' . $picture['file_name'],
                        'new_image' => base_path() . 'upload/video_thumb/' . $picture['file_name']
                    ));
                    if (!$this->image_lib->resize()) {
                        $error = $this->image_lib->display_errors();
                    }
                    $this->image_lib->clear();


                    
                    if($this->input->post('prev_video_image')!=''){
                        
                        $get_video_image_record=$this->delete_video_image_record($video_id,1,$this->input->post('prev_video_image'));
                        
                    }
                    


                    $data_gallery = array(
                        'video_id' => $video_id,
                        'attachment_is_main' => 1,
                        'attachment_name' => $image,
                        'attachment_type' => 1,
                        'attachment_order' => 1
                    );
                    $this->db->insert('video_attachment', $data_gallery);
                }
            }

            //===end coding
            //==
        } elseif ($this->input->post('video_type') == '2' || $this->input->post('video_type') == 2) {
            $video_url = $this->input->post('video_url');
            if ($video_url != '') {
                $data_gallery = array(
                    'video_id' => $video_id,
                    'attachment_is_main' => 1,
                    'attachment_url' => $video_url,
                    'attachment_type' => 2,
                    'attachment_order' => 1
                );
                $this->db->insert('video_attachment', $data_gallery);
            }
        }


        ///===end attahcment
    }
    
    
    
     function delete_video_image_record($video_id, $video_type,$image_name) {
        $this->db->select('*');
        $this->db->from('video_attachment');
        $this->db->where('video_id', $video_id);      
        $this->db->where('attachment_type', $video_type);
        $this->db->where('attachment_name',$image_name);
      

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();

            $get_video_image = $result->attachment_name;
            
             if ($get_video_image != '') {
                if (file_exists(base_path() . 'upload/video_medium/' . $get_video_image)) {
                    unlink(base_path() . 'upload/video_medium/' . $get_video_image);
                }

                if (file_exists(base_path() . 'upload/video_thumb/' . $get_video_image)) {
                    unlink(base_path() . 'upload/video_thumb/' . $get_video_image);
                }
            }
            
       $delete_attach = $this->db->query("delete from " . $this->db->dbprefix('video_attachment') . " where attachment_id='" . $result->attachment_id . "'");
                        
                        
        }
       
    }

    function delete_video_attachment($video_id) {

        $video_attach = $this->db->query("select * from " . $this->db->dbprefix('video_attachment') . " where video_id='" . $video_id . "'");

        if ($video_attach->num_rows() > 0) {


            $video_atd = $video_attach->result();

            if ($video_atd) {
                foreach ($video_atd as $vd) {
                    if ($vd->attachment_name != '') {
                        if (file_exists(base_path() . 'upload/video_medium/' . $vd->attachment_name)) {
                            unlink(base_path() . 'upload/video_medium/' . $vd->attachment_name);
                        }

                        if (file_exists(base_path() . 'upload/video_thumb/' . $vd->attachment_name)) {
                            unlink(base_path() . 'upload/video_thumb/' . $vd->attachment_name);
                        }
                    }


                    if ($vd->attachment_file != '') {
                        if (file_exists(base_path() . 'upload/video_orig/' . $vd->attachment_file)) {
                            unlink(base_path() . 'upload/video_orig/' . $vd->attachment_file);
                        }

                        if (file_exists(base_path() . 'upload/video/' . $vd->attachment_file)) {
                            unlink(base_path() . 'upload/video/' . $vd->attachment_file);
                        }
                    }
                }
            }

            $delete_attach = $this->db->query("delete from " . $this->db->dbprefix('video_attachment') . " where video_id='" . $video_id . "'");
        }
    }

    function get_user_video($user_id, $limit, $video_id = 0) {
        $this->db->select('v.*,c.category_name');
        $this->db->from('video v');
        $this->db->join('video_category_rel dr', 'v.video_id=dr.video_id');
        $this->db->join('video_category c', 'c.category_id=dr.category_id');
        $this->db->where('v.user_id', $user_id);
        $this->db->where('v.video_status', '1');
        $this->db->where('v.video_id !=', $video_id);
        $this->db->order_by('v.video_id', 'desc');
        $this->db->limit($limit);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function get_popular_video($limit, $video_id = 0) {
        $this->db->select('v.*,c.category_name');
        $this->db->from('video v');

        $this->db->join('video_category_rel dr', 'v.video_id=dr.video_id');
        $this->db->join('video_category c', 'c.category_id=dr.category_id', 'LEFT');
        $this->db->where('v.video_status', '1');
        //$this->db->where('v.user_id',$user_id);
        if($video_id>0){
            $this->db->where('v.video_id !=', $video_id);
        }
        $this->db->order_by('video_id', 'video_view_count');
        $this->db->limit($limit);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function get_total_user_video_count($user_id) {
        $this->db->select('v.*,c.category_name');
        $this->db->from('video v');
        $this->db->join('video_category_rel dr', 'v.video_id=dr.video_id');
        $this->db->join('video_category c', 'c.category_id=dr.category_id', 'LEFT');
        $this->db->where('v.user_id', $user_id);
        $this->db->order_by('video_id', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return 0;
    }

    function get_video_result($user_id, $limit, $offset) {

        $this->db->select('v.*,c.category_name');
        $this->db->from('video v');
        $this->db->join('video_category_rel dr', 'v.video_id=dr.video_id');
        $this->db->join('video_category c', 'c.category_id=dr.category_id', 'LEFT');
        $this->db->where('v.user_id', $user_id);
        $this->db->order_by('video_id', 'desc');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function get_one_video($video_id) {
        $this->db->select('d.*,dr.category_id');
        $this->db->from('video d');
        $this->db->join('video_category_rel dr', 'd.video_id=dr.video_id');
        //$this->db->join('design_category c','c.category_id=dr.category_id');
        $this->db->where('d.video_id', $video_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return 0;
        }
    }

    function get_one_video_tag($video_id) {
        $this->db->select('tg.video_tags_id,t.tag_name,t.tag_id');
        $this->db->from('video_tags tg');
        $this->db->join('tags t', 't.tag_id=tg.tag_id');
        $this->db->where('tg.video_id', $video_id);
        $this->db->group_by('tg.tag_id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            //$query=$this->db->get_where('design_tags',array('design_id'=>$design_id));
            return $query->result();
        }
    }

    function get_total_video_count() {
        $this->db->select('d.*,c.category_name');
        $this->db->from('video d');
        $this->db->join('video_category_rel dr', 'd.video_id=dr.video_id');
        $this->db->join('video_category c', 'c.category_id=dr.category_id', 'LEFT');
        $this->db->order_by('d.video_id', 'desc');
        //$this->db->limit($limit,$offset);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return 0;
    }

    function get_all_video_result($limit, $offset) {
        $this->db->select('d.*,c.category_name');
        $this->db->from('video d');
        $this->db->join('video_category_rel dr', 'd.video_id=dr.video_id');
        $this->db->join('video_category c', 'c.category_id=dr.category_id', 'LEFT');
        $this->db->order_by('d.video_id', 'desc');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function get_total_category_video_count($category_id, $keyword) {
        $this->db->select('d.*,c.category_name,u.*,up.*');
        $this->db->from('video d');
        $this->db->join('user u','d.user_id=u.user_id',"LEFT");
	$this->db->join('user_profile up','u.user_id=up.user_id',"LEFT");
        $this->db->join('video_category_rel dr', 'd.video_id=dr.video_id');
        $this->db->join('video_category c', 'c.category_id=dr.category_id', 'LEFT');
        $this->db->where('d.video_status',1);
        
        $this->db->order_by('d.video_id', 'desc');
        if ($category_id > 0 && $category_id!='') {
            
            $final_category='';
            if(strstr($category_id,'-')){
                
                $exp_cat=explode('-',$category_id);
                
                if(!empty($exp_cat)){
                    foreach($exp_cat as $cat_id){
                        if($cat_id>0){
                         
                          $final_category.=$this->get_tree_category($cat_id);
                          
                          $final_category.=$cat_id.'-';
                          
                        }
                    }
                    
                    $final_category=substr($final_category, 0,-1);
                }
                
            } else {
                $final_category.=$this->get_tree_category($category_id);
                $final_category.=$category_id.'-';
                
                $final_category=substr($final_category, 0,-1);
            }
            
            if($final_category!=''){
                $cond = "dr.category_id in (" . str_replace("-", ",", $final_category) . ")";
                $this->db->where($cond);
            }
        }
        if ($keyword != '' && $keyword != "0") {
            $this->db->like('d.video_title', $keyword);

            if (substr_count($keyword, ' ') >= 1) {
                $ex = explode(' ', $keyword);

                foreach ($ex as $val) {
                    $this->db->like('d.video_title', $val);
                }
            }
        }
        //$this->db->limit($limit,$offset);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return 0;
    }

    function get_category_video_result($limit, $offset, $category_id, $keyword) {
        $this->db->select('d.*,c.category_name,u.*,up.*');
        $this->db->from('video d');
        $this->db->join('user u','d.user_id=u.user_id',"LEFT");
	$this->db->join('user_profile up','u.user_id=up.user_id',"LEFT");
        $this->db->join('video_category_rel dr', 'd.video_id=dr.video_id');
        $this->db->join('video_category c', 'c.category_id=dr.category_id', 'LEFT');
         $this->db->where('d.video_status',1);
        $this->db->order_by('d.video_id', 'desc');
        
        
        
        if ($category_id > 0 && $category_id!='') {
            
            $final_category='';
            if(strstr($category_id,'-')){
                
                $exp_cat=explode('-',$category_id);
                
                if(!empty($exp_cat)){
                    foreach($exp_cat as $cat_id){
                        if($cat_id>0){
                         
                          $final_category.=$this->get_tree_category($cat_id);
                          
                          $final_category.=$cat_id.'-';
                          
                        }
                    }
                    
                    $final_category=substr($final_category, 0,-1);
                }
                
            } else {
                $final_category.=$this->get_tree_category($category_id);
                $final_category.=$category_id.'-';
                
                $final_category=substr($final_category, 0,-1);
            }
            
            if($final_category!=''){
                $cond = "dr.category_id in (" . str_replace("-", ",", $final_category) . ")";
                $this->db->where($cond);
            }
        }
        
        
        if ($keyword != '' && $keyword != "0") {
            $this->db->like('d.video_title', $keyword);

            if (substr_count($keyword, ' ') >= 1) {
                $ex = explode(' ', $keyword);

                foreach ($ex as $val) {
                    $this->db->like('d.video_title', $val);
                }
            }
        }
        $this->db->limit($limit, $offset);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    
        
    function get_tree_category($category_id){
        
        $temp = '';
        $chk_parent = $this->db->query("select category_id,category_parent_id from " . $this->db->dbprefix('video_category') . " where category_status=1 and category_parent_id='" . $category_id . "'");

        if ($chk_parent->num_rows() > 0) {
            $res = $chk_parent->result();
            
            foreach ($res as $rs) {

                $temp.=$rs->category_id. "-";


                    $chk_parent2 = $this->db->query("select category_id from " . $this->db->dbprefix('video_category') . " where category_status=1 and category_parent_id='" . $rs->category_id . "'");

                    if ($chk_parent2->num_rows() > 0) {
                        $temp.=$this->get_tree_category($rs->category_id);
                    }
                
            }
        }


        return $temp;
    }

    function video_buy_process($video_id) {


        $cur_date = date('Y-m-d H:i:s');
        $user_ip = getRealIP();  ///===use custom helper function  getrealIP
        //$picture_sql=$this->db->query(("SELECT * FROM ".$this->db->dbprefix('iweb_design')." where design_id=".$design_id);

        $this->db->select('*');
        $this->db->from($this->db->dbprefix('video'));
        $this->db->where('video_id', $video_id);
        $picture_sql = $this->db->get();

            
            

        if ($picture_sql->num_rows() > 0) {
            $video_detail = $picture_sql->row();

            //echo "image points <br /><br />";

            $video_points = $video_detail->video_point;
            $picture_owner_id = $video_detail->user_id;

            //		echo "<br /><br />";

            $cur_login_user_id = get_authenticateUserID(); ///==replace with session user id
            //==check balance of user
            $user_cur_balance = getuserpoints($cur_login_user_id);

            if ($user_cur_balance >= $image_points) {

                ///===buyer user debit from wallet
                $data_trans = array('user_id' => $cur_login_user_id,
                    'video_id' => $video_id,
                    'pay_points' => $video_points,
                    'host_ip' => $user_ip,
                    'transaction_date_time' => $cur_date
                );
                $this->db->insert('transaction', $data_trans);

                //$do_transaction=mysql_query("insert into iweb_transaction(user_id,design_id,pay_points,host_ip,transaction_date_time)values('".$cur_login_user_id."','".$design_id."','".$image_points."','".$user_ip."','".$cur_date."')");

                $transaction_id = mysql_insert_id();

                //$do_wallet_transaction=mysql_query("insert into iweb_wallet(user_id,design_id,debit,transaction_id,wallet_ip,wallet_date)values('".$cur_login_user_id."','".$design_id."','".$image_points."','".$transaction_id."','".$user_ip."','".$cur_date."')");

                $data_wallet = array('user_id' => $cur_login_user_id,
                    'video_id' => $video_id,
                    'debit' => $video_points,
                    'transaction_id' => $transaction_id,
                    'wallet_ip' => $user_ip,
                    'wallet_date' => $cur_date
                );
                $this->db->insert('wallet', $data_wallet);
                ///===end buying
                //====get user tree for recursion payment
                $files_user_ids = array();

                if ($image_points > 0) {
                    if ($picture_detail->parent_video_id > 0 && $picture_detail->video_is_modified == 1) {

                        $files_user_ids = get_all_picture_tree($picture_detail->parent_video_id);
                    }
                }

                //echo "files_user_ids <br /><br />";
                //print_r($files_user_ids); //die;
                ///===add current design user id
                $all_ids[] = $picture_owner_id;
                $final_ids = array_merge($all_ids, $files_user_ids);
                ///===total users id===
                //echo "<br /><br />total users ids<br /><br />";
                //print_r($final_ids);
                ///====commission settings
                $admin_video_point_commission = 20; ///in %
                $commission_setting = commission_setting();

                if (!empty($commission_setting)) {
                    $admin_video_point_commission = $commission_setting->admin_commission_video;
                }

                $user_total_point_commission = 100 - $admin_video_point_commission;


                ///=====assign and split percentage to user
                //echo "total pay to users<br /><br />";
                $pay_to_user = splitampunt($user_total_point_commission, $final_ids);
                //print_r($pay_to_user);
                ////====total pay to individual users=
                //echo "total pay to all individual users not admin<br /><br />";
                $final_pay_to_user = $pay_to_user;
                //print_r($final_pay_to_user);  ///die;
                //====count admin points===
                $admin_points = countpoints($image_points, $admin_video_point_commission);

                foreach ($final_pay_to_user as $user_id => $user_perc) {

                    $user_video_points = countpoints($image_points, $user_perc);


                    //====insert into db=====
                    //$check_user_exit=mysql_query("select * from iweb_user where user_id=".$user_id);
                    $this->db->select('*');
                    $this->db->from('user');
                    $this->db->where('user_id', $user_id);
                    $check_user_exit = $this->db->get();

                    if ($check_user_exit->num_rows() > 0) {

                        //$do_transaction=mysql_query("insert into iweb_transaction(parent_transaction_id,user_id,design_id,pay_points,host_ip,transaction_date_time)values('".$transaction_id."','".$user_id."','".$design_id."','".$user_image_points."','".$user_ip."','".$cur_date."')");
                        $do_transaction = array('parent_transaction_id' => $transaction_id,
                            'user_id' => $user_id,
                            'video_id' => $video_id,
                            'pay_points' => $user_video_points,
                            'host_ip' => $user_ip,
                            'transaction_date_time' => $cur_date
                        );
                        $this->db->insert('transaction', $do_transaction);

                        $last_transaction_id = mysql_insert_id();

                        //$do_wallet_transaction=mysql_query("insert into iweb_wallet(user_id,design_id,credit,transaction_id,wallet_ip,wallet_date)values('".$user_id."','".$design_id."','".$user_image_points."','".$last_transaction_id."','".$user_ip."','".$cur_date."')");

                        $do_wallet_transaction = array('user_id' => $user_id,
                            'video_id' => $video_id,
                            'credit' => $user_video_points,
                            'transaction_id' => $last_transaction_id,
                            'wallet_ip' => $user_ip,
                            'wallet_date' => $cur_date
                        );
                        $this->db->insert('wallet', $do_wallet_transaction);
                    } else {
                        ///==if user not found than transaction points to admin
                        $user_id = $admin_id;
                        $admin_points = $admin_points + $user_video_points;
                    }
                } //===foreach users
                ///===pay to admin wallet====
                if ($admin_points != '') {
                    $is_point = 1;

                    //mysql_query("insert into iweb_wallet_admin(credit,is_point,wallet_date,transaction_id,design_id)values('".$admin_points."','".$is_point."','".$cur_date."','".$transaction_id."','".$design_id."')");

                    $pay_admin_wallet_transaction = array('credit' => $admin_points,
                        'is_point' => $is_point,
                        'wallet_date' => $cur_date,
                        'transaction_id' => $transaction_id,
                        'video_id' => $video_id
                    );
                    $this->db->insert('wallet_admin', $pay_admin_wallet_transaction);
                }///===pay to admin wallet====
                
                
                
                
                
                 ///===Notification to owner
        $data_notification = array('to_user_id' => $picture_owner_id,
            'video_id' => $video_id,
            'act' => 'YOUR_VIDEO_BUY',
            'is_read' => 1,
            'notification_date' => $cur_date
        );
        $this->db->insert('system_notification', $data_notification);
        
        
          $owner_user_info = get_user_profile_by_id($picture_owner_id);
          
          
          
            // owner
           $email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Video Owner Purchase Notification'");
           $email_temp=$email_template->row();				

           $email_from_name=$email_temp->from_name;

           $email_address_from=$email_temp->from_address;
           $email_address_reply=$email_temp->reply_address;

           $email_subject=$email_temp->subject;				
           $email_message=$email_temp->message;

            $email_to = $owner_user_info->email;



        $email_message=str_replace('{break}','<br/>',$email_message);
        $email_message=str_replace('{user_name}',$owner_user_info->full_name,$email_message);
        $email_message=str_replace('{video_title}',$video_detail->video_title,$email_message);
        $email_message=str_replace('{video_point}',$video_detail->video_point,$email_message);        
        
      

        $str=$email_message;

        /** custom_helper email function **/

        email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
                
            
        
        
        
        ///===Notification to buyer
         $data_notification = array('to_user_id' => get_authenticateUserID(),
                'video_id' => $video_id,
                'act' => 'VIDEO_BUY',
                'is_read' => 1,
                'notification_date' => date("Y-m-d H:i:s")
            );
            $this->db->insert('system_notification', $data_notification);
            
       
                
            $buyer_user_info = get_user_profile_by_id(get_authenticateUserID());
            
            
            
           $download_link=site_url('user/videodownload');
            
            
            
            // buyer
           $email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Video Purchase Notification'");
           $email_temp=$email_template->row();				

           $email_from_name=$email_temp->from_name;

           $email_address_from=$email_temp->from_address;
           $email_address_reply=$email_temp->reply_address;

           $email_subject=$email_temp->subject;				
           $email_message=$email_temp->message;

            $email_to = $buyer_user_info->email;



        $email_message=str_replace('{break}','<br/>',$email_message);
        $email_message=str_replace('{user_name}',$buyer_user_info->full_name,$email_message);
        $email_message=str_replace('{video_title}',$video_detail->video_title,$email_message);
        $email_message=str_replace('{video_point}',$video_detail->video_point,$email_message);
        $email_message=str_replace('{download_link}',$download_link,$email_message);
      

        $str=$email_message;

        /** custom_helper email function **/

        email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
                //// *************** end ******************/////	
            

                
                
                
                
                return 1;
            } //==if balance
            else {
                echo "NO_BALANCE";
            }
        } //===if design get
    }

    function chk_track_view($design_id, $user_id) {
        $cur_date = date('Y-m-d');

        $query = $this->db->query("select * from " . $this->db->dbprefix('design_view') . " where design_id = " . $design_id . " and user_id = " . $user_id . " and DATE_FORMAT(view_date,'%Y-%m-%d') = '" . $cur_date . "'");

        if ($query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function chk_video_track_view($video_id, $user_id) {
        $cur_date = date('Y-m-d');

        $query = $this->db->query("select * from " . $this->db->dbprefix('video_view') . " where video_id = " . $video_id . " and user_id = " . $user_id . " and DATE_FORMAT(view_date,'%Y-%m-%d') = '" . $cur_date . "'");

        if ($query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function set_video_view($video_id) {
        $user_id = 0;
        $chk_video_track_view = 1;
        if (get_authenticateUserID() > 0) {
            $user_id = get_authenticateUserID();
        }
        if ($user_id > 0) {
            $chk_video_track_view = $this->chk_video_track_view($video_id, $user_id);
        }
        if ($chk_video_track_view == 0) {

            $sql_res = "update " . $this->db->dbprefix('video') . " set `video_view_count` = `video_view_count` + 1 where video_id =" . $video_id;
            $this->db->query($sql_res);

            $data = array('video_id' => $video_id,
                'user_id' => $user_id,
                'view_date' => date('Y-m-d h:i:s'),
                'view_ip' => getRealIP()
            );
            $this->db->insert('video_view', $data);
        } else {


            $chk_track_other_view = $this->chk_track_other_view($video_id);

            if ($chk_track_other_view == 0) {
                $sql_res = "update " . $this->db->dbprefix('video') . " set `video_view_count` = `video_view_count` + 1 where video_id =" . $video_id;
                $this->db->query($sql_res);

                $data = array('video_id' => $video_id,
                    'user_id' => 0,
                    'view_date' => date('Y-m-d h:i:s'),
                    'view_ip' => getRealIP()
                );
                $this->db->insert('video_view', $data);
            }
        }
    }

    function check_like_track($user_id =0, $video_id = 0) {

        $query = $this->db->query("select * from " . $this->db->dbprefix('video_like') . " where video_id = " . $video_id . " and user_id = " . $user_id);

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return 0;
        }
    }

    function chk_track_other_view($video_id) {
        $cur_date = date('Y-m-d');

        $query = $this->db->query("select * from " . $this->db->dbprefix('video_view') . " where video_id = " . $video_id . " and view_ip = '" . getRealIP() . "' and DATE_FORMAT(view_date,'%Y-%m-%d') = '" . $cur_date . "'");

        if ($query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function check_like_status($like_status, $user_id, $video_id) {
        $query = $this->db->query("select * from " . $this->db->dbprefix('video_like') . " where video_id = " . $video_id . " and like_status = " . $like_status . " and user_id = " . $user_id);

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return 0;
        }
    }

    function set_like_dislike($like_status=0, $video_id = 0) {

        //$chk_track_like  = 1;
        //like

        $chk_like = $this->check_like_track(get_authenticateUserID(), $video_id);


        /// new code//
        if ($chk_like) {
            $data = array('video_id' => $video_id,
                'user_id' => get_authenticateUserID(),
                'like_date' => date('Y-m-d h:i:s'),
                'like_status' => $like_status,
                'like_ip' => getRealIP()
            );
            $this->db->where(array('user_id' => get_authenticateUserID(), 'video_id' => $video_id));
            $this->db->update('video_like', $data);


            if ($like_status == 1) {
                $sql_res = "update " . $this->db->dbprefix('video') . " set `video_like_count` = `video_like_count` + 1 where video_id =" . $video_id;
            } else {
                $sql_res = "update " . $this->db->dbprefix('video') . " set `video_like_count` = `video_like_count` - 1 where video_id =" . $video_id;
            }

            $this->db->query($sql_res);
        } else {

            $data = array('video_id' => $video_id,
                'user_id' => get_authenticateUserID(),
                'like_date' => date('Y-m-d h:i:s'),
                'like_status' => $like_status,
                'like_ip' => getRealIP()
            );
            $this->db->insert('video_like', $data);
            $sql_res = "update " . $this->db->dbprefix('video') . " set `video_like_count` = `video_like_count` + 1 where video_id =" . $video_id;
            $this->db->query($sql_res);
        }
        // end of new code//
    }
    
    
      ///==============video category part===
    
    
    
    function get_video_category_child_by_id($category_id){
        $query = $this->db->query("select * from " . $this->db->dbprefix('video_category') . " where (category_status=1 or (category_status=0 and category_user_id='".get_authenticateUserID()."') ) and category_parent_id='" . $category_id . "'");
        
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }
    

    function insert_video_category($category_name,$category_parent_id) {

       
        $category_url_name = clean_url($category_name);


        $chk_url_exists = $this->db->query("select MAX(category_url_name) as category_url_name from " . $this->db->dbprefix('video_category') . " where category_url_name like '" . $category_url_name . "%'");

        if ($chk_url_exists->num_rows() > 0) {
            $get_pr = $chk_url_exists->row();

            $strre = '0';
            if ($get_pr->category_url_name != '') {
                $strre = str_replace($category_url_name, '', $get_pr->category_url_name);
            }

            if ($strre == '0') {
                $newcnt = '';
            } elseif ($strre == '') {
                $newcnt = '1';
            } else {
                $newcnt = (int) $strre + 1;
            }

            $category_url_name = $category_url_name . $newcnt;
        }

        $data = array('category_name' => $category_name,
            'category_url_name' => $category_url_name,
            'category_parent_id' => $category_parent_id,
            'category_description' => $category_name,            
            'category_status' => 1,
            'category_user_id'=>get_authenticateUserID()
            
        );

        $this->db->insert('video_category', $data);
    }


}

?>