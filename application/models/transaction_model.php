<?php
class Transaction_model extends IWEB_Model 
{

	/*
	Function name :User_model
	Description :its default constuctor which called when user_model object initialzie.its load necesary parent constructor
	*/
	function Transaction_model()
    {
        parent::__construct();	
    } 
	
	function get_total_user_transaction_count($user_id)
	{
		$this->db->select('*');
		$this->db->from('transaction');
		$this->db->where('user_id',$user_id);
		$this->db->order_by('transaction_id','desc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_transaction_result($user_id,$limit,$offset)
	{

		$this->db->select('*');
		$this->db->from('transaction');
		$this->db->where('user_id',$user_id);
		$this->db->order_by('transaction_id','desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
}
?>