<?php
class Printer_model extends IWEB_Model 
{

    /*
    Function name :User_model
    Description :its default constuctor which called when user_model object initialzie.its load necesary parent constructor
    */
    function Printer_model()
    {
        parent::__construct();	
    } 
    
    function check_review_owner($store_id,$store_review_id,$user_id){
        
        $this->db->select('*');
        $this->db->from('store_review sr');   
        $this->db->join('store s','sr.store_id=s.store_id');
        $this->db->where('sr.store_id',$store_id);
        $this->db->where('sr.store_review_id',$store_review_id);
        
        if($user_id>0){
            $this->db->where('(sr.user_id='.$user_id.' OR s.user_id='.$user_id.')');
        }
        
        $query = $this->db->get();
        
       
        if ($query->num_rows() > 0) {
                return $query->row();
        }
        return false;
        
        
    }
    
    
    function place_review($parent_review_id){
        
        $new_review_desc=BadWords($this->input->post('review_desc'));
	$store_review_description = $new_review_desc['str'];
        
        
        $store_rating =(int) $this->input->post('rating'); 
        $store_review_ip=getRealIP();
        $store_review_date=date('Y-m-d h:i:s');
        $user_id=get_authenticateUserID();
        
        $store_id =(int) $this->input->post('store_id');
        $review_id =(int) $this->input->post('review_id');
        $preview_id =(int) $this->input->post('preview_id');
        
        $review_type=$this->input->post('review_type'); 
        
        $final_store_rating=0;
        
        
        if($review_id>0 ) {
            
            $data_review= array(                           
                'store_review_description'=>$store_review_description                        
                );
            $this->db->where('store_review_id',$review_id);
            $this->db->update('store_review',$data_review);
            
//            if($preview_id==0 && $store_rating>0){
//                
//                $final_store_rating=$this->get_store_avg_rating($store_id);
//                
//                $this->db->trans_start();
//                $sql_res = "update " . $this->db->dbprefix('store') . " set `store_total_rating` = ".$final_store_rating." where store_id =" . $store_id;
//                $this->db->query($sql_res);
//                $this->db->trans_complete();
//            }
            
        } else {
            $data_review= array(
                'parent_review_id'=>$preview_id,
                'store_id'=>$store_id,
                'user_id'=>$user_id,
                'store_rating'=>$store_rating,            
                'store_review_description'=>$store_review_description,
                'store_review_ip'=>$store_review_ip,
                'store_review_date'=>$store_review_date           
                );
            $this->db->insert('store_review',$data_review);
            
            if($preview_id==0){
                $final_store_rating=$this->get_store_avg_rating($store_id);
                $this->db->trans_start();
                $sql_res = "update " . $this->db->dbprefix('store') . " set `store_total_review` = `store_total_review` + 1,`store_total_rating` = ".$final_store_rating." where store_id =" . $store_id;
                $this->db->query($sql_res);
                $this->db->trans_complete();
            }
        } 
        
        
       
                        
                        
        
        ///===Notification
        $data_notification = array('to_user_id'=>get_authenticateUserID(),
        'store_id'=>$store_id,
        'act'=>'STORE_REVIEW_POST',
        'is_read'=>1,
        'notification_date'=>date("Y-m-d H:i:s")
        );
        $this->db->insert('system_notification',$data_notification);
        
        
         return 1;
                
                
    }
    
    
    function get_store_avg_rating($store_id)
    {
         $sql="SELECT AVG(store_rating) as final_store_rating from ".$this->db->dbprefix('store_review')." where parent_review_id=0  and store_id=".$store_id;
         
         $query = $this->db->query($sql);
        
        if($query->num_rows()>0){
        
            $result=$query->row();
            
            return $result->final_store_rating;
      
        }
        return 0;
      
      
    }
    
    function get_store_all_review_old($store_id,$limit,$offset){
        // select * FROM iweb_store_review r1 left join iweb_store_review r2 on r1.store_review_id=r2.parent_review_id  WHERE r1.store_id=1 group by r1.store_review_id order by r1.store_review_id asc, r2.parent_review_id  asc 
        
        
        
           $if_user = 'if(r1.user_id>0,(select CONCAT_WS("####",IFNULL(us.first_name,"blank"),IFNULL(us.last_name,"blank"),IFNULL(us.profile_name,"blank"),IFNULL(up.profile_image,"blank")) as user_full_name from '.$this->db->dbprefix('user us').' left join '.$this->db->dbprefix('user_profile up').' on us.user_id=up.user_id where us.user_id=r1.user_id and us.user_status=1),"guest") as user_profile_name';
      
           
          
        
      $fields='r1.*,'.$if_user;
      
      $sql='SELECT '.$fields;
      
      $sql.=" from ".$this->db->dbprefix('store_review r1');
      $sql.=' left join '.$this->db->dbprefix('store_review r2').' on r1.store_review_id=r2.parent_review_id ';
      
     
      
      $sql.=' where r1.store_id='.$store_id;
      
      $sql.=' group by r1.store_review_id';
      $sql.=' order by r1.store_review_id asc';
      
      $sql.=' limit '.$limit.' offset '.$offset;
      
        
      
        $query = $this->db->query($sql);
        
        if($query->num_rows()>0){
        
            $result=$query->result();
        
            $results=array();
            
            $previous_section = null;
            foreach($result as $row) {
            
                if ($row->store_review_id != $previous_section && $row->parent_review_id==0)
                {
                    $previous_section = $row->store_review_id;
                                       
                    $results[$row->store_review_id]['reviews']=$row;
                } else {                    
                    $results[$row->parent_review_id]['replys'][$row->store_review_id] = $row;
                }
            }
            //echo "<pre>";
            //print_r($results); die;
            return $results;
        } 
        return false;
                
    }
    
         
    function get_store_all_review($store_id,$limit,$offset){
        // select * FROM iweb_store_review r1 left join iweb_store_review r2 on r1.store_review_id=r2.parent_review_id  WHERE r1.store_id=1 group by r1.store_review_id order by r1.store_review_id asc, r2.parent_review_id  asc 
        
        
        
           $if_user = 'if(r1.user_id>0,(select CONCAT_WS("####",IFNULL(us.first_name,"blank"),IFNULL(us.last_name,"blank"),IFNULL(us.profile_name,"blank"),IFNULL(up.profile_image,"blank")) as user_full_name from '.$this->db->dbprefix('user us').' left join '.$this->db->dbprefix('user_profile up').' on us.user_id=up.user_id where us.user_id=r1.user_id and us.user_status=1),"guest") as user_profile_name';
      
           
          
        
      $fields='r1.*,'.$if_user;
      
      $sql='SELECT '.$fields;
      
      $sql.=" from ".$this->db->dbprefix('store_review r1');
     
     
      
      $sql.=' where r1.parent_review_id=0 and r1.store_id='.$store_id;
      
      
      
      $sql.=' group by r1.store_review_id';
      $sql.=' order by r1.store_review_id asc';
      
      $sql.=' limit '.$limit.' offset '.$offset;
      
        
      
        $query = $this->db->query($sql);
        
        if($query->num_rows()>0){
        
            $result=$query->result();       
            
            return $result;
        } 
        return false;
                
    }
    
        
    function get_store_review_reply($store_id,$parent_review_id){
      
        
           $if_user = 'if(r1.user_id>0,(select CONCAT_WS("####",IFNULL(us.first_name,"blank"),IFNULL(us.last_name,"blank"),IFNULL(us.profile_name,"blank"),IFNULL(up.profile_image,"blank")) as user_full_name from '.$this->db->dbprefix('user us').' left join '.$this->db->dbprefix('user_profile up').' on us.user_id=up.user_id where us.user_id=r1.user_id and us.user_status=1),"guest") as user_profile_name';
      
           
          
        
      $fields='r1.*,'.$if_user;
      
      $sql='SELECT '.$fields;
      
      $sql.=" from ".$this->db->dbprefix('store_review r1');
     
      
     
      
      $sql.=' where r1.parent_review_id='.$parent_review_id.' and r1.store_id='.$store_id;
      
      $sql.=' group by r1.store_review_id';
      $sql.=' order by r1.store_review_id asc';
      
    
      
        
      
        $query = $this->db->query($sql);
        
        if($query->num_rows()>0){
        
            $result=$query->result();
        
           
            return $result;
        } 
        return false;
                
    }
    
    
     
    
    
    function delete_store_review($store_id,$store_review_id){
         
        $this->db->select('*');
        $this->db->from('store_review sr');      
        $this->db->where('sr.store_id',$store_id);
        $this->db->where('sr.store_review_id',$store_review_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
                $res= $query->row();
                
                $this->db->trans_start();
                $delete_store_review = $this->db->query("delete from " . $this->db->dbprefix('store_review') . " where (store_review_id='" . $store_review_id . "' or  parent_review_id='" . $store_review_id . "'   )");
                $this->db->trans_complete();
                
                $parent_review_id=$res->parent_review_id;
                
                if($parent_review_id==0){
                    
                    if ($this->db->trans_status() === FALSE)
                    { } else {
                        
                        $final_store_rating=$this->get_store_avg_rating($store_id);
                        
                        $this->db->trans_start();
                        $sql_res = "update " . $this->db->dbprefix('store') . " set `store_total_review` = `store_total_review` - 1,`store_total_rating` = ".$final_store_rating." where store_id =" . $store_id;
                        $this->db->query($sql_res);
                        $this->db->trans_complete();
                    }
                    
                }
                
               
                
                
        }
        
        
        
            
    }
    
    
    function get_user_store_review_count($store_id)
	{
		$this->db->select('*');
		$this->db->from('store_review sr');
		$this->db->join('user u','sr.user_id=u.user_id',"LEFT");
                $this->db->join('user_profile up','u.user_id=up.user_id',"LEFT");
		$this->db->where('sr.store_id',$store_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_user_store_review($store_id,$limit,$offset)
	{
		$this->db->select('*');
		$this->db->from('store_review sr');
		$this->db->join('user u','sr.user_id=u.user_id',"LEFT");
                $this->db->join('user_profile up','u.user_id=up.user_id',"LEFT");
		$this->db->where('sr.store_id',$store_id);
		$this->db->order_by('sr.store_review_id','desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
		
	}	
    
    
    function get_printer_geo_result($distance_range,$postal_code='',$visitor_lat='',$visitor_lon='')
    {
        
        $visitor_postal_code=$this->session->userdata('visitor_user_postal_code');
    
        $do_postal=0;
        if($visitor_postal_code!=$postal_code && $postal_code!='') {        
            $do_postal=1;
        }
        
        
        $fields='s.*,sc.*';
        
        $latlong_field='';
        
        $search_by_miles = 3959;
        $search_by_kms = 6371;
        
        $site_setting=site_setting();        
        
         $system_lat=$site_setting->default_latitude;
         $system_long=$site_setting->default_longitude;
         
         
         if ($visitor_lat == '' && $visitor_lon == ''){
             
            $visitor_lat=$system_lat;
            $visitor_lon=$system_long;
             
         }
         
//         if($do_postal==1){
//             $visitor_lat=$system_lat;
//             $visitor_lon=$system_long;
//         }
         
         
        
        
        if ($visitor_lat != '' && $visitor_lon != '') {

                $latlong_field = "(
                    $search_by_kms * acos (
                      cos ( radians(" . $visitor_lat . ") )
                      * cos( radians( s.store_lat ) )
                      * cos( radians( s.store_long ) - radians(" . $visitor_lon . ") )
                      + sin ( radians(" . $visitor_lat . ") )
                      * sin( radians( s.store_lat ) )
                    )
                  ) AS distance";
            }
            
            
            if($latlong_field!=''){
                $fields.=','.$latlong_field;
            }
            
            
        $this->db->select($fields);
        $this->db->from('store s');
        $this->db->join('store_category_rel scrl','scrl.store_id=s.store_id','left');
        $this->db->join('store_category sc','sc.category_id=scrl.category_id','left');
        $this->db->where('s.store_status',1);
        $this->db->where('sc.category_status',1);
        
        //==&& $do_postal==1
        if($postal_code!='' ){
            $this->db->where('s.store_postal_code',$postal_code);
        }
        
        $this->db->order_by('s.store_id','desc');
        
        if($latlong_field!=''){
            $this->db->having('distance <= ',$distance_range);  
        }
        
     
        $query = $this->db->get();
        
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
                return $query->result_array();
        }
        return 0;
    }
	
	
	
	function get_printer_result()
	{
		$this->db->select('*');
		$this->db->from('store s');
		$this->db->join('store_category_rel scrl','scrl.store_id=s.store_id','left');
		$this->db->join('store_category sc','sc.category_id=scrl.category_id','left');
                $this->db->where('s.store_status',1);
                $this->db->where('sc.category_status',1);
		$this->db->order_by('s.store_id','desc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		}
		return 0;
	}
        
        
        ///========manage part=================
        
        function get_user_location_count($user_id)
	{
		$this->db->select('*');
		$this->db->from('store s');
		$this->db->join('store_category_rel scrl','scrl.store_id=s.store_id','left');
		$this->db->join('store_category sc','sc.category_id=scrl.category_id','left');
		$this->db->where('s.user_id',$user_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_user_location($user_id,$limit,$offset)
	{
		$this->db->select('*');
		$this->db->from('store s');
		$this->db->join('store_category_rel scrl','scrl.store_id=s.store_id','left');
		$this->db->join('store_category sc','sc.category_id=scrl.category_id','left');
		$this->db->where('s.user_id',$user_id);
		$this->db->order_by('s.store_id','desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
		
	}	
	
	function insert_location()
	{
            
            $store_slug = clean_url($this->input->post('store_name'));
            
            
		$data = array(		
				'user_id'=>get_authenticateUserID(),
				'store_date'=>date('Y-m-d H:i:s'),
				'store_ip' =>getRealIP(),
				//'store_status' => $this->input->post('store_status'),
				'store_name' => (addslashes($this->input->post('store_name'))),
				'store_address' => (addslashes($this->input->post('store_address'))),
				'store_address2' => (addslashes($this->input->post('store_address2'))),
				'store_ctiy' => (addslashes($this->input->post('store_ctiy'))),
				'store_state' => (addslashes($this->input->post('store_state'))),
				'store_postal_code' => (addslashes($this->input->post('store_postal_code'))),
				'store_country' => (addslashes($this->input->post('store_country'))),
				'store_lat' => ($this->input->post('store_lat')),
				'store_long'=>($this->input->post('store_long')),
				'store_email'=>($this->input->post('store_email')),
				'store_phone'=>($this->input->post('store_phone')),
				'store_url'=>($this->input->post('store_url')),
				);
			$this->db->insert('store',$data);
			
			$store_id = mysql_insert_id();	
                        
                        
                         //Update slug
      
                        $data_slug = array('store_slug'=>$store_slug.'-' .$store_id );
                        $this->db->where('store_id', $store_id);
                        $this->db->update('store', $data_slug);
                        
                        
                        ///==add category
			$data_rel = array('store_id'=>$store_id,
							  'category_id'=>$this->input->post('category_id'));
			$this->db->insert('store_category_rel',$data_rel);
		
	}
	
	function update_location()
	{
            
            
            $store_slug = clean_url($this->input->post('store_name')).'-'.$this->input->post('store_id');
            
		$data = array(		
				'user_id'=>get_authenticateUserID(),
				'store_date'=>date('Y-m-d H:i:s'),
				'store_ip' =>getRealIP(),
				//'store_status' => $this->input->post('store_status'),
                    'store_name' => (addslashes($this->input->post('store_name'))),
				'store_address' => (addslashes($this->input->post('store_address'))),
				'store_address2' => (addslashes($this->input->post('store_address2'))),
				'store_ctiy' => (addslashes($this->input->post('store_ctiy'))),
				'store_state' => (addslashes($this->input->post('store_state'))),
				'store_postal_code' => (addslashes($this->input->post('store_postal_code'))),
				'store_country' => (addslashes($this->input->post('store_country'))),
				'store_lat' => ($this->input->post('store_lat')),
				'store_long'=>($this->input->post('store_long')),
				'store_email'=>($this->input->post('store_email')),
				'store_phone'=>($this->input->post('store_phone')),
				'store_url'=>($this->input->post('store_url')),
                    
                                'store_slug'=>$store_slug
				);
			$this->db->where('store_id',$this->input->post('store_id'));
			$this->db->update('store',$data);
			
			$data_rel = array('category_id'=>$this->input->post('category_id'));
			$this->db->where('store_id',$this->input->post('store_id'));
			$this->db->update('store_category_rel',$data_rel);			
			
		
	}
	
	
	
	function emailTaken($store_email)
	{
	
		 $query = $this->db->query("select * from ".$this->db->dbprefix('store')." where store_email='".$store_email."'");
		 
		 if($query->num_rows()>0)
		 {
			return true;
		 }
		 else 
		 {
		 	
			return false;
			
		 }		
	}
	
		
	function get_all_store_category()
	{
		$query=$this->db->get_where('store_category',array('category_status'=>1));
		
		if($query->num_rows()>0)
		{			
			return $query->result();
			
		} else {
		
			return false;		
		}
	}
	
	function check_exist_store($store_id,$user_id)
	{
		$this->db->select('*');
		$this->db->from('store');
		$this->db->where('store_id',$store_id);
		if($user_id > 0)
		{
			$this->db->where('user_id',$user_id);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function get_one_store($store_id)
	{
		$this->db->select('*');
		$this->db->from('store s');
		$this->db->join('store_category_rel scrl','scrl.store_id=s.store_id','left');
		$this->db->join('store_category sc','sc.category_id=scrl.category_id','left');
		$this->db->where('s.store_id',$store_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else
		{
			return 0;
		}
	}
        
        function get_one_store_by_slug($store_slug)
	{
		$this->db->select('*');
		$this->db->from('store s');
		$this->db->join('store_category_rel scrl','scrl.store_id=s.store_id','left');
		$this->db->join('store_category sc','sc.category_id=scrl.category_id','left');
		$this->db->where('s.store_slug',$store_slug);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else
		{
			return 0;
		}
	}
        
        
        
   function chk_track_view($store_id, $user_id) {
        $cur_date = date('Y-m-d');

        $query = $this->db->query("select * from " . $this->db->dbprefix('store_view') . " where store_id = " . $store_id . " and user_id = " . $user_id . " and DATE_FORMAT(view_date,'%Y-%m-%d') = '" . $cur_date . "'");

        if ($query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function chk_track_other_view($store_id) {
        $cur_date = date('Y-m-d');

        $query = $this->db->query("select * from " . $this->db->dbprefix('store_view') . " where store_id = " . $store_id . " and view_ip = '" . getRealIP() . "' and DATE_FORMAT(view_date,'%Y-%m-%d') = '" . $cur_date . "'");

        if ($query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function set_store_view($store_id) {


        $user_id = 0;
        $chk_track_view = 1;
        if (get_authenticateUserID() > 0) {
            $user_id = get_authenticateUserID();
        }
        if ($user_id > 0) {
            $chk_track_view = $this->chk_track_view($store_id, $user_id);
        }
        if ($chk_track_view == 0) {

            $sql_res = "update " . $this->db->dbprefix('store') . " set `store_view_count` = `store_view_count` + 1 where store_id =" . $store_id;
            $this->db->query($sql_res);

            $data = array('store_id' => $store_id,
                'user_id' => $user_id,
                'view_date' => date('Y-m-d h:i:s'),
                'view_ip' => getRealIP()
            );
            $this->db->insert('store_view', $data);
        } else {
            $chk_track_other_view = $this->chk_track_other_view($store_id);

            if ($chk_track_other_view == 0) {
                $sql_res = "update " . $this->db->dbprefix('store') . " set `store_view_count` = `store_view_count` + 1 where store_id =" . $store_id;
                $this->db->query($sql_res);

                $data = array('store_id' => $store_id,
                    'user_id' => 0,
                    'view_date' => date('Y-m-d h:i:s'),
                    'view_ip' => getRealIP()
                );
                $this->db->insert('store_view', $data);
            }
        }
    }
	
	
}
?>