<?php
/*********************************************************************************
 * This the taskrabbitclone.com  by Rockers Technology. is paid software. It is released under the terms of 
 * the following BSD License.
 * 
 *  Rockers Technologies (Head Office)
 *    5038,Berthpage Dr
 *    suwanee, GA. Zip Code : 30024
    
 *    E-mail Address : nishu@rockersinfo.com
 * 
 * Copyright © 2012-2020 by Rockers Technology , INC a domestic profit corporation has been duly incorporated under
the laws of the state of georiga , USA. www.rockersinfo.com
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this 
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this 
 *   list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.

 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
 
class Trip_model extends IWEB_Model 
{

	/*
	Function name :Trip_model
	Description :its default constuctor which called when Home_model object initialzie.its load necesary parent constructor
	*/
	function Trip_model()
    {
        parent::__construct();	
    } 
	
	function post_trip()
	{
		
		$trip_depart = explode(',',$this->input->post('departure_from'));
		$trip_end = explode(',',$this->input->post('want_go_to'));
		
		if(get_authenticateUserID() > 0)
		{
			$trip_status = 1;
		}
		else
		{
			$trip_status = 3;
		}
		
		$random_cookie = '';
		
		if(get_authenticateUserID() == '' || get_authenticateUserID() == 0)
		{
			
			$temp=get_cookie('tripdetail');
			if(!empty($temp))
			{
				$random_cookie = $temp;
			} else {
				$random_cookie = nonRepeat(0,20,10);
				$cookie = array(
				'name' => 'tripdetail',
				'value' => $random_cookie,
				'expire' => time()+(3600*6),
				'domain' => '',
				'path' => '/',
				'prefix' => ''
				);
				set_cookie($cookie);
			}
		
		}
		
		$data = array(		
				'user_id' => get_authenticateUserID(),		
				'package_type' => $this->input->post('trip_package'),
				'trip_from_place' => $trip_depart[0],	
				'trip_from_location'=>$this->input->post('departure_from'),		
				'trip_from_city'=>$trip_depart[0],		
				'trip_from_state'=>$trip_depart[1],
				'trip_to_place'=>$trip_end[0],
				'trip_to_location'=>$this->input->post('want_go_to'),
				'trip_to_city'=>$trip_end[0],
				'trip_to_state'=>$trip_end[1],
				'total_people'=>$this->input->post('total_people'),
				'total_adult'=>$this->input->post('adult'),
				'total_child'=>$this->input->post('child'),
				'currency_code_id'=>$this->input->post('currency'),
				'trip_min_budget'=>$this->input->post('min_budget'),
				'trip_max_budget'=>$this->input->post('max_budget'),
				'trip_month'=>$this->input->post('trip_month'),
				'trip_days'=>$this->input->post('total_day'),
				'trip_nights'=>$this->input->post('total_night'),
				'total_room'=>$this->input->post('room'),
				'trip_ip'=>getRealIP(),
				'trip_added_date'=>date('Y-m-d H:i:s'),
				'trip_confirm_date'=>date('Y-m-d H:i:s',strtotime($this->input->post('confirm_date'))),
				'trip_must_complete_date'=>date('Y-m-d H:i:s',strtotime($this->input->post('complete_date'))),
				'trip_status'=>$trip_status,
				'trip_activity_status'=>0,
				'trip_special_comment'=>$this->input->post('special_comment'),
				'trip_destination_places'=>$this->input->post('destination_place'),
				'trip_hotels'=>$this->input->post('trip_hotel'),
				'trip_cars'=>$this->input->post('trip_car'),
				'trip_flights'=>$this->input->post('trip_plane'),
				'trip_cruises'=>$this->input->post('trip_cruises'),
				'trip_railways'=>$this->input->post('railways'),
				'trip_build_package'=>$this->input->post('b_pack'),
				'trip_guest_id'=>$random_cookie,
				'trip_from_lattitude'=>$this->input->post('trip_start_lat'),
				'trip_from_longitude'=>$this->input->post('trip_start_long'),
				'trip_to_lattitude'=>$this->input->post('trip_end_lat'),
				'trip_to_longitude'=>$this->input->post('trip_end_long')
				); 
				
		$this->db->insert('trip', $data);
		$trip_id = mysql_insert_id();
		return $trip_id;
	}
	
	function update_cookie_trip($user_id,$trip_id)
	{
		/*Here comers trip guest id not original trip id*/
		$data = array('user_id'=>0,'trip_status'=>3,'trip_guest_id'=>$trip_id);
		$query = $this->db->get_where('trip',$data);
		if($query->num_rows() > 0)
		{
			$this->db->where('trip_guest_id',$trip_id);
			$this->db->where('trip_status',3);
			$udata= array('user_id'=>$user_id,'trip_status'=>1);
			$this->db->update('trip',$udata);
			delete_cookie('tripdetail');
			
			$query_user = $this->db->get_where("trip",array('trip_status'=>1,'trip_guest_id'=>$trip_id));
			if($query_user->num_rows()>0)
			{
				$res = $query->row();
				$trip_id = $res->trip_id;
				$trip_from = $res->trip_from_place;
				$trip_to = $res->trip_to_place;
				
				$user_info = get_user_profile_by_id($user_id);
				
				$username =$user_info->full_name;				
				$user_id=$user_info->user_id;
				$trip_link=site_url('from/'.$trip_from.'/to/'.$trip_to.'/'.$trip_id);
				
				/////////////============new trp email===========	

				$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='New Trip'");
				$email_temp=$email_template->row();
				if($email_temp)
				{
					$email_address_from=$email_temp->from_address;
					$email_address_reply=$email_temp->reply_address;
					$email_subject=$email_temp->subject;				
					$email_message=$email_temp->message;
					$email = $user_info->email;
					$email_to=$user_info->email;
				
					$email_message=str_replace('{break}','<br/>',$email_message);
					$email_message=str_replace('{user_name}',$username,$email_message);
					$email_message=str_replace('{trip_link}',$trip_link,$email_message);
					$email_temp_url=base_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
					$str=$email_message;
					/** custom_helper email function **/
					email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str);
				}
				/*$msg=base64_decode($msg);
				if(strstr($msg,'http'))
				{						
					redirect($msg);
				} else {
					 redirect('trip/post/success'); 
				}*/
			
				
			}
			
		}
		else
		{
			return 0;
		}
		
				
	}
	
	
	
	function edit_trip()
	{
		
		$trip_depart = explode(',',$this->input->post('departure_from'));
		$trip_end = explode(',',$this->input->post('want_go_to'));
		
		$data = array(		
				'package_type' => $this->input->post('trip_package'),
				'trip_from_place' => $trip_depart[0],	
				'trip_from_location'=>$this->input->post('departure_from'),		
				'trip_from_city'=>$trip_depart[0],		
				'trip_from_state'=>$trip_depart[1],
				'trip_to_place'=>$trip_end[0],
				'trip_to_location'=>$this->input->post('want_go_to'),
				'trip_to_city'=>$trip_end[0],
				'trip_to_state'=>$trip_end[1],
				'total_people'=>$this->input->post('total_people'),
				'total_adult'=>$this->input->post('adult'),
				'total_child'=>$this->input->post('child'),
				'currency_code_id'=>$this->input->post('currency'),
				'trip_min_budget'=>$this->input->post('min_budget'),
				'trip_max_budget'=>$this->input->post('max_budget'),
				'trip_month'=>$this->input->post('trip_month'),
				'trip_days'=>$this->input->post('total_day'),
				'trip_nights'=>$this->input->post('total_day') + 1,
				'total_room'=>$this->input->post('room'),
				'trip_update_date'=>date('Y-m-d H:i:s'),
				'trip_confirm_date'=>date('Y-m-d H:i:s',strtotime($this->input->post('confirm_date'))),
				'trip_must_complete_date'=>date('Y-m-d H:i:s',strtotime($this->input->post('complete_date'))),
				'trip_special_comment'=>$this->input->post('special_comment'),
				'trip_destination_places'=>$this->input->post('destination_place'),
				'trip_hotels'=>$this->input->post('trip_hotel'),
				'trip_cars'=>$this->input->post('trip_car'),
				'trip_flights'=>$this->input->post('trip_plane'),
				'trip_cruises'=>$this->input->post('trip_cruises'),
				'trip_railways'=>$this->input->post('railways'),
				'trip_build_package'=>$this->input->post('b_pack'),
				'trip_from_lattitude'=>$this->input->post('trip_start_lat'),
				'trip_from_longitude'=>$this->input->post('trip_start_long'),
				'trip_to_lattitude'=>$this->input->post('trip_end_lat'),
				'trip_to_longitude'=>$this->input->post('trip_end_long')
				); 
				
		//echo '<pre>'; print_r($data); die();
		$this->db->where('trip_id', $this->input->post('trip_id'));
		$this->db->update('trip', $data);
		
	}
	
	
	
	
	function get_similar_trip($trip_id,$trip_from,$trip_to)
	{
			
		$this->db->select('*');
		$this->db->from('trip');
		$this->db->join('user','trip.user_id=user.user_id');
		$this->db->join('user_profile','user.user_id=user_profile.user_id');
		$this->db->where('trip.trip_from_place',$trip_from);
		$this->db->where('trip.trip_to_place',$trip_to);
		$this->db->where('trip.trip_status',1);
		$this->db->where('trip.trip_activity_status',0);
		$this->db->where('trip.trip_agent_assign_id',0);
		$this->db->where('trip.trip_confirm_date >= ',date('Y-m-d H:i:s'));
		$this->db->where('trip.trip_id !=',$trip_id);
		$this->db->group_by('trip.trip_id');
		
		
		$query=$this->db->get();
		if($query->num_rows()>0)
		{	
		
			return  $query->result();
		}
		
		return 0;
	}
	
	function post_offer()
	{
		$data = array(	
			
				'trip_id'=> $this->input->post('trip_id'),
				'user_id'=> get_authenticateUserID(),
				'offer_content' => $this->input->post('proposal_desc'),
				'offer_build_package' => $this->input->post('offer_build_package'),
				'offer_amount' => $this->input->post('offer_amount'),
				'admin_fee' => $this->input->post('admin_fees'),
				'final_offer_amount' => $this->input->post('final_offer_agent'),
				'offer_days' => $this->input->post('offer_days'),
				'offer_nights' => $this->input->post('offer_nights'),
				'offer_ip' => getRealIP(),
				'offer_date' =>date('Y-m-d H:i:s')
			); 
			$this->db->insert('trip_offer', $data);
			$trip_offer_id = mysql_insert_id();
		/*document upload*/
		if($_FILES) { 
			if($_FILES['file_up']['name']!='')
			{
			//$cnt=1; 
			$this->load->library('upload');
			$rand=rand(0,100000);
				 for($i=0;$i<count($_FILES['file_up']['name']);$i++)
				 {
					if($_FILES['file_up']['name'][$i]!='')
					{
						$_FILES['userfile']['name']    =   $_FILES['file_up']['name'][$i];
						$_FILES['userfile']['type']    =   $_FILES['file_up']['type'][$i];
						$_FILES['userfile']['tmp_name'] =   $_FILES['file_up']['tmp_name'][$i];
						$_FILES['userfile']['error']       =   $_FILES['file_up']['error'][$i];
						$_FILES['userfile']['size']    =   $_FILES['file_up']['size'][$i]; 
		
						$config['file_name']     = $rand.'trip_offer_'.$trip_offer_id.'_'.$i;
						$config['upload_path'] =base_path().'upload/trip_offer_doc/';					
						$config['allowed_types'] = 'doc|docx|pdf';
			 		   $this->upload->initialize($config);
						if (!$this->upload->do_upload())
						{		
							$error =  $this->upload->display_errors();
						} 
						$picture = $this->upload->data();
				
							$data_doc=array(
							'trip_offer_id'=>$trip_offer_id,
							'offer_attachment'=>$picture['file_name']	
							);
						$this->db->insert('trip_offer_attachment',$data_doc);
					}	
												
				}
	   
   		
		}
		}
		/*end document upload*/		
	}
	
	function update_post_offer($trip_offer_id='')
	{
		
		
		$data = array(	
			
				'trip_id'=> $this->input->post('trip_id'),
				'user_id'=> get_authenticateUserID(),
				'offer_content' => $this->input->post('proposal_desc'),
				'offer_build_package' => $this->input->post('offer_build_package'),
				'offer_amount' => $this->input->post('offer_amount'),
				'admin_fee' => $this->input->post('admin_fees'),
				'final_offer_amount' => $this->input->post('final_offer_agent'),
				'offer_days' => $this->input->post('offer_days'),
				'offer_nights' => $this->input->post('offer_nights'),
				'offer_ip' => getRealIP(),
				'offer_date' =>date('Y-m-d H:i:s')
			); 
			$this->db->where('trip_offer_id', $trip_offer_id);
			$this->db->update('trip_offer', $data);

		/*document upload*/
		if($_FILES) { 
			if($_FILES['file_up']['name']!='')
			{
			
			//$cnt=1; 
			$this->load->library('upload');
			$rand=rand(0,100000);
				 for($i=0;$i<count($_FILES['file_up']['name']);$i++)
				 {
				
					if($_FILES['file_up']['name'][$i]!='')
					{
					
						$_FILES['userfile']['name']    =   $_FILES['file_up']['name'][$i];
						$_FILES['userfile']['type']    =   $_FILES['file_up']['type'][$i];
						$_FILES['userfile']['tmp_name'] =   $_FILES['file_up']['tmp_name'][$i];
						$_FILES['userfile']['error']       =   $_FILES['file_up']['error'][$i];
						$_FILES['userfile']['size']    =   $_FILES['file_up']['size'][$i]; 
		
						$config['file_name']     = $rand.'trip_offer_'.$trip_offer_id.'_'.$i;
						$config['upload_path'] =base_path().'upload/trip_offer_doc/';					
						$config['allowed_types'] = 'doc|docx|pdf';
			 		   $this->upload->initialize($config);
						if (!$this->upload->do_upload())
						{		
							 $error =  $this->upload->display_errors(); 
						} 
						
						$chk_trip_doc=$this->db->get_where('egg_trip_offer_attachment',array('trip_offer_id'=>$trip_offer_id));
		
		if($chk_trip_doc->num_rows()>0)
		{
			$trip_doc=$chk_trip_doc->row();
				///====image delete
				if($trip_doc->offer_attachment!='')
				{
					if(file_exists(base_path().'upload/trip_offer_doc/'.$trip_doc->offer_attachment))
					{
						unlink(base_path().'upload/trip_offer_doc/'.$trip_doc->offer_attachment);
					}
					$this->db->query("delete from egg_trip_offer_attachment where trip_offer_id='".$trip_doc->trip_offer_id."'");
				}	
			
		}
				
						$picture = $this->upload->data();
				
							$data_doc=array(
							'trip_offer_id'=>$trip_offer_id,
							'offer_attachment'=>$picture['file_name']	
							);
						$this->db->insert('trip_offer_attachment',$data_doc);
						
						
						
						
						
				
						
						
						
						
						
						
					
												
				}
	   
   		   }
		}
	   }
		/*end document upload*/		
	
	}
	
	function get_agent_offer($trip_id='',$user_id='')
	{
	
	   $this->db->select('*');
	   $this->db->from('user us');
	   $this->db->join('user_profile up','us.user_id=up.user_id');
	   $this->db->join('agent_profile ap','ap.user_id=up.user_id');
	   $this->db->join('trip_offer tf','tf.user_id=ap.user_id');
	   $this->db->join('trip tp','tf.trip_id=tp.trip_id');
	   $this->db->where('us.user_id',$user_id);
	   $this->db->where('tp.trip_id',$trip_id);   
	   $this->db->where('tp.trip_id',$trip_id);                
	   $this->db->where('tp.trip_status',1);    
	   $this->db->where('tp.trip_activity_status',0);
	   $this->db->where('tp.trip_agent_assign_id',0);
	   //$this->db->where('ap.agent_status',1);
	   $this->db->where('ap.agent_app_approved',1);
	   $this->db->group_by('tf.trip_offer_id');                  
	   
	   $query=$this->db->get();
	  
	   if($query->num_rows()>0)
	   {
	   	   return $query->result();
	   }
	   else
	   {
			return 0;
		}
	}
	
	function delete_trip_offer($trip_offer_id='')
	{
		
		
		$chk_trip_doc=$this->db->get_where('egg_trip_offer_attachment',array('trip_offer_id'=>$trip_offer_id));
		
		if($chk_trip_doc->num_rows()>0)
		{
			$trip_doc=$chk_trip_doc->row();
				///====image delete
				if($trip_doc->offer_attachment!='')
				{
					if(file_exists(base_path().'upload/trip_offer_doc/'.$trip_doc->offer_attachment))
					{
						unlink(base_path().'upload/trip_offer_doc/'.$trip_doc->offer_attachment);
					}
					$this->db->query("delete from egg_trip_offer_attachment where trip_offer_id='".$trip_doc->trip_offer_id."'");	
				}
		}
		
		$chk_trip_offer=$this->db->get_where('egg_trip_offer',array('trip_offer_id'=>$trip_offer_id));
		
		if($chk_trip_offer->num_rows()>0)
		{
			$this->db->query("delete from egg_trip_offer where trip_offer_id='".$trip_offer_id."'");	
		}
		
		
		
	}
	
	
	
	
	
	
	
		/*Get Trip Proposals 7-11-2012*/
	function trip_offer_doc($trip_id='',$user_id='')
	{
	 
	   $this->db->select('*');
	   $this->db->from('trip_offer tp');
	   $this->db->join('trip_offer_attachment tof','tp.trip_offer_id=tof.trip_offer_id');
	   $this->db->where('tp.user_id',$user_id);
	   $this->db->where('tp.trip_id',$trip_id);
	   $this->db->group_by('tp.trip_id');                  
	  
	   $query=$this->db->get();
	 
	   if($query->num_rows()>0)
	   {
	     return $query->result();
	   }
	   else
	   {
			return 0;
		}
	
		
	}
	function get_trip_proposal($trip_id='')
	{
	   $this->db->select('*,tof.trip_offer_id as offer_id');
	   $this->db->from('trip tp');
	   $this->db->join('trip_offer tof','tp.trip_id=tof.trip_id');
	   $this->db->join('trip_offer_attachment tat','tof.trip_offer_id=tat.trip_offer_id','left');
	   $this->db->where('tp.trip_id',$trip_id);
	   $this->db->where('tof.offer_status_for_trip_owner',0);
	   $this->db->group_by('tof.trip_offer_id');                  
	   $this->db->order_by('tof.offer_date',"desc");                  
	  
	   $query=$this->db->get();
	 
	   if($query->num_rows()>0)
	   {
	     return $query->result();
	   }
	   else
	   {
			return 0;
		}
	
	}
	function get_trip_proposal_asc($trip_id='')
	{
		 $this->db->select('*,tof.trip_offer_id as offer_id');
	   $this->db->from('trip tp');
	   $this->db->join('trip_offer tof','tp.trip_id=tof.trip_id');
	   $this->db->join('trip_offer_attachment tat','tof.trip_offer_id=tat.trip_offer_id','left');
	   $this->db->where('tp.trip_id',$trip_id);
	   $this->db->where('tof.offer_status_for_trip_owner',0);
	   $this->db->group_by('tof.trip_offer_id');                  
	   $this->db->order_by('tof.offer_date',"asc");                  
	  
	   $query=$this->db->get();
	 
	   if($query->num_rows()>0)
	   {
	     return $query->result();
	   }
	   else
	   {
			return 0;
		}
	}
	
	
	function get_trip_average($trip_id='')
	{
		$query = $this->db->query("SELECT AVG(`final_offer_amount`) AS average_amount from egg_trip_offer WHERE `trip_id` = ".$trip_id."");
		
		if($query->num_rows()>0)
	   {
	   		$res = $query->row();
	     return round($res->average_amount, 2);
	   }
	   else
	   {
			return 0;
		}
	}
	
	function get_trip_high_amount($trip_id)
	{
		$query = $this->db->query("SELECT MAX(`final_offer_amount`) AS max_amount from egg_trip_offer WHERE `trip_id` = ".$trip_id."");
		
		if($query->num_rows()>0)
	   {
	   		$res = $query->row();
	     return round($res->max_amount, 2);
	   }
	   else
	   {
			return 0;
		}
	}
	
	function get_trip_low_amount($trip_id)
	{
		$query = $this->db->query("SELECT MIN(`final_offer_amount`) AS min_amount from egg_trip_offer WHERE `trip_id` = ".$trip_id."");
		
		if($query->num_rows()>0)
	   {
	   		$res = $query->row();
	     return round($res->min_amount, 2);
	   }
	   else
	   {
			return 0;
		}
	}
	
	function get_hidden_proposal($trip_id='')
	{
	    $this->db->select('*,tof.trip_offer_id as offer_id');
	   $this->db->from('trip tp');
	   $this->db->join('trip_offer tof','tp.trip_id=tof.trip_id');
	   $this->db->join('trip_offer_attachment tat','tof.trip_offer_id=tat.trip_offer_id','left');
	   $this->db->where('tp.trip_id',$trip_id);
	   $this->db->where('tof.offer_status_for_trip_owner',1);
	   $this->db->group_by('tof.trip_offer_id');                  
	   $this->db->order_by('tof.offer_date',"desc");                  
	  
	   $query=$this->db->get();
	 
	   if($query->num_rows()>0)
	   {
	     return $query->result();
	   }
	   else
	   {
			return 0;
		}
	
	}
	
	function get_hidden_proposal_asc($trip_id='')
	{
	   $this->db->select('*,tof.trip_offer_id as offer_id');
	   $this->db->from('trip tp');
	   $this->db->join('trip_offer tof','tp.trip_id=tof.trip_id');
	   $this->db->join('trip_offer_attachment tat','tof.trip_offer_id=tat.trip_offer_id','left');
	   $this->db->where('tp.trip_id',$trip_id);
	   $this->db->where('tof.offer_status_for_trip_owner',1);
	   $this->db->group_by('tof.trip_offer_id');                  
	   $this->db->order_by('tof.offer_date',"asc");                  
	  
	   $query=$this->db->get();
	 
	   if($query->num_rows()>0)
	   {
	     return $query->result();
	   }
	   else
	   {
			return 0;
		}
	
	}
	
	function get_decline_proposal($trip_id='')
	{
	    $this->db->select('*,tof.trip_offer_id as offer_id');
	   $this->db->from('trip tp');
	   $this->db->join('trip_offer tof','tp.trip_id=tof.trip_id');
	   $this->db->join('trip_offer_attachment tat','tof.trip_offer_id=tat.trip_offer_id','left');
	   $this->db->where('tp.trip_id',$trip_id);
	   $this->db->where('tof.offer_status_for_trip_owner',2);
	     $this->db->group_by('tof.trip_offer_id');                     
	   $this->db->order_by('tof.offer_date',"desc");                  
	  
	   $query=$this->db->get();
	 
	   if($query->num_rows()>0)
	   {
	     return $query->result();
	   }
	   else
	   {
			return 0;
		}
	
	}
	
	function get_decline_proposal_asc($trip_id='')
	{
	   $this->db->select('*,tof.trip_offer_id as offer_id');
	   $this->db->from('trip tp');
	   $this->db->join('trip_offer tof','tp.trip_id=tof.trip_id');
	   $this->db->join('trip_offer_attachment tat','tof.trip_offer_id=tat.trip_offer_id','left');
	   $this->db->where('tp.trip_id',$trip_id);
	   $this->db->where('tof.offer_status_for_trip_owner',2);
	     $this->db->group_by('tof.trip_offer_id');                     
	   $this->db->order_by('tof.offer_date',"asc");                  
	  
	   $query=$this->db->get();
	 
	   if($query->num_rows()>0)
	   {
	     return $query->result();
	   }
	   else
	   {
			return 0;
		}
	
	}
	
	function trip_hidden($trip_offer_id='')
	{
		$data = array('offer_status_for_trip_owner'=>1);
		$this->db->where('trip_offer_id',$trip_offer_id);
		$this->db->update('trip_offer',$data);
	}
	
	function trip_decline($trip_offer_id='')
	{
		$data = array('offer_status_for_trip_owner'=>2);
		$this->db->where('trip_offer_id',$trip_offer_id);
		$this->db->update('trip_offer',$data);
	}
	function remove_hidden_trip_offer($trip_offer_id='')
	{
		$data = array('offer_status_for_trip_owner'=>0);
		$this->db->where('trip_offer_id',$trip_offer_id);
		$this->db->update('trip_offer',$data);
	}
	
	
	function get_invted_proposal($trip_id='')
	{
	 
	    $this->db->select('*,tof.trip_offer_id as offer_id');
	   $this->db->from('trip_agent_invite ti');
	   $this->db->join('trip tp','ti.invite_by_user_id=tp.user_id');
	   $this->db->join('trip_offer tof','ti.invite_to_user_id=tof.user_id','left');
	   $this->db->join('trip_offer_attachment tat','tof.trip_offer_id=tat.trip_offer_id','left');
	   $this->db->where('ti.trip_id',$trip_id);
	   $this->db->group_by('ti.trip_agent_invite_id');                  
	   $this->db->order_by('ti.invite_date',"desc");                  
	  
	   $query=$this->db->get();
	   if($query->num_rows()>0)
	   {
	   
	     return $query->result();
	   }
	   else
	   {
			return 0;
		}
	}
	
	function get_invted_proposal_asc($trip_id='')
	{
	 
	    $this->db->select('*,tof.trip_offer_id as offer_id');
	   $this->db->from('trip_agent_invite ti');
	   $this->db->join('trip tp','ti.invite_by_user_id=tp.user_id');
	   $this->db->join('trip_offer tof','ti.invite_to_user_id=tof.user_id','left');
	   $this->db->join('trip_offer_attachment tat','tof.trip_offer_id=tat.trip_offer_id','left');
	   $this->db->where('ti.trip_id',$trip_id);
	   $this->db->group_by('ti.trip_agent_invite_id');                  
	   $this->db->order_by('ti.invite_date',"asc");                  
	  
	   $query=$this->db->get();
	   if($query->num_rows()>0)
	   {
	   
	     return $query->result();
	   }
	   else
	   {
			return 0;
		}
	}
	
	function conversation($trip_id)
	{
		$trip_info = get_one_trip($trip_id);
		$trip_user_id = $trip_info->user_id;
	
		$data = array('trip_id'=>$trip_id,
		'trip_user_id'=>$trip_user_id,
		'post_to_user_id'=>$trip_user_id,
		'post_by_user_id'=>get_authenticateUserID(),
		'reply_trip_conversation_id'=>0,
		'conversation'=>$this->input->post('ask_question'),
		'conversation_date'=>date('Y-m-d H:i:s'),
		'conversation_ip'=>getRealIP()
		);
		
		$this->db->insert('trip_coversation',$data);
		
	}
	
	function conversation_reply($trip_id)
	{
	
		
		$tip_con_id = $this->input->post('trip_conversation_id');
		
		$sql="select * from ".$this->db->dbprefix('trip_coversation')." where trip_conversation_id='".$tip_con_id."'";
		
		$query = $this->db->query($sql);
		
		if($query->num_rows()>0)
		{
			$res = $query->row();
		}
		
		
		$post_to_user_id = $res->post_by_user_id;
		
		$trip_info = get_one_trip($trip_id);
		$trip_user_id = $trip_info->user_id;

		
		$data = array('trip_id'=>$trip_id,
		'trip_user_id'=>$trip_user_id,
		'post_to_user_id'=>$post_to_user_id,
		'post_by_user_id'=>get_authenticateUserID(),
		'reply_trip_conversation_id'=>$this->input->post('trip_conversation_id'),
		'conversation'=>$this->input->post('ask_question_rply'),
		'conversation_date'=>date('Y-m-d H:i:s'),
		'conversation_ip'=>getRealIP()
		);
		
		$this->db->insert('trip_coversation',$data);
	}
	
	function get_trip_conversation($trip_id='',$trip_user_id='')
	{
	   $this->db->select('*');
	   $this->db->from('trip_coversation');
	   $this->db->where('trip_id',$trip_id);
	   $this->db->where('trip_user_id',$trip_user_id);
	    $this->db->where('reply_trip_conversation_id',0);
	   $query=$this->db->get();
	   if($query->num_rows()>0)
	   {
	   
	     return $query->result();
	   }
	   else
	   {
			return 0;
		}
	}
	
	function get_conversation_reply($trip_conversation_id='')
	{	
		
		$this->db->select('*');
	   $this->db->from('trip_coversation');
	    $this->db->where('reply_trip_conversation_id',$trip_conversation_id);
	   $query=$this->db->get();
	   if($query->num_rows()>0)
	   {
	   
	     return $query->row();
	   }
	   else
	   {
			return 0;
		}
	}


}

?>