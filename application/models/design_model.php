<?php

class Design_model extends IWEB_Model {
    /*
      Function name :User_model
      Description :its default constuctor which called when user_model object initialzie.its load necesary parent constructor
     */

    function Design_model() {
        parent::__construct();
    }
    
    
    function place_rating(){
        
        
        
        
        $design_rating =(int) $this->input->post('rating'); 
        $design_review_ip=getRealIP();
        $design_review_date=date('Y-m-d h:i:s');
        $user_id=get_authenticateUserID();
        
        $design_id =(int) $this->input->post('design_id');
        
        
        $final_design_rating=0;
        
        
        
            $data_review= array(
                'parent_review_id'=>0,
                'design_id'=>$design_id,
                'user_id'=>$user_id,
                'design_rating'=>$design_rating,            
                'design_review_description'=>'',
                'design_review_ip'=>$design_review_ip,
                'design_review_date'=>$design_review_date           
                );
            $this->db->insert('design_review',$data_review);
            
            
                $final_design_rating=$this->get_design_avg_rating($design_id);
                $this->db->trans_start();
                $sql_res = "update " . $this->db->dbprefix('design') . " set `design_total_rating` = ".$final_design_rating." where design_id =" . $design_id;
                $this->db->query($sql_res);
                $this->db->trans_complete();
            
        
        
        
         return 1;
        
        
    }
    
    
    function get_design_avg_rating($design_id)
    {
         $sql="SELECT AVG(design_rating) as final_design_rating from ".$this->db->dbprefix('design_review')." where parent_review_id=0  and design_id=".$design_id;
         
         $query = $this->db->query($sql);
        
        if($query->num_rows()>0){
        
            $result=$query->row();
            
            return $result->final_design_rating;
      
        }
        return 0;
      
      
    }
    

    function check_exist_design($design_id, $user_id) {
        $this->db->select('*');
        $this->db->from('design');
        $this->db->where('design_id', $design_id);
        if ($user_id > 0) {
            //For check in view also
            $this->db->where('user_id', $user_id);
        } else {
            $this->db->where('design_status', 1);
        }
        $query = $this->db->get();
       
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function insert_design() {

        $new_design_title = BadWords($this->input->post('design_title'));
        $design_title = $new_design_title['str'];

        $new_design_content = BadWords($this->input->post('design_content'));
        $design_content = $new_design_content['str'];

        $new_design_meta_keyword = BadWords($this->input->post('design_meta_keyword'));
        $design_meta_keyword = $new_design_meta_keyword['str'];

        $new_design_meta_description = BadWords($this->input->post('design_meta_description'));
        $design_meta_description = $new_design_meta_description['str'];
        
        
        $design_is_modified=$this->input->post('design_is_modified');
        $parent_design_id=$this->input->post('parent_design_id');
        
        if($design_is_modified==0 || $design_is_modified==''){
            $parent_design_id=0;
        }

        //$design_slug = get_design_slug($this->input->post('design_title'), 0);
        $design_slug = clean_url($this->input->post('design_title'));
        
        $design_status=0;
        
        $get_cat_detail=get_category_obj_by_id('design',$this->input->post('design_category'));
        
        if($get_cat_detail){
            if($get_cat_detail->category_status==1){
                $design_status=1;
            }
        }
        
        
        
        $data = array('design_title' => $design_title,
            'design_is_modified' => $design_is_modified,
            'parent_design_id' => $parent_design_id,
            'design_slug' => $design_slug,
            'design_content' => $design_content,
            'design_point' => $this->input->post('design_point'),
            'design_ref_id' => $this->input->post('design_ref_id'),
            'design_by' => $this->input->post('design_by'),            
            'design_meta_keyword' => $design_meta_keyword,
            'design_meta_description' => $design_meta_description,
            'user_id' => get_authenticateUserID(),
            'design_date' => date('Y-m-d h:i:s'),
            'design_ip' => getRealIP(),
            'design_status' => $design_status,
            'design_gallery_type' => $this->input->post('design_gallery_type')
        );

        $this->db->insert('design', $data);
        $design_id = mysql_insert_id();


        //Update slug
      
        $data_slug = array('design_slug'=>$design_slug.'-' .$design_id );
        $this->db->where('design_id', $design_id);
        $this->db->update('design', $data_slug);

        /* Insert data in design_category_rel */
        if($get_cat_detail){
        $data_design_category_rel = array('category_id' => $this->input->post('design_category'),
            'design_id' => $design_id);
        $this->db->insert('design_category_rel', $data_design_category_rel);
        }


        $extra_file=array('fbx','c4d','max','dae','ma','mb','lwo','lws','lxo','x','dxf','xsi','3ds','obj','ply','skp','stl','iges','x3d','ac','pov','rwx','sldasm','sldprt','wrl','prt','asm');      
        
        $image_settings = image_setting();
        $this->load->library('upload');
        if ($_FILES['attachment_name']['name'] != '') {
            for ($i = 0; $i < count($_FILES['attachment_name']['name']); $i++) {
                if ($_FILES['attachment_name']['name'][$i] != '') {

                    $rand = rand(0, 100000);

                    $_FILES['userfile']['name'] = $_FILES['attachment_name']['name'][$i];
                    $_FILES['userfile']['type'] = $_FILES['attachment_name']['type'][$i];
                    $_FILES['userfile']['tmp_name'] = $_FILES['attachment_name']['tmp_name'][$i];
                    $_FILES['userfile']['error'] = $_FILES['attachment_name']['error'][$i];
                    $_FILES['userfile']['size'] = $_FILES['attachment_name']['size'][$i];
                    
                    
                    $ext = pathinfo($_FILES['attachment_name']['name'][$i], PATHINFO_EXTENSION);
                    
                    $config['file_name'] = $rand . 'design_' . $design_id . '_' . $i;
                    $config['upload_path'] = base_path() . 'upload/design_orig/';
                    //$config['allowed_types'] = 'doc|docx|jpg|jpeg|gif|png|bmp';
                    $config['allowed_types'] = '*';

                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload()) {
                        $error = $this->upload->display_errors();
                    }


                    $picture = $this->upload->data();
                    
                     $image = $picture['file_name'];
                     
                     
                    $this->load->library('image_lib');
                    $this->image_lib->clear();
                    $gd_var = 'gd2';

                    if ($_FILES['attachment_name']['type'][$i] != "image/png" and $_FILES['attachment_name']['type'][$i] != "image/x-png") {
                        $gd_var = 'gd2';
                    }

                    if ($_FILES['attachment_name']['type'][$i] != "image/gif") {
                        $gd_var = 'gd2';
                    }

                    if ($_FILES['attachment_name']['type'][$i] != "image/jpeg" and $_FILES['attachment_name']['type'][$i] != "image/pjpeg") {
                        $gd_var = 'gd2';
                    }
                    
                    
                    if(in_array($ext,$extra_file)){
                        
                          copy($picture['full_path'],base_path().'upload/design/'.$picture['file_name']);
                          
                           $data_gallery = array(
                                'design_id' => $design_id,
                                'attachment_is_main' => 0,
                                'attachment_file' => $image,
                                'attachment_type' => 1,
                                'attachment_order' => 0,
                                'date_added' => date('Y-m-d H:i:s')
                            );
                            $this->db->insert('design_attachment', $data_gallery);
                        
                    } else {
                        $this->image_lib->initialize(array(
                            'image_library' => $gd_var,
                            'thumb_marker' => "",
                            'create_thumb' => TRUE,
                            'master_dim' => 'width',
                            'maintain_ratio' => TRUE,
                            'quality' => '100%',
                            'width' => $image_settings->design_width,
                            'height' => $image_settings->design_height,
                            'source_image' => base_path() . 'upload/design_orig/' . $picture['file_name'],
                            'new_image' => base_path() . 'upload/design/' . $picture['file_name']                    
                            
                        ));
                        if (!$this->image_lib->resize()) {
                            $error = $this->image_lib->display_errors();
                        }
                        $this->image_lib->clear();
                        
                        
                        if ($i == 0) {
                        $attachment_is_main = 1;
                    } else {
                        $attachment_is_main = 0;
                    }
                    $data_gallery = array(
                        'design_id' => $design_id,
                        'attachment_is_main' => $attachment_is_main,
                        'attachment_name' => $image,
                        'attachment_type' => 1,
                        'attachment_order' => $i,
                        'date_added' => date('Y-m-d H:i:s')
                    );
                    $this->db->insert('design_attachment', $data_gallery);
                    
                    
                    }

                   


                    
                }
            }
        }
        
        
        //===3d file===
        
        
        if ($_FILES['3dattachment_name']['name'] != '') {
          

            $i=1;
            
                    $rand = rand(0, 100000);

                    $_FILES['userfile']['name'] = $_FILES['3dattachment_name']['name'];
                    $_FILES['userfile']['type'] = $_FILES['3dattachment_name']['type'];
                    $_FILES['userfile']['tmp_name'] = $_FILES['3dattachment_name']['tmp_name'];
                    $_FILES['userfile']['error'] = $_FILES['3dattachment_name']['error'];
                    $_FILES['userfile']['size'] = $_FILES['3dattachment_name']['size'];
                    
                    
                    $ext = pathinfo($_FILES['3dattachment_name']['name'], PATHINFO_EXTENSION);
                    
                    if(in_array($ext,$extra_file)){
                        
                        $config['file_name'] = $rand . 'design_' . $design_id . '_' . $i;
                        $config['upload_path'] = base_path() . 'upload/design_orig/';
                        //$config['allowed_types'] = 'doc|docx|jpg|jpeg|gif|png|bmp';
                        $config['allowed_types'] = '*';

                        $this->upload->initialize($config);
                        if (!$this->upload->do_upload()) {
                            $error = $this->upload->display_errors();
                        }


                        $picture = $this->upload->data();
                   
                    

                            $image = $picture['file_name'];


                            $data_gallery = array(
                                'design_id' => $design_id,
                                'attachment_is_main' => 0,
                                'attachment_file' => $image,
                                'attachment_type' => 1,
                                'attachment_order' => 0,
                                'date_added' => date('Y-m-d H:i:s')
                            );
                            $this->db->insert('design_attachment', $data_gallery);
                } 
            
        }
        
        
        
        //===logic counter===
        
       
        $this->db->select('*');
        $this->db->from('design_attachment');
        $this->db->where('design_id', $design_id);       
        $attachment_query = $this->db->get();
        if ($attachment_query->num_rows() > 0) {
            
        } else {
             $counter_id=gallery_counter_logic();
             $data_counter = array('design_counter_id'=>$counter_id);
             $this->db->where('design_id', $design_id);
             $this->db->update('design', $data_counter);
             gallery_counter_update($counter_id);
        }
        
        
       //===end logic counter===
        
        

        /* Insert Tags */
        $tags = explode(",", $this->input->post('tag_name'));
        if ($tags) {
            for ($i = 0; $i < count($tags); $i++) {
                $tag_name=trim(strip_tags($tags[$i]));
                if($tag_name!=''){
                $tag_id = check_tag($tag_name);
                if($tag_id>0){
                $data_tag = array('design_id' => $design_id,
                    'tag_id' => $tag_id,
                    'tag_date' => date('Y-m-d h:i:s'),
                    'tag_ip' => getRealIP());
                $this->db->insert('design_tags', $data_tag);
                }
                }
            }

           
        } //design_tags
    }

    function update_design() {
        
        
        
        //$design_slug = get_design_slug($this->input->post('design_title'), 0);
        $design_slug = clean_url($this->input->post('design_title'));
        $design_id = $this->input->post('design_id');
        $design_slug = $design_slug . '-' . $design_id;


        $new_design_title = BadWords($this->input->post('design_title'));
        $design_title = $new_design_title['str'];

        $new_design_content = BadWords($this->input->post('design_content'));
        $design_content = $new_design_content['str'];

        $new_design_meta_keyword = BadWords($this->input->post('design_meta_keyword'));
        $design_meta_keyword = $new_design_meta_keyword['str'];

        $new_design_meta_description = BadWords($this->input->post('design_meta_description'));
        $design_meta_description = $new_design_meta_description['str'];
        
        $design_is_modified=$this->input->post('design_is_modified');
        $parent_design_id=$this->input->post('parent_design_id');
        
        if($design_is_modified==0 || $design_is_modified==''){
            $parent_design_id=0;
        }
        
        
        $design_status=0;
        
        $get_cat_detail=get_category_obj_by_id('design',$this->input->post('design_category'));
        
        if($get_cat_detail){
            if($get_cat_detail->category_status==1){
                $design_status=1;
            }
        }
        

        $data = array('design_title' => $design_title,
            'design_is_modified' => $design_is_modified,
            'parent_design_id' => $parent_design_id,
            'design_slug' => $design_slug,
            'design_content' => $design_content,
            'design_point' => $this->input->post('design_point'),
            'design_ref_id' => $this->input->post('design_ref_id'),
            'design_by' => $this->input->post('design_by'),            
            'design_meta_keyword' => $design_meta_keyword,
            'design_meta_description' => $design_meta_description,
            'design_ip' => getRealIP(),
            'design_status' => $design_status,
            'design_gallery_type' => $this->input->post('design_gallery_type')
        );
        $this->db->where('design_id', $design_id);
        $this->db->update('design', $data);

        /* Insert data in design_category_rel */
        if($get_cat_detail){
            $data_design_category_rel = array('category_id' => $this->input->post('design_category'),
                'design_id' => $design_id);
            $this->db->where('design_id', $design_id);
            $this->db->update('design_category_rel', $data_design_category_rel);
        }
        
        
        
        ///===========upload part
        $is_first=0;
        $this->db->select('*');
        $this->db->from('design_attachment');
        $this->db->where('design_id', $design_id);
        $this->db->where('attachment_name != ', '');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $is_first=1;
        }
        
        
        $extra_file=array('fbx','c4d','max','dae','ma','mb','lwo','lws','lxo','x','dxf','xsi','3ds','obj','ply','skp','stl','iges','x3d','ac','pov','rwx','sldasm','sldprt','wrl','prt','asm');      
        
        $image_settings = image_setting();
        $this->load->library('upload');
        if ($_FILES['attachment_name']['name'] != '') {
            for ($i = 0; $i < count($_FILES['attachment_name']['name']); $i++) {
                if ($_FILES['attachment_name']['name'][$i] != '') {

                    $rand = rand(0, 100000);

                    $_FILES['userfile']['name'] = $_FILES['attachment_name']['name'][$i];
                    $_FILES['userfile']['type'] = $_FILES['attachment_name']['type'][$i];
                    $_FILES['userfile']['tmp_name'] = $_FILES['attachment_name']['tmp_name'][$i];
                    $_FILES['userfile']['error'] = $_FILES['attachment_name']['error'][$i];
                    $_FILES['userfile']['size'] = $_FILES['attachment_name']['size'][$i];

                    
                    $ext = pathinfo($_FILES['attachment_name']['name'][$i], PATHINFO_EXTENSION);
                    
                    $config['file_name'] = $rand . 'design_' . $design_id . '_' . $i;
                    $config['upload_path'] = base_path() . 'upload/design_orig/';
                    //$config['allowed_types'] = 'doc|docx|jpg|jpeg|gif|png|bmp';
                    $config['allowed_types'] = '*';

                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload()) {
                        $error = $this->upload->display_errors();
                    }

                    
                    $picture = $this->upload->data();
                     $image = $picture['file_name'];
                  
                    
                    
                    $this->load->library('image_lib');
                    $this->image_lib->clear();
                    $gd_var = 'gd2';

                    if ($_FILES['attachment_name']['type'][$i] != "image/png" and $_FILES['attachment_name']['type'][$i] != "image/x-png") {
                        $gd_var = 'gd2';
                    }

                    if ($_FILES['attachment_name']['type'][$i] != "image/gif") {
                        $gd_var = 'gd2';
                    }

                    if ($_FILES['attachment_name']['type'][$i] != "image/jpeg" and $_FILES['attachment_name']['type'][$i] != "image/pjpeg") {
                        $gd_var = 'gd2';
                    }
                    
                     if(in_array($ext,$extra_file)){
                        
                        copy($picture['full_path'],base_path().'upload/design/'.$picture['file_name']);
                        
                         $data_gallery = array(
                                'design_id' => $design_id,
                                'attachment_is_main' => 0,
                                'attachment_file' => $image,
                                'attachment_type' => 1,
                                'attachment_order' => 0,
                                'date_added' => date('Y-m-d H:i:s')
                            );
                            $this->db->insert('design_attachment', $data_gallery);
                        
                    } else {  
                        $this->image_lib->initialize(array(
                            'image_library' => $gd_var,
                            'thumb_marker' => "",
                            'create_thumb' => TRUE,
                            'master_dim' => 'width',
                            'maintain_ratio' => TRUE,
                            'quality' => '100%',
                            'width' => $image_settings->design_width,
                            'height' => $image_settings->design_height,
                            'source_image' => base_path() . 'upload/design_orig/' . $picture['file_name'],
                            'new_image' => base_path() . 'upload/design/' . $picture['file_name']
                        ));
                        if (!$this->image_lib->resize()) {
                            $error = $this->image_lib->display_errors();
                        }
                        $this->image_lib->clear();
                        
                        
                         if ($i == 1 || $is_first==0) {
                       
                        $attachment_is_main = 1;
                       
                    } else {
                        $attachment_is_main = 0;
                    }
                    $data_gallery = array(
                        'design_id' => $design_id,
                        'attachment_is_main' => $attachment_is_main,
                        'attachment_name' => $image,
                        'attachment_type' => 1,
                        'attachment_order' => $i,
                        'date_added' => date('Y-m-d H:i:s')
                    );
                    $this->db->insert('design_attachment', $data_gallery);
                    
                    
                    }

                   


                   
                }
            }
        }
        
        
        
        
        //===3d file===
        
        
        if ($_FILES['3dattachment_name']['name'] != '') {
          

            $i=1;
            
                    $rand = rand(0, 100000);

                    $_FILES['userfile']['name'] = $_FILES['3dattachment_name']['name'];
                    $_FILES['userfile']['type'] = $_FILES['3dattachment_name']['type'];
                    $_FILES['userfile']['tmp_name'] = $_FILES['3dattachment_name']['tmp_name'];
                    $_FILES['userfile']['error'] = $_FILES['3dattachment_name']['error'];
                    $_FILES['userfile']['size'] = $_FILES['3dattachment_name']['size'];
                    
                    
                    $ext = pathinfo($_FILES['3dattachment_name']['name'], PATHINFO_EXTENSION);
                    
                    if(in_array($ext,$extra_file)){
                        
                        $config['file_name'] = $rand . 'design_' . $design_id . '_' . $i;
                        $config['upload_path'] = base_path() . 'upload/design_orig/';
                        //$config['allowed_types'] = 'doc|docx|jpg|jpeg|gif|png|bmp';
                        $config['allowed_types'] = '*';

                        $this->upload->initialize($config);
                        if (!$this->upload->do_upload()) {
                            $error = $this->upload->display_errors();
                        }


                        $picture = $this->upload->data();
                   
                    

                            $image = $picture['file_name'];
                            
                            
                            
                            
                            if($this->input->post('prev_3dattachment_name')!=''){
                        
                                   
                                   $this->db->select('*');
                                    $this->db->from('design_attachment');
                                    $this->db->where('design_id', $design_id);      
                                    $this->db->where('attachment_type', 1);
                                    $this->db->where('attachment_file',$this->input->post('prev_3dattachment_name'));


                                    $query = $this->db->get();
                                    if ($query->num_rows() > 0) {
                                        $result = $query->row();

                                        $get_design_3d_image = $result->attachment_file;

                                         if ($get_design_3d_image != '') {
                                            if (file_exists(base_path() . 'upload/design_orig/' . $get_design_3d_image)) {
                                                unlink(base_path() . 'upload/design_orig/' . $get_design_3d_image);
                                            }

                                            
                                        }

                                   $delete_attach = $this->db->query("delete from " . $this->db->dbprefix('design_attachment') . " where attachment_id='" . $result->attachment_id . "'");


                                    }
                                
                                

                            }


                            $data_gallery = array(
                                'design_id' => $design_id,
                                'attachment_is_main' => 0,
                                'attachment_file' => $image,
                                'attachment_type' => 1,
                                'attachment_order' => 0,
                                'date_added' => date('Y-m-d H:i:s')
                            );
                            $this->db->insert('design_attachment', $data_gallery);
                } 
            
        }
        
        //===logic counter===
        
        $this->db->select('*');
        $this->db->from('design');
        $this->db->where('design_id', $design_id);       
        $design_query = $this->db->get();
        
       
        $this->db->select('*');
        $this->db->from('design_attachment');
        $this->db->where('design_id', $design_id);       
        $attachment_query = $this->db->get();
        if ($attachment_query->num_rows() > 0) {
            if($design_query->num_rows()>0){
                $des_res=$design_query->row();
                if($des_res->design_counter_id>0){
                    $data_counter = array('design_counter_id'=>0);
                    $this->db->where('design_id', $design_id);
                    $this->db->update('design', $data_counter);
                }
            }
            
        } else {
            if($design_query->num_rows()>0){
                $des_res=$design_query->row();
                if($des_res->design_counter_id==0){
                     $counter_id=gallery_counter_logic();
                     $data_counter = array('design_counter_id'=>$counter_id);
                     $this->db->where('design_id', $design_id);
                     $this->db->update('design', $data_counter);
                     gallery_counter_update($counter_id);
                }
            }
        }
        
        
       //===end logic counter===
        

        /* Insert Tags */
        //Delete design tag
        $delete_tag = $this->db->query("delete from " . $this->db->dbprefix('design_tags') . " where design_id='" . $design_id . "'");

        
         /* Insert Tags */
        $tags = explode(",", $this->input->post('tag_name'));
        if ($tags) {
            for ($i = 0; $i < count($tags); $i++) {
                $tag_name=trim(strip_tags($tags[$i]));
                if($tag_name!=''){
                $tag_id = check_tag($tag_name);
                if($tag_id>0){
                $data_tag = array('design_id' => $design_id,
                    'tag_id' => $tag_id,
                    'tag_date' => date('Y-m-d h:i:s'),
                    'tag_ip' => getRealIP());
                $this->db->insert('design_tags', $data_tag);
                }
                }
            }

           
        } //design_tags
    }

    function get_popular_design($limit, $design_id = 0) {
        $this->db->select('d.*,c.category_name');
        $this->db->from('design d');
       
        $this->db->join('design_category_rel dr', 'd.design_id=dr.design_id');
        $this->db->join('design_category c', 'c.category_id=dr.category_id');
         $this->db->where('d.design_status', '1');
        $this->db->where('d.design_id != ', $design_id);
        $this->db->order_by('design_like_count', 'desc');
        $this->db->limit($limit);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function get_user_design($user_id, $limit, $design_id = 0) {

        $this->db->select('d.*,c.category_name');
        $this->db->from('design d');
        
        $this->db->join('design_category_rel dr', 'd.design_id=dr.design_id');
        $this->db->join('design_category c', 'c.category_id=dr.category_id');
$this->db->where('d.design_status', '1');
        $this->db->where('d.user_id', $user_id);
        $this->db->where('d.design_id != ', $design_id);
        $this->db->order_by('d.design_id', 'desc');
        //$this->db->order_by('design_like_count','desc');
        $this->db->limit($limit);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function get_user_design_gallery($limit,$user_id, $design_id) {
        $this->db->select('d.*,c.category_name,at.attachment_name');
        $this->db->from('design d');

        $this->db->join('design_category_rel dr', 'd.design_id=dr.design_id');
        $this->db->join('design_category c', 'c.category_id=dr.category_id');
        $this->db->join('design_attachment at', 'at.design_id=d.design_id and at.attachment_is_main=1 ','LEFT');
        /* if($user_id > 0)
          {
          //For home page gallery
          $this->db->where('d.user_id',$user_id);
          } */
        $this->db->where('d.design_status', '1');
        //$this->db->where("at.attachment_is_main", 1);
        $this->db->where('d.design_id != ', $design_id);
        
        $this->db->group_by('d.design_id');
        //$this->db->order_by('design_like_count','desc');
        $this->db->order_by('d.design_id', 'desc');
        $this->db->limit($limit);
        $query = $this->db->get();


        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function get_toala_user_design_count($user_id) {
        $this->db->select('d.*,c.category_name');
        $this->db->from('design d');
        $this->db->join('design_category_rel dr', 'd.design_id=dr.design_id');
        $this->db->join('design_category c', 'c.category_id=dr.category_id', 'LEFT');
        $this->db->where('d.user_id', $user_id);
        $this->db->order_by('design_id', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return 0;
    }

    function get_design_result($user_id, $limit, $offset) {

        $this->db->select('d.*,c.category_name');
        $this->db->from('design d');
        $this->db->join('design_category_rel dr', 'd.design_id=dr.design_id');
        $this->db->join('design_category c', 'c.category_id=dr.category_id', 'LEFT');
        $this->db->where('d.user_id', $user_id);
        $this->db->order_by('design_id', 'desc');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function get_one_design($design_id) {
        $this->db->select('d.*,dr.category_id');
        $this->db->from('design d');
        $this->db->join('design_category_rel dr', 'd.design_id=dr.design_id');
        //$this->db->join('design_category c','c.category_id=dr.category_id');
        $this->db->where('d.design_id', $design_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return 0;
        }
    }

    function get_one_design_tag($design_id) {
        $this->db->select('tg.design_tags_id,t.tag_name,tg.tag_id');
        $this->db->from('design_tags tg');
        $this->db->join('tags t', 't.tag_id=tg.tag_id');
        $this->db->where('tg.design_id', $design_id);
        $this->db->group_by('tg.tag_id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            //$query=$this->db->get_where('design_tags',array('design_id'=>$design_id));
            return $query->result();
        }
        return false;
    }

    function get_one_design_images($design_id) {
        $this->db->select('*');
        $this->db->from('design_attachment');
        $this->db->where('design_id', $design_id);
        $this->db->where('attachment_name != ','');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
           
            return $query->result();
        }
    }
    
    
     function get_design_files($design_id) {
        $this->db->select('*');
        $this->db->from('design_attachment');
        $this->db->where('design_id', $design_id);
        $this->db->where('attachment_file != ','');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            
            return $query->row();
        }
    }

    function get_one_design_attachment($attachment_id) {
        $this->db->select('*');
        $this->db->from('design_attachment');
        $this->db->where('attachment_id', $attachment_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
           
            return $query->row();
        }
    }

    function get_one_main_design_image($design_id) {
        $this->db->select('*');
        $this->db->from('design_attachment');
        $this->db->where('attachment_is_main', 1);
        $this->db->where('attachment_name != ','');
        $this->db->where('design_id', $design_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
           
            $res = $query->row();
            return $res->attachment_name;
        }
    }

    function get_total_design_count() {
        $this->db->select('d.*,c.category_name');
        $this->db->from('design d');
        $this->db->join('design_category_rel dr', 'd.design_id=dr.design_id');
        $this->db->join('design_category c', 'c.category_id=dr.category_id', 'LEFT');
        //$this->db->where('d.design_status', 1);
        $this->db->order_by('d.design_id', 'desc');
        //$this->db->limit($limit,$offset);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return 0;
    }

    function get_all_design_result($limit, $offset) {
        $this->db->select('d.*,c.category_name');
        $this->db->from('design d');
        $this->db->join('design_category_rel dr', 'd.design_id=dr.design_id');
        $this->db->join('design_category c', 'c.category_id=dr.category_id', 'LEFT');
        $this->db->order_by('design_id', 'desc');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function get_total_category_design_count($category_id, $keyword) {
        $this->db->select('d.*,c.category_name,u.*,up.*');
        $this->db->from('design d');
        $this->db->join('user u','d.user_id=u.user_id',"LEFT");
	$this->db->join('user_profile up','u.user_id=up.user_id',"LEFT");
        $this->db->join('design_category_rel dr', 'd.design_id=dr.design_id');
        $this->db->join('design_category c', 'c.category_id=dr.category_id', 'LEFT');
        $this->db->join('design_tags dt', 'dt.design_id=d.design_id', 'LEFT');
        $this->db->join('tags tg', 'tg.tag_id=dt.tag_id', 'LEFT');
        $this->db->where('d.design_status', 1);
        $this->db->order_by('design_id', 'desc');
        if ($category_id > 0 && $category_id!='') {
            
            $final_category='';
            if(strstr($category_id,'-')){
                
                $exp_cat=explode('-',$category_id);
                
                if(!empty($exp_cat)){
                    foreach($exp_cat as $cat_id){
                        if($cat_id>0){
                         
                          $final_category.=$this->get_tree_category($cat_id);
                          
                          $final_category.=$cat_id.'-';
                          
                        }
                    }
                    
                    $final_category=substr($final_category, 0,-1);
                }
                
            } else {
                $final_category.=$this->get_tree_category($category_id);
                $final_category.=$category_id.'-';
                
                $final_category=substr($final_category, 0,-1);
            }
            
            if($final_category!=''){
                $cond = "dr.category_id in (" . str_replace("-", ",", $final_category) . ")";
                $this->db->where($cond);
            }
        }
        if ($keyword != '' && $keyword != "0") {
            $this->db->like('d.design_title', $keyword);

            if (substr_count($keyword, ' ') >= 1) {
                $ex = explode(' ', $keyword);

                foreach ($ex as $val) {
                    $this->db->like('d.design_title', $val);
                }
            }

            $this->db->or_like('tg.tag_name', $keyword);
        }
        //$this->db->limit($limit,$offset);
        $this->db->group_by('d.design_id');
        $query = $this->db->get();
        //echo $this->db->last_query();
        //die;
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return 0;
    }

    function get_category_design_result($limit, $offset, $category_id, $keyword) {
        $this->db->select('d.*,c.category_name,u.*,up.*');
        $this->db->from('design d');
        $this->db->join('user u','d.user_id=u.user_id',"LEFT");
	$this->db->join('user_profile up','u.user_id=up.user_id',"LEFT");
        $this->db->join('design_category_rel dr', 'd.design_id=dr.design_id');
        $this->db->join('design_category c', 'c.category_id=dr.category_id', 'LEFT');
        $this->db->join('design_tags dt', 'dt.design_id=d.design_id', 'LEFT');
        $this->db->join('tags tg', 'tg.tag_id=dt.tag_id', 'LEFT');
        $this->db->where('d.design_status', 1);
        $this->db->order_by('design_id', 'desc');
        if ($category_id > 0 && $category_id!='') {
            
            $final_category='';
            if(strstr($category_id,'-')){
                
                $exp_cat=explode('-',$category_id);
                
                if(!empty($exp_cat)){
                    foreach($exp_cat as $cat_id){
                        if($cat_id>0){
                         
                          $final_category.=$this->get_tree_category($cat_id);
                          
                          $final_category.=$cat_id.'-';
                          
                        }
                    }
                    
                    $final_category=substr($final_category, 0,-1);
                }
                
            } else {
                $final_category.=$this->get_tree_category($category_id);
                $final_category.=$category_id.'-';
                
                $final_category=substr($final_category, 0,-1);
            }
            
            if($final_category!=''){
                $cond = "dr.category_id in (" . str_replace("-", ",", $final_category) . ")";
                $this->db->where($cond);
            }
        }


        if ($keyword != '' && $keyword != "0") {

            $this->db->like('d.design_title', $keyword);

            if (substr_count($keyword, ' ') >= 1) {
                $ex = explode(' ', $keyword);

                foreach ($ex as $val) {
                    $this->db->like('d.design_title', $val);
                }
            }
            $this->db->or_like('tg.tag_name', $keyword);
        }
        $this->db->group_by('d.design_id');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();
        
        //echo $this->db->last_query(); die;

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }
    
    function get_tree_category($category_id){
        
        $temp = '';
        $chk_parent = $this->db->query("select category_id,category_parent_id from " . $this->db->dbprefix('design_category') . " where category_status=1 and category_parent_id='" . $category_id . "'");

        if ($chk_parent->num_rows() > 0) {
            $res = $chk_parent->result();
            
            foreach ($res as $rs) {

                $temp.=$rs->category_id. "-";


                    $chk_parent2 = $this->db->query("select category_id from " . $this->db->dbprefix('design_category') . " where category_status=1 and category_parent_id='" . $rs->category_id . "'");

                    if ($chk_parent2->num_rows() > 0) {
                        $temp.=$this->get_tree_category($rs->category_id);
                    }
                
            }
        }


        return $temp;
    }

    function design_buy_process($design_id) {


        $cur_date = date('Y-m-d H:i:s');
        $user_ip = getRealIP();  ///===use custom helper function  getrealIP
        //$picture_sql=$this->db->query(("SELECT * FROM ".$this->db->dbprefix('iweb_design')." where design_id=".$design_id);
       

        $this->db->select('*');
        $this->db->from($this->db->dbprefix('design'));
        $this->db->where('design_id', $design_id);
        $picture_sql = $this->db->get();

        if ($picture_sql->num_rows() > 0) {
            $picture_detail = $picture_sql->row();

            //echo "image points <br /><br />";

            $image_points = $picture_detail->design_point;
            $picture_owner_id = $picture_detail->user_id;

            //		echo "<br /><br />";

            $cur_login_user_id = get_authenticateUserID(); ///==replace with session user id
            //==check balance of user
            $user_cur_balance = getuserpoints($cur_login_user_id);
            //echo "cure bal".$user_cur_balance."imag".$image_points;die;
            if ($user_cur_balance >= $image_points) {

                ///===buyer user debit from wallet
                $data_trans = array('user_id' => $cur_login_user_id,
                    'design_id' => $design_id,
                    'pay_points' => $image_points,
                    'host_ip' => $user_ip,
                    'transaction_date_time' => $cur_date
                );
                $this->db->insert('transaction', $data_trans);

                //$do_transaction=mysql_query("insert into iweb_transaction(user_id,design_id,pay_points,host_ip,transaction_date_time)values('".$cur_login_user_id."','".$design_id."','".$image_points."','".$user_ip."','".$cur_date."')");

                $transaction_id = mysql_insert_id();

                //$do_wallet_transaction=mysql_query("insert into iweb_wallet(user_id,design_id,debit,transaction_id,wallet_ip,wallet_date)values('".$cur_login_user_id."','".$design_id."','".$image_points."','".$transaction_id."','".$user_ip."','".$cur_date."')");

                $data_wallet = array('user_id' => $cur_login_user_id,
                    'design_id' => $design_id,
                    'debit' => $image_points,
                    'transaction_id' => $transaction_id,
                    'wallet_ip' => $user_ip,
                    'wallet_date' => $cur_date
                );
                $this->db->insert('wallet', $data_wallet);
                ///===end buying
                
                
                
                
                //====get user tree for recursion payment
                $files_user_ids = array();

                if ($image_points > 0) {
                    if ($picture_detail->parent_design_id > 0 && $picture_detail->design_is_modified == 1) {

                        $files_user_ids = get_all_picture_tree($picture_detail->parent_design_id);
                    }
                }

                //echo "files_user_ids <br /><br />";
                //print_r($files_user_ids); //die;
                ///===add current design user id
                $all_ids[] = $picture_owner_id;
                $final_ids = array_merge($all_ids, $files_user_ids);
                ///===total users id===
                //echo "<br /><br />total users ids<br /><br />";
                //print_r($final_ids);
                ///====commission settings
                $admin_design_point_commission = 20; ///in %
                $commission_setting = commission_setting();

                if (!empty($commission_setting)) {
                    $admin_design_point_commission = $commission_setting->admin_commission_image;
                }

                $user_total_point_commission = 100 - $admin_design_point_commission;


                ///=====assign and split percentage to user
                //echo "total pay to users<br /><br />";
                $pay_to_user = splitampunt($user_total_point_commission, $final_ids);
                //print_r($pay_to_user);
                ////====total pay to individual users=
                //echo "total pay to all individual users not admin<br /><br />";
                $final_pay_to_user = $pay_to_user;
                //print_r($final_pay_to_user);  ///die;
                //====count admin points===
                $admin_points = countpoints($image_points, $admin_design_point_commission);

                foreach ($final_pay_to_user as $user_id => $user_perc) {

                    $user_image_points = countpoints($image_points, $user_perc);


                    //====insert into db=====
                    //$check_user_exit=mysql_query("select * from iweb_user where user_id=".$user_id);
                    $this->db->select('*');
                    $this->db->from('user');
                    $this->db->where('user_id', $user_id);
                    $check_user_exit = $this->db->get();

                    if ($check_user_exit->num_rows() > 0) {

                        //$do_transaction=mysql_query("insert into iweb_transaction(parent_transaction_id,user_id,design_id,pay_points,host_ip,transaction_date_time)values('".$transaction_id."','".$user_id."','".$design_id."','".$user_image_points."','".$user_ip."','".$cur_date."')");
                        $do_transaction = array('parent_transaction_id' => $transaction_id,
                            'user_id' => $user_id,
                            'design_id' => $design_id,
                            'pay_points' => $user_image_points,
                            'host_ip' => $user_ip,
                            'transaction_date_time' => $cur_date
                        );
                        $this->db->insert('transaction', $do_transaction);

                        $last_transaction_id = mysql_insert_id();

                        //$do_wallet_transaction=mysql_query("insert into iweb_wallet(user_id,design_id,credit,transaction_id,wallet_ip,wallet_date)values('".$user_id."','".$design_id."','".$user_image_points."','".$last_transaction_id."','".$user_ip."','".$cur_date."')");

                        $do_wallet_transaction = array('user_id' => $user_id,
                            'design_id' => $design_id,
                            'credit' => $user_image_points,
                            'transaction_id' => $last_transaction_id,
                            'wallet_ip' => $user_ip,
                            'wallet_date' => $cur_date
                        );
                        $this->db->insert('wallet', $do_wallet_transaction);
                    } else {
                        ///==if user not found than transaction points to admin		
                        $user_id = $admin_id;
                        $admin_points = $admin_points + $user_image_points;
                    }
                } //===foreach users
                ///===pay to admin wallet====
                if ($admin_points != '') {
                    $is_point = 1;

                    //mysql_query("insert into iweb_wallet_admin(credit,is_point,wallet_date,transaction_id,design_id)values('".$admin_points."','".$is_point."','".$cur_date."','".$transaction_id."','".$design_id."')");	

                    $pay_admin_wallet_transaction = array('credit' => $admin_points,
                        'is_point' => $is_point,
                        'wallet_date' => $cur_date,
                        'transaction_id' => $transaction_id,
                        'design_id' => $design_id
                    );
                    $this->db->insert('wallet_admin', $pay_admin_wallet_transaction);
                }///===pay to admin wallet====
                
                
                
                
             
                
                
                 ///===Notification to owner
                $data_notification = array('to_user_id' => $picture_owner_id,
                    'design_id' => $design_id,
                    'act' => 'YOUR_DESIGN_BUY',
                    'is_read' => 1,
                    'notification_date' => $cur_date
                );
                $this->db->insert('system_notification', $data_notification);
                
                
                $owner_user_info = get_user_profile_by_id($picture_owner_id);
            
            
            
           
            
            
            
            // owner
           $email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Design Owner Purchase Notification'");
           $email_temp=$email_template->row();				

           $email_from_name=$email_temp->from_name;

           $email_address_from=$email_temp->from_address;
           $email_address_reply=$email_temp->reply_address;

           $email_subject=$email_temp->subject;				
           $email_message=$email_temp->message;

            $email_to = $owner_user_info->email;



        $email_message=str_replace('{break}','<br/>',$email_message);
        $email_message=str_replace('{user_name}',$owner_user_info->full_name,$email_message);
        $email_message=str_replace('{design_title}',$picture_detail->design_title,$email_message);
        $email_message=str_replace('{design_point}',$picture_detail->design_point,$email_message);        
        
      

        $str=$email_message;

        /** custom_helper email function **/

        email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
                
                
            ///===Notification to buyer
            $data_notification = array('to_user_id' => get_authenticateUserID(),
                'design_id' => $design_id,
                'act' => 'DESIGN_BUY',
                'is_read' => 1,
                'notification_date' => date("Y-m-d H:i:s")
            );
            $this->db->insert('system_notification', $data_notification);
            
            
            $buyer_user_info = get_user_profile_by_id(get_authenticateUserID());
            
            
            
           $download_link=site_url('user/designdownload');
            
            
            
            // buyer
           $email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Design Purchase Notification'");
           $email_temp=$email_template->row();				

           $email_from_name=$email_temp->from_name;

           $email_address_from=$email_temp->from_address;
           $email_address_reply=$email_temp->reply_address;

           $email_subject=$email_temp->subject;				
           $email_message=$email_temp->message;

            $email_to = $buyer_user_info->email;



        $email_message=str_replace('{break}','<br/>',$email_message);
        $email_message=str_replace('{user_name}',$buyer_user_info->full_name,$email_message);
        $email_message=str_replace('{design_title}',$picture_detail->design_title,$email_message);
        $email_message=str_replace('{design_point}',$picture_detail->design_point,$email_message);        
        $email_message=str_replace('{download_link}',$download_link,$email_message);
      

        $str=$email_message;

        /** custom_helper email function **/

        email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
                //// *************** end ******************/////	
            

        
        
        

                return 1;
            } //==if balance
            else {
                return "NO_BALANCE";
                die;
            }
        } //===if design get
    }

    function chk_track_view($design_id, $user_id) {
        $cur_date = date('Y-m-d');

        $query = $this->db->query("select * from " . $this->db->dbprefix('design_view') . " where design_id = " . $design_id . " and user_id = " . $user_id . " and DATE_FORMAT(view_date,'%Y-%m-%d') = '" . $cur_date . "'");

        if ($query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function chk_track_other_view($design_id) {
        $cur_date = date('Y-m-d');

        $query = $this->db->query("select * from " . $this->db->dbprefix('design_view') . " where design_id = " . $design_id . " and view_ip = '" . getRealIP() . "' and DATE_FORMAT(view_date,'%Y-%m-%d') = '" . $cur_date . "'");

        if ($query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function set_design_view($design_id) {


        $user_id = 0;
        $chk_track_view = 1;
        if (get_authenticateUserID() > 0) {
            $user_id = get_authenticateUserID();
        }
        if ($user_id > 0) {
            $chk_track_view = $this->chk_track_view($design_id, $user_id);
        }
        if ($chk_track_view == 0) {

            $sql_res = "update " . $this->db->dbprefix('design') . " set `design_view_count` = `design_view_count` + 1 where design_id =" . $design_id;
            $this->db->query($sql_res);

            $data = array('design_id' => $design_id,
                'user_id' => $user_id,
                'view_date' => date('Y-m-d h:i:s'),
                'view_ip' => getRealIP()
            );
            $this->db->insert('design_view', $data);
        } else {
            $chk_track_other_view = $this->chk_track_other_view($design_id);

            if ($chk_track_other_view == 0) {
                $sql_res = "update " . $this->db->dbprefix('design') . " set `design_view_count` = `design_view_count` + 1 where design_id =" . $design_id;
                $this->db->query($sql_res);

                $data = array('design_id' => $design_id,
                    'user_id' => 0,
                    'view_date' => date('Y-m-d h:i:s'),
                    'view_ip' => getRealIP()
                );
                $this->db->insert('design_view', $data);
            }
        }
    }

    function check_like_track($user_id, $design_id) {

        $query = $this->db->query("select * from " . $this->db->dbprefix('design_like') . " where design_id = " . $design_id . " and user_id = " . $user_id);

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return 0;
        }
    }

    function check_like_status($like_status, $user_id, $design_id) {
        $query = $this->db->query("select * from " . $this->db->dbprefix('design_like') . " where design_id = " . $design_id . " and like_status = " . $like_status . " and user_id = " . $user_id);

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return 0;
        }
    }

    function set_like_dislike($like_status, $design_id) {

        //$chk_track_like  = 1;
        //like
        $chk_like = $this->check_like_track(get_authenticateUserID(), $design_id);

        if ($chk_like) {
            $data = array('design_id' => $design_id,
                'user_id' => get_authenticateUserID(),
                'like_date' => date('Y-m-d h:i:s'),
                'like_status' => $like_status,
                'like_ip' => getRealIP()
            );
            $this->db->where(array('user_id' => get_authenticateUserID(), 'design_id' => $design_id));
            $this->db->update('design_like', $data);


            if ($like_status == 1) {
                $sql_res = "update " . $this->db->dbprefix('design') . " set `design_like_count` = `design_like_count` + 1 where design_id =" . $design_id;
            } else {
                $sql_res = "update " . $this->db->dbprefix('design') . " set `design_like_count` = `design_like_count` - 1 where design_id =" . $design_id;
            }

            $this->db->query($sql_res);
        } else {

            $data = array('design_id' => $design_id,
                'user_id' => get_authenticateUserID(),
                'like_date' => date('Y-m-d h:i:s'),
                'like_status' => $like_status,
                'like_ip' => getRealIP()
            );
            $this->db->insert('design_like', $data);
            $sql_res = "update " . $this->db->dbprefix('design') . " set `design_like_count` = `design_like_count` + 1 where design_id =" . $design_id;
            $this->db->query($sql_res);
        }
    }

    function get_all_images_of_design($design_id = 0) {
        $this->db->select('*');
        $this->db->from('design_attachment');
        $this->db->where('design_id', $design_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return 0;
    }
    
    
    ///==============design category part===
    
    
    
    function get_design_category_child_by_id($category_id){
        $query = $this->db->query("select * from " . $this->db->dbprefix('design_category') . " where (category_status=1 or (category_status=0 and category_user_id='".get_authenticateUserID()."') ) and category_parent_id='" . $category_id . "'");
        
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }
    

    function insert_design_category($category_name,$category_parent_id) {

       
        $category_url_name = clean_url($category_name);


        $chk_url_exists = $this->db->query("select MAX(category_url_name) as category_url_name from " . $this->db->dbprefix('design_category') . " where category_url_name like '" . $category_url_name . "%'");

        if ($chk_url_exists->num_rows() > 0) {
            $get_pr = $chk_url_exists->row();

            $strre = '0';
            if ($get_pr->category_url_name != '') {
                $strre = str_replace($category_url_name, '', $get_pr->category_url_name);
            }

            if ($strre == '0') {
                $newcnt = '';
            } elseif ($strre == '') {
                $newcnt = '1';
            } else {
                $newcnt = (int) $strre + 1;
            }

            $category_url_name = $category_url_name . $newcnt;
        }

        $data = array('category_name' => $category_name,
            'category_url_name' => $category_url_name,
            'category_parent_id' => $category_parent_id,
            'category_description' => $category_name,            
            'category_status' => 1,
            'category_user_id'=>get_authenticateUserID()
            
        );

        $this->db->insert('design_category', $data);
    }

}

?>