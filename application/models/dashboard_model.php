<?php
class Dashboard_model extends IWEB_Model 
{

	/*
	Function name :Dashboard_model
	Description :its default constuctor which called when Dashboard_model object initialzie.its load necesary parent constructor
	*/
	function Dashboard_model()
    {
        parent::__construct();	
    }
	
	function get_all_draft_trip($user_id,$limit)
	{	
		$id = array(2,3);
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('trip');
		$CI->db->where('trip.user_id',$user_id);		
		$CI->db->where_in('trip.trip_status',$id);		
		$CI->db->order_by('trip.trip_id','desc');	
		$CI->db->limit($limit,0);		

		$query=$CI->db->get();

		if($query->num_rows()>0)
		{
			return $query->result();
		}
		return 0;
	}
	
	function get_last_trip($user_id,$limit)
	{	
	
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('trip');
		$this->db->join('user','trip.user_id=user.user_id');
		$this->db->join('user_profile','trip.user_id=user_profile.user_id','left');
		$this->db->join('currency_code','trip.currency_code_id=currency_code.currency_code_id','left');
		$CI->db->where('trip.user_id',$user_id);	
		//$CI->db->where('trip.trip_activity_status !=',3);	
		$CI->db->order_by('trip.trip_activity_status','desc');	
		$CI->db->limit($limit,0);		

		$query=$CI->db->get();

		if($query->num_rows()>0)
		{
			return $query->result();
		}
		return 0;
	}
} ?>