<?php
/*********************************************************************************
 * This the taskrabbitclone.com  by Rockers Technology. is paid software. It is released under the terms of 
 * the following BSD License.
 * 
 *  Rockers Technologies (Head Office)
 *    5038,Berthpage Dr
 *    suwanee, GA. Zip Code : 30024
    
 *    E-mail Address : nishu@rockersinfo.com
 * 
 * Copyright © 2012-2020 by Rockers Technology , INC a domestic profit corporation has been duly incorporated under
the laws of the state of georiga , USA. www.rockersinfo.com
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this 
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this 
 *   list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.

 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Travelagent_model extends IWEB_Model 
{

	/*
	Function name :User_model
	Description :its default constuctor which called when user_model object initialzie.its load necesary parent constructor
	*/
	function Travelagent_model()
    {
        parent::__construct();	
    } 
	
	function get_agent_city_wise($city_name='',$limit='',$offset='')
	{
	
		$query = $this->db->query("select * from ".$this->db->dbprefix('user')." u,".$this->db->dbprefix('user_profile')." up,".$this->db->dbprefix('agent_profile')." ap where u.user_id=up.user_id and up.user_id = ap.user_id and u.user_status=1 and u.verify_email = 1 and ap.agent_app_approved =1 and ap.agency_city ='".$city_name."' order by ap.agent_level desc limit ".$limit." offset ".$offset."");
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
	}
	

	function get_total_agent_city_wise($city_name='',$limit='',$offset='')
	{
		
		
		$query = $this->db->query("select * from ".$this->db->dbprefix('user')." u,".$this->db->dbprefix('user_profile')." up,".$this->db->dbprefix('agent_profile')." ap where u.user_id=up.user_id and up.user_id = ap.user_id and u.user_status=1 and u.verify_email = 1 and ap.agent_app_approved =1 and ap.agency_city ='".$city_name."'");
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;
	}
	
	function get_total_keyword_agent_city_wise($city_name='',$keyword='')
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","@","(",")",":",";",">","<","/"),'',$keyword));
		
		
		
		$query = $this->db->query("select * from ".$this->db->dbprefix('user')." u,".$this->db->dbprefix('user_profile')." up,".$this->db->dbprefix('agent_profile')." ap where u.user_id=up.user_id and up.user_id = ap.user_id and u.user_status=1 and u.verify_email = 1 and ap.agent_app_approved =1 and ap.agency_city ='".$city_name."' and (u.first_name like '%".$keyword."%' or u.last_name like '%".$keyword."%')");
	

		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;
	}
	
	function get_agent_keyword_city_wise($city_name='',$keyword='',$limit='',$offset='')
	{
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","@","(",")",":",";",">","<","/"),'',$keyword));

		$query = $this->db->query("select * from ".$this->db->dbprefix('user')." u,".$this->db->dbprefix('user_profile')." up,".$this->db->dbprefix('agent_profile')." ap where u.user_id=up.user_id and up.user_id = ap.user_id and (u.first_name like '%".$keyword."%' or u.last_name like '%".$keyword."%') and u.user_status=1 and u.verify_email = 1 and ap.agent_app_approved =1 and ap.agency_city ='".$city_name."' order by ap.agent_level desc limit ".$limit." offset ".$offset."");
		
		//echo $this->db->last_query();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
	}
	
	function check_agent_detail($user_id)
	{
		
		$query=$this->db->get_where('agent_profile',array('user_id'=>$user_id,'agent_app_approved'=>1));
		
		if($query->num_rows()>0)
		{
			return $query->row();
		}
		
		return 0;
		
	}
	
	
	function became_travelagent()
	{
		
		
		$profile_name =$this->input->post('agency_name');
		$profile_name=clean_url($profile_name);
		
			
	    $chk_url_exists=$this->db->query("select MAX(profile_name) as profile_name from ".$this->db->dbprefix('user')." where user_id!='".get_authenticateUserID()."' and profile_name like '".$profile_name."%'");
		
		if($chk_url_exists->num_rows()>0)
		{			
				$get_pr=$chk_url_exists->row();					
				
				$strre='0';
				if($get_pr->profile_name!='')
				{
					$strre=str_replace($profile_name,'',$get_pr->profile_name);
				}
				
				if($strre=='0') 
				{					
					$newcnt=''; 
					  				
				} 
				elseif($strre=='') 
				{					
					$newcnt='1';   				
				}
				else
				{				
					$newcnt=(int)$strre+1;			
				}
				
			 	$profile_name=$profile_name.$newcnt;
						
		}
		
		
		
		$memberassociate = $this->input->post('agent_member_of_association');
		///print_r($memberassociate);
		
		if($memberassociate)
		{
			$memberassociate = implode(',',$this->input->post('agent_member_of_association'));
		}
		else
		{
			$memberassociate = '';
		}
		
		$agency_location = explode(',',$this->input->post('agency_location'));
		
		$agency_city='';
		$agency_state='';
		$agency_country='';
		
		if(isset($agency_location[0]))
		{
			$agency_city=$agency_location[0];
		}
		if(isset($agency_location[1]))
		{
			$agency_state=$agency_location[1];
		}
		if(isset($agency_location[2]))
		{
			$agency_country=$agency_location[2];
		}
		
		
		$agentdata = array(
			'user_id' => get_authenticateUserID(),
			'agency_name'=>$this->input->post('agency_name'),		
			'contact_person_name'=>$this->input->post('contact_person_name'),
			'agency_location'=>$this->input->post('agency_location'),	
			'agency_city'=>$agency_city,
			'agency_state'=>$agency_state,
			'agency_country'=>$agency_country,
			'agency_contact_email'=>$this->input->post('agency_contact_email'),
			'agency_phone'=>$this->input->post('agency_phone'),
			'agency_customer_support_no'=>$this->input->post('agency_customer_support_no'),
			'agency_website'=>seturl($this->input->post('agency_website')),
			'agent_member_of_association'=>$memberassociate,
			'agent_status' =>1,
			'agent_date' =>date('Y-m-d H:i:s'),
			'agent_ip' =>getRealIP(),
		);
		//echo '<pre>'; print_r($agentdata); die(); 
		$this->db->insert('agent_profile', $agentdata);
		$agent_id = mysql_insert_id();
		
		$data_user=array(
			'profile_name'=>$profile_name,			
			);
			$this->db->where('user_id',get_authenticateUserID());
			$this->db->update('user',$data_user);
		/*******upload document files start ****/
			
			
		if($_FILES['file_up']['name']!='')
		{

			$cnt=1; 
			
			$this->load->library('upload');
			$rand=randomCode(); 
				
			for($i=0;$i<count($_FILES['file_up']['name']);$i++)
			 {
			 
				if($_FILES['file_up']['name'][$i]!='')
				{
				
					$_FILES['userfile']['name']    =   $_FILES['file_up']['name'][$i];
					$_FILES['userfile']['type']    =   $_FILES['file_up']['type'][$i];
					$_FILES['userfile']['tmp_name']=   $_FILES['file_up']['tmp_name'][$i];
					$_FILES['userfile']['error']   =   $_FILES['file_up']['error'][$i];
					$_FILES['userfile']['size']    =   $_FILES['file_up']['size'][$i]; 

					$config['file_name']     = $rand.'_agent_document_'.get_authenticateUserID().'_'.$i;
					$config['upload_path'] =base_path().'upload/agent_doc/';					
					$config['allowed_types'] = 'jpg|jpeg|gif|png|pdf';

					$this->upload->initialize($config);

					if (!$this->upload->do_upload())
					{		
					 $error =  $this->upload->display_errors(); 
					} 
					
					$picture = $this->upload->data();

					$agentdata_doc=array(					
						'user_id'=>get_authenticateUserID(),
						'agent_id'=>$agent_id,
						'agent_document'=>$picture['file_name'],
						'document_date'=>date('Y-m-d H:i:s')					
					);
					$this->db->insert('agent_document',$agentdata_doc);
				}										
			}
		}
		/****upload document files end ****/		
		
	}
	
	
	function edit_agent_profile()
	{
		
		
		
		$memberassociate = $this->input->post('agent_member_of_association');
		///print_r($memberassociate);
		
		if($memberassociate)
		{
			$memberassociate = implode(',',$this->input->post('agent_member_of_association'));
		}
		else
		{
			$memberassociate = '';
		}
		
		$agency_location = explode(',',$this->input->post('agency_location'));
		
		$agency_city='';
		$agency_state='';
		$agency_country='';
		
		if(isset($agency_location[0]))
		{
			$agency_city=$agency_location[0];
		}
		if(isset($agency_location[1]))
		{
			$agency_state=$agency_location[1];
		}
		if(isset($agency_location[2]))
		{
			$agency_country=$agency_location[2];
		}
		
		
		$profile_name =$this->input->post('agency_name');
		$profile_name=clean_url($profile_name);
		
			
	    $chk_url_exists=$this->db->query("select MAX(profile_name) as profile_name from ".$this->db->dbprefix('user')." where user_id!='".get_authenticateUserID()."' and profile_name like '".$profile_name."%'");
		
		if($chk_url_exists->num_rows()>0)
		{			
				$get_pr=$chk_url_exists->row();					
				
				$strre='0';
				if($get_pr->profile_name!='')
				{
					$strre=str_replace($profile_name,'',$get_pr->profile_name);
				}
				
				if($strre=='0') 
				{					
					$newcnt=''; 
					  				
				} 
				elseif($strre=='') 
				{					
					$newcnt='1';   				
				}
				else
				{				
					$newcnt=(int)$strre+1;			
				}
				
			 	$profile_name=$profile_name.$newcnt;
						
		}
		
		
		$agentdata = array(
			'agency_name'=>$this->input->post('agency_name'),		
			'contact_person_name'=>$this->input->post('contact_person_name'),
			'agency_location'=>$this->input->post('agency_location'),	
			'agency_city'=>$agency_city,
			'agency_state'=>$agency_state,
			'agency_country'=>$agency_country,
			'agency_contact_email'=>$this->input->post('agency_contact_email'),
			'agency_phone'=>$this->input->post('agency_phone'),
			'agency_customer_support_no'=>$this->input->post('agency_customer_support_no'),
			'agency_website'=>seturl($this->input->post('agency_website')),
			'agent_member_of_association'=>$memberassociate,
			'agent_update_date' =>date('Y-m-d H:i:s')
		);
		//echo '<pre>'; print_r($agentdata); die(); 
		$this->db->where('user_id',get_authenticateUserID());
		$this->db->update('agent_profile', $agentdata);
		
		
		$data_user=array(
			'profile_name'=>$profile_name,			
			);
			$this->db->where('user_id',get_authenticateUserID());
			$this->db->update('user',$data_user);
		/*******upload document files start ****/
			
			
		if($_FILES['file_up']['name']!='')
		{

			$cnt=1; 
			
			$this->load->library('upload');
			$rand=randomCode(); 
				
			for($i=0;$i<count($_FILES['file_up']['name']);$i++)
			 {
			 
				if($_FILES['file_up']['name'][$i]!='')
				{
				
					$_FILES['userfile']['name']    =   $_FILES['file_up']['name'][$i];
					$_FILES['userfile']['type']    =   $_FILES['file_up']['type'][$i];
					$_FILES['userfile']['tmp_name']=   $_FILES['file_up']['tmp_name'][$i];
					$_FILES['userfile']['error']   =   $_FILES['file_up']['error'][$i];
					$_FILES['userfile']['size']    =   $_FILES['file_up']['size'][$i]; 

					$config['file_name']     = $rand.'_agent_document_'.get_authenticateUserID().'_'.$i;
					$config['upload_path'] =base_path().'upload/agent_doc/';					
					$config['allowed_types'] = 'jpg|jpeg|gif|png|pdf';

					$this->upload->initialize($config);

					if (!$this->upload->do_upload())
					{		
					 $error =  $this->upload->display_errors(); 
					} 
					
					$picture = $this->upload->data();

					$agentdata_doc=array(					
						'user_id'=>get_authenticateUserID(),
						'agent_id'=>$this->input->post('agent_id'),
						'agent_document'=>$picture['file_name'],
						'document_date'=>date('Y-m-d H:i:s')					
					);
					$this->db->insert('agent_document',$agentdata_doc);
				}										
			}
		}
		/****upload document files end ****/		
		
	}
	
	function get_total_agent_multicity_wise($city='',$keyword='all',$type,$mcnt)
	{
		
		$city_name = explode(',',$city);
		
		
	//// 1= for keyword   2= last login desc   3= desc of added date  4= asc of added date
	
		
		if($type==1)
		{		
			$this->db->select('*');
			$this->db->from('user us');
			$this->db->join('user_profile up','us.user_id=up.user_id');
			$this->db->join('agent_profile ag','us.user_id=ag.user_id');
			
			$this->db->where('us.user_status',1);
			$this->db->where('us.verify_email',1);
			
			if($keyword != 'all')
			{
				//$this->db->like('us.first_name',$keyword);
				//$this->db->or_like('us.last_name',$keyword);
				$this->db->where('ag.agency_name like "%'.$keyword.'%"');	
			}
		
			//$this->db->where('ag.agent_status',1);
			$this->db->where('ag.agent_app_approved',1);
			
			if(count($city_name)==1)
			{
				$this->db->like('agency_city',$city_name[0]);
			}
			
			else//if(count($city_name)>1)
			{
				foreach($city_name as $val)
				{
					$this->db->or_like('agency_city',$val);
				}	
			}						
			
			$this->db->order_by('ag.agent_id',"desc");
			$query = $this->db->get();	
		}
		
		
		elseif($type==2)
		{		
			$this->db->select('*');
			$this->db->from('user us');
			$this->db->join('user_profile up','us.user_id=up.user_id');
			$this->db->join('agent_profile ag','us.user_id=ag.user_id');
			//$this->db->join('user_login ulog','us.user_id=ulog.user_id','left');
			
			$this->db->where('us.user_status',1);
			$this->db->where('us.verify_email',1);
		
			if($keyword != 'all')
			{
				//$this->db->like('us.first_name',$keyword);
				//$this->db->or_like('us.last_name',$keyword);
				$this->db->where('ag.agency_name like "%'.$keyword.'%"');
			}
		
			//$this->db->where('ag.agent_status',1);
			$this->db->where('ag.agent_app_approved',1);
			
			if(count($city_name)==1)
			{
				$this->db->like('agency_city',$city_name[0]);
			}
			
			else//if(count($city_name)>1)
			{
				foreach($city_name as $val)
				{
					$this->db->or_like('agency_city',$val);
				}	
			}						
			
			
			$this->db->order_by('(select login_date_time from '.$this->db->dbprefix("user_login").' where user_id=us.user_id order by login_date_time desc limit 1)','desc');
			
			$query = $this->db->get();	
		}
		
		
		elseif($type==3)
		{		
			$this->db->select('*');
			$this->db->from('user us');
			$this->db->join('user_profile up','us.user_id=up.user_id');
			$this->db->join('agent_profile ag','us.user_id=ag.user_id');
			
			$this->db->where('us.user_status',1);
			$this->db->where('us.verify_email',1);
			
			if($keyword != 'all')
			{
				//$this->db->like('us.first_name',$keyword);
				//$this->db->or_like('us.last_name',$keyword);
				$this->db->where('ag.agency_name like "%'.$keyword.'%"');		
			}
		
			//$this->db->where('ag.agent_status',1);
			$this->db->where('ag.agent_app_approved',1);
			
			if(count($city_name)==1)
			{
				$this->db->like('agency_city',$city_name[0]);
			}
			
			else//if(count($city_name)>1)
			{
				foreach($city_name as $val)
				{
					$this->db->or_like('agency_city',$val);
				}	
			}						
			
			$this->db->order_by('ag.agent_id',"desc");
			$query = $this->db->get();	
		}
		
		
		elseif($type==4)
		{		
			$this->db->select('*');
			$this->db->from('user us');
			$this->db->join('user_profile up','us.user_id=up.user_id');
			$this->db->join('agent_profile ag','us.user_id=ag.user_id');
			
			$this->db->where('us.user_status',1);
			$this->db->where('us.verify_email',1);
			
			if($keyword != 'all')
			{
				//$this->db->like('us.first_name',$keyword);
				//$this->db->or_like('us.last_name',$keyword);
				$this->db->where('ag.agency_name like "%'.$keyword.'%"');	
			}
		
			//$this->db->where('ag.agent_status',1);
			$this->db->where('ag.agent_app_approved',1);
			
			if(count($city_name)==1)
			{
				$this->db->like('agency_city',$city_name[0]);
			}
			
			else//if(count($city_name)>1)
			{
				foreach($city_name as $val)
				{
					$this->db->or_like('agency_city',$val);
				}	
			}						
			
			$this->db->order_by('ag.agent_id',"asc");
			
			$query = $this->db->get();	
		}
		
		elseif($type==5)
		{		
			/*$this->db->select('*');
			$this->db->from('user us');
			$this->db->join('user_profile up','us.user_id=up.user_id');
			$this->db->join('agent_profile ag','us.user_id=ag.user_id');
			
			$this->db->where('us.user_status',1);
			
			if($keyword != 'all')
			{
				//$this->db->like('us.first_name',$keyword);
				//$this->db->or_like('us.last_name',$keyword);
				$this->db->like('ag.agency_name',$keyword);			
			}
		
			//$this->db->where('ag.agent_status',1);
			$this->db->where('ag.agent_app_approved',1);
			
			if(count($city_name)==1)
			{
				$this->db->like('agency_city',$city_name[0]);
			}
			
			else//if(count($city_name)>1)
			{
				foreach($city_name as $val)
				{
					$this->db->or_like('agency_city',$val);
				}	
			}*/						
			
			$arr=array();
			
			$cstr='';
			
			if($mcnt==0 || $mcnt==9)
			{
				for($i=0;$i<=9;$i++)
				{
					$arr[]=$i;
					$cstr .= "'".$i."',";
				}
			
			}
			else
			{
				
				for($i=$mcnt;$i<=9;$i++)
				{
					$arr[]=$i;
					$cstr .= "'".$i."',";
				}
				
				for($i=0;$i<=$mcnt-1;$i++)
				{
					$arr[]=$i;
					$cstr .= "'".$i."',";
				}
				
				
			}
			$mycnt= implode(',',$arr);
			
			$cstr = substr($cstr,0,-1);
			
		/*	$custom_field='FIELD(SUBSTRING(ag.agent_id,-1),'.$mycnt.')';
			
			$this->db->order_by($custom_field,'desc');*/

			//$this->db->order_by('ag.agent_id','desc');
			
			$city_cond='';
			
			
			if(count($city_name)==1)
			{
				$city_cond .= " AND ag.agency_city LIKE '%".$city_name[0]."%' ";
			}
			
			else
			{
				$city_cond .= " AND ( ";
				$i=1;
				foreach($city_name as $val)
				{
					
					if($i==count($city_name))
					{
						$city_cond .= " ag.agency_city LIKE '%".$val."%' ";
					} else {
						$city_cond .= " ag.agency_city LIKE '%".$val."%' OR ";
					}
					$i++;
				}	
				
				$city_cond .= " ) ";
			}	
			
			
			$key_cond='';
			
			if($keyword != 'all')
			{
				$key_cond = " and ag.agency_name like '%".$keyword."%' ";			
			}
			
			
			
			$query = $this->db->query("SELECT * FROM ".$this->db->dbprefix('user us')." JOIN ".$this->db->dbprefix('user_profile up')." ON us.user_id=up.user_id JOIN ".$this->db->dbprefix('agent_profile ag')." ON us.user_id=ag.user_id WHERE us.user_status = 1 and    us.verify_email = 1 AND ag.agent_app_approved = 1 ".$key_cond.$city_cond." ORDER BY FIELD(SUBSTRING(ag.agent_id, -1),".$mycnt.") desc");
				
		}
		
		else
		{		
			$this->db->select('*');
			$this->db->from('user us');
			$this->db->join('user_profile up','us.user_id=up.user_id');
			$this->db->join('agent_profile ag','us.user_id=ag.user_id');
			
			$this->db->where('us.user_status',1);
			$this->db->where('us.verify_email',1);
			
			if($keyword != 'all')
			{
				//$this->db->like('us.first_name',$keyword);
				//$this->db->or_like('us.last_name',$keyword);
				$this->db->where('ag.agency_name like "%'.$keyword.'%"');		
			}
		
			//$this->db->where('ag.agent_status',1);
			$this->db->where('ag.agent_app_approved',1);
			
			if(count($city_name)==1)
			{
				$this->db->like('agency_city',$city_name[0]);
			}
			
			else//if(count($city_name)>1)
			{
				foreach($city_name as $val)
				{
					$this->db->or_like('agency_city',$val);
				}	
			}						
			
			$this->db->order_by('ag.agent_id',"desc");
			
			$query = $this->db->get();
		}
		
		
		
		
		
		if ($query->num_rows() > 0) {
		
			return $query->num_rows();
		}
		return 0;
		
	}
	function get_agent_multicity_wise($city='',$keyword='all',$limit,$offset,$type,$mcnt)
	{
		
		$city_name = explode(',',$city);
		
		
		
		
		//// 1= for keyword   2= last login desc   3= desc of added date  4= asc of added date
	
		
		if($type==1)
		{		
			$this->db->select('*');
			$this->db->from('user us');
			$this->db->join('user_profile up','us.user_id=up.user_id');
			$this->db->join('agent_profile ag','us.user_id=ag.user_id');
			
			$this->db->where('us.user_status',1);
			$this->db->where('us.verify_email',1);
			
			if($keyword != 'all')
			{
				//$this->db->like('us.first_name',$keyword);
				//$this->db->or_like('us.last_name',$keyword);
				$this->db->where('ag.agency_name like "%'.$keyword.'%"');	
			}
		
			//$this->db->where('ag.agent_status',1);
			$this->db->where('ag.agent_app_approved',1);
			
			if(count($city_name)==1)
			{
				$this->db->like('agency_city',$city_name[0]);
			}
			
			else//if(count($city_name)>1)
			{
				foreach($city_name as $val)
				{
					$this->db->or_like('agency_city',$val);
				}	
			}						
			
			$this->db->order_by('ag.agent_id',"desc");
			
			$this->db->limit($limit,$offset);
		
			$query = $this->db->get();
				
		}
		
		
		elseif($type==2)
		{		
			$this->db->select('*');
			$this->db->from('user us');
			$this->db->join('user_profile up','us.user_id=up.user_id');
			$this->db->join('agent_profile ag','us.user_id=ag.user_id');
			//$this->db->join('user_login ulog','us.user_id=ulog.user_id','left');
			
			$this->db->where('us.user_status',1);
			$this->db->where('us.verify_email',1);
		
			if($keyword != 'all')
			{
				//$this->db->like('us.first_name',$keyword);
				//$this->db->or_like('us.last_name',$keyword);
				$this->db->where('ag.agency_name like "%'.$keyword.'%"');			
			}
		
			//$this->db->where('ag.agent_status',1);
			$this->db->where('ag.agent_app_approved',1);
			
			if(count($city_name)==1)
			{
				$this->db->like('agency_city',$city_name[0]);
			}
			
			else//if(count($city_name)>1)
			{
				foreach($city_name as $val)
				{
					$this->db->or_like('agency_city',$val);
				}	
			}						
			
			
			$this->db->order_by('(select login_date_time from '.$this->db->dbprefix("user_login").' where user_id=us.user_id order by login_date_time desc limit 1)','desc');
			
			$this->db->limit($limit,$offset);
		
			$query = $this->db->get();
				
		}
		
		
		elseif($type==3)
		{		
			$this->db->select('*');
			$this->db->from('user us');
			$this->db->join('user_profile up','us.user_id=up.user_id');
			$this->db->join('agent_profile ag','us.user_id=ag.user_id');
			
			$this->db->where('us.user_status',1);
			$this->db->where('us.verify_email',1);
			
			if($keyword != 'all')
			{
				//$this->db->like('us.first_name',$keyword);
				//$this->db->or_like('us.last_name',$keyword);
				$this->db->where('ag.agency_name like "%'.$keyword.'%"');
			}
		
			//$this->db->where('ag.agent_status',1);
			$this->db->where('ag.agent_app_approved',1);
			
			if(count($city_name)==1)
			{
				$this->db->like('agency_city',$city_name[0]);
			}
			
			else//if(count($city_name)>1)
			{
				foreach($city_name as $val)
				{
					$this->db->or_like('agency_city',$val);
				}	
			}						
			
			$this->db->order_by('ag.agent_id',"desc");
			
			$this->db->limit($limit,$offset);
		
			$query = $this->db->get();
				
		}
		
		
		elseif($type==4)
		{		
			$this->db->select('*');
			$this->db->from('user us');
			$this->db->join('user_profile up','us.user_id=up.user_id');
			$this->db->join('agent_profile ag','us.user_id=ag.user_id');
			
			$this->db->where('us.user_status',1);
			$this->db->where('us.verify_email',1);
			
			if($keyword != 'all')
			{
				//$this->db->like('us.first_name',$keyword);
				//$this->db->or_like('us.last_name',$keyword);
				$this->db->where('ag.agency_name like "%'.$keyword.'%"');		
			}
		
			//$this->db->where('ag.agent_status',1);
			$this->db->where('ag.agent_app_approved',1);
			
			if(count($city_name)==1)
			{
				$this->db->like('agency_city',$city_name[0]);
			}
			
			else//if(count($city_name)>1)
			{
				foreach($city_name as $val)
				{
					$this->db->or_like('agency_city',$val);
				}	
			}						
			
			$this->db->order_by('ag.agent_id',"asc");
			
			$this->db->limit($limit,$offset);
		
			$query = $this->db->get();
				
		}
		
		
		elseif($type==5)
		{		
			/*$this->db->select('*');
			$this->db->from('user us');
			$this->db->join('user_profile up','us.user_id=up.user_id');
			$this->db->join('agent_profile ag','us.user_id=ag.user_id');
			
			$this->db->where('us.user_status',1);
			
			if($keyword != 'all')
			{
				//$this->db->like('us.first_name',$keyword);
				//$this->db->or_like('us.last_name',$keyword);
				$this->db->like('ag.agency_name',$keyword);			
			}
		
			//$this->db->where('ag.agent_status',1);
			$this->db->where('ag.agent_app_approved',1);
			
			if(count($city_name)==1)
			{
				$this->db->like('agency_city',$city_name[0]);
			}
			
			else//if(count($city_name)>1)
			{
				foreach($city_name as $val)
				{
					$this->db->or_like('agency_city',$val);
				}	
			}*/						
			
			$arr=array();
			
			$cstr='';
			
			if($mcnt==0 || $mcnt==9)
			{
				for($i=0;$i<=9;$i++)
				{
					$arr[]=$i;
					$cstr .= "'".$i."',";
				}
			
			}
			else
			{
				
				for($i=$mcnt;$i<=9;$i++)
				{
					$arr[]=$i;
					$cstr .= "'".$i."',";
				}
				
				for($i=0;$i<=$mcnt-1;$i++)
				{
					$arr[]=$i;
					$cstr .= "'".$i."',";
				}
				
				
			}
			$mycnt= implode(',',$arr);
			
			$cstr = substr($cstr,0,-1);
			
		/*	$custom_field='FIELD(SUBSTRING(ag.agent_id,-1),'.$mycnt.')';
			
			$this->db->order_by($custom_field,'desc');*/

			//$this->db->order_by('ag.agent_id','desc');
			
			$city_cond='';
			
			
			if(count($city_name)==1)
			{
				$city_cond .= " AND ag.agency_city LIKE '%".$city_name[0]."%' ";
			}
			
			else
			{
				$city_cond .= " AND ( ";
				$i=1;
				foreach($city_name as $val)
				{
					
					if($i==count($city_name))
					{
						$city_cond .= " ag.agency_city LIKE '%".$val."%' ";
					} else {
						$city_cond .= " ag.agency_city LIKE '%".$val."%' OR ";
					}
					$i++;
				}	
				
				$city_cond .= " ) ";
			}	
			
			
			$key_cond='';
			
			if($keyword != 'all')
			{
				$key_cond = " and ag.agency_name like '%".$keyword."%' ";			
			}
			
			
			
			$query = $this->db->query("SELECT * FROM ".$this->db->dbprefix('user us')." JOIN ".$this->db->dbprefix('user_profile up')." ON us.user_id=up.user_id JOIN ".$this->db->dbprefix('agent_profile ag')." ON us.user_id=ag.user_id WHERE us.user_status = 1 and  us.verify_email = 1 AND ag.agent_app_approved = 1 ".$key_cond.$city_cond." ORDER BY FIELD(SUBSTRING(ag.agent_id, -1),".$mycnt.") desc LIMIT ".$limit." offset ".$offset);
				
		}
		
		else
		{		
			$this->db->select('*');
			$this->db->from('user us');
			$this->db->join('user_profile up','us.user_id=up.user_id');
			$this->db->join('agent_profile ag','us.user_id=ag.user_id');
			
			$this->db->where('us.user_status',1);
			$this->db->where('us.verify_email',1);
			
			if($keyword != 'all')
			{
				//$this->db->like('us.first_name',$keyword);
				//$this->db->or_like('us.last_name',$keyword);
				$this->db->where('ag.agency_name like "%'.$keyword.'%"');			
			}
		
			//$this->db->where('ag.agent_status',1);
			$this->db->where('ag.agent_app_approved',1);
			
			if(count($city_name)==1)
			{
				$this->db->like('agency_city',$city_name[0]);
			}
			
			else//if(count($city_name)>1)
			{
				foreach($city_name as $val)
				{
					$this->db->or_like('agency_city',$val);
				}	
			}						
			
			$this->db->order_by('ag.agent_id',"desc");
			
			$this->db->limit($limit,$offset);
		
			$query = $this->db->get();
				
		}
		
		
		

		//echo $this->db->last_query();


		if ($query->num_rows() > 0) {
		
			return $query->result();
		}
		return 0;
	}

}
?>