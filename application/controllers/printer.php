<?php

class Printer extends IWEB_Controller {
    /*
      Function name :User()
      Description :Its Default Constuctor which called when user object initialzie.its load necesary models
     */

    function Printer() {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->model('user_model');
        $this->load->model('printer_model');
        //$this->load->model('video_model');
    }

    /*
      Function name :index()
      Parameter :none
      Return : none
      Use : redirect to user dashboard
      Description : none
     */

    public function index($slug='', $msg='') {


        if (trim($slug) == '') {
            redirect('printer/search');
        }


        $store_detail = $this->printer_model->get_one_store_by_slug($slug);


        if (empty($store_detail)) {

            redirect('home/404');
        }





        $data['active_menu'] = '3dprinter';

        $site_setting = site_setting();
        $data['site_setting'] = $site_setting;
        $data['error'] = "";
        $data['msg'] = $msg;

        $data['store_detail'] = $store_detail;

        $data['design_by'] = '';
        $data['design_by_profile_name'] = '';
        $desing_user_info = get_user_profile_by_id($store_detail->user_id);
        if (!empty($desing_user_info)) {
            $data['design_by'] = ucwords($desing_user_info->full_name);
            $data['design_by_profile_name'] = ucwords($desing_user_info->profile_name);
        }


        $store_view = $this->printer_model->set_store_view($store_detail->store_id);

        $review_limit = 10;
        $review_offset = 0;

        $data['store_reviews'] = $this->printer_model->get_store_all_review($store_detail->store_id, $review_limit, $review_offset);



        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        $data['theme'] = $theme;

        $meta_setting = meta_setting();
        $pageTitle = ucfirst(stripslashes($store_detail->store_name)) . ' ' . $site_setting->site_name;
        $metaDescription = ucfirst(stripslashes($store_detail->store_name)) . ',' . $store_detail->store_address . ',' . $store_detail->store_ctiy . ',' . $store_detail->store_state . '-' . $store_detail->store_postal_code . ',' . $store_detail->store_country;
        $metaKeyword = ucfirst(stripslashes($store_detail->store_name)) . ' ' . $site_setting->site_name;

        $this->template->write('pageTitle', $pageTitle, TRUE);
        $this->template->write('metaDescription', $metaDescription, TRUE);
        $this->template->write('metaKeyword', $metaKeyword, TRUE);
        $this->template->write_view('header', $theme . '/layout/common/header_home', $data, TRUE);
        $this->template->write_view('content_center', $theme . '/layout/printer/view_printer', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
        $this->template->render();
    }
    
    
    public function loadmore($store_id,$review_offset=0){
        
         if ('XMLHttpRequest' == @$_SERVER['HTTP_X_REQUESTED_WITH']) {
            $store_detail = $this->printer_model->get_one_store($store_id);

            if (!empty($store_detail)) {
                
                
               
                $theme = getThemeName();
                
                $data['store_detail'] = $store_detail;

                $review_limit = 10;
               

                $data['store_reviews'] = $this->printer_model->get_store_all_review($store_id, $review_limit, $review_offset);

               
                echo $this->load->view($theme . '/layout/printer/loadmore',$data, TRUE);
            } else {
                    echo "nothing";
            }
            die;
        } else {
            redirect('printer/search');
        }
    }

    public function post_review() {

        if ('XMLHttpRequest' == @$_SERVER['HTTP_X_REQUESTED_WITH']) {
            
            
           
            

            $data['error'] = '';
            $data["msg"] = '';
            
            $review_id=0;
            $parent_review_id=0;
            $is_owner='';
           
            if(get_authenticateUserID()>0){
            if($_POST){
                $store_id=(int) $this->input->post('store_id');
                $review_id=$this->input->post('review_id');    ////0=for iinsert and greater than 0 is update
                
                $preview_id=$this->input->post('preview_id'); 
            
            
      $store_detail = $this->printer_model->check_exist_store($store_id, 0);
      
        if (empty($store_detail)) {
            $data['error'] = '3D Printer not Found.';
        } else {
            
            
            if($review_id>0){
                $check_review_owner=$this->printer_model->check_review_owner($store_id,$review_id,get_authenticateUserID()); 
                if(isset($check_review_owner) && !empty($check_review_owner)){
                    $parent_review_id=$check_review_owner->parent_review_id;
                } else {
                    $is_owner='not';
                }
            }
            
           if($is_owner=='not'){
               $data['error'] = 'Review record not Found.';
           } else {
            
            $this->form_validation->set_rules('rating', 'Rating', 'trim|required|is_natural');
            $this->form_validation->set_rules('store_id', 'Store', 'trim|required|is_natural');
            $this->form_validation->set_rules('review_id', 'Review', 'trim|required|is_natural');
            $this->form_validation->set_rules('review_desc', 'Comment', 'trim|required|min_length[10]|max_length[100]');

            

            if ($this->form_validation->run() == FALSE) {
                if (validation_errors()) {
                    $data["error"] = validation_errors();
                } else {
                    $data["error"] = "";
                }

               
                    $data["rating"] = $this->input->post('rating');
                    $data["review_desc"] = $this->input->post('review_desc');
                    $data["store_id"] = $this->input->post('store_id');
                    $data["review_id"] = $this->input->post('review_id'); 
                    $data["review_type"] = $this->input->post('review_type'); 
                    $data["preview_id"] = $this->input->post('preview_id'); 
               
            } else {
                            

                    $res = $this->printer_model->place_review($parent_review_id);
                    if ($res == 1) {
                        $data['msg'] = 'success';

                    } else {
                        $data['error'] = 'Faile to add Review. Please try again.';
                    }
                
            }
        }   }
        
        } } else {
            $data['error'] = 'Please take a login to write a review.';
        }
            echo json_encode($data);
            exit;
        } else {
            redirect('printer/search');
        }
    }

    /*
      Function name :index()
      Parameter :none
      Return : none
      Use : redirect to user dashboard
      Description : none
     */

    public function all_review($store_id, $limit='20', $offset=0, $msg='') {
        if (!check_user_authentication()) {
            redirect('login');
        }

        $check_store_detail = $this->printer_model->check_exist_store($store_id, get_authenticateUserID());

        if (!$check_store_detail) {
            redirect('printer/all');
        }

        $store_detail = $this->printer_model->get_one_store($store_id);

        $data['store_detail'] = $store_detail;
        $data['active_menu'] = '3dprinter';

        $site_setting = site_setting();
        $data['site_setting'] = $site_setting;
        $data['error'] = "";
        $data['msg'] = $msg;

        $this->load->library('pagination');
        $config['uri_segment'] = '4';
        $config['base_url'] = site_url('printer/all_review/' . $store_id . '/' . $limit . '/');
        $config['total_rows'] = $this->printer_model->get_user_store_review_count($store_id);

        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        $data['page_link'] = $this->pagination->create_links();
        $data['result'] = $this->printer_model->get_user_store_review($store_id, $limit, $offset);


        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        $data['theme'] = $theme;

        $meta_setting = meta_setting();
        $pageTitle = 'Review on 3D Printer- ' . $meta_setting->title;
        $metaDescription = '3D Printer - ' . $meta_setting->meta_description;
        $metaKeyword = '3D Printer - ' . $meta_setting->meta_keyword;

        $this->template->write('pageTitle', $pageTitle, TRUE);
        $this->template->write('metaDescription', $metaDescription, TRUE);
        $this->template->write('metaKeyword', $metaKeyword, TRUE);
        $this->template->write_view('header', $theme . '/layout/common/header_theme', $data, TRUE);
        $this->template->write_view('content_center', $theme . '/layout/printer/list_printer_review', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer_theme', $data, TRUE);
        $this->template->render();
    }

    public function delete_review_me($store_id, $store_review_id) {
        if (!check_user_authentication()) {
            redirect('login');
        }

        $store_detail = $this->printer_model->check_exist_store($store_id, get_authenticateUserID());

        if (!$store_detail) {
            redirect('printer/all');
        }
        
        $store_detail = $this->printer_model->get_one_store($store_id);
        
        if ($store_review_id != '' && $store_review_id > 0) {

            $this->printer_model->delete_store_review($store_id, $store_review_id);

            redirect('printer/'.$store_detail->store_slug.'/delete');
        } else {
            redirect('user/dashboard');
        }
    }

    /*
      Function name :index()
      Parameter :none
      Return : none
      Use : redirect to user dashboard
      Description : none
     */

    public function all($limit='20', $offset=0, $msg='') {
        if (!check_user_authentication()) {
            redirect('login');
        }

        $data['active_menu'] = '3dprinter';

        $site_setting = site_setting();
        $data['site_setting'] = $site_setting;
        $data['error'] = "";
        $data['msg'] = $msg;

        $this->load->library('pagination');
        $config['uri_segment'] = '4';
        $config['base_url'] = site_url('printer/all/' . $limit . '/');
        $config['total_rows'] = $this->printer_model->get_user_location_count(get_authenticateUserID());

        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        $data['page_link'] = $this->pagination->create_links();
        $data['result'] = $this->printer_model->get_user_location(get_authenticateUserID(), $limit, $offset);

        //  $user_notification=$this->user_model->get_user_notification(get_authenticateUserID());
        //$data['user_notification']=$user_notification;


        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        $data['theme'] = $theme;

        $meta_setting = meta_setting();
        $pageTitle = '3D Printer- ' . $meta_setting->title;
        $metaDescription = '3D Printer - ' . $meta_setting->meta_description;
        $metaKeyword = '3D Printer - ' . $meta_setting->meta_keyword;

        $this->template->write('pageTitle', $pageTitle, TRUE);
        $this->template->write('metaDescription', $metaDescription, TRUE);
        $this->template->write('metaKeyword', $metaKeyword, TRUE);
        $this->template->write_view('header', $theme . '/layout/common/header_theme', $data, TRUE);
        $this->template->write_view('content_center', $theme . '/layout/printer/list_printer', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer_theme', $data, TRUE);
        $this->template->render();
    }

    function add_me() {
        if (!check_user_authentication()) {
            redirect('login');
        }
        $data = array();
        $data['site_setting'] = site_setting();
        $data['active_menu'] = "3dprinter";
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        $data['theme'] = $theme;
        $data["error"] = "";
        $meta_setting = meta_setting();

        $data['store_category'] = $this->printer_model->get_all_store_category();

        if ($_POST) {
            $this->form_validation->set_rules('store_name', 'Store Name', 'trim|required');
            $this->form_validation->set_rules('store_address', 'Address', 'trim|required');
            $this->form_validation->set_rules('store_ctiy', 'City', 'trim|required');
            $this->form_validation->set_rules('store_state', 'State', 'required');
            $this->form_validation->set_rules('store_country', 'Country', 'required');
            $this->form_validation->set_rules('store_postal_code', 'Postal Code', 'required|alpha_postal_code');
            //$this->form_validation->set_rules('store_lat', 'Lattitude', 'required');
            //$this->form_validation->set_rules('store_long', 'Longitude', 'required');
            $this->form_validation->set_rules('store_email', 'Email', 'required|valid_email');

            if ($this->input->post('store_url') != '') {
                $this->form_validation->set_rules('store_url', 'Valid URL', 'valid_url');
            }

            if ($this->form_validation->run() == FALSE) {
                if (validation_errors()) {
                    $data["error"] = validation_errors();
                } else {
                    $data["error"] = "";
                }

                $data['store_name'] = $this->input->post('store_name');
                $data['store_address'] = $this->input->post('store_address');
                $data['store_address2'] = $this->input->post('store_address2');
                $data['store_ctiy'] = $this->input->post('store_ctiy');
                $data['store_state'] = $this->input->post('store_state');
                $data['store_postal_code'] = $this->input->post('store_postal_code');
                $data['store_country'] = $this->input->post('store_country');
                $data['store_lat'] = $this->input->post('store_lat');
                $data['store_long'] = $this->input->post('store_long');
                $data['store_email'] = $this->input->post('store_email');
                $data['store_phone'] = $this->input->post('store_phone');
                $data['store_url'] = $this->input->post('store_url');
                $data['store_id'] = $this->input->post('store_id');
                //$data['store_status']=$this->input->post('store_status');
                $data['category_id'] = $this->input->post('category_id');
            } else {

                if ($this->input->post('store_id') != '') {
                    $result = $this->printer_model->update_location();
                    redirect('printer/all/20/0/update_location');
                } else {
                    $result = $this->printer_model->insert_location();
                    redirect('printer/all/20/0/success_location');
                }
            }
        } else {
            $data['store_name'] = $this->input->post('store_name');
            $data['store_address'] = $this->input->post('store_address');
            $data['store_address2'] = $this->input->post('store_address2');
            $data['store_ctiy'] = $this->input->post('store_ctiy');
            $data['store_state'] = $this->input->post('store_state');
            $data['store_postal_code'] = $this->input->post('store_postal_code');
            $data['store_country'] = $this->input->post('store_country');
            $data['store_lat'] = $this->input->post('store_lat');
            $data['store_long'] = $this->input->post('store_long');
            $data['store_email'] = $this->input->post('store_email');
            $data['store_phone'] = $this->input->post('store_phone');
            $data['store_url'] = $this->input->post('store_url');
            $data['store_id'] = $this->input->post('store_id');
            //$data['store_status']=$this->input->post('store_status');
            $data['category_id'] = $this->input->post('category_id');
        }

        $pageTitle = '3D Printer - ' . $meta_setting->title;
        $metaDescription = '3D Printer - ' . $meta_setting->meta_description;
        $metaKeyword = '3D Printer - ' . $meta_setting->meta_keyword;

        $this->template->write('pageTitle', $pageTitle, TRUE);
        $this->template->write('metaDescription', $metaDescription, TRUE);
        $this->template->write('metaKeyword', $metaKeyword, TRUE);
        $this->template->write_view('header', $theme . '/layout/common/header_theme', $data, TRUE);
        $this->template->write_view('content_center', $theme . '/layout/printer/add_printer', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer_theme', $data, TRUE);
        $this->template->render();
    }

    function edit_me($store_id='') {

        if (!check_user_authentication()) {
            redirect('login');
        }

        $store_detail = $this->printer_model->check_exist_store($store_id, get_authenticateUserID());

        if (!$store_detail) {
            redirect('printer/all');
        }

        if ($store_id != '' && $store_id > 0) {

            $data = array();
            $data['site_setting'] = site_setting();
            $data['active_menu'] = "3dprinter";
            $theme = getThemeName();
            $this->template->set_master_template($theme . '/template.php');

            $data['theme'] = $theme;
            $data['store_category'] = $this->printer_model->get_all_store_category();
            $data['error'] = '';
            $one_location = $this->printer_model->get_one_store($store_id);

            $data['store_id'] = $one_location->store_id;
            $data['store_name'] = stripslashes($one_location->store_name);
            $data['store_address'] = stripslashes($one_location->store_address);
            $data['store_address2'] = stripslashes($one_location->store_address2);
            $data['store_ctiy'] = stripslashes($one_location->store_ctiy);
            $data['store_state'] = stripslashes($one_location->store_state);
            $data['store_postal_code'] = stripslashes($one_location->store_postal_code);
            $data['store_country'] = stripslashes($one_location->store_country);
            $data['store_lat'] = $one_location->store_lat;
            $data['store_long'] = $one_location->store_long;
            $data['store_email'] = $one_location->store_email;
            $data['store_phone'] = $one_location->store_phone;
            $data['store_url'] = $one_location->store_url;
            //$data['store_status']=$one_location->store_status;
            $data['category_id'] = $one_location->category_id;

            $meta_setting = meta_setting();

            $pageTitle = '3D-Printer- ' . $meta_setting->title;
            $metaDescription = '3D-Printer - ' . $meta_setting->meta_description;
            $metaKeyword = '3D-Printer - ' . $meta_setting->meta_keyword;

            $this->template->write('pageTitle', $pageTitle, TRUE);
            $this->template->write('metaDescription', $metaDescription, TRUE);
            $this->template->write('metaKeyword', $metaKeyword, TRUE);
            $this->template->write_view('header', $theme . '/layout/common/header_theme', $data, TRUE);
            $this->template->write_view('content_center', $theme . '/layout/printer/add_printer', $data, TRUE);
            $this->template->write_view('footer', $theme . '/layout/common/footer_theme', $data, TRUE);
            $this->template->render();
        } else {
            redirect('user/dashboard');
        }
    }

    function delete_me($store_id) {

        if (!check_user_authentication()) {
            redirect('login');
        }

        $store_detail = $this->printer_model->check_exist_store($store_id, get_authenticateUserID());

        if (!$store_detail) {
            redirect('printer/all');
        }

        if ($store_id != '' && $store_id > 0) {

            $delete_store_rel = $this->db->query("delete from " . $this->db->dbprefix('store_category_rel') . " where store_id='" . $store_id . "'");
            $delete_store_review = $this->db->query("delete from " . $this->db->dbprefix('store_review') . " where store_id='" . $store_id . "'");
            $delete_store = $this->db->query("delete from " . $this->db->dbprefix('store') . " where store_id='" . $store_id . "'");

            redirect('printer/all/20/0/delete_location');
        } else {
            redirect('user/dashboard');
        }
    }

    function email_check($store_email) {

        $cemail = $this->printer_model->emailTaken($store_email);

        if ($cemail) {
            $this->form_validation->set_message('email_check', 'There is an existing account associated with this email.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function search() {
        $data = array();
        $data['site_setting'] = site_setting();
        $data['active_menu'] = "3dprinter";
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');
        $data['theme'] = $theme;
        $meta_setting = meta_setting();
        $data['result'] = ''; //$this->printer_model->get_printer_result();


        /* $this->session->set_userdata('visitor_user_postal_code', $this->security->xss_clean(''));
          $this->session->set_userdata('visitor_user_lat', $this->security->xss_clean(''));
          $this->session->set_userdata('visitor_user_lon', $this->security->xss_clean('')); */

        $data['store_category'] = $this->printer_model->get_all_store_category();

        $pageTitle = '3D-Printer - ' . $meta_setting->title;
        $metaDescription = '3D-Printer - ' . $meta_setting->meta_description;
        $metaKeyword = '3D-Printer - ' . $meta_setting->meta_keyword;

        $this->template->write('pageTitle', $pageTitle, TRUE);
        $this->template->write('metaDescription', $metaDescription, TRUE);
        $this->template->write('metaKeyword', $metaKeyword, TRUE);
        $this->template->write_view('header', $theme . '/layout/common/header_home', $data, TRUE);
        $this->template->write_view('content_center', $theme . '/layout/printer/search', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
        $this->template->render();
    }

    function getstore() {
        $postal_code = '';
        $visitor_lat = '';
        $visitor_lon = '';

        $distance_range = 10000;

        if ($_POST) {
            if (isset($_POST['lat'])) {
                $visitor_lat = $_POST['lat'];
            }
            if (isset($_POST['lon'])) {
                $visitor_lon = $_POST['lon'];
            }
            if (isset($_POST['postal_code'])) {
                $postal_code = $_POST['postal_code'];
            }
            if (isset($_POST['distance_range'])) {
                $distance_range = $_POST['distance_range'];
            }
        }

        $result = $this->printer_model->get_printer_geo_result($distance_range, $postal_code, $visitor_lat, $visitor_lon);

        if (!empty($result)) {

            foreach ($result as $key => $res) {

                $result[$key]['store_name'] = ucfirst($res['store_name']);
                $result[$key]['store_slug'] = site_url('printer/' . $res['store_slug']);

                $result[$key]['store_total_rating'] = roundDownToHalf($res['store_total_rating']);

                if ($res['store_url'] != '') {
                    $result[$key]['store_url'] = addhttp($res['store_url']);
                    $result[$key]['store_domain'] = get_domain_name($res['store_url']);
                } else {
                    $result[$key]['store_url'] = '';
                    $result[$key]['store_domain'] = '';
                }

                if (file_exists(base_path() . 'upload/category_orig/' . $res['category_image']) && $res['category_image'] != '') {
                    $result[$key]['category_image'] = base_url() . 'upload/category_orig/' . $res['category_image'];
                }
            }
        }

        echo json_encode($result);
        die;
    }

    function getgeolocate($latitude='', $longitude='') {
        $postal_code = '';
        $visitor_lat = '';
        $visitor_lon = '';

        $result_arr = array();

        if ($_POST) {
            if (isset($_POST['lat'])) {
                $visitor_lat = $_POST['lat'];
            }
            if (isset($_POST['lon'])) {
                $visitor_lon = $_POST['lon'];
            }
        } elseif ($latitude != '' && $longitude != '') {
            $visitor_lat = $latitude;
            $visitor_lon = $longitude;
        }

        if ($visitor_lat != '' && $visitor_lon != '') {

            $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . $visitor_lat . "," . $visitor_lon . '&sensor=true';


            if (function_exists('curl_init')) {
                $defaults = array(
                    CURLOPT_HEADER => 0,
                    CURLOPT_URL => $url,
                    CURLOPT_FRESH_CONNECT => 1,
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_FORBID_REUSE => 1,
                    CURLOPT_TIMEOUT => 15
                );

                $ch = curl_init();
                curl_setopt_array($ch, $defaults);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result, true);

                $postal_code = $this->checkpostalcode($response);
            } else if (function_exists('file_get_contents')) {

                $response = json_decode(file_get_contents($url), true);
                $postal_code = $this->checkpostalcode($response);
            }
        }

        $result_arr['postal_code'] = $postal_code;
        $result_arr['lat'] = $visitor_lat;
        $result_arr['lon'] = $visitor_lon;

        /* $this->session->set_userdata('visitor_user_postal_code', $this->security->xss_clean(''));
          $this->session->set_userdata('visitor_user_lat', $this->security->xss_clean(''));
          $this->session->set_userdata('visitor_user_lon', $this->security->xss_clean('')); */

        $this->session->set_userdata('visitor_user_postal_code', $postal_code);
        $this->session->set_userdata('visitor_user_lat', $visitor_lat);
        $this->session->set_userdata('visitor_user_lon', $visitor_lon);

        echo json_encode($result_arr);
        die;
        //return $postal_code;
    }

    function checkpostalcode($response=array()) {
        //echo "<pre>";
        //print_r($response);
        $postal_code = '';

        if (isset($response['status']) && strtolower($response['status']) == 'ok') {
            if (isset($response['results']) && !empty($response['results'])) {
                $result = $response['results'];
                foreach ($result as $key => $val) {
                    if (isset($val['address_components']) && !empty($val['address_components'])) {
                        $address_components = $val['address_components'];

                        foreach ($address_components as $akey => $aval) {
                            if (isset($aval['types']) && !empty($aval['types'])) {
                                $types = $aval['types'];
                                foreach ($types as $tkey => $tval) {
                                    if ($tval == 'postal_code') {
                                        if (isset($aval['long_name']) && $aval['long_name'] != '') {
                                            $postal_code = $aval['long_name'];
                                            break;
                                        } elseif (isset($aval['short_name']) && $aval['short_name'] != '') {
                                            $postal_code = $aval['short_name'];
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //die;
        return $postal_code;
    }

    function getapplocate() {
        $postal_code = '';
        $visitor_lat = '';
        $visitor_lon = '';

        $result_arr = array();


        include("geoip/geoipcity.inc");
        include("geoip/geoipregionvars.php");

        $geo = geoip_open("geoip/GeoLiteCity.dat", GEOIP_STANDARD);

        $ip = getRealIP(); //'71.9.127.141'; //'14.139.245.144'; //'184.106.196.45'; //'208.111.40.148'; //


        if ($ip != '') {
            $record = geoip_record_by_addr($geo, $ip);

            //echo "<pre>";
            //print_r($record);
            //die;
            //$city = $record->city;
            //$state = $record->region;

            $loc_set = 0;
            if ($_POST) {
                if (isset($_POST['lat']) && $_POST['lat'] != '' && isset($_POST['lon']) && $_POST['lon'] != '') {
                    $visitor_lat = $_POST['lat'];
                    $visitor_lon = $_POST['lon'];
                    $loc_set = 1;
                }
            }

            if (isset($record->latitude) && $record->latitude != '' && isset($record->longitude) && $record->longitude != '' && $loc_set == 0) {
                $visitor_lat = $record->latitude;
                $visitor_lon = $record->longitude;
            }


            if (isset($record->postal_code) && $record->postal_code != '') {
                $postal_code = $record->postal_code;
            } else {
                $postal_code = $this->getgeolocate($visitor_lat, $visitor_lon);
            }
        }

        $result_arr['postal_code'] = $postal_code;
        $result_arr['lat'] = $visitor_lat;
        $result_arr['lon'] = $visitor_lon;


        /* $this->session->set_userdata('visitor_user_postal_code', $this->security->xss_clean(''));
          $this->session->set_userdata('visitor_user_lat', $this->security->xss_clean(''));
          $this->session->set_userdata('visitor_user_lon', $this->security->xss_clean('')); */


        $this->session->set_userdata('visitor_user_postal_code', $postal_code);
        $this->session->set_userdata('visitor_user_lat', $visitor_lat);
        $this->session->set_userdata('visitor_user_lon', $visitor_lon);

        echo json_encode($result_arr);
        die;
    }

}

?>