<?php
class Challenge extends IWEB_Controller {
	
	/*
	Function name :Dashboard()
	Description :Its Default Constuctor which called when home object initialzie.its load necesary models
	*/
	
	function Challenge()
	{
		parent::__construct();	
		$this->load->model('home_model');	
		$this->load->model('user_model');	
		$this->load->model('challenge_model');	
		
			
	}
	
	function index($slug='',$msg='')
	{
			
		$chkchallenge = check_challenge_exist($slug);
		if(!$chkchallenge)
		{
			redirect('home/404');
			
		}
		else
		{
		
			$challenge_id = $chkchallenge->challenge_id; 
			
			$challenge_detail =  $this->challenge_model->check_exist_challenge($challenge_id,0);
			
			$data = array();
			$data['site_setting']=site_setting();
			$data['msg']=$msg;
			$data['active_menu']="3dchallenge";
			$theme = getThemeName();
			$this->template->set_master_template($theme .'/template.php');
			
			$data['theme']=$theme;
			$data['category'] = get_parent_category();
			$data['challenge_slug']=$slug;
			$data['allchallenge'] = get_allchallenge();
			
			//Set challenge view count
			$challenge_view = $this->challenge_model->set_challenge_view($challenge_id);
			
			$one_challenge = $this->challenge_model->get_one_challenge($challenge_id);
			
			$get_one_challenge_tag = $this->challenge_model->get_one_challenge_tag($challenge_id);
			//$data['challenge_images']= $this->challenge_model->get_one_challenge_images($challenge_id);
			
			$data['user_challenge']= $this->challenge_model->get_user_challenge($one_challenge->user_id,4);
			$data['popular_challenge']= $this->challenge_model->get_popular_challenge(4);
			
			/*if($get_one_challenge_tag)
			{
				$challenge_tag ='';
				foreach($get_one_challenge_tag as $gt)
				{
					$challenge_tag .= $gt->tag_name.',';
				}
				
			}*/
			
			$data['one_challenge']=$one_challenge;
			$data['get_one_challenge_tag']=$get_one_challenge_tag;
			$data['main_image'] =  $this->challenge_model->get_one_main_challenge_image($challenge_id);
			$data['user_id']=$one_challenge->user_id;
			$data['challenge_id']=$one_challenge->challenge_id;
			//$data['challenge_tag'] = substr($challenge_tag,0,-1);
			$data['challenge_title']=$one_challenge->challenge_title;
			$data['challenge_specification']=$one_challenge->challenge_specification;
			$data['challenge_criteria']=$one_challenge->challenge_criteria;
			$data['challenge_requirements']=$one_challenge->challenge_requirements;
			$data['category_id']=$one_challenge->category_id;
			$data['challenge_point']=$one_challenge->challenge_point;
			$data['challenge_meta_keyword']=$one_challenge->challenge_meta_keyword;
			$data['challenge_meta_description']=$one_challenge->challenge_meta_description;
			$data['challenge_date']=$one_challenge->challenge_date;
			$data['challenge_end_date']=$one_challenge->challenge_end_date;
			$data['challenge_view_count']=$one_challenge->challenge_view_count;
			$data['challenge_like_count']=$one_challenge->challenge_like_count;
		    
			$meta_setting=meta_setting();
			
			$pageTitle=$one_challenge->challenge_title.'-'.$meta_setting->title;
			$metaDescription=$one_challenge->challenge_title.'-'.$meta_setting->meta_description;
			$metaKeyword=$one_challenge->challenge_title.'-'.$meta_setting->meta_keyword;
			
			$this->template->write('pageTitle',$pageTitle,TRUE);
			$this->template->write('metaDescription',$metaDescription,TRUE);
			$this->template->write('metaKeyword',$metaKeyword,TRUE);
			$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
			$this->template->write_view('content_center',$theme .'/layout/challenge/view_challenge',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();	
		}
		//if(!check_user_authentication()) {  redirect('login'); } 
		 //redirect('challenge/all'); 
		
	}
	
	
	function running($limit='20',$offset=0,$msg='')
	{
		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3dchallenge_running";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['msg']=$msg;
		
		$this->load->library('pagination');
		$config['uri_segment']='4';
		$config['base_url'] = base_url().'challenge/all/'.$limit.'/';
		$config['total_rows']= $this->challenge_model->get_total_user_challenge_running_count(get_authenticateUserID());
	
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->challenge_model->get_challenge_running_result(get_authenticateUserID(),$limit,$offset);
		
		$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		$pageTitle='3D-challenge Hive- '.$meta_setting->title;
		$metaDescription='3D-challenge Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-challenge Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/challenge/list_3dchallenge_running',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	
	}
	
	function all($limit='20',$offset=0,$msg='')
	{
		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3dchallenge";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['msg']=$msg;
		
		$this->load->library('pagination');
		$config['uri_segment']='4';
		$config['base_url'] = base_url().'challenge/all/'.$limit.'/';
		$config['total_rows']= $this->challenge_model->get_toala_user_challenge_count(get_authenticateUserID());
	
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->challenge_model->get_challenge_result(get_authenticateUserID(),$limit,$offset);
		
		$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		$pageTitle='3D-challenge Hive- '.$meta_setting->title;
		$metaDescription='3D-challenge Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-challenge Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/challenge/list_3dchallenge_hive',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	
	}
	/*
	 * fucnton : open
	 * list all open task of users
	 * 
	 * */
	function open($limit='20',$offset=0,$msg='')
	{
		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3dchallenge";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['msg']=$msg;
		
		$this->load->library('pagination');
		$config['uri_segment']='4';
		$config['base_url'] = base_url().'challenge/open/'.$limit.'/';
		$config['total_rows']= $this->challenge_model->get_total_open_challenge_count(get_authenticateUserID());
	
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->challenge_model->get_open_challenge_list(get_authenticateUserID(),$limit,$offset);
		
		$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		$pageTitle='3D-challenge Hive- '.$meta_setting->title;
		$metaDescription='3D-challenge Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-challenge Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/challenge/list_open_3dchallenge',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	}

    /*
	 * fucnton : open
	 * list all open task of users
	 * 
	 * */
	function assigned($limit='20',$offset=0,$msg='')
	{
		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3dchallenge";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['msg']=$msg;
		
		$this->load->library('pagination');
		$config['uri_segment']='4';
		$config['base_url'] = base_url().'challenge/assigned/'.$limit.'/';
		$config['total_rows']= $this->challenge_model->get_total_assigned_challenge_count(get_authenticateUserID());
	
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->challenge_model->get_assigned_challenge_list(get_authenticateUserID(),$limit,$offset);
		
		$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		$pageTitle='3D-challenge Hive- '.$meta_setting->title;
		$metaDescription='3D-challenge Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-challenge Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/challenge/list_assigned_3dchallenge',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	}


   function closed($limit='20',$offset=0,$msg='')
	{
		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3dchallenge";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['msg']=$msg;
		
		$this->load->library('pagination');
		$config['uri_segment']='4';
		$config['base_url'] = base_url().'challenge/closed/'.$limit.'/';
		$config['total_rows']= $this->challenge_model->get_total_closed_challenge_count(get_authenticateUserID());
	
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->challenge_model->get_closed_challenge_list(get_authenticateUserID(),$limit,$offset);
		
		$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		$pageTitle='3D-challenge Hive- '.$meta_setting->title;
		$metaDescription='3D-challenge Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-challenge Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/challenge/list_closed_3dchallenge',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	}


    function loss($limit='20',$offset=0,$msg='')
	{
		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3dchallenge";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['msg']=$msg;
		
		$this->load->library('pagination');
		$config['uri_segment']='4';
		$config['base_url'] = base_url().'challenge/closed/'.$limit.'/';
		$config['total_rows']= $this->challenge_model->get_total_loss_challenge_count(get_authenticateUserID());
	
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->challenge_model->get_loss_challenge_list(get_authenticateUserID(),$limit,$offset);
		
		$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		$pageTitle='3D-challenge Hive- '.$meta_setting->title;
		$metaDescription='3D-challenge Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-challenge Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/challenge/list_loss_3dchallenge',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	}
   

	function add_me()
	{
		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3dchallenge";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$data['category'] = get_parent_challenge_category();
		$data['allchallenge'] = get_allchallenge();
		$data['category_id']='';
		$data['parent_challenge_id']='';
	//	$data['challenge_images']='';
	//	$data['challenge_id']='';
		$data['challenge_gallery_type']='';		
		$data["error"] = "";
		$meta_setting=meta_setting();
		
		if($_POST)
		{
			$this->form_validation->set_rules('challenge_title', 'challenge Title', 'trim|required');
			$this->form_validation->set_rules('challenge_point', 'challenge Point', 'trim|required');
	        $this->form_validation->set_rules('challenge_category', 'challenge Category', 'required');
		    $this->form_validation->set_rules('challenge_criteria', 'challenge Criteria', 'required');
			$this->form_validation->set_rules('challenge_requirements', 'challenge Requirements', 'required');
			$this->form_validation->set_rules('challenge_specification', 'challenge specification', 'required');
			
			$this->form_validation->set_rules('challenge_start_date', 'challenge Start Date', 'required');
			$this->form_validation->set_rules('challenge_end_date', 'challenge End Date', 'required');
			
			if($this->form_validation->run() == FALSE)
				{
						if(validation_errors())
						{
							$data["error"] = validation_errors();
						}
						else
						{
								$data["error"] = "";
						}
						
						$data['challenge_title']=$this->input->post('challenge_title');
						$data['hallenge_criteria']=$this->input->post('19	challenge_criteria');
						$data['challenge_category']=$this->input->post('challenge_category');
						$data['challenge_point']=$this->input->post('challenge_point');
						
						
					
						$data['challenge_meta_keyword']=$this->input->post('challenge_meta_keyword');
						$data['challenge_meta_description']=$this->input->post('challenge_meta_description');
						$data['challenge_tag']=$this->input->post('tag_name');
						$data['challenge_start_date']=$this->input->post('challenge_start_date');
						$data['challenge_end_date']=$this->input->post('challenge_end_date');
						$data['challenge_is_free']=$this->input->post('challenge_is_free');
						$data['challenge_criteria']=$this->input->post('challenge_criteria');
						$data['challenge_requirements']=$this->input->post('challenge_requirements');
						$data['challenge_specification']=$this->input->post('challenge_specification');
						$data['challenge_mail_allow']=$this->input->post('challenge_mail_allow');
						
						
					
					
						$challenge_id = $this->input->post('challenge_id');
						if($challenge_id > 0)
						{
							$data['challenge_images']= $this->challenge_model->get_one_challenge_images($challenge_id);
							$data['challenge_id']=$this->input->post('challenge_id');
						}
						else
						{
							$data['challenge_images']='';
							$data['challenge_id']='';
						}
						
				}
				else
				{
					
					if($this->input->post('challenge_id') != '')
					{
						$result = $this->challenge_model->update_challenge();
						redirect('challenge/all/20/0/update_challenge');
					}
					else
					{
						$result = $this->challenge_model->insert_challenge();
						redirect('challenge/all/20/0/success_challenge');
					}
				}	
		}
		else
		{
			$data['challenge_title']=$this->input->post('challenge_title');
			$data['challenge_content']=$this->input->post('challenge_content');
			$data['challenge_category']=$this->input->post('challenge_category');
			$data['challenge_point']=$this->input->post('challenge_point');
		
			$data['challenge_meta_keyword']=$this->input->post('challenge_meta_keyword');
			$data['challenge_meta_description']=$this->input->post('challenge_meta_description');
			$data['challenge_tag']=$this->input->post('tag_name');
			
			$data['challenge_start_date']=$this->input->post('challenge_start_date');
			$data['challenge_end_date']=$this->input->post('challenge_end_date');
			$data['challenge_is_free']=$this->input->post('challenge_is_free');
			$data['challenge_criteria']=$this->input->post('challenge_criteria');
			$data['challenge_requirements']=$this->input->post('challenge_requirements');
			$data['challenge_specification']=$this->input->post('challenge_specification');
			$data['challenge_mail_allow']=$this->input->post('challenge_mail_allow');
						
			$data['challenge_id']=$this->input->post('challenge_id');
			
			
			
			
		}
		
		$pageTitle='3D-challenge Hive- '.$meta_setting->title;
		$metaDescription='3D-challenge Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-challenge Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/challenge/add_3d_challenge_hive',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	}
	
	function edit_me($challenge_id='')
	{
		
		if(!check_user_authentication()) {  redirect('login'); } 
		
		$challenge_detail =  $this->challenge_model->check_exist_challenge($challenge_id,get_authenticateUserID());
		
		if(!$challenge_detail)
		{
			redirect('challenge/all');
		}
		
		if($challenge_id != '' && $challenge_id > 0)
		{
		
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3dchallenge";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$data['category'] = get_parent_challenge_category();
	    $data['error']='';
		$one_challenge = $this->challenge_model->get_one_challenge($challenge_id);
		$get_one_challenge_tag = $this->challenge_model->get_one_challenge_tag($challenge_id);
	
		if($get_one_challenge_tag)
		{
			$challenge_tag ='';
			foreach($get_one_challenge_tag as $gt)
			{
				$challenge_tag .= $gt->tag_name.',';
			}
			
		}
		
		$data['challenge_id']=$one_challenge->challenge_id;
		$data['challenge_tag'] = substr($challenge_tag,0,-1);
		$data['challenge_title']=$one_challenge->challenge_title;
		$data['challenge_category']=$one_challenge->category_id;
		$data['challenge_point']=$one_challenge->challenge_point;
		
		$data['challenge_meta_keyword']=$one_challenge->challenge_meta_keyword;
		$data['challenge_meta_description']=$one_challenge->challenge_meta_description;
		
			
		$data['challenge_start_date']=$one_challenge->challenge_start_date;
		$data['challenge_end_date']=$one_challenge->challenge_end_date;
		$data['challenge_is_free']=$one_challenge->challenge_is_free;
		$data['challenge_criteria']=$one_challenge->challenge_criteria;
		$data['challenge_requirements']=$one_challenge->challenge_requirements;
		$data['challenge_specification']=$one_challenge->challenge_specification;
		$data['challenge_mail_allow']=$one_challenge->challenge_mail_allow;


		$meta_setting=meta_setting();
		
		$pageTitle='3D-challenge Hive- '.$meta_setting->title;
		$metaDescription='3D-challenge Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-challenge Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/challenge/add_3d_challenge_hive',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	}
		else
		{
			redirect('user/dashboard');
		}
	
	}
	
	
	function delete_me($challenge_id)
	{
		
		if(!check_user_authentication()) {  redirect('login'); } 
		
		$challenge_detail =  $this->challenge_model->check_exist_challenge($challenge_id,get_authenticateUserID());
		if(!$challenge_detail)
		{
			redirect('challenge/all');
		}
		
		
		if($challenge_id != '' && $challenge_id > 0)
		{
			$delete_tag=$this->db->query("delete from ".$this->db->dbprefix('challenge_tags')." where challenge_id='".$challenge_id."'");
			$delete_cat=$this->db->query("delete from ".$this->db->dbprefix('challenge_category_rel')." where challenge_id='".$challenge_id."'");
			
			$challenge_images= $this->challenge_model->get_one_challenge_images($challenge_id);
			 if(challenge_images) 
			{
				foreach($challenge_images as $di)
				{
					if(file_exists(base_path().'upload/challenge_orig/'.$di->attachment_name))
					{
						unlink(base_path().'upload/challenge_orig/'.$di->attachment_name);
					}	
								
					if(file_exists(base_path().'upload/challenge/'.$di->attachment_name))
					{
						unlink(base_path().'upload/challenge/'.$di->attachment_name);
					}
				}
				
			}
			
			$delete_attach=$this->db->query("delete from ".$this->db->dbprefix('challenge_attachment')." where challenge_id='".$challenge_id."'");
			$challenge_comment=$this->db->query("delete from ".$this->db->dbprefix('challenge_comment')." where challenge_id='".$challenge_id."'");
			$challenge_view = $this->db->query("delete from ".$this->db->dbprefix('challenge_view')." where challenge_id='".$challenge_id."'");
			$challenge_share = $this->db->query("delete from ".$this->db->dbprefix('challenge_share')." where challenge_id='".$challenge_id."'");
			$challenge_like = $this->db->query("delete from ".$this->db->dbprefix('challenge_like')." where challenge_id='".$challenge_id."'");
			$challenge_download = $this->db->query("delete from ".$this->db->dbprefix('challenge_download')." where challenge_id='".$challenge_id."'");
			$delete_tag=$this->db->query("delete from ".$this->db->dbprefix('challenge')." where challenge_id='".$challenge_id."'");
			
			redirect('challenge/all/20/0/delete_challenge');
		}
		else
		{
			redirect('user/dashboard');
		}
		
	}
	
	function delete_challenge_attachment()
	{
		 $attachment_id = $_REQUEST['attachment_id'];
		
		$challenge_attachment= $this->challenge_model->get_one_challenge_attachment($attachment_id);
		 if($challenge_attachment) 
		{		if(file_exists(base_path().'upload/challenge_orig/'.$challenge_attachment->attachment_name))
				{
					unlink(base_path().'upload/challenge_orig/'.$challenge_attachment->attachment_name);
				}	
							
				if(file_exists(base_path().'upload/challenge/'.$challenge_attachment->attachment_name))
				{
					unlink(base_path().'upload/challenge/'.$challenge_attachment->attachment_name);
				}
			
		}
		$delete_attach=$this->db->query("delete from ".$this->db->dbprefix('challenge_attachment')." where attachment_id='".$attachment_id."'");
		echo "success";
		die;
		
	}
	
	function search($limit='20',$category_id=0,$keyword='',$offset=0,$msg='')
	{
	
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3dchallenge";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['category'] = get_parent_category();
		
	
		$data['msg']=$msg;
		
		$this->load->library('pagination');
		
		if($_POST)
		{
		
			$category_ids = '';
			if(isset($_POST['category'])){ $category_ids =$_POST['category']; };
			$keyword = $_POST['keyword'];
			
			if($keyword == "")
			{
				$keyword = 0;
			}
			if($category_ids)
			{
				$category_id = implode("-",$category_ids);
			}
			else
			{
				$category_id  = 0;
				
			}
			
			$limit = $this->input->post("limit");
			
			
			$config['uri_segment']='5';
			$config['base_url'] = base_url().'challenge/search/'.$limit.'/'.$category_id.'/'.$keyword;
			$config['total_rows']= $this->challenge_model->get_total_category_challenge_count($category_id,$keyword);
			$config['per_page'] = $limit;		
			$this->pagination->initialize($config);		
			$data['page_link'] = $this->pagination->create_links();
			$data['result'] = $this->challenge_model->get_category_challenge_result($limit,$offset,$category_id,$keyword);
			
		}
		else
		{
			//$category_id='';
			//$keyword='';
			
		    $config['uri_segment']='5';
			$config['base_url'] = base_url().'challenge/search/'.$limit.'/'.$category_id.'/'.$keyword;
			$config['total_rows']= $this->challenge_model->get_total_category_challenge_count($category_id,$keyword);
			$config['per_page'] = $limit;		
			$this->pagination->initialize($config);		
			$data['page_link'] = $this->pagination->create_links();
			$data['result'] = $this->challenge_model->get_category_challenge_result($limit,$offset,$category_id,$keyword);
		}
			
		/*echo "<pre>";
		print_r($data['result']);
		die;*/
		$data['category_id']=$category_id;
		$data['keyword']=$keyword;
		$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$pageTitle='3D-challenge Hive- '.$meta_setting->title;
		$metaDescription='3D-challenge Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-challenge Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/challenge/search',$data,TRUE);
	$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	
	}
	
	function add_challenge_category()
	{
		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3dchallenge";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data["error"] = "";
		$data['category'] = get_parent_category();
	
		if($this->input->post('submit')) 
		{
		
				$this->form_validation->set_rules('category_name', 'Category Name', 'trim|required');
				if($this->form_validation->run() == FALSE)
				{
						if(validation_errors())
						{
							$data["error"] = validation_errors();
						}
						else
						{
								$data["error"] = "";
						}
						
						$data['category_parent_id']=$this->input->post('category_parent_id');
						$data['category_name']=$this->input->post('category_name');
						$data['category_description']=$this->input->post('category_description');
						$data['category_status']=$this->input->post('category_status');

				}
				else
				{
					$result = $this->challenge_model->insert_challenge_category();
					redirect('challenge/all/20/0/success');
				}	
		
		}	
		else
		{
			$data['category_name']=$this->input->post('category_name');
			$data['category_description']=$this->input->post('category_description');
		}	
		$pageTitle='3D-challenge Hive- '.$meta_setting->title;
		$metaDescription='3D-challenge Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-challenge Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/challenge/add_3d_challenge_category',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	}
	function view_me($challenge_id='')
	{
		
		//if(!check_user_authentication()) {  redirect('login'); } 
		
		if($challenge_id == '')
		{
			redirect('home/404');
		}
		$challenge_detail =  $this->challenge_model->check_exist_challenge($challenge_id,0);
		if(!$challenge_detail)
		{
			redirect('challenge/all');
		}
		
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3dchallenge";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$data['category'] = get_parent_category();
		$data['allchallenge'] = get_allchallenge();
		
		//Set challenge view count
		$challenge_view = $this->challenge_model->set_challenge_view($challenge_id);
		
		$one_challenge = $this->challenge_model->get_one_challenge($challenge_id);
		
		
		$get_one_challenge_tag = $this->challenge_model->get_one_challenge_tag($challenge_id);
		$data['challenge_images']= $this->challenge_model->get_one_challenge_images($challenge_id);
		
		
		$data['popular_challenge']= $this->challenge_model->get_popular_challenge(4);
		
		/*if($get_one_challenge_tag)
		{
			$challenge_tag ='';
			foreach($get_one_challenge_tag as $gt)
			{
				$challenge_tag .= $gt->tag_name.',';
			}
			
		}*/
		$data['get_one_challenge_tag']=$get_one_challenge_tag;
		$data['main_image'] =  $this->challenge_model->get_one_main_challenge_image($challenge_id);
		$data['user_id']=$one_challenge->user_id;
		$data['challenge_id']=$one_challenge->challenge_id;
		//$data['challenge_tag'] = substr($challenge_tag,0,-1);
		$data['challenge_title']=$one_challenge->challenge_title;
		$data['challenge_content']=$one_challenge->challenge_content;
		$data['category_id']=$one_challenge->category_id;
		$data['challenge_point']=$one_challenge->challenge_point;
		$data['challenge_ref_id']=$one_challenge->challenge_ref_id;
		$data['challenge_by']=$one_challenge->challenge_by;
		$data['challenge_is_modified']=$one_challenge->challenge_is_modified;
		$data['challenge_meta_keyword']=$one_challenge->challenge_meta_keyword;
		$data['challenge_meta_description']=$one_challenge->challenge_meta_description;
		$data['parent_challenge_id']=$one_challenge->parent_challenge_id;
		$data['challenge_date']=$one_challenge->challenge_date;
		$data['challenge_view_count']=$one_challenge->challenge_view_count;
		$data['challenge_like_count']=$one_challenge->challenge_like_count;
		$data["desing_gallery_type"] = $one_challenge->challenge_gallery_type;
		

		$meta_setting=meta_setting();
		
		$pageTitle='3D-challenge Hive- '.$meta_setting->title;
		$metaDescription='3D-challenge Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-challenge Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/challenge/view_challenge',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	
	}
	
	
	function like_dislike($like =0 ,$id = 0)
	{
		
		if(!check_user_authentication())
		{
			$this->session->set_userdata('previousPage', 'challenge/view_me/'.$id);
			 redirect("home/login");
		}
		
		$like_status = $_REQUEST['like_status'];
		$challenge_id = $_REQUEST['challenge_id'];
		$res = $this->challenge_model->set_like_dislike($like_status,$challenge_id);
		echo "success";
		exit;
		
	}
	/* 
	 * Function : offer
	 * show bid on challenge
	 *
	 * */
	 
	 function offer($challenge_id = 0, $limit='20',$offset=0,$msg='')
	{
		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3dchallenge";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['msg']=$msg;
		
		$this->load->library('pagination');
		$config['uri_segment']='4';
		$config['base_url'] = base_url().'challenge/closed/'.$limit.'/';
		$config['total_rows']= $this->challenge_model->get_total_bid_count($challenge_id);
	
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->challenge_model->get_bid_list($challenge_id,$limit,$offset);
		$challenge_win_info = get_challenge_win_info($challenge_id);
		$data["is_win"] = 0;
		if($challenge_win_info)
		{
			$data["is_win"] == "1";
		}
		$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		$pageTitle='3D-challenge Hive- '.$meta_setting->title;
		$metaDescription='3D-challenge Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-challenge Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/challenge/list_offer',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	}

	
	
	function post_challenge($msg="")
    {
        
        $data['active_menu']='';
        $theme = getThemeName();

        $data['error']='';
        $data["msg"]=$msg;
        $this->load->library('form_validation');
        $this->form_validation->set_rules('amount', 'Amount', 'required|numeric');
        $this->form_validation->set_rules('comment', 'Comment', 'required');

        //$data["account_type"]='';

        if($this->form_validation->run() == FALSE)
        {
            if(validation_errors())
            {
                 $data["error"] = validation_errors();
                
            }else{
                $data["error"] = "";
            }
          
            if($_POST){
                $data["amount"] = $this->input->post('amount');
                $data["comment"] = $this->input->post('comment');
            }
        }
        else
        {
            $amount  = $this->input->post('amount');
            $comment  = $this->input->post('comment');
            $challenge_id = $this->input->post('challenge_id');
			$challenge_slug = $this->input->post('challenge_slug');
            
            $res = $this->challenge_model->place_bid($challenge_id,$amount,$comment);
            if($res == 1)
            {
                $data['msg']=$challenge_slug;
				//redirect('challenge/'.$challenge_slug.'/success');
            }
            else
             {
                 $data['error']='fail';
             }            
                
        }
        echo json_encode($data);
        exit;
    }


   function conversation($challenge_bid_id = 0 , $limit = 20 , $offset = 0)
   {
   	  if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3dchallenge";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['msg']="";
		
	
		$data['conversation_list'] = $this->challenge_model->get_conversation_list($challenge_bid_id,get_authenticateUserID());
		
		$data["bid_detail"] = $this->challenge_model->get_bid_details($challenge_bid_id);
		
		$data["challenge_details"] = $this->challenge_model->get_one_challenge($data["bid_detail"]["challenge_id"]);
		//
		
	//	$data['msg'] = $msg;
		
		$data['offset'] = $offset;
		$data['error']='';
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		
		$data['offset'] = $offset;
		$data['error']='';
		$data["challenge_bid_id"] = $challenge_bid_id;
		if($this->input->post('limit') != '')
		{
			$data['limit']=$this->input->post('limit');
		}
		else
		{
			$data['limit']=$limit;
		}
		
		
		$pageTitle='3D-challenge Hive- '.$meta_setting->title;
		$metaDescription='3D-challenge Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-challenge Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/challenge/list_conversation',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
   }
	
	
	function setchallenge($slug='')
    {
	   if(!check_user_authentication())
		{
		$this->session->set_userdata('previousPage', 'challenge/'.$slug);
		redirect("home/login");
		}
   }
	
	function add_comment()
	{
		  $insert_response = $this->challenge_model->inseret_comment($_POST);
		  
		 
		  echo $insert_response;
		  die;
	}
	
	
	function invoice($challenge_bid_id = 0)
	{
		 if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3dchallenge";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['msg']="";
		
	
	    $data["bid_detail"] = $this->challenge_model->get_bid_details($challenge_bid_id);
		$data["challenge_details"] = $this->challenge_model->get_one_challenge($data["bid_detail"]["challenge_id"]);
		//
		
	//	$data['msg'] = $msg;
	
		$data['error']='';
		$data["challenge_bid_id"] = $challenge_bid_id;
		
		$pageTitle='3D-challenge Hive- '.$meta_setting->title;
		$metaDescription='3D-challenge Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-challenge Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/challenge/invoice',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
	}
	
	
	
	
	function pay($challenge_bid_id = 0)
	{
		 if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3dchallenge";
		$data['msg']="";
		
		$bid_detail = $this->challenge_model->get_bid_details($challenge_bid_id);
		$challenge_details = $this->challenge_model->get_one_challenge($bid_detail["challenge_id"]);
		
		$data['error']='';
		$data["challenge_bid_id"] = $challenge_bid_id;
		
		
		//// insert entry into transaction table/////
		$data_trans  = array("user_id"=>get_authenticateUserID(),
		                     "challenge_id"=>$bid_detail["challenge_id"],
							 "is_authorize"=>1,
							 "pay_points"=>$challenge_details->challenge_point,
							 "host_ip"=>getRealIP(),
							 "transaction_date_time"=>date("Y-m-d H:i:s"));
		$this->db->insert("transaction",$data_trans);	
		
		$transaction_id = mysql_insert_id();
		
	   // end of insert entry in transaction table..//
		
		//************insert into wallet***************///////
		$wallet_data = array("debit"=>$challenge_details->challenge_point,
		                     "is_authorize"=>1,
		                     "user_id"=>get_authenticateUserID(),
		                     "challenge_id"=>$bid_detail["challenge_id"],
		                     "wallet_date"=>date("Y-m-d H:i:s"),
		                     "transaction_id"=>$transaction_id,
		                     "wallet_ip"=>getRealIP(),
							 );
			$this->db->insert("wallet",$wallet_data);					 
		//********** end of insert into wallet ************///
		
		///update activiy status in challenge table...///
		$challenge_update = array("challenge_activity_status"=>1);
		$this->db->where("challenge_id",$bid_detail["challenge_id"]);
		$this->db->update("challenge",$challenge_update);
		/// end ///
		
		///update bid table.///
		$bid_update = array("is_won"=>1);
		$this->db->where("challenge_bid_id",$challenge_bid_id);
		$this->db->update("challenge_bid",$bid_update);
		/// end ///
		
		
		redirect("challenge/offer/".$challenge_details->challenge_id."/"."20/0/accept");
		
	}


    function complete($challenge_bid_id = 0)
	{
		
		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['site_setting']=site_setting();
		$data['active_menu']="3dchallenge";
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['msg']="";
		
	    $bid_detail = $this->challenge_model->get_bid_details($challenge_bid_id);
		 
		// if offer completed by user beeder/// 
	   if(get_authenticateUserID() == $bid_detail["user_id"])
	   {
	   	   $this->challenge_complete_by_beeder($bid_detail);
		   redirect("challenge/running/20/0/complete");
	   }
	   
	 
	   /// end//
		
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_rating', 'User rating', 'required|numeric');
        $this->form_validation->set_rules('user_review', 'user Review', 'required');

        //$data["account_type"]='';

        if($this->form_validation->run() == FALSE)
        {
            if(validation_errors())
            {
                 $data["error"] = validation_errors();
                
            }else{
                $data["error"] = "";
            }
          
            if($_POST){
                $data["user_rating"] = $this->input->post('user_rating');
                $data["user_review"] = $this->input->post('user_review');
            }
        }
        else
        {
              $data["user_rating"] = $this->input->post('user_review');
              $data["user_review"]  = $this->input->post('user_rating');
         
            
            $res = $this->challenge_model->complete_challenge($_POST,$challenge_bid_id);
            redirect("challenge/offer/".$bid_detail["challenge_id"]."/"."20/0/complete");
                
        }
       
	   $data["user_rating"] = $this->input->post('user_rating');
       $data["user_review"] = $this->input->post('user_review');
	   $data["challenge_bid_id"] = $challenge_bid_id;
	   
		$data["challenge_bid_id"] = $challenge_bid_id;
		
		$pageTitle='3D-challenge Hive- '.$meta_setting->title;
		$metaDescription='3D-challenge Hive - '.$meta_setting->meta_description;
		$metaKeyword='3D-challenge Hive - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/challenge/challenge_review',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();	
		
	}

   function challenge_complete_by_beeder($bid_detail = array())
   {
   	
   	      ///update bid///
		  $data_bid = array("is_complete_by_bidder"=>"1");
		  $this->db->where("challenge_bid_id",$bid_detail["challenge_bid_id"]);
		  $this->db->update("challenge_bid",$data_bid);
		  /// end//	
		  
		   ///update challenge///
		  $data_challenge = array("challenge_complete_date"=>date("Y-m-d H:i:s"));
		  $this->db->where("challenge_id",$bid_detail["challenge_id"]);
		  $this->db->update("challenge",$data_challenge);
		  /// end//	
   }
}
?>