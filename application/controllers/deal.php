<?php
/*********************************************************************************
 * This the taskrabbitclone.com  by Rockers Technology. is paid software. It is released under the terms of 
 * the following BSD License.
 * 
 *  Rockers Technologies (Head Office)
 *    5038,Berthpage Dr
 *    suwanee, GA. Zip Code : 30024
    
 *    E-mail Address : nishu@rockersinfo.com
 * 
 * Copyright © 2012-2020 by Rockers Technology , INC a domestic profit corporation has been duly incorporated under
the laws of the state of georiga , USA. www.rockersinfo.com
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this 
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this 
 *   list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.

 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class Deal extends IWEB_Controller {
	
	/*
	Function name :Home()
	Description :Its Default Constuctor which called when home object initialzie.its load necesary models
	*/
	
	function Deal()
	{
		parent::__construct();	
		$this->load->model('deal_model');	
	}
	
	
	/*
	Function name :index()
	Parameter :none
	Return : none
	Use : user can see category list, map, recent tasks, top runners and other details
	Description : site home page its default function which called http://hostname/home
				  SEO friendly URL which is declare in config route.php file  http://hostname/
	*/
	public function index()
	{		
		
		if(!check_user_authentication()) {  redirect('login'); } 
		redirect('deal/mydeal');
		
	}
	
	
 	function mydeal($msg='',$offset=0)
	{
		if(!check_user_authentication()) {  redirect('login'); } 
		$data = array();
		$data['msg'] = $msg;
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$site_setting=site_setting();
		$data['site_setting']=$site_setting;
		
		$this->load->library('pagination');

		$limit = '10';
		
		
		$config['base_url'] = site_url('deal/mydeal/');
		$config['total_rows'] = $this->deal_model->get_all_total_deal();
		$config['per_page'] = 2;
		$config['uri_segment']='3';	
				
		$this->pagination->initialize($config);		
		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['total_rows']=$config['total_rows'];
		
		$data['result']=$this->deal_model->get_all_deal($limit,$offset);
		$data['select']='mydeal';
		
		$pageTitle='My Deal >>'.$meta_setting->title;
		$metaDescription='My Deal >>'.$meta_setting->meta_description;
		$metaKeyword='My Deal >>'.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/deal/my_deal',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	}
	
	public function adddeal($msg='')
	{	
			
		if(!check_user_authentication()) {  redirect('login'); } 
		
		$user_info=get_user_profile_by_id(get_authenticateUserID());
		
		if(!$user_info)
		{
			redirect('home');
		}
		
		
		$data['user_info']=$user_info;
		
		$check_total_deal_limit=$this->deal_model->get_total_deal_by_user_id(get_authenticateUserID());
		
		if($check_total_deal_limit>=5)
		{
			redirect('deal/mydeal/reachlimit');
		}
		
		
		$data = array();
		$data['msg'] = $msg;
		$data['error'] = '';
		if($this->input->post('submit')) 
		{
			$this->form_validation->set_rules('deal_title', 'Deal title', 'required');
			$this->form_validation->set_rules('deal_summary', 'Deal summary', 'required');
			$this->form_validation->set_rules('deal_description', 'Deal description', 'required');
			$this->form_validation->set_rules('deal_amount', 'Deal amount', 'required|numeric');
			$this->form_validation->set_rules('deal_days', 'Deal Days', 'required|numeric');
			
			if($this->form_validation->run() == FALSE)
			{
				
				
				$data["error"] = validation_errors();
				
				$data['deal_title']=$this->input->post('deal_title');
				$data['deal_summary']=$this->input->post('deal_summary');
				$data['deal_description']=$this->input->post('deal_description');
				$data['deal_amount']=$this->input->post('deal_amount');
				$data['deal_id']=$this->input->post('deal_id');
				$data['deal_days']=$this->input->post('deal_days');
				$data['prev_deal_image']=$this->input->post('prev_deal_image');
				
			}
			else
			{
				if($this->input->post('deal_id')>0)
				{
					$deal_id = $this->input->post('deal_id');
					$deal_update=$this->deal_model->deal_update($deal_id);
				
				
					$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Update Deal'");
					$email_temp=$email_template->row();
					
					if($email_temp)
					{
					
					$username = $user_info->first_name.' '.$user_info->last_name;
					$user_name_link=site_url('travelagent/'.$user_info->profile_name);
					
					$deal_title = $this->input->post('deal_title');
					$deal_summary=$this->input->post('deal_summary');
					$deal_amount=$this->input->post('deal_amount');
					
					$email_address_from=$email_temp->from_address;
					$email_address_reply=$email_temp->reply_address;
					
					$email_subject=$email_temp->subject;				
					$email_message=$email_temp->message;
					
					$email = $email_address_from;
					
					$email_to=$email_address_from;
					
					
					$email_message=str_replace('{break}','<br/>',$email_message);
					$email_message=str_replace('{user_name}',$username,$email_message);
					$email_message=str_replace('{deal_title}',$deal_title,$email_message);
					$email_message=str_replace('{deal_summary}',$deal_summary,$email_message);
					$email_message=str_replace('{deal_amount}',$deal_amount,$email_message);
					$email_message=str_replace('{user_name_link}',$user_name_link,$email_message);
					
					$email_temp_url=base_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
					$str=$email_message;
				
					/** custom_helper email function **/
					email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str);
				}
					$msg='update';
					redirect('deal/mydeal/'.$msg);
				}
				else
				{
					$deal_id=$this->deal_model->deal_insert();
					$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Add Deal'");
					$email_temp=$email_template->row();
					
					if($email_temp)
					{
					
					$username = $user_info->first_name.' '.$user_info->last_name;
					$user_name_link=site_url('travelagent/'.$user_info->profile_name);
					
					$deal_title = $this->input->post('deal_title');
					$deal_summary=$this->input->post('deal_summary');
					$deal_amount=$this->input->post('deal_amount');
					
					$email_address_from=$email_temp->from_address;
					$email_address_reply=$email_temp->reply_address;
					
					$email_subject=$email_temp->subject;				
					$email_message=$email_temp->message;
					
					$email = $email_address_from;
					
					$email_to=$email_address_from;
					
					
					$email_message=str_replace('{break}','<br/>',$email_message);
					$email_message=str_replace('{user_name}',$username,$email_message);
					$email_message=str_replace('{deal_title}',$deal_title,$email_message);
					$email_message=str_replace('{deal_summary}',$deal_summary,$email_message);
					$email_message=str_replace('{deal_amount}',$deal_amount,$email_message);
					$email_message=str_replace('{user_name_link}',$user_name_link,$email_message);
					
					$email_temp_url=base_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
					$str=$email_message;
					/** custom_helper email function **/
					email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str);
				}
					$msg='success';
					redirect('deal/mydeal/'.$msg);
				}
				
			}
			
		}
		else
		{
			$data['deal_title']='';
			$data['deal_summary']='';
			$data['deal_description']='';
			$data['deal_amount']='';
			$data['deal_id']='';
			$data['deal_days']='';
			$data['prev_deal_image']='';
			
		}
		
		$data['select']='mydeal';
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$site_setting=site_setting();
		$data['site_setting']=$site_setting;
		
		$pageTitle='Add Deal >>'.$meta_setting->title;
		$metaDescription='Add Deal >>'.$meta_setting->meta_description;
		$metaKeyword='Add Deal >>'.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/deal/add_deal',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	
	}
	
	public function editdeal($deal_id='')
	{	
			
		if(!check_user_authentication()) {  redirect('login'); } 
		
		if($deal_id == '')
		{	
			redirect('deal/mydeal');
		}
		
		$user_info=get_user_profile_by_id(get_authenticateUserID());
		
		if(!$user_info)
		{
			redirect('home');
		}
		
		$one_deal = $this->deal_model->get_one_deal_by_id($deal_id);
		
		if(!$one_deal)
		{
			redirect('mydeal');
		}
		else
		{
			if($one_deal->user_id!=get_authenticateUserID())
			{
				redirect('mydeal');
			}
		}
		
		
		$data['user_info']=$user_info;
		
		$data = array();
		$data['select']='mydeal';
		$data['msg'] = '';
		$data['error'] = '';
		if($this->input->post('submit')) 
		{
			$this->form_validation->set_rules('deal_title', 'Deal title', 'required');
			$this->form_validation->set_rules('deal_summary', 'Deal summary', 'required');
			$this->form_validation->set_rules('deal_description', 'Deal description', 'required');
			$this->form_validation->set_rules('deal_amount', 'Deal amount', 'required|numeric');
			$this->form_validation->set_rules('deal_days', 'Deal Days', 'required|numeric');
			
			
			if($this->form_validation->run() == FALSE)
			{
				
				
				$data["error"] = validation_errors();
				
				$data['deal_title']=$this->input->post('deal_title');
				$data['deal_summary']=$this->input->post('deal_summary');
				$data['deal_description']=$this->input->post('deal_description');
				$data['deal_amount']=$this->input->post('deal_amount');
				$data['deal_days']=$this->input->post('deal_days');
				$data['prev_deal_image']=$this->input->post('prev_deal_image');
				
				
			}
			else
			{
				$deal_id=$this->deal_model->deal_insert();
				$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Add Deal'");
				$email_temp=$email_template->row();
				
				if($email_temp)
				{
				
				$username = $user_info->first_name.' '.$user_info->last_name;
				$user_name_link=site_url('travelagent/'.$user_info->profile_name);
				
				$deal_title = $this->input->post('deal_title');
				$deal_summary=$this->input->post('deal_summary');
				$deal_amount=$this->input->post('deal_amount');
				
				$email_address_from=$email_temp->from_address;
				$email_address_reply=$email_temp->reply_address;
				
				$email_subject=$email_temp->subject;				
				$email_message=$email_temp->message;
				
				$email = $email_address_from;
				
				$email_to=$email_address_from;
				
				
				$email_message=str_replace('{break}','<br/>',$email_message);
				$email_message=str_replace('{user_name}',$username,$email_message);
				$email_message=str_replace('{deal_title}',$deal_title,$email_message);
				$email_message=str_replace('{deal_summary}',$deal_summary,$email_message);
				$email_message=str_replace('{deal_amount}',$deal_amount,$email_message);
				$email_message=str_replace('{user_name_link}',$user_name_link,$email_message);
				
				$email_temp_url=base_url().'email/';
				$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
				$str=$email_message;
				/** custom_helper email function **/
				email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str);
			}
				$msg='success';
				redirect('deal/mydeal/'.$msg);
				
			}
			
		}
		else
		{
			$one_deal = $this->deal_model->get_one_deal_by_id($deal_id);
			$data['one_deal'] =$one_deal ; 
			$data['deal_title']=$one_deal->deal_title;
			$data['deal_summary']=$one_deal->deal_summary;
			$data['deal_description']=$one_deal->deal_description;
			$data['deal_amount']=$one_deal->deal_amount;
			$data['deal_id']=$one_deal->deal_id;
			$data['deal_days']=$one_deal->deal_days;
			$data['prev_deal_image']=$one_deal->deal_image;
		}
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$site_setting=site_setting();
		$data['site_setting']=$site_setting;
		
		$pageTitle='Edit Deal >>'.$meta_setting->title;
		$metaDescription='Edit Deal >>'.$meta_setting->meta_description;
		$metaKeyword='Edit Deal >>'.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/deal/add_deal',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	
	}
	
	function deletedeal($deal_id)
	{
		$check_deal=$this->db->get_where('deal',array('user_id'=>get_authenticateUserID(),'deal_id'=>$deal_id));
		
		if($check_deal->num_rows()>0)
		{
			
			$res=$check_deal->row();
			
			
			$prev_deal_image=$res->deal_image;
				
			 if($prev_deal_image != '') 
				{
					if(file_exists(base_path().'upload/deal_orig/'.$prev_deal_image))
					{
						unlink(base_path().'upload/deal_orig/'.$prev_deal_image);
					}
					if(file_exists(base_path().'upload/deal/'.$prev_deal_image))
					{
						unlink(base_path().'upload/deal/'.$prev_deal_image);
					}					
					
				}
			
			
			
			$this->db->delete('deal',array('deal_id'=>$deal_id,'user_id'=>get_authenticateUserID()));
			$msg = 'delete';
			redirect('deal/mydeal/'.$msg);	
		}
		
		redirect('deal/mydeal/');	
	}
	
	
	function sethotdeal($deal_id)
	{
		
		
		$check_deal=$this->db->get_where('deal',array('user_id'=>get_authenticateUserID()));
		
		if($check_deal->num_rows()>0)
		{
			
			$deal_data=array('deal_is_hot'=>0);
			$this->db->where('user_id',get_authenticateUserID());
			$this->db->update('deal',$deal_data);
			
			
			$hot_deal_data=array('deal_is_hot'=>1);
			$this->db->where('user_id',get_authenticateUserID());
			$this->db->where('deal_id',$deal_id);
			$this->db->update('deal',$hot_deal_data);
			
			$msg = 'hotdeal';
			redirect('deal/mydeal/'.$msg);	
		}
		
		redirect('deal/mydeal/');	
	}
	

	function removehot($deal_id)
	{
		
		
		$check_deal=$this->db->get_where('deal',array('user_id'=>get_authenticateUserID(),'deal_id'=>$deal_id));
		
		if($check_deal->num_rows()>0)
		{
			
			$deal_data=array('deal_is_hot'=>0);
			$this->db->where('user_id',get_authenticateUserID());
			$this->db->where('deal_id',$deal_id);
			$this->db->update('deal',$deal_data);
		
			$msg = 'removehot';
			redirect('deal/mydeal/'.$msg);	
		}
		
		redirect('deal/mydeal/');	
	}
	
	
}

?>