<?php
class Home extends IWEB_Controller {
	
	/*
	Function name :Home()
	Description :Its Default Constuctor which called when home object initialzie.its load necesary models
	*/
	
	function Home()
	{
		parent::__construct();	
		$this->load->model('home_model');	
		$this->load->model('user_model');
		$this->load->model('design_model');
		$this->load->helper('cookie');
			
	}
	
	
	/*
	Function name :index()
	Parameter :none
	Return : none
	Use : user can see category list, map, recent tasks, top runners and other details
	Description : site home page its default function which called http://hostname/home
				  SEO friendly URL which is declare in config route.php file  http://hostname/
	*/
	
	public function index()
	{
		
			
		$data=array();
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['latest_printer']=$this->home_model->latest_printer('12');
		$data['latest_design']=$this->home_model->latest_design('12');
                
                $data['is_home']=1;
		
		
		$meta_setting=meta_setting();
		$site_setting=site_setting();
		
		$pageTitle=$meta_setting->title;
		$metaDescription=$meta_setting->meta_description;
		$metaKeyword=$meta_setting->meta_keyword;
		 
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('campaign_box',$theme .'/layout/content/content',$data,TRUE);		
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
		
	}
	

		   
   	/*
	Function name :login()
	Parameter : $msg (cusotm message)
	Return : none
	Use : user can login in this site send request to forget password
	Description : user can register in this site, login in this site and send request to forget password using this function 
				  which called http://hostname/home/login  or 
	              SEO friendly URL which is declare in config route.php file  http://hostname/login
	*/
	
	function login($msg='',$redirect_url='')
	{
		$data= array();
		$data['msg']=$msg;
		$data['error'] = '';
		


		if(get_authenticateUserID()!='') { 	
		
			
			redirect('user/dashboard');
		}
		
                
                 if($_POST){
                    
                    $redirect_url=$this->input->post('redirect_url');
                }
	
	
		
		$chk_user='false';
		$spamer=spam_protection();
      	if($spamer==1 || $spamer=='1') {	$chk_user='true';	}
		
		if($this->input->post('submit')) 
		{
		
		
				$this->form_validation->set_rules('login_email', 'Email', 'trim|required|valid_email');
				$this->form_validation->set_rules('login_password', 'Password', 'trim|required|min_length[8]|max_length[12]');
				
				if($this->form_validation->run() == FALSE || $chk_user=='true')
				{
						if(validation_errors() || $chk_user=='true')
						{
							$spam_message='';
							
							if($chk_user=='true')
							{
								$spam_message='<p>Your IP has been Band due to Spam.You can not take Login.</p>';
								
								$send_spam_mail = $this->home_model->send_spam_mail();
							}
							
							$data["error"] = validation_errors().$spam_message;
						}
						else
						{
							if($chk_user=='true')
							{
								$data["error"] = '<p>Your IP has been Band due to Spam.You can not take Login.</p>';
								$send_spam_mail = $this->home_model->send_spam_mail();
							}
							else
							{				
								$data["error"] = "";
							}
						}
						
						$data['msg']=$msg;
						$data['view']='login';
						
						
						$data['login_email']=$this->input->post('login_email');
						$data['login_password']=$this->input->post('login_password');
                                                
                                              
						
					}
					else
					{
						
						$login = $this->home_model->is_login();
						if($login == 1)
						{	
						
							if($this->input->post('redirect_url') != '')
							{
								
								
								redirect(base64_decode($this->input->post('redirect_url')));			
							}
							else
							{
								
								redirect('user/dashboard');		
							}
						}
						elseif($login==2)
						{	
						$email=base64_encode($this->input->post('login_email'));
						
								$data['error']="<p>Email address is not verified. Click here to ".anchor('home/resendverification/'.$email,'resend verification')." email.</p>";
						
							
							$data['msg']=$msg;
							$data['view']='login';
							
							$data['login_email']=$this->input->post('login_email');
							$data['login_password']=$this->input->post('login_password');
							
						}
						elseif($login == 3)
						{
							$data['error']="<p>Your Email ID was not activated or stopped by web master, So please Contact Administration.</p>";
						
							
							$data['msg']=$msg;
							$data['view']='login';
							
							$data['login_email']=$this->input->post('login_email');
							$data['login_password']=$this->input->post('login_password');
						}
						elseif($login==4)
						{	
						$email=base64_encode($this->input->post('login_email'));
						
								$data['error']="<p>Email address is not verified. First verify your email address then wait 24 hours for administrator approval. Click here to ".anchor('home/resendverification/'.$email,'resend verification')." email.</p>";
						
							
							$data['msg']=$msg;
							$data['view']='login';
							
							$data['login_email']=$this->input->post('login_email');
							$data['login_password']=$this->input->post('login_password');
							
						}
						else
						{
							
							$data['error']="<p>Sorry, we couldn&acute;t verify your email and password.</p>";
						
							
							$data['msg']=$msg;
							$data['view']='login';
							
							$data['login_email']=$this->input->post('login_email');
							$data['login_password']=$this->input->post('login_password');
							
						}
					}	
		
		}		
		
		
		
		
			$theme = getThemeName();
			$data['theme'] = $theme;
                        
                        $data['redirect_url'] = $redirect_url;
                        
			$this->template->set_master_template($theme .'/template.php');
			
			$meta_setting=meta_setting();
			$site_setting=site_setting();
			
			$pageTitle=$site_setting->site_name.' - Login - '.$meta_setting->title;
			$metaDescription=$site_setting->site_name.' - Login - '.$meta_setting->meta_description;
			$metaKeyword=$site_setting->site_name.' - Login - '.$meta_setting->meta_keyword;
			
			$this->template->write('pageTitle',$pageTitle,TRUE);
			$this->template->write('metaDescription',$metaDescription,TRUE);
			$this->template->write('metaKeyword',$metaKeyword,TRUE);
		
		
			$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
			$this->template->write_view('content_center',$theme .'/layout/common/login',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();	
	
	}
	
	/*
	Function name :resendverification()
	Parameter : none
	Return : array
	Use : send verfication mail to user
	Description : send verfication mail to user using this function 
	*/
	function resendverification($email='')
	{
		$email=base64_decode($email);
		
		if($email=='')
		{
			redirect('home/login/notfound');
		}
		else
		{
			
			$user_info=get_user_profile_by_email($email);
			
			if(!$user_info)
			{
				redirect('home/login/notfound');
			}
			else
			{
			
					$email=$user_info->email;
					
					
					$username =ucfirst($user_info->first_name).' '.ucfirst($user_info->last_name);	
					$profile_name =$user_info->profile_name;	
								
					$email_verification_code=$user_info->email_verification_code;
					$user_id=$user_info->user_id;
					
					$verification_link=site_url('verify/'.$user_id.'/'.$email_verification_code);
					
					$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Resend Verification'");
					$email_temp=$email_template->row();
					
					if($email_temp)
					{
						$email_from_name=$email_temp->from_name;
						
						$email_address_from=$email_temp->from_address;
						$email_address_reply=$email_temp->reply_address;
						
						$email_subject=$email_temp->subject;				
						$email_message=$email_temp->message;
						
						
						
						$email_to=$email;
						
						
						
						$email_message=str_replace('{break}','<br/>',$email_message);
						$email_message=str_replace('{user_name}',$username,$email_message);
						$email_message=str_replace('{verify_link}',$verification_link,$email_message);
						
						
						$email_temp_url=base_url().'email/';
						$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
						
						$str=$email_message;
					
						
						/** custom_helper email function **/
						if($email_to!='')
						{
							email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
						}
						
						redirect('home/login/sendverification');
	
					}

				/////////////============verification email===========	
			
			
				}
						
			
				
		}
		
	}
	
	
	/*from thank you page*/
	function resend_verification($email='')
	{
		$email=base64_decode($email);
		
		if($email=='')
		{
			redirect('home/login/notfound');
		}
		else
		{
			
			$user_info=get_user_profile_by_email($email);
			
			if(!$user_info)
			{
				redirect('home/login/notfound');
			}
			else
			{
			
					$email=$user_info->email;
					
					
					$username =ucfirst($user_info->first_name).' '.ucfirst($user_info->last_name);	
					$profile_name =$user_info->profile_name;	
								
					$email_verification_code=$user_info->email_verification_code;
					$user_id=$user_info->user_id;
					
					$verification_link=site_url('verify/'.$user_id.'/'.$email_verification_code);
					
								
					$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Resend Verification User'");
					$email_temp=$email_template->row();
					
					if($email_temp)
					{
						$email_from_name=$email_temp->from_name;
						
						$email_address_from=$email_temp->from_address;
						$email_address_reply=$email_temp->reply_address;
						
						$email_subject=$email_temp->subject;				
						$email_message=$email_temp->message;
						
						
						
						$email_to=$email;
						
						
						
						$email_message=str_replace('{break}','<br/>',$email_message);
						$email_message=str_replace('{user_name}',$username,$email_message);
						$email_message=str_replace('{verify_link}',$verification_link,$email_message);
						
						$email_temp_url=base_url().'email/';
						$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
						
						$str=$email_message;
						
						/** custom_helper email function **/
						if($email_to!='')
						{
							email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
						}
						
						redirect('home/login/sendverification');
	
					}

				/////////////============verification email===========	
			
			
				}
						
			
				
		}
		
	}
	
	
	
	function forget_password()
	{

		$data['twitter'] = 0;
		$data['facebook'] = 0;
		$data['google'] = 0;
		$data['yahoo'] = 0;
			
		if(get_authenticateUserID()!='') { 	 	
			$check_suspend=check_user_suspend();
			if($check_suspend!=0) {  redirect('suspend'); }
			redirect('home');
		}
		
		$chk_user='false';
		$spamer=spam_protection();
      	if($spamer==1 || $spamer=='1') {	$chk_user='true';	}
		$data['error']='';

		if($this->input->post('cforget')) 
		{
	
				$this->form_validation->set_rules('forget_email', 'Email Address', 'trim|required|valid_email');
								
				if($this->form_validation->run() == FALSE)
				{
						if(validation_errors())
						{													
							$data["error"] = validation_errors();
						}
						else
						{		
								$data["error"] = "";							
						}
						
						
						$data['view']='cforget';
						
						
						
					}
					else
					{
						
					
						
						$profilename =$this->home_model->get_exists_user_profile($this->input->post('forget_email'));
						
						
							
						if(!$profilename){
							$data['error']="<p>No Email Address Found.</p>";		
							$data['view']='cforget';
						}
						elseif($profilename)
						{
							$user_profile = $profilename;
						
							if($user_profile) {
								if($user_profile->tw_id >0 && $user_profile->tw_id != ''){
									$data['twitter'] = 1;
								} 
								if($user_profile->fb_id >0 && $user_profile->fb_id != ''){
									$data['facebook'] = 1;
								}
								if($user_profile->google ==1){
									$data['google'] = 1;
								}
								if($user_profile->yahoo == 1){
									$data['yahoo'] = 1;
								} else {
									$f_p = $this->home_model->check_email();
								
									if($f_p == "1"){
										redirect('password/reset');
							
										$data['error']="<p>Your account details is sent to your Email Address.</p>";
										
										$data['msg']=$msg;
										$data['view']='cforget';
										
										$data['login_email']='';
										$data['login_password']='';
									} else {
										

										$data['error']="<p>No Email Address Found.</p>";					
										
										$data['msg']=$msg;
										$data['view']='cforget';
										
										$data['login_email']='';
										$data['login_password']='';

									}
								}
									
							} /*else {
								redirect('password/reset');
							
								$data['error']="<p>Your account details is sent to your Email Address.</p>";
								
								$data['msg']=$msg;
								$data['view']='cforget';
								
								$data['login_email']='';
								$data['login_password']='';
							}
							$data['msg']=$msg;
							$data['view']='cforget';
							
							$data['login_email']='';
							$data['login_password']='';*/
						
						}
						else
						{ 

							$data['error']="<p>No Email Address Found.</p>";						
							$data['view']='cforget';
							
							
						
				}					
			}
		}
		

			$theme = getThemeName();
			$data['theme'] = $theme;
			$this->template->set_master_template($theme .'/template.php');
			
			$meta_setting=meta_setting();
			$site_setting=site_setting();
			
			$pageTitle=$site_setting->site_name.' - Login - '.$meta_setting->title;
			$metaDescription=$site_setting->site_name.' - Login - '.$meta_setting->meta_description;
			$metaKeyword=$site_setting->site_name.' - Login - '.$meta_setting->meta_keyword;
			
			$this->template->write('pageTitle',$pageTitle,TRUE);
			$this->template->write('metaDescription',$metaDescription,TRUE);
			$this->template->write('metaKeyword',$metaKeyword,TRUE);
		
		
			$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
			$this->template->write_view('content_center',$theme .'/layout/common/forgotpassword',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();	
	
	}
	
	/*
	Function name :password_reset()
	Parameter : none
	Return : none
	Use : after user submit successfull forget password request goes here
	Description : user submit the forget password request and after successful submit and email send then user redirect here
				  using this function which called http://hostname/home/password_reset  or 
	              SEO friendly URL which is declare in config route.php file  http://hostname/password_reset
	*/
	
	function password_reset()
	{	
		$data=array();
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$meta_setting=meta_setting();
		$site_setting=site_setting();
		
		$pageTitle=$site_setting->site_name.' - Email Sent - '.$meta_setting->title;
		$metaDescription=$site_setting->site_name.' - Email Sent - '.$meta_setting->meta_description;
		$metaKeyword=$site_setting->site_name.' - Email Sent - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
	
	
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/common/forget_password_msg',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	}
	
	
   
   
   
   
	/*
	Function name :verify()
	Parameter : $user_id (user id), $code (Email verify code)
	Return : none
	Use : verify user email accounts
	Description : user can verify email accounts using this function 
				  which called http://hostname/home/verify/$user_id/$code  or 
	              SEO friendly URL which is declare in config route.php file  http://hostname/verify/$user_id/$code
	*/
	
	function verify($user_id,$code)
	{
		$status='fail';
		
		$verify_user=$this->home_model->verify_user($user_id,$code);
		
		if($verify_user)
		{		
			$status='success';
			
			redirect("home/login/verify");
		}
		
		$data['status']=$status;
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$meta_setting=meta_setting();
		
		$pageTitle='Verify Account - '.$meta_setting->title;
		$metaDescription='Verify Account - '.$meta_setting->meta_description;
		$metaKeyword='Verify Account - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/common/verify_account',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
		
	}

	
	/*
	Function name :reset_password()
	Parameter : $user_id (user id), $code (code)
	Return : none
	Use : user can reset password request
	Description : user can reset password using this function 
				  which called http://hostname/home/reset_password/$user_id/$code  or 
	              SEO friendly URL which is declare in config route.php file  http://hostname/reset_password/$user_id/$code
	*/
	
	function reset_password($user_id,$code)
	{
		$status='fail';
		
		
		$check_valid_request=$this->home_model->check_valid_request($code,$user_id);
		
		if($check_valid_request)
		{
			$status='valid';	
		}
		
		else
		{
			  redirect("home/login/allready_reset");
		}
		
		
			
		$this->form_validation->set_rules('new_password', 'New Password', 'required|matches[confirm_password]|min_length[8]|max_length[12]');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required');

				if($this->form_validation->run() == FALSE)
				{
						if(validation_errors())
						{													
							$data["error"] = validation_errors();
						}
						else
						{		
								$data["error"] = "";							
						}
				
						
				} else	{

									
					$reset_password = $this->home_model->reset_password($code,$user_id);
					$data['error']="reset_success";
					
					redirect("home/login/reset");
					
					
			   }	
		
		
		
		$data['user_id']=$user_id;
		$data['code']=$code;	
		
		$data['status']=$status;
		
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$site_setting=site_setting();
		$meta_setting=meta_setting();
		
	
	if($status!='fail') {
	
	 
		if($data['error']=="reset_success")
		{
			$pageTitle=$site_setting->site_name.' - Reset Password - '.$meta_setting->title;
			$metaDescription=$site_setting->site_name.' - Reset Password - '.$meta_setting->meta_description;
			$metaKeyword=$site_setting->site_name.' - Reset Password - '.$meta_setting->meta_keyword;
		}
		else
		{
			$pageTitle=$site_setting->site_name.' - Password Reset Completed- '.$meta_setting->title;
			$metaDescription=$site_setting->site_name.' - Password Reset Completed- '.$meta_setting->meta_description;
			$metaKeyword=$site_setting->site_name.' - Password Reset Completed- '.$meta_setting->meta_keyword;
		}
		
	} else {
	
			$pageTitle=$site_setting->site_name.' - Not Found - '.$meta_setting->title;
			$metaDescription=$site_setting->site_name.' - Not Found - '.$meta_setting->meta_description;
			$metaKeyword=$site_setting->site_name.' - Not Found - '.$meta_setting->meta_keyword;	
	}
	
	
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/common/reset_password',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
		
		
		
		
	
	}
	
	
	
	
   	
	/*
	Function name :logout()
	Parameter : none
	Return : none
	Use : destroy user login session and redirect to home page
	Description : destroy user login session and redirect to home page
	*/
	
	function logout()
	{
			
			
			$this->load->helper('cookie');
				
				////==destroy cache====	
				$this->load->driver('cache');			
				
				$supported_cache=check_supported_cache_driver();
				
				////==destroy now====		
				if(isset($supported_cache))
				{
					if($supported_cache!='' && $supported_cache!='none')
					{	
						if($this->cache->$supported_cache->get('user_login'.get_authenticateUserID()))
						{								
							$this->cache->$supported_cache->delete('user_login'.get_authenticateUserID());						
						}
					}
					
				}
				
				$data_up=array(
					'login_status'=>0
				);
				
			 	$this->db->where('user_id',get_authenticateUserID());
			 	$this->db->update('user_login',$data_up);
				
				
				
				
				////==destroy user session===
				$this->session->set_userdata('user_id', $this->security->xss_clean(''));	
				$this->session->set_userdata('full_name', $this->security->xss_clean(''));	
				$this->session->set_userdata('first_name', $this->security->xss_clean(''));	
				
				
				
				
				
				
			$this->session->sess_destroy();
		
			delete_cookie('redirect_uri','','','');
			if (isset($_COOKIE['redirect_uri']))
			{
				unset($_COOKIE['redirect_uri']);
			}
			
			
			delete_cookie('fbs_'.$this->fb_connect->appkey, '', '', '');
			if (isset($_COOKIE['fbs_'.$this->fb_connect->appkey]))
			{
			unset($_COOKIE['fbs_'.$this->fb_connect->appkey]);
			}
			
			
			delete_cookie('fbsr_'.$this->fb_connect->appkey, '', '', '');
			if (isset($_COOKIE['fbsr_'.$this->fb_connect->appkey]))
			{
			unset($_COOKIE['fbsr_'.$this->fb_connect->appkey]);
			}
			
			
		$data['logged_out'] = TRUE;
	 
	
			
		redirect("home/");
	}
	
	
	
	function register($msg='') {
	
		$data= array();
		$data['error'] = '';
		
		$data['msg'] = $msg;

		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		//$openid = 'openid/openid.php';
		//require $openid;
		
		//$cryptinstall = 'code/cryptographp.fct.php';
	//	include $cryptinstall; 
	
		
		$site_setting=site_setting();
		
		
			require_once(APPPATH.'libraries/recaptchalib.php');
			$captcha_result='';
			$publickey = $site_setting->captcha_public_key;
			$privatekey = $site_setting->captcha_private_key;
			# the response from reCAPTCHA
			$resp = null;
			# the error code from reCAPTCHA, if any
			$error = null;
			
		if(get_authenticateUserID()!='') { 	 	
			$check_suspend=check_user_suspend();
			if($check_suspend!=0) {  redirect('suspend'); }
			redirect('home');
		}
		
		$chk_user='false';
		$spamer=spam_protection();
      	if($spamer==1 || $spamer=='1') { $chk_user='true'; }
		
		
		if($this->input->post('registeruser')) 
		{
		
				
				$this->form_validation->set_rules('profilename', 'User Name', 'required|alpha_numeric|callback_username_check');
				$this->form_validation->set_rules('firstname', 'First Name', 'required|alpha');
				$this->form_validation->set_rules('lastname', 'Last Name', 'required|alpha');
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');		
				$this->form_validation->set_rules('password', 'Password', 'required|matches[confirmpassword]|min_length[8]|max_length[12]');
				$this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'required|min_length[8]|max_length[12]');
				$this->form_validation->set_rules('terms_condition', 'Please accept terms & condtion', 'required');
				$this->form_validation->set_rules('legal_desc', 'Legal Disclaimers for uploading/downloading ', 'required');
				
				
				$error ='';
				/*if($site_setting->captcha_enable==1)
				{		
				 $resp = recaptcha_check_answer ($privatekey,
														$_SERVER["REMOTE_ADDR"],
														$_POST["recaptcha_challenge_field"],
														$_POST["recaptcha_response_field"]);
					 if ($this->input->post('registeruser')) 
					 {	
							
							if ($resp->is_valid) {
							$captcha_result = ''; 
							} else {
							# set the error code so that we can display it
							$captcha_result = '<p>Image verification is Wrong.</p>';
							}


					 }
						$error = $captcha_result;
					
					
				} else {
				
					$error ='';
				}*/
				
					
					if($this->form_validation->run() == FALSE || $error != "" || $chk_user=='true')
					{	
				
					
					if(validation_errors() || $error != ""  || $chk_user=='true')
					{
						$spam_message='';
						
						if($chk_user=='true')
						{
							$spam_message='<p>Your IP has been Band due to Spam.You can not Register Now.</p>';
							$send_spam_mail = $this->home_model->send_spam_mail();
						}
						
						$data["error"] = $spam_message.validation_errors().$error;
						
					}else{
						
						if($chk_user=='true')
						{
							$data["error"] = '<p>Your IP has been Band due to Spam.You can not Register Now.</p>';
							$send_spam_mail = $this->home_model->send_spam_mail();
						}
						else
						{				
							$data["error"] = "";
						}
						
					}
			
					$data['firstname'] = $this->input->post('firstname');					
					$data['lastname'] = $this->input->post('lastname');
					$data['profilename'] = $this->input->post('profilename');
					$data['email'] = $this->input->post('email');
					$data['password'] = $this->input->post('password');
					
					
					
					
					$data['view']='login';
					$data['msg']=$msg;
				
					
			
		} else {			

			$chk_spam_register=$this->home_model->check_spam_register();
				
			if($chk_spam_register==1)
			{
				
				$data["error"] = '<p>Your IP has been Band due to Spam.You can not Register Now.</p>';	
				$send_spam_mail = $this->home_model->send_spam_mail();
					
				$data['firstname'] = $this->input->post('firstname');					
				$data['lastname'] = $this->input->post('lastname');
				$data['profilename'] = $this->input->post('profilename');
				$data['email'] = $this->input->post('email');
				$data['password'] = $this->input->post('password');
				
				$data['view']='login';
				$data['msg']=$msg;
			
			
						
			}
			else
			{
							
				$sign=$this->home_model->register_user();
				
				if($sign > 0)
				{

			
					$email=$this->input->post('email');
					$get_user_info=$this->db->get_where('user',array('email'=>$email));
					$user_info=$get_user_info->row();
					
					$username =$user_info->full_name;				
					$email_verification_code=$user_info->email_verification_code;
					$user_id=$user_info->user_id;
					
					$verification_link=site_url('verify/'.$user_id.'/'.$email_verification_code);
				
				
			
				
			
				
				
				/////////////============new user email===========	


				
				$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='New User Join'");
				$email_temp=$email_template->row();
				
				if($email_temp)
				{
				
				$email_address_from=$email_temp->from_address;
				$email_address_reply=$email_temp->reply_address;
				
				$email_subject=$email_temp->subject;				
				$email_message=$email_temp->message;
				
				//$username =$this->input->post('user_name');
				$password = $this->input->post('password');
				$email = $this->input->post('email');
				
				$email_to=$this->input->post('email');
				
				//$login_link=site_url('sign_up');
				
				$email_from_name=$email_temp->from_name;
				$email_message=str_replace('{break}','<br/>',$email_message);
				$email_message=str_replace('{user_name}',$username,$email_message);
				$email_message=str_replace('{password}',$password,$email_message);
				$email_message=str_replace('{email}',$email,$email_message);
			//	$email_message=str_replace('{login_link}',$login_link,$email_message);
				$email_message=str_replace('{verify_email_link}',$verification_link,$email_message);
				$email_temp_url=base_url().'email/';
				$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
				$str=$email_message;
				/** custom_helper email function **/
				email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
				}

				/////////////============new user email===========	
						
					//redirect('home/thank_you/sign_up');
					
							
					$msg=base64_decode($msg);
					if(strstr($msg,'http'))
					{						
						redirect($msg);
					} else {
						 redirect('home/login/signup_success'); 
								 
					}
					
			
				}
				else
				{
					redirect('home/register/fail');
				}
			
			
			
			}
			
			
			
					
			
			
		}
		
		
		}

		else
		{			
			$data['msg']=$msg;
			$data['view']='login';
			$data['error']='';
		
			
			$data['login_email']='';
			$data['login_password']='';	
			$data['firstname'] = '';					
			$data['lastname'] = '';					
			$data['email'] = '';
			$data['password'] = '';	
			$data['profilename']='';
			
			
			
		}
		
		 	//echo "show in full page ";
			$meta_setting=meta_setting();
			$site_setting=site_setting();
			
			$data['site_setting']=$site_setting;
			
			$pageTitle='Register>>'.$meta_setting->title;
			$metaDescription='Register>>'.$meta_setting->meta_description;
			$metaKeyword='Register>>'.$meta_setting->meta_keyword;
			
			
			$this->template->write('pageTitle',$pageTitle,TRUE);
			$this->template->write('metaDescription',$metaDescription,TRUE);
			$this->template->write('metaKeyword',$metaKeyword,TRUE);
			$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
			$this->template->write_view('content_center',$theme .'/layout/common/sign_up',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render(); 
		
	}
	
	function registeragent($msg='')
	{
		
	
		$data= array();
		$data['error'] = '';
		
		$data['msg'] = $msg;

		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		//$openid = 'openid/openid.php';
		//require $openid;
		
		//$cryptinstall = 'code/cryptographp.fct.php';
	//	include $cryptinstall; 
	
		
		$site_setting=site_setting();
		$data['member_association'] = member_association();
		
		require_once(APPPATH.'libraries/recaptchalib.php');
			$captcha_result='';
			$publickey = $site_setting->captcha_public_key;
			$privatekey = $site_setting->captcha_private_key;
			# the response from reCAPTCHA
			$resp = null;
			# the error code from reCAPTCHA, if any
			$error = null;
			
		if(get_authenticateUserID()!='') { 	 	
			$check_suspend=check_user_suspend();
			if($check_suspend!=0) {  redirect('suspend'); }
			redirect('home');
		}
		
		$chk_user='false';
		$spamer=spam_protection();
      	if($spamer==1 || $spamer=='1') { $chk_user='true'; }
		
		if($this->input->post('registeragent')) 
		{
			
				/*$this->form_validation->set_rules('agentprofilename', 'User Name', 'trim|required|alpha_numeric|callback_username_check');*/
				$this->form_validation->set_rules('agentfirstname', 'First Name', 'trim|required|alpha');
				$this->form_validation->set_rules('agentlastname', 'Last Name', 'trim|required|alpha');
				
				$this->form_validation->set_rules('agentpassword', 'Password', 'required|matches[agentconfirmpassword]|min_length[8]|max_length[12]');
				$this->form_validation->set_rules('agentconfirmpassword', 'Confirm Password', 'required|min_length[8]|max_length[12]');
				
				$this->form_validation->set_rules('agencyname', 'Agency Name', 'trim|required|alpha_dot_space');

				$this->form_validation->set_rules('contactperson', 'Contact Person Name', 'trim|required|alpha_space');
		
				$this->form_validation->set_rules('agency_location', 'Agency Location', 'trim|required');
				
				
				$this->form_validation->set_rules('agentemail', 'Email', 'trim|required|valid_email|callback_email_check');	
				$this->form_validation->set_rules('agent_support_email', 'trim|Agent Support Email', 'trim|valid_email');	
				$this->form_validation->set_rules('agency_phone', 'Phone No.', 'trim|required|phone_valid|min_length[10]|max_length[18]');
				$this->form_validation->set_rules('agency_customer_support_no', 'Customer Support Number', 'trim|phone_valid|min_length[10]|max_length[18]');
				
				
				$this->form_validation->set_rules('agency_website', 'Agency Website', 'trim|valid_url');
						
				$this->form_validation->set_rules('agree','Agree', 'callback_terms_condition');
				
				
				if($site_setting->captcha_enable==1)
				{
					
					if($this->session->userdata('register34Agent')=='') 
					{  
						 $resp = recaptcha_check_answer ($privatekey,
															$_SERVER["REMOTE_ADDR"],
															$_POST["recaptcha_challenge_field"],
															$_POST["recaptcha_response_field"]);
							 if($this->input->post('registeragent')) 
							 {	
								if ($resp->is_valid) {
									
									$captcha_result = ''; 
									
									$data_captcha_sess=array(
									'register34Agent'=>randomCode()
									);
									$this->session->set_userdata($data_captcha_sess);
									
									
								} else {
								# set the error code so that we can display it
									$captcha_result = '<p>Image Verification is Wrong.</p>';
								}


							 }
					
					} else {
						$captcha_result='';
					}
					
					$error = $captcha_result;
					
				} else {
				
					$error ='';
				}
				
				
				if($this->form_validation->run() == FALSE || $error != "" || $chk_user=='true')
				{
						if(validation_errors() || $chk_user=='true' || $error != "")
						{
							$spam_message='';
							
							if($chk_user=='true')
							{
								$spam_message='<p>Your IP has been Band due to Spam.You can not take Login.</p>';
								
							}
							
							$data["error"] = validation_errors().$spam_message.$error;;
						}
						else
						{
							if($chk_user=='true')
							{
								$data["error"] = '<p>Your IP has been Band due to Spam.You can not take Login.</p>';
							}
							else
							{				
								$data["error"] = "";
							}
						}
						
						$data['msg']=$msg;
						$data['view']='login';
						
						//$data['agentprofilename']=trim($this->input->post('agentprofilename'));
						$data['agentfirstname']=$this->input->post('agentfirstname');
						$data['agentlastname']=$this->input->post('agentlastname');
						$data['agentpassword']=$this->input->post('agentpassword');
						$data['agentconfirmpassword']=$this->input->post('agentconfirmpassword');
						$data['agencyname']=$this->input->post('agencyname');
						$data['agency_location']=$this->input->post('agency_location');
						$data['contactperson']=$this->input->post('contactperson');
						$data['agent_email']=$this->input->post('agentemail');
						$data['agent_support_email']=$this->input->post('agent_support_email');
						$data['agency_phone']=$this->input->post('agency_phone');
						$data['agency_customer_support_no']=$this->input->post('agency_customer_support_no');
						$data['agency_website']=$this->input->post('agency_website');
						$data['agentprofilename']=$this->input->post('agentprofilename');
						$data['memberassociate']=$this->input->post('memberassociate');
						
					}
					else
					{
						$sign=$this->home_model->register_agent();
						if($sign > 0)
						{
					
							$email=$this->input->post('agentemail');
							$get_user_info=$this->db->get_where('user',array('email'=>$email));
							$user_info=$get_user_info->row();
							
							$username =$user_info->full_name;				
							$email_verification_code=$user_info->email_verification_code;
							$user_id=$user_info->user_id;
							
							$verification_link=site_url('verify/'.$user_id.'/'.$email_verification_code);
						
						
					/////////////============welcome email===========	
		
						
						$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Welcome Email'");
						$email_temp=$email_template->row();
						
						if($email_temp)
						{
							$login_link=site_url('sign_up');
							
							$email_from_name=$email_temp->from_name;
							$email_address_from=$email_temp->from_address;
							$email_address_reply=$email_temp->reply_address;
							$email_subject=$email_temp->subject;				
							$email_message=$email_temp->message;			
							$email_to=$this->input->post('agentemail');		
							$email_message=str_replace('{break}','<br/>',$email_message);
							$email_message=str_replace('{user_name}',$username,$email_message);	
							$email_message=str_replace('{agent_signup_link}',$login_link,$email_message);		
							$email_message=str_replace('{email}',$email,$email_message);
							$email_temp_url=base_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
							$str=$email_message;		
							
						/** custom_helper email function **/
							email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
						
						}
						
						
						/////////////============new user email===========	
		
		
						
						$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='New User Join'");
						$email_temp=$email_template->row();
						
						if($email_temp)
						{
						
						$email_address_from=$email_temp->from_address;
						$email_address_reply=$email_temp->reply_address;
						
						$email_subject=$email_temp->subject;				
						$email_message=$email_temp->message;

						
						//$username =$this->input->post('user_name');
						$password = $this->input->post('agentpassword');
						$email = $this->input->post('agentemail');
						
						$email_to=$this->input->post('agentemail');
						
						$login_link=site_url('sign_up');
						
						$email_from_name=$email_temp->from_name;
						$email_message=str_replace('{break}','<br/>',$email_message);
						$email_message=str_replace('{user_name}',$username,$email_message);
						$email_message=str_replace('{password}',$password,$email_message);
						$email_message=str_replace('{email}',$email,$email_message);
						$email_message=str_replace('{login_link}',$login_link,$email_message);
						$email_message=str_replace('{verify_email_link}',$verification_link,$email_message);
						$email_temp_url=base_url().'email/';
						$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
						$str=$email_message;
						/** custom_helper email function **/
						email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
						}
		
						/////////////============new user email===========	
								
							//redirect('home/thank_you/sign_up');
							
									
							$msg=base64_decode($msg);
							if(strstr($msg,'http'))
							{						
								redirect($msg);
							} else {
							
								redirect('home/thank_you/sign_up/'.$sign); 
										 
							}
							
					
						}
						else
						{
							redirect('home/registeragent/fail');
						}
					}	
		
		}
	
		else
		{			
			$data['msg']=$msg;
			$data['view']='login';
			$data['error']='';
			
			/*agent registration*/
			$data['agentfirstname']='';
			$data['agentlastname']='';
			$data['agencyname']='';
			$data['contactperson']='';
			$data['agent_email']='';
			$data['agent_support_email']='';
			$data['agency_phone']='';
			$data['agency_customer_support_no']='';
			$data['agency_website']='';
			$data['agentprofilename']='';
			$data['memberassociate']='';
			$data['agency_location']='';
			$data['agentpassword']='';
			$data['agentconfirmpassword']='';
			
			
			
			
		}
		
		 	//echo "show in full page ";
			$meta_setting=meta_setting();
			$site_setting=site_setting();
			
			$data['site_setting']=$site_setting;
			
			$pageTitle='Register/ Sign In - '.$meta_setting->title;
			$metaDescription='Register/ Sign In - '.$meta_setting->meta_description;
			$metaKeyword='Register/ Sign In - '.$meta_setting->meta_keyword;
			
			
			$this->template->write('pageTitle',$pageTitle,TRUE);
			$this->template->write('metaDescription',$metaDescription,TRUE);
			$this->template->write('metaKeyword',$metaKeyword,TRUE);
			$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
			$this->template->write_view('content_center',$theme .'/layout/common/register_agent',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render(); 
		
	
	}
	
	
	function agentactivate($user_id,$code)
	{
		$status='fail';
		
		$verify_user=$this->home_model->check_active_profile_request($user_id,$code);
		
		if($verify_user!='none')
		{		
			$status='success';
			redirect('user/'.$verify_user);
		}
		
		$data['status']=$status;
		
		if($status=='fail')
		{
			
			$theme = getThemeName();
			$this->template->set_master_template($theme .'/template.php');
			
			$site_setting=site_setting();
			$meta_setting=meta_setting();
		
				
			$pageTitle=$site_setting->site_name.' - Not Found - '.$meta_setting->title;
			$metaDescription=$site_setting->site_name.' - Not Found - '.$meta_setting->meta_description;
			$metaKeyword=$site_setting->site_name.' - Not Found - '.$meta_setting->meta_keyword;	
	
	
	
			$this->template->write('pageTitle',$pageTitle,TRUE);
			$this->template->write('metaDescription',$metaDescription,TRUE);
			$this->template->write('metaKeyword',$metaKeyword,TRUE);
			$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
			$this->template->write_view('content_center',$theme .'/layout/common/no_user_profile',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();
		}
	}
	
	function previewprofile($user_id,$code)
	{
		$status='fail';
		
		$verify_user=$this->home_model->check_preview_profile_request($user_id,$code);
		
		if($verify_user!='none')
		{		
			$status='success';
			redirect('user/'.$verify_user);
		}
		
		$data['status']=$status;
		
		if($status=='fail')
		{
			
			$theme = getThemeName();
			$this->template->set_master_template($theme .'/template.php');
			
			$site_setting=site_setting();
			$meta_setting=meta_setting();
		
				
			$pageTitle=$site_setting->site_name.' - Not Found - '.$meta_setting->title;
			$metaDescription=$site_setting->site_name.' - Not Found - '.$meta_setting->meta_description;
			$metaKeyword=$site_setting->site_name.' - Not Found - '.$meta_setting->meta_keyword;	
	
	
	
			$this->template->write('pageTitle',$pageTitle,TRUE);
			$this->template->write('metaDescription',$metaDescription,TRUE);
			$this->template->write('metaKeyword',$metaKeyword,TRUE);
			$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
			$this->template->write_view('content_center',$theme .'/layout/common/no_user_profile',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();
		}
	}
	
	function editagent()
	{
		$status='fail';
		
		$user_id = $this->session->userdata('email_invite_user_id');
		$code = $this->session->userdata('email_verification_code');
		
		$verify_user=$this->home_model->check_active_profile_request($user_id,$code);
		
		if($verify_user!='none')
		{		
			$status='success';
			redirect('user/edit');
		}
		
		$data['status']=$status;
		
		if($status=='fail')
		{
			
			$theme = getThemeName();
			$this->template->set_master_template($theme .'/template.php');
			
			$site_setting=site_setting();
			$meta_setting=meta_setting();
		
				
			$pageTitle=$site_setting->site_name.' - Not Found - '.$meta_setting->title;
			$metaDescription=$site_setting->site_name.' - Not Found - '.$meta_setting->meta_description;
			$metaKeyword=$site_setting->site_name.' - Not Found - '.$meta_setting->meta_keyword;	
	
	
	
			$this->template->write('pageTitle',$pageTitle,TRUE);
			$this->template->write('metaDescription',$metaDescription,TRUE);
			$this->template->write('metaKeyword',$metaKeyword,TRUE);
			$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
			$this->template->write_view('content_center',$theme .'/layout/common/no_user_profile',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();
		}
	}
	
	
	/*
	Function name :thank_you()
	Parameter : $msg (Custom msg)
	Return : none
	Use : Custom thank you page
	Description : user can see Custom thank you page using this function 
				  which called http://hostname/home/checkUserEmail/$email		  
	*/
	
	function thank_you($msg='',$user_id='')
	{
		
		$data['msg']=$msg;
		$data['user_id'] = $user_id;
		$data['user_info'] = get_user_without_status_by_id($user_id);
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$meta_setting=meta_setting();
		
		$pageTitle=$meta_setting->title;
		$metaDescription=$meta_setting->meta_description;
		$metaKeyword=$meta_setting->meta_keyword;
		
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/common/thank_you',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	
	}
	
	
	/*
	Function name :error_page()
	Parameter : none
	Return : none
	Use :  404 not found page
	Description : user can see this page using this function 
				  which called http://hostname/home/error_page/		  
	*/
	
	function error_page($msg='')
	{
		
		$data['msg']=$msg;
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$meta_setting=meta_setting();
		$site_setting=site_setting();
		
		$pageTitle=$site_setting->site_name.' - 404';
		$metaDescription=$site_setting->site_name.' - 404';
		$metaKeyword=$site_setting->site_name.' - 404';
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/common/no_user_profile',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	
	}
	
	
	
	
	
	
	
				
	/*
	Function name :request_email_check()
	Parameter : $email (email id)
	Return : boolen
	Use : check the user unique email address
	Description : check the user unique email address
	*/
		
	function request_email_check($email)
	{
		$cemail = $this->home_model->requestemailTaken($email);
		
		if($cemail)
		{
			$this->form_validation->set_message('request_email_check', 'There is an existing account associated with this email.');
			return FALSE;
		}
		else
		{
				return TRUE;
		}
	}	
	
	/*
	Function name :checkrequestemailavailability()
	Parameter : none
	Return : none
	Use : user can check email address is available ajax
	Description : user can check email address is available for unique user registration using this function 			  
	*/
	
	function checkrequestemailavailability()
	{
		
		  echo json_encode($this->checkRequestUserEmail($_POST['email']));
   		 exit; // only print out the json version of the response
	
	}
	
	
	/*
	Function name :checkRequestUserEmail()
	Parameter : $email (email id)
	Return : single array of boolen
	Use : check email address is valid or not
	Description : user can check email address is valid or not using this function 			  
	*/
	
	function checkRequestUserEmail($email) 
	{

		  $email = trim($email); // strip any white space
		  $response = array(); // our response
		  
		  // if the username is blank
		  if (!$email) {
			$response = array(
			  'ok' => false, 
			 );
			 
			 
			  
		  // if the username does not match a-z or '.', '-', '_' then it's not valid
		  } else if (!preg_match('^[a-zA-Z0-9]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$^', $email)) {
			$response = array(
			  'ok' => false, 
			  );
			  
		  // this would live in an external library just to check if the username is taken
		  } else if ($this->home_model->requestemailTaken($email)) {
			$response = array(
			  'ok' => false, 
			  );
			  
		  // it's all good
		  } else {
			
			$response = array(
			  'ok' => true, 
			  );
		  }
		
		  return $response;        
	}
	
	
	
		
	
	
	
	/*
	Function name :username_check()
	Parameter : $user_name (profile name)
	Return : boolen
	Use : check the user unique profile name
	Description : check the user unique profile name
	*/
		
	function username_check($user_name)
	{
		$cuser_name = $this->home_model->usernameTaken($user_name);
		
		if($cuser_name)
		{
			$this->form_validation->set_message('username_check', 'There is an existing profile associated with this Username.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}	

	
	
	/*
	Function name :checkusernameavailability()
	Parameter : none
	Return : none
	Use : user can check profile name is available ajax
	Description : user can check profile name is available for unique user profile name using this function 			  
	*/
	
	function checkusernameavailability()
	{
		
		  echo json_encode($this->checkUserName($_POST['user_name']));
   		 exit; // only print out the json version of the response
	
	}
	
	
	/*
	Function name :checkUserName()
	Parameter : $user_name (email id)
	Return : single array of boolen
	Use : check profile name is valid or not
	Description : user can check profile name is valid or not using this function 			  
	*/
	
	function checkUserName($user_name) 
	{

		  $user_name = trim($user_name); // strip any white space
		  $response = array(); // our response
		  
		  // if the username is blank
		  if (!$user_name) {
			$response = array(
			  'ok' => false, 
			 );
			 
			 
			  
		  // if the username does not match a-z or '.', '-', '_' then it's not valid
		  } else if (!preg_match('^[a-z0-9]$^', $user_name)) {
			$response = array(
			  'ok' => false, 
			  );
			  
		  // this would live in an external library just to check if the username is taken
		  } else if ($this->home_model->usernameTaken($user_name)) {
			$response = array(
			  'ok' => false, 
			  );
			  
		  // it's all good
		  } else {
			
			$response = array(
			  'ok' => true, 
			  );
		  }
		
		  return $response;        
	}
	
	
	
	
	/*
	Function name :email_check()
	Parameter : $email (email id)
	Return : boolen
	Use : check the user unique email address
	Description : check the user unique email address
	*/
		
	function email_check($email)
	{
		$cemail = $this->home_model->emailTaken($email);
		
		if($cemail)
		{
			$this->form_validation->set_message('email_check', 'There is an existing account associated with this email.');
			return FALSE;
		}
		else
		{
				return TRUE;
		}
	}	
	
	function email_check_ajax($email)
	{
		$cemail = $this->home_model->emailTaken($email);
		
		if($cemail)
		{
			echo "exist";
			die;
			/*$this->form_validation->set_message('email_check', 'There is an existing account associated with this email.');
			return FALSE;*/
		}
		else
		{
			//return TRUE;
		}
	}	

	
	
	/*
	Function name :checkemailavailability()
	Parameter : none
	Return : none
	Use : user can check email address is available ajax
	Description : user can check email address is available for unique user registration using this function 			  
	*/
	
	function checkemailavailability()
	{
		
		  echo json_encode($this->checkUserEmail($_POST['email']));
   		 exit; // only print out the json version of the response
	
	}
	
	
	/*
	Function name :checkUserEmail()
	Parameter : $email (email id)
	Return : single array of boolen
	Use : check email address is valid or not
	Description : user can check email address is valid or not using this function 			  
	*/
	
	function checkUserEmail($email) 
	{

		  $email = trim($email); // strip any white space
		  $response = array(); // our response
		  
		  // if the username is blank
		  if (!$email) {
			$response = array(
			  'ok' => false, 
			 );
			 
			 
			  
		  // if the username does not match a-z or '.', '-', '_' then it's not valid
		  } else if (!preg_match('^[a-zA-Z0-9]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$^', $email)) {
			$response = array(
			  'ok' => false, 
			  );
			  
		  // this would live in an external library just to check if the username is taken
		  } else if ($this->home_model->emailTaken($email)) {
			$response = array(
			  'ok' => false, 
			  );
			  
		  // it's all good
		  } else {
			
			$response = array(
			  'ok' => true, 
			  );
		  }
		
		  return $response;        
	}
	
	
	
	

	
	
		
	 /*
	Function name :social_signup()
	Parameter : $social_data (array of user details)
	Return : none
	Use : user can sign up if his/her login using social site 
	Description : user can add details when his/her login using social site 
	*/
   
   function social_signup($social_data=array())
	{
	
		//echo '<pre>'; print_r($social_data);die();
		if(get_authenticateUserID()!='') { 	 	
			$check_suspend=check_user_suspend();
			if($check_suspend!=0) {  redirect('suspend'); }
			redirect('home');
		}
		
		$data['fb_img']='';
		$data['twiter_img']='';
		$data['yahoo']='';	
		$data['google']='';	
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$meta_setting=meta_setting();
		$site_setting=site_setting();
		
		$pageTitle=$site_setting->site_name.' - Create Account - '.$meta_setting->title;
		$metaDescription=$site_setting->site_name.' - Create Account - '.$meta_setting->meta_description;
		$metaKeyword=$site_setting->site_name.' - Create Account - '.$meta_setting->meta_keyword;
		
		
		if($social_data)
		{
			
			if(isset($social_data['tw_screen_name']))
			{
				$data['user_name']= $social_data['tw_screen_name'];	
			}
			else
			{
				$data['user_name']= $social_data['first_name'].' '.$social_data['last_name'];	
			}
			
			
			$data['first_name']=$social_data['first_name'];
			$data['last_name']=$social_data['last_name'];
			$data['zip_code']= '';
			$data['mobile_no']='';
			$data['email']= $social_data['email'];
			$data['password']= '';
			$data['fb_id']= $social_data['fb_id'];
			$data['tw_id']=$social_data['tw_id'];
			$data['tw_screen_name']=$social_data['tw_screen_name'];
			$data['oauth_token']=$social_data['oauth_token'];
			$data['oauth_token_secret']=$social_data['oauth_token_secret'];
			//$data['full_name']=$social_data['full_name'];
			//$data['profile_name']=$social_data['profile_name'];

			
			if(isset($social_data['fb_img']))$data['fb_img']=$social_data['fb_img'];
			if(isset($social_data['twiter_img']))$data['twiter_img']=$social_data['twiter_img'];
			if(isset($social_data['yahoo']))$data['yahoo']=$social_data['yahoo'];
			if(isset($social_data['google']))$data['google']=$social_data['google'];
				
			
		}
		else
		{
			$data['user_name']= '';
			$data['first_name']='';
			$data['last_name']='';
			$data['zip_code']= '';
			$data['mobile_no']= '';
			$data['email']= '';
			$data['password']= '';
			$data['fb_id']= '';
			$data['tw_id']='';
			$data['tw_screen_name']='';
			$data['oauth_token']='';
			$data['oauth_token_secret']='';
			//$data['full_name']='';
			//$data['profile_name']='';
			
			$data['fb_img']=$this->input->post('fb_img');
			$data['twiter_img']=$this->input->post('twiter_img');
			
			$data['yahoo']=$this->input->post('yahoo');
			$data['google']=$this->input->post('google');
		
		}
		
		
		
		$chk_user='false';
		$spamer=spam_protection();
      	if($spamer==1 || $spamer=='1') {	$chk_user='true';	}
		
		
		
	
				
		$this->form_validation->set_rules('user_name', 'Username', 'required');
				
				
		$profile = $this->user_model->check_user_profile_exists($this->input->post('user_name'));
				
				$profile_error= '';
				if($profile){
					$profile_error = 'Username / Account URL is alredy exist';
				} else {
					$profile_error= '';
				}
				
				
				
					$error ='';
				
				
				
					if($this->form_validation->run() == FALSE || $error != "" || $chk_user=='true' || $profile_error !="")
					{	
				
					
					if(validation_errors() || $error != "" || $profile_error !="")
					{
						$spam_message='';
						
						if($chk_user=='true')
						{
							$spam_message='<p>Your IP has been Band due to Spam.You can not Register Now.</p>';
						}
						
						$data["error"] = $spam_message.validation_errors().$error. $profile_error;
						
					}else{
						
						if($chk_user=='true')
						{
							$data["error"] = '<p>Your IP has been Band due to Spam.You can not Register Now.</p>';
						}
						else
						{				
							$data["error"] = "";
						}
						
					}
			
			
					if($_POST)
					{
				
						$data['user_name'] = $this->input->post('user_name');						
						$data['first_name']=$this->input->post('first_name');
						$data['last_name']=$this->input->post('last_name');	
										
						$data['email'] = $this->input->post('email');
						$data['password'] = $this->input->post('password');						
						
						$data['fb_id'] = $this->input->post('fb_id');
						$data['tw_id'] = $this->input->post('tw_id');
						$data['tw_screen_name'] = $this->input->post('tw_screen_name');		
						
						$data['oauth_token']=$this->input->post('oauth_token');
						$data['oauth_token_secret']=$this->input->post('oauth_token_secret');
						
						
						//$data['invite_code']=$this->input->post('invite_code');
					}		
					
			
		} else {			
		
		
			
			$chk_spam_register=$this->home_model->check_spam_register();
				
			if($chk_spam_register==1)
			{
				
				$data["error"] = '<p>Your IP has been Band due to Spam.You can not Register Now.</p>';	
				
				
				$data['user_name'] = $this->input->post('user_name');	
				$data['first_name']=$this->input->post('first_name');
				$data['last_name']=$this->input->post('last_name');					
				
				$data['email'] = $this->input->post('email');
				$data['password'] = $this->input->post('password');
								
				$data['fb_id'] = $this->input->post('fb_id');
				$data['tw_id'] = $this->input->post('tw_id');
				$data['tw_screen_name'] = $this->input->post('tw_screen_name');		
				$data['oauth_token']=$this->input->post('oauth_token');
				$data['oauth_token_secret']=$this->input->post('oauth_token_secret');
										
				//$data['invite_code']=$this->input->post('invite_code');
						
			}
			else
			{
							
				$sign=$this->home_model->register();
				
				if($sign=='1')
				{
					
					
					//echo $this->input->post('user_name'); die();	
						/*if($this->input->post('tw_id'))
						{
							$get_user_info=$this->db->get_where('user',array('profile_name'=>$this->input->post('tw_screen_name')));
						} elseif($this->input->post('fb_id')){
							$get_user_info=$this->db->get_where('user',array('profile_name'=>str_replace('.Bilderia.com','',$this->input->post('user_name'))));
						} else {	
						
							$email=$this->input->post('email');
							$get_user_info=$this->db->get_where('user',array('email'=>$email));
						}*/
						$get_user_info=$this->db->get_where('user',array('profile_name'=>$this->input->post('user_name')));
					$user_info=$get_user_info->row();
					
					//print_r($user_info); die();
					
					$username =$user_info->full_name;	
					$profile_name =$user_info->profile_name;	
								
					$email_verification_code=$user_info->email_verification_code;
					$user_id=$user_info->user_id;
					
					$verification_link=site_url('verify/'.$user_id.'/'.$email_verification_code);
			
			
			if($user_info->email!='')
			{	
				
				/////////////============welcome email===========	

				
				$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Welcome Email'");
				$email_temp=$email_template->row();
				
				if($email_temp)
				{
				$email_from_name=$email_temp->from_name;
				
				$email_address_from=$email_temp->from_address;
				$email_address_reply=$email_temp->reply_address;
				
				$email_subject=$email_temp->subject;				
				$email_message=$email_temp->message;			
				
								
				$email_to=$this->input->post('email');		
				
			
				
				$email_message=str_replace('{break}','<br/>',$email_message);
				$email_message=str_replace('{user_name}',$username,$email_message);			
				$email_message=str_replace('{email}',$email,$email_message);
				
					$email_temp_url=base_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
				$str=$email_message;		
				
					
				/** custom_helper email function **/
					
				email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
				
				}
				
				/////////////============new user email===========	

				
				
				
					
					$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='New User Join'");
					$email_temp=$email_template->row();
					if($email_temp)
					{
					
					$email_from_name=$email_temp->from_name;
					
					$email_address_from=$email_temp->from_address;
					$email_address_reply=$email_temp->reply_address;
					
					$email_subject=$email_temp->subject;				
					$email_message=$email_temp->message;
					
					$username =$this->input->post('user_name');
					$password = $this->input->post('password');
					$email = $this->input->post('email');
					
					$email_to=$this->input->post('email');
					
					$login_link=site_url('login');
					
					$email_message=str_replace('{break}','<br/>',$email_message);
					$email_message=str_replace('{user_name}',$username,$email_message);
					$email_message=str_replace('{password}',$password,$email_message);
					$email_message=str_replace('{email}',$email,$email_message);
					$email_message=str_replace('{login_link}',$login_link,$email_message);
					$email_message=str_replace('{verify_email_link}',$verification_link,$email_message);
					
					
						$email_temp_url=base_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
					$str=$email_message;
				
					
					/** custom_helper email function **/
					
					email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
	
					}



			
				/////////////============new user email===========	
			
			
				} ////-=====email check
				
				
				
				$data_login=array(
						'user_id'=>$user_id,
						'login_date_time'=> date('Y-m-d H:i:s'),
						'login_ip'=>getRealIP()
						); 
				$this->db->insert('user_login',$data_login);
				
				$data_sess=array(
						'user_id' => $user_id,
						'full_name' => $user_info->full_name,
						'first_name'=>$user_info->first_name,
						'email'=>$user_info->email,
						'profile_name' => $user_info->profile_name
						);
				$this->session->set_userdata($data_sess);	
						
					redirect('all');
					
			
				}
				else
				{
					redirect('home/social_signup/fail');
				}
			
			}
						
		}
		
		
		
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/common/fb_sign_up',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render(); 
	}
	
	
	
	
	//////////////============facebook authentication==================
	
	/*
	Function name :_facebook_validate()
	Parameter : $uid (user unquie ID), $email (email addrress)
	Return : redirect to account/dashboard  page
	Use : check user facebook details
	Description : user can check facebook details using this function 
	*/
	
	function _facebook_validate($uid = 0,$email='') 
	{
	
   		//this query basically sees if the users facebook user id is associated with a user.
   		$bQry = $this->home_model->validate_user_facebook($uid,$email);
		
		
		
      	if($bQry=='2'){
		
		
		 	redirect('home');
			
			
		
		}elseif(!$bQry) { // if the user's credentials validated...
		
			 redirect('home');	
	 
			 
      	} else {
        	// incorrect username or password
		
		
        	$data = array();
         	$data["login_failed"] = TRUE;
         	$this->index($data);
      	}
   }
   
   /*
	Function name :facebook()
	Parameter : none
	Return : array of facebook user details
	Use : login using facebook
	Description : user can login using facebook
	*/
	
   function facebook($invite_code='') 
   {
   		
		$data=array();
		$data['social_type']='Facebook';
		
		$user_chk=0;
		if(get_authenticateUserID()>0) { 
			$user_chk=1;
		}
		
		
		
		//1. Check to see if the facebook session has been declared   		
   		if(!$this->fb_connect->fbSession) {
   			//2. If No, bounce back to login   			
			redirect('home');
			
   		} else {
   			
   			
			 $fb_uid = $this->fb_connect->user_id;			
   			 $fb_usr = $this->fb_connect->user;
			
			//echo var_dump($fb_usr);exit;
			//echo '<pre>'; print_r($fb_usr); exit;
			if($fb_uid != ''){
				$this->session->set_userdata(array("facebook_id"=>$fb_uid));					
			}
		
		
		
		
   			if($fb_uid != false) {
	   			//3. If yes, see if the facebook id is associated with any existing account
	   				if(isset($fb_usr["email"]))
					{
						 $email = $fb_usr["email"];
					}
					else
					{
						 $email='';
					}
					
					
		
				$usr = $this->home_model->get_user_by_fb_uid($fb_uid,$email);
	   			
				
				///=====if user is already have account then create login and redirect				
	   			if($usr) {
				
				
				
					if(get_authenticateUserID()>0)
					{
						
						$this->home_model->add_facebook($fb_uid);	
						if($invite_code=='pg_board') {								
							echo "<script>opener.location.reload()</script>";		
							echo "<script>window.close()</script>";				
						}elseif($invite_code=='backinvite') {								
							redirect('invites/facebook');		
						} else {
							echo "<script>window.close()</script>";	
						}	
						
					} else {
					
					
					
					
	   				//3.a. if yes, log the person in   		
						
					$this->_facebook_validate($fb_uid,$email);
					
					}
										
	   			} else {
				
				
					
					
					///===add user facebook account if user is logged in
					if(get_authenticateUserID()>0)
					{
					
						
						$this->home_model->add_facebook($fb_uid);	
						/*if($invite_code=='pg_board') {								
							echo "<script>opener.location.reload()</script>";		
							echo "<script>window.close()</script>";				
						} elseif($invite_code=='backinvite') {								
							redirect('invites/facebook');		
						}else {
							echo "<script>window.close()</script>";	
						}	*/		
						
					}
					else
					{
					
						//if(isset($invite_code)) 
						//{
						
						//if($invite_code!='' && $invite_code!='pg_board' && $invite_code!='backinvite')
						//{
							/////======redirect for sign up===========
					
							 $fname = $fb_usr["first_name"]; 
							 $lname = $fb_usr["last_name"];
							 $fullname = $fb_usr["name"];
							 $pwd = ''; //left blank so user can modify this later
							 
							if(isset($fb_usr["email"]))
							{
								 $email = $fb_usr["email"];
							}
							else
							{
								 $email='';
							}
							
							$fb_img='';
							
							//echo "<pre>";
							//print_r($fb_usr["picture"]["data"]["url"]);
							
							if(isset($fb_usr["picture"]['data']['url']))
							{
								$fb_usr["picture"]=$fb_usr["picture"]['data']['url'];
							}
					//	echo $fb_usr["picture"]; die;
							if(isset($fb_usr["picture"]))
							{
									
									$base_path = base_path();
									$image_settings = image_setting();
									$inPath=$fb_usr["picture"];
									$outPath= $base_path.'upload/user_orig/'.$fb_uid.'.jpg';
								
									//////======
								if(fopen($inPath, "rb"))
								{
									$in=    fopen($inPath, "rb");
									$out=   fopen($outPath, "wb");
									while ($chunk = fread($in,8192))
									{
										fwrite($out, $chunk, 8192);
									}
									fclose($in);
									fclose($out);
									$fb_img=$fb_uid.'.jpg';
									$config['upload_path'] = $base_path.'upload/user_orig/';
									$config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
									//$config['max_size']	= '100';// in KB
									$this->load->library('upload', $config);
								
									
									$this->load->library('image_lib');					
									$this->image_lib->clear();
								
									$config['source_image'] = $this->upload->upload_path.$fb_img;
									$config['new_image'] = $base_path."upload/user/";
									$config['thumb_marker'] = "";
									//$config['maintain_ratio'] = $image_settings['u_ratio'];
									$config['create_thumb'] = TRUE;
									$config['width'] = $image_settings->user_width;
									$config['height'] = $image_settings->user_height;
									$this->image_lib->initialize($config);
							
									if(!$this->image_lib->resize()){
										$error = $this->image_lib->display_errors();				
									}
									
									
									
									$this->image_lib->clear();
								
									$config['source_image'] = $this->upload->upload_path.$fb_img;
									$config['new_image'] = $base_path."upload/user_thumb/";
									$config['thumb_marker'] = "";
									//$config['maintain_ratio'] = $image_settings['u_ratio'];
									$config['create_thumb'] = TRUE;
									$config['width'] = $image_settings->user_small_thumb_width;
									$config['height'] = $image_settings->user_small_thumb_height;
									$this->image_lib->initialize($config);
							
									if(!$this->image_lib->resize()){
										$error = $this->image_lib->display_errors();				
									}
									
								}
									//////====
									
							}
							
							
							$fb_values = array (                    
								'fb_id' => "".$fb_uid,
								'first_name' => strtolower(str_replace (" ", "",$fname)),
								'last_name' => strtolower(str_replace (" ", "",$lname)),                    
								'email'=>$email,
								'tw_id'=>'',
								'fb_img'=>$fb_img,
								'tw_screen_name'=>'',
								'oauth_token'=>'',
								'oauth_token_secret'=>'',
								'google'=>'',
								'yahoo'=>''	
								//'invite_code'=>$invite_code
							);
							 
							//data ready, try to create the new user 
							
							
							 $this->social_signup($fb_values);
							 
						//}
						/*else
						{
							/////====social not found your account
							
							$theme = getThemeName();
							$this->template->set_master_template($theme .'/template.php');
							
							$meta_setting=meta_setting();
							$site_setting=site_setting();
							
							$pageTitle=$site_setting->site_name.' - No Account - '.$meta_setting->title;
							$metaDescription=$site_setting->site_name.' - No Account - '.$meta_setting->meta_description;
							$metaKeyword=$site_setting->site_name.' - No Account - '.$meta_setting->meta_keyword;
							
							$this->template->write('pageTitle',$pageTitle,TRUE);
							$this->template->write('metaDescription',$metaDescription,TRUE);
							$this->template->write('metaKeyword',$metaKeyword,TRUE);
						
						
							//$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
							$this->template->write_view('content_center',$theme .'/layout/common/no_account',$data,TRUE);
							$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
							$this->template->render();	
						}*/
					//}
						/*else
						{
						/////====social not found your account
						$theme = getThemeName();
						$this->template->set_master_template($theme .'/template.php');
						
						$meta_setting=meta_setting();
						$site_setting=site_setting();
						
						$pageTitle=$site_setting->site_name.' - No Account';
						$metaDescription=$site_setting->site_name.' - No Account';
						$metaKeyword=$site_setting->site_name.' - No Account';
						
						$this->template->write('pageTitle',$pageTitle,TRUE);
						$this->template->write('metaDescription',$metaDescription,TRUE);
						$this->template->write('metaKeyword',$metaKeyword,TRUE);
					
					
						//$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
						$this->template->write_view('content_center',$theme .'/layout/common/no_account',$data,TRUE);
						$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
						$this->template->render();	
						
					}
*/					
					}
				
				

	   			}
	   		} 
			
			
			/*else {
				
				
			
				
				
					if(get_authenticateUserID()>0)
					{
						
						$this->home_model->add_facebook($fb_uid);	
						if($invite_code=='pg_board') {								
							echo "<script>opener.location.reload()</script>";		
							echo "<script>window.close()</script>";				
						}elseif($invite_code=='backinvite') {								
							redirect('invites/facebook');		
						} else {
							echo "<script>window.close()</script>";	
						}	
						
					}
					else
						{
						
						
						
						
						/////====social not found your account
						$theme = getThemeName();
						$this->template->set_master_template($theme .'/template.php');
						
						$meta_setting=meta_setting();
						$site_setting=site_setting();
						
						$pageTitle=$site_setting->site_name.' - No Account - '.$meta_setting->title;
						$metaDescription=$site_setting->site_name.' - No Account - '.$meta_setting->meta_description;
						$metaKeyword=$site_setting->site_name.' - No Account - '.$meta_setting->meta_keyword;
						
						$this->template->write('pageTitle',$pageTitle,TRUE);
						$this->template->write('metaDescription',$metaDescription,TRUE);
						$this->template->write('metaKeyword',$metaKeyword,TRUE);
					
					
						//$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
						$this->template->write_view('content_center',$theme .'/layout/common/no_account',$data,TRUE);
						$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
						$this->template->render();	
						
					}
					
					
					
			
			}*/
			
   		}
   } 
   
  	/*
	Function name :remove_fb()
	Parameter : $loc
	Return : redirect to account/customize profile page
	Use : remove facebook connection
	Description : user can remove facebook connection using this function 
	*/
	function remove_fb($loc=0)
	{
		$this->home_model->remove_fb();		
		echo "<script>window.close()</script>";		
	}
	
	
	
	  //////////////============twitter authentication==================
   
   /*
	Function name :twitter_auth()
	Parameter : none
	Return : redirect to auth function
	Use : get user tweeter details
	Description : user can get tweeter details using this function 
	*/
	
   function twitter_auth()
	{
		$this->load->library('tweet');
		
	
		if ( !$this->tweet->logged_in() )
		{
			$this->tweet->set_callback(site_url('home/auth'));
			$this->tweet->login();
			
		}
		else{
				
				// $tokens = $this->tweet->get_tokens();
				
				//echo $tokens['oauth_token'];
				//echo $tokens['oauth_token_secret'];
				
				 redirect('home/auth');		
				
				// These can be saved in a db alongside a user record
				// if you already have your own auth system.
			}
			
			
				
		/*
		$twitter_setting=twitter_setting();
		
		$consumerKey = $twitter_setting->consumer_key;
        $consumerSecret = $twitter_setting->consumer_secret;  
	   
		$oauth_token = NULL; 
		$oauth_token_secret = NULL;
		$callback_url=site_url('home/auth');
		
		$this->load->library('twitteroauth');
		///	
		$connection = new TwitterOAuth($consumerKey,$consumerSecret,$oauth_token,$oauth_token_secret,$callback_url); 	
			
			$request_token=$connection->getRequestToken();
			
			echo "<pre>";
			echo "Request Token"; echo "<br/>";	
			print_r($request_token);
			
			echo " Authorize Url"; echo "<br/>";
			
			
			
			echo $authorize_url=$connection->getAuthorizeURL($request_token,TRUE);
			
			redirect($authorize_url);
			
			
			
			/*print_r($connection->getAuthorizeURL());
			
			echo "Access Token"; echo "<br/>";
			print_r($connection->getAccessToken());
			die;
				*/
				
														   
																			   						
										
	
	}
	
	
	/*
	Function name :auth()
	Parameter : none
	Return : array of tweeter user details
	Use : login using tweeter
	Description : user can login using tweeter
	*/
	
	function auth()
	{
		
	
		if(isset($_REQUEST['denied'])){
			redirect('home');
		}
		
		


		$this->load->library('tweet');
		//select from data if already register otherwise create new
				
		$tokens = $this->tweet->get_tokens();
		$user = $this->tweet->call('get', 'account/verify_credentials');
		
		//echo $tokens['oauth_token'];
		
		//echo '<pre>'; print_r($tokens); die();
		
		$data=array();
		$data['social_type']='twitter';
			
		$twitter_id= $user->id;			
		$first_name='';
		$last_name='';
		$twiter_img_url='';
		$oauth_token='';
		$oauth_token_secret='';
		
		if(isset($tokens['oauth_token']))
		{	
			$oauth_token=$tokens['oauth_token'];
		}
		if(isset($tokens['oauth_token_secret']))
		{
			$oauth_token_secret=$tokens['oauth_token_secret'];	
		}
		
			
	if(isset($user->screen_name) && $twitter_id>0) {

			$screen_name=$user->screen_name;
			
			$twiter_img_url=$user->profile_image_url_https;
			
			if(isset($user->name))
			{
				$first_name=$user->name;
				
				if(substr_count($first_name,' ')>=1)
				{
					$ex=explode(' ',$first_name);
					
					$first_name=$ex[0];
					$last_name=$ex[1];
				}
				
				
			}
			
			$name=$user->name;
		
			$check_twitter_exists=$this->home_model->check_twitter_exists($twitter_id);
			
			
			
			///=====if user is already have account then create login and redirect	
			if($check_twitter_exists)
			{
				$get_twitter_user_detail=$this->home_model->get_twitter_user_detail($twitter_id);
					
				$data1=array(
						'user_id'=>$get_twitter_user_detail['user_id'],
						'login_date_time'=> date('Y-m-d H:i:s'),
						'login_ip'=>getRealIP()
						); 
				$this->db->insert('user_login',$data1);
				
				$data_tw=array(
						'user_id'=>$get_twitter_user_detail['user_id'],
						'full_name' => $get_twitter_user_detail['full_name'],
						'first_name'=> $get_twitter_user_detail['first_name'],
						'email'=>$get_twitter_user_detail['email'],
						'tw_id' =>$get_twitter_user_detail['tw_id'],
						);
				$this->session->set_userdata($data_tw);				
				
				redirect('home');	
	 
			}
			else
			{
				///===add user facebook account if user is logged in
				if(get_authenticateUserID()!='')
				{
		
					$this->home_model->add_twitter($twitter_id,$screen_name,$oauth_token,$oauth_token_secret);		
					/*if($invite_code=='pg_board') {	
						echo "<script>opener.location.reload()</script>";		
						echo "<script>window.close()</script>";	
																			
					} else {						
						echo "<script>window.close()</script>";	
					}*/	
					
				}
				else
				{

							/////======redirect for sign up===========
							error_reporting(0);
				 
							$twiter_img='';
	   						if(isset($twiter_img_url) and function_exists('fopen'))
							{
								
								$base_path = base_path();
								$image_settings = image_setting();
								$inPath=$twiter_img_url;
								$outPath= $base_path.'upload/user_orig/'.$screen_name.'.jpg';
								$in=    fopen($inPath, "rb");
								$out=   fopen($outPath, "wb");
								while ($chunk = fread($in,8192))
								{
									fwrite($out, $chunk, 8192);
								}
								fclose($in);
								fclose($out);
								
								
								$twiter_img=$screen_name.'.jpg';
								$config['upload_path'] = $base_path.'upload/user_orig/';
								$config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
								//$config['max_size']	= '100';// in KB
								$this->load->library('upload', $config);
								
								
								$this->load->library('image_lib');					
								$this->image_lib->clear();	
								
								$config['source_image'] = $this->upload->upload_path.$twiter_img;
								$config['new_image'] = $base_path."upload/user/";
								$config['thumb_marker'] = "";
								//$config['maintain_ratio'] = $image_settings['u_ratio'];
								$config['create_thumb'] = TRUE;
								$config['width'] = $image_settings->user_width;
								$config['height'] = $image_settings->user_height;
								$this->image_lib->initialize($config);
							
							
								
								if(!$this->image_lib->resize()){
									$error = $this->image_lib->display_errors();				
								}
								
								
								
								$this->image_lib->clear();
									
								$config['source_image'] = $this->upload->upload_path.$twiter_img;
								$config['new_image'] = $base_path."upload/user_thumb/";
								$config['thumb_marker'] = "";
								//$config['maintain_ratio'] = $image_settings['u_ratio'];
								$config['create_thumb'] = TRUE;
								$config['width'] = $image_settings->user_small_thumb_width;
								$config['height'] = $image_settings->user_small_thumb_height;
								$this->image_lib->initialize($config);
						
								if(!$this->image_lib->resize()){
									$error = $this->image_lib->display_errors();				
								}
										
										
								$this->image_lib->clear();	
								
							}	
					
				
							 $db_values = array (                    
									'fb_id' => '',
									'first_name' =>strtolower(str_replace (" ", "",$first_name)),
									'last_name' =>strtolower(str_replace (" ", "",$last_name)),                    
									'email'=>'',
									'tw_id'=>$twitter_id,
									'twiter_img'=>$twiter_img,
									'tw_screen_name'=>$screen_name,
									'oauth_token'=>$oauth_token,
									'oauth_token_secret'=>$oauth_token_secret,
									'google'=>'',
									'yahoo'=>''	
									//'invite_code'=>$invite_code,
								);
							
						
							$this->social_signup($db_values);
						
	
				}				 
			}		
		}
		else
		{
			redirect('login');		
		}	
			
	
	}
	
	/*
	Function name :remove_tw()
	Parameter : $loc
	Return : redirect to account/customize profile page
	Use : remove tweeter connection
	Description : user can remove tweeter connection using this function 
	*/
	function remove_tw($loc=0)
	{
		$this->home_model->remove_tw();
		echo "<script>window.close()</script>";
	}
   
   
   
   

    //////////////============other authentication stuff==================
	
	
	
	
	
	/*
	Function name :sign_up()
	Parameter : $msg (cusotm message)
	Return : none
	Use : user can register in this site, login in this site and send request to forget password
	Description : user can register in this site, login in this site and send request to forget password using this function 
				  which called http://hostname/home/sign_up  or 
	              SEO friendly URL which is declare in config route.php file  http://hostname/sign_up
	*/
	
	function sign_up22($msg='')
	{
	
		
		if(check_user_authentication())
		{
			redirect('all');
		}
		
		    $openid = 'openid/openid.php';
			require $openid;
			
			$cryptinstall = 'code/cryptographp.fct.php';
			include $cryptinstall; 
		
		
		
		$chk_user='false';
		$spamer=spam_protection();
      	if($spamer==1 || $spamer=='1') {	$chk_user='true';	}
		
		$site_setting=site_setting();
			
		if($this->input->post('register')) 
		{
		
				
				$this->form_validation->set_rules('full_name', 'Full Name', 'required');
				
				$profile = $this->user_model->check_user_profile_exists($this->input->post('full_name'));
				
				$profile_error= '';
				if($profile){
					$profile_error = 'Username / Account URL is alredy exist.';
				} else {
					$profile_error= '';
				}
				
				
				
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');		
				$this->form_validation->set_rules('password', 'Password', 'required|matches[retypePassword]|min_length[8]|max_length[12]');
				$this->form_validation->set_rules('retypePassword', 'Retype Password', 'required|min_length[8]|max_length[12]');
				
				
				
				if($site_setting->captcha_enable==1)
				{		
				
					 if ($this->input->post('register')) 
					 {	
							
							if (!chk_crypt($_POST['captcha'])) 
							{
								$captcha_result = '<p>Image verification is Wrong.</p>';
							} else {
								$captcha_result = ''; 
							}


					 }
						$error = $captcha_result;
					
					
				} else {
				
					$error ='';
				}
				
					
					if($this->form_validation->run() == FALSE || $error != "" || $chk_user=='true'  || $profile_error !="")
					{	
				
					
					if(validation_errors() || $error != ""  || $profile_error !="")
					{
						$spam_message='';
						
						if($chk_user=='true')
						{
							$spam_message='<p>Your IP has been Band due to Spam.You can not Register Now.</p>';
						}
						
						$data["error"] = $spam_message.validation_errors().$error. $profile_error;
						
					}else{
						
						if($chk_user=='true')
						{
							$data["error"] = '<p>Your IP has been Band due to Spam.You can not Register Now.</p>';
						}
						else
						{				
							$data["error"] = "";
						}
						
					}
			
			
				
					$data['full_name'] = $this->input->post('full_name');					
					$data['email'] = $this->input->post('email');
					$data['password'] = $this->input->post('password');
					
					
					$data['login_email']='';
					$data['login_password']='';
					
					
					$data['view']='login';
					$data['msg']=$msg;
				
					
			
		} else {			

			$chk_spam_register=$this->home_model->check_spam_register();
				
			if($chk_spam_register==1)
			{
				
				$data["error"] = '<p>Your IP has been Band due to Spam.You can not Register Now.</p>';	
				
				
				$data['full_name'] = $this->input->post('full_name');					
				$data['email'] = $this->input->post('email');
				$data['password'] = $this->input->post('password');

				
				$data['view']='login';
				$data['msg']=$msg;
			
			
						
			}
			else
			{
							
				$sign=$this->home_model->register();
				
				if($sign=='1')
				{
			
					$email=$this->input->post('email');
					$get_user_info=$this->db->get_where('user',array('email'=>$email));
					$user_info=$get_user_info->row();
					
					$username =$user_info->full_name;				
					$email_verification_code=$user_info->email_verification_code;
					$user_id=$user_info->user_id;
					
					$verification_link=site_url('verify/'.$user_id.'/'.$email_verification_code);
				
				
			/////////////============welcome email===========	

				
				$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Welcome Email'");
				$email_temp=$email_template->row();
				
				if($email_temp)
				{
				$email_address_from=$email_temp->from_address;
				$email_address_reply=$email_temp->reply_address;
				
				$email_subject=$email_temp->subject;				
				$email_message=$email_temp->message;			
				
								
				$email_to=$this->input->post('email');		
				
			
				
				$email_message=str_replace('{break}','<br/>',$email_message);
				$email_message=str_replace('{user_name}',$username,$email_message);			
				$email_message=str_replace('{email}',$email,$email_message);
				
					$email_temp_url=base_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
				$str=$email_message;		
				
					
				/** custom_helper email function **/
					
				email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str);
				
				}
				
				
				/////////////============new user email===========	


				
				$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='New User Join'");
				$email_temp=$email_template->row();
				
				if($email_temp)
				{
				
				$email_address_from=$email_temp->from_address;
				$email_address_reply=$email_temp->reply_address;
				
				$email_subject=$email_temp->subject;				
				$email_message=$email_temp->message;
				
				$username =$this->input->post('user_name');
				$password = $this->input->post('password');
				$email = $this->input->post('email');
				
				$email_to=$this->input->post('email');
				
				$login_link=site_url('sign_up');
				
				$email_message=str_replace('{break}','<br/>',$email_message);
				$email_message=str_replace('{user_name}',$username,$email_message);
				$email_message=str_replace('{password}',$password,$email_message);
				$email_message=str_replace('{email}',$email,$email_message);
				$email_message=str_replace('{login_link}',$login_link,$email_message);
				$email_message=str_replace('{verify_email_link}',$verification_link,$email_message);
				
				
					$email_temp_url=base_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
				$str=$email_message;
			
				
				/** custom_helper email function **/
				
				email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str);

				}

				/////////////============new user email===========	
						
					//redirect('home/thank_you/sign_up');
					
					$msg=base64_decode($msg);
					
					 
					if(strstr($msg,'http'))
					{	
						redirect($msg);
					} else {			
					 	redirect('home/thank_you/sign_up');			 
					}
					
			
				}
				else
				{
					redirect('home/sign_up/fail');
				}
			
			
			
			}
			
			
			
					
			
			
		}
		
		
		}

		
		else
		{
			$data['msg']=$msg;
			$data['view']='login';
			$data['error']='';
			
			$data['full_name'] = '';					
			$data['email'] = '';
			$data['password'] = '';
		}
		
			
		
		
		$theme = getThemeName();
		$data['theme'] = $theme;
		$this->template->set_master_template($theme .'/template.php');
	
		
		$meta_setting=meta_setting();
		
		$pageTitle='Sign Up - '.$meta_setting->title;
		$metaDescription='Sign Up - '.$meta_setting->meta_description;
		$metaKeyword='Sign Up - '.$meta_setting->meta_keyword;
		
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/common/sign_up',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	
	}

	

	
	
	function google_signin($data = null){
	
		
		$openid = 'openid/openid.php';
	    require $openid;
		
		
		
		try {
			# Change 'localhost' to your domain name.
			$openid = new LightOpenID(base_url());
		
		
		
		
			if(!$openid->mode) {
					$openid->identity = 'https://www.google.com/accounts/o8/id';
					header('Location: ' . $openid->authUrl());
			} elseif($openid->mode == 'cancel') {
				//echo 'User has canceled authentication!';
				redirect('login');
				exit;
			} else {
				//echo 'User ' . ($openid->validate() ? $openid->identity . ' has ' : 'has not ') . 'logged in.';
				$user = $openid->getAttributes();		
					
				if(is_null(@$user['contact/email'])) 
				{
					$openid->identity = ($openid->validate() ."?". $openid->identity);
					# The following two lines request email, full name, and a nickname
					# from the provider. Remove them if you don't need that data.
					//$openid->required = array('contact/email');
					$openid->required = array('namePerson/first','namePerson/last', 'contact/email','birthDate','Person/gender','pref/language');
					$openid->optional = array('namePerson', 'namePerson/friendly');
					header('Location: ' . $openid->authUrl());
				} elseif($openid->mode == 'cancel') {
					//echo 'User has canceled authentication!';
					redirect('login');
					exit;
				} else {
					//echo 'User ' . ($openid->validate() ? $openid->identity . ' has ' : 'has not ') . 'logged in.';
					$user = $openid->getAttributes();
					
					
			//*************** Save user detail ********************//	
			   // Create the User
			  
					$existing_user = findByEmail($user['contact/email']);
					
					
					//print_r($existing_user); die();	
					if($existing_user)
					{
						if($existing_user->profile_name !=''){
							$data1=array(
									'user_id'=>$existing_user->user_id,
									'login_date_time'=> date('Y-m-d H:i:s'),
									'login_ip'=>getRealIP()
									); 
							$this->db->insert('user_login',$data1);
							
							$data_google=array(
									'user_id'=>$existing_user->user_id,
									'full_name' => $existing_user->full_name,
									'first_name'=> $existing_user->first_name,
									'email'=>$existing_user->email,
									'profile_name' => $existing_user->profile_name,
									'google' =>'1',
									);
							$this->session->set_userdata($data_google);				
							
							redirect('all');	 die();
						} else { 
							redirect('home/openid_sign_up/'.$existing_user->user_id);
						}

					}
					else
					{
						
						$first_name = $user['namePerson/first'];
						$last_name  = $user['namePerson/last'];
						
						$uname = explode("@",$user['contact/email']);
						
						
						$google_values = array (                    
								'full_name' =>$uname[0],
								'first_name' => $first_name,
								'last_name' => $last_name,                     
								'email'=>$user['contact/email'],
								//'profile_name' =>clean_url($uname[0]),
								'user_status' =>1,
								'sign_up_date' =>date('Y-m-d H:i:s'),
								'sign_up_ip' =>getRealIP(),
								'tw_id'=>'',
								'fb_id' => '',
								'twitter_screen_name'=>'',
								'tw_oauth_token'=>'',
								'tw_oauth_token_secret'=>'',
								'yahoo'=>'',
								'google'=>1
						);	
						$this->db->insert('user',$google_values);
						
						$user_id = mysql_insert_id();
						
						redirect('home/openid_sign_up/'.$user_id);
						
						
						/*$google_values = array (   
								'full_name' =>$uname[0],
								'first_name' => $first_name,
								'last_name' => $last_name,                    
								'email'=>$user['contact/email'],
								'profile_name' =>$uname[0],               
								'fb_id' => '',
								'tw_id'=>'',
								'fb_img'=>'',
								'tw_screen_name'=>'',
								'oauth_token'=>'',
								'oauth_token_secret'=>'',
								'google'=>1	,
								'yahoo'=>''	
							);
							//data ready, try to create the new user 
							
							
							 $this->social_signup($google_values);*/
							 
						/*$google_values = array (                    
								'full_name' =>$uname[0],
								'first_name' => $first_name,
								'last_name' => $last_name,                    
								'email'=>$user['contact/email'],
								'profile_name' =>clean_url($uname[0]),
								'user_status' =>1,
								'sign_up_date' =>date('Y-m-d H:i:s'),
								'sign_up_ip' =>getRealIP(),
								'tw_id'=>'',
								'fb_id' => '',
								'twitter_screen_name'=>'',
								'tw_oauth_token'=>'',
								'tw_oauth_token_secret'=>'',
								'google'=>1	,
								'yahoo'=>''		
						);	
						$this->db->insert('user',$google_values);
						
						$user_id = mysql_insert_id();
						
						$data1=array(
								'user_id'=>$user_id,
								'login_date_time'=> date('Y-m-d H:i:s'),
								'login_ip'=>getRealIP()
								); 
						$this->db->insert('user_login',$data1);
						
						$data_google=array(
								'user_id'=>$user_id,
								'full_name' => $uname[0],
								'first_name'=> $first_name,
								'email'=>$user['contact/email'],
								'google' =>'1',
								'profile_name' => $user->profile_name
								);
						$this->session->set_userdata($data_google);	

						redirect('all')	; die();*/
						

					}
			
					
				}
			}
		} catch(ErrorException $e) {
			echo $e->getMessage();
		}
		
		exit;
	}
	
	
	function yahoo_signin($data = null){
	
		$openid = 'openid/openid.php';
	    require $openid;
		
		try {
	
   		 # Change 'localhost' to your domain name.
			$openid = new LightOpenID(base_url());
			if(!$openid->mode) {
					$openid->identity = 'me.yahoo.com';
					# The following two lines request email, full name, and a nickname
					# from the provider. Remove them if you don't need that data.
					$openid->required = array('namePerson/first','namePerson/last', 'contact/email','pref/language');
					$openid->optional = array('namePerson', 'namePerson/friendly');
					header('Location: ' . $openid->authUrl());
			} elseif($openid->mode == 'cancel') {
				//echo 'User has canceled authentication!';
				redirect('login');
				exit;
			} else {
				//echo 'User ' . ($openid->validate() ? $openid->identity . ' has ' : 'has not ') . 'logged in.';
			$user = $openid->getAttributes();
			
			//*************** Save user detail ********************//

                $existing_user = findByEmail($user['contact/email']);
				
				//echo '<pre>'; print_r($existing_user); die();

					if($existing_user)
					{
						if($existing_user->profile_name !=''){
							$data1=array(
									'user_id'=>$existing_user->user_id,
									'login_date_time'=> date('Y-m-d H:i:s'),
									'login_ip'=>getRealIP()
									); 
							$this->db->insert('user_login',$data1);
							
							$data_google=array(
									'user_id'=>$existing_user->user_id,
									'full_name' => $existing_user->full_name,
									'first_name'=> $existing_user->first_name,
									'email'=>$existing_user->email,
									'yahoo'=>'1',
									'profile_name' => $existing_user->profile_name
									);
							$this->session->set_userdata($data_google);				
							
							redirect('all');
						} else { redirect('home/openid_sign_up/'.$existing_user->user_id);}	

					}
					else
					{
						$full_name = explode(" ",$user['namePerson']);
						$uname = explode("@",$user['contact/email']);

						$yahoo_values = array (                    
								'full_name' =>$uname[0],
								'first_name' => $full_name[0],
								'last_name' => $full_name[1],                    
								'email'=>$user['contact/email'],
								//'profile_name' =>clean_url($uname[0]),
								'user_status' =>1,
								'sign_up_date' =>date('Y-m-d H:i:s'),
								'sign_up_ip' =>getRealIP(),
								'tw_id'=>'',
								'fb_id' => '',
								'twitter_screen_name'=>'',
								'tw_oauth_token'=>'',
								'tw_oauth_token_secret'=>'',
								'yahoo'=>1,
								'google'=>''
						);	
						$this->db->insert('user',$yahoo_values);
						
						$user_id = mysql_insert_id();
						
						redirect('home/openid_sign_up/'.$user_id);
						
						/*$data1=array(
								'user_id'=>$user_id,
								'login_date_time'=> date('Y-m-d H:i:s'),
								'login_ip'=>getRealIP()
								); 
						$this->db->insert('user_login',$data1);
						
						$data_yahoo=array(
								'user_id'=>$user_id,
								'full_name' => $uname[0],
								'first_name'=> $first_name,
								'profile_name' => $user->profile_name,
								'email'=>$user['contact/email'],
								'yahoo' =>'1'
								);
						$this->session->set_userdata($data_yahoo);	
						
						
						redirect('all'); die();*/
						
						/*$yahoo_values = array (   
								'full_name' =>$uname[0],
								'first_name' => $full_name[0],
								'last_name' => $full_name[1],                    
								'email'=>$user['contact/email'],
								'profile_name' =>$uname[0],                 
								'fb_id' => '',
								'tw_id'=>'',
								'fb_img'=>'',
								'tw_screen_name'=>'',
								'oauth_token'=>'',
								'oauth_token_secret'=>'',
								'google'=>'',
								'yahoo'=>1	
							);
							 
							//data ready, try to create the new user 
							$this->social_signup($yahoo_values);*/
						

					}

				
			
				

            }
			
			//**************** Save user end *********/
		} catch(ErrorException $e) {
			echo $e->getMessage();
		}
		exit;
	}
	
	function openid_sign_up($user_id){
		
		if(get_authenticateUserID()!='') { 	 	
			$check_suspend=check_user_suspend();
			if($check_suspend!=0) {  redirect('suspend'); }
			redirect('home');
		}
			$data['fb_img']='';
			$data['twiter_img']='';
			$data['user_id']=$user_id;
			/*if($openid == 0){
				$data['google']=1;
				$data['yahoo']=0;
			} else {
				$data['google']=0;
				$data['yahoo']=1;
			}*/
			
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		$meta_setting=meta_setting();
		$site_setting=site_setting();
		
		$pageTitle=$site_setting->site_name.' - Create Account - '.$meta_setting->title;
		$metaDescription=$site_setting->site_name.' - Create Account - '.$meta_setting->meta_description;
		$metaKeyword=$site_setting->site_name.' - Create Account - '.$meta_setting->meta_keyword;
		
		
		if($user_id != '' && $user_id != 0)
		{
			
			$social_data = $this->home_model->get_user_profile_by_id($user_id);
			//print_r($social_data);
			$data['user_name']= $social_data->full_name;	
			$data['first_name']=$social_data->first_name;
			$data['last_name']=$social_data->last_name;
			$data['zip_code']= '';
			$data['mobile_no']='';
			$data['email']= $social_data->email;
			$data['password']= '';
			$data['fb_id']= $social_data->fb_id;
			$data['tw_id']=$social_data->tw_id;
			$data['tw_screen_name']=$social_data->twitter_screen_name;
			$data['oauth_token']=$social_data->tw_oauth_token;
			$data['oauth_token_secret']=$social_data->tw_oauth_token_secret;
			$data['google']=$social_data->google;
			$data['yahoo']=$social_data->yahoo;
			
			if(isset($social_data->fb_img))$data['fb_img']=$social_data->fb_img;
			if(isset($social_data->twiter_img))$data['twiter_img']=$social_data->twiter_img;
		}
		else
		{
			$data['user_name']= '';
			$data['first_name']='';
			$data['last_name']='';
			$data['zip_code']= '';
			$data['mobile_no']= '';
			$data['email']= '';
			$data['password']= '';
			$data['fb_id']= '';
			$data['tw_id']='';
			$data['tw_screen_name']='';
			$data['oauth_token']='';
			$data['oauth_token_secret']='';
			$data['google']='';
			$data['yahoo']='';
			
			$data['fb_img']='';
			$data['twiter_img']='';
			
		}

		
		$chk_user='false';
		$spamer=spam_protection();
      	if($spamer==1 || $spamer=='1') {	$chk_user='true';	}

				$this->form_validation->set_rules('user_name', 'Username', 'required');
				
				
				$profile = $this->user_model->check_user_profile_exists($this->input->post('user_name'));
				
				$profile_error= '';
				if($profile){
					$profile_error = 'Username / Account URL is alredy exist';
				} else {
					$profile_error= '';
				}
				
				
				
					$error ='';
				
				
				
					if($this->form_validation->run() == FALSE || $error != "" || $chk_user=='true' || $profile_error !="")
					{	
				
					
					if(validation_errors() || $error != "" || $profile_error !="")
					{
						$spam_message='';
						
						if($chk_user=='true')
						{
							$spam_message='<p>Your IP has been Band due to Spam.You can not Register Now.</p>';
						}
						
						$data["error"] = $spam_message.validation_errors().$error. $profile_error;
						
					}else{
						
						if($chk_user=='true')
						{
							$data["error"] = '<p>Your IP has been Band due to Spam.You can not Register Now.</p>';
						}
						else
						{				
							$data["error"] = "";
						}
						
					}
			
			
					if($_POST)
					{
				
						$data['user_name'] =  $this->input->post('user_name');						
						$data['first_name']=$this->input->post('first_name');
						$data['last_name']=$this->input->post('last_name');	
										
						$data['email'] = $this->input->post('email');
						$data['password'] = $this->input->post('password');						
						
						$data['fb_id'] = $this->input->post('fb_id');
						$data['tw_id'] = $this->input->post('tw_id');
						$data['tw_screen_name'] = $this->input->post('tw_screen_name');		
						
						$data['oauth_token']=$this->input->post('oauth_token');
						$data['oauth_token_secret']=$this->input->post('oauth_token_secret');
						
						$data['google'] = $this->input->post('google');
						$data['yahoo'] = $this->input->post('yahoo');

						
					}		
					
			
		} else {			
		
		
			
			$chk_spam_register=$this->home_model->check_spam_register();
				
			if($chk_spam_register==1)
			{
				
				$data["error"] = '<p>Your IP has been Band due to Spam.You can not Register Now.</p>';	
				
				
				$data['user_name'] = $this->input->post('user_name');	
				$data['first_name']=$this->input->post('first_name');
				$data['last_name']=$this->input->post('last_name');					
				
				$data['email'] = $this->input->post('email');
				$data['password'] = $this->input->post('password');
								
				$data['fb_id'] = $this->input->post('fb_id');
				$data['tw_id'] = $this->input->post('tw_id');
				$data['tw_screen_name'] = $this->input->post('tw_screen_name');		
				$data['oauth_token']=$this->input->post('oauth_token');
				$data['oauth_token_secret']=$this->input->post('oauth_token_secret');
				
				$data['google'] = $this->input->post('google');
				$data['yahoo'] = $this->input->post('yahoo');
				
						
			}
			else
			{
							
				$sign=$this->home_model->openid_register();
				
				if($sign=='1')
				{
							
					$email=$this->input->post('email');
					$get_user_info=$this->db->get_where('user',array('email'=>$email));
						
					$user_info=$get_user_info->row();
					
					$username =$user_info->full_name;	
					$profile_name =$user_info->profile_name;	
								
					$email_verification_code=$user_info->email_verification_code;
					$user_id=$user_info->user_id;
					
					$verification_link=site_url('verify/'.$user_id.'/'.$email_verification_code);
				
				
				/////////////============welcome email===========	

				
				$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Welcome Email'");
				$email_temp=$email_template->row();
				
				if($email_temp)
				{
				$email_from_name=$email_temp->from_name;
				
				$email_address_from=$email_temp->from_address;
				$email_address_reply=$email_temp->reply_address;
				
				$email_subject=$email_temp->subject;				
				$email_message=$email_temp->message;			
				
								
				$email_to=$this->input->post('email');		
				
			
				
				$email_message=str_replace('{break}','<br/>',$email_message);
				$email_message=str_replace('{user_name}',$username,$email_message);			
				$email_message=str_replace('{email}',$email,$email_message);
				
					$email_temp_url=base_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
				$str=$email_message;		
				
					
				/** custom_helper email function **/
					
				email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
				
				}
				
				/////////////============new user email===========	


					
					$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='New User Join'");
					$email_temp=$email_template->row();
					if($email_temp)
					{
					
					$email_from_name=$email_temp->from_name;
					
					$email_address_from=$email_temp->from_address;
					$email_address_reply=$email_temp->reply_address;
					
					$email_subject=$email_temp->subject;				
					$email_message=$email_temp->message;
					
					$username =$this->input->post('user_name');
					$password = $this->input->post('password');
					$email = $this->input->post('email');
					
					$email_to=$this->input->post('email');
					
					$login_link=site_url('login');
					
					$email_message=str_replace('{break}','<br/>',$email_message);
					$email_message=str_replace('{user_name}',$username,$email_message);
					$email_message=str_replace('{password}',$password,$email_message);
					$email_message=str_replace('{email}',$email,$email_message);
					$email_message=str_replace('{login_link}',$login_link,$email_message);
					$email_message=str_replace('{verify_email_link}',$verification_link,$email_message);
					
					
						$email_temp_url=base_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
					$str=$email_message;
				
					
					/** custom_helper email function **/
					
					email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
	
					}

				/////////////============new user email===========	
			
				$data_login=array(
						'user_id'=>$user_id,
						'login_date_time'=> date('Y-m-d H:i:s'),
						'login_ip'=>getRealIP()
						); 
				$this->db->insert('user_login',$data_login);
				
				$data_sess=array(
						'user_id' => $user_id,
						'full_name' => $user_info->full_name,
						'first_name'=>$user_info->first_name,
						'email'=>$user_info->email,
						'profile_name' => $user_info->profile_name,	
						);
				$this->session->set_userdata($data_sess);	
						
					redirect('all');
					
			
				}
				else
				{
					redirect('home/social_signup/fail');
				}
			
			}
						
		}
		
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/common/fb_sign_up',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render(); //echo 'test'; die();
	}
	
	
	

	
	
	
	
	function invited($code,$invite_email='')
	{
		if($this->session->userdata('user_id')!='')
		{
			redirect('home/main_dashboard');
		}
		$email='';
		if($invite_email!='facebook')
		{
		$check_request_exists=$this->db->get_where('invite_request',array('invite_code'=>$code,'invite_email'=>$invite_email));
		$email=$invite_email;
		}
		elseif($invite_email=='facebook')
		{
		}
		$affiliate_setting=affiliate_setting();
		$referral_time_expire=$affiliate_setting->cookie_expire_time;
		$domain_name='';
		$get_domain_name=get_domain_name(base_url());
		if($get_domain_name)
		{
			$domain_name=$get_domain_name;
		}
		if($referral_time_expire==0 || $referral_time_expire=='')
		{
			$referral_time_expire=86500;
		}	
		else
		{
			$referral_time_expire=$referral_time_expire*60;
		}
			$this->load->helper('cookie');
			$cookie = array(
			'name'   => 'invite_code',
			'value'  => $code,
			'expire' => time()+$referral_time_expire,
			'domain' => $domain_name,	
			'secure' => TRUE
			);
			//set_cookie($cookie);
			setcookie('invite_code',$code,time()+$referral_time_expire,'/',$domain_name,TRUE);
			$this->session->set_userdata(array('invite_code'=>$code));
			$db_values = array (                    
			'fb_uid' => '',
			'user_name' =>'',
			'last_name' =>'',                    
			'email'=>$email,
			'tw_id'=>'',
			'fb_img'=>'',
			'fb_access_token'=>'',
			'tw_screen_name'=>'',
			'oauth_token'=>'',
			'oauth_token_secret'=>'',
			'invite_code'=>$code,
			);
		//data ready, try to create the new user 
		$this->social_signup($db_values);
	}
	
	
	
	
	function contact_us()
	{
		
		$data=array();	
		
		$site_setting=site_setting();
		$data['site_setting']=$site_setting;
		
		$spam=spam_protection();
      	
		$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Contact Us'");
		$email_temp=$email_template->row();	
				
		$data['email_temp']=$email_temp;	
		
		$this->form_validation->set_rules('your_name', 'Your Name', 'required|alpha_space');	
		$this->form_validation->set_rules('email_address', 'Your Email', 'required|valid_email');
		$this->form_validation->set_rules('subject', 'Your Subject', 'required');
		$this->form_validation->set_rules('message', 'Your Message', 'required');
		
		$error ='';
		
		if($this->form_validation->run() == FALSE)
		{			
			if(validation_errors() || $spam==1 || $error != "")
			{	
				$spam_message='';
				if($spam==1)
				{
					$spam_message='Your IP has been Banned due to Spam.Please contact Web Master';
					
				}
				
				$data["error"] = validation_errors().$spam_message.$error;
			}else{
				
				$spam_message='';
				if($spam==1)
				{
					$spam_message='Your IP has been Banned due to Spam.Please contact Web Master';
				}
				
				$data["error"] = $spam_message;
			}
			
			$data["your_name"] = $this->input->post('your_name');
			$data["email_address"] = $this->input->post('email_address');
			$data["subject"] = $this->input->post('subject');
			$data["message"] = $this->input->post('message');
			$data['msg']='';
			
			
		}
		
		else
		{
			
			
			if($spam==1)
			{
				$spam_message='Your IP has been Banned due to Spam.Please contact Web Master';
				
				$data['msg']='';
				$data["error"] = $spam_message;
				$data["your_name"] = '';
				$data["email_address"] = '';
				$data["subject"] = '';
				$data["message"] = '';
			}
			else
			{
			
				$dac=array(
				'inquire_date'=>date('Y-m-d'),
				'inquire_spam_ip'=>getRealIP(),
				);
				
				$this->db->insert('spam_inquiry',$dac);
			
	
				
							
			
				if($email_temp)
				{
					$email_from_name=$this->input->post('your_name');
					
					$email_address_from=$this->input->post('email_address');  
					$email_address_reply=$email_address_from;  
					
					$email_subject=$email_temp->subject.' : '.$this->input->post('subject');				
					$email_message=$email_temp->message;
					
					$email_to = $email_temp->from_address;
					
					$contact_us_link = site_url('home/contact');
					
					$email_message=str_replace('{break}','<br/>',$email_message);
					$email_message=str_replace('{name}',$email_from_name,$email_message);
					$email_message=str_replace('{email}',$email_address_from,$email_message);
					$email_message=str_replace('{contact_us_link}',$contact_us_link,$email_message);
					$email_message=str_replace('{message}',$this->input->post('message'),$email_message);
					$email_temp_url=base_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
					$str=$email_message;
					
					
					/** custom_helper email function **/
										
					email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
				
				}
			
			
			
				$data['msg']='Your Message has been sent successfully.';
				$data["error"] = "";
				$data["your_name"] = '';
				$data["email_address"] = '';
				$data["subject"] = '';
				$data["message"] = '';
			}
		
			
		}
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$meta_setting=meta_setting();
		
		
		$pageTitle=$meta_setting->title;
		$metaDescription=$meta_setting->meta_description;
		$metaKeyword=$meta_setting->meta_keyword;
		
		
		$this->template->write('pageTitle', 'Contact Us-'.$pageTitle, TRUE);
		$this->template->write('metaDescription', 'Contact Us-'.$metaDescription, TRUE);
		$this->template->write('metaKeyword', 'Contact Us-'.$metaKeyword, TRUE);	
			
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('campaign_box',$theme .'/layout/common/contact_us',$data,TRUE);		
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
		
		
	}
	
	function terms_condition()
	{

		$site_setting=site_setting();
		
		if (!$this->input->post('agree'))
		{			
			$this->form_validation->set_message('terms_condition', 'Please Accept Terms & Condition of '.ucwords($site_setting->site_name));
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	
	public function cmsdemo()
	{
			
		$data=array();
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$meta_setting=meta_setting();
		$site_setting=site_setting();
		
		$pageTitle=$meta_setting->title;
		$metaDescription=$meta_setting->meta_description;
		$metaKeyword=$meta_setting->meta_keyword;
		 
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('campaign_box',$theme .'/layout/cmsdemo/cmsdemo',$data,TRUE);		
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
		
	}
	
	
	public function content($slug='')
	{
	
		$data=array();
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$meta_setting=meta_setting();
		$site_setting=site_setting();
		
		$pageTitle=$meta_setting->title;
		$metaDescription=$meta_setting->meta_description;
		$metaKeyword=$meta_setting->meta_keyword;
		 
                $res=$this->home_model->get_page_content($slug);
                if(empty($res)){
                    redirect('home/404');
                }
		$data['res']=$res;
                
                $pageTitle=$res->pages_title;
		$metaDescription=$res->meta_description;
		$metaKeyword=$res->meta_keyword;
		
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('campaign_box',$theme .'/layout/content/page',$data,TRUE);		
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
		
	}
	
	public function contactus($msg='')
	{
	
	
		$data= array();
		$data['msg']=$msg;
		$data['error'] = '';
		$chk_user='false';
		$spamer=spam_protection();
      	if($spamer==1 || $spamer=='1') {	$chk_user='true';	}
		
		if($this->input->post('submit')) 
		{
				$this->form_validation->set_rules('first_name', 'First Name', 'required');
				$this->form_validation->set_rules('last_name', 'Last Name', 'required');
				$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
				
			
				if($this->form_validation->run() == FALSE || $chk_user=='true')
				{
						if(validation_errors() || $chk_user=='true')
						{
							$spam_message='';
							if($chk_user=='true')
							{
								$spam_message='<p>Your IP has been Band due to Spam.You can not contact.</p>';
								$send_spam_mail = $this->home_model->send_spam_mail();
							}
							$data["error"] = validation_errors().$spam_message;
						}
						else
						{
							if($chk_user=='true')
							{
								$data["error"] = '<p>Your IP has been Band due to Spam.You can not contact.</p>';
								$send_spam_mail = $this->home_model->send_spam_mail();
							}
							else
							{				
								$data["error"] = "";
							}
						}
						$data['first_name']=$this->input->post('first_name');
						$data['last_name']=$this->input->post('last_name');
						$data['email']=$this->input->post('email');
						
					}
					else
					{
						$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Contact Us'");
						$email_temp=$email_template->row();	
						
						$email_from_name=$this->input->post('first_name').''.$this->input->post('last_name');
					
						$email_address_from=$this->input->post('email');  
						$email_address_reply=$email_address_from;  
						
						$email_subject=$email_temp->subject.' : '.$this->input->post('subject');				
						$email_message=$email_temp->message;
						
						$email_to = $email_temp->from_address;
						
						$contact_us_link = site_url('home/contactus');
						
						$email_message=str_replace('{break}','<br/>',$email_message);
						$email_message=str_replace('{name}',$email_from_name,$email_message);
						$email_message=str_replace('{email}',$email_address_from,$email_message);
						$email_message=str_replace('{contact_us_link}',$contact_us_link,$email_message);
						$email_message=str_replace('{message}',$this->input->post('message'),$email_message);
						$email_temp_url=base_url().'email/';
						$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
						$str=$email_message;
						
						
						/** custom_helper email function **/
											
						email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
						
						redirect('home/contactus/success');
						//send email
					}	
		
		}		
	
			$theme = getThemeName();
			$data['theme'] = $theme;
			$this->template->set_master_template($theme .'/template.php');
			
			$meta_setting=meta_setting();
			$site_setting=site_setting();
			
			$pageTitle=$site_setting->site_name.' - Contact Us - '.$meta_setting->title;
			$metaDescription=$site_setting->site_name.' - Contact Us - '.$meta_setting->meta_description;
			$metaKeyword=$site_setting->site_name.' - Contact Us - '.$meta_setting->meta_keyword;
			
			$this->template->write('pageTitle',$pageTitle,TRUE);
			$this->template->write('metaDescription',$metaDescription,TRUE);
			$this->template->write('metaKeyword',$metaKeyword,TRUE);
		
			$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
			$this->template->write_view('content_center',$theme .'/layout/common/contact_us',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();	
	}
	
	public function test()
	{
			
		$data=array();
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$meta_setting=meta_setting();
		$site_setting=site_setting();
		
		$pageTitle=$meta_setting->title;
		$metaDescription=$meta_setting->meta_description;
		$metaKeyword=$meta_setting->meta_keyword;
		 
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('campaign_box',$theme .'/layout/content/test',$data,TRUE);		
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
		
	}
	

}

?>