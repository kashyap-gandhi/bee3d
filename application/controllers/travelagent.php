<?php
/*********************************************************************************
 * This the taskrabbitclone.com  by Rockers Technology. is paid software. It is released under the terms of 
 * the following BSD License.
 * 
 *  Rockers Technologies (Head Office)
 *    5038,Berthpage Dr
 *    suwanee, GA. Zip Code : 30024
    
 *    E-mail Address : nishu@rockersinfo.com
 * 
 * Copyright © 2012-2020 by Rockers Technology , INC a domestic profit corporation has been duly incorporated under
the laws of the state of georiga , USA. www.rockersinfo.com
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this 
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this 
 *   list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.

 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class Travelagent extends IWEB_Controller {
	
	/*
	Function name :Home()
	Description :Its Default Constuctor which called when home object initialzie.its load necesary models
	*/
	
	function Travelagent()
	{
		parent::__construct();	
		$this->load->model('travelagent_model');	
		$this->load->model('home_model');	
		$this->load->model('user_model');		
	}
	
	
	/*
	Function name :index()
	Parameter :none
	Return : none
	Use : user can see category list, map, recent tasks, top runners and other details
	Description : site home page its default function which called http://hostname/home
				  SEO friendly URL which is declare in config route.php file  http://hostname/
	*/
	public function index($profile_name='',$msg='')
	{
		//redirect('travelagent/directory');
		 	$data=array();
			$theme = getThemeName();
			$this->template->set_master_template($theme .'/template.php');
			
			$data['theme']=$theme;
			$data['select'] = 'user_profile';
			$data['msg']=$msg;
			
			$data['profile_name']=$profile_name;
			
			$meta_setting=meta_setting();
			
			$user_data = get_user_profile_by_profile_name($profile_name);
			
			
			
			if(!$user_data)
			{
				$pageTitle='404 - '.$meta_setting->title;
				$metaDescription='404 - '.$meta_setting->meta_description;
				$metaKeyword='404 - '.$meta_setting->meta_keyword;
		
				
				$this->template->write_view('campaign_box',$theme .'/layout/common/no_user_profile',$data,TRUE);	
			}
			else
			{
				
				$data['user_deal'] = get_user_deal($user_data->user_id);
				
				$user_info = get_user_profile_by_id($user_data->user_id);
				$data['user_info']=$user_info;
			
				$agent_info = check_user_agent($user_data->user_id);
				$data['agent_info']=$agent_info;
				
				if(!$agent_info)
				{
					redirect('user/'.$profile_name);
				}
				
				$review_limit = 5;
				$data['reviews'] = get_user_reviews($user_data->user_id,$review_limit);	
				
				$pageTitle=ucwords($agent_info->agency_name.' '.$agent_info->agency_location);
				$metaDescription=ucwords($agent_info->agency_name.' '.$agent_info->agency_location);
				$metaKeyword=ucwords($agent_info->agency_name.' '.$agent_info->agency_location);
				
				$this->template->write_view('content_center',$theme .'/layout/user/profile_detail',$data,TRUE);
			
			}
			
			$this->template->write('pageTitle',$pageTitle,TRUE);
			$this->template->write('metaDescription',$metaDescription,TRUE);
			$this->template->write('metaKeyword',$metaKeyword,TRUE);
			$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
			
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();	
		
	}
	
	
	public function in($offset=0)
	{
		$data = array();
		$limit=10;
		//$offset=0;
		
		
	

		if(isset($_REQUEST['city'])) { if($_REQUEST['city']!='') {  $city_name=$_REQUEST['city'];   } }
		if(isset($_REQUEST['country'])) { if($_REQUEST['country']!='') {  $country_id=$_REQUEST['country'];   } }
		if(isset($_REQUEST['per_page'])) { if($_REQUEST['per_page']!='') {  $offset=$_REQUEST['per_page'];   } }
		if(isset($_REQUEST['keyword'])) { if($_REQUEST['keyword']!='') {  $keyword=$_REQUEST['keyword'];   } }
		if(isset($_REQUEST['type'])) { if($_REQUEST['type']!='') {  $type=$_REQUEST['type'];   } else { $type=rand(1,5); } }  else { $type=rand(1,5); }
		
		
		$rd=0;
		
		if($type==5)
		{
	
	if(isset($_REQUEST['rd'])){ if($_REQUEST['rd']!=''){  $rd=$_REQUEST['rd'];   } else { $rd=rand(0,9); } }  else { $rd=rand(0,9); }
						
		}
		
		if($country_id=='' || $country_id==0)
		{
			redirect('home');
		}
		
		if($city_name=='')
		{
				redirect('home');
		}
		
		
		//$city_name=urldecode($city_name);
		$data['per_page'] = $limit;
		$data['city_name'] = $city_name; 
		$data['keyword'] = $keyword; 
		
		$data['type']=$type;
		$data['rd']=$rd;
		
		
		
		$city_detail=get_city_detail_from_name($city_name);
		
		$data['select_city']=explode(',',$city_name);
		$data['city_detail']=$city_detail;
		
		$data['offset']=$offset;
		$data['countrylist'] = country_list();
		$data['country_id'] = $country_id;
		$this->load->library('pagination');
		$config['uri_segment']='4';
			
			
		//$config['base_url'] = site_url('travelagent/in/'.$city_name);
		
		
		$set_url = 'http://'.$_SERVER['HTTP_HOST'].substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], "/")+1).'travelagent/in?city='.$city_name.'&country='.$country_id.'&keyword='.$keyword.'&type='.$type.'&rd='.$rd;
		
		$config['page_query_string']=true;	
		$config['base_url'] = $set_url;
		$config['total_rows'] = $this->travelagent_model->get_total_agent_multicity_wise($city_name,$keyword,$type,$rd);
		$config['per_page'] = $limit;
		
		$this->pagination->initialize($config);		
			
		$data['page_link'] = $this->pagination->create_links();
		
		$data['get_agent'] = $this->travelagent_model->get_agent_multicity_wise($city_name,$keyword,$limit,$offset,$type,$rd);
		
		/*echo "<pre>";
		print_r($data['get_agent']);
		die;*/
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		
		$pageTitle='Agent >>'.$city_name. '>>'.$meta_setting->title;
		$metaDescription='Agent >>'.$city_name. '>>'.$meta_setting->meta_description;
		$metaKeyword='Agent >>'.$city_name. '>>'.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/travelagent/agent_search',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	}
	
	public function multicity($offset=0)
	{
		$data = array();
		$limit=10;
		
		$data['per_page'] = $limit;
		
		if(isset($_REQUEST['city'])) { if($_REQUEST['city']!='') {  $city=$_REQUEST['city'];   } }
		if(isset($_REQUEST['country'])) { if($_REQUEST['country']!='') {  $country_id=$_REQUEST['country'];   } }
		if(isset($_REQUEST['state'])) { if($_REQUEST['state']!='') {  $state_id=$_REQUEST['state'];   } }
		if(isset($_REQUEST['keyword'])) { if($_REQUEST['keyword']!='') {  $keyword=$_REQUEST['keyword'];   } }
		if(isset($_REQUEST['type'])) { if($_REQUEST['type']!='') {  $type=$_REQUEST['type'];   } else { $type=rand(1,5); } }  else { $type=rand(1,5); }
		
		
		
		$rd=0;
		
		if($type==5)
		{
	
	if(isset($_REQUEST['rd'])){ if($_REQUEST['rd']!=''){  $rd=$_REQUEST['rd'];   } else { $rd=rand(0,9); } }  else { $rd=rand(0,9); }
						
		}
		
			
			
		 $data['city_name'] ='';
		
		
		if($_REQUEST['city']!='')
		{
	
			$data['city_name'] = $city; 
			$data['select_city']=explode(',',$city);
		
			$city_name = $city; 
			$city_detail=get_city_detail_from_name($city);
			$data['city_detail']=$city_detail;
		}
		else
		{
			$data['city_name'] = ''; 
			$city_name = ''; 
			$data['city_detail']='';
		}
		
		$data['type']=$type;
		$data['rd']=$rd;
		
		$data['countrylist'] = country_list();
		$data['country_id'] = $country_id;
		$data['keyword'] = $keyword; 
		$data['offset']=$offset;
		$this->load->library('pagination');
		$config['uri_segment']='4';
			
		if($_REQUEST['city']!='')
		{	
		
	$set_url = 'http://'.$_SERVER['HTTP_HOST'].substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], "/")+1).'travelagent/in?city='.$city_name.'&country='.$country_id.'&keyword='.$keyword.'&type='.$type.'&rd='.$rd;
		
		}
		else
		{
			$set_url='';
		}
		$config['page_query_string']=true;		
		$config['base_url'] = $set_url;
		//$config['base_url'] = site_url('travelagent/multicity/'.$city);
		if($_REQUEST['city']!='')
		{
			$config['total_rows'] = $this->travelagent_model->get_total_agent_multicity_wise($city_name,$keyword,$type,$rd);
		}
		
		
		$config['per_page'] = $limit;
		
		$this->pagination->initialize($config);		
			
		$data['page_link'] = $this->pagination->create_links();
		
		if($_REQUEST['city']!='')
		{
			$data['get_agent'] = $this->travelagent_model->get_agent_multicity_wise($city,$keyword,$limit,$offset,$type,$rd);
		}
		else
		{
			$data['get_agent'] ='';
		}
		
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		
		$this->load->view($theme.'/layout/travelagent/agent_search_ajax',$data);
	}
	
	
	
	function search($city_name='all',$keyword='',$offset=0)
	{
		
		$data = array();
		
		$limit=1;
		//$offset=0;
		
		$city_name=urldecode($city_name);
		
		$data['city_name'] = $city_name; 
		
		$city_detail=get_city_detail_from_name($city_name);
		
		
		$data['city_detail']=$city_detail;
		
		
		
		if($_POST)
		{
			if($this->input->post('keyword') != '' )
			{
				$keyword =$this->input->post('keyword');
			}
			else
			{
				$keyword='none';
			}
		
			
		}else
			{
				$keyword=$keyword;
			
			}
			
		$data['keyword']=$keyword;
		$data['offset']=$offset;
		$data['countrylist'] = country_list();
		
		
		$this->load->library('pagination');
		
	
			if($keyword != '' && $offset > 0)
			{
				$config['uri_segment']='5';
			}
			else if($keyword != '')
			{
				$config['uri_segment']='4';
			}
			else 
			{
				$config['uri_segment']='3';
			}
			
		
			
		$config['base_url'] = site_url('travelagent/search/'.$city_name.'/'.$keyword);
		$config['total_rows'] = $this->travelagent_model->get_total_keyword_agent_city_wise($city_name,$keyword);
		$config['per_page'] = $limit;
		
		$this->pagination->initialize($config);		
			
		$data['page_link'] = $this->pagination->create_links();
		
		$data['get_agent'] = $this->travelagent_model->get_agent_keyword_city_wise($city_name,$keyword,$limit,$offset);
		
		
		
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		
		$pageTitle='Agent >>'.$city_name. '>>'.$meta_setting->title;
		$metaDescription='Agent >>'.$city_name. '>>'.$meta_setting->meta_description;
		$metaKeyword='Agent >>'.$city_name. '>>'.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/travelagent/agent_search',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
		
	}
	
	
	public function directory($select_country='')
	{
		
		$data = array();
		
		
			///////====track visitors======
		
		$ip_country='USA';
		
		if($select_country!='')
		{
			if($select_country=='USA'  || $select_country=='usa'  || $select_country=='america')
			{
				$current_loc='USA';
				$ip_country='USA';
			}
			elseif($select_country=='IND' || $select_country=='ind' || $select_country=='india')
			{
				$current_loc='INDIA';
				$ip_country='INDIA';
			}
			else
			{
				$current_loc=$ip_country;
			}		
		}
		else
		{
		
			
	
			$ip_region='';
			$ip_city='';
			
			$current_loc='';
			
			$ip_data=getip2location(getRealIP());
			
			
			
			if($ip_data)
			{
				
				if(isset($ip_data->CityName))
				{
					if($ip_data->CityName!='') {  
						if(strtolower($ip_data->CityName)!='unknown'){	
								
								$ip_city	=$ip_data->CityName;   
								//$current_loc .= $ip_city; 
					} }
				}
				
				if(isset($ip_data->RegionName))
				{
					if($ip_data->RegionName!='') { 	
						if(strtolower($ip_data->RegionName)!='unknown'){
							
						$ip_region	=$ip_data->RegionName;   
						//$current_loc .= ','.$ip_region; 
						
					} }
				}
				
				if(isset($ip_data->CountryCode3))
				{
					if($ip_data->CountryCode3!='') { 
						if(strtolower($ip_data->CountryCode3)!='unknown'){
						
							$ip_country	=$ip_data->CountryCode3; 
							
							
					} }
				}
				
				
				
						
			}
			
			
			if($current_loc!='')
			{ 	$current_loc .= ','.$ip_country;
			} else {
				$current_loc .= $ip_country;
			}
	
		}
			///////====track visitors======
			
			
			
			
		 $data['current_loc']=$current_loc;
		 $data['ip_country']=$ip_country;
		
	
		$data['countrylist'] = country_list();
		
		$data['keyword']='';
		$data['offset']='';
		
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		
		$pageTitle='Agent Directory - '.$meta_setting->title;
		$metaDescription='Agent Directory - '.$meta_setting->meta_description;
		$metaKeyword='Agent Directory - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/travelagent/agent_directory',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	}
	
	function state($state_name='',$offset='')
	{
		$chk_state = get_state_id_from_statename(urldecode($state_name));
		
		$data = array();
		$data['get_city_list']='';
		$data['get_metro_city_list']='';
		$data['keyword']='';
		$data['offset']='';
		
		if($chk_state)
		{
			$get_city_list = get_city_from_state($chk_state->state_id);
			$get_metro_city_list = get_metro_city_list($chk_state->state_id);
			$data['get_metro_city_list']=$get_metro_city_list;
			$data['get_city_list']=$get_city_list;
		}
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
	
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$data['state_name'] = $state_name;
		
		$pageTitle='Agent Directory - '.$meta_setting->title;
		$metaDescription='Agent Directory - '.$meta_setting->meta_description;
		$metaKeyword='Agent Directory - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/travelagent/city_directory',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
		
	}	
	
	
	
	
	function became($msg='')
	{
	
		if(!check_user_authentication()) {  redirect('sign_up'); } 


		$agent_profile = check_user_applied_for_agent(get_authenticateUserID());
		if($agent_profile)
		{
			redirect('travelagent/profile/');
		}
		
		
		$data['msg']=$msg;
		$data["error"]='';
		$data['select']='agent_profile';
		$data['site_setting']=site_setting();
		
		$user_info=get_user_profile_by_id(get_authenticateUserID());		
		$data['user_info']=$user_info;

		$data['member_association'] = member_association();

		
		$this->form_validation->set_rules('agency_name', 'Agency Name', 'trim|required|alpha_dot_space');
		$this->form_validation->set_rules('contact_person_name', 'Contact Person Name', 'trim|required|alpha_space');
		$this->form_validation->set_rules('agency_location', 'Agency Location', 'trim|required');
		$this->form_validation->set_rules('agency_phone', 'Agency Phone', 'required|phone_valid|min_length[10]|max_length[18]');
		$this->form_validation->set_rules('agency_customer_support_no', 'Customer Support Number', 'trim|phone_valid|min_length[10]|max_length[18]');
		
		$this->form_validation->set_rules('agency_website', 'Agency Website', 'trim|valid_url');
		

				
		if($this->form_validation->run() == FALSE)
		{
			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}
			else
			{
			$data["error"] = "";
			}	
				
			if($_POST) {
				
				$data['user_id']=$this->input->post('user_id');
			
				$data['agency_name']=$this->input->post('agency_name');
				$data['agency_location']=$this->input->post('agency_location');
				$data['contact_person_name']=$this->input->post('contact_person_name');
			
				$data['agency_contact_email']=$this->input->post('agency_contact_email');
				$data['agency_phone']=$this->input->post('agency_phone');
				$data['agency_customer_support_no']=$this->input->post('agency_customer_support_no');
				$data['agency_website']=$this->input->post('agency_website');
				$data['agent_member_of_association']=$this->input->post('agent_member_of_association');
				
			} else {
			
				$data['user_id']=$user_info->user_id;
			
				$data['agency_name']='';
				$data['agency_location']='';
				$data['contact_person_name']='';
				
				$data['agency_contact_email']='';
				$data['agency_phone']='';
				$data['agency_customer_support_no']='';
				$data['agency_website']='';
				$data['agent_member_of_association'] = '';
				
			}
				
		} else {

			$became_travelagent = $this->travelagent_model->became_travelagent();
			$msg="create";
			redirect('travelagent/profile/'.$msg);
		}	

		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		$data['theme']=$theme;
		
		$meta_setting=meta_setting();


		$meta_setting=meta_setting();
		$pageTitle=ucfirst($user_info->first_name).' '.ucfirst(substr($user_info->last_name,0,1)).' - Became TravelAgent '.$meta_setting->title;
		$metaDescription=ucfirst($user_info->first_name).' '.ucfirst(substr($user_info->last_name,0,1)).' - Became  TravelAgent '.$meta_setting->meta_description;
		$metaKeyword=ucfirst($user_info->first_name).' '.ucfirst(substr($user_info->last_name,0,1)).' - Became TravelAgent '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/travelagent/became_travelagent',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	}
	
	
	
	function profile($msg='')
	{
		if(!check_user_authentication()) {  redirect('sign_up'); } 
		  
		$agent_profile = check_user_applied_for_agent(get_authenticateUserID());
		if(!$agent_profile)
		{
			redirect('travelagent/became');
		}
		
		$data['agent_profile']=$agent_profile;
		$data['msg']=$msg;
		$data["error"]='';
		$data['select']='agent_profile';
		$data['site_setting']=site_setting();
		
		$data['member_association'] = member_association();

	
		$this->form_validation->set_rules('agency_name', 'Agency Name', 'trim|required|alpha_dot_space');
		$this->form_validation->set_rules('contact_person_name', 'Contact Person Name', 'trim|required|alpha_space');
		$this->form_validation->set_rules('agency_location', 'Agency Location', 'trim|required');
		$this->form_validation->set_rules('agency_phone', 'Agency Phone', 'required|phone_valid|min_length[10]|max_length[18]');
		$this->form_validation->set_rules('agency_customer_support_no', 'Customer Support Number', 'trim|phone_valid|min_length[10]|max_length[18]');
		
		$this->form_validation->set_rules('agency_website', 'Agency Website', 'trim|valid_url');
				
		if($this->form_validation->run() == FALSE)
		{
			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}
			else
			{
			$data["error"] = "";
			}	
				
			if($_POST) {
				
				$data['agent_id']=$this->input->post('agent_id');
				$data['user_id']=$this->input->post('user_id');
				
				$data['agency_name']=$this->input->post('agency_name');
				$data['agency_location']=$this->input->post('agency_location');
				$data['contact_person_name']=$this->input->post('contact_person_name');
			
				$data['agency_contact_email']=$this->input->post('agency_contact_email');
				$data['agency_phone']=$this->input->post('agency_phone');
				$data['agency_customer_support_no']=$this->input->post('agency_customer_support_no');
				$data['agency_website']=$this->input->post('agency_website');
				$data['agent_member_of_association']=$this->input->post('agent_member_of_association');
			} else {
			
				$data['agent_id']=$agent_profile->agent_id;
				$data['user_id']=$agent_profile->user_id;
				
				$data['agency_name']=$agent_profile->agency_name;
				$data['agency_location']=$agent_profile->agency_location;
				$data['contact_person_name']=$agent_profile->contact_person_name;
			
				$data['agency_contact_email']=$agent_profile->agency_contact_email;
				$data['agency_phone']=$agent_profile->agency_phone;
				$data['agency_customer_support_no']=$agent_profile->agency_customer_support_no;
				$data['agency_website']=$agent_profile->agency_website;
				
				
				$ex_agent_member_of_association=array();
				if($agent_profile->agent_member_of_association!='')
				{
					if(substr_count($agent_profile->agent_member_of_association,',')>0)
					{
						$ex_agent_member_of_association=explode(',',$agent_profile->agent_member_of_association);					
					}
					else
					{
						$ex_agent_member_of_association[]=$agent_profile->agent_member_of_association;
					}
				}
				$data['agent_member_of_association'] = $ex_agent_member_of_association;
				
			}
				
		} else {

			$edit_agent_profile = $this->travelagent_model->edit_agent_profile();
			$msg="update";
			redirect('travelagent/profile/'.$msg);
		}	

		
		$data['agent_document'] = get_agent_document(get_authenticateUserID());
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		$data['theme']=$theme;
		
		$meta_setting=meta_setting();


		$meta_setting=meta_setting();
		$pageTitle=ucfirst($agent_profile->first_name).' '.ucfirst(substr($agent_profile->last_name,0,1)).' - TravelAgent Profile '.$meta_setting->title;
		$metaDescription=ucfirst($agent_profile->first_name).' '.ucfirst(substr($agent_profile->last_name,0,1)).' - TravelAgent Profilet '.$meta_setting->meta_description;
		$metaKeyword=ucfirst($agent_profile->first_name).' '.ucfirst(substr($agent_profile->last_name,0,1)).' - TravelAgent Profile '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/travelagent/agent_profile',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	}	
	
}

?>