<?php
/*********************************************************************************
 * This the taskrabbitclone.com  by Rockers Technology. is paid software. It is released under the terms of 
 * the following BSD License.
 * 
 *  Rockers Technologies (Head Office)
 *    5038,Berthpage Dr
 *    suwanee, GA. Zip Code : 30024
    
 *    E-mail Address : nishu@rockersinfo.com
 * 
 * Copyright � 2012-2020 by Rockers Technology , INC a domestic profit corporation has been duly incorporated under
the laws of the state of georiga , USA. www.rockersinfo.com
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this 
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this 
 *   list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.

 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Myoffer extends IWEB_Controller 
{
	
	/*
	Function name :Myoffer()
	Description :Its Default Constuctor which called when user object initialzie.its load necesary models
	*/
	function Myoffer()
	{
		parent::__construct();	
		$this->load->model('myoffer_model');	
	}
	
	
	/*
	Function name :index()
	Parameter :none
	Return : none
	Use : redirect to user dashboard
	Description : none
	*/
	
	public function index()
	{
		redirect('myoffer/all');
	}

	
	function all($offset=0)
	{	
		if(!check_user_authentication()) {  redirect('login'); } 
		
		$data['select'] = 'myoffer';
		$data['selecttab'] = 'all';
		
		$this->load->library('pagination');

		$limit = '10';
		
		$config['base_url'] = site_url('myoffer/all/');
		$config['total_rows'] = $this->myoffer_model->get_all_total_trips();
		$config['per_page'] = $limit;
				
		$this->pagination->initialize($config);		
		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->myoffer_model->get_all_trips_list($limit,$offset);
		$data['total_rows']=$config['total_rows'];
		
		$data['site_setting'] = site_setting();

		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		$data['theme']=$theme;
		
		$meta_setting=meta_setting();
		
		$pageTitle='All My Offer on Trips - '.$meta_setting->title;
		$metaDescription='All My Offer on Trips - '.$meta_setting->meta_description;
		$metaKeyword='All My Offer on Trips - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/myoffer/myoffer_all',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
		
	}
	
	
	function open($offset=0)
	{	
		if(!check_user_authentication()) {  redirect('login'); } 
		
		$data['select'] = 'myoffer';
		$data['selecttab'] = 'open';
		
		$this->load->library('pagination');

		$limit = '10';
		
		$config['base_url'] = site_url('myoffer/open/');
		$config['total_rows'] = $this->myoffer_model->get_open_total_trips();
		$config['per_page'] = $limit;
				
		$this->pagination->initialize($config);		
		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->myoffer_model->get_open_trips_list($limit,$offset);
		$data['total_rows']=$config['total_rows'];
		
		$data['site_setting'] = site_setting();

		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		$data['theme']=$theme;
		
		$meta_setting=meta_setting();
		
		$pageTitle='My Open Offers on Trips - '.$meta_setting->title;
		$metaDescription='My Open Offers on Trips - '.$meta_setting->meta_description;
		$metaKeyword='My Open Offers on Trips - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/myoffer/myoffer_open',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
		
	}
	
	
	function assigned($offset=0)
	{	
		if(!check_user_authentication()) {  redirect('login'); } 
		
		$data['select'] = 'myoffer';
		$data['selecttab'] = 'assigned';
		
		$this->load->library('pagination');

		$limit = '10';
		
		$config['base_url'] = site_url('myoffer/assigned/');
		$config['total_rows'] = $this->myoffer_model->get_assigned_total_trips();
		$config['per_page'] = $limit;
				
		$this->pagination->initialize($config);		
		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->myoffer_model->get_assigned_trips_list($limit,$offset);
		$data['total_rows']=$config['total_rows'];
		
		$data['site_setting'] = site_setting();

		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		$data['theme']=$theme;
		
		$meta_setting=meta_setting();
		
		$pageTitle='My Assigned Offers on Trips - '.$meta_setting->title;
		$metaDescription='My Assigned Offers on Trips - '.$meta_setting->meta_description;
		$metaKeyword='My Assigned Offers on Trips - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/myoffer/myoffer_assigned',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
		
	}
	
	
	function completed($offset=0)
	{	
		if(!check_user_authentication()) {  redirect('login'); } 
		
		$data['select'] = 'myoffer';
		$data['selecttab'] = 'completed';
		
		$this->load->library('pagination');

		$limit = '10';
		
		$config['base_url'] = site_url('myoffer/completed/');
		$config['total_rows'] = $this->myoffer_model->get_completed_total_trips();
		$config['per_page'] = $limit;
				
		$this->pagination->initialize($config);		
		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->myoffer_model->get_completed_trips_list($limit,$offset);
		$data['total_rows']=$config['total_rows'];
		
		$data['site_setting'] = site_setting();

		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		$data['theme']=$theme;
		
		$meta_setting=meta_setting();
		
		$pageTitle='My Completed Offers on Trips - '.$meta_setting->title;
		$metaDescription='My Completed Offers on Trips - '.$meta_setting->meta_description;
		$metaKeyword='My Completed Offers on Trips - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/myoffer/myoffer_completed',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
		
	}
	
	
	function close($offset=0)
	{	
		if(!check_user_authentication()) {  redirect('login'); } 
		
		$data['select'] = 'myoffer';
		$data['selecttab'] = 'close';
		
		$this->load->library('pagination');

		$limit = '10';
		
		$config['base_url'] = site_url('myoffer/close/');
		$config['total_rows'] = $this->myoffer_model->get_close_total_trips();
		$config['per_page'] = $limit;
				
		$this->pagination->initialize($config);		
		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->myoffer_model->get_close_trips_list($limit,$offset);
		$data['total_rows']=$config['total_rows'];
		
		$data['site_setting'] = site_setting();

		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		$data['theme']=$theme;
		
		$meta_setting=meta_setting();
		
		$pageTitle='My Close Trips - '.$meta_setting->title;
		$metaDescription='My Close Trips - '.$meta_setting->meta_description;
		$metaKeyword='My Close Trips - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/myoffer/myoffer_close',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
		
	}



}

?>