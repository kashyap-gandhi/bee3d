<?php
/*********************************************************************************
 * This the taskrabbitclone.com  by Rockers Technology. is paid software. It is released under the terms of 
 * the following BSD License.
 * 
 *  Rockers Technologies (Head Office)
 *    5038,Berthpage Dr
 *    suwanee, GA. Zip Code : 30024
    
 *    E-mail Address : nishu@rockersinfo.com
 * 
 * Copyright © 2012-2020 by Rockers Technology , INC a domestic profit corporation has been duly incorporated under
the laws of the state of georiga , USA. www.rockersinfo.com
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this 
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this 
 *   list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.

 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class Conversation extends IWEB_Controller {
	
	/*
	Function name :Conversation()
	Description :Its Default Constuctor which called when home object initialzie.its load necesary models
	*/
	
	function Conversation()
	{
		parent::__construct();	
		$this->load->model('conversation_model');	
		$this->load->model('trip_model');	
		$this->load->model('travelagent_model');	
		$this->load->helper('cookie');
			
		
	}
	
	function offer_user_conversation($trip_offer_id,$msg='')
	{
	
		if(!check_user_authentication()) {  redirect('login'); } 
		
		if($trip_offer_id=='' || $trip_offer_id==0)
	    {
			redirect('home');
	    }
		
		
		$trip_offer_info = $this->conversation_model->get_offer_detail($trip_offer_id);
		
		if(!$trip_offer_info)
		{
			redirect('home');
		}
		
		$data['trip_offer_info']=$trip_offer_info;
	
		
		$data = array();
		
		$trip_id = $trip_offer_info->trip_id;
		$offer_user_id = $trip_offer_info->user_id;
		
		// trip details  
		$trip_info = get_one_trip($trip_id);
		
		if(!$trip_info)
		{
			redirect('home');
		}
		$data['trip_info']=$trip_info;
		$data['trip_id']=$trip_id;
		
		// user details  
		$user_info = get_user_profile_by_id($trip_info->user_id);
		if(!$user_info)
		{
			redirect('home');
		}
		$data['user_info']=$user_info;	
		$trip_user_id = $user_info->user_id;
		$data['trip_user_id']=$trip_user_id;
		
		
		// agent details  
		$offer_user_info = get_user_profile_by_id($offer_user_id);
		if(!$offer_user_info)
		{
			redirect('home');
		}
		$data['offer_user_info']=$offer_user_info;	
		$data['offer_user_id']=$offer_user_id;
		
		$agent_bid = $this->conversation_model->agent_offer_details($trip_id,get_authenticateUserID());
		$data['agent_bid'] = $agent_bid;
		
		
		if(!$agent_bid)
		{
			redirect('home');
		}
		
		$data['offer_trip_id'] = $trip_offer_id;
		$data['msg']=$msg;
		$data['trip_conversation'] = $this->conversation_model->get_all_conversation($trip_id,$trip_user_id,$offer_user_id);
		
		$this->form_validation->set_rules('conversation_message', 'Conversation Message', 'required');
		
		if($this->form_validation->run() == FALSE ){
			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
				
			}else{
				$data["error"] = "";
			}
			
			$data['conversation_message'] = $this->input->post('conversation_message');
			
			
		} else {
		
			$data["error"] = "";
			$apply=$this->conversation_model->user_new_conversation();
			$data['conversation_message'] = $this->input->post('conversation_message');
			redirect('conversation/offer/'.$trip_offer_id.'/success');

		}
		
		
	    
		
		
		$trip_from = $trip_info->trip_from_place;
		$trip_to = $trip_info->trip_to_place;

		$data['trip_from']=$trip_from;
		$data['trip_to']=$trip_to;

		$data['sow_bid_frm']=1;
		
		$chk_bid=0;
		
		
		if(get_authenticateUserID() > 0 && $data['agent_bid'] != 0) {
		
			$agent_bid=$data['agent_bid'];
			if($agent_bid) { $chk_bid=1;}
		
			$data['bid_now']=$chk_bid;
			$data['proposal_desc'] = $agent_bid[0]->offer_content;
			$data['offer_build_package']=$agent_bid[0]->offer_build_package;
			$data['offer_amount']=$agent_bid[0]->offer_amount;
			$data['offer_days']=$agent_bid[0]->offer_days;
			$data['offer_nights']=$agent_bid[0]->offer_nights;
			$data['final_offer_amount']=$agent_bid[0]->final_offer_amount;
		    //$data['agent_offer']=$agent_bid[0]->offer_nights;
			$data['trip_offer_id']=$agent_bid[0]->trip_offer_id;
			$data['admin_fees']=$agent_bid[0]->admin_fee;
			$data['final_offer_agent']=$agent_bid[0]->final_offer_amount;
		}
		else
		{
			$data['agent_bid'] = '';
			$data['bid_now']=$chk_bid;
			$data['proposal_desc'] = '';
			$data['offer_build_package']='';
			$data['offer_amount']='';
			$data['offer_days']='';
			$data['offer_nights']='';
			$data['final_offer_amount']='';
			//$data['agent_offer']='';
			$data['trip_offer_id']='';
			$data['admin_fees']='';
			$data['final_offer_agent']='';
		}
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$site_setting = site_setting();
		$data['site_setting']=$site_setting;
		
		
		
		$pageTitle='Conversation on Trip from-'.$trip_from.'-to-'.$trip_to.'-'.$meta_setting->title;
		$metaDescription='Conversation on Trip from -'.$trip_from.'-to-'.$trip_to.'-'.$meta_setting->meta_description;
		$metaKeyword='Conversation on Trip from -'.$trip_from.'-to-'.$trip_to.'-'.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/conversation/offer_user_conversation',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	}
	
	function trip_user_conversation($trip_offer_id,$msg='')
	{

		if(!check_user_authentication()) {  redirect('login'); } 
		
		if($trip_offer_id=='' || $trip_offer_id==0)
	    {
			redirect('home');
	    }
		
		
		$trip_offer_info = $this->conversation_model->get_offer_detail($trip_offer_id);
		
		if(!$trip_offer_info)
		{
			redirect('home');
		}
		
		$data['trip_offer_info']=$trip_offer_info;
		
		
		
		$data = array();
		
		// trip details  
		$trip_id = $trip_offer_info->trip_id;
		$offer_user_id = $trip_offer_info->user_id;
		
		$trip_info = get_one_trip($trip_id);
		
		if(!$trip_info)
		{
			redirect('home');
		}
		$data['trip_info']=$trip_info;
		$data['trip_id']=$trip_id;
		
		// user details  
		$user_info = get_user_profile_by_id($trip_info->user_id);
		if(!$user_info)
		{
			redirect('home');
		}
		$data['user_info']=$user_info;	
		$trip_user_id = $user_info->user_id;
		$data['trip_user_id']=$trip_user_id;
		
		
		// agent details  
		$offer_user_info = get_user_profile_by_id($offer_user_id);
		if(!$offer_user_info)
		{
			redirect('home');
		}
		$data['offer_user_info']=$offer_user_info;	
		$data['offer_user_id']=$offer_user_id;
		
		$agent_bid = $this->conversation_model->agent_offer_details($trip_id,$offer_user_id);
		$data['agent_bid'] = $agent_bid;
		
		
		if($trip_info->user_id != get_authenticateUserID())
		{
			redirect('home');
		}
		
		
		$data['offer_trip_id'] = $trip_offer_id;
	    $data['msg']=$msg;
		$data['trip_conversation'] = $this->conversation_model->get_all_conversation($trip_id,$trip_user_id,$offer_user_id);
		
		
		
		
		$this->form_validation->set_rules('conversation_message', 'Conversation Message', 'required');
		
		if($this->form_validation->run() == FALSE ){
			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
				
			}else{
				$data["error"] = "";
			}
			
			$data['conversation_message'] = $this->input->post('conversation_message');
			
			
		} else {
		
			$data["error"] = "";
			$apply=$this->conversation_model->user_new_conversation();
			$data['conversation_message'] = $this->input->post('conversation_message');
			redirect('conversation/user/'.$trip_offer_id.'/success');

		}
		
		
		$trip_from = $trip_info->trip_from_place;
		$trip_to = $trip_info->trip_to_place;
		$data['trip_from']=$trip_from;
		$data['trip_to']=$trip_to;
		
		$data['sow_bid_frm']=0;
		$data['agent_bid'] = '';
		$data['bid_now']=0;
		$data['proposal_desc'] = '';
		$data['offer_build_package']='';
		$data['offer_amount']='';
		$data['offer_days']='';
		$data['offer_nights']='';
		$data['final_offer_amount']='';
		//$data['agent_offer']='';
		$data['trip_offer_id']='';
		$data['admin_fees']='';
		$data['final_offer_agent']='';
			
			
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		$site_setting = site_setting();
		$data['site_setting']=$site_setting;
		
		
		
		$pageTitle='Conversation on Trip from-'.$trip_from.'-to-'.$trip_to.'-'.$meta_setting->title;
		$metaDescription='Conversation on Trip from -'.$trip_from.'-to-'.$trip_to.'-'.$meta_setting->meta_description;
		$metaKeyword='Conversation on Trip from -'.$trip_from.'-to-'.$trip_to.'-'.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/conversation/trip_user_conversation',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	}
	
	function accept_offer($trip_offer_id){
	
		if(!check_user_authentication()) {  redirect('login'); } 
		
		// trip details 
		$trip_offer_info = $this->conversation_model->get_offer_detail($trip_offer_id);
		
		if(!$trip_offer_info)
		{
			redirect('home');
		}
	
		
		if($trip_offer_info->trip_activity_status == 0 && strtotime($trip_offer_info->trip_confirm_date)>=strtotime(date('Y-m-d H:i:s'))) {
			$assign = $this->conversation_model->accept_offer($trip_offer_info->user_id,$trip_offer_info->trip_id);
			redirect('conversation/user/'.$trip_offer_id.'/accept');
		}
		redirect('conversation/user/'.$trip_offer_id);
		
	}
	
	function complete($trip_id)
	{        
		if(!check_user_authentication()) {  redirect('sign_up'); } 
	 
	   
		if($trip_id=='' || $trip_id==0)
	   	{
			redirect('home');
	   	}
		
		$trip_info = get_one_trip($trip_id);
		if(!$trip_info)
		{
			redirect('home');
		}
		
		
		if($trip_info->trip_activity_status == 2)
		{
			redirect('from/'.$trip_info->trip_from_place.'/to/'.$trip_info->trip_to_place.'/'.$trip_info->trip_id);
		}
		$data['trip_info']=$trip_info;
		
		$user_info = get_user_profile_by_id($trip_info->user_id);
		$data['user_info']=$user_info;	
		$data['trip_user_id']=$user_info->user_id;	

		$offer_user_info = get_user_profile_by_id($trip_info->trip_agent_assign_id);
		$data['offer_user_info']=$offer_user_info;	
		$data['trip_offer_user_id']=$offer_user_info->user_id;
		
	   
	   
	   
		$site_setting=site_setting();
	    $theme = getThemeName();
	    $this->template->set_master_template($theme .'/template.php');
	   
	    $this->form_validation->set_rules('user_review', 'User Review', 'required');
	  
	   
	    if($this->form_validation->run() == FALSE){
	
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			
			$data['user_review'] = $this->input->post('user_review');
			$data['trip_id'] = $this->input->post('trip_id');
			$data['user_rating'] = $this->input->post('user_rating');

		} else {
	
			$apply=$this->conversation_model->complete_trip();
			redirect('from/'.$trip_info->trip_from_place.'/to/'.$trip_info->trip_to_place.'/'.$trip_info->trip_id);

		}


	   $data['trip_id']=$trip_id;
	  
	   $data['site_setting']=site_setting();
	   $data['theme']=$theme;
	   $meta_setting=meta_setting(); 
	   
	   $trip_from = $trip_info->trip_from_place;
	   $trip_to = $trip_info->trip_to_place;
		 
	   $pageTitle='Complete Trip from-'.$trip_from.'-to-'.$trip_to.'-'.$meta_setting->title;
	   $metaDescription='Complete Trip from -'.$trip_from.'-to-'.$trip_to.'-'.$meta_setting->meta_description;
	   $metaKeyword='Complete Trip from -'.$trip_from.'-to-'.$trip_to.'-'.$meta_setting->meta_keyword;
		
		
	 
	   $this->template->write('pageTitle',$pageTitle,TRUE);
	   $this->template->write('metaDescription',$metaDescription,TRUE);
	   $this->template->write('metaKeyword',$metaKeyword,TRUE);
	   $this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
	   $this->template->write_view('content_center',$theme .'/layout/conversation/complete_trip',$data,TRUE);
	   $this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
	   $this->template->render();	       
               
   }
	

	
	
}

?>