<?php
/*********************************************************************************
 * This the taskrabbitclone.com  by Rockers Technology. is paid software. It is released under the terms of 
 * the following BSD License.
 * 
 *  Rockers Technologies (Head Office)
 *    5038,Berthpage Dr
 *    suwanee, GA. Zip Code : 30024
    
 *    E-mail Address : nishu@rockersinfo.com
 * 
 * Copyright © 2012-2020 by Rockers Technology , INC a domestic profit corporation has been duly incorporated under
the laws of the state of georiga , USA. www.rockersinfo.com
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this 
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this 
 *   list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.

 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class Dashboard extends IWEB_Controller {
	
	/*
	Function name :Dashboard()
	Description :Its Default Constuctor which called when home object initialzie.its load necesary models
	*/
	
	function Dashboard()
	{
		parent::__construct();	
		$this->load->model('home_model');	
		$this->load->model('user_model');	
		$this->load->model('trip_model');	
		$this->load->model('mytrip_model');		
		$this->load->model('dashboard_model');		
	}
	
	function index()
	{
			
		if(!check_user_authentication()) {  redirect('login'); } 
		  
		$user_info=get_user_profile_by_id(get_authenticateUserID());
		$data['user_info']=$user_info;

		$draft_trip_limit = 5;
		$data['draft_trips'] = $this->dashboard_model->get_all_draft_trip(get_authenticateUserID(),$draft_trip_limit);
		
		$last_trip_limit = 5;
		$data['last_trips'] = $this->dashboard_model->get_last_trip(get_authenticateUserID(),$last_trip_limit);
		
		$today_trip_limit = 5;
		$data['today_trips'] = get_today_trips(get_authenticateUserID(),$today_trip_limit);
		
		$yesterday_trip_limit = 5;
		$data['yesterday_trips'] = get_yesterday_trips(get_authenticateUserID(),$yesterday_trip_limit);
		
		$top_agent_limit = 5;
		$data['top_agents'] = get_top_agent($top_agent_limit);
		
		$data['site_setting']=site_setting();
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		$meta_setting=meta_setting();
		
		$pageTitle='Dashboard - '.$meta_setting->title;
		$metaDescription='Dashboard - '.$meta_setting->meta_description;
		$metaKeyword='Dashboard - '.$meta_setting->meta_keyword;
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('content_center',$theme .'/layout/dashboard/dashboard',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
		
	
	}
}
?>