<?php

class Design extends IWEB_Controller {
    /*
      Function name :Dashboard()
      Description :Its Default Constuctor which called when home object initialzie.its load necesary models
     */
    public $cnt=0;
    
    function Design() {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->model('user_model');
        $this->load->model('design_model');
        $this->load->model('package_model');
        $this->load->model('wallet_model');
        $this->load->library('zip');
        
       
    }
    
    
    
     function landing()
	{
		
			
		$data=array();
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['latest_printer']=$this->home_model->latest_printer('12');
		$data['latest_design']=$this->home_model->latest_design('12');
                
                //$data['is_home']=1;
		
		
		$meta_setting=meta_setting();
		$site_setting=site_setting();
		
		$pageTitle=$meta_setting->title;
		$metaDescription=$meta_setting->meta_description;
		$metaKeyword=$meta_setting->meta_keyword;
		 
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		
		$this->template->write_view('header',$theme .'/layout/common/header_home',$data,TRUE);
		$this->template->write_view('campaign_box',$theme .'/layout/design/design_content',$data,TRUE);		
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
		
	}

    function index($slug='',$msg='') {

        $chkdesign = check_design_exist($slug);
        if (!$chkdesign) {
            redirect('home/404');
        } else {

            $design_id = $chkdesign->design_id;


            $design_detail = $this->design_model->check_exist_design($design_id, 0);
            
          
            if (empty($design_detail)) {
               
                redirect('home/404');
            }

            $data = array();

            


            $data['msg'] = $msg;


            $data['site_setting'] = site_setting();
            $data['active_menu'] = "3ddesign";
            $theme = getThemeName();
            $this->template->set_master_template($theme . '/template.php');

            $data['theme'] = $theme;
            $data['category'] = get_parent_category();
            $data['alldesign'] = get_alldesign();

            //Set design view count
            $design_view = $this->design_model->set_design_view($design_id);

            $one_design = $this->design_model->get_one_design($design_id);

            $data['design_tree'] = '';

            if ($one_design->design_is_modified == 1 && $one_design->parent_design_id > 0) {

                $data['design_tree'] = $this->design_parent_tree($design_id);
            }


            $get_one_design_tag = $this->design_model->get_one_design_tag($design_id);
            $data['design_images'] = $this->design_model->get_one_design_images($design_id);

            $data['user_design'] = $this->design_model->get_user_design($one_design->user_id, 4, $design_id);

            $data['user_design_galley'] = $this->design_model->get_user_design_gallery(20,$one_design->user_id, $design_id);



            $data['popular_design'] = $this->design_model->get_popular_design(4, $design_id);
            
            $data['design_by'] = '';
            $data['design_by_profile_name'] = '';
            $desing_user_info = get_user_profile_by_id($one_design->user_id);
            if(!empty($desing_user_info)){
             $data['design_by'] = ucwords($desing_user_info->full_name);
             $data['design_by_profile_name'] = ucwords($desing_user_info->profile_name);
             
            }

            /* if($get_one_design_tag)
              {
              $design_tag ='';
              foreach($get_one_design_tag as $gt)
              {
              $design_tag .= $gt->tag_name.',';
              }

              } */
            $data['get_one_design_tag'] = $get_one_design_tag;
            $data['main_image'] = $this->design_model->get_one_main_design_image($design_id);
            
            
            
            $design_3d_file=$this->design_model->get_design_files($design_id);
            
            $file_3d_name='';
            
            if($design_3d_file){
                if($design_3d_file->attachment_file!=''){
                    if(file_exists(base_path() . 'upload/design_orig/' . $design_3d_file->attachment_file)){
                       $file_3d_name= $design_3d_file->attachment_file;
                    }
                }
            }
            
            
            $data['main_3d_image'] = $file_3d_name;
            
            
            $data['user_id'] = $one_design->user_id;
            $data['design_id'] = $one_design->design_id;
            //$data['design_tag'] = substr($design_tag,0,-1);
            $data['design_title'] = $one_design->design_title;
            
            $data['design_slug'] = $one_design->design_slug;
            
            $data['design_content'] = $one_design->design_content;
            $data['category_id'] = $one_design->category_id;
            $data['design_point'] = $one_design->design_point;
            $data['design_ref_id'] = $one_design->design_ref_id;
            
            
            
            
            $data['design_is_modified'] = $one_design->design_is_modified;
            $data['design_meta_keyword'] = $one_design->design_meta_keyword;
            $data['design_meta_description'] = $one_design->design_meta_description;
            $data['parent_design_id'] = $one_design->parent_design_id;
            $data['design_date'] = $one_design->design_date;
            $data['design_view_count'] = $one_design->design_view_count;
            $data['design_like_count'] = $one_design->design_like_count;
            $data['design_gallery_type'] = $one_design->design_gallery_type;
            $data['design_counter_id'] = $one_design->design_counter_id;

            $data['design_total_rating'] = $one_design->design_total_rating;
            $data['design_total_review'] = $one_design->design_total_review;
            
            $meta_setting = meta_setting();

            $pageTitle = $one_design->design_title . '-' . $meta_setting->title;
            $metaDescription = $one_design->design_meta_description . '-' . $meta_setting->meta_description;
            $metaKeyword = $one_design->design_meta_keyword . '-' . $meta_setting->meta_keyword;

            $this->template->write('pageTitle', $pageTitle, TRUE);
            $this->template->write('metaDescription', $metaDescription, TRUE);
            $this->template->write('metaKeyword', $metaKeyword, TRUE);
            $this->template->write_view('header', $theme . '/layout/common/header_home', $data, TRUE);
            $this->template->write_view('content_center', $theme . '/layout/design/view_design', $data, TRUE);
            $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
            $this->template->render();
        }
        //if(!check_user_authentication()) {  redirect('login'); } 
        //redirect('design/all'); 
    }

    function all($limit='20', $offset=0, $msg='') {
        if (!check_user_authentication()) {
            redirect('login');
        }
        $data = array();
        $data['site_setting'] = site_setting();
        $data['active_menu'] = "3ddesign";
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        $data['theme'] = $theme;
        $meta_setting = meta_setting();
        $data['msg'] = $msg;

        $this->load->library('pagination');
        $config['uri_segment'] = '4';
        $config['base_url'] = site_url('design/all/' . $limit . '/');
        $config['total_rows'] = $this->design_model->get_toala_user_design_count(get_authenticateUserID());

        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        $data['page_link'] = $this->pagination->create_links();
        $data['result'] = $this->design_model->get_design_result(get_authenticateUserID(), $limit, $offset);

        $data['msg'] = $msg;

        $data['offset'] = $offset;
        $data['error'] = '';
        if ($this->input->post('limit') != '') {
            $data['limit'] = $this->input->post('limit');
        } else {
            $data['limit'] = $limit;
        }



        $data['offset'] = $offset;
        $data['error'] = '';
        if ($this->input->post('limit') != '') {
            $data['limit'] = $this->input->post('limit');
        } else {
            $data['limit'] = $limit;
        }


        $pageTitle = '3D-Design Hive- ' . $meta_setting->title;
        $metaDescription = '3D-Design Hive - ' . $meta_setting->meta_description;
        $metaKeyword = '3D-Design Hive - ' . $meta_setting->meta_keyword;

        $this->template->write('pageTitle', $pageTitle, TRUE);
        $this->template->write('metaDescription', $metaDescription, TRUE);
        $this->template->write('metaKeyword', $metaKeyword, TRUE);
        $this->template->write_view('header', $theme . '/layout/common/header_theme', $data, TRUE);
        $this->template->write_view('content_center', $theme . '/layout/design/list_3ddesign_hive', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer_theme', $data, TRUE);
        $this->template->render();
    }
    
    
    function post_rating(){
        if ('XMLHttpRequest' == @$_SERVER['HTTP_X_REQUESTED_WITH']) {


            $data['error'] = '';
            $data["msg"] = '';



            if (get_authenticateUserID() > 0) {
                if ($_POST) {
                    
                    $design_id = (int) $this->input->post('design_id');

                    $design_detail = $this->design_model->check_exist_design($design_id, 0);

                    if (empty($design_detail)) {
                        $data['error'] = '3D Design is not Found.';
                    } else {

                        $this->form_validation->set_rules('rating', 'Rating', 'trim|required|is_natural');
                        $this->form_validation->set_rules('design_id', 'Design', 'trim|required|is_natural');
                        

                        if ($this->form_validation->run() == FALSE) {
                            if (validation_errors()) {
                                $data["error"] = validation_errors();
                            } else {
                                $data["error"] = "";
                            }


                            $data["rating"] = $this->input->post('rating');
                            $data["design_id"] = $this->input->post('design_id');
                        } else {


                            $res = $this->design_model->place_rating();
                            
                            if ($res == 1) {
                                $data['msg'] = 'successr';
                            } else {
                                $data['error'] = 'Faile to add Rating. Please try again.';
                            }
                        }
                    }
                }
            } else {
                $data['error'] = 'Please take a login to write a review.';
            }
            echo json_encode($data);
            exit;
        } else {
            redirect('design/search');
        }
    }

    function add_me() {
        if (!check_user_authentication()) {
            redirect('login');
        }
        $data = array();
        $data['site_setting'] = site_setting();
        $data['active_menu'] = "3ddesign";
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        $data['theme'] = $theme;
        //$data['category'] = get_parent_category();


        $current_design_id = 0;
        if ($_POST) {
            $current_design_id = $this->input->post('design_id');
            $data['category'] = $this->listing_design_dropdown_category($this->input->post('design_category'), 0);
        } else {
            $data['category'] = $this->listing_design_dropdown_category(0, 0);
        }
        $data['alldesign'] = get_alldesign($current_design_id);

        $data['category_id'] = '';
        $data['parent_design_id'] = '';
        //	$data['design_images']='';
        //	$data['design_id']='';
        $data['design_gallery_type'] = '';
        $data["error"] = "";
        $data['design_tag'] = '';
        $data['prev_3dattachment_name']='';
        
        $meta_setting = meta_setting();

        if ($_POST) {
            $this->form_validation->set_rules('design_title', 'Design Title', 'trim|required');
            $this->form_validation->set_rules('design_point', 'Design Point', 'trim|required');
            $this->form_validation->set_rules('design_content', 'Design Content', 'trim|required');
            $this->form_validation->set_rules('design_category', 'Design Category', 'required');
            $this->form_validation->set_rules('design_gallery_type', 'Gallery Type', 'required');

            if ($this->form_validation->run() == FALSE) {
                if (validation_errors()) {
                    $data["error"] = validation_errors();
                } else {
                    $data["error"] = "";
                }

                $data['design_title'] = $this->input->post('design_title');
                $data['design_content'] = $this->input->post('design_content');
                $data['design_category'] = $this->input->post('design_category');
                $data['design_point'] = $this->input->post('design_point');
                $data['design_ref_id'] = $this->input->post('design_ref_id');
                $data['design_by'] = $this->input->post('design_by');
                $data['design_is_modified'] = $this->input->post('design_is_modified');
                $data['design_meta_keyword'] = $this->input->post('design_meta_keyword');
                $data['design_meta_description'] = $this->input->post('design_meta_description');
                $data['design_tag'] = $this->input->post('tag_name');

                $data['design_gallery_type'] = $this->input->post('design_gallery_type');
                
                $data['prev_3dattachment_name']=$this->input->post('prev_3dattachment_name');

                $design_id = $this->input->post('design_id');
                if ($design_id > 0) {
                    $data['design_images'] = $this->design_model->get_one_design_images($design_id);
                    $data['design_id'] = $this->input->post('design_id');
                } else {
                    $data['design_images'] = '';
                    $data['design_id'] = '';
                }
            } else {

                if ($this->input->post('design_id') != '') {
                    $result = $this->design_model->update_design();
                    redirect('design/all/20/0/update_design');
                } else {
                    $result = $this->design_model->insert_design();
                    redirect('design/all/20/0/success_design');
                }
            }
        } else {
            $data['design_title'] = $this->input->post('design_title');
            $data['design_content'] = $this->input->post('design_content');
            $data['design_category'] = $this->input->post('design_category');
            $data['design_point'] = $this->input->post('design_point');
            $data['design_ref_id'] = $this->input->post('design_ref_id');
            $data['design_by'] = $this->input->post('design_by');
            $data['design_is_modified'] = $this->input->post('design_is_modified');
            $data['design_meta_keyword'] = $this->input->post('design_meta_keyword');
            $data['design_meta_description'] = $this->input->post('design_meta_description');
            $data['design_tag'] = $this->input->post('tag_name');
            $data['design_id'] = $this->input->post('design_id');
            $data['design_gallery_type'] = $this->input->post('design_gallery_type');
            $data['design_images'] = '';
            $data['prev_3dattachment_name']=$this->input->post('prev_3dattachment_name');
        }

        $pageTitle = '3D-Design Hive- ' . $meta_setting->title;
        $metaDescription = '3D-Design Hive - ' . $meta_setting->meta_description;
        $metaKeyword = '3D-Design Hive - ' . $meta_setting->meta_keyword;

        $this->template->write('pageTitle', $pageTitle, TRUE);
        $this->template->write('metaDescription', $metaDescription, TRUE);
        $this->template->write('metaKeyword', $metaKeyword, TRUE);
        $this->template->write_view('header', $theme . '/layout/common/header_theme', $data, TRUE);
        $this->template->write_view('content_center', $theme . '/layout/design/add_3d_design_hive', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer_theme', $data, TRUE);
        $this->template->render();
    }

    function edit_me($design_id='') {

        if (!check_user_authentication()) {
            redirect('login');
        }

        $design_detail = $this->design_model->check_exist_design($design_id, get_authenticateUserID());

        if (!$design_detail) {
            redirect('design/all');
        }

        if ($design_id != '' && $design_id > 0) {

            $data = array();
            $data['site_setting'] = site_setting();
            $data['active_menu'] = "3ddesign";
            $theme = getThemeName();
            $this->template->set_master_template($theme . '/template.php');

            $data['theme'] = $theme;



            //$data['alldesign'] = get_modified_design($design_id);

            $current_design_id = $design_id;

            $data['alldesign'] = get_alldesign($current_design_id);




            $data['error'] = '';
            $one_design = $this->design_model->get_one_design($design_id);
            $get_one_design_tag = $this->design_model->get_one_design_tag($design_id);
            $data['design_images'] = $this->design_model->get_one_design_images($design_id);
            
            $design_3d_file=$this->design_model->get_design_files($design_id);
            
            $file_3d_name='';
            
            if($design_3d_file){
                if($design_3d_file->attachment_file!=''){
                    if(file_exists(base_path() . 'upload/design_orig/' . $design_3d_file->attachment_file)){
                       $file_3d_name= $design_3d_file->attachment_file;
                    }
                }
            }
            
            $data['prev_3dattachment_name']=$file_3d_name;
             

            $design_tag = '';
            if ($get_one_design_tag) {

                foreach ($get_one_design_tag as $gt) {
                    $design_tag .= $gt->tag_name . ',';
                }
            }

            $data['design_id'] = $one_design->design_id;
            $data['design_tag'] = substr($design_tag, 0, -1);
            $data['design_title'] = $one_design->design_title;
            $data['design_content'] = $one_design->design_content;
            $data['design_category'] = $one_design->category_id;
            $data['design_point'] = $one_design->design_point;
            $data['design_ref_id'] = $one_design->design_ref_id;
            $data['design_by'] = $one_design->design_by;
            $data['design_is_modified'] = $one_design->design_is_modified;
            $data['design_meta_keyword'] = $one_design->design_meta_keyword;
            $data['design_meta_description'] = $one_design->design_meta_description;
            $data['parent_design_id'] = $one_design->parent_design_id;
            $data['design_gallery_type'] = $one_design->design_gallery_type;


            //$data['category'] = get_parent_category();
            $data['category'] = $this->listing_design_dropdown_category($one_design->category_id, 0);

            $meta_setting = meta_setting();

            $pageTitle = '3D-Design Hive- ' . $meta_setting->title;
            $metaDescription = '3D-Design Hive - ' . $meta_setting->meta_description;
            $metaKeyword = '3D-Design Hive - ' . $meta_setting->meta_keyword;

            $this->template->write('pageTitle', $pageTitle, TRUE);
            $this->template->write('metaDescription', $metaDescription, TRUE);
            $this->template->write('metaKeyword', $metaKeyword, TRUE);
            $this->template->write_view('header', $theme . '/layout/common/header_theme', $data, TRUE);
            $this->template->write_view('content_center', $theme . '/layout/design/add_3d_design_hive', $data, TRUE);
            $this->template->write_view('footer', $theme . '/layout/common/footer_theme', $data, TRUE);
            $this->template->render();
        } else {
            redirect('user/dashboard');
        }
    }

    function delete_me($design_id) {

        if (!check_user_authentication()) {
            redirect('login');
        }

        $design_detail = $this->design_model->check_exist_design($design_id, get_authenticateUserID());
        if (!$design_detail) {
            redirect('design/all');
        }


        if ($design_id != '' && $design_id > 0) {
            $delete_tag = $this->db->query("delete from " . $this->db->dbprefix('design_tags') . " where design_id='" . $design_id . "'");
            $delete_cat = $this->db->query("delete from " . $this->db->dbprefix('design_category_rel') . " where design_id='" . $design_id . "'");

            $design_images = $this->design_model->get_one_design_images($design_id);
            if ($design_images) {
                foreach ($design_images as $di) {
                    if (file_exists(base_path() . 'upload/design_orig/' . $di->attachment_name)) {
                        unlink(base_path() . 'upload/design_orig/' . $di->attachment_name);
                    }

                    if (file_exists(base_path() . 'upload/design/' . $di->attachment_name)) {
                        unlink(base_path() . 'upload/design/' . $di->attachment_name);
                    }
                }
            }
            
            $design_3d_file=$this->design_model->get_design_files($design_id);
            
            if($design_3d_file){
                if($design_3d_file->attachment_file!=''){
                    if(file_exists(base_path() . 'upload/design_orig/' . $design_3d_file->attachment_file)){
                        unlink(base_path() . 'upload/design_orig/' . $design_3d_file->attachment_file);
                    }
                }
            }
            
            

            $delete_attach = $this->db->query("delete from " . $this->db->dbprefix('design_attachment') . " where design_id='" . $design_id . "'");
            $design_comment = $this->db->query("delete from " . $this->db->dbprefix('design_comment') . " where design_id='" . $design_id . "'");
            $design_view = $this->db->query("delete from " . $this->db->dbprefix('design_view') . " where design_id='" . $design_id . "'");
            $design_share = $this->db->query("delete from " . $this->db->dbprefix('design_share') . " where design_id='" . $design_id . "'");
            $design_like = $this->db->query("delete from " . $this->db->dbprefix('design_like') . " where design_id='" . $design_id . "'");
            $design_download = $this->db->query("delete from " . $this->db->dbprefix('design_download') . " where design_id='" . $design_id . "'");
            $delete_tag = $this->db->query("delete from " . $this->db->dbprefix('design') . " where design_id='" . $design_id . "'");

            redirect('design/all/20/0/delete_design');
        } else {
            redirect('user/dashboard');
        }
    }

    function delete_design_attachment() {
        $attachment_id = $_REQUEST['attachment_id'];

        $design_attachment = $this->design_model->get_one_design_attachment($attachment_id);
        if ($design_attachment) {
            if (file_exists(base_path() . 'upload/design_orig/' . $design_attachment->attachment_name)) {
                unlink(base_path() . 'upload/design_orig/' . $design_attachment->attachment_name);
            }

            if (file_exists(base_path() . 'upload/design/' . $design_attachment->attachment_name)) {
                unlink(base_path() . 'upload/design/' . $design_attachment->attachment_name);
            }
        }
        $delete_attach = $this->db->query("delete from " . $this->db->dbprefix('design_attachment') . " where attachment_id='" . $attachment_id . "'");
        echo "success";
        die;
    }

    function search($limit='20', $category_id=0, $keyword='', $offset=0, $msg='') {

        $data = array();
        $data['site_setting'] = site_setting();
        $data['active_menu'] = "3ddesign";
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        $data['theme'] = $theme;
        $meta_setting = meta_setting();
        $data['category'] = get_parent_category();

      
       

        $this->load->library('pagination');

        if ($_POST) {

            $category_ids = '';
            if (isset($_POST['category'])) {
                $category_ids = $_POST['category'];
            };
            $keyword = trim(strip_tags($_POST['keyword']));

            if ($keyword == "") {
                $keyword = 0;
            }
            if ($category_ids) {
                $category_id = trim(strip_tags(implode("-", $category_ids)));
            } else {
                $category_id = 0;
            }

            $limit = trim(strip_tags($this->input->post("limit")));


        } else {
            $limit = trim(strip_tags($limit));
            $keyword = trim(strip_tags($keyword));
            $category_ids = trim(strip_tags($category_id));
            $offset = trim(strip_tags($offset));
            $msg = trim(strip_tags($msg));
        }
        
        if($keyword==''){
            $keyword=0;
        }
        
        
        $config['uri_segment'] = '6';
        $config['base_url'] = site_url( 'design/search/' . $limit . '/' . $category_id . '/' . $keyword);
        $config['total_rows'] = $this->design_model->get_total_category_design_count($category_id, $keyword);
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        $data['page_link'] = $this->pagination->create_links();
        $data['result'] = $this->design_model->get_category_design_result($limit, $offset, $category_id, $keyword);

        /* echo "<pre>";
          print_r($data['result']);
          die; */
        $data['category_id'] = $category_id;
        $data['keyword'] = $keyword;
        $data['msg'] = $msg;

        $data['offset'] = $offset;
        $data['error'] = '';
        if ($this->input->post('limit') != '') {
            $data['limit'] = $this->input->post('limit');
        } else {
            $data['limit'] = $limit;
        }
        
        
        $select_cat=array();
        if($category_id!='')
        {
            $select_cat=explode("-", $category_id);
        }
        $data['search_category_menu']=$this->listing_design_search_category($select_cat,0);




        $pageTitle = '3D-Design Hive- ' . $meta_setting->title;
        $metaDescription = '3D-Design Hive - ' . $meta_setting->meta_description;
        $metaKeyword = '3D-Design Hive - ' . $meta_setting->meta_keyword;

        $this->template->write('pageTitle', $pageTitle, TRUE);
        $this->template->write('metaDescription', $metaDescription, TRUE);
        $this->template->write('metaKeyword', $metaKeyword, TRUE);
        $this->template->write_view('header', $theme . '/layout/common/header_home', $data, TRUE);
        $this->template->write_view('content_center', $theme . '/layout/design/search', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
        $this->template->render();
    }

  

    function view_me($design_id='') {
        //if(!check_user_authentication()) {  redirect('login'); } 

        if ($design_id == '') {
            redirect('home/404');
        }
        $design_detail = $this->design_model->check_exist_design($design_id, 0);
        if (!$design_detail) {
            redirect('design/all');
        }

        $data = array();
        $data['site_setting'] = site_setting();
        $data['active_menu'] = "3ddesign";
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        $data['theme'] = $theme;
        $data['category'] = get_parent_category();
        $data['alldesign'] = get_alldesign();

        //Set design view count
        $design_view = $this->design_model->set_design_view($design_id);

        $one_design = $this->design_model->get_one_design($design_id);


        $get_one_design_tag = $this->design_model->get_one_design_tag($design_id);
        $data['design_images'] = $this->design_model->get_one_design_images($design_id);


        $data['popular_design'] = $this->design_model->get_popular_design(4);

        //$data['popular_design']= $this->design_model->get_user_design(4);


        /* if($get_one_design_tag)
          {
          $design_tag ='';
          foreach($get_one_design_tag as $gt)
          {
          $design_tag .= $gt->tag_name.',';
          }

          } */
        $data['get_one_design_tag'] = $get_one_design_tag;
        $data['main_image'] = $this->design_model->get_one_main_design_image($design_id);
        $data['user_id'] = $one_design->user_id;
        $data['design_id'] = $one_design->design_id;
        //$data['design_tag'] = substr($design_tag,0,-1);
        $data['design_title'] = $one_design->design_title;
        $data['design_content'] = $one_design->design_content;
        $data['category_id'] = $one_design->category_id;
        $data['design_point'] = $one_design->design_point;
        $data['design_ref_id'] = $one_design->design_ref_id;
        $data['design_by'] = $one_design->design_by;
        $data['design_is_modified'] = $one_design->design_is_modified;
        $data['design_meta_keyword'] = $one_design->design_meta_keyword;
        $data['design_meta_description'] = $one_design->design_meta_description;
        $data['parent_design_id'] = $one_design->parent_design_id;
        $data['design_date'] = $one_design->design_date;
        $data['design_view_count'] = $one_design->design_view_count;
        $data['design_like_count'] = $one_design->design_like_count;
        $data["desing_gallery_type"] = $one_design->design_gallery_type;


        $meta_setting = meta_setting();

        $pageTitle = $one_design->design_title . '-' . $meta_setting->title;
        $metaDescription = $one_design->design_meta_description . '-' . $meta_setting->meta_description;
        $metaKeyword = $one_design->design_meta_keyword . '-' . $meta_setting->meta_keyword;

        $this->template->write('pageTitle', $pageTitle, TRUE);
        $this->template->write('metaDescription', $metaDescription, TRUE);
        $this->template->write('metaKeyword', $metaKeyword, TRUE);
        $this->template->write_view('header', $theme . '/layout/common/header_home', $data, TRUE);
        $this->template->write_view('content_center', $theme . '/layout/design/view_design', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
        $this->template->render();
    }

    function buy($design_id = '') {
        $design_id = base64_decode($design_id);
        if (!check_user_authentication()) {
            redirect('login');
        }
        if ($design_id == '') {
            redirect('home/404');
        }
        $design_detail = $this->design_model->check_exist_design($design_id, 0);
        if (!$design_detail) {
            redirect('design/all');
        }
        $res = $this->design_model->design_buy_process($design_id);
        if ($res == 1) {
            
            redirect('user/designdownload/20/0/success_design');
        } else if ($res == 'NO_BALANCE') {
            redirect('design/buypoint');
        } else {
            redirect('design/all');
        }
    }

    function buypoint() {
        $data = array();
        $data['site_setting'] = site_setting();
        $data['active_menu'] = "";
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        $data['theme'] = $theme;
        $data["package_list"] = $this->package_model->get_package_list();
        $meta_setting = meta_setting();

        $pageTitle = '3D-Design Hive- ' . $meta_setting->title;
        $metaDescription = '3D-Design Hive - ' . $meta_setting->meta_description;
        $metaKeyword = '3D-Design Hive - ' . $meta_setting->meta_keyword;

        $this->template->write('pageTitle', $pageTitle, TRUE);
        $this->template->write('metaDescription', $metaDescription, TRUE);
        $this->template->write('metaKeyword', $metaKeyword, TRUE);
        $this->template->write_view('header', $theme . '/layout/common/header_home', $data, TRUE);
        $this->template->write_view('content_center', $theme . '/layout/user/buypoints', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
        $this->template->render();
    }

    function like_dislike($like =0, $id = 0) {

        if (!check_user_authentication()) {
            $this->session->set_userdata('previousPage', 'design/view_me/' . $id);
            redirect("home/login");
        }

        $like_status = $_REQUEST['like_status'];
        $design_id = $_REQUEST['design_id'];
        $res = $this->design_model->set_like_dislike($like_status, $design_id);
        echo "success";
        exit;
    }

    function download_image_zip($design_id) {
        $image_data = $this->design_model->get_all_images_of_design($design_id);
        if ($image_data) {
            foreach ($image_data as $img) {
                
                if($img->attachment_name!=''){
                    if (file_exists(base_path() . 'upload/design_orig/' . $img->attachment_name)) {
                        $path = base_path() . 'upload/design_orig/' . $img->attachment_name;
                        $this->zip->read_file($path);
                    }
                }
                if($img->attachment_file!=''){
                    if (file_exists(base_path() . 'upload/design_orig/' . $img->attachment_file)) {
                        $path = base_path() . 'upload/design_orig/' . $img->attachment_file;
                        $this->zip->read_file($path);
                    }
                }
            }

            $file_name = date('d-m-Y') . '_' . rand() . '_myimage.zip';

            $this->zip->archive(base_path() . 'upload/design_download/' . $file_name);
            $this->zip->download($file_name);
        } else {
            redirect('design/all');
        }
    }

    //============tree======



    function get_parent_design_tree($design_id) {

        $temp = '';
        $chk_parent = $this->db->query("select design_id,design_is_modified,parent_design_id,design_title,design_slug,design_status from " . $this->db->dbprefix('design') . " where design_id='" . $design_id . "'");

        if ($chk_parent->num_rows() > 0) {
            $res = $chk_parent->result();
            foreach ($res as $rs) {


                $cat_link = 'design/' . $rs->design_slug;

                if($rs->design_status==1){
                    $temp.=anchor($cat_link, $rs->design_title) . "###";
                }

                if ($rs->design_is_modified == 1 && $rs->parent_design_id > 0) {




                    $chk_parent2 = $this->db->query("select design_id from " . $this->db->dbprefix('design') . " where design_id='" . $rs->parent_design_id . "'");

                    if ($chk_parent2->num_rows() > 0) {
                        $temp.=$this->get_parent_design_tree($rs->parent_design_id);
                    }
                }
            }
        }


        return $temp;
    }

    function design_parent_tree($design_id) {

        $full_tree = $this->get_parent_design_tree($design_id);
        $reverse_design_link = explode('###', $full_tree);
        $all_design_link = array_reverse($reverse_design_link);

        $html = '';
        if (!empty($all_design_link)) {

            $html.='<ul>';

            $padding_left = 0;
            $level = 1;
            foreach ($all_design_link as $key => $val) {
                if ($val != '') {
                    $padding_left = $padding_left + 10;
                    $html.='<li class="level' . $level . '" style="padding-left:' . $padding_left . 'px;">' . $val . '</li>';
                    $level++;
                }
            }

            $html.='</ul>';
        }
        return $html;
    }

    ///=======design category add tree=======



    function listing_design_dropdown_category($select, $id='',$level=0) {

        $this->cnt++;


        $cat = '';
        $id = empty($id) ? 0 : $id;

        $check_cate = $this->design_model->get_design_category_child_by_id($id);
        if ($check_cate) {

            foreach ($check_cate as $key => $res) {

                if ($select == $res->category_id) {

                    $cat.='<option value="' . $res->category_id . '" selected="selected">';
                } else {

                    $cat.='<option value="' . $res->category_id . '">';
                }




                if ($res->category_parent_id != 0) {

                    //for ($i = 0; $i < $this->cnt; $i++) {
                    for ($i = 0; $i < $level; $i++) {
                        $cat.="&nbsp;&nbsp;&nbsp;&nbsp;";
                    }

                    $cat.='&nbsp;|_';
                }

                $cat.=ucfirst(strtolower($res->category_name)) . '</option>';


                $check_sub_cate = $this->design_model->get_design_category_child_by_id($res->category_id);
                if ($check_sub_cate) {

                    $cat.=$this->listing_design_dropdown_category($select, $res->category_id,$level+1);
                    $this->cnt--;
                }

                $this->cnt--;
            }
        }

       

        return $cat;
    }

    
    
    

    function listing_design_menu_category($select, $id='') {

        
        $cat = '';
        $id = empty($id) ? 0 : $id;

        $check_cate = $this->design_model->get_design_category_child_by_id($id);
        if ($check_cate) {

            foreach ($check_cate as $key => $res) {

                if ($select == $res->category_id) {
                    
                } else {
                   
                }
                
               
                
                $check_sub_cate = $this->design_model->get_design_category_child_by_id($res->category_id);
                $cat.='<li class="expandable">';
                //if ($check_sub_cate) {
                    $cat.='<div class="hitarea expandable-hitarea"></div>';  
                //} 
                
                $cat.='<a href="javascript:void(0)" class="addselcategory" data-id="'.$res->category_id.'">'.ucfirst($res->category_name).'</a>';


                
                if ($check_sub_cate) {                    
                    $cat.='<ul style="display: none;">';
                    $cat.=$this->listing_design_menu_category($select, $res->category_id);
                    $cat.='</ul>';
                } else {
                    $cat.='<ul style="display: none;">';                    
                    $cat.='</ul>';
                }
                
                $cat.='</li>';

                
            }
        }

       

        return $cat;
    }

   
    
    function designcattree($cnt=0, $category_id=0) {

        $html = $this->listing_design_menu_category(0,0);

        echo $html;
        die;
    }
      function addcategory() {
         $status="fail";
         $msg="";
          
        if (!check_user_authentication()) {
            $msg="login";
        }
        

        if ($_POST) {
            
            $category_name=trim(strip_tags($this->input->post('category_name')));
            $category_parent_id=trim(strip_tags($this->input->post('selected_category')));
                    
            if($category_name!=''){
           
           
                $result = $this->design_model->insert_design_category($category_name,$category_parent_id);
                $status="success";
                
            } else {
                $msg="Please enter category name.";
            }
           
        } else {
            $msg="Enable to post data.";
        }
        
        
        echo json_encode(array('status'=>$status,"msg"=>$msg));
     die;
    }
    
    
    

    function listing_design_search_category($select=array(), $id='') {

        
        $cat = '';
        $id = empty($id) ? 0 : $id;

        $check_cate = $this->design_model->get_design_category_child_by_id($id);
        if ($check_cate) {

            foreach ($check_cate as $key => $res) {

                
                
               
                
                $check_sub_cate = $this->design_model->get_design_category_child_by_id($res->category_id);
                $cat.='<li>';
                
                
                $is_select='';
                 if (in_array($res->category_id, $select, true)) {  $is_select=' checked="checked" ';  }
                
                $cat.='<div class="radio"><label><input type="checkbox" name="category[]" '.$is_select.' id="category'.$res->category_id.'" value="'.$res->category_id.'" /> '.ucfirst(strtolower($res->category_name)).' </label></div>';  
                 
                
              

                
                if ($check_sub_cate) {                    
                    $cat.='<ul class="list-unstyled">';
                    $cat.=$this->listing_design_search_category($select, $res->category_id);
                    $cat.='</ul>';
                } 
                
                $cat.='</li>';

                
            }
        }

       

        return $cat;
    }

   

}

?>