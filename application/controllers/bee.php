<?php
class Bee extends IWEB_Controller 
{
	
	/*
	Function name :Bee()
	Description :Its Default Constuctor which called when user object initialzie.its load necesary models
	*/
	function Bee()
	{
		parent::__construct();	
		$this->load->model('home_model');	
		$this->load->model('user_model');	
		$this->load->model('design_model');	
		$this->load->model('video_model');	
		$this->load->model('challenge_model');	
		
	}
	
	
	/*
	Function name :index()
	Parameter :none
	Return : none
	Use : redirect to user dashboard
	Description : none
	*/
	
	public function index()
	{
		redirect('home');
	}
	
	function profile($profile_name='')
	{
           
            if ($profile_name=='') {
                redirect('home/404');
            } 
            
            $user_profile=get_user_profile_by_profile_name($profile_name);
            
            if(!$user_profile) { redirect('home/404'); }
            if(empty($user_profile)) { redirect('home/404'); }
		
		
            $data['user_profile']=$user_profile;

            
            
            $data['site_setting'] = site_setting();
            $data['active_menu'] = "";
            $theme = getThemeName();
            $this->template->set_master_template($theme . '/template.php');

            $data['theme'] = $theme;
            $data['category'] = get_parent_category();
           


            $data['user_design'] =  $this->design_model->get_user_design($user_profile->user_id, 100, 0);
            
         

            $meta_setting = meta_setting();

            $pageTitle = $user_profile->first_name.' '.$user_profile->last_name . ' - ' . $meta_setting->title;
            $metaDescription = $user_profile->first_name.' '.$user_profile->last_name . ' - ' . $meta_setting->title;
            $metaKeyword = $user_profile->first_name.' '.$user_profile->last_name . ' - ' . $meta_setting->title;

            $this->template->write('pageTitle', $pageTitle, TRUE);
            $this->template->write('metaDescription', $metaDescription, TRUE);
            $this->template->write('metaKeyword', $metaKeyword, TRUE);
            $this->template->write_view('header', $theme . '/layout/common/header_home', $data, TRUE);
            $this->template->write_view('content_center', $theme . '/layout/bee/profile', $data, TRUE);
            $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
            $this->template->render();
       
		
	}
	
		
	

}

?>