<?php
class Package extends IWEB_Controller 
{
	
	/*
	Function name :User()
	Description :Its Default Constuctor which called when user object initialzie.its load necesary models
	*/
	function Package()
	{
		parent::__construct();	
		$this->load->model('package_model');	
		
	}
	
	
	function all($msg = '')
	{
	     	if(!check_user_authentication()) {  redirect('login'); } 
		$data=array();
		$theme = getThemeName();
		
		$data['active_menu']='buycredit';
		$this->template->set_master_template($theme .'/template.php');
		
		$meta_setting=meta_setting();
		$data["site_setting"]=site_setting();
		
		$data["package_list"] = $this->package_model->get_package_list();
		$data["msg"] = base64_decode($msg);
		
		$pageTitle=$meta_setting->title;
		$metaDescription=$meta_setting->meta_description;
		$metaKeyword=$meta_setting->meta_keyword;
		
		
		$this->template->write('pageTitle',$pageTitle,TRUE);
		$this->template->write('metaDescription',$metaDescription,TRUE);
		$this->template->write('metaKeyword',$metaKeyword,TRUE);
		
		
		$this->template->write_view('header',$theme .'/layout/common/header_theme',$data,TRUE);
		$this->template->write_view('campaign_box',$theme .'/layout/package/list',$data,TRUE);		
		$this->template->write_view('footer',$theme .'/layout/common/footer_theme',$data,TRUE);
		$this->template->render();
	}
	
	
	function buy_credit($package_id = 0)
	{
		 $package_id = base64_decode($package_id);
		 $package_detail = $this->package_model->get_one_package_detail($package_id);
		 
		 /// as of now pass static id
		 $paypal_setting_id = 1;
		 /************************ instant payment***************************/
		 $num='WL'.randomCode();
			
			$this->load->library('paypal_lib');
			
			$Paypal_Email=$this->package_model->get_gateway_result($paypal_setting_id,'paypal_email');			
			$Paypal_Status=$this->package_model->get_gateway_result($paypal_setting_id,'site_status');
			$Paypal_Status=(array) $Paypal_Status;
			$Paypal_Email=(array) $Paypal_Email;
			
			$Paypal_Url='https://www.sandbox.paypal.com/cgi-bin/webscri';
			
			if(trim($Paypal_Status['value'])=='sandbox')
			{
				$Paypal_Url='https://www.sandbox.paypal.com/cgi-bin/webscri';
			}
			if(trim($Paypal_Status['value'])=='live')
			{
				$Paypal_Url='https://www.paypal.com/cgi-bin/webscri';
			}
                        
                        
			
			$meta_setting=meta_setting();
			
			$user_info = get_user_info(get_authenticateUserID());
			
			$data['site_setting']=(array)site_setting();
			$user_detail=(array)$user_info;
			$site_setting=$data['site_setting'];
			
			$paypal_form = '<div data-role="page" data-url="" class="ui-page ui-body-c ui-page-active" style="min-height: 383px;">';
			$paypal_form .= '<form name="paypal_form" id="paypal_form" action="'.$Paypal_Url.'" method="post">';
			$paypal_form .= '<input type="hidden" name="rm" value="2" >';
			$paypal_form .= '<input type="hidden" name="cmd" value="_xclick" >';
			$paypal_form .= '<input type="hidden" name="quantity" value="1" >';
			$paypal_form .= '<input type="hidden" name="currency_code" value="'.$site_setting['currency_code'].'" >';
			$paypal_form .= '<input type="hidden" name="business" value="'.$Paypal_Email['value'].'" >';
			$paypal_form .= '<input type="hidden" name="return" value="'.site_url('package/paypalsuccess/'.$package_id).'" >';
			$paypal_form .= '<input type="hidden" name="cancel_return" value="'.site_url('package/paypalcancel/'.$package_id).'" >';
	  		$paypal_form .= '<input type="hidden" name="notify_url" value="'.site_url('package/paypalipn').'" >';
			$paypal_form .= '<input type="hidden" name="custom" value="'.$package_detail["package_price"].'#'.$package_id.'#'.get_authenticateUserID().'" >';
			$paypal_form .= '<input type="hidden" name="item_name" value="New package purchesed in '.$site_setting['site_name'].' by '.$user_detail['first_name'].' '.$user_detail['last_name'].'" >';
			$paypal_form .= '<input type="hidden" name="item_number" value="'.$num.'" >';
			$paypal_form .= '<input type="hidden" name="amount" value="'.$package_detail["package_price"].'" >';
			$paypal_form .= '<input type="submit" value="Click here to redirected paypal" name="pp_submit" class="ui-btn-hidden" aria-disabled="false">';
			$paypal_form .= '</form>';
			$paypal_form .= '</div>';
			//$this->paypal_lib->paypal_auto_form();
                       
			echo $paypal_form;
                         echo '<script>document.getElementById("paypal_form").submit();</script>';
		 /**********************end ***************************************/
		
	}
	
	
	 function paypalsuccess($package_id=0)
	{
			$msg = base64_encode("success");
			redirect("package/all/".$msg);
    }	
	
	
	function paypalcancel($task_id=0,$task_comment_id=0)
	{		
			$msg= base64_encode("fail");
			redirect("package/all/".$msg);
	}
	
	
	/*
	Function name :paypalipn()
	Parameter : none
	Return : none
	Use : When user successfully paid amount on paypal then user transaction information send in this function.
	Description : paypal.com send user transaction information to this function which called http://hostname/paypalcancel
	*/

	function paypalipn()
	{	
		$vals = array();
		$strtemp='';
		foreach ($_POST as $key => $value) 
		{					
			$vals[$key] = $value;
			$strtemp.= $key."=".$value.",";
		}
		      
		
		
			
			$status=$_POST['payment_status'];
			$custom=explode('#',$_POST['custom']);
			$package_id=$custom[1];
			$package_price=$custom[0];
			$pay_gross=$_POST['mc_gross'];
			$payee_email=$_POST['payer_email'];
			$payer_status =$_POST['payer_status'];
			$txn_id=$_POST['txn_id'];
			$date=date('Y-m-d H:i:s');
			$ip=getRealIP();
			$user_id=$custom[2];
			$user_info = get_user_profile_by_id($user_id);
			$login_user=(array)$user_info;
			
			$package_detail = $this->package_model->get_one_package_detail($package_id);
			
			$chk_transaction_id=$this->package_model->check_unique_transaction($txn_id);
			
			
			$strtemp.= "chk_transaction_id=".$chk_transaction_id.",";
			
			
			log_message('error',"PAYPAL IPN DATA:".$strtemp);
			
			if((strtolower($status)=='completed' || strtolower($status)=='pending') && $pay_gross>=$custom_amt && $chk_transaction_id==0)
			{
				
				//**************** add into Transaction *******************//
				$tranaction_insert = array("user_id"=>$user_id,
				                           "package_id"=>$package_id,
										   "pay_amount"=>$pay_gross,
										   "pay_points"=>$package_detail["package_points"],
										   "host_ip"=>$ip,
										   "pay_email"=>$payee_email,
										   "transaction_date_time"=>$date,
										   "pay_transaction_id"=>$txn_id,
										   "transaction_approved"=>"1");
			    $this->db->insert("transaction",$tranaction_insert);	
				$transaction_id = mysql_insert_id();						   
				//**************** end ************************************//
					
					
					
					
					
					$data=array(
					'credit' => $package_detail["package_points"],
					'user_id' => $user_id,
					'status' => $payer_status,
					'admin_status' => "Confirm",
					'wallet_date' => $date,
					'wallet_transaction_id' => $txn_id,
					"transaction_id"=>$transaction_id,
					'wallet_ip' => $ip	
							
					);					
					$add_wallet=$this->db->insert('wallet',$data);			
					
					
					///// user package///
					$data=array(
					'package_id' => $package_id,
					'user_id' => $user_id,
					'subscription_date' => $date,
					'package_is_free' => "0",
					'package_type'=>'1',
					"package_price"=>$package_detail["package_price"],
					"package_points"=>$package_detail["package_points"],
					'subscription_ip' => $ip	
					);					
					$this->db->insert('user_package',$data);	
					/// end//
					
					//////*********** send email /*****************//
					
					// buyer
					$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Package Purchase Notification'");
				   $email_temp=$email_template->row();				
			
				   $email_from_name=$email_temp->from_name;

				   $email_address_from=$email_temp->from_address;
				   $email_address_reply=$email_temp->reply_address;
				
				   $email_subject=$email_temp->subject;				
				   $email_message=$email_temp->message;
				   
				    $email_to = $user_info->email;
				
				
				
				$email_message=str_replace('{break}','<br/>',$email_message);
				$email_message=str_replace('{user_name}',$user_info->full_name,$email_message);
				$email_message=str_replace('{package_title}',$package_detail["package_name"],$email_message);
				$email_message=str_replace('{package_price}',$package_detail["package_price"],$email_message);
				$email_message=str_replace('{package_point}',$package_detail["package_points"],$email_message);
				$email_message=str_replace('{payee_email}',$payee_email,$email_message);
				$email_message=str_replace('{txn_id}',$txn_id,$email_message);
			
				$str=$email_message;
				
				/** custom_helper email function **/
									
				email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
					//// *************** end ******************/////		
					
			 //end buyer
			 
			 // administrator
			 $email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Package Purchase Admin Notification'");
				   $email_temp=$email_template->row();				
			
				   $email_from_name=$email_temp->from_name;

				   $email_address_from=$email_temp->from_address;
				   $email_address_reply=$email_temp->reply_address;
				
				   $email_subject=$email_temp->subject;				
				   $email_message=$email_temp->message;
				  $email_to = $email_temp->from_address;
				
				
				$email_message=str_replace('{break}','<br/>',$email_message);
				$email_message=str_replace('{user_name}',$user_info->full_name,$email_message);
				$email_message=str_replace('{package_title}',$package_detail["package_name"],$email_message);
				$email_message=str_replace('{package_price}',$package_detail["package_price"],$email_message);
				$email_message=str_replace('{package_point}',$package_detail["package_points"],$email_message);
				$email_message=str_replace('{payee_email}',$payee_email,$email_message);
				$email_message=str_replace('{txn_id}',$txn_id,$email_message);
			
				$str=$email_message;
				
				/** custom_helper email function **/
									
				email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
					//// *************** end ******************/////		
			 // end administrator							
			}
			
			else
			{
			     ///payment process fail
			      
			}
				
	}
	
	
	
}	