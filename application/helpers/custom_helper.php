<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');




// --------------------------------------------------------------------

/**
 * Site Base Path
 *
 * @access	public
 * @param	string	the Base Path string
 * @return	string
 */
function base_path() {
    $CI = & get_instance();
    return $base_path = $CI->config->slash_item('base_path');
}

function credit_setting() {
    $CI = & get_instance();
    $query = $CI->db->get_where("credit_setting", array('credit_setting_id' => 1));
    if ($query->num_rows > 0) {
        return $query->row();
    }
}

// --------------------------------------------------------------------

/**
 * Site Front Url
 *
 * @access	public
 * @param	string	the Front Url string
 * @return	string
 */
function front_base_url() {
    $CI = & get_instance();
    return $base_path = $CI->config->slash_item('base_url_site');
}

// --------------------------------------------------------------------

/**
 * Site Front ActiveTemplate
 *
 * @access	public
 * @param	string	current theme folder name
 * @return	string
 */
function getThemeName() {

    $default_theme_name = 'default';

    $CI = & get_instance();


    $supported_cache = check_supported_cache_driver();

    if (isset($supported_cache)) {
        if ($supported_cache != '' && $supported_cache != 'none') {
            ////===load cache driver===
            $CI->load->driver('cache');

            if ($CI->cache->$supported_cache->get('front_theme_name')) {
                $theme_name = $CI->cache->$supported_cache->get('front_theme_name');
            } else {
                $query = $CI->db->get_where("template_manager", array('active_template' => 1, 'is_admin_template' => 0));
                $row = $query->row();
                $theme_name = trim($row->template_name);

                $CI->cache->$supported_cache->save('front_theme_name', $theme_name, CACHE_VALID_SEC);
            }
        } else {
            $query = $CI->db->get_where("template_manager", array('active_template' => 1, 'is_admin_template' => 0));
            $row = $query->row();
            $theme_name = trim($row->template_name);
        }
    } else {
        $query = $CI->db->get_where("template_manager", array('active_template' => 1, 'is_admin_template' => 0));
        $row = $query->row();
        $theme_name = trim($row->template_name);
    }
    //////////====end cache part








    if (is_dir(APPPATH . 'views/' . $theme_name)) {
        return $theme_name;
    } else {
        return $default_theme_name;
    }
}

/* * ** get dynamic logo of current theme
 * return array
 * * */

function logo_image() {

    $CI = & get_instance();
    $query = $CI->db->get_where("template_manager", array('active_template' => 1, 'is_admin_template' => 0));

    if ($query->num_rows() > 0) {
        return $query->row();
    }
    return 0;
}

function get_one_transaction($transaction_id) {

    $CI = & get_instance();
    $query = $CI->db->get_where("transaction", array('transaction_id' => $transaction_id));

    if ($query->num_rows() > 0) {
        return $query->row();
    }
    return 0;
}

function commission_setting() {
    $CI = & get_instance();
    $commission_sql = $CI->db->query("SELECT * FROM " . $CI->db->dbprefix('commission_setting') . " order by commission_id desc limit 1");

    if ($commission_sql->num_rows() > 0) {
        return $commission_sql->row();
    }
    return false;
}

function getuserpoints($user_id) {

    /* $CI =& get_instance();
      $balance_sql=$CI->db->query("SELECT SUM(credit) as total_balance FROM ".$CI->db->dbprefix('wallet')." where is_authorize=0 and user_id='".$user_id."'");

      if($balance_sql->num_rows()>0) {
      $result= $balance_sql->row();
      return $result->total_balance;
      }

      return 0; */

    /* $CI =& get_instance();
      $query = $CI->db->query("SELECT SUM(debit) as sumd,SUM(credit) as sumc FROM ".$CI->db->dbprefix('wallet')." where is_authorize!=1 and user_id='".get_authenticateUserID()."'");


      if($query->num_rows()>0)
      {

      $result = $query->row();

      $debit=$result->sumd;
      $credit=$result->sumc;

      $total=$credit-$debit;

      return $total;

      }

      return 0; */
    $CI = & get_instance();
    $query = $CI->db->query("SELECT SUM(debit) as sumd,SUM(credit) as sumc FROM " . $CI->db->dbprefix('wallet') . " where user_id='" . $user_id . "'");


    if ($query->num_rows() > 0) {

        $result = $query->row();

        $debit = $result->sumd;
        $credit = $result->sumc;

        $total = $credit - $debit;

        return $total;
    }

    return 0;
}

function getuserpoints_unauth($user_id) {

    /* $CI =& get_instance();
      $balance_sql=$CI->db->query("SELECT SUM(credit) as total_balance FROM ".$CI->db->dbprefix('wallet')." where is_authorize=0 and user_id='".$user_id."'");

      if($balance_sql->num_rows()>0) {
      $result= $balance_sql->row();
      return $result->total_balance;
      }

      return 0; */

    $CI = & get_instance();
    $query = $CI->db->query("SELECT SUM(debit) as sumd,SUM(credit) as sumc FROM " . $CI->db->dbprefix('wallet') . " where is_authorize='0' and user_id='" . get_authenticateUserID() . "'");


    if ($query->num_rows() > 0) {

        $result = $query->row();

        $debit = $result->sumd;
        $credit = $result->sumc;

        $total = $credit - $debit;

        return $total;
    }

    return 0;
}

function getuserpoints_auth($user_id) {

    /* $CI =& get_instance();
      $balance_sql=$CI->db->query("SELECT SUM(credit) as total_balance FROM ".$CI->db->dbprefix('wallet')." where is_authorize=0 and user_id='".$user_id."'");

      if($balance_sql->num_rows()>0) {
      $result= $balance_sql->row();
      return $result->total_balance;
      }

      return 0; */
    $total = 0;
    $CI = & get_instance();
    $query = $CI->db->query("SELECT SUM(debit) as sumd,SUM(credit) as sumc FROM " . $CI->db->dbprefix('wallet') . " where is_authorize='1' and user_id='" . get_authenticateUserID() . "'");


    if ($query->num_rows() > 0) {

        $result = $query->row();

        $debit = $result->sumd;
        $credit = $result->sumc;
        if ($debit > 0) {
            $total = $debit;
        }
    }

    return $total;
}

function get_all_picture_tree($design_id=0) {
    $CI = & get_instance();
    $ids = array();

    $picture_sql = $CI->db->query("SELECT * FROM " . $CI->db->dbprefix('design') . " where design_id=" . $design_id);

    if ($picture_sql->num_rows() > 0) {
        ///===current parent image author=====
        $picture_detail = $picture_sql->row();

        $ids[] = $picture_detail->user_id;
        ///===current parent image author=====
        //===check parent parent=======
        if ($picture_detail->parent_design_id > 0 && $picture_detail->design_is_modified == 1) {
            $new_ids = get_all_picture_tree($picture_detail->parent_design_id);
            $ids = array_merge($ids, $new_ids);
        }
    }
    return $ids;
}

function splitampunt($user_total_point_commission, $ids=array()) {
    $CI = & get_instance();
    $pay = array();

    $total_split = count($ids);

    if ($total_split > 0) {

        $individual_credits = $user_total_point_commission / $total_split;

        $final_credits_percentage = round($individual_credits, 2);

        foreach ($ids as $key => $user_id) {
            if (isset($pay[$user_id])) {
                $pay[$user_id] = $pay[$user_id] + $final_credits_percentage;
            } else {
                $pay[$user_id] = $final_credits_percentage;
            }
        }
    }

    return $pay;
}

function countpoints($points, $perc) {
    $image_points = round((($points * $perc) / 100), 2);

    return $image_points;
}

// --------------------------------------------------------------------

/**
 * Check user login
 *
 * @return	boolen
 */
function check_user_authentication() {
    $CI = & get_instance();

    if ($CI->session->userdata('user_id') != '') {
        return true;
    } else {
        return false;
    }
}

// --------------------------------------------------------------------

/**
 * get login user id
 *
 * @return	integer
 */
function get_authenticateUserID() {
    $CI = & get_instance();
    return $CI->session->userdata('user_id');
}

function get_user_info($user_id) {
    $CI = & get_instance();
    $query = $CI->db->get_where('user', array('user_id' => $user_id));
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return 0;
    }
}

/* * ** check user online status
 * *
 * ** */

function check_online_user() {
    $cur_date_time = date('Y-m-d H:i:s');

    $CI = & get_instance();

    $login_details = '';

    $supported_cache = check_supported_cache_driver();

    ////===load cache driver===
    $CI->load->driver('cache');

    if (isset($supported_cache)) {
        if ($supported_cache != '' && $supported_cache != 'none') {


            if ($CI->cache->$supported_cache->get('user_login' . get_authenticateUserID())) {
                $login_details = (object) $CI->cache->$supported_cache->get('user_login' . get_authenticateUserID());
            } else {

                $get_user_login = $CI->db->query("select * from " . $CI->db->dbprefix('user_login') . " where DATE(login_date_time)='" . date('Y-m-d') . "'  and login_status=1 and user_id='" . get_authenticateUserID() . "' order by login_id desc limit 1");

                if ($get_user_login->num_rows() > 0) {
                    $login_details = $get_user_login->row();
                    $CI->cache->$supported_cache->save('user_login' . get_authenticateUserID(), $login_details, CACHE_VALID_SEC);
                }
            }
        } else {

            $get_user_login = $CI->db->query("select * from " . $CI->db->dbprefix('user_login') . " where DATE(login_date_time)='" . date('Y-m-d') . "'  and login_status=1 and user_id='" . get_authenticateUserID() . "' order by login_id desc limit 1");

            if ($get_user_login->num_rows() > 0) {
                $login_details = $get_user_login->row();
            }
        }
    } else {
        $get_user_login = $CI->db->query("select * from " . $CI->db->dbprefix('user_login') . " where DATE(login_date_time)='" . date('Y-m-d') . "'  and login_status=1 and user_id='" . get_authenticateUserID() . "' order by login_id desc limit 1");

        if ($get_user_login->num_rows() > 0) {
            $login_details = $get_user_login->row();
        }
    }
    //////////====end cache part




    if ($login_details) {

        $login_time = $login_details->login_date_time;
        $login_extend_time = date('Y-m-d H:i:s', strtotime(date("Y-m-d H:i:s", strtotime($login_time)) . " +20 minutes"));


        if (strtotime($cur_date_time) > strtotime($login_extend_time)) {
            $data_up = array(
                'login_status' => 0
            );

            $CI->db->where('login_id', $login_details->login_id);
            $CI->db->update('user_login', $data_up);



            ////==destroy cache====	


            if (isset($supported_cache)) {
                if ($supported_cache != '' && $supported_cache != 'none') {
                    if ($CI->cache->$supported_cache->get('user_login' . get_authenticateUserID())) {
                        $CI->cache->$supported_cache->delete('user_login' . get_authenticateUserID());
                    }
                }
            }


            ////==destroy user session===
            $CI->session->set_userdata('user_id', $CI->security->xss_clean(''));
            $CI->session->set_userdata('full_name', $CI->security->xss_clean(''));
        } else {

            $data_up = array(
                'login_status' => 1,
                'login_date_time' => $cur_date_time,
                'login_id' => $login_details->login_id,
                'user_id' => get_authenticateUserID(),
                'login_ip' => $login_details->login_ip,
            );


            if (isset($supported_cache)) {
                if ($supported_cache != '' && $supported_cache != 'none') {
                    $CI->cache->$supported_cache->save('user_login' . get_authenticateUserID(), $data_up, CACHE_VALID_SEC);
                } else {
                    $data_sess = array(
                        'login_status' => 1,
                        'login_date_time' => $cur_date_time
                    );


                    $CI->db->where('login_id', $login_details->login_id);
                    $CI->db->update('user_login', $data_sess);
                }
            } else {
                $data_sess = array(
                    'login_status' => 1,
                    'login_date_time' => $cur_date_time
                );


                $CI->db->where('login_id', $login_details->login_id);
                $CI->db->update('user_login', $data_sess);
            }
        }
    } else {


        ////==destroy cache====				
        if (isset($supported_cache)) {
            if ($supported_cache != '' && $supported_cache != 'none') {
                if ($CI->cache->$supported_cache->get('user_login' . get_authenticateUserID())) {
                    $CI->cache->$supported_cache->delete('user_login' . get_authenticateUserID());
                }
            }
        }

        ////==destroy user session===
        $CI->session->set_userdata('user_id', $CI->security->xss_clean(''));
        $CI->session->set_userdata('full_name', $CI->security->xss_clean(''));
    }
}

/* * * check user suspend 
 * ** 1= temprory, 0= none, 2=permanent
 * ** return boolean
 * * */

function check_user_suspend() {
    $CI = & get_instance();

    $cur_date = date('Y-m-d H:i:s');

    /* $query=$CI->db->get_where('user',array('user_id'=>get_authenticateUserID()));

      if($query->num_rows()>0)
      {
      $result=$query->row();

      if($result->user_status==2)
      {
      return 1;
      }

      return 0;
      } */


    $query = $CI->db->query("select * from " . $CI->db->dbprefix('user_suspend') . " where user_id='" . get_authenticateUserID() . "' and '" . $cur_date . "' between suspend_from_date and suspend_to_date order by user_suspend_id desc limit 1");


    if ($query->num_rows() > 0) {
        return 1;
    } else {

        $query = $CI->db->query("select * from " . $CI->db->dbprefix('user_suspend') . " where user_id='" . get_authenticateUserID() . "' and is_permanent=1 order by user_suspend_id desc limit 1");

        if ($query->num_rows() > 0) {
            return 2;
        }
    }

    return 0;
}

/* * * get all paypal detail
 * return all record array
 * */

function paypal_setting() {

    $CI = & get_instance();
    $query = $CI->db->get("paypal");
    return $query->row();
}

/* * *
 * 	get spam setting
 *
 */

function spam_setting() {
    $CI = & get_instance();
    $query = $CI->db->get("spam_control");

    return $query->row();
}

function credit_card_setting() {
    $CI = & get_instance();
    $query = $CI->db->get("paypal_credit_card");
    return $query->row();
}

// --------------------------------------------------------------------

/**
 * get site visitor ip address 
 *
 * @return	integer
 */
function spam_protection() {
    $CI = & get_instance();
    $CI->db->order_by('spam_id', 'desc');
    $query = $CI->db->get_where("spam_ip", array('spam_ip' => getRealIP()), 1);

    if ($query->num_rows() > 0) {
        return 1;
    }

    return 0;
}

/**
 * get site visitor ip address
 *
 * @return	integer
 */
function email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str, $email_from_name='', $email_attachment='') {

    $CI = & get_instance();
    $query = $CI->db->get_where("email_setting", array('email_setting_id' => 1));
    $email_set = $query->row();


    $CI->load->library(array('email'));

    ///////====smtp====

    if ($email_set->mailer == 'smtp') {

        $config['protocol'] = 'smtp';
        $config['smtp_host'] = trim($email_set->smtp_host);
        $config['smtp_port'] = trim($email_set->smtp_port);
        $config['smtp_timeout'] = '30';
        $config['smtp_user'] = trim($email_set->smtp_email);
        $config['smtp_pass'] = trim($email_set->smtp_password);
    }

    /////=====sendmail======
    elseif ($email_set->mailer == 'sendmail') {

        $config['protocol'] = 'sendmail';
        $config['mailpath'] = trim($email_set->sendmail_path);
    }

    /////=====php mail default======
    else {
        
    }


    $config['wordwrap'] = TRUE;
    $config['mailtype'] = 'html';
    $config['crlf'] = '\n\n';
    $config['newline'] = '\n\n';

    $CI->email->initialize($config);

    $CI->email->clear();

    if ($email_from_name != '') {
        $CI->email->from($email_address_from, $email_from_name);
    } else {
        $CI->email->from($email_address_from);
    }

    if ($email_attachment != '') {
        $CI->email->attach($email_attachment);
    }

    $CI->email->reply_to($email_address_reply);
    $CI->email->to($email_to);
    $CI->email->subject($email_subject);
    $CI->email->message($str);
    $CI->email->send();
}

function nonRepeat($min, $max, $count) {
    //prevent function from hanging  
    //due to a request of more values than are possible     
    if ($max - $min < $count) {
        return false;
    }

    $nonrepeatarray = array();
    for ($i = 0; $i < $count; $i++) {
        $rand = rand($min, $max);

        //ensure value isn't already in the array 
        //if it is, recalculate the rand until we 
        //find one that's not in the array 
        while (in_array($rand, $nonrepeatarray)) {
            $rand = rand($min, $max);
        }

        //add it to the array 
        $nonrepeatarray[$i] = $rand;
    }
    return implode($nonrepeatarray);
}

/**
 * generate random code
 *
 * @return	string
 */
function randomCode() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array();

    for ($i = 0; $i < 12; $i++) {
        $n = rand(0, strlen($alphabet) - 1); //use strlen instead of count
        $pass[$i] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function getrandomCode($length=12) {

    $code = '';
    //Ascii Code for number, lowercase, uppercase and special characters
    $no = range(48, 57);
    $lo = range(97, 122);
    $up = range(65, 90);

    //exclude character I, l, 1, 0, O
    $eno = array(48, 49);
    $elo = array(108);
    $eup = array(73, 79);
    $no = array_diff($no, $eno);
    $lo = array_diff($lo, $elo);
    $up = array_diff($up, $eup);
    $chr = array_merge($no, $lo, $up);


    for ($i = 1; $i <= $length; $i++) {

        $code.= chr($chr[rand(0, count($chr) - 1)]);
    }
    return $code;
}

/* * * load site setting
 *  return single record array
 * */

function site_setting() {
    $CI = & get_instance();


    $supported_cache = check_supported_cache_driver();

    if (isset($supported_cache)) {
        if ($supported_cache != '' && $supported_cache != 'none') {
            ////===load cache driver===
            $CI->load->driver('cache');

            if ($CI->cache->$supported_cache->get('site_setting')) {
                return (object) $CI->cache->$supported_cache->get('site_setting');
            } else {
                $query = $CI->db->get("site_setting");
                $CI->cache->$supported_cache->save('site_setting', $query->row(), CACHE_VALID_SEC);
                return $query->row();
            }
        } else {
            $query = $CI->db->get("site_setting");
            return $query->row();
        }
    } else {
        $query = $CI->db->get("site_setting");
        return $query->row();
    }
    //////////====end cache part
}

/* * * load user setting
 *  return single record array
 * */

function user_setting() {
    $CI = & get_instance();




    $supported_cache = check_supported_cache_driver();

    if (isset($supported_cache)) {
        if ($supported_cache != '' && $supported_cache != 'none') {
            ////===load cache driver===
            $CI->load->driver('cache');

            if ($CI->cache->$supported_cache->get('user_setting')) {
                return (object) $CI->cache->$supported_cache->get('user_setting');
            } else {
                $query = $CI->db->get("user_setting");
                $CI->cache->$supported_cache->save('user_setting', $query->row(), CACHE_VALID_SEC);
                return $query->row();
            }
        } else {
            $query = $CI->db->get("user_setting");
            return $query->row();
        }
    } else {
        $query = $CI->db->get("user_setting");
        return $query->row();
    }
    //////////====end cache part
}

/* * * load meta setting
 *  return single record array
 * */

function meta_setting() {
    $CI = & get_instance();


    $supported_cache = check_supported_cache_driver();

    if (isset($supported_cache)) {
        if ($supported_cache != '' && $supported_cache != 'none') {
            ////===load cache driver===
            $CI->load->driver('cache');

            if ($CI->cache->$supported_cache->get('meta_setting')) {
                return (object) $CI->cache->$supported_cache->get('meta_setting');
            } else {
                $query = $CI->db->get("meta_setting");
                $CI->cache->$supported_cache->save('meta_setting', $query->row(), CACHE_VALID_SEC);
                return $query->row();
            }
        } else {
            $query = $CI->db->get("meta_setting");
            return $query->row();
        }
    } else {
        $query = $CI->db->get("meta_setting");
        return $query->row();
    }
    //////////====end cache part
}

/* * * load image setting
 *  return single record array
 * */

function image_setting() {
    $CI = & get_instance();
    $query = $CI->db->get("image_setting");
    return $query->row();
}

/* * * load facebook setting
 *  return single record array
 * */

function facebook_setting() {
    $CI = & get_instance();
    $query = $CI->db->get("facebook_setting");
    return $query->row();
}

/* * * load twitter setting
 *  return single record array
 * */

function twitter_setting() {
    $CI = & get_instance();
    $query = $CI->db->get("twitter_setting");
    return $query->row();
}

/* * * load google setting
 *  return single record array
 * */

function google_setting() {
    $CI = & get_instance();
    $query = $CI->db->get("google_setting");
    return $query->row();
}

/* * * load yahoo setting
 *  return single record array
 * */

function yahoo_setting() {
    $CI = & get_instance();
    $query = $CI->db->get("yahoo_setting");
    return $query->row();
}

/* * * load paging setting
 *  return single record array
 * */

function paging_setting() {
    $CI = & get_instance();
    $query = $CI->db->get("paging_setting");
    return $query->row();
}

/* * * load wallet setting
 *  return single record array
 * */

function wallet_setting() {
    $CI = & get_instance();



    $supported_cache = check_supported_cache_driver();

    if (isset($supported_cache)) {
        if ($supported_cache != '' && $supported_cache != 'none') {
            ////===load cache driver===
            $CI->load->driver('cache');

            if ($CI->cache->$supported_cache->get('wallet_setting')) {
                return (object) $CI->cache->$supported_cache->get('wallet_setting');
            } else {
                $query = $CI->db->get("wallet_setting");
                $CI->cache->$supported_cache->save('wallet_setting', $query->row(), CACHE_VALID_SEC);
                return $query->row();
            }
        } else {
            $query = $CI->db->get("wallet_setting");
            return $query->row();
        }
    } else {
        $query = $CI->db->get("wallet_setting");
        return $query->row();
    }
    //////////====end cache part
}

/* * ** get gateway details by id
 * *
 * * */

function get_payment_gateway_details_by_id($gateway_id) {

    $CI = & get_instance();
    $query = $CI->db->get_where('payments_gateways', array('id' => $gateway_id));

    if ($query->num_rows() > 0) {
        return $query->row();
    }

    return 0;
}

/* * * get all currency code details
 *  return all record array
 * */

function get_currency() {
    $CI = & get_instance();
    $query = $CI->db->get('currency_code');
    return $query->result();
}

/* * *******detect amount and currency from content ** */

function findAmountAndCurrency($s, &$amount, &$currency) {

    $re_curr = "/[�\$��]+/";
    $cs = '';
    $currency = get_currency();
    if ($currency) {
        foreach ($currency as $cur) {
            $cs.=mb_convert_encoding(trim($cur->currency_symbol), 'UTF-8', 'HTML-ENTITIES');
        }
    }

    $re_curr = "/[$cs]+/";
    $re_amount = "/[0-9\.]+/";

    $amount = '';
    preg_match($re_amount, $s, $matches);

    if (isset($matches[0])) {
        $amount = floatval($matches[0]);
    }

    $currency = '';
    preg_match($re_curr, $s, $matches);
    if (isset($matches[0])) {
        $currency = $matches[0];
    }
}

/* * *******amount from content
 * * return amount,currency, currency_code array
 * * */

function get_amount_from_content($s) {
    $currencySymbols = array();
    $currencyCodes = array();
    //$currencySymbols = array('�'=>'GBP', '$'=>'USD','�'=>'EUR','kc'=>'CZK','kr'=>'NOK','Ft'=>'HUF','?'=>'ILS','�'=>'JPY','zt'=>'PLN','CHF'=>'CHF');

    $currency = get_currency();
    if ($currency) {
        foreach ($currency as $cur) {
            $currencySymbols[mb_convert_encoding(trim($cur->currency_symbol), 'UTF-8', 'HTML-ENTITIES')] = trim($cur->currency_code);
            $currencyCodes[trim($cur->currency_code)] = mb_convert_encoding(trim($cur->currency_symbol), 'UTF-8', 'HTML-ENTITIES');
        }
    }




    findAmountAndCurrency($s, $amount, $currency);

    $identifier = '';

    if (in_array(trim($currency), $currencyCodes)) {

        $code_index = array_keys($currencyCodes, $currency);
        if (isset($code_index[0])) {
            $identifier = $code_index[0];
        } elseif (array_key_exists(trim($currency), $currencySymbols)) {
            $identifier = $currencySymbols[$currency];
        }
    } elseif (array_key_exists(trim($currency), $currencySymbols)) {
        $identifier = $currencySymbols[$currency];
    } elseif (array_key_exists(trim($currency), $currencyCodes)) {
        $identifier = $currencyCodes[$currency];
    }
    /* 	echo("Amount: " . $amount . "<br/>");
      echo("Currency: " . $currency . "<br/>");
      echo("Identified Currency: " .  $identifier. "<br/>"); */

    $response = array(
        'amount' => $amount,
        'currency' => $currency,
        'currency_code' => $identifier
    );

    return $response;
}

/* * *******detect email address from content
 * * return email address array
 * * */

function get_emails($str) {
    $emails = array();
    preg_match_all("/\b\w+\@\w+[\.\w+]+\b/", $str, $output);

    if (isset($output[0])) {
        foreach ($output[0] as $email) {
            array_push($emails, strtolower($email));
        }

        if (count($emails) >= 1) {
            return $emails;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

/* * *******detect bad words from content
 * * return bad words array
 * * */

function BadWords($str) {

    $arrya = array();


    $temp['badwords'] = '';
    $temp['str'] = $str;


    $CI = & get_instance();


    $supported_cache = check_supported_cache_driver();

    if (isset($supported_cache)) {
        if ($supported_cache != '' && $supported_cache != 'none') {
            ////===load cache driver===
            $CI->load->driver('cache');

            if ($CI->cache->$supported_cache->get('spam_word')) {
                $arrya = (object) $CI->cache->$supported_cache->get('spam_word');
            } else {

                $query = $CI->db->query("select spam_word from " . $CI->db->dbprefix('spam_word') . " order by spam_word asc");

                if ($query->num_rows() > 0) {
                    $res = $query->result();

                    foreach ($res as $spm) {
                        $arrya[] = $spm->spam_word;
                    }

                    $CI->cache->$supported_cache->save('spam_word', $arrya, CACHE_VALID_SEC);
                }
            }
        } else {
            $query = $CI->db->query("select spam_word from " . $CI->db->dbprefix('spam_word') . " order by spam_word asc");

            if ($query->num_rows() > 0) {
                $res = $query->result();

                foreach ($res as $spm) {
                    $arrya[] = $spm->spam_word;
                }
            }
        }
    } else {

        $query = $CI->db->query("select spam_word from " . $CI->db->dbprefix('spam_word') . " order by spam_word asc");

        if ($query->num_rows() > 0) {
            $res = $query->result();

            foreach ($res as $spm) {
                $arrya[] = $spm->spam_word;
            }
        }
    }
    //////////====end cache part



    $str = strtolower($str);
    $cleanstr = $str;

    $badwords = array();



    /* $charlist = array("|3"=>"b","13"=>"b","l3"=>"b","|)"=>"d","1)"=>"d","[)"=>"d","|("=>"k","1("=>"k","$"=>"s","("=>"c","1"=>"i","+"=>"t","|"=>"i","!"=>"i","#"=>"h","<"=>"c","@"=>"a","0"=>"o","{"=>"c","["=>"c");

      foreach($charlist as $char=>$value)
      {
      $cleanstr = strtolower(str_replace($char, $value, $cleanstr));
      } */



    $badarray = $arrya;

    //   print_r($badarray);
    // die;

    if ($badarray) {
        foreach ($badarray as $naughty) {
            if (preg_match("/$naughty/", $str) or preg_match("/$naughty/", $cleanstr)) {
                //return true;
                $badwords[] = $naughty;
                $cleanstr = str_replace($naughty, '', $cleanstr);
            }
        }

        $temp['badwords'] = array_unique($badwords);
        $temp['str'] = $cleanstr;
        return $temp;
    }

    return $temp;
}

// end of bad word
//    Example :
//		 $str="you will fuck by him ,do not smash with them fucking jig";
//       $ret=BadWords($str);
//       if(is_array($ret))
//       {
//       echo "Some bad words detected like..".implode(",",$ret);
//       }
//	   





/* * ** get domain name from url
 * */

function get_domain_name($url) {
    $matches = parse_url($url);

    if (isset($matches['host'])) {
        $domain = $matches['host'];

        $domain = str_replace(array('www.'), '', $domain);

        return $domain;
    }

    return $url;
}

/* * * load captcha
 *  return captcha image
 * */

function load_captcha() {
    $CI = & get_instance();
    $CI->load->helper('captcha');

    $vals = array(
        'img_path' => './captcha/',
        'img_url' => base_url() . 'captcha/',
        'font_path' => base_path() . 'captcha/fonts/texb.ttf',
        'img_width' => '150',
        'img_height' => 30,
        'expiration' => 7200
    );

    $cap = create_captcha($vals);

    if ($cap) {
        $data = array(
            'captcha_id' => '',
            'captcha_time' => $cap['time'],
            'ip_address' => getRealIP(),
            'word' => $cap['word'],
        );
        $query = $CI->db->insert_string('captcha', $data);
        $CI->db->query($query);
    } else {
        return "Umm captcha not work";
    }
    return $cap['image'];
}

/* * * check captcha validation
 *  return boolen
 * */

function check_capthca() {
    $CI = & get_instance();
    // Delete old data ( 2hours)
    $expiration = time() - 7200;
    $sql = " DELETE FROM " . $CI->db->dbprefix('captcha') . " WHERE captcha_time < ? ";
    $binds = array($expiration);
    $query = $CI->db->query($sql, $binds);

    //checking input
    $sql = "SELECT COUNT(*) AS count FROM  " . $CI->db->dbprefix('captcha') . " WHERE word = ? AND ip_address = ? AND captcha_time > ?";
    $binds = array($_POST['captcha'], getRealIP(), $expiration);
    $query = $CI->db->query($sql, $binds);
    $row = $query->row();

    if ($row->count > 0) {
        return true;
    }
    return false;
}

/* * **  create seo friendly url 
 * var string $text
 * */

function clean_url($text) {

    $text = strtolower($text);
    $code_entities_match = array('&quot;', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '+', '{', '}', '|', ':', '"', '<', '>', '?', '[', ']', '', ';', "'", ',', '.', '_', '/', '*', '+', '~', '`', '=', ' ', '---', '--', '--', '�');
    $code_entities_replace = array('', '-', '-', '', '', '', '-', '-', '', '', '', '', '', '', '', '-', '', '', '', '', '', '', '', '', '', '-', '', '-', '-', '', '', '', '', '', '-', '-', '-', '-');
    $text = str_replace($code_entities_match, $code_entities_replace, $text);
    return $text;
}

/* * *** get all country list
 * **
 * */

function country_list() {

    $CI = & get_instance();


    $supported_cache = check_supported_cache_driver();

    if (isset($supported_cache)) {
        if ($supported_cache != '' && $supported_cache != 'none') {
            ////===load cache driver===
            $CI->load->driver('cache');

            if ($CI->cache->$supported_cache->get('country_list')) {
                return (object) $CI->cache->$supported_cache->get('country_list');
            } else {

                $CI->db->order_by('country_name', 'asc');
                $query = $CI->db->get_where("country", array('active' => 1));

                if ($query->num_rows() > 0) {
                    $CI->cache->$supported_cache->save('country_list', $query->result(), CACHE_VALID_SEC);
                    return $query->result();
                }

                return 0;
            }
        } else {
            $CI->db->order_by('country_name', 'asc');
            $query = $CI->db->get_where("country", array('active' => 1));

            if ($query->num_rows() > 0) {
                return $query->result();
            }

            return 0;
        }
    } else {
        $CI->db->order_by('country_name', 'asc');
        $query = $CI->db->get_where("country", array('active' => 1));

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return 0;
    }
    //////////====end cache part




    return 0;
}

/* * *** get all city list
 * **
 * */

function city_list() {

    $CI = & get_instance();


    $supported_cache = check_supported_cache_driver();

    if (isset($supported_cache)) {
        if ($supported_cache != '' && $supported_cache != 'none') {
            ////===load cache driver===
            $CI->load->driver('cache');

            if ($CI->cache->$supported_cache->get('city_list')) {
                return (object) $CI->cache->$supported_cache->get('city_list');
            } else {

                $CI->db->order_by('city_name', 'asc');
                $query = $CI->db->get_where("city", array('active' => 1));

                if ($query->num_rows() > 0) {
                    $CI->cache->$supported_cache->save('city_list', $query->result(), CACHE_VALID_SEC);
                    return $query->result();
                }

                return 0;
            }
        } else {
            $CI->db->order_by('city_name', 'asc');
            $query = $CI->db->get_where("city", array('active' => 1));

            if ($query->num_rows() > 0) {
                return $query->result();
            }

            return 0;
        }
    } else {
        $CI->db->order_by('city_name', 'asc');
        $query = $CI->db->get_where("city", array('active' => 1));

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return 0;
    }
    //////////====end cache part




    return 0;
}

/* * *  get  city id
 * var integer cityname
 * */

function getCityId($cityname) {
    $CI = & get_instance();

    $query = $CI->db->get_where("city", array('city_name' => $cityname));

    if ($query->num_rows() > 0) {
        $result = $query->row();
        return $result->city_id;
    }

    return 0;
}

/* * ** get city latitude and longitude
 * return single array of city detail
 * * */

function get_cityDetail($city_id) {
    $CI = & get_instance();

    $query = $CI->db->get_where("city", array('city_id' => $city_id));

    if ($query->num_rows() > 0) {
        return $query->row();
    }

    return 0;
}

function get_city_detail_from_name($cityname) {
    $CI = & get_instance();

    $query = $CI->db->get_where("city", array('city_name' => $cityname));

    if ($query->num_rows() > 0) {
        $result = $query->row();
        return $result;
    }

    return 0;
}

/* * ***  get  city name
 * var integer city_id
 * */

function getCityName($city_id) {
    $CI = & get_instance();

    $supported_cache = check_supported_cache_driver();

    if (isset($supported_cache)) {
        if ($supported_cache != '' && $supported_cache != 'none') {
            ////===load cache driver===
            $CI->load->driver('cache');

            if ($CI->cache->$supported_cache->get('city' . $city_id)) {
                return $CI->cache->$supported_cache->get('city' . $city_id);
            } else {
                $query = $CI->db->get_where("city", array('city_id' => $city_id));

                if ($query->num_rows() > 0) {
                    $result = $query->row();
                    $CI->cache->$supported_cache->save('city' . $city_id, $result->city_name, CACHE_VALID_SEC);

                    return $result->city_name;
                }


                return 0;
            }
        } else {
            $query = $CI->db->get_where("city", array('city_id' => $city_id));

            if ($query->num_rows() > 0) {
                $result = $query->row();
                return $result->city_name;
            }


            return 0;
        }
    } else {
        $query = $CI->db->get_where("city", array('city_id' => $city_id));

        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->city_name;
        }


        return 0;
    }
    //////////====end cache part






    return 0;
}

/* get all city of country */

function get_city_from_contry($country_id='') {
    $CI = & get_instance();
    $query = $CI->db->query("select * from " . $CI->db->dbprefix('city') . " where country_id =" . $country_id . " and active=1 and metro_city =1 group by city_id");
    if ($query->num_rows() > 0) {
        return $query->result();
    }

    return 0;
}

/* get all state from country */

function get_state_from_contry($country_id='') {
    $CI = & get_instance();
    $query = $CI->db->query("select * from " . $CI->db->dbprefix('state') . " where (country_id =" . $country_id . " and active=1) group by state_id");

    if ($query->num_rows() > 0) {
        return $query->result();
    }

    return 0;
}

/* get state info */

function get_state_id_from_statename($state_name='') {
    $CI = & get_instance();
    $query = $CI->db->get_where('state', array('state_name' => $state_name));
    if ($query->num_rows() > 0) {
        return $query->row();
    }

    return 0;
}

/* get city list from state */

function get_city_from_state($state_id='') {
    $CI = & get_instance();
    $query = $CI->db->query("select * from " . $CI->db->dbprefix('city') . " where state_id =" . $state_id . " and active=1 and metro_city =0");

    if ($query->num_rows() > 0) {
        return $query->result();
    }

    return 0;
}

/* get metro city */

function get_metro_city_list($state_id='') {
    $CI = & get_instance();
    $query = $CI->db->query("select * from " . $CI->db->dbprefix('city') . " where state_id =" . $state_id . " and active=1 and metro_city =1");

    if ($query->num_rows() > 0) {
        return $query->result();
    }

    return 0;
}

/* * ** get all category
 * ** return array of category
 * ** */

function get_all_category() {
    $CI = & get_instance();

    $supported_cache = check_supported_cache_driver();

    if (isset($supported_cache)) {
        if ($supported_cache != '' && $supported_cache != 'none') {
            ////===load cache driver===
            $CI->load->driver('cache');

            if ($CI->cache->$supported_cache->get('getallcategory')) {
                return (object) $CI->cache->$supported_cache->get('getallcategory');
            } else {
                $CI->db->order_by('category_name', 'asc');
                $query = $CI->db->get_where("design_category", array('category_status' => 1));

                if ($query->num_rows() > 0) {
                    $CI->cache->$supported_cache->save('getallcategory', $query->result(), CACHE_VALID_SEC);

                    return $query->result();
                }


                return 0;
            }
        } else {
            $CI->db->order_by('category_name', 'asc');
            $query = $CI->db->get_where("design_category", array('category_status' => 1));

            if ($query->num_rows() > 0) {
                return $query->result();
            }


            return 0;
        }
    } else {
        $CI->db->order_by('category_name', 'asc');
        $query = $CI->db->get_where("design_category", array('category_status' => 1));

        if ($query->num_rows() > 0) {
            return $query->result();
        }


        return 0;
    }
    //////////====end cache part



    return 0;
}

/* Video Category */

function get_parent_video_category() {
    $CI = & get_instance();


    $supported_cache = check_supported_cache_driver();

    /* 	if(isset($supported_cache))
      {

      if($supported_cache!='' && $supported_cache!='none')
      {
      ////===load cache driver===
      $CI->load->driver('cache');

      if($CI->cache->$supported_cache->get('getvideoparentcategory'))
      {
      return (object)$CI->cache->$supported_cache->get('getvideoparentcategory');
      }
      else
      {
      $query = $CI->db->get_where("video_category",array('category_status'=>1,'category_parent_id'=>0));

      if($query->num_rows()>0)
      {
      $CI->cache->$supported_cache->save('getvideoparentcategory', $query->result(),CACHE_VALID_SEC);

      return $query->result();
      }



      return 0;
      }

      }

      else
      {
      $query = $CI->db->get_where("video_category",array('category_status'=>1,'category_parent_id'=>0));

      if($query->num_rows()>0)
      {
      return $query->result();
      }


      return 0;
      }
      }
      else
      { */   //echo "343";die;
    $query = $CI->db->get_where("video_category", array('category_status' => 1, 'category_parent_id' => 0));

    if ($query->num_rows() > 0) {
        return $query->result();
    }



    return 0;
    //}
    //////////====end cache part
    //return 0;	
}

/* * ** get all parent category
 * ** return array of category
 * ** */

function get_parent_category() {
    $CI = & get_instance();


    /*$supported_cache = check_supported_cache_driver();

    if (isset($supported_cache)) {
        if ($supported_cache != '' && $supported_cache != 'none') {
            ////===load cache driver===
            $CI->load->driver('cache');

            if ($CI->cache->$supported_cache->get('getparentcategory')) {
                return (object) $CI->cache->$supported_cache->get('getparentcategory');
            } else {
                $query = $CI->db->get_where("design_category", array('category_status' => 1, 'category_parent_id' => 0));

                if ($query->num_rows() > 0) {
                    $CI->cache->$supported_cache->save('getparentcategory', $query->result(), CACHE_VALID_SEC);

                    return $query->result();
                }



                return 0;
            }
        } else {
            $query = $CI->db->get_where("design_category", array('category_status' => 1, 'category_parent_id' => 0));

            if ($query->num_rows() > 0) {
                return $query->result();
            }


            return 0;
        }
    } else {*/
        $query = $CI->db->get_where("design_category", array('category_status' => 1, 'category_parent_id' => 0));

        if ($query->num_rows() > 0) {
            return $query->result();
        }



        return 0;
    /*}*/

    //////////====end cache part


    return 0;
}

/* * * get category detail
 * *** $category_id
 * ** return array
 * * */

function get_category_detail_by_id($category_id) {
    $CI = & get_instance();

    $query = $CI->db->get_where('category', array('category_id' => $category_id));

    if ($query->num_rows() > 0) {
        return $query->row();
    }

    return 0;
}

/* * * get category detail
 * *** $category_id
 * ** return array
 * * */

function get_category_detail_by_url_name($category_url_name) {
    $CI = & get_instance();

    $query = $CI->db->get_where('category', array('category_url_name' => $category_url_name));

    if ($query->num_rows() > 0) {
        return $query->row();
    }

    return 0;
}

/* * *  get  duration between two dates
 * * $date ( any date), $task_id (task id) 
 * ** compare date difference with task city timezone
 * return string ago
 * */

function getDuration($date, $task_id='') {

    $CI = & get_instance();

    $curdate = date('Y-m-d H:i:s');

    if ($task_id != '' && $task_id > 0) {


        ///=====fetch city id from concept table put query here

        /* $city_detail=get_cityDetail($concept_detail->task_city_id);

          if($city_detail)
          {
          $dateTime = new DateTime("now", new DateTimeZone('America/Los_Angeles'));
          $task_timezone=tzOffsetToName($city_detail->city_timezone);
          $dateTimeZone = new DateTimeZone($task_timezone);
          $dateTime->setTimezone($dateTimeZone);
          $curdate= $dateTime->format("Y-m-d H:i:s");

          } */
    }



    $diff = abs(strtotime($date) - strtotime($curdate));
    $years = floor($diff / (365 * 60 * 60 * 24));
    $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
    $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
    $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 ) / (60 * 60));
    $mins = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / (60));

    $ago = '';
    if ($years != 0) {
        if ($years > 1) {
            $ago = $years . ' years';
        } else {
            $ago = $years . ' year';
        }
    } elseif ($months != 0) {
        if ($months > 1) {
            $ago = $months . ' months';
        } else {
            $ago = $months . ' month';
        }
    } elseif ($days != 0) {
        if ($days > 1) {
            $ago = $days . ' days';
        } else {
            $ago = $days . ' day';
        }
    } elseif ($hours != 0) {
        if ($hours > 1) {
            $ago = $hours . ' hours';
        } else {
            $ago = $hours . ' hour';
        }
    } else {
        if ($mins > 1) {
            $ago = $mins . ' minutes';
        } else {
            $ago = $mins . ' minute';
        }
    }
    return $ago . ' ago';
}

/* * *  get  duration between two dates in reverse count
 * * $start_date ( any date), $end_date (any date) 
 * return string of hours-minute-sec
 * */

function getReverseDuration($start_date, $end_date) {

    $auto_complete_date = $end_date;
    $task_timezone_date = $start_date;

    $dateg = $auto_complete_date;

    $date1 = $auto_complete_date;
    $date2 = $task_timezone_date;

    $diff = abs(strtotime($task_timezone_date) - strtotime($auto_complete_date));
    $test = floor($diff / (60 * 60 * 24));
    $str = array();

    $str['day'] = '';
    $str['hour'] = '';
    $str['minute'] = '';
    $str['second'] = '';


    $hours = 0;
    $minuts = 0;
    $dategg = $dateg;


    if (strtotime(date('Y-m-d H:i:s', strtotime($dateg))) > strtotime($task_timezone_date)) {

        //echo $date2;
        $diff2 = abs(strtotime($dategg) - strtotime($date2));
        $day1 = floor($diff2 / (60 * 60 * 24));


        $hours = floor(($diff2 - $day1 * 60 * 60 * 24) / (60 * 60));
        $minuts = floor(($diff2 - $day1 * 60 * 60 * 24 - $hours * 60 * 60) / 60);
        $seconds = floor(($diff2 - $day1 * 60 * 60 * 24 - $hours * 60 * 60 - $minuts * 60));

        $str['day'] = $day1;
        $str['hour'] = $hours;
        $str['minute'] = $minuts;
        $str['second'] = $seconds;
    }



    return $str;
}

/* * ******offset to timezone
 * **  var $offset
 * * return string timezone name
 * * */

function tzOffsetToName($offset, $isDst = null) {
    if ($isDst === null) {
        $isDst = date('I');
    }

    $offset *= 3600;
    $zone = timezone_name_from_abbr('', $offset, $isDst);

    if ($zone === false) {
        foreach (timezone_abbreviations_list() as $abbr) {
            foreach ($abbr as $city) {
                if ((bool) $city['dst'] === (bool) $isDst &&
                        strlen($city['timezone_id']) > 0 &&
                        $city['offset'] == $offset) {
                    $zone = $city['timezone_id'];
                    break;
                }
            }

            if ($zone !== false) {
                break;
            }
        }
    }

    return $zone;
}

/* * ******get original client ip
 * * return ip string
 * * */

function getRealIP() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

/* * *******get user location from ip
 * ** return array
 * * */

function getip2location($user_ip) {
    //$url = 'http://msapi.com/ipaddress_location/';
    $url = 'http://api.easyjquery.com/ips/?full=true&ip=';
    $url .=$user_ip;

    if (function_exists('curl_init')) {
        $defaults = array(
            CURLOPT_HEADER => 0,
            CURLOPT_URL => $url,
            CURLOPT_FRESH_CONNECT => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FORBID_REUSE => 1,
            CURLOPT_TIMEOUT => 15
        );

        $ch = curl_init();
        curl_setopt_array($ch, $defaults);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);
        return $response;
    } else if (function_exists('file_get_contents')) {

        $response = json_decode(file_get_contents($url));
        return $response;
    }

    return false;
}

function getImageHeight($image) {
    $size = getimagesize($image);
    $height = $size[1];
    return $height;
}

//You do not need to alter these functions
function getImageWidth($image) {
    $size = getimagesize($image);
    $width = $size[0];
    return $width;
}

/**
 * @param DateTime $date A given date
 * @param int $firstDay 0-6, Sun-Sat respectively
 * @return DateTime
 */
function get_first_day_of_week($date) {
    $day_of_week = date('N', strtotime($date));
    $week_first_day = date('Y-m-d', strtotime($date . " - " . ($day_of_week - 1) . " days"));
    return $week_first_day;
}

function get_last_day_of_week($date) {
    $day_of_week = date('N', strtotime($date));
    $week_last_day = date('Y-m-d', strtotime($date . " + " . (7 - $day_of_week) . " days"));
    return $week_last_day;
}

function get_currency_symbol($currency_code_id='') {

    $CI = & get_instance();
    $query = $CI->db->get_where("currency_code", array('currency_code_id' => $currency_code_id));
    $res = $query->row();
    return $res->currency_symbol;
}

function get_back_color_city($city_name='') {
    $CI = & get_instance();
    $query = $CI->db->get_where("city", array('city_name' => $city_name));
    $res = $query->row();
    if ($res) {
        return $res->city_color;
    } else {
        return '#FFFFFF';
    }
}

function get_alldesign($design_id=0) {
    $CI = & get_instance();

    $CI->db->select('*');
    $CI->db->from('design d');
    $CI->db->join('user us', 'd.user_id=us.user_id', 'left');
    $CI->db->join('user_profile up', 'us.user_id=up.user_id', 'left');
    $CI->db->where('d.design_status', '1');


    if ($design_id > 0) {
        $CI->db->where('d.design_id !=', $design_id);
    }

    $query = $CI->db->get();


    if ($query->num_rows() > 0) {
        return $query->result();
    }
    return 0;
}

function get_allvideo($video_id=0) {
    $CI = & get_instance();

    $CI->db->select('*');
    $CI->db->from('video v');
    $CI->db->join('user us', 'v.user_id=us.user_id', 'left');
    $CI->db->join('user_profile up', 'us.user_id=up.user_id', 'left');
    $CI->db->where('v.video_status', '1');


    if ($video_id > 0) {
        $CI->db->where('v.video_id !=', $video_id);
    }

    $query = $CI->db->get();


    if ($query->num_rows() > 0) {
        return $query->result();
    }
    return 0;
}

function get_design_name($design_id='') {
    $CI = & get_instance();
    $query = $CI->db->get_where("design", array('design_id' => $design_id));
    if ($query->num_rows() > 0) {
        $res = $query->row();
        return $res->design_title;
    }
    return 0;
}

function get_video_name($video_id='') {
    $CI = & get_instance();
    $query = $CI->db->get_where("video", array('video_id' => $video_id));
    if ($query->num_rows() > 0) {
        $res = $query->row();
        return $res->video_title;
    }
    return 0;
}

function get_modified_design($design_id) {

    $CI = & get_instance();
    $query = $CI->db->query("select * from " . $CI->db->dbprefix('design') . " where design_status = 1 and design_id !=" . $design_id);
    if ($query->num_rows() > 0) {
        return $query->result();
    }
    return 0;
}

function get_modified_video($video_id) {
    $CI = & get_instance();
    $query = $CI->db->query("select * from " . $CI->db->dbprefix('video') . " where video_status = 1 and video_id !=" . $video_id);
    if ($query->num_rows() > 0) {
        return $query->result();
    }
    return 0;
}

function check_tag($tag_name='') {
    $CI = & get_instance();
    $query = $CI->db->get_where("tags", array('LOWER(tag_name)' => strtolower($tag_name)));
    if ($query->num_rows() > 0) {
        $res = $query->row();
        return $res->tag_id;
    } else {
        $data = array('tag_name' => $tag_name,
            'tag_date' => date('Y-m-d h:i:s'),
            'tag_ip' => getRealIP(),
            'tag_status' => 1);
        $CI->db->insert('tags', $data);
        $tag_id = mysql_insert_id();
        return $tag_id;
    }
}

function seturl($url) {

    if ($url != '') {

        if (substr_count($url, 'http://') >= 1) {
            $url = $url;
        } elseif (substr_count($url, 'https://') >= 1) {
            $url = $url;
        } else {
            $url = str_replace(array(':', '/', '//', '::'), '', $url);
            $url = 'http://' . $url;
        }

        return $url;
    } else {
        return false;
    }
}

function get_category_slug($str, $id=0) {
    $CI = & get_instance();
    $slug = url_title($str, 'dash', true);
    if ($id != 0) {
        $query = $CI->db->get_where("design_category", array('category_url_name' => $slug, 'category_id' => $id));
        if ($query->num_rows() > 0) {
            if ($query->num_rows() == 1) {
                return $slug;
            } else {
                return $slug . ($query->num_rows() + 1);
            }
        } else {
            return $slug;
        }
    } else {
        $query = $CI->db->get_where("design_category", array('category_url_name' => $slug));
        if ($query->num_rows() > 0) {
            return $slug . ($query->num_rows() + 1);
        }

        return $slug;
    }
}

function get_design_slug($str, $id=0) {
    $CI = & get_instance();
    $slug = url_title($str, 'dash', true);
    if ($id != 0) {
        $query = $CI->db->get_where("design", array('design_slug' => $slug, 'design_id' => $id));
        if ($query->num_rows() > 0) {
            if ($query->num_rows() == 1) {
                return $slug;
            } else {
                return $slug . ($query->num_rows() + 1);
            }
        } else {
            return $slug;
        }
    } else {
        $query = $CI->db->get_where("design", array('design_slug' => $slug));
        if ($query->num_rows() > 0) {
            return $slug . ($query->num_rows() + 1);
        }
        return $slug;
    }
}

function get_video_slug($str, $id=0) {
    $CI = & get_instance();
    $slug = url_title($str, 'dash', true);
    if ($id != 0) {
        $query = $CI->db->get_where("video", array('video_slug' => $slug, 'video_id' => $id));
        if ($query->num_rows() > 0) {
            if ($query->num_rows() == 1) {
                return $slug;
            } else {
                return $slug . ($query->num_rows() + 1);
            }
        } else {
            return $slug;
        }
    } else {
        $query = $CI->db->get_where("video", array('video_slug' => $slug));
        if ($query->num_rows() > 0) {
            return $slug . ($query->num_rows() + 1);
        }
        return $slug;
    }
}

function get_tag_rel_count($tag_id, $str='') {
    $CI = & get_instance();

    if ($str == 'design') {
        $query = $CI->db->get_where("design_tags", array('tag_id' => $tag_id));
    } else {
        $query = $CI->db->get_where("video_tags", array('tag_id' => $tag_id));
    }


    if ($query->num_rows() > 0) {
        return $query->num_rows();
    } else {
        return 0;
    }
}

function check_design_exist($design_slug) {
    $CI = & get_instance();
    $query = $CI->db->get_where("design", array('design_slug' => $design_slug));
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

function check_challenge_exist($design_slug) {
    $CI = & get_instance();
    $query = $CI->db->get_where("challenge", array('challenge_slug' => $design_slug));
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

function check_video_exist($video_slug) {
    $CI = & get_instance();
    $query = $CI->db->get_where("video", array('video_slug' => $video_slug));
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

function get_user_unread_notification($user_id) {
    $CI = & get_instance();
    $query = $CI->db->get_where("system_notification", array('to_user_id' => $user_id, 'is_read' => 1));
    if ($query->num_rows() > 0) {
        return $query->num_rows();
    } else {
        return false;
    }
}

function check_for_treepay($transaction_id) {
    $CI = & get_instance();
    $query = $CI->db->get_where("transaction", array('transaction_id' => $transaction_id, 'parent_transaction_id > ' => 0));
    if ($query->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}

function get_allchallenge() {
    $CI = & get_instance();
    $query = $CI->db->get_where("challenge", array('challenge_status' => 1));
    if ($query->num_rows() > 0) {
        return $query->result();
    }
    return 0;
}

function get_challenge_win_info($challenge_id=0) {
    $CI = & get_instance();
    $query = $CI->db->get_where("challenge_bid", array('challenge_id' => $challenge_id, 'is_won ' => 1));
    if ($query->num_rows() > 0) {
        return $query->row();
    }
    return 0;
}

function get_challenge_slug($str, $id=0) {
    $CI = & get_instance();
    $slug = url_title($str, 'dash', true);
    if ($id != 0) {
        $query = $CI->db->get_where("challenge", array('challenge_slug' => $slug, 'challenge_id' => $id));
        if ($query->num_rows() > 0) {
            if ($query->num_rows() == 1) {
                return $slug;
            } else {
                return $slug . ($query->num_rows() + 1);
            }
        } else {
            return $slug;
        }
    } else {
        $query = $CI->db->get_where("challenge", array('challenge_slug' => $slug));
        if ($query->num_rows() > 0) {
            return $slug . ($query->num_rows() + 1);
        }
        return $slug;
    }
}

function get_parent_challenge_category() {
    $CI = & get_instance();


    $supported_cache = check_supported_cache_driver();

    if (isset($supported_cache)) {
        if ($supported_cache != '' && $supported_cache != 'none') {
            ////===load cache driver===
            $CI->load->driver('cache');

            if ($CI->cache->$supported_cache->get('getchallengeparentcategory')) {
                return (object) $CI->cache->$supported_cache->get('getchallengeparentcategory');
            } else {
                $query = $CI->db->get_where("challenge_category", array('category_status' => 1, 'category_parent_id' => 0));

                if ($query->num_rows() > 0) {
                    $CI->cache->$supported_cache->save('getchallengeparentcategory', $query->result(), CACHE_VALID_SEC);

                    return $query->result();
                }



                return 0;
            }
        } else {
            $query = $CI->db->get_where("challenge_category", array('category_status' => 1, 'category_parent_id' => 0));

            if ($query->num_rows() > 0) {
                return $query->result();
            }


            return 0;
        }
    } else {
        $query = $CI->db->get_where("challenge_category", array('category_status' => 1, 'category_parent_id' => 0));

        if ($query->num_rows() > 0) {
            return $query->result();
        }



        return 0;
    }

    //////////====end cache part


    return 0;
}

/* * ** get all parent category
 * ** return array of category
 * ** */

function get_category() {
    $CI = & get_instance();

    $query = $CI->db->get_where("challenge_category", array('category_status' => 1, 'category_parent_id' => 0));

    if ($query->num_rows() > 0) {
        return $query->result();
    }

    return 0;
}

/* * ** get all sub category
 * **  integer $pid (parent category id)
 * ** return array of category
 * ** */

function sub_category($pid) {
    $CI = & get_instance();

    $query = $CI->db->get_where("challenge_category", array('category_parent_id' => $pid));

    if ($query->num_rows() > 0) {
        return $query->result();
    }
    return 0;
}

/* End of file custom_helper.php */
/* Location: ./system/application/helpers/custom_helper.php */

/* End of file custom_helper.php */
/* Location: ./system/application/helpers/custom_helper.php */

function get_parent_transaction_challenge($cid =0) {
    $CI = & get_instance();
    $qry = $CI->db->get_where("transaction", array("challenge_id" => $cid, "is_authorize" => "1"));

    if ($qry->num_rows() > 0) {
        $rs = $qry->row();
        return $rs->transaction_id;
    }

    return 0;
}

function get_total_design() {
    $CI = & get_instance();

    $CI->db->select('*');
    $CI->db->from('design dg');
    $CI->db->join('user us', 'dg.user_id=us.user_id', 'left');
    $CI->db->join('user_profile up', 'us.user_id=up.user_id', 'left');
    $CI->db->where('dg.user_id', get_authenticateUserID());

    $query = $CI->db->get();

    if ($query->num_rows() > 0) {
        return $query->num_rows();
    }

    return 0;
}

function get_total_video() {
    $CI = & get_instance();
    $CI->db->select('*');
    $CI->db->from('video vd');
    $CI->db->join('user us', 'vd.user_id=us.user_id', 'left');
    $CI->db->join('user_profile up', 'us.user_id=up.user_id', 'left');
    $CI->db->where('vd.user_id', get_authenticateUserID());
    $query = $CI->db->get();

    if ($query->num_rows() > 0) {
        return $query->num_rows();
    }

    return 0;
}

function get_total_challenge() {
    $CI = & get_instance();
    $CI->db->select('*');
    $CI->db->from('challenge ch');
    $CI->db->join('user us', 'ch.user_id=us.user_id', 'left');
    $CI->db->join('user_profile up', 'us.user_id=up.user_id', 'left');
    $CI->db->where('ch.user_id', get_authenticateUserID());
    $query = $CI->db->get();

    if ($query->num_rows() > 0) {
        return $query->num_rows();
    }

    return 0;
}

function get_total_store() {
    $CI = & get_instance();
    $CI->db->select('*');
    $CI->db->from('store st');
    $CI->db->join('user us', 'st.user_id=us.user_id', 'left');
    $CI->db->join('user_profile up', 'us.user_id=up.user_id', 'left');
    $CI->db->where('st.user_id', get_authenticateUserID());
    $query = $CI->db->get();

    if ($query->num_rows() > 0) {
        return $query->num_rows();
    }

    return 0;
}

function bid_counter($id = 0) {
    $CI = & get_instance();
    $qry = $CI->db->get_where("challenge_bid", array("challenge_id" => $id));
    return $qry->num_rows();
}

function get_category_name($category_id) {
    $CI = & get_instance();
    $query = $CI->db->get_where("challenge_category", array("category_id" => $category_id, 'category_status' => 1));
    if ($query->num_rows() > 0) {
        return $query->row();
    }
}

function check_user_bid($user_id = 0, $challenge_id = 0) {
    $CI = & get_instance();
    $query = $CI->db->get_where("challenge_bid", array("challenge_id" => $challenge_id, 'user_id' => $user_id));
    if ($query->num_rows() > 0) {
        return 1;
    }

    return 0;
}

function get_category_obj_by_id($type='design',$category_id) {
    $CI = & get_instance();
      
    $query = $CI->db->query("select * from " . $CI->db->dbprefix($type.'_category') . " where (category_status=1 or (category_status=0 and category_user_id='".get_authenticateUserID()."') ) and category_id='" . $category_id . "'");
        
        if ($query->num_rows() > 0) {
            return $query->row();
        }
        return false;
        
        
}


function gallery_counter_logic(){
    $CI = & get_instance();
    
    $query = $CI->db->query("select gallery_id from " . $CI->db->dbprefix('gallery') . " where gallery_status=1  and gallery_cnt>0 order by gallery_order asc limit 1");
        
        if ($query->num_rows() > 0) {
            $result= $query->row();
            return $result->gallery_id;
        } else {
            
            
             $query = $CI->db->query("select gallery_id from " . $CI->db->dbprefix('gallery') . " where gallery_status=1 order by gallery_order asc limit 1");
        
            if ($query->num_rows() > 0) {
                $result= $query->row();
                return $result->gallery_id;
            }
            
        }
        return 0;
}

function gallery_counter_update($counter_id){
    $CI = & get_instance();
     $query = $CI->db->query("select gallery_id from " . $CI->db->dbprefix('gallery') . " g1 where g1.gallery_id != '".$counter_id."' and g1.gallery_order>(select g2.gallery_order from " . $CI->db->dbprefix('gallery') . " g2 where g2.gallery_id=".$counter_id.") and g1.gallery_status=1  and g1.gallery_cnt=0 order by g1.gallery_order asc limit 1");
        
        $new_counter_id=0;
        if ($query->num_rows() > 0) {
            $result= $query->row();
            $new_counter_id= $result->gallery_id;
        } else {
             $query = $CI->db->query("select gallery_id from " . $CI->db->dbprefix('gallery') . " where gallery_status=1 order by gallery_order asc limit 1");
        
            if ($query->num_rows() > 0) {
                $result= $query->row();
                 $new_counter_id= $result->gallery_id;
            }
        }
        
       
       if($new_counter_id>0){
           $CI->db->query("update " . $CI->db->dbprefix('gallery') . " set gallery_cnt=0  where gallery_id>0 ");
           $CI->db->query("update " . $CI->db->dbprefix('gallery') . " set gallery_cnt=1  where gallery_id=".$new_counter_id);
       }
       
        
}


function get_counter_detail($counter_id){
    
    $CI = & get_instance();
    $query = $CI->db->query("select * from " . $CI->db->dbprefix('gallery') . " where gallery_id = '".$counter_id."' and gallery_status=1 order by gallery_order asc limit 1");
        
            if ($query->num_rows() > 0) {
                return $query->row();                 
            }
            
            return false;
}

function addhttp($url) {
                            $parsed = parse_url($url);
                            if (empty($parsed['scheme'])) {
                                $url = 'http://' . ltrim($url, '/');
                            }
                            return $url;
                        }
                        
                        
    function roundDownToHalf($number) {
      
        if($number>=0.5){
            $remainder = ($number * 10) % 10;


            if($remainder <=3) {
                $half = 0;
            } else if($remainder > 3 && $remainder <= 7){
                $half = 0.5;
            } else {
                $half = 1;
            }
            $value = floatval(intval($number) + $half);

        } else {
           if($number>0 && $number<0.5){
            $value=0.5;
           } else {
               $value=0;
           }
            
        }
        
        
        
        return $value;
        //return number_format($value, 1, '.', '');
    }

    
    function check_user_rate_design($design_id,$user_id){
        
        
        $CI = & get_instance();
    $query = $CI->db->query("select * from " . $CI->db->dbprefix('design_review') . " where design_id = '".$design_id."' and user_id='".$user_id."' order by design_review_id desc limit 1");
        
            if ($query->num_rows() > 0) {
                return true;             
            }
            
            return false;
        
        
    }
    
    
    function check_user_rate_video($video_id,$user_id){
        
        
        $CI = & get_instance();
    $query = $CI->db->query("select * from " . $CI->db->dbprefix('video_review') . " where video_id = '".$video_id."' and user_id='".$user_id."' order by video_review_id desc limit 1");
        
            if ($query->num_rows() > 0) {
                return true;             
            }
            
            return false;
        
        
    }
    
      function get_design_total_user_rate($design_id)
    {
           $CI = & get_instance();
         $sql="SELECT design_review_id from ".$CI->db->dbprefix('design_review')." where parent_review_id=0  and design_id=".$design_id;
         
         $query = $CI->db->query($sql);
        
        if($query->num_rows()>0){
        
            return $query->num_rows();    
        }
        return 0;
      
      
    }
    
    function get_video_total_user_rate($video_id)
    {
         $CI = & get_instance();
         $sql="SELECT video_review_id from ".$CI->db->dbprefix('video_review')." where parent_review_id=0  and video_id=".$video_id;
         
         $query = $CI->db->query($sql);
        
        if($query->num_rows()>0){
        
             return $query->num_rows();    
      
        }
        return 0;
      
      
    }
?>