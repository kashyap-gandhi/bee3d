<?php
function get_one_trip($trip_id)
	{
		$CI =& get_instance();
		
		$query=$CI->db->query("select * from ".$CI->db->dbprefix('trip')." where trip_id='".$trip_id."' and trip_status=1");	
		
		if($query->num_rows()>0)
		{		
			return $query->row();
		}
		
		return 0;
	}
	
	
	function get_total_bid_on_offer($trip_id)
	{
		$CI =& get_instance();
		
		$query=$CI->db->query("select * from ".$CI->db->dbprefix('trip_offer')." where trip_id='".$trip_id."'");	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;	
	}
	
	
	function get_today_trips($user_id,$limit,$offset = 0){
	
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('trip tp');
		$CI->db->join('user us','tp.user_id=us.user_id');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('tp.trip_status',1);
		$CI->db->where('tp.trip_activity_status',0);
		$CI->db->where('tp.trip_agent_assign_id',0);
		$CI->db->where('tp.user_id',$user_id);		
		$CI->db->where('DATE(tp.trip_added_date)',date('Y-m-d'));
		$CI->db->where('tp.trip_confirm_date >= ',date('Y-m-d H:i:s'));
		$CI->db->order_by('tp.trip_added_date','desc');	
		$CI->db->limit($limit,$offset);	
		$query=$CI->db->get();

		if($query->num_rows()>0)
		{
			return $query->result();
		}
		return 0;
	}
	
	function get_yesterday_trips($user_id,$limit,$offset = 0){
	
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('trip tp');
		$CI->db->join('user us','tp.user_id=us.user_id');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('tp.trip_status',1);
		$CI->db->where('tp.trip_activity_status',0);
		$CI->db->where('tp.trip_agent_assign_id',0);
		$CI->db->where('tp.user_id',$user_id);		
		$CI->db->where('DATE(tp.trip_added_date) < ',date('Y-m-d'));
		$CI->db->where('tp.trip_confirm_date >= ',date('Y-m-d H:i:s'));
		$CI->db->order_by('tp.trip_added_date','desc');	
		$CI->db->limit($limit,$offset);		
		$query=$CI->db->get();

		if($query->num_rows()>0)
		{
			return $query->result();
		}
		return 0;


	}
	
	function count_trip_proposal($trip_id='')
	{
	   $CI =& get_instance();
	   $CI->db->select('*');
	   $CI->db->from('trip tp');
	   $CI->db->join('trip_offer tof','tp.trip_id=tof.trip_id');
	   $CI->db->join('trip_offer_attachment tat','tof.trip_offer_id=tat.trip_offer_id');
	   $CI->db->where('tp.trip_id',$trip_id);
	   $CI->db->where('tof.offer_status_for_trip_owner',0);
	    $CI->db->group_by('tof.trip_offer_id');                 
	   $CI->db->order_by('tof.offer_date',"desc");                  
	  
	   $query=$CI->db->get();
	 
	   if($query->num_rows()>0)
	   {
	     return $query->num_rows();
	   }
	   else
	   {
			return 0;
		}
	
	
	}
	
	function count_hidden_proposal($trip_id='')
	{
	   $CI =& get_instance();
	   $CI->db->select('*');
	   $CI->db->from('trip tp');
	   $CI->db->join('trip_offer tof','tp.trip_id=tof.trip_id');
	   $CI->db->join('trip_offer_attachment tat','tof.trip_offer_id=tat.trip_offer_id');
	   $CI->db->where('tp.trip_id',$trip_id);
	   $CI->db->where('tof.offer_status_for_trip_owner',1);
	   $CI->db->group_by('tof.trip_offer_id');                  
	   $CI->db->order_by('tof.offer_date',"desc");                  
	  
	   $query=$CI->db->get();
	 
	   if($query->num_rows()>0)
	   {
	     return $query->num_rows();
	   }
	   else
	   {
			return 0;
		}
	
	
	}
	
	function count_decline_proposal($trip_id='')
	{
	   $CI =& get_instance();
	   $CI->db->select('*');
	   $CI->db->from('trip tp');
	   $CI->db->join('trip_offer tof','tp.trip_id=tof.trip_id');
	   $CI->db->join('trip_offer_attachment tat','tof.trip_offer_id=tat.trip_offer_id');
	   $CI->db->where('tp.trip_id',$trip_id);
	   $CI->db->where('tof.offer_status_for_trip_owner',2);
	    $CI->db->group_by('tof.trip_offer_id');                 
	   $CI->db->order_by('tof.offer_date',"desc");                  
	  
	   $query=$CI->db->get();
	 
	   if($query->num_rows()>0)
	   {
	     return $query->num_rows();
	   }
	   else
	   {
			return 0;
		}
	
	
	}
	
	function count_invited_proposal($trip_id='')
	{
	 
	    $CI =& get_instance();
	   $CI->db->select('*');
	   $CI->db->from('trip_agent_invite ti');
	   $CI->db->join('trip tp','ti.invite_by_user_id=tp.user_id');
	   $CI->db->join('trip_offer tof','ti.invite_to_user_id=tof.user_id','left');
	   $CI->db->join('trip_offer_attachment tat','tof.trip_offer_id=tat.trip_offer_id','left');
	   $CI->db->where('ti.trip_id',$trip_id);
	   $CI->db->group_by('ti.trip_agent_invite_id');                  
	   $CI->db->order_by('ti.invite_date',"desc");                  
	  
	   $query=$CI->db->get();
	   if($query->num_rows()>0)
	   {
	   
	     return $query->num_rows();
	   }
	   else
	   {
			return 0;
		}
	}
	
?>