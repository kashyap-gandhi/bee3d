<?php

class Site_setting_model extends CI_Model {
	
    function Site_setting_model()
    {
        parent::__construct();	
    }   
	
	
	/*** admin site setting update function
	* var integer $site_setting_id
	* var integer $site_online
	* var integer $captcha_enable
	* var string $site_name
	* var integer $site_version
	* var integer $site_language
	* var string $currency_code
	* var string $date_format
	* var string $time_format
	* var string $site_tracker
	* var text $how_it_works_video
	* var integer $zipcode_min
	* var integer $zipcode_max
	**/
	function site_setting_update()
	{
		$currency = $this->db->get_where('currency_code',array('currency_code'=>$this->input->post('currency_code')));
		$cur=$currency->row();
		$currency_symbol=$cur->currency_symbol;
		//echo $this->input->post('site_setting_id');
		//echo '<pre>'; print_r($_POST);
		
		//'site_language' => $this->input->post('site_language'),	
			//'how_it_works_video' => $this->input->post('how_it_works_video'),
			//'zipcode_min' => $this->input->post('zipcode_min'),	
			//'zipcode_max' => $this->input->post('zipcode_max'),
			//'default_longitude'=>trim($this->input->post('default_longitude')),
			//'default_latitude'=>trim($this->input->post('default_latitude')),
			//'google_map_key'=>trim($this->input->post('google_map_key')),
			//'site_video'=>$this->input->post('site_video')
	
		
		
		$data = array(	
			'site_online' => $this->input->post('site_online'),
			'captcha_enable' => $this->input->post('captcha_enable'),		
			'site_name' => $this->input->post('site_name'),	
			'site_offline_title' => $this->input->post('site_offline_title'),	
			'site_offline_desc' => $this->input->post('site_offline_desc'),				
			'currency_symbol' => $currency_symbol,
			'currency_code' => $this->input->post('currency_code'),
			'date_format' => $this->input->post('date_format'),	
			'time_format' => $this->input->post('time_format'),
			'date_time_format' => $this->input->post('date_time_format'),
			'site_tracker' => $this->input->post('site_tracker'),		
			'activate_email_upload'=>$this->input->post('activate_email_upload'),
			'site_timezone' => $this->input->post('site_timezone'),			
			'site_email' => $this->input->post('site_email'),
			'google_plus_link' => $this->input->post('google_plus_link'),
			'pinterest_link' => $this->input->post('pinterest_link'),
                    'facebook_link' => $this->input->post('facebook_link'),
                    'twitter_link' => $this->input->post('twitter_link'),
                    'linkedin_link' => $this->input->post('linkedin_link'),
			'captcha_public_key' => $this->input->post('captcha_public_key'),
			'captcha_private_key' => $this->input->post('captcha_private_key'),
			
		);
		//print_r($data);// die();
		
		$this->db->where('site_setting_id',$this->input->post('site_setting_id'));
		$this->db->update('site_setting',$data);
		
		
		
		
		$supported_cache=check_supported_cache_driver();
		
		if(isset($supported_cache))
		{
			if($supported_cache!='' && $supported_cache!='none')
			{
				////===load cache driver===
				$this->load->driver('cache');				
				
				$query = $this->db->get("site_setting");					
				$this->cache->$supported_cache->save('site_setting', $query->row(),CACHE_VALID_SEC);		
				
			}			
			
		}
		
		
		
		
		
		
		
	}
	
	/*** get image size setting details
	*  return single record array
	**/
	function get_one_img_setting()
	{
		$query = $this->db->get_where('image_setting');
		return $query->row();
	}
	
	/*** admin image size setting update function
	* var integer $p_width
	* var integer $p_height
	* var integer $u_width
	* var integer $u_height
	**/
	function img_setting_update()
	{
		$data = array(		
			'user_width' => $this->input->post('user_width'),	
			'user_height' => $this->input->post('user_height'),
			
			'user_small_thumb_width' => $this->input->post('user_small_thumb_width'),
			'user_small_thumb_height' => $this->input->post('user_small_thumb_height'),
                    
                    'category_width' => $this->input->post('category_width'),
			'category_height' => $this->input->post('category_height'),
			
			'gallery_thumb_width' => $this->input->post('gallery_thumb_width'),
			'gallery_thumb_height' => $this->input->post('gallery_thumb_height'),
			'design_width' => $this->input->post('design_width'),
			'design_height' => $this->input->post('design_height')
		);
		$this->db->where('image_setting_id',$this->input->post('image_setting_id'));
		$this->db->update('image_setting',$data);
	}
	
        
        
        /*** get commission setting details
	*  return single record array
	**/
	function get_commission_setting()
	{
		$query = $this->db->get_where('commission_setting');
		return $query->row();
	}
	
	/*** admin commission setting update function
	**/
	function commission_setting_update()
	{
		$data = array(		
			'admin_commission_image' => $this->input->post('admin_commission_image'),	
			'admin_commission_video' => $this->input->post('admin_commission_video'),			
			'admin_commission_challange' => $this->input->post('admin_commission_challange')
		);
		$this->db->where('commission_id',$this->input->post('commission_id'));
		$this->db->update('commission_setting',$data);
	}
        
        
        
        
        
          /*** get commission setting details
	*  return single record array
	**/
	function get_credit_setting()
	{
		$query = $this->db->get_where('credit_setting');
		return $query->row();
	}
	
	/*** admin commission setting update function
	**/
	function credit_setting_update()
	{
		$data = array(		
			'point2paisa' => $this->input->post('point2paisa')
		);
		$this->db->where('credit_setting_id',$this->input->post('credit_setting_id'));
		$this->db->update('credit_setting',$data);
	}
	
	
}
?>