<?php

class Wallet_model  extends CI_Model  {
	
    function Wallet_model()
    {
       /* parent::Model();
		$this->load->helper('url');
		$this->load->database();*/
		parent::__construct();	
    }  
	
	
	function wallet_setting()
	{
		$query=$this->db->get_where('wallet_setting');
		return $query->row();
			
	}
	
	
	
	
	function user_current_wallet_amount($uid) 
	{
		 $query = $this->db->query("SELECT SUM(debit) as sumd,SUM(credit) as sumc FROM ".$this->db->dbprefix('wallet')." where is_authorize!=1 and user_id='".$uid."'"); 
	 
	 
	 		if($query->num_rows()>0)
			{
			
				 $result = $query->row();
			
				 $debit=$result->sumd;
				 $credit=$result->sumc;
				
				 $total=$credit-$debit;
				
				return $total;
			
			}
			
			return 0;
	}	
	
	function get_total_search_withdraw_count($option,$keyword)
	{
		if($option=='full_name')
		{
			$option='us.full_name';
		}
		if($option=='email')
		{
			$option='us.email';
		}
		if($option=='withdraw_method')
		{
			$option='wt.withdraw_method';
		}
		if($option=='withdraw_status')
		{
			$option='wt.withdraw_status';
		}
		if($option=='withdraw_ip')
		{
			$option='wt.withdraw_ip';
		}
		
		
		
		
		
		$query=$this->db->query("select wt.*, us.full_name,us.last_name,us.email from ".$this->db->dbprefix('wallet_withdraw')." wt left join ".$this->db->dbprefix('user')." us on wt.user_id=us.user_id where ".$option." like '%".$keyword."%'  order by  wt.withdraw_status desc, wt.withdraw_id desc");
		
		if($query->num_rows()>0)
		{	
			return $query->num_rows();
		}
		
		return 0;
	}
	
	
	function get_search_withdraw_result($option,$keyword,$offset, $limit)
	{
		
		if($option=='full_name')
		{
			$option='us.full_name';
		}
		if($option=='email')
		{
			$option='us.email';
		}
		if($option=='withdraw_method')
		{
			$option='wt.withdraw_method';
		}
		if($option=='withdraw_status')
		{
			$option='wt.withdraw_status';
		}
		if($option=='withdraw_ip')
		{
			$option='wt.withdraw_ip';
		}
	
		$query=$this->db->query("select wt.*, us.full_name,us.last_name,us.email from ".$this->db->dbprefix('wallet_withdraw')." wt left join ".$this->db->dbprefix('user')." us on wt.user_id=us.user_id where ".$option." like '%".$keyword."%' order by wt.withdraw_status desc, wt.withdraw_id desc limit ".$limit." offset ".$offset);
		
		if($query->num_rows()>0)
		{	
			return $query->result();
		}
		
		return 0;
	
	}
	
	
	
	
	function get_total_withdraw_count()
	{
		$query=$this->db->query("select wt.*, us.full_name,us.last_name,us.email from ".$this->db->dbprefix('wallet_withdraw')." wt left join ".$this->db->dbprefix('user')." us on wt.user_id=us.user_id  order by  wt.withdraw_status desc, wt.withdraw_id desc");
		
		if($query->num_rows()>0)
		{	
			return $query->num_rows();
		}
		
		return 0;
	}
	
	
	function get_withdraw_result($offset, $limit)
	{
		$query=$this->db->query("select wt.*, us.full_name,us.last_name,us.email from ".$this->db->dbprefix('wallet_withdraw')." wt left join ".$this->db->dbprefix('user')." us on wt.user_id=us.user_id  order by wt.withdraw_status desc, wt.withdraw_id desc limit ".$limit." offset ".$offset);
		
		if($query->num_rows()>0)
		{	
			return $query->result();
		}
		
		return 0;
	
	}
	
	
	
	function get_withdraw_detail($id)
	{
		$query=$this->db->query("select wt.*,us.full_name,us.last_name,us.email from ".$this->db->dbprefix('wallet_withdraw')." wt left join ".$this->db->dbprefix('user')." us on wt.user_id=us.user_id where wt.withdraw_id='".$id."'");
		
		if($query->num_rows()>0)
		{
			return $query->row();
		}
		
		return 0;
		
	}
	
	
	function get_withdraw_method_detail($withdraw_id,$type)
	{
		
		if($type=='bank')
		{
		
	$get_bank_details=$this->db->query("select * from ".$this->db->dbprefix('wallet_bank')." where bank_withdraw_type='bank' and withdraw_id='".$withdraw_id."'");
			
			if($get_bank_details->num_rows()>0)
			{			
				return $get_bank_details->row();
			}
			return 0;
		}
		
		
		if($type=='check')
		{
		
	$get_check_details=$this->db->query("select * from ".$this->db->dbprefix('wallet_bank')." where bank_withdraw_type='check' and withdraw_id='".$withdraw_id."'");
			
			if($get_check_details->num_rows()>0)
			{		
				return $get_check_details->row();
			}
			return 0;
		}
		
		
		
		if($type=='gateway')
		{
			
			$get_gateway_details=$this->db->query("select * from ".$this->db->dbprefix('wallet_withdraw_gateway')." where withdraw_id='".$withdraw_id."'");
			
			if($get_gateway_details->num_rows()>0)
			{	
				return $get_gateway_details->row();
			}
			
			return 0;
		}
		
		return 0;
		
	}
	
	
	
	
	
	
	
	
	
	function get_gateway_name($id)
	{
		$query = $this->db->get_where('payments_gateways',array('id'=>$id));
		return $query->row();
	}
	
	
	function get_total_wallet_review_count()
	{
            
            	$query=$this->db->query("select us.user_id,us.first_name,us.last_name,us.email, (select SUM(wt.credit)-SUM(wt.debit) from ".$this->db->dbprefix('wallet')." wt where wt.user_id=us.user_id     ) as total_points from ".$this->db->dbprefix('user')." us where us.user_id!=0 order by total_points desc");
		
		if($query->num_rows()>0)
		{	
			return $query->num_rows();
		}
		
		return 0;
	}
	
	
	function get_wallet_review_result($offset, $limit)
	{
		$query=$this->db->query("select us.user_id, us.first_name,us.last_name,us.email, (select SUM(wt.credit)-SUM(wt.debit) from ".$this->db->dbprefix('wallet')." wt where wt.user_id=us.user_id   ) as total_points from ".$this->db->dbprefix('user')." us where us.user_id!=0  order by total_points desc limit ".$limit." offset ".$offset);
		
		if($query->num_rows()>0)
		{	
			return $query->result();
		}
		
		return 0;
	
	}
	
	
	function get_total_search_wallet_review_count($option,$keyword)
	{
		if($option=='full_name')
		{
			$option='us.full_name';
		}
		if($option=='email')
		{
			$option='us.email';
		}
		
		
		
		
		$query=$this->db->query("select us.user_id, us.first_name,us.last_name,us.email, (select SUM(wt.credit)-SUM(wt.debit) from ".$this->db->dbprefix('wallet')." wt where wt.user_id=us.user_id  ) as total_points from ".$this->db->dbprefix('user')." us where us.user_id!=0  and  ".$option." like '%".$keyword."%' order by  total_points  desc");
		
				
		if($query->num_rows()>0)
		{	
			return $query->num_rows();
		}
		
		return 0;
	}
	
	
	function get_search_wallet_review_result($option,$keyword,$offset, $limit)
	{
		
		if($option=='full_name')
		{
			$option='us.full_name';
		}
		if($option=='email')
		{
			$option='us.email';
		}
		
	
		
		$query=$this->db->query("select us.user_id, us.first_name,us.last_name,us.email, (select SUM(wt.credit)-SUM(wt.debit) from ".$this->db->dbprefix('wallet')." wt where wt.user_id=us.user_id   ) as total_points from ".$this->db->dbprefix('user')." us where us.user_id!=0 and ".$option." like '%".$keyword."%'  order by total_points desc limit ".$limit." offset ".$offset);
		
		
		if($query->num_rows()>0)
		{	
			return $query->result();
		}
		
		return 0;
	
	}
	
        
        
        ///===admin review
        
        function get_total_admin_wallet_review_count()
	{
            
            	$query=$this->db->query("select us.user_id,us.first_name,us.last_name,us.email, (select SUM(wt.credit)-SUM(wt.debit) from ".$this->db->dbprefix('wallet')." wt where wt.user_id=us.user_id and  wt.is_authorize!=1  ) as total_points from ".$this->db->dbprefix('user')." us where us.user_id!=0 order by total_points desc");
		
		if($query->num_rows()>0)
		{	
			return $query->num_rows();
		}
		
		return 0;
	}
	
	
	function get_admin_wallet_review_result($offset, $limit)
	{
		$query=$this->db->query("select us.user_id, us.first_name,us.last_name,us.email, (select SUM(wt.credit)-SUM(wt.debit) from ".$this->db->dbprefix('wallet')." wt where wt.user_id=us.user_id and  wt.is_authorize!=1  ) as total_points from ".$this->db->dbprefix('user')." us where us.user_id!=0  order by total_points desc limit ".$limit." offset ".$offset);
		
		if($query->num_rows()>0)
		{	
			return $query->result();
		}
		
		return 0;
	
	}
	
}

?>