<?php

class Category_model extends CI_Model {

    function Category_model() {
        parent::__construct();
    }

    ////=======store category =========


    function check_store_category_have_sub_category($category_id) {
        $query = $this->db->get_where('store_category', array('category_parent_id ' => $category_id));
        if ($query->num_rows() > 0) {
            return 1;
        }
        return 0;
    }

    function get_total_store_category_count() {
        return $this->db->count_all('store_category');
    }

    function get_store_category_result($offset, $limit) {
        //$query=$this->db->query("select * from ".$this->db->dbprefix('category')." c order by category_id desc limit ".$limit." offset ".$offset);
        $query = $this->db->query("select * from " . $this->db->dbprefix('store_category') . " c order by category_id desc");


        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function get_store_parent_category() {
        $query = $this->db->get_where('store_category', array('category_parent_id ' => '0'));
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function store_category_insert() {
        $category_image = 'no_image.png';
        $image_settings = image_setting();

        if ($_FILES['file_up']['name'] != '') {
            $this->load->library('upload');
            $rand = rand(0, 100000);

            $_FILES['userfile']['name'] = $_FILES['file_up']['name'];
            $_FILES['userfile']['type'] = $_FILES['file_up']['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES['file_up']['tmp_name'];
            $_FILES['userfile']['error'] = $_FILES['file_up']['error'];
            $_FILES['userfile']['size'] = $_FILES['file_up']['size'];

            $config['file_name'] = $rand . 'category';
            $config['upload_path'] = base_path() . 'upload/category_orig/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';

            $this->upload->initialize($config);

            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
            }

            $picture = $this->upload->data();

            $this->load->library('image_lib');

            $this->image_lib->clear();


            $config['image_library'] = 'GD2';
            $config['maintain_ratio'] = FALSE;
            //$config['create_thumb'] = TRUE;
            //$config['master_dim'] = 'width';
            $config['quality'] = '100%';
            $config['source_image'] = base_path() . 'upload/category_orig/' . $picture['file_name'];
            $config['new_image'] = base_path() . 'upload/category/' . $picture['file_name'];
            $config['width'] = $image_settings->category_width;
            $config['height'] = $image_settings->category_height;
            $this->image_lib->initialize($config);






            if (!$this->image_lib->resize()) {
                $error = $this->image_lib->display_errors();
            }

            $category_image = $picture['file_name'];



            if ($this->input->post('prev_category_image') != '') {
                if (file_exists(base_path() . 'upload/category/' . $this->input->post('prev_category_image'))) {
                    $link = base_path() . 'upload/category/' . $this->input->post('prev_category_image');
                    unlink($link);
                }
                if (file_exists(base_path() . 'upload/category_orig/' . $this->input->post('prev_category_image'))) {
                    $link2 = base_path() . 'upload/category_orig/' . $this->input->post('prev_category_image');
                    unlink($link2);
                }
            }
        } else {
            if ($this->input->post('prev_category_image') != '') {
                $category_image = $this->input->post('prev_category_image');
            }
        }

        $category_url_name = clean_url($this->input->post('category_name'));



        $chk_url_exists = $this->db->query("select MAX(category_url_name) as category_url_name from " . $this->db->dbprefix('store_category') . " where category_url_name like '" . $category_url_name . "%'");

        if ($chk_url_exists->num_rows() > 0) {
            $get_pr = $chk_url_exists->row();

            $strre = '0';
            if ($get_pr->category_url_name != '') {
                $strre = str_replace($category_url_name, '', $get_pr->category_url_name);
            }

            if ($strre == '0') {
                $newcnt = '';
            } elseif ($strre == '') {
                $newcnt = '1';
            } else {
                $newcnt = (int) $strre + 1;
            }

            $category_url_name = $category_url_name . $newcnt;
        }

        $data = array(
            'category_name' => $this->input->post('category_name'),
            'category_parent_id' => $this->input->post('category_parent_id'),
            'category_status' => $this->input->post('category_status'),
            'category_url_name' => $category_url_name,
            'category_description' => $this->input->post('category_description'),
            'category_image' => $category_image
        );
        $this->db->insert('store_category', $data);


        $pid = mysql_insert_id();
    }

    function store_category_update() {



        $category_image = 'no_image.png';
        $image_settings = image_setting();
        if ($_FILES['file_up']['name'] != '') {

            $this->load->library('upload');
            $rand = rand(0, 100000);
            $_FILES['userfile']['name'] = $_FILES['file_up']['name'];
            $_FILES['userfile']['type'] = $_FILES['file_up']['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES['file_up']['tmp_name'];
            $_FILES['userfile']['error'] = $_FILES['file_up']['error'];
            $_FILES['userfile']['size'] = $_FILES['file_up']['size'];

            $config['file_name'] = $rand . 'category';
            $config['upload_path'] = base_path() . 'upload/category_orig/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';

            $this->upload->initialize($config);


            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
            }


            $picture = $this->upload->data();

            $this->load->library('image_lib');

            $this->image_lib->clear();



            $config['image_library'] = 'GD2';
            $config['maintain_ratio'] = FALSE;
            //$config['create_thumb'] = TRUE;
            //$config['master_dim'] = 'width';
            $config['quality'] = '100%';
            $config['source_image'] = base_path() . 'upload/category_orig/' . $picture['file_name'];
            $config['new_image'] = base_path() . 'upload/category/' . $picture['file_name'];
            $config['width'] = $image_settings->category_width;
            $config['height'] = $image_settings->category_height;
            $this->image_lib->initialize($config);



            if (!$this->image_lib->resize()) {
                $error = $this->image_lib->display_errors();
            }


            $category_image = $picture['file_name'];

            if ($this->input->post('prev_category_image') != '') {
                if (file_exists(base_path() . 'upload/category/' . $this->input->post('prev_category_image'))) {
                    $link = base_path() . 'upload/category/' . $this->input->post('prev_category_image');
                    unlink($link);
                }
                if (file_exists(base_path() . 'upload/category_orig/' . $this->input->post('prev_category_image'))) {
                    $link2 = base_path() . 'upload/category_orig/' . $this->input->post('prev_category_image');
                    unlink($link2);
                }
            }
        } else {
            if ($this->input->post('prev_category_image') != '') {
                $category_image = $this->input->post('prev_category_image');
            }
        }


        $category_url_name = clean_url($this->input->post('category_name'));


        $chk_url_exists = $this->db->query("select MAX(category_url_name) as category_url_name from " . $this->db->dbprefix('store_category') . " where category_id!='" . $this->input->post('category_id') . "' and category_url_name like '" . $category_url_name . "%'");

        if ($chk_url_exists->num_rows() > 0) {
            $get_pr = $chk_url_exists->row();

            $strre = '0';
            if ($get_pr->category_url_name != '') {
                $strre = str_replace($category_url_name, '', $get_pr->category_url_name);
            }

            if ($strre == '0') {
                $newcnt = '';
            } elseif ($strre == '') {
                $newcnt = '1';
            } else {
                $newcnt = (int) $strre + 1;
            }

            $category_url_name = $category_url_name . $newcnt;
        }


        $data = array(
            'category_name' => $this->input->post('category_name'),
            'category_parent_id' => $this->input->post('category_parent_id'),
            'category_status' => $this->input->post('category_status'),
            'category_url_name' => $category_url_name,
            'category_description' => $this->input->post('category_description'),
            'category_image' => $category_image,
        );
        $this->db->where('category_id', $this->input->post('category_id'));
        $this->db->update('store_category', $data);
    }

    function get_one_store_category($id) {
        $query = $this->db->get_where('store_category', array('category_id' => $id));
        return $query->row_array();
    }

    ////=======challenge category =========


    function check_challenge_category_have_sub_category($category_id) {
        $query = $this->db->get_where('challenge_category', array('category_parent_id ' => $category_id));
        if ($query->num_rows() > 0) {
            return 1;
        }
        return 0;
    }

    function get_total_challenge_category_count() {
        return $this->db->count_all('challenge_category');
    }

    function get_challenge_category_result($offset, $limit) {
        //$query=$this->db->query("select * from ".$this->db->dbprefix('category')." c order by category_id desc limit ".$limit." offset ".$offset);
        $query = $this->db->query("select * from " . $this->db->dbprefix('challenge_category') . " c order by category_id desc");


        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function get_challenge_parent_category() {
        $query = $this->db->get_where('challenge_category', array('category_parent_id ' => '0'));
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function challenge_category_insert() {
        $category_image = 'no_image.png';
        $image_settings = image_setting();

        if ($_FILES['file_up']['name'] != '') {
            $this->load->library('upload');
            $rand = rand(0, 100000);

            $_FILES['userfile']['name'] = $_FILES['file_up']['name'];
            $_FILES['userfile']['type'] = $_FILES['file_up']['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES['file_up']['tmp_name'];
            $_FILES['userfile']['error'] = $_FILES['file_up']['error'];
            $_FILES['userfile']['size'] = $_FILES['file_up']['size'];

            $config['file_name'] = $rand . 'category';
            $config['upload_path'] = base_path() . 'upload/category_orig/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';

            $this->upload->initialize($config);

            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
            }

            $picture = $this->upload->data();

            $this->load->library('image_lib');

            $this->image_lib->clear();


            $config['image_library'] = 'GD2';
            $config['maintain_ratio'] = FALSE;
            //$config['create_thumb'] = TRUE;
            //$config['master_dim'] = 'width';
            $config['quality'] = '100%';
            $config['source_image'] = base_path() . 'upload/category_orig/' . $picture['file_name'];
            $config['new_image'] = base_path() . 'upload/category/' . $picture['file_name'];
            $config['width'] = $image_settings->category_width;
            $config['height'] = $image_settings->category_height;
            $this->image_lib->initialize($config);






            if (!$this->image_lib->resize()) {
                $error = $this->image_lib->display_errors();
            }

            $category_image = $picture['file_name'];



            if ($this->input->post('prev_category_image') != '') {
                if (file_exists(base_path() . 'upload/category/' . $this->input->post('prev_category_image'))) {
                    $link = base_path() . 'upload/category/' . $this->input->post('prev_category_image');
                    unlink($link);
                }
                if (file_exists(base_path() . 'upload/category_orig/' . $this->input->post('prev_category_image'))) {
                    $link2 = base_path() . 'upload/category_orig/' . $this->input->post('prev_category_image');
                    unlink($link2);
                }
            }
        } else {
            if ($this->input->post('prev_category_image') != '') {
                $category_image = $this->input->post('prev_category_image');
            }
        }

        $category_url_name = clean_url($this->input->post('category_name'));



        $chk_url_exists = $this->db->query("select MAX(category_url_name) as category_url_name from " . $this->db->dbprefix('challenge_category') . " where category_url_name like '" . $category_url_name . "%'");

        if ($chk_url_exists->num_rows() > 0) {
            $get_pr = $chk_url_exists->row();

            $strre = '0';
            if ($get_pr->category_url_name != '') {
                $strre = str_replace($category_url_name, '', $get_pr->category_url_name);
            }

            if ($strre == '0') {
                $newcnt = '';
            } elseif ($strre == '') {
                $newcnt = '1';
            } else {
                $newcnt = (int) $strre + 1;
            }

            $category_url_name = $category_url_name . $newcnt;
        }

        $data = array(
            'category_name' => $this->input->post('category_name'),
            'category_parent_id' => $this->input->post('category_parent_id'),
            'category_status' => $this->input->post('category_status'),
            'category_url_name' => $category_url_name,
            'category_description' => $this->input->post('category_description'),
            'category_image' => $category_image
        );
        $this->db->insert('challenge_category', $data);


        $pid = mysql_insert_id();
    }

    function challenge_category_update() {



        $category_image = 'no_image.png';
        $image_settings = image_setting();
        if ($_FILES['file_up']['name'] != '') {

            $this->load->library('upload');
            $rand = rand(0, 100000);
            $_FILES['userfile']['name'] = $_FILES['file_up']['name'];
            $_FILES['userfile']['type'] = $_FILES['file_up']['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES['file_up']['tmp_name'];
            $_FILES['userfile']['error'] = $_FILES['file_up']['error'];
            $_FILES['userfile']['size'] = $_FILES['file_up']['size'];

            $config['file_name'] = $rand . 'category';
            $config['upload_path'] = base_path() . 'upload/category_orig/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';

            $this->upload->initialize($config);


            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
            }


            $picture = $this->upload->data();

            $this->load->library('image_lib');

            $this->image_lib->clear();



            $config['image_library'] = 'GD2';
            $config['maintain_ratio'] = FALSE;
            //$config['create_thumb'] = TRUE;
            //$config['master_dim'] = 'width';
            $config['quality'] = '100%';
            $config['source_image'] = base_path() . 'upload/category_orig/' . $picture['file_name'];
            $config['new_image'] = base_path() . 'upload/category/' . $picture['file_name'];
            $config['width'] = $image_settings->category_width;
            $config['height'] = $image_settings->category_height;
            $this->image_lib->initialize($config);



            if (!$this->image_lib->resize()) {
                $error = $this->image_lib->display_errors();
            }


            $category_image = $picture['file_name'];

            if ($this->input->post('prev_category_image') != '') {
                if (file_exists(base_path() . 'upload/category/' . $this->input->post('prev_category_image'))) {
                    $link = base_path() . 'upload/category/' . $this->input->post('prev_category_image');
                    unlink($link);
                }
                if (file_exists(base_path() . 'upload/category_orig/' . $this->input->post('prev_category_image'))) {
                    $link2 = base_path() . 'upload/category_orig/' . $this->input->post('prev_category_image');
                    unlink($link2);
                }
            }
        } else {
            if ($this->input->post('prev_category_image') != '') {
                $category_image = $this->input->post('prev_category_image');
            }
        }


        $category_url_name = clean_url($this->input->post('category_name'));


        $chk_url_exists = $this->db->query("select MAX(category_url_name) as category_url_name from " . $this->db->dbprefix('challenge_category') . " where category_id!='" . $this->input->post('category_id') . "' and category_url_name like '" . $category_url_name . "%'");

        if ($chk_url_exists->num_rows() > 0) {
            $get_pr = $chk_url_exists->row();

            $strre = '0';
            if ($get_pr->category_url_name != '') {
                $strre = str_replace($category_url_name, '', $get_pr->category_url_name);
            }

            if ($strre == '0') {
                $newcnt = '';
            } elseif ($strre == '') {
                $newcnt = '1';
            } else {
                $newcnt = (int) $strre + 1;
            }

            $category_url_name = $category_url_name . $newcnt;
        }


        $data = array(
            'category_name' => $this->input->post('category_name'),
            'category_parent_id' => $this->input->post('category_parent_id'),
            'category_status' => $this->input->post('category_status'),
            'category_url_name' => $category_url_name,
            'category_description' => $this->input->post('category_description'),
            'category_image' => $category_image,
        );
        $this->db->where('category_id', $this->input->post('category_id'));
        $this->db->update('challenge_category', $data);
    }

    function get_one_challenge_category($id) {
        $query = $this->db->get_where('challenge_category', array('category_id' => $id));
        return $query->row_array();
    }

    ////=======video category =========


    function check_video_category_have_sub_category($category_id) {
        $query = $this->db->get_where('video_category', array('category_parent_id ' => $category_id));
        if ($query->num_rows() > 0) {
            return 1;
        }
        return 0;
    }

    function get_video_category_multi($id) {
        $query = $this->db->query("select * from " . $this->db->dbprefix('video_category') . " where category_parent_id='" . $id . "'");
        
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }
    
    function get_total_video_category_count() {
       
        $query = $this->db->query("select * from " . $this->db->dbprefix('video_category') . " where (category_status=1 or (category_status=0 and category_user_id=0) ) order by category_id desc");


        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return 0;
        
    }

    function get_video_category_result($offset, $limit) {
         $query = $this->db->query("select * from " . $this->db->dbprefix('video_category') . " where (category_status=1 or (category_status=0 and category_user_id=0) ) order by category_id desc");

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }
    
    
    
     function get_total_pending_video_category_count() {
        $query = $this->db->query("select * from " . $this->db->dbprefix('video_category') . " where category_status=0 and category_user_id>0 order by category_id desc");


        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return 0;
    }

    function get_pending_video_category_result($offset, $limit) {
        //$query=$this->db->query("select * from ".$this->db->dbprefix('category')." c order by category_id desc limit ".$limit." offset ".$offset);
        $query = $this->db->query("select * from " . $this->db->dbprefix('video_category') . " where category_status=0 and category_user_id>0 order by category_id desc");


        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function get_video_parent_category() {
        $query = $this->db->get_where('video_category', array('category_parent_id ' => '0'));
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function video_category_insert() {
        $category_image = 'no_image.png';
        $image_settings = image_setting();

        if ($_FILES['file_up']['name'] != '') {
            $this->load->library('upload');
            $rand = rand(0, 100000);

            $_FILES['userfile']['name'] = $_FILES['file_up']['name'];
            $_FILES['userfile']['type'] = $_FILES['file_up']['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES['file_up']['tmp_name'];
            $_FILES['userfile']['error'] = $_FILES['file_up']['error'];
            $_FILES['userfile']['size'] = $_FILES['file_up']['size'];

            $config['file_name'] = $rand . 'category';
            $config['upload_path'] = base_path() . 'upload/category_orig/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';

            $this->upload->initialize($config);

            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
            }

            $picture = $this->upload->data();

            $this->load->library('image_lib');

            $this->image_lib->clear();


            $config['image_library'] = 'GD2';
            $config['maintain_ratio'] = FALSE;
            //$config['create_thumb'] = TRUE;
            //$config['master_dim'] = 'width';
            $config['quality'] = '100%';
            $config['source_image'] = base_path() . 'upload/category_orig/' . $picture['file_name'];
            $config['new_image'] = base_path() . 'upload/category/' . $picture['file_name'];
            $config['width'] = $image_settings->category_width;
            $config['height'] = $image_settings->category_height;
            $this->image_lib->initialize($config);






            if (!$this->image_lib->resize()) {
                $error = $this->image_lib->display_errors();
            }

            $category_image = $picture['file_name'];



            if ($this->input->post('prev_category_image') != '') {
                if (file_exists(base_path() . 'upload/category/' . $this->input->post('prev_category_image'))) {
                    $link = base_path() . 'upload/category/' . $this->input->post('prev_category_image');
                    unlink($link);
                }
                if (file_exists(base_path() . 'upload/category_orig/' . $this->input->post('prev_category_image'))) {
                    $link2 = base_path() . 'upload/category_orig/' . $this->input->post('prev_category_image');
                    unlink($link2);
                }
            }
        } else {
            if ($this->input->post('prev_category_image') != '') {
                $category_image = $this->input->post('prev_category_image');
            }
        }

        $category_url_name = clean_url($this->input->post('category_name'));



        $chk_url_exists = $this->db->query("select MAX(category_url_name) as category_url_name from " . $this->db->dbprefix('video_category') . " where category_url_name like '" . $category_url_name . "%'");

        if ($chk_url_exists->num_rows() > 0) {
            $get_pr = $chk_url_exists->row();

            $strre = '0';
            if ($get_pr->category_url_name != '') {
                $strre = str_replace($category_url_name, '', $get_pr->category_url_name);
            }

            if ($strre == '0') {
                $newcnt = '';
            } elseif ($strre == '') {
                $newcnt = '1';
            } else {
                $newcnt = (int) $strre + 1;
            }

            $category_url_name = $category_url_name . $newcnt;
        }

        $data = array(
            'category_name' => $this->input->post('category_name'),
            'category_parent_id' => $this->input->post('category_parent_id'),
            'category_status' => $this->input->post('category_status'),
            'category_url_name' => $category_url_name,
            'category_description' => $this->input->post('category_description'),
            'category_image' => $category_image
        );
        $this->db->insert('video_category', $data);


        $pid = mysql_insert_id();
    }

    function video_category_update() {



        $category_image = 'no_image.png';
        $image_settings = image_setting();
        if ($_FILES['file_up']['name'] != '') {

            $this->load->library('upload');
            $rand = rand(0, 100000);
            $_FILES['userfile']['name'] = $_FILES['file_up']['name'];
            $_FILES['userfile']['type'] = $_FILES['file_up']['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES['file_up']['tmp_name'];
            $_FILES['userfile']['error'] = $_FILES['file_up']['error'];
            $_FILES['userfile']['size'] = $_FILES['file_up']['size'];

            $config['file_name'] = $rand . 'category';
            $config['upload_path'] = base_path() . 'upload/category_orig/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';

            $this->upload->initialize($config);


            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
            }


            $picture = $this->upload->data();

            $this->load->library('image_lib');

            $this->image_lib->clear();



            $config['image_library'] = 'GD2';
            $config['maintain_ratio'] = FALSE;
            //$config['create_thumb'] = TRUE;
            //$config['master_dim'] = 'width';
            $config['quality'] = '100%';
            $config['source_image'] = base_path() . 'upload/category_orig/' . $picture['file_name'];
            $config['new_image'] = base_path() . 'upload/category/' . $picture['file_name'];
            $config['width'] = $image_settings->category_width;
            $config['height'] = $image_settings->category_height;
            $this->image_lib->initialize($config);



            if (!$this->image_lib->resize()) {
                $error = $this->image_lib->display_errors();
            }


            $category_image = $picture['file_name'];

            if ($this->input->post('prev_category_image') != '') {
                if (file_exists(base_path() . 'upload/category/' . $this->input->post('prev_category_image'))) {
                    $link = base_path() . 'upload/category/' . $this->input->post('prev_category_image');
                    unlink($link);
                }
                if (file_exists(base_path() . 'upload/category_orig/' . $this->input->post('prev_category_image'))) {
                    $link2 = base_path() . 'upload/category_orig/' . $this->input->post('prev_category_image');
                    unlink($link2);
                }
            }
        } else {
            if ($this->input->post('prev_category_image') != '') {
                $category_image = $this->input->post('prev_category_image');
            }
        }


        $category_url_name = clean_url($this->input->post('category_name'));


        $chk_url_exists = $this->db->query("select MAX(category_url_name) as category_url_name from " . $this->db->dbprefix('video_category') . " where category_id!='" . $this->input->post('category_id') . "' and category_url_name like '" . $category_url_name . "%'");

        if ($chk_url_exists->num_rows() > 0) {
            $get_pr = $chk_url_exists->row();

            $strre = '0';
            if ($get_pr->category_url_name != '') {
                $strre = str_replace($category_url_name, '', $get_pr->category_url_name);
            }

            if ($strre == '0') {
                $newcnt = '';
            } elseif ($strre == '') {
                $newcnt = '1';
            } else {
                $newcnt = (int) $strre + 1;
            }

            $category_url_name = $category_url_name . $newcnt;
        }


        $data = array(
            'category_name' => $this->input->post('category_name'),
            'category_parent_id' => $this->input->post('category_parent_id'),
            'category_status' => $this->input->post('category_status'),
            'category_url_name' => $category_url_name,
            'category_description' => $this->input->post('category_description'),
            'category_image' => $category_image,
        );
        $this->db->where('category_id', $this->input->post('category_id'));
        $this->db->update('video_category', $data);
        
        $category_id=$this->input->post('category_id');
         //===for activate design===
        $category_detail=$this->get_one_video_category($category_id);
        
        if($category_detail){
            
            if($category_detail['category_user_id']>0){
                
                
                $this->db->select('d.video_id');
                $this->db->from('video d');
                $this->db->join('video_category_rel dr', 'd.video_id=dr.video_id');
                $this->db->join('video_category c', 'c.category_id=dr.category_id'); 
                $this->db->where('d.video_status', 0);
                $this->db->where('dr.category_id', $category_id);              
                $this->db->group_by('d.video_id');
               
                $query = $this->db->get();

                if ($query->num_rows() > 0) {
                    $result=$query->result();
                    
                    foreach($result as $res){
                        
                        $video_data=array();
                        
                        $video_data=array('video_status'=>1);
                        $this->db->where('video_id',$res->video_id);
                        $this->db->update('video',$video_data);
                        
                    }
                    
                }
                
                
                
            }
            
            
        }
        ///======
        
        
    }

    function get_one_video_category($id) {
        $query = $this->db->get_where('video_category', array('category_id' => $id));
        return $query->row_array();
    }

    ///==========design category========

    function check_design_category_have_sub_category($category_id) {
        $query = $this->db->get_where('design_category', array('category_parent_id ' => $category_id));
        if ($query->num_rows() > 0) {
            return 1;
        }
        return 0;
    }

    function get_design_category_multi($id) {
        $query = $this->db->query("select * from " . $this->db->dbprefix('design_category') . " where category_parent_id='" . $id . "'");
        
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    function get_total_design_category_count() {
        $query = $this->db->query("select * from " . $this->db->dbprefix('design_category') . " where (category_status=1 or (category_status=0 and category_user_id=0) ) order by category_id desc");


        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return 0;
    }

    function get_design_category_result($offset, $limit) {
        //$query=$this->db->query("select * from ".$this->db->dbprefix('category')." c order by category_id desc limit ".$limit." offset ".$offset);
        $query = $this->db->query("select * from " . $this->db->dbprefix('design_category') . " where (category_status=1 or (category_status=0 and category_user_id=0) ) order by category_id desc");


        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }
    
    
    function get_total_pending_design_category_count() {
        $query = $this->db->query("select * from " . $this->db->dbprefix('design_category') . " where category_status=0 and category_user_id>0 order by category_id desc");


        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return 0;
    }

    function get_pending_design_category_result($offset, $limit) {
        //$query=$this->db->query("select * from ".$this->db->dbprefix('category')." c order by category_id desc limit ".$limit." offset ".$offset);
        $query = $this->db->query("select * from " . $this->db->dbprefix('design_category') . " where category_status=0 and category_user_id>0 order by category_id desc");


        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function get_design_parent_category() {
        $query = $this->db->get_where('design_category', array('category_parent_id ' => '0'));
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function design_category_insert() {
        $category_image = 'no_image.png';
        $image_settings = image_setting();

        if ($_FILES['file_up']['name'] != '') {
            $this->load->library('upload');
            $rand = rand(0, 100000);

            $_FILES['userfile']['name'] = $_FILES['file_up']['name'];
            $_FILES['userfile']['type'] = $_FILES['file_up']['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES['file_up']['tmp_name'];
            $_FILES['userfile']['error'] = $_FILES['file_up']['error'];
            $_FILES['userfile']['size'] = $_FILES['file_up']['size'];

            $config['file_name'] = $rand . 'category';
            $config['upload_path'] = base_path() . 'upload/category_orig/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';

            $this->upload->initialize($config);

            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
            }

            $picture = $this->upload->data();

            $this->load->library('image_lib');

            $this->image_lib->clear();


            $config['image_library'] = 'GD2';
            $config['maintain_ratio'] = FALSE;
            //$config['create_thumb'] = TRUE;
            //$config['master_dim'] = 'width';
            $config['quality'] = '100%';
            $config['source_image'] = base_path() . 'upload/category_orig/' . $picture['file_name'];
            $config['new_image'] = base_path() . 'upload/category/' . $picture['file_name'];
            $config['width'] = $image_settings->category_width;
            $config['height'] = $image_settings->category_height;
            $this->image_lib->initialize($config);






            if (!$this->image_lib->resize()) {
                $error = $this->image_lib->display_errors();
            }

            $category_image = $picture['file_name'];



            if ($this->input->post('prev_category_image') != '') {
                if (file_exists(base_path() . 'upload/category/' . $this->input->post('prev_category_image'))) {
                    $link = base_path() . 'upload/category/' . $this->input->post('prev_category_image');
                    unlink($link);
                }
                if (file_exists(base_path() . 'upload/category_orig/' . $this->input->post('prev_category_image'))) {
                    $link2 = base_path() . 'upload/category_orig/' . $this->input->post('prev_category_image');
                    unlink($link2);
                }
            }
        } else {
            if ($this->input->post('prev_category_image') != '') {
                $category_image = $this->input->post('prev_category_image');
            }
        }

        $category_url_name = clean_url($this->input->post('category_name'));



        $chk_url_exists = $this->db->query("select MAX(category_url_name) as category_url_name from " . $this->db->dbprefix('design_category') . " where category_url_name like '" . $category_url_name . "%'");

        if ($chk_url_exists->num_rows() > 0) {
            $get_pr = $chk_url_exists->row();

            $strre = '0';
            if ($get_pr->category_url_name != '') {
                $strre = str_replace($category_url_name, '', $get_pr->category_url_name);
            }

            if ($strre == '0') {
                $newcnt = '';
            } elseif ($strre == '') {
                $newcnt = '1';
            } else {
                $newcnt = (int) $strre + 1;
            }

            $category_url_name = $category_url_name . $newcnt;
        }

        $data = array(
            'category_name' => $this->input->post('category_name'),
            'category_parent_id' => $this->input->post('category_parent_id'),
            'category_status' => $this->input->post('category_status'),
            'category_url_name' => $category_url_name,
            'category_description' => $this->input->post('category_description'),
            'category_image' => $category_image
        );
        $this->db->insert('design_category', $data);


        $pid = mysql_insert_id();
    }

    function design_category_update() {

        

        $category_image = 'no_image.png';
        $image_settings = image_setting();
        if ($_FILES['file_up']['name'] != '') {

            $this->load->library('upload');
            $rand = rand(0, 100000);
            $_FILES['userfile']['name'] = $_FILES['file_up']['name'];
            $_FILES['userfile']['type'] = $_FILES['file_up']['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES['file_up']['tmp_name'];
            $_FILES['userfile']['error'] = $_FILES['file_up']['error'];
            $_FILES['userfile']['size'] = $_FILES['file_up']['size'];

            $config['file_name'] = $rand . 'category';
            $config['upload_path'] = base_path() . 'upload/category_orig/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';

            $this->upload->initialize($config);


            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
            }


            $picture = $this->upload->data();

            $this->load->library('image_lib');

            $this->image_lib->clear();



            $config['image_library'] = 'GD2';
            $config['maintain_ratio'] = FALSE;
            //$config['create_thumb'] = TRUE;
            //$config['master_dim'] = 'width';
            $config['quality'] = '100%';
            $config['source_image'] = base_path() . 'upload/category_orig/' . $picture['file_name'];
            $config['new_image'] = base_path() . 'upload/category/' . $picture['file_name'];
            $config['width'] = $image_settings->category_width;
            $config['height'] = $image_settings->category_height;
            $this->image_lib->initialize($config);



            if (!$this->image_lib->resize()) {
                $error = $this->image_lib->display_errors();
            }


            $category_image = $picture['file_name'];

            if ($this->input->post('prev_category_image') != '') {
                if (file_exists(base_path() . 'upload/category/' . $this->input->post('prev_category_image'))) {
                    $link = base_path() . 'upload/category/' . $this->input->post('prev_category_image');
                    unlink($link);
                }
                if (file_exists(base_path() . 'upload/category_orig/' . $this->input->post('prev_category_image'))) {
                    $link2 = base_path() . 'upload/category_orig/' . $this->input->post('prev_category_image');
                    unlink($link2);
                }
            }
        } else {
            if ($this->input->post('prev_category_image') != '') {
                $category_image = $this->input->post('prev_category_image');
            }
        }


        $category_url_name = clean_url($this->input->post('category_name'));


        $chk_url_exists = $this->db->query("select MAX(category_url_name) as category_url_name from " . $this->db->dbprefix('design_category') . " where category_id!='" . $this->input->post('category_id') . "' and category_url_name like '" . $category_url_name . "%'");

        if ($chk_url_exists->num_rows() > 0) {
            $get_pr = $chk_url_exists->row();

            $strre = '0';
            if ($get_pr->category_url_name != '') {
                $strre = str_replace($category_url_name, '', $get_pr->category_url_name);
            }

            if ($strre == '0') {
                $newcnt = '';
            } elseif ($strre == '') {
                $newcnt = '1';
            } else {
                $newcnt = (int) $strre + 1;
            }

            $category_url_name = $category_url_name . $newcnt;
        }


        $data = array(
            'category_name' => $this->input->post('category_name'),
            'category_parent_id' => $this->input->post('category_parent_id'),
            'category_status' => $this->input->post('category_status'),
            'category_url_name' => $category_url_name,
            'category_description' => $this->input->post('category_description'),
            'category_image' => $category_image,
        );
        $this->db->where('category_id', $this->input->post('category_id'));
        $this->db->update('design_category', $data);
        
        $category_id=$this->input->post('category_id');
        
        
        //===for activate design===
        $category_detail=$this->get_one_design_category($category_id);
        
        if($category_detail){
            
            if($category_detail['category_user_id']>0){
                
                
                $this->db->select('d.design_id');
                $this->db->from('design d');
                $this->db->join('design_category_rel dr', 'd.design_id=dr.design_id');
                $this->db->join('design_category c', 'c.category_id=dr.category_id'); 
                $this->db->where('d.design_status', 0);
                $this->db->where('dr.category_id', $category_id);              
                $this->db->group_by('d.design_id');
               
                $query = $this->db->get();

                if ($query->num_rows() > 0) {
                    $result=$query->result();
                    
                    foreach($result as $res){
                        
                        $design_data=array();
                        
                        $design_data=array('design_status'=>1);
                        $this->db->where('design_id',$res->design_id);
                        $this->db->update('design',$design_data);
                        
                    }
                    
                }
                
                
                
            }
            
            
        }
        ///======
        
        
    }

    function get_one_design_category($id) {
        $query = $this->db->get_where('design_category', array('category_id' => $id));
        return $query->row_array();
    }

}

?>