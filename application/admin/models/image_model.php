<?php

class Image_model extends CI_Model {
	
    function Image_model()
    {
        parent::__construct();	
    }   
	
	
	/************email upload***************/
	
	function get_total_activate_email_count()
	{
		$this->db->select('*');
		$this->db->from('user_email_activate ue');
		$this->db->join('user us','ue.user_id=us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		//$this->db->where('ue.status',1);
		$this->db->order_by('ue.activate_email_id','desc');
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;
		
	}
	
	
	function get_all_activate_email_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user_email_activate ue');
		$this->db->join('user us','ue.user_id=us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		//$this->db->where('ue.status',1);
		$this->db->order_by('ue.activate_email_id','desc');
		$this->db->limit($limit,$offset);
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
	}
	
	
	function get_total_search_activate_email_count($option,$keyword)
	{
		$this->db->select('*');
		$this->db->from('user_email_activate ue');
		$this->db->join('user us','ue.user_id=us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		//$this->db->where('ue.status',1);
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
		
		
		$this->db->order_by('ue.activate_email_id','desc');
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;
	}
	
	
	function get_all_search_activate_email_result($option,$keyword,$offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user_email_activate ue');
		$this->db->join('user us','ue.user_id=us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		//$this->db->where('ue.status',1);
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
		
		
		$this->db->order_by('ue.activate_email_id','desc');
		$this->db->limit($limit,$offset);
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
	}
	
	
	function add_email_image()
	{
		$image_settings = image_setting();	
		
			
		if($_FILES)
		{
			
			if($_FILES['image_file_up']['name']!='')
			{
				 
			 $this->load->library('upload');
			 $rand=randomCode(); 
			 $rand=unique_image_code($rand); 
			  
			 $_FILES['userfile']['name']     =   $_FILES['image_file_up']['name'];
			 $_FILES['userfile']['type']     =   $_FILES['image_file_up']['type'];
			 $_FILES['userfile']['tmp_name'] =   $_FILES['image_file_up']['tmp_name'];
			 $_FILES['userfile']['error']    =   $_FILES['image_file_up']['error'];
			 $_FILES['userfile']['size']     =   $_FILES['image_file_up']['size'];
  
			
			 $config['file_name']     = $rand;
			 $config['upload_path'] = base_path().'upload/image_orig/';
			 $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';  
			 $config['overwrite']= TRUE;
 
			 $this->upload->initialize($config);
 
			  if (!$this->upload->do_upload())
			  {
				$error =  $this->upload->display_errors();   
			  } 
			   
			   
			  $picture = $this->upload->data();		
			  $image_name=$picture['file_name'];
				
				$this->load->library('image_lib');					
				$this->image_lib->clear();	
			
				$config['image_library'] = 'GD2';
				$config['thumb_marker'] = "";
				$config['maintain_ratio'] = TRUE;
				$config['create_thumb'] = TRUE;
				$config['overwrite']= TRUE;
				$config['quality'] = '100%';
				$config['master_dim'] = 'width';
				$config['source_image'] = $picture['full_path'];
				$config['new_image'] = base_path().'upload/image_thumb/'.$picture['file_name'];					
				$config['width'] = $image_settings->image_small_thumb_width;
				$config['height'] = $image_settings->image_small_thumb_height;
				$this->image_lib->initialize($config);
			
			
				
				if(!$this->image_lib->resize()){
					$error = $this->image_lib->display_errors();				
				}
							
					
					
				$this->image_lib->clear();
					
				
				$config['image_library'] = 'GD2';
				$config['thumb_marker'] = "";
				$config['maintain_ratio'] = TRUE;
				$config['create_thumb'] = TRUE;
				$config['overwrite']= TRUE;
				$config['quality'] = '100%';
				$config['master_dim'] = 'width';
				$config['source_image'] = $picture['full_path'];
				$config['new_image'] = base_path().'upload/image_medium/'.$picture['file_name'];
				$config['width'] = $image_settings->image_medium_thumb_width;
				$config['height'] = $image_settings->image_medium_thumb_height;
				$this->image_lib->initialize($config);
		
				if(!$this->image_lib->resize()){
					$error = $this->image_lib->display_errors();				
				}
						
						
				$this->image_lib->clear();	
				
				$filesize = 0;	
				$file_size=$_FILES['image_file_up']['size'];	

				if($file_size>0)
				{	
					$filesize = ($file_size * .0009765625) * .0009765625; 
					
					$filesize1 = number_format($filesize,2); 
					if($filesize1>0) {  $filesize=$filesize1; } else { $filesize =  number_format($filesize,3);    }
				}
						
					
				$image_add= array(
					'image_size' => $filesize,
					'image_original_name' => $_FILES['image_file_up']['name'],
					'image_name' => $image_name,
					'image_user_id'=>$this->input->post('user_id'),
					'image_active'=>1,
					'image_ip'=>$_SERVER['REMOTE_ADDR'],
					'image_date'=>date('Y-m-d H:i:s'),
					'image_unique_code'=>$rand,
					'image_delete_code'=>randomCode(),
					'image_is_public'=>0					
				);
				$this->db->insert('images',$image_add);
				
		}
		
		
		
		}
	
	}
	
	
	/***************************/
	
	function get_total_search_image_count($option,$keyword)
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		$this->db->select('*');
		$this->db->from('images img');
		$this->db->join('user us','us.user_id = img.image_user_id','left');	
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
		
		$this->db->order_by('img.image_id','desc');
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();		
		}
		
		return 0;
	}
	
	function get_all_search_image_result($option,$keyword,$offset, $limit)
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		$this->db->select('*');
		$this->db->from('images img');
		$this->db->join('user us','us.user_id = img.image_user_id','left');
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
		
		
		
		$this->db->order_by('img.image_id','desc');
		$this->db->limit($limit,$offset);
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}
		
		return 0;
	}
	
	
	
	function get_total_image_count()
	{
		$this->db->select('*');
		$this->db->from('images img');
		$this->db->join('user us', 'us.user_id = img.image_user_id','left');
		$this->db->join('user_profile up', 'us.user_id = up.user_id','left');
		$this->db->order_by('img.image_id','desc');
				
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();		
		}
		
		return 0;
	}
	
	function get_all_image_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('images img');
		$this->db->join('user us', 'us.user_id = img.image_user_id','left');
		$this->db->join('user_profile up', 'us.user_id = up.user_id','left');
		$this->db->order_by('img.image_id','desc');
		$this->db->limit($limit,$offset);
		
		$query=$this->db->get();
		
		
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}
		
		return 0;
	}
	
	function update_image()
	{
	
		$image_settings = image_setting();	
		
		$this->db->select('*');
		$this->db->from('images im');
		$this->db->join('user us','im.image_user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->where('im.image_id',$this->input->post('image_id'));
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();		
				

			if($_FILES['image_file_up']['name']!='')
			{
				 
			 $this->load->library('upload');
			 $rand=$res->image_unique_code; 
			  
			 $_FILES['userfile']['name']     =   $_FILES['image_file_up']['name'];
			 $_FILES['userfile']['type']     =   $_FILES['image_file_up']['type'];
			 $_FILES['userfile']['tmp_name'] =   $_FILES['image_file_up']['tmp_name'];
			 $_FILES['userfile']['error']    =   $_FILES['image_file_up']['error'];
			 $_FILES['userfile']['size']     =   $_FILES['image_file_up']['size'];
  
			 
			 $config['file_name']     = $rand;
			 $config['upload_path'] = base_path().'upload/image_orig/';
			 $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';  
			 $config['overwrite']= TRUE;
 
			 $this->upload->initialize($config);
 
			  if (!$this->upload->do_upload())
			  {
				$error =  $this->upload->display_errors();   
			  } 
			   
			   
			  $picture = $this->upload->data();		
			  $image_name=$picture['file_name'];
				
				$this->load->library('image_lib');					
				$this->image_lib->clear();	
			
				$config['image_library'] = 'GD2';
				$config['thumb_marker'] = "";
				$config['maintain_ratio'] = TRUE;
				$config['create_thumb'] = TRUE;
				$config['overwrite']= TRUE;
				$config['quality'] = '100%';
				$config['master_dim'] = 'width';
				$config['source_image'] = $picture['full_path'];
				$config['new_image'] = base_path().'upload/image_thumb/'.$picture['file_name'];					
				$config['width'] = $image_settings->image_small_thumb_width;
				$config['height'] = $image_settings->image_small_thumb_height;
				$this->image_lib->initialize($config);
			
			
				
				if(!$this->image_lib->resize()){
					$error = $this->image_lib->display_errors();				
				}
							
					
					
				$this->image_lib->clear();
					
				
				$config['image_library'] = 'GD2';
				$config['thumb_marker'] = "";
				$config['maintain_ratio'] = TRUE;
				$config['create_thumb'] = TRUE;
				$config['overwrite']= TRUE;
				$config['quality'] = '100%';
				$config['master_dim'] = 'width';
				$config['source_image'] = $picture['full_path'];
				$config['new_image'] = base_path().'upload/image_medium/'.$picture['file_name'];
				$config['width'] = $image_settings->image_medium_thumb_width;
				$config['height'] = $image_settings->image_medium_thumb_height;
				$this->image_lib->initialize($config);
		
				if(!$this->image_lib->resize()){
					$error = $this->image_lib->display_errors();				
				}
						
						
				$this->image_lib->clear();	
				
				$filesize = 0;	
				$file_size=$_FILES['image_file_up']['size'];	

				if($file_size>0)
				{	
					$filesize = ($file_size * .0009765625) * .0009765625; 
					
					$filesize1 = number_format($filesize,2); 
					if($filesize1>0) {  $filesize=$filesize1; } else { $filesize =  number_format($filesize,3);    }
				}
							
				$image_update= array(
					'image_size' => $filesize,
					'image_original_name' => $_FILES['image_file_up']['name'],
					'image_name' => $image_name
				);
				$this->db->where('image_id',$this->input->post('image_id'));
				$this->db->update('images',$image_update);
				
		}
		
		
		/////////////===url part
		
			$new_image_title =$this->input->post('image_title');
			
			
				$new_image_url_title_org='';
				
			
				$image_url_title=$res->image_url_title;
				
				$image_url_title_org=$res->image_url_title_org;
				
		
				if($new_image_title!='')
				{
					
					
					////====
				
					$new_image_url_title=clean_url($new_image_title);
					
					$comparestr=strcmp($image_url_title_org,$new_image_url_title);
					
					$new_image_url_title_org=$new_image_url_title;
					
				
					if($comparestr!=0)
					{
							
						$chk_url_exists=$this->db->query("select * from ".$this->db->dbprefix('images')." where image_unique_code!='".$res->image_unique_code."' and image_url_title like '".$new_image_url_title."%' ");
						
					$image_url_title=$new_image_url_title;
					
						$temp=array();
					
						if($chk_url_exists->num_rows()>0)
						{			
						
							$get_pr=$chk_url_exists->result();					
						
							foreach($get_pr as $row)
							{
							$strre='0';
							if($row->image_url_title!='')
							{
								$strre=str_replace($new_image_url_title,'',$row->image_url_title);
							}
							
							if($strre=='0') 
							{					
								$temp[$row->image_url_title]=0;
												
							} 
							elseif($strre=='') 
							{					
								$temp[$row->image_url_title]=0;		
							}
							else
							{				
								$temp[$row->image_url_title]=(int)$strre;			
							}						
							
						}
						
							$newcnt='';
							if($temp)
							{
								array_unique($temp);
								arsort($temp);
								
							
								
								
								if(array_key_exists($new_image_url_title,$temp))
								{
								
								//} else {
								
								
								
									
								$first_key= key($temp);
									
									$first_comp=strcmp($new_image_url_title,$first_key);
								
									if($first_comp!=0)
									{
										$strre=str_replace($new_image_url_title,'',$first_key);
									}
									
									$newcnt=(int)$strre+1;		
									
								}
								
								
							}
							
							$image_url_title=$new_image_url_title.$newcnt;
									
						}
						
					}
					
					////===
					
				}
			
			
			
			
		/////////////===url part	
			
			
			
			$description=trim(strip_tags($this->input->post('image_description')));
			
			$data=array(	
				'image_description'=>$description,
				'image_title'=>$new_image_title,
				'image_show_on_home'=>$this->input->post('show_on_home_page'),
				'image_url_title'=>$image_url_title,
				'image_url_title_org' => $new_image_url_title_org
			);
			$this->db->where('image_id',$this->input->post('image_id'));
			$this->db->update('images',$data);
		
		}
		
		
	}
	
	/*
	Function name : delete_image()
	Parameter : $image_id
	Return : none
	Use : delete image
	*/
	
	function delete_image($image_id)
	{
		///////===check image
		$check_image=$this->db->get_where('images',array('image_id'=>$image_id));
		
		if($check_image->num_rows()>0)
		{
			
				$image_info=$check_image->row();
			
				
				///====image delete
				
				if(file_exists(base_path().'upload/image_orig/'.$image_info->image_name))
				{
					unlink(base_path().'upload/image_orig/'.$image_info->image_name);
				}
				if(file_exists(base_path().'upload/image_thumb/'.$image_info->image_name))
				{
					unlink(base_path().'upload/image_thumb/'.$image_info->image_name);
				}
				
				if(file_exists(base_path().'upload/image_medium/'.$image_info->image_name))
				{
					unlink(base_path().'upload/image_medium/'.$image_info->image_name);
				}
				
				///====image like
				
				$check_image_like=$this->db->get_where('image_like',array('image_id'=>$image_info->image_id));
				
				if($check_image_like->num_rows()>0)
				{
					$this->db->delete('image_like',array('image_id'=>$image_info->image_id));				
				}
				
				///===image comment 
				
				$check_image_comment=$this->db->get_where('image_comment',array('image_id'=>$image_info->image_id));
				
				if($check_image_comment->num_rows()>0)
				{
					$image_comment=$check_image_comment->result();
					
					foreach($image_comment as $imgcmt)
					{
						
						///=====check image comment like
						
						$check_comment_like=$this->db->get_where('image_comment_like',array('comment_id'=>$imgcmt->image_comment_id));
						
						if($check_comment_like->num_rows()>0)
						{
							$this->db->delete('image_comment_like',array('comment_id'=>$imgcmt->image_comment_id));	
						}
						
						////====check comment report
						
						$check_comment_report=$this->db->get_where('block_report',array('comment_id'=>$imgcmt->image_comment_id));
						
						if($check_comment_report->num_rows()>0)
						{
							$this->db->delete('block_report',array('comment_id'=>$imgcmt->image_comment_id));	
						}
						
						////=====
						
					}
					
					///===delete all comment
					$this->db->delete('image_comment',array('image_id'=>$image_info->image_id));				
				}
				
				
				///==== album images
				
				$check_album_image=$this->db->get_where('album_image',array('image_id'=>$image_info->image_id));
				
				if($check_album_image->num_rows()>0)
				{
					$this->db->delete('album_image',array('image_id'=>$image_info->image_id));				
				}
				
				
				
				////====check image report
						
				$check_image_report=$this->db->get_where('block_report',array('image_id'=>$image_info->image_id));
				
				if($check_image_report->num_rows()>0)
				{
					$this->db->delete('block_report',array('image_id'=>$image_info->image_id));	
				}
				
				//////=================all image related part deleted====
				
				$this->db->delete('images',array('image_id'=>$image_info->image_id));	
				
			
		
		}
	}
	
	function get_list_comment_detail_result($image_id,$offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('image_comment ic');
		$this->db->join('user us','ic.user_id=us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('images img','ic.image_id=img.image_id');
		$this->db->where('img.image_id',$image_id);
		$this->db->order_by('ic.image_comment_id','desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get(); 
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	function get_total_comment_count($image_id)
	{
		$this->db->select('*');
		$this->db->from('image_comment ic');
		$this->db->join('user us','ic.user_id=us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('images img','ic.image_id=img.image_id');
		$this->db->where('img.image_id',$image_id);
		$this->db->order_by('ic.image_comment_id','desc');
		
		$query = $this->db->get(); 
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_total_search_comment_count($image_id,$option,$keyword)
	{
	 
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
	
		
		$this->db->select('*');
		$this->db->from('image_comment imc');
		$this->db->join('images im','imc.image_id=im.image_id');
		$this->db->join('user us','imc.user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->where('imc.image_id',$image_id);

		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
		
		
		$this->db->order_by('imc.image_comment_id','desc');
				
		$query=$this->db->get();
		
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();		
		}
		
		return 0;
	
	}
	
	function get_all_search_comment_result($image_id,$option,$keyword,$offset, $limit)
	{
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		$this->db->select('*');
		$this->db->from('image_comment imc');
		$this->db->join('images im','imc.image_id=im.image_id');
		$this->db->join('user us','imc.user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->where('imc.image_id',$image_id);
		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
		
		
		
		$this->db->order_by('imc.image_comment_id','desc');
	
		$this->db->limit($limit,$offset);
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}
		
		return 0;
	
	}
	
	
	
	///////////////////////////====================request deletion ==================
	
	function get_total_image_delete_request_count()
	{
		
		
		return $this->db->count_all('image_delete_request');
	}
	
	function get_image_delete_request_count($offset, $limit)
	{
		
		$this->db->select('*');
		$this->db->from('image_delete_request');
		$this->db->order_by("img_delete_request_id", "desc");		
		$this->db->limit($limit,$offset);
		
		
		$query=$this->db->get();

		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}
		
		   return 0;
	}
	
	
	function get_delete_request_by_id($img_delete_request_id)
	{
		
		$query=$this->db->get_where('image_delete_request',array('img_delete_request_id'=>$img_delete_request_id));
		
		if($query->num_rows()>0)
		{
			return $query->row();		
		}
		
		   return 0;
		
	}
	
	
	function get_total_search_delete_image_request_count($option,$keyword)
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		$this->db->select('*');
		$this->db->from('image_delete_request img');
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
		
		$this->db->order_by('img.img_delete_request_id','desc');
				
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();		
		}
		
		return 0;
	}
	
	function get_all_search_delete_image_request_result($option,$keyword,$offset, $limit)
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		$this->db->select('*');
		$this->db->from('image_delete_request img');
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
		
		
	
		
		$this->db->order_by('img.img_delete_request_id','desc');
		$this->db->limit($limit,$offset);
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}
		
		return 0;
	}
	
	

}
?>