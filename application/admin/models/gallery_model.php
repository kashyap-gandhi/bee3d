<?php

class Gallery_model extends CI_Model {

    function Gallery_model() {
        parent::__construct();
    }

    function check_gallery_have_sub_gallery($gallery_id) {
        $query = $this->db->get_where('gallery', array('gallery_id ' => $gallery_id));
        if ($query->num_rows() > 0) {
            return 1;
        }
        return 0;
    }

    function get_total_gallery_count() {
        return $this->db->count_all('gallery');
    }

    function get_gallery_result($offset, $limit) {
        $query = $this->db->query("select * from " . $this->db->dbprefix('gallery') . " c order by gallery_id desc limit " . $limit . " offset " . $offset);


        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function gallery_insert() {
        $gallery_image = '';
        $image_settings = image_setting();

        if ($_FILES['file_up']['name'] != '') {
            $this->load->library('upload');
            $rand = rand(0, 100000);

            $_FILES['userfile']['name'] = $_FILES['file_up']['name'];
            $_FILES['userfile']['type'] = $_FILES['file_up']['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES['file_up']['tmp_name'];
            $_FILES['userfile']['error'] = $_FILES['file_up']['error'];
            $_FILES['userfile']['size'] = $_FILES['file_up']['size'];



            $config['file_name'] = $rand . 'gallery';
            $config['upload_path'] = base_path() . 'upload/gallery_orig/';
            //$config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
            $config['allowed_types'] = '*';

            $this->upload->initialize($config);

            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
            }


            $picture = $this->upload->data();

            $this->load->library('image_lib');

            $this->image_lib->clear();


            $gd_var = 'gd2';


            if ($_FILES["file_up"]["type"] != "image/png" and $_FILES["file_up"]["type"] != "image/x-png") {

                $gd_var = 'gd2';
            }


            if ($_FILES["file_up"]["type"] != "image/gif") {

                $gd_var = 'gd2';
            }


            if ($_FILES["file_up"]["type"] != "image/jpeg" and $_FILES["file_up"]["type"] != "image/pjpeg") {

                $gd_var = 'gd2';
            }

            $this->image_lib->initialize(array(
                'thumb_marker' => "",
                'image_library' => $gd_var,
                'source_image' => base_path() . 'upload/gallery_orig/' . $picture['file_name'],
                'new_image' => base_path() . 'upload/gallery/' . $picture['file_name'],
                'maintain_ratio' => TRUE,
                'quality' => '100%',
                'create_thumb' => TRUE,
                'master_dim' => 'width',
                'width' => $image_settings->design_width,
                'height' => $image_settings->design_height,
            ));


            if (!$this->image_lib->resize()) {
                $error = $this->image_lib->display_errors();
            }

            $gallery_image = $picture['file_name'];
            
             $data = array(
                'gallery_title' => $this->input->post('gallery_title'),
                'gallery_status' => $this->input->post('gallery_status'),
                'gallery_order' => $this->input->post('gallery_order'),
                'gallery_image' => $gallery_image
            );
            $this->db->insert('gallery', $data);
        
        }





       
    }

    function gallery_update() {


        $gallery_image = '';
        $image_settings = image_setting();

        if ($_FILES['file_up']['name'] != '') {
            $this->load->library('upload');
            $rand = rand(0, 100000);

            $_FILES['userfile']['name'] = $_FILES['file_up']['name'];
            $_FILES['userfile']['type'] = $_FILES['file_up']['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES['file_up']['tmp_name'];
            $_FILES['userfile']['error'] = $_FILES['file_up']['error'];
            $_FILES['userfile']['size'] = $_FILES['file_up']['size'];



            $config['file_name'] = $rand . 'gallery';
            $config['upload_path'] = base_path() . 'upload/gallery_orig/';
            //$config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
            $config['allowed_types'] = '*';

            $this->upload->initialize($config);

            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
            }


            $picture = $this->upload->data();

            $this->load->library('image_lib');

            $this->image_lib->clear();


            $gd_var = 'gd2';


            if ($_FILES["file_up"]["type"] != "image/png" and $_FILES["file_up"]["type"] != "image/x-png") {

                $gd_var = 'gd2';
            }


            if ($_FILES["file_up"]["type"] != "image/gif") {

                $gd_var = 'gd2';
            }


            if ($_FILES["file_up"]["type"] != "image/jpeg" and $_FILES["file_up"]["type"] != "image/pjpeg") {

                $gd_var = 'gd2';
            }

            $this->image_lib->initialize(array(
                'thumb_marker' => "",
                'image_library' => $gd_var,
                'source_image' => base_path() . 'upload/gallery_orig/' . $picture['file_name'],
                'new_image' => base_path() . 'upload/gallery/' . $picture['file_name'],
                'maintain_ratio' => TRUE,
                'quality' => '100%',
                'create_thumb' => TRUE,
                'master_dim' => 'width',
                'width' => $image_settings->design_width,
                'height' => $image_settings->design_height,
            ));


            if (!$this->image_lib->resize()) {
                $error = $this->image_lib->display_errors();
            }

            $gallery_image = $picture['file_name'];


            if ($this->input->post('prev_gallery_image') != '') {
                if (file_exists(base_path() . 'upload/gallery/' . $this->input->post('prev_gallery_image'))) {
                    $link = base_path() . 'upload/gallery/' . $this->input->post('prev_gallery_image');
                    unlink($link);
                }
                if (file_exists(base_path() . 'upload/gallery_orig/' . $this->input->post('prev_gallery_image'))) {
                    $link2 = base_path() . 'upload/gallery_orig/' . $this->input->post('prev_gallery_image');
                    unlink($link2);
                }
            }
        } else {
            if ($this->input->post('prev_gallery_image') != '') {
                $gallery_image = $this->input->post('prev_gallery_image');
            }
        }

        $data = array(
            'gallery_title' => $this->input->post('gallery_title'),
            'gallery_status' => $this->input->post('gallery_status'),
            'gallery_order' => $this->input->post('gallery_order'),
            'gallery_image' => $gallery_image
        );
        $this->db->where('gallery_id', $this->input->post('gallery_id'));
        $this->db->update('gallery', $data);
    }

    function get_one_gallery($id) {
        $query = $this->db->get_where('gallery', array('gallery_id' => $id));
        return $query->row_array();
    }

}

?>