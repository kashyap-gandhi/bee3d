<?php

class Quote_model extends CI_Model {
	
    function Quote_model()
    {
        parent::__construct();	
    }   
	
	/*** get total quote 
	*  return number
	**/
	function get_total_quote_count()
	{
		//return $this->db->count_all('user');
		$query=$this->db->query("select * from ".$this->db->dbprefix('trip_quote')."");
		
		if($query->num_rows()>0)
		{	
			return $query->num_rows();
		}
		
		return 0;
	}
		
/*** get quote details
	*  return multiple records array
	**/
	function get_quote_result($offset, $limit)
	{
		
		$query = $this->db->query("select * from ".$this->db->dbprefix('trip_quote')." order by quote_id desc limit ".$limit." offset ".$offset);
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}	
	
	
	/*** get single user details
	*  return single record array
	**/
	function get_one_quote($id)
	{
		$this->db->select('*');
		$this->db->from('trip_quote tq');
		$this->db->where('tq.quote_id',$id);
		$query=$this->db->get();
		if($query->num_rows()>0)
		{		
			return $query->row();
		}
		
		return 0;
	}	
	
	function approve_quote($quote_id='')
	{
		$data=array('trip_quote_approve'=>1);	
		$this->db->where('quote_id',$quote_id);
		$this->db->update('trip_quote',$data);		
	}
	
	/*Search Start*/
	
	/*** get total user 
	*  return number
	**/
	function get_total_search_quote_count($option,$keyword)
	{
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		$this->db->select('trip_quote.*');
		$this->db->from('trip_quote');
		
		
				
		if($option=='from')
		{
			$this->db->like('trip_quote.trip_from_place',$keyword);
			$this->db->or_like('trip_quote.airline_from',$keyword);
			
			if(substr_count($keyword,' ')>=1)
			{
				$ex=explode(' ',$keyword);
				
				foreach($ex as $val)
				{
					$this->db->like('trip_quote.trip_from_place',$val);
					$this->db->or_like('trip_quote.airline_from',$val);
				}	
			}

			
		}
		if($option=='to')
		{
			$this->db->like('trip_quote.trip_to_place',$keyword);
			$this->db->or_like('trip_quote.airline_to',$keyword);
			
			if(substr_count($keyword,' ')>=1)
			{
				$ex=explode(' ',$keyword);
				
				foreach($ex as $val)
				{
					$this->db->like('trip_quote.trip_to_place',$val);
					$this->db->or_like('trip_quote.airline_to',$val);
				}	
			}

			
		}
		
		$this->db->order_by("trip_quote.quote_id", "desc"); 
		$query = $this->db->get();
		
		return $query->num_rows();
	}
	
	
		/*** get users details
	*  return multiple records array
	**/
	function get_search_quote_result($option,$keyword,$offset, $limit)
	{
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		$this->db->select('trip_quote.*');
		$this->db->from('trip_quote');
		
		
				
		if($option=='from')
		{
			$this->db->like('trip_quote.trip_from_place',$keyword);
			$this->db->or_like('trip_quote.airline_from',$keyword);
			
			
			if(substr_count($keyword,' ')>=1)
			{
				$ex=explode(' ',$keyword);
				
				foreach($ex as $val)
				{
					$this->db->like('trip_quote.trip_from_place',$val);
					$this->db->or_like('trip_quote.airline_from',$val);
				}	
			}

			
		}
		if($option=='to')
		{
			$this->db->like('trip_quote.trip_to_place',$keyword);
			$this->db->or_like('trip_quote.airline_to',$keyword);
			
			if(substr_count($keyword,' ')>=1)
			{
				$ex=explode(' ',$keyword);
				
				foreach($ex as $val)
				{
					$this->db->like('trip_quote.trip_to_place',$val);
					$this->db->or_like('trip_quote.airline_to',$val);
				}	
			}

			
		}
		
		$this->db->order_by("trip_quote.quote_id", "desc"); 
		$this->db->limit($limit,$offset);
		
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			//return $query->result_array();
			return $query->result();
		}
		return 0;
	}
	
	/*Search End*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
/**************************************************************************************/
	function deactivateuser($user_id)
	{
	
	///===user own things
	
	
	///images, image_like, image_comment, comment_like , block_report, 
	
		$check_user_image=$this->db->get_where('images',array('image_user_id'=>$user_id));
		
		if($check_user_image->num_rows()>0)
		{
			$res_img=$check_user_image->result();
			
			
			foreach($res_img as $image_info)
			{
			
				
			
				
				///====image delete
				
				if(file_exists(base_path().'upload/image_orig/'.$image_info->image_name))
				{
					unlink(base_path().'upload/image_orig/'.$image_info->image_name);
				}
				if(file_exists(base_path().'upload/image_thumb/'.$image_info->image_name))
				{
					unlink(base_path().'upload/image_thumb/'.$image_info->image_name);
				}
				
				if(file_exists(base_path().'upload/image_medium/'.$image_info->image_name))
				{
					unlink(base_path().'upload/image_medium/'.$image_info->image_name);
				}
				
				///====image like
				
				$check_image_like=$this->db->get_where('image_like',array('image_id'=>$image_info->image_id));
				
				if($check_image_like->num_rows()>0)
				{
					$this->db->delete('image_like',array('image_id'=>$image_info->image_id));				
				}
				
				///===image comment 
				
				$check_image_comment=$this->db->get_where('image_comment',array('image_id'=>$image_info->image_id));
				
				if($check_image_comment->num_rows()>0)
				{
					$image_comment=$check_image_comment->result();
					
					foreach($image_comment as $imgcmt)
					{
						
						///=====check image comment like
						
						$check_comment_like=$this->db->get_where('image_comment_like',array('comment_id'=>$imgcmt->image_comment_id));
						
						if($check_comment_like->num_rows()>0)
						{
							$this->db->delete('image_comment_like',array('comment_id'=>$imgcmt->image_comment_id));	
						}
						
						////====check comment report
						
						$check_comment_report=$this->db->get_where('block_report',array('comment_id'=>$imgcmt->image_comment_id));
						
						if($check_comment_report->num_rows()>0)
						{
							$this->db->delete('block_report',array('comment_id'=>$imgcmt->image_comment_id));	
						}
						
						////=====
						
					}
					
					///===delete all comment
					$this->db->delete('image_comment',array('image_id'=>$image_info->image_id));				
				}
				
				
				///==== album images
				
				$check_album_image=$this->db->get_where('album_image',array('image_id'=>$image_info->image_id));
				
				if($check_album_image->num_rows()>0)
				{
					$this->db->delete('album_image',array('image_id'=>$image_info->image_id));				
				}
				
				
				
				////====check image report
						
				$check_image_report=$this->db->get_where('block_report',array('image_id'=>$image_info->image_id));
				
				if($check_image_report->num_rows()>0)
				{
					$this->db->delete('block_report',array('image_id'=>$image_info->image_id));	
				}
				
				//////=================all image related part deleted====
				
				$this->db->delete('images',array('image_id'=>$image_info->image_id));	
				
			
		
		
		
			}
			
		}
	
	
	
	
	
	///===album, album_images, album report
	
	$check_user_album=$this->db->get_where('album',array('user_id'=>$user_id));
	
	if($check_user_album->num_rows()>0)
	{
		
		
		$res_album=$check_user_album->result();
		
		
		foreach($res_album as $rabl)
		{
			
						
			/////////====album images
			
			$check_album_images=$this->db->get_where('album_image',array('album_id'=>$rabl->album_id));
	
			if($check_album_images->num_rows()>0)
			{
				$this->db->delete('album_image',array('album_id'=>$rabl->album_id));
			}
			
			
			////===========album report			
			
			$check_reported_album=$this->db->get_where('block_report',array('album_id'=>$rabl->album_id));
	
			if($check_reported_album->num_rows()>0)
			{
				$this->db->delete('block_report',array('album_id'=>$rabl->album_id));
			}
			
			
		
		}
		
		
		
		///===delete all album
		
		$this->db->delete('album',array('user_id'=>$user_id));
			
	}
	
	
	
	
	

	///===user own things
	
	
	
	

	
	///===check user image comment related sub comment, comment report, comment like, replace replay_comment_id to 0
	
	$check_user_image_comment=$this->db->get_where('image_comment',array('user_id'=>$user_id));
	
	if($check_user_image_comment->num_rows()>0)
	{
		
		
		$res_comment=$check_user_image_comment->result();
		
		
		foreach($res_comment as $rcm)
		{
			
			
			/////====replace comment id to 0
			
			$check_reply_message=$this->db->get_where('image_comment',array('replay_comment_id'=>$rcm->image_comment_id));
			
			if($check_reply_message->num_rows()>0)
			{
				$data_repl=array('replay_comment_id'=>0);
				
				$this->db->where('replay_comment_id',$rcm->image_comment_id);
				$this->db->update('image_comment',$data_repl);			
			}
			
			
			/////////====comment likes
			
			$check_like_comment=$this->db->get_where('image_comment_like',array('comment_id'=>$rcm->image_comment_id));
	
			if($check_like_comment->num_rows()>0)
			{
				$this->db->delete('image_comment_like',array('comment_id'=>$rcm->image_comment_id));
			}
			
			
			////===========comment report			
			
			$check_reported_comment=$this->db->get_where('block_report',array('comment_id'=>$rcm->image_comment_id));
	
			if($check_reported_comment->num_rows()>0)
			{
				$this->db->delete('block_report',array('comment_id'=>$rcm->image_comment_id));
			}
			
			
		
		}
		
		
		
		///===delete all comment
		
		$this->db->delete('image_comment',array('user_id'=>$user_id));
		
		
		
	}
	
	
	///====user like commment
	
	$check_user_like_comment=$this->db->get_where('image_comment_like',array('user_id'=>$user_id));
	
	if($check_user_like_comment->num_rows()>0)
	{
		$this->db->delete('image_comment_like',array('user_id'=>$user_id));
	}
	
	
	
	///=====message  by_user
	
	$check_user_message_by_user=$this->db->get_where('message',array('message_by_user_id'=>$user_id));
	
	if($check_user_message_by_user->num_rows()>0)
	{
		$this->db->delete('message',array('message_by_user_id'=>$user_id));
	}
	
	///=====message  to_user
	
	$check_user_message_to_user=$this->db->get_where('message',array('message_to_user_id'=>$user_id));
	
	if($check_user_message_to_user->num_rows()>0)
	{
		$this->db->delete('message',array('message_to_user_id'=>$user_id));
	}
	
	
	
	
	
	///=====user_block  by_user
	
	$check_user_block_by_user=$this->db->get_where('user_block_user',array('block_by_user_id'=>$user_id));
	
	if($check_user_block_by_user->num_rows()>0)
	{
		$this->db->delete('user_block_user',array('block_by_user_id'=>$user_id));
	}
	
	///=====user_block  to_user
	
	$check_user_block_to_user=$this->db->get_where('user_block_user',array('block_to_user_id'=>$user_id));
	
	if($check_user_block_to_user->num_rows()>0)
	{
		$this->db->delete('user_block_user',array('block_to_user_id'=>$user_id));
	}
	
	
	////=======user_email_activate, 
	
	$check_user_email_activate=$this->db->get_where('user_email_activate',array('user_id'=>$user_id));
	
	if($check_user_email_activate->num_rows()>0)
	{
		$this->db->delete('user_email_activate',array('user_id'=>$user_id));
	}
	
	
	
	
	///===check user image like
	
	$check_user_image_like=$this->db->get_where('image_like',array('user_id'=>$user_id));
	
	if($check_user_image_like->num_rows()>0)
	{
		$this->db->delete('image_like',array('user_id'=>$user_id));
	}
	
	
	
	///===check user report
	
	$check_user_image_report=$this->db->get_where('block_report',array('user_id'=>$user_id));
	
	if($check_user_image_report->num_rows()>0)
	{
		$this->db->delete('block_report',array('user_id'=>$user_id));
	}
	
	
	
	/*user_login
	
	user_notification
	user_profile
	user_package
	user_suspend
	user_suspend_message*/
	


$datauser=array(
	'user_status'=>3,
	'tw_id'=>'',
	'twitter_screen_name'=>'',
	'tw_oauth_token'=>'',
	'tw_oauth_token_secret'=>'',
	'fb_id'=>''
);


$this->db->where('user_id',$user_id);
$this->db->update('user',$datauser);


				
				////==destroy cache====	
				$this->load->driver('cache');			
				
				$supported_cache=check_supported_cache_driver();
				
				////==destroy now====		
				if(isset($supported_cache))
				{
					if($supported_cache!='' && $supported_cache!='none')
					{	
						if($this->cache->$supported_cache->get('user_login'.$user_id))
						{								
							$this->cache->$supported_cache->delete('user_login'.$user_id);						
						}
					}
					
				}
				
				
				
			

		
		
		////========
	}
	
	
	function delete_forever($user_id)
	{
	
		///===check user login
	
		$check_user_login=$this->db->get_where('user_login',array('user_id'=>$user_id));
		
		if($check_user_login->num_rows()>0)
		{
			$this->db->delete('user_login',array('user_id'=>$user_id));
		}
		
		///===check user notification
	
		$check_user_notification=$this->db->get_where('user_notification',array('user_id'=>$user_id));
		
		if($check_user_notification->num_rows()>0)
		{
			$this->db->delete('user_notification',array('user_id'=>$user_id));
		}
	
		
		///===check user suspend
	
		$check_user_suspend=$this->db->get_where('user_suspend',array('user_id'=>$user_id));
		
		if($check_user_suspend->num_rows()>0)
		{
		
			$resup=$check_user_suspend->result();
			
			foreach($resup as $sup)
			{
				
				$check_suspend_msg=$this->db->get_where('user_suspend_message',array('user_suspend_id'=>$sup->user_suspend_id));
		
				if($check_suspend_msg->num_rows()>0)
				{
					$this->db->delete('user_suspend_message',array('user_suspend_id'=>$sup->user_suspend_id));
				}				
				
			}		
			
			$this->db->delete('user_suspend',array('user_id'=>$sup->user_id));
			
		}
		
		
		
		///===check user profile
	
		$check_user_profile=$this->db->get_where('user_profile',array('user_id'=>$user_id));
		
		if($check_user_profile->num_rows()>0)
		{
			$this->db->delete('user_profile',array('user_id'=>$user_id));
		}
		
		
		
		///===check user package
	
		$check_user_profile=$this->db->get_where('user_package',array('user_id'=>$user_id));
		
		if($check_user_profile->num_rows()>0)
		{
			$this->db->delete('user_package',array('user_id'=>$user_id));
		}
		
		
		
	
		///===check user 
	
		$check_user_own=$this->db->get_where('user',array('user_id'=>$user_id));
		
		if($check_user_own->num_rows()>0)
		{
			$this->db->delete('user',array('user_id'=>$user_id));
		}
		
		
		
		
	
		
		
	}
	
	
	/*** check the user unique email address
	*	var string $email
	*  return boolen
	**/

	function emailTaken($email)
	{
	
		if($this->input->post('user_id')>0)
		{
			$query = $this->db->query("select * from ".$this->db->dbprefix('user')." where user_id!='".$this->input->post('user_id')."' and email='".$email."'");
			 
		} else {
		
		 $query = $this->db->query("select * from ".$this->db->dbprefix('user')." where email='".$email."'");
		 
		}	 
		 
		 
		 if($query->num_rows()>0)
		 {
			return true;
		 }
		 else 
		 {
			return false;
		 }		
	}
	
	
	
	
	/**** add user ***/
	
	
	
	function add_user()
	{
	
		$email_verification_code=randomCode();
		
		
		$password=randomCode();
		
		
		
		
		
		$first_name=$this->input->post('first_name');
		$last_name=$this->input->post('last_name');
		
		$profile_name=clean_url($first_name.' '.substr($last_name,0,1));
		
		
			
	$chk_url_exists=$this->db->query("select MAX(profile_name) as profile_name from ".$this->db->dbprefix('user')." where profile_name like '".$profile_name."%'");
		
		if($chk_url_exists->num_rows()>0)
		{			
				$get_pr=$chk_url_exists->row();					
				
				$strre='0';
				if($get_pr->profile_name!='')
				{
					$strre=str_replace($profile_name,'',$get_pr->profile_name);
				}
				
				if($strre=='0') 
				{					
					$newcnt=''; 
					  				
				} 
				elseif($strre=='') 
				{					
					$newcnt='1';   				
				}
				else
				{				
					$newcnt=(int)$strre+1;			
				}
				
			 	$profile_name=$profile_name.$newcnt;
						
		}
			
			
			
		
		
		
		$data = array(		
				'full_name' => $first_name.' '.$last_name,		
				'first_name' => $first_name,
				'last_name' => $last_name,	
				'profile_name'=>$profile_name,			
				'email' => $this->input->post('email'),			
				'password' => md5($password),		
				'sign_up_ip' => $this->input->ip_address(),
				'email_verification_code'=>$email_verification_code,
				'zip_code'=>$this->input->post('zip_code'),
				'mobile_no'=>$this->input->post('mobile_no'),			
				'phone_no'=>$this->input->post('phone_no'),						
				'user_status' =>$this->input->post('user_status'),
				'sign_up_date' => date('Y-m-d H:i:s'),
				); 
		$this->db->insert('user', $data);
		$user_id = mysql_insert_id();
		
		
		/*****create profile****/
		
		$data_profile=array(
		'user_id'=>$user_id,
		'current_city'=>$this->input->post('current_city'),
		'about_user'=>$this->input->post('about_user')
		);
		
		$this->db->insert('user_profile',$data_profile);
		
		
		
		$data_profile=array(
		'user_id'=>$user_id,
		'current_city'=>$this->input->post('current_city'),
		'about_user'=>$this->input->post('about_user')
		);
		
		$this->db->insert('user_profile',$data_profile);
		
		
		
		
			/*** user notification ****/
	
		$user_notification=mysql_query("SHOW COLUMNS FROM ".$this->db->dbprefix('user_notification'));
		$res=mysql_fetch_array($user_notification);
		$fields="";
		$values="";
				
		while($res=mysql_fetch_array($user_notification)){
		//print_r($res['Field']);echo '<br>';
		if($fields==""){$fields.="(`".$res['Field']."`"; $values.="('".$user_id."'";}
		else {$fields.=",`".$res['Field']."`";	 $values.=",'1'";}
		}
		$fields.=")";
		 $values.=")";								   
		$insert_val= $fields.' values '.$values;
		
		$this->db->query("insert into ".$this->db->dbprefix('user_notification')." ".$insert_val."");
		
	
		/*******************/
	
	
	
		return $password;
		
		
		
	}
	
	
	
	
	/*** get single user details
	*  return single record array
	**/
	function get_one_user($id)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->where('us.user_id',$id);
		$query=$this->db->get();
		if($query->num_rows()>0)
		{		
			return $query->row();
		}
		
		return 0;
	}	
	
	
	/*** get user location details
	*  return single record array
	**/
	function get_user_location($id)
	{
		$query = $this->db->get_where('user_location',array('user_id'=>$id));
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		return 0;
	}	
	

	
	
	
	
	
	
		/*** get total user 
	*  return number
	**/
	function get_total_waiting_user_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('user_request')." where request_email not in(select email from ".$this->db->dbprefix('user').") group by request_email order by request_id desc");
		
		if($query->num_rows()>0)
		{	
			return $query->num_rows();
		}
		
		return 0;
	}
	
	
	/*** get users details
	*  return multiple records array
	**/
	function get_waiting_user_result($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('user_request')." where request_email not in(select email from ".$this->db->dbprefix('user').") group by request_email order by request_id desc limit ".$limit." offset ".$offset);
		
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
	function send_join_request($request_id)
	{
		
		$this->db->select('*');
		$this->db->from('user_request');
		$this->db->where('request_id',$request_id);
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			
			$res=$query->row();
			
			if($res->request_code!='')
			{
				$code=$res->request_code;
			}
			else
			{				
				$code=randomCode();
				
				$data=array(
				'request_code'=>$code
				);
				
				$this->db->where('request_id',$request_id);
				$this->db->update('user_request',$data);
			}
			
			
			/////////////============joining email===========	

				
				$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Join Pinterest'");
				$email_temp=$email_template->row();
				
				
				$email_address_from=$email_temp->from_address;
				$email_from_name=$email_temp->from_name;
				$email_address_reply=$email_temp->reply_address;
				
				$email_subject=$email_temp->subject;				
				$email_message=$email_temp->message;			
				
				$email=$res->request_email;
				
				$email_to=$email;
				
				$invite_link=front_base_url().'invited/'.$email.'/'.$code;				
				$email_link='<a href="'.$invite_link.'">'.$invite_link.'</a>';
									
				$email_message=str_replace('{break}','<br/>',$email_message);
				$email_message=str_replace('{create_link}',$email_link,$email_message);	
				$email_message=str_replace('{email}',$email,$email_message);
								
				$str=$email_message;	
									
				/** custom_helper email function **/
					
				email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
				
				////=====
			
		}
		
		
	}
	
	
	
	
	
	function get_total_active_user_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('user')." where user_status=1");
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	
	/*** get users details
	*  return multiple records array
	**/
	function get_active_user_result($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('user')." where user_status=1 order by user_id desc limit ".$limit." offset ".$offset);
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
	
	
	function get_total_inactive_user_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('user')." where user_status=0");
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	
	/*** get users details
	*  return multiple records array
	**/
	function get_inactive_user_result($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('user')." where user_status=0 order by user_id desc limit ".$limit." offset ".$offset);
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
	
	
	function get_total_delete_user_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('use')."r where user_status=3");
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	
	/*** get users details
	*  return multiple records array
	**/
	function get_delete_user_result($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('user')." where user_status=3 order by user_id desc limit ".$limit." offset ".$offset);
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
	

	
	
	/***** show all uploaded photos of a user
	*N
	***/
	
	function get_user_portfolio_photo($user_id)
	{
		$query=$this->db->get_where('user_portfolio',array('user_id' => $user_id,'portfolio_image !=' => ''));
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
		
	}
	
	
	/***** show all uploaded photos of a user
	*N
	***/
	
	function get_user_portfolio_video($user_id)
	{
		$query=$this->db->get_where('user_portfolio',array('user_id' => $user_id,'portfolio_video !=' => ''));
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
		
	}
	
	function get_total_userlogin_count()
	{
		$this->db->select('*');
		$this->db->from('user_login');
		$this->db->join('user', 'user_login.user_id= user.user_id','left');
	
		$this->db->order_by('user_login.login_id','desc');
		$query = $this->db->get();


		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_userlogin_result($offset, $limit)
	{
		//$query = $this->db->get('user_login',$limit,$offset);
		$this->db->select('*');
		$this->db->from('user_login');
		$this->db->join('user', 'user_login.user_id= user.user_id','left');
		$this->db->limit($limit,$offset);
		$this->db->order_by('user_login.login_id','desc');
		$query = $this->db->get();


		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
	function check_worker($user_id){
	
		$this->db->select('*');
		$this->db->from('worker');
		$this->db->where('worker.user_id',$user_id);
		$this->db->join('user', 'user.user_id= worker.user_id','left');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->row();
		}
		return 0;
	}
	
	
	/*** get total suspend user 
	*  return number
	**/
	
	function get_total_suspend_user_count()
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('user_suspend spu','us.user_id=spu.user_id');
		$this->db->where('us.user_status',2);
		$this->db->group_by('spu.user_id');
		$this->db->order_by('spu.user_suspend_id','desc');
		$query=$this->db->get();
		return $query->num_rows();
		
	}
	
	
	/*** get suspend users details
	*  return multiple records array
	**/
	function get_suspend_user_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('user_suspend spu','us.user_id=spu.user_id');
		$this->db->where('us.user_status',2);
		$this->db->group_by('spu.user_id');
		$this->db->order_by('spu.user_suspend_id','desc');
		$this->db->limit($limit,$offset);
		$query=$this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	function get_suspend_user_count($user_id){
	
		$this->db->select('*');
		$this->db->from('user_suspend us');
		$this->db->where('us.user_id',$user_id);
		$query=$this->db->get();
		return $query->num_rows();
	}

    function get_comment_detail()
	{
	  return $this->db->count_all('image_comment');
	}
	
	
	
	function comment_detail_update()
	{
	   $data = array(
			'user_comment' => $this->input->post('user_comment'),
			
		);		
		
		//print_r ($data);
		//echo  $this->input->post('pin_comment_id');
		//die;
		
	
		//$this->db->where('pin_comment_id',array('pin_comment_id'=>$id));
		$this->db->where('image_comment_id',$this->input->post('image_comment_id'));
		$this->db->update('image_comment',$data);
		
		
		
	    		
		
	  
	
	} 
	
	function get_one_comment($id)
	{
	    $this->db->select('*');
		$this->db->from('image_comment pc');
		$this->db->join('user us','pc.user_id=us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');		
		$this->db->where('pc.image_comment_id',$id);
		$query = $this->db->get(); 
		
		if ($query->num_rows() > 0) {
			
			return $query->row();
		}
		return 0;
	    
		
	}
	
	
		
	function get_total_comment_count()
	{
	
		$this->db->select('*');
		$this->db->from('image_comment ic');
		$this->db->join('user us','ic.user_id=us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('images img','ic.image_id=img.image_id');
		$this->db->order_by('ic.image_comment_id','desc');
		$query = $this->db->get(); 
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_list_comment_detail_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('image_comment ic');
		$this->db->join('user us','ic.user_id=us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('images img','ic.image_id=img.image_id');
		$this->db->order_by('ic.image_comment_id','desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get(); 
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	function get_total_search_comment_count($option,$keyword)
	{
	 
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
	
		
		$this->db->select('*');
		$this->db->from('image_comment imc');
		$this->db->join('images im','imc.image_id=im.image_id');
		$this->db->join('user us','imc.user_id=us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');

		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
		
		
		$this->db->order_by('imc.image_comment_id','desc');
				
		$query=$this->db->get();
		
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();		
		}
		
		return 0;
	
	}
	
	function get_all_search_comment_result($option,$keyword,$offset, $limit)
	{
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		$this->db->select('*');
		$this->db->from('image_comment imc');
		$this->db->join('images im','imc.image_id=im.image_id');
		$this->db->join('user us','imc.user_id=us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		
		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
		
		
		
		$this->db->order_by('imc.image_comment_id','desc');
		$this->db->limit($limit,$offset);
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}
		
		return 0;
	
	}
	

	
	
}  	
	
?>