<?php
class Paging_setting_model extends CI_Model {
	
    function Paging_setting_model()
    {
         parent::__construct();	
    }   
	
	
	function paging_setting_update()
	{
		$data = array(	
			'album_like_limit'=>$this->input->post('album_like_limit'),		
			'profile_album_limit' => $this->input->post('profile_album_limit'),
			'profile_image_limit' => $this->input->post('profile_image_limit'),
			'profile_like_limit' => $this->input->post('profile_like_limit'),
			'profile_activity_limit' => $this->input->post('profile_activity_limit'),
			'home_image_limit ' => $this->input->post('home_image_limit'),
			'source_image_limit'=>$this->input->post('source_image_limit'),
			'category_image_limit'=>$this->input->post('category_image_limit'),
			'popular_image_limit'=>$this->input->post('popular_image_limit'),
			'search_image_limit'=>$this->input->post('search_image_limit'),
			'image_description_limit'=>$this->input->post('image_description_limit')
			
		);
		$this->db->where('paging_setting_id',$this->input->post('paging_setting_id'));
		$this->db->update('paging_setting',$data);
	}		
	
	
	function get_paging_setting($offset, $limit)
	{
		$this->db->order_by('paging_setting_id','asc');
		$query = $this->db->get('paging_setting',$limit,$offset);
		return $query->result();
	}
	
	function get_paging_setting_count()
	{
		return $this->db->count_all('paging_setting');
	}
	
	function get_one_paging_setting()
	{
		$query = $this->db->get_where('paging_setting');
		return $query->row();
	}
}
?>