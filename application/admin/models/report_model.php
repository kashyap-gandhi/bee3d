<?php

class Report_model extends CI_Model {
	
    function Report_model()
    {
        parent::__construct();	
    }  
	
	
	/********************report_comment**************************/	
	
	function get_total_comment()
	{
		 $this->db->select('*');
		$this->db->from('block_report prt');		
		$this->db->join('image_comment imc','prt.comment_id=imc.image_comment_id','left');
		$this->db->join('images im','imc.image_id=im.image_id');
		$this->db->join('user us','prt.report_by_user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->where('prt.comment_id > ',0);
		$this->db->order_by('prt.report_id','desc');
		
		$query = $this->db->get(); 
		
	

		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}	
	
	function get_list_comment_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('block_report prt');
		$this->db->join('image_comment imc','prt.comment_id=imc.image_comment_id','left');
		$this->db->join('images im','imc.image_id=im.image_id');
		$this->db->join('user us','prt.report_by_user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->where('prt.comment_id > ',0);
		$this->db->order_by('prt.report_id','desc');
		$this->db->limit($limit,$offset);
		
		$query = $this->db->get(); 	

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	function get_one_comment($id)
	{
	    $this->db->select('*');
		$this->db->from('block_report prt');
		$this->db->join('user us','prt.report_by_user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->where('prt.report_id',$id);
		$query = $this->db->get(); 
		
		if ($query->num_rows() > 0) {
			
			return $query->row();
		}
		return 0;
	    
		
	}	
	
	function comment_detail_update()
	{
	   $data = array(
			'user_comment' => $this->input->post('user_comment')			
		);				
		
		$this->db->where('image_comment_id',$this->input->post('image_comment_id'));
		$this->db->update('image_comment',$data);
	
	
	} 
	
	
	/********************report_image**************************/
	
	
	function get_total_image_count()
	{
		$this->db->select('*');
		$this->db->from('block_report prt');
		$this->db->join('images im','prt.image_id=im.image_id');
		$this->db->join('user us','prt.report_by_user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->where('prt.image_id > ' ,0);
		$this->db->order_by('prt.report_id','desc');	
		
		$query = $this->db->get(); 
	
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	
	
	function get_list_image_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('block_report prt');
		$this->db->join('images im','prt.image_id=im.image_id');
		$this->db->join('user us','prt.report_by_user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->where('prt.image_id > ' ,0);
		$this->db->order_by('prt.report_id','desc');
		
		$this->db->limit($limit,$offset);
		
		$query = $this->db->get(); 
		
	

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	

	
	
	
	function get_one_image($id)
	{
	     $this->db->select('*');
		$this->db->from('block_report prt');
		$this->db->join('user us','prt.report_by_user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->where('prt.report_id',$id);
		$query = $this->db->get(); 
		
		if ($query->num_rows() > 0) {
			
			return $query->row();
		}
		return 0;
	
		
	}
	
	/********************report_user**************************/
		
	
	
	function get_total_user_count()
	{
		$this->db->select('prt.*,us.*,up.*,usr.first_name as report_first_name,usr.last_name as report_last_name,usr.email as report_email,usr.profile_name as report_profile_name');
		$this->db->from('block_report prt');		
		$this->db->join('user us','prt.user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->join('user usr','prt.report_by_user_id=usr.user_id','left');
		$this->db->join('user_profile upr','usr.user_id=upr.user_id','left');
		$this->db->where('prt.user_id > ' ,0);
		$this->db->where('prt.image_id' ,0);
		$this->db->where('prt.album_id' ,0);
		$this->db->where('prt.comment_id' ,0);
		$this->db->order_by('prt.report_id','desc');	
		
		$query = $this->db->get(); 
	
		
		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	
	
	function get_list_user_result($offset, $limit)
	{
		$this->db->select('prt.*,us.*,up.*,usr.first_name as report_first_name,usr.last_name as report_last_name,usr.email as report_email,usr.profile_name as report_profile_name');
		$this->db->from('block_report prt');
		$this->db->join('user us','prt.user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->join('user usr','prt.report_by_user_id=usr.user_id','left');
		$this->db->join('user_profile upr','usr.user_id=upr.user_id','left');
		$this->db->where('prt.user_id > ' ,0);
		$this->db->where('prt.image_id' ,0);
		$this->db->where('prt.album_id' ,0);
		$this->db->where('prt.comment_id' ,0);
		$this->db->order_by('prt.report_id','desc');	
		
		$this->db->limit($limit,$offset);
		
		$query = $this->db->get(); 
		
	

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
	function get_one_user($id)
	{
	    $this->db->select('*');
		$this->db->from('block_report prt');
		$this->db->where('prt.report_id',$id);
		$query = $this->db->get(); 
		
		if ($query->num_rows() > 0) {
			
			return $query->row();
		}
		return 0;
	
		
	}
}	
?>