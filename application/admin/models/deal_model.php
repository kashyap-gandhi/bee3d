<?php
class Deal_model extends CI_Model {	
	
	function Deal_model()
	{
		parent::__construct();	
	} 
	
	
	function get_one_deal_by_id($deal_id)
	{
		$this->db->select('*');
		$this->db->from('deal');
		$this->db->where('deal_id',$deal_id);
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->row();
		}
		
		return 0;
	}
	
	
	function deal_update()
	{
			
		$deal_image_name=$this->input->post('prev_deal_image');
		
		
		
		
		///===
		
		$image_settings = image_setting();
		if($_FILES['deal_image']['name']!='')
		{
			
				 
				 
				 $this->load->library('upload');
				 $rand=randomCode(); 
				  
				 $_FILES['userfile']['name']     =   $_FILES['deal_image']['name'];
				 $_FILES['userfile']['type']     =   $_FILES['deal_image']['type'];
				 $_FILES['userfile']['tmp_name'] =   $_FILES['deal_image']['tmp_name'];
				 $_FILES['userfile']['error']    =   $_FILES['deal_image']['error'];
				 $_FILES['userfile']['size']     =   $_FILES['deal_image']['size'];
	  
				 
				 $config['file_name'] = $rand;
				 $config['upload_path'] = base_path().'upload/deal_orig/';
				 $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
					
					
				$this->upload->initialize($config);
 
				  if (!$this->upload->do_upload())
				  {
					$error =  $this->upload->display_errors();   
				  } 
				   
				   
				  $picture = $this->upload->data();		
				  $deal_image_name=$picture['file_name'];
				 

					
				$this->load->library('image_lib');					
				$this->image_lib->clear();	
			
				$config['image_library'] = 'GD2';
				$config['thumb_marker'] = "";
				$config['maintain_ratio'] = TRUE;
				$config['create_thumb'] = TRUE;
				$config['quality'] = '100%';
				$config['master_dim'] = 'width';
				$config['source_image'] = $picture['full_path'];
				$config['new_image'] = base_path().'upload/deal/'.$picture['file_name'];					
				$config['width'] = $image_settings->deal_thumb_width;
				$config['height'] = $image_settings->deal_thumb_height;
				$this->image_lib->initialize($config);
				
				
					
				if(!$this->image_lib->resize()){
					$error = $this->image_lib->display_errors();				
				}
							
					
					
				$this->image_lib->clear();
				
				
				
				$prev_deal_image=$this->input->post('prev_deal_image');
				
				 if($prev_deal_image != '') 
					{
						if(file_exists(base_path().'upload/deal_orig/'.$prev_deal_image))
						{
							unlink(base_path().'upload/deal_orig/'.$prev_deal_image);
						}
						if(file_exists(base_path().'upload/deal/'.$prev_deal_image))
						{
							unlink(base_path().'upload/deal/'.$prev_deal_image);
						}					
						
					}
							
				
				
			}
			
		
		
		///===
		
		
		
		
		$data=array(
		'deal_title'=>$this->input->post('deal_title'),
		'deal_summary'=>$this->input->post('deal_summary'),
		'deal_description'=>$this->input->post('deal_description'),
		'deal_status'=>$this->input->post('deal_status'),
		'deal_amount'=>$this->input->post('deal_amount'),
		'deal_days'=>$this->input->post('deal_days'),
		'deal_image'=>$deal_image_name,
		'deal_update_date'=>date('Y-m-d H:i:s')
		);
		
		$this->db->where('deal_id',$this->input->post('deal_id'));
		$this->db->update('deal',$data);
		
	}
	
	
	function deal_insert()
	{
	
	}
	
	
	function get_total_deal_count($filter)
	{
		//0=inactive,1=pending,2=accept,3=reject
		$this->db->select('*');
		$this->db->from('deal de');
		$this->db->join('user us','de.user_id=us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		
		if($filter=='inactive')
		{
			$this->db->where('de.deal_status',0);
		}
		elseif($filter=='pending')
		{
			$this->db->where('de.deal_status',1);
		}
		elseif($filter=='accept')
		{
			$this->db->where('de.deal_status',2);
		}
		elseif($filter=='reject')
		{
			$this->db->where('de.deal_status',3);
		}
	
			
		$query = $this->db->get();
		
		return $query->num_rows();			
	}  
	
	
	
	function get_all_deal_result($filter,$offset, $limit)
	{
		//0=inactive,1=pending,2=accept,3=reject
		$this->db->select('*');
		$this->db->from('deal de');
		$this->db->join('user us','de.user_id=us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		
		if($filter=='inactive')
		{
			$this->db->where('de.deal_status',0);
		}
		elseif($filter=='pending')
		{
			$this->db->where('de.deal_status',1);
		}
		elseif($filter=='accept')
		{
			$this->db->where('de.deal_status',2);
		}
		elseif($filter=='reject')
		{
			$this->db->where('de.deal_status',3);
		}
		
		$this->db->order_by('de.deal_id','desc');		
		
		$this->db->limit($limit,$offset);
		
		
			
		$query = $this->db->get();
		
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		
		return 0;	
	}
	
	
	
	function get_total_search_deal_count($filter,$option,$keyword)
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		//0=inactive,1=pending,2=accept,3=reject
		$this->db->select('*');
		$this->db->from('deal de');
		$this->db->join('user us','de.user_id=us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		
		if($filter=='inactive')
		{
			$this->db->where('de.deal_status',0);
		}
		elseif($filter=='pending')
		{
			$this->db->where('de.deal_status',1);
		}
		elseif($filter=='accept')
		{
			$this->db->where('de.deal_status',2);
		}
		elseif($filter=='reject')
		{
			$this->db->where('de.deal_status',3);
		}
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
		
	
			
		$query = $this->db->get();
		
		return $query->num_rows();			
	}  
	
	
	
	function get_all_search_deal_result($filter,$option,$keyword,$offset, $limit)
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		//0=inactive,1=pending,2=accept,3=reject
		$this->db->select('*');
		$this->db->from('deal de');
		$this->db->join('user us','de.user_id=us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		
		if($filter=='inactive')
		{
			$this->db->where('de.deal_status',0);
		}
		elseif($filter=='pending')
		{
			$this->db->where('de.deal_status',1);
		}
		elseif($filter=='accept')
		{
			$this->db->where('de.deal_status',2);
		}
		elseif($filter=='reject')
		{
			$this->db->where('de.deal_status',3);
		}
		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
		
		
		
		
		$this->db->order_by('de.deal_id','desc');		
		
		$this->db->limit($limit,$offset);
		
		
			
		$query = $this->db->get();
		
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		
		return 0;	
	}
	
	
}
?>