<?php

class Google_setting_model extends CI_Model {
	
    function Google_setting_model()
    {
       parent::__construct();	
    }   
	
	/** admin facebook setting update function
	* var integer $facebook_application_id
	* var integer $facebook_login_enable
	* var string $facebook_api_key
	* var string $facebook_secret_key	
	* var string $facebook_url	
	**/
	function google_setting_update()
	{
		$data = array(			
			'consumer_key' => $this->input->post('consumer_key'),
			'consumer_secret' => $this->input->post('consumer_secret'),
			'google_enable' => $this->input->post('google_enable'),
			
		);
		$this->db->where('google_setting_id',$this->input->post('google_setting_id'));
		$this->db->update('google_setting',$data);
	}
	
	/*** get facebook setting details
	*  return single record array
	**/
	function get_one_google_setting()
	{
		$query = $this->db->get_where('google_setting');
		return $query->row();
	}
}
?>