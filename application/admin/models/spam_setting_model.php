<?php

class Spam_setting_model extends CI_Model {
	
    function Spam_setting_model()
    {
        parent::__construct();	
    }   
	
	
	
	function get_word_data()
	
	{
	   return $this->db->count_all('spam_word');
	}
	
	function get_word_report_result($offset, $limit)
	{
		$this->db->order_by('spam_word_id','desc');
		$query = $this->db->get('spam_word',$limit,$offset);
		

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	function get_one_word($id)
	{
	
	  $query = $this->db->get_where('spam_word',array('spam_word_id'=>$id));
	  return $query->row_array();
	}
	
	function list_word_insert()
	{
		$data = array(
			'spam_word' => $this->input->post('spam_word')	
		);		
		$this->db->insert('spam_word',$data);
		
		
		////==cache
		
		$supported_cache=check_supported_cache_driver();
		
		if(isset($supported_cache))
		{
			if($supported_cache!='' && $supported_cache!='none')
			{
				////===load cache driver===
				$this->load->driver('cache');				
				
			
					$query = $this->db->query("select spam_word from ".$this->db->dbprefix('spam_word')." order by spam_word asc");
					
					if($query->num_rows()>0)
					{
						$res = $query->result();
				
						foreach($res as $spm)
						{
							$arrya[]=$spm->spam_word;
						}
				
				
						 $this->cache->$supported_cache->save('spam_word',$arrya,CACHE_VALID_SEC);	
						
					}		
				
			}			
			
		}
		
		////==cache
		
		
		
	}
	
	function list_word_update()
	{
		$data = array(
			'spam_word' => $this->input->post('spam_word')	
		);		
		$this->db->where('spam_word_id',$this->input->post('spam_word_id'));
		$this->db->update('spam_word',$data);
		
		
		////==cache
		
		$supported_cache=check_supported_cache_driver();
		
		if(isset($supported_cache))
		{
			if($supported_cache!='' && $supported_cache!='none')
			{
				////===load cache driver===
				$this->load->driver('cache');				
				
			
					$query = $this->db->query("select spam_word from ".$this->db->dbprefix('spam_word')." order by spam_word asc");
					
					if($query->num_rows()>0)
					{
						$res = $query->result();
				
						foreach($res as $spm)
						{
							$arrya[]=$spm->spam_word;
						}
				
				
						 $this->cache->$supported_cache->save('spam_word',$arrya,CACHE_VALID_SEC);	
						
					}		
				
			}			
			
		}
		
		////==cache
		
		
		
		
	}


	
	
	
	
	function add_spammer()
	{
	
	
		$data=array(
		'spam_ip'=>$this->input->post('spam_ip'),
		'permenant_spam'=>$this->input->post('permenant_spam'),
		'start_date'=>date('Y-m-d'),
		'end_date'=>date('Y-m-d',strtotime("+30 days"))
		);
		/*print_r($data);
		die();*/
		
		$this->db->insert('spam_ip',$data);
	
	
	}
	
	
	function spam_control_update()
	{
		
		$data = array(			
			'spam_report_total' => $this->input->post('spam_report_total'),			
			'spam_report_expire' => $this->input->post('spam_report_expire'),
			'total_register' =>  $this->input->post('total_register'),
			'register_expire' => $this->input->post('register_expire'),
			'total_comment' => $this->input->post('total_comment'),
			'comment_expire' => $this->input->post('comment_expire'),			
			'total_contact' => $this->input->post('total_contact'),
			'contact_expire' => $this->input->post('contact_expire'),
			'user_daily_album' => $this->input->post('user_daily_album'),
			'user_daily_image' => $this->input->post('user_daily_image')			
		);
		$this->db->where('spam_control_id',$this->input->post('spam_control_id'));
		$this->db->update('spam_control',$data);
	}
	
	function get_spam_control()
	{
		$query = $this->db->get_where('spam_control');
		return $query->row_array();
	}
	
	
	function get_total_spam_report_count()
	{
		$query=$this->db->query("select *, COUNT(*) as total_spam from ".$this->db->dbprefix('spam_report_ip')." group by spam_ip HAVING COUNT(*) >=1 order by spam_report_id desc");
		return $query->num_rows();	
	}
	
	function get_spam_report_result($offset, $limit)
	{
		$query=$this->db->query("select *, COUNT(*) as total_spam from ".$this->db->dbprefix('spam_report_ip')." group by spam_ip HAVING COUNT(*) >=1 order by spam_report_id desc LIMIT ".$limit." OFFSET ".$offset);
		
		if($query->num_rows()>0)
		{
		
			return $query->result();
		}
		
		return 0;
			
	}
	
	function delete_spam_report($ip)
	{
		$query=$this->db->query("delete from spam_report_ip where spam_ip='".$ip."'");	
	}
	
	function make_spam($ip)
	{
		$spam_control=$this->db->query("select * from spam_control");
		$control=$spam_control->row();
				
		$report_expire=date('Y-m-d', strtotime('+'.$control->spam_report_expire.' days'));
		
		
		$insert_spam=$this->db->query("insert into spam_ip(`spam_ip`,`start_date`,`end_date`)values('".$ip."','".date('Y-m-d')."','".$report_expire."')");
												
		$delete_from_report=$this->db->query("delete from spam_report_ip where spam_ip='".$ip."'");		
		
	}
	
	
	
	
	
	function get_total_spamer_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('spam_ip')." group by spam_ip order by spam_id desc");
		return $query->num_rows();	
	}
	
	function get_spamer_result($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('spam_ip')." group by spam_ip order by spam_id desc LIMIT ".$limit." OFFSET ".$offset);
		
		if($query->num_rows()>0)
		{
		
			return $query->result();
		}
		
		return 0;			
	}
	
	function delete_spam($ip){
		$query=$this->db->query("delete from ".$this->db->dbprefix('spam_report_ip')." where spam_report_id='".$ip."'");
	}
	
	function delete_spamer($ip)
	{
		$query=$this->db->query("delete from ".$this->db->dbprefix('spam_ip')." where spam_ip='".$ip."'");	
	}
	
	
	function make_spam_permenant($ip)
	{
		$query=$this->db->query("update ".$this->db->dbprefix('spam_ip')." set permenant_spam='1' where spam_ip='".$ip."'");
	}
	
	function get_total_search_spam_count($option,$keyword)
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		$this->db->select('spam_ip.*');
		$this->db->from('spam_ip');
		
		if($option=='ip')
		{
			$this->db->like('spam_ip.spam_ip',$keyword);
			
			if(substr_count($keyword,' ')>=1)
			{
				$ex=explode(' ',$keyword);
				
				foreach($ex as $val)
				{
					$this->db->like('spam_ip.spam_ip',$val);
				}	
			}

		}
		
		$this->db->order_by("spam_ip.spam_id", "desc"); 
		
		$query = $this->db->get();
		
		return $query->num_rows();
	}
	
	
	
	function get_search_spam_result($option,$keyword,$offset, $limit)
	{
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		$this->db->select('spam_ip.*');
		$this->db->from('spam_ip');
		
		if($option=='ip')
		{
			$this->db->like('spam_ip.spam_ip',$keyword);
			
			if(substr_count($keyword,' ')>=1)
			{
				$ex=explode(' ',$keyword);
				
				foreach($ex as $val)
				{
					$this->db->like('spam_ip.spam_ip',$val);
				}	
			}

		}
		
		$this->db->order_by("spam_ip.spam_id", "desc"); 
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			
			return $query->result();
		}
		return 0;
	}
	
	
	function get_total_spam_spamreport_count($option,$keyword)
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		
		
		$this->db->select('spam_report_ip.*,COUNT(*) as total_spam');
		$this->db->from('spam_report_ip');
		
		if($option=='ip')
		{
			$this->db->like('spam_report_ip.spam_ip',$keyword);
			
			if(substr_count($keyword,' ')>=1)
			{
				$ex=explode(' ',$keyword);
				
				foreach($ex as $val)
				{
					$this->db->like('spam_report_ip.spam_ip',$val);
				}	
			}

		}
		
		$this->db->group_by ("spam_report_ip.spam_ip", "desc"); 
		$this->db->order_by("spam_report_ip.spam_report_id", "desc"); 
		
		$query = $this->db->get();
		
		return $query->num_rows();
	}
	
	function get_spam_spamreport_result($option,$keyword,$offset, $limit)
	{
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		//$query=$this->db->query("select *, COUNT(*) as total_spam from spam_report_ip group by spam_ip HAVING COUNT(*) >=1 order by spam_report_id desc LIMIT ".$limit." OFFSET ".$offset);
		
		$this->db->select('spam_report_ip.*,COUNT(*) as total_spam');
		$this->db->from('spam_report_ip');
		
		if($option=='ip')
		{
			$this->db->like('spam_report_ip.spam_ip',$keyword);
			
			if(substr_count($keyword,' ')>=1)
			{
				$ex=explode(' ',$keyword);
				
				foreach($ex as $val)
				{
					$this->db->like('spam_report_ip.spam_ip',$val);
				}	
			}

		}
		
		$this->db->group_by("spam_report_ip.spam_ip", "desc"); 
		$this->db->order_by("spam_report_ip.spam_report_id", "desc"); 
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			
			return $query->result();
		}
		return 0;
	}
	
	function check_gallery_have_sub_gallery($gallery_id)
	{
		$query = $this->db->get_where('gallery',array('gallery_id '=>$gallery_id));
		if ($query->num_rows() > 0) {
			return 1;
		}
		return 0;	
	}
	
	function get_report_data()
	{
	  return $this->db->count_all('report_type');
	}
	
	function get_list_report_result($offset, $limit)
	{
		$this->db->order_by('report_id','desc');
		$query = $this->db->get('report_type',$limit,$offset);
		

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	function list_report_insert()
	{
		$data = array(
			'report_text' => $this->input->post('report_text'),
			'report_is_comment' => $this->input->post('report_is_comment'),
			'report_is_pin' => $this->input->post('report_is_pin'),
			'report_status' => $this->input->post('report_status'),
			'report_order' => $this->input->post('report_order'),
		
		);		
		$this->db->insert('report_type',$data);
		
	}
	
	function get_one_report($id)
	{
	  $query = $this->db->get_where('report_type',array('report_id'=>$id));
		return $query->row_array();
	}
	
	function list_report_update()
	{
	 $data = array(
			'report_text' => $this->input->post('report_text'),
			'report_is_comment' => $this->input->post('report_is_comment'),
			'report_is_pin' => $this->input->post('report_is_pin'),
			'report_status' => $this->input->post('report_status'),
			'report_order' => $this->input->post('report_order'),
		
		);		
		$this->db->where('report_id',$this->input->post('report_id'));
		$this->db->update('report_type',$data);
	}
	
	
	


	
	
	
	
}
?>