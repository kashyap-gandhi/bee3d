<?php

class User_album_model extends CI_Model {
	
    function User_album_model()
    {
        parent::__construct();	
    }   

	
	
	
	function check_user_profile_exists($user_id)
	{
		
		$query=$this->db->query("select * from ".$this->db->dbprefix('user')." where user_id='".$user_id."'");
		
		if($query->num_rows()>0)
		{
			return true;
		}
		else
		{
			return false;		
		}
		
	
	}
	
	function get_user_profile($user_id)
	{
		
		$query=$this->db->query("select * from ".$this->db->dbprefix('user')." usr, ".$this->db->dbprefix('user_profile')." pr where usr.user_id=pr.user_id and usr.user_id='".$user_id."' ");	
		
		if($query->num_rows()>0) {
				
			return $query->row();
		} 
		return 0;
	
	}
	
	
	function get_user_total_album($user_id)
	{
		$this->db->select('*');
		$this->db->from('album alb');
		$this->db->join('user us','alb.user_id = us.user_id');
		$this->db->join('user_profile up','us.user_id = up.user_id');
		$this->db->where('alb.user_id',$user_id);			
		$this->db->order_by('alb.album_id','desc');
	
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();		
		}
		
		return 0;
	}
	
	
	function get_user_album($user_id,$limit,$offset)
	{
		$this->db->select('*');
		$this->db->from('album alb');
		$this->db->join('user us','alb.user_id = us.user_id');
		$this->db->join('user_profile up','us.user_id = up.user_id');
		$this->db->where('alb.user_id',$user_id);			
		$this->db->order_by('alb.album_id','desc');
		$this->db->limit($limit,$offset);
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}
		
		return 0;
	}
	
	
	function get_user_album_img($id)
	{
		$query = $this->db->get_where('album_image',array('album_id'=>$id));
		return $query->row_array();
	}
	
	function get_one_album($id)
	{
		$query = $this->db->get_where('album',array('album_id'=>$id));
		return $query->row_array();
	}
	
	
	
	
	
	
	function album_update()
	{

		$image_settings = image_setting();	
		$album_cover_image_id=0;
		
		if($_FILES)
		{
				$this->db->select('*');
				$this->db->from('images');
				$this->db->where('image_id',$this->input->post('album_image_name'));
				$query = $this->db->get();
				
				
				
			if($query->num_rows()>0)
			{	
				$image_details = $query->row_array();
				
					
				if($_FILES['image_file_up']['name']!='')
				{
				 
					 $this->load->library('upload');
					 $rand=$image_details['image_unique_code']; 
			  
					 $_FILES['userfile']['name']     =   $_FILES['image_file_up']['name'];
					 $_FILES['userfile']['type']     =   $_FILES['image_file_up']['type'];
					 $_FILES['userfile']['tmp_name'] =   $_FILES['image_file_up']['tmp_name'];
					 $_FILES['userfile']['error']    =   $_FILES['image_file_up']['error'];
					 $_FILES['userfile']['size']     =   $_FILES['image_file_up']['size'];
		  
					 
					 $config['file_name']     = $rand;
					 $config['upload_path'] = base_path().'upload/image_orig/';
					 $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
					 $config['overwrite']= TRUE;  
		 
					 $this->upload->initialize($config);
		 
					  if (!$this->upload->do_upload())
					  {
						$error =  $this->upload->display_errors();   
					  } 
					   
					   
					  $picture = $this->upload->data();		
					  $album_cover_image=$picture['file_name'];
					  
							
						$this->load->library('image_lib');					
						$this->image_lib->clear();	
					
						$config['image_library'] = 'GD2';
						$config['thumb_marker'] = "";
						$config['maintain_ratio'] = TRUE;
                        $config['overwrite']= TRUE;
						$config['create_thumb'] = TRUE;
						$config['quality'] = '100%';
						$config['master_dim'] = 'width';
						$config['source_image'] = $picture['full_path'];
						$config['new_image'] = base_path().'upload/image_thumb/'.$picture['file_name'];					
						$config['width'] = $image_settings->image_small_thumb_width;
						$config['height'] = $image_settings->image_small_thumb_height;
						$this->image_lib->initialize($config);
					
					
						
						if(!$this->image_lib->resize()){
							$error = $this->image_lib->display_errors();				
						}
									
							
							
						$this->image_lib->clear();
							
						
						$config['image_library'] = 'GD2';
						$config['thumb_marker'] = "";
						$config['maintain_ratio'] = TRUE;
						$config['create_thumb'] = TRUE;
						$config['overwrite']= TRUE;
						$config['quality'] = '100%';
						$config['master_dim'] = 'width';
						$config['source_image'] = $picture['full_path'];
						$config['new_image'] = base_path().'upload/image_medium/'.$picture['file_name'];
						$config['width'] = $image_settings->image_medium_thumb_width;
						$config['height'] = $image_settings->image_medium_thumb_height;
						$this->image_lib->initialize($config);
				
						if(!$this->image_lib->resize()){
							$error = $this->image_lib->display_errors();				
						}
						
						
										
						
						$filesize = 0;	
						$file_size=$_FILES['image_file_up']['size'];	
		
						if($file_size>0)
						{	
							$filesize = ($file_size * .0009765625) * .0009765625; 
							
							$filesize1 = number_format($filesize,2); 
							if($filesize1>0) {  $filesize=$filesize1; } else { $filesize =  number_format($filesize,3);    }
						}
								
						$album_image_update= array(
							'image_size' => $filesize,
							'image_original_name' => $_FILES['image_file_up']['name'],
							'image_name' => $album_cover_image
						);
						
						
						$album_cover_image_id=$image_details["image_id"];
						
						$this->db->where('image_id',$image_details["image_id"]);
						$this->db->update('images',$album_image_update);
						
						
					}
		
			}
		  
		} 
		
		
		
		$data = array(		
			'album_name' => $this->input->post('album_name'),
			'album_description' => $this->input->post('album_description'),
			'album_layout_id'=>$this->input->post('album_layout_id'),
			'album_is_public'=>$this->input->post('album_privacy'),
			'album_cover_image_id'=>$album_cover_image_id
		);
		
		
		$this->db->where('album_id',$this->input->post('album_id'));
		$this->db->update('album',$data);
		
		
		
	}
	
	
	
	function get_total_all_search_album_count($user_id,$option,$keyword)
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		$this->db->select('*');
		$this->db->from('album alb');
		$this->db->join('user us','alb.user_id = us.user_id');
		$this->db->join('user_profile up','us.user_id = up.user_id');
		$this->db->where('alb.user_id',$user_id);	
		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
			
		$this->db->order_by('alb.album_id','desc');
		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();		
		}
		
		return 0;
	}
	

	
	function get_all_search_album_result($user_id,$option,$keyword,$offset, $limit)
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		$this->db->select('*');
		$this->db->from('album alb');
		$this->db->join('user us','alb.user_id = us.user_id');
		$this->db->join('user_profile up','us.user_id = up.user_id');
		$this->db->where('alb.user_id',$user_id);
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
		
			
		$this->db->order_by('alb.album_id','desc');
		$this->db->limit($limit,$offset);
		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}
		
		return 0;
	}
	
	
	function get_album_total_image($album_id)
	{
		$this->db->select('*');
		$this->db->from('album_image albimg');
		$this->db->join('album alb','albimg.album_id=alb.album_id');
		$this->db->join('images img','albimg.image_id=img.image_id');	
		$this->db->join('user us','alb.user_id = us.user_id');
		$this->db->join('user_profile up','us.user_id = up.user_id');
		$this->db->where('albimg.album_id',$album_id);
	
		
		$query=$this->db->get();
		
			
		//echo $this->db->last_query();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();		
		}
		
		return 0;
	}
	
	
	function get_album_images($album_id,$limit,$offset)
	{
		
		
		$this->db->select('*');
		$this->db->from('album_image albimg');
		$this->db->join('album alb','albimg.album_id=alb.album_id');
		$this->db->join('images img','albimg.image_id=img.image_id');	
		$this->db->join('user us','alb.user_id = us.user_id');
		$this->db->join('user_profile up','us.user_id = up.user_id');		
		$this->db->where('albimg.album_id',$album_id);
		$this->db->order_by('albimg.album_image_id','desc');
		$this->db->limit($limit,$offset);
		
		
		
		$query=$this->db->get();
		
		
	//	echo $this->db->last_query();
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}
		
		return 0;
	}
	
	
	
	function get_total_search_image_count($album_id,$option,$keyword)
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		$this->db->select('*');
		$this->db->from('images img');
		$this->db->join('album_image alb','img.image_id=alb.image_id');
		$this->db->join('album albimg','albimg.album_id=alb.album_id');
		$this->db->join('user us','albimg.user_id = us.user_id');
		$this->db->join('user_profile up','us.user_id = up.user_id');
		$this->db->order_by('img.image_id','desc');
		$this->db->where('alb.album_id',$album_id);
		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
		
		
		
		$this->db->order_by('img.image_id','desc');
				
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();		
		}
		
		return 0;
	}
	
	
	
	function get_all_search_image_result($album_id,$option,$keyword,$offset, $limit)
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		$this->db->select('*');
		$this->db->from('images img');
		$this->db->join('album_image alb','img.image_id=alb.image_id');
		$this->db->join('album albimg','albimg.album_id=alb.album_id');
		$this->db->join('user us','albimg.user_id = us.user_id');
		$this->db->join('user_profile up','us.user_id = up.user_id');
		$this->db->order_by('img.image_id','desc');
		$this->db->where('alb.album_id',$album_id);
		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
		
		
		
		$this->db->order_by('img.image_id','desc');
		$this->db->limit($limit,$offset);
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}
		
		return 0;
	}
	
	
}
?>