<?php

class Pages_model extends CI_Model {
	
    function Pages_model()
    {
       parent::__construct();
    }   
	
	function pages_insert()
	{
		$data = array(
			'pages_title' => $this->input->post('pages_title'),
			'description' => $this->input->post('description'),
			'slug' => $this->input->post('slug'),
			'active' => $this->input->post('active'),
			'meta_keyword' => $this->input->post('meta_keyword'),
			'meta_description' => $this->input->post('meta_description'),
			'header_bar' => $this->input->post('header_bar'),
			'footer_bar' => $this->input->post('footer_bar'),
			'left_side' => $this->input->post('left_side'),
			'right_side' => $this->input->post('right_side'),
			'external_link' => $this->input->post('external_link')
		);		
		$this->db->insert('pages',$data);
	}
	
	function pages_update()
	{
		$data = array(			
			'pages_title' => $this->input->post('pages_title'),
			'description' => $this->input->post('description'),
			'slug' => $this->input->post('slug'),
			'active' => $this->input->post('active'),
			'meta_keyword' => $this->input->post('meta_keyword'),
			'meta_description' => $this->input->post('meta_description'),
			'header_bar' => $this->input->post('header_bar'),
			'footer_bar' => $this->input->post('footer_bar'),
			'left_side' => $this->input->post('left_side'),
			'right_side' => $this->input->post('right_side'),
			'external_link' => $this->input->post('external_link')
		);
		$this->db->where('pages_id',$this->input->post('pages_id'));
		$this->db->update('pages',$data);
	}
	
	function get_one_pages($id)
	{
		$query = $this->db->get_where('pages',array('pages_id'=>$id));
		return $query->row_array();
	}	
	
	function get_total_pages_count()
	{
		return $this->db->count_all('pages');
	}
	
	function get_pages_result($offset, $limit)
	{
			$this->db->order_by('pages_id','desc');
		$query = $this->db->get('pages',$limit,$offset);

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	function get_total_search_pages_count($option,$keyword)
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		$this->db->select('pages.*');
		$this->db->from('pages');
		
		if($option=='title')
		{
			$this->db->like('pages.pages_title',$keyword);
			
			if(substr_count($keyword,' ')>=1)
			{
				$ex=explode(' ',$keyword);
				
				foreach($ex as $val)
				{
					$this->db->like('pages.pages_title',$val);
				}	
			}

		}
				
		$this->db->order_by("pages_id", "desc"); 
		
		$query = $this->db->get();
		
		return $query->num_rows();
	}
	
	
	
	function get_search_pages_result($option,$keyword,$offset, $limit)
	{
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		$this->db->select('pages.*');
		$this->db->from('pages');
		
		if($option=='title')
		{
			$this->db->like('pages.pages_title',$keyword);
			
			if(substr_count($keyword,' ')>=1)
			{
				$ex=explode(' ',$keyword);
				
				foreach($ex as $val)
				{
					$this->db->like('pages.pages_title',$val);
				}	
			}

		}
				
		$this->db->order_by("pages_id", "desc"); 
		
		$query = $this->db->get();
		$this->db->limit($limit,$offset);
		if ($query->num_rows() > 0) {
			
			return $query->result();
		}
		return 0;
	}
	
	
}
?>