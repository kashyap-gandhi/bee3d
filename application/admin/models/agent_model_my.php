<?php
class Agent_model extends CI_Model {	
    function Agent_model()
    {
        parent::__construct();	
    }   
	
	
	
	/*** check the user unique email address
	*	var string $email
	*  return boolen
	**/

	function emailTaken($email)
	{
	
		if($this->input->post('user_id')>0)
		{
		 	$query = $this->db->query("select * from ".$this->db->dbprefix('user')." where user_id!='".$this->input->post('user_id')."' and  email='".$email."'");
			
		} else {
		
			 $query = $this->db->query("select * from ".$this->db->dbprefix('user')." where email='".$email."'");
			 
		}
		
		 if($query->num_rows()>0)
		 {
			return true;
		 }
		 else 
		 {
			return false;
		 }		
	}
	
	function get_agent_by_id($user_id)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->where('ag.user_id',$user_id);		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->row();
		}
		
		return 0;	
		
		
	}
	
	
	function add_agent()
	{
	
		
		
		$email_verification_code=randomCode();
		
		
		$password=randomCode();
		
		
		
		
		
		$first_name=$this->input->post('first_name');
		$last_name=$this->input->post('last_name');
		
		$profile_name=clean_url($first_name.' '.substr($last_name,0,1));
		
		
			
	$chk_url_exists=$this->db->query("select MAX(profile_name) as profile_name from ".$this->db->dbprefix('user')." where profile_name like '".$profile_name."%'");
		
		if($chk_url_exists->num_rows()>0)
		{			
				$get_pr=$chk_url_exists->row();					
				
				$strre='0';
				if($get_pr->profile_name!='')
				{
					$strre=str_replace($profile_name,'',$get_pr->profile_name);
				}
				
				if($strre=='0') 
				{					
					$newcnt=''; 
					  				
				} 
				elseif($strre=='') 
				{					
					$newcnt='1';   				
				}
				else
				{				
					$newcnt=(int)$strre+1;			
				}
				
			 	$profile_name=$profile_name.$newcnt;
						
		}
			
			
			
		
		
		
		$data = array(		
				'full_name' => $first_name.' '.$last_name,		
				'first_name' => $first_name,
				'last_name' => $last_name,	
				'profile_name'=>$profile_name,			
				'email' => $this->input->post('email'),			
				'password' => md5($password),		
				'sign_up_ip' => $this->input->ip_address(),
				'email_verification_code'=>$email_verification_code,
				'zip_code'=>$this->input->post('zip_code'),
				'mobile_no'=>$this->input->post('mobile_no'),			
				'phone_no'=>$this->input->post('phone_no'),						
				'user_status' =>1,
				'sign_up_date' => date('Y-m-d H:i:s'),
				); 
		$this->db->insert('user', $data);
		$user_id = mysql_insert_id();
		
		
		/*****create profile****/
		
		$data_profile=array(
		'user_id'=>$user_id,
		'current_city'=>$this->input->post('current_city'),
		'about_user'=>$this->input->post('about_user')
		);
		
		$this->db->insert('user_profile',$data_profile);
		
		
		
		
		
		
		
		
			/*** user notification ****/
	
		$user_notification=mysql_query("SHOW COLUMNS FROM ".$this->db->dbprefix('user_notification'));
		$res=mysql_fetch_array($user_notification);
		$fields="";
		$values="";
				
		while($res=mysql_fetch_array($user_notification)){
		//print_r($res['Field']);echo '<br>';
		if($fields==""){$fields.="(`".$res['Field']."`"; $values.="('".$user_id."'";}
		else {$fields.=",`".$res['Field']."`";	 $values.=",'1'";}
		}
		$fields.=")";
		 $values.=")";								   
		$insert_val= $fields.' values '.$values;
		
		$this->db->query("insert into ".$this->db->dbprefix('user_notification')." ".$insert_val."");
		
	
		/*******************/
	
	$post_ass=$this->input->post('agent_member_of_association');
	$member_ass='';
	
	if($post_ass)
	{
	
		$member_ass=implode(',',$post_ass);
	
	}
		
		
		
		
		
		$agency_location = explode(',',$this->input->post('agency_location'));
		
		$agency_city='';
		$agency_state='';
		$agency_country='';
		
		
		if(isset($agency_location[0]))
		{
			$agency_city=$agency_location[0];
		}
		if(isset($agency_location[1]))
		{
			$agency_state=$agency_location[1];
		}
		
		if(isset($agency_location[2]))
		{
			$agency_country=$agency_location[2];
		}
		
		
		
		
		
		$agent_data=array(
		'user_id'=>$user_id,
		'agency_name'=>$this->input->post('agency_name'),
		'contact_person_name'=>$this->input->post('contact_person_name'),
		'agency_location'=>$this->input->post('agency_location'),
		'agency_city'=>$agency_city,
		'agency_state'=>$agency_state,
		'agency_country'=>$agency_country,
		'agency_contact_email'=>$this->input->post('agency_contact_email'),
		'agency_phone'=>$this->input->post('agency_phone'),
		'agency_customer_support_no'=>$this->input->post('agency_customer_support_no'),
		'agency_website'=>seturl($this->input->post('agency_website')),
		'agent_ip'=>$_SERVER['REMOTE_ADDR'],
		'agent_date'=> date('Y-m-d H:i:s'),
		'agent_status'=>1,
		'agent_app_approved'=>$this->input->post('agent_app_approved'),
		'agent_background_approved'=>$this->input->post('agent_background_approved'),
		'agent_phone_approved'=>$this->input->post('agent_phone_approved'),
		'agent_level'=>$this->input->post('agent_level'),
		'agent_member_of_association'=>$member_ass
		);
		
		
		
		$this->db->insert('agent_profile',$agent_data);
		
		
		
		
	
	
		return $password;
		
			
			
			
			
	}
	
	
	
	function edit_agent()
	{
		
		$user_id=$this->input->post('user_id');
		$agent_id=$this->input->post('agent_id');
		
		$user_info=get_user_profile_by_id($user_id);
		
		$image_settings = image_setting();
		
		if($_FILES['profileimage']['name']!='')
		{
			
				 
				 
				 $this->load->library('upload');
				 $rand=randomCode(); 
				  
				 $_FILES['userfile']['name']     =   $_FILES['profileimage']['name'];
				 $_FILES['userfile']['type']     =   $_FILES['profileimage']['type'];
				 $_FILES['userfile']['tmp_name'] =   $_FILES['profileimage']['tmp_name'];
				 $_FILES['userfile']['error']    =   $_FILES['profileimage']['error'];
				 $_FILES['userfile']['size']     =   $_FILES['profileimage']['size'];
	  
				 
				 $config['file_name'] = $rand;
				 $config['upload_path'] = base_path().'upload/user_orig/';
				 $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
					
					
				$this->upload->initialize($config);
 
				  if (!$this->upload->do_upload())
				  {
					$error =  $this->upload->display_errors();   
				  } 
				   
				   
				  $picture = $this->upload->data();		
				  $image_name=$picture['file_name'];
				 

					
				$this->load->library('image_lib');					
				$this->image_lib->clear();	
			
				$config['image_library'] = 'GD2';
				$config['thumb_marker'] = "";
				$config['maintain_ratio'] = TRUE;
				$config['create_thumb'] = TRUE;
				$config['quality'] = '100%';
				$config['master_dim'] = 'width';
				$config['source_image'] = $picture['full_path'];
				$config['new_image'] = base_path().'upload/user_thumb/'.$picture['file_name'];					
				$config['width'] = $image_settings->image_small_thumb_width;
				$config['height'] = $image_settings->image_small_thumb_height;
				$this->image_lib->initialize($config);
				
				
					
				if(!$this->image_lib->resize()){
					$error = $this->image_lib->display_errors();				
				}
							
					
					
				$this->image_lib->clear();
				
				$config['image_library'] = 'GD2';
				$config['thumb_marker'] = "";
				$config['maintain_ratio'] = TRUE;
				$config['create_thumb'] = TRUE;
				$config['quality'] = '100%';
				$config['master_dim'] = 'width';
				$config['source_image'] = $picture['full_path'];
				$config['new_image'] = base_path().'upload/user/'.$picture['file_name'];
				$config['width'] = $image_settings->image_medium_thumb_width;
				$config['height'] = $image_settings->image_medium_thumb_height;
				$this->image_lib->initialize($config);
		
				if(!$this->image_lib->resize()){
					$error = $this->image_lib->display_errors();				
				}
						
						
				$this->image_lib->clear();
				
				
				
				
				 if($user_info->profile_image != '') 
					{
						if(file_exists(base_path().'upload/user_orig/'.$user_info->profile_image))
						{
							unlink(base_path().'upload/user_orig/'.$user_info->profile_image);
						}
						if(file_exists(base_path().'upload/user_thumb/'.$user_info->profile_image))
						{
							unlink(base_path().'upload/user_thumb/'.$user_info->profile_image);
						}
						
						if(file_exists(base_path().'upload/user/'.$user_info->profile_image))
						{
							unlink(base_path().'upload/user/'.$user_info->profile_image);
						}
					}
							
				
				
			
				$data_img=array(
					'profile_image' => $picture['file_name'],
				);
				$this->db->where('user_id',$user_id);
				$this->db->update('user_profile',$data_img);
			}
		
		
		
		
		
		/***********user detail*****/
		
		$first_name=$this->input->post('first_name');
		$last_name=$this->input->post('last_name');
		
		$profile_name=clean_url($first_name.' '.substr($last_name,0,1));
		
		
			
	$chk_url_exists=$this->db->query("select MAX(profile_name) as profile_name from ".$this->db->dbprefix('user')." where user_id!='".$user_id."'  and profile_name like '".$profile_name."%'");
		
		if($chk_url_exists->num_rows()>0)
		{			
				$get_pr=$chk_url_exists->row();					
				
				$strre='0';
				if($get_pr->profile_name!='')
				{
					$strre=str_replace($profile_name,'',$get_pr->profile_name);
				}
				
				if($strre=='0') 
				{					
					$newcnt=''; 
					  				
				} 
				elseif($strre=='') 
				{					
					$newcnt='1';   				
				}
				else
				{				
					$newcnt=(int)$strre+1;			
				}
				
			 	$profile_name=$profile_name.$newcnt;
						
		}
			
			
			
		
		
		
		$data = array(		
				'full_name' => $first_name.' '.$last_name,		
				'first_name' => $first_name,
				'last_name' => $last_name,	
				'profile_name'=>$profile_name,			
				'email' => $this->input->post('email'),			
				'zip_code'=>$this->input->post('zip_code'),
				'mobile_no'=>$this->input->post('mobile_no'),			
				'phone_no'=>$this->input->post('phone_no'),						
				'user_status' =>$this->input->post('user_status'),
				); 
		$this->db->where('user_id',$user_id);		
		$this->db->update('user', $data);
		
		
		
			
		/*****create profile****/
		
		$data_profile=array(
		'about_user'=>$this->input->post('about_user')
		);
		$this->db->where('user_id',$user_id);		
		$this->db->update('user_profile',$data_profile);
		
		
		
			/***********agent detail********/
	
		$post_ass=$this->input->post('agent_member_of_association');
		$member_ass='';
		
		if($post_ass)
		{
		
			$member_ass=implode(',',$post_ass);
		
		}
		
		
		$agency_location = explode(',',$this->input->post('agency_location'));
		
		$agency_city='';
		$agency_state='';
		$agency_country='';
		
		
		if(isset($agency_location[0]))
		{
			$agency_city=$agency_location[0];
		}
		if(isset($agency_location[1]))
		{
			$agency_state=$agency_location[1];
		}
		
		if(isset($agency_location[2]))
		{
			$agency_country=$agency_location[2];
		}
		
		
		
		
		
		$agent_data=array(
		'agency_name'=>$this->input->post('agency_name'),
		'contact_person_name'=>$this->input->post('contact_person_name'),
		'agency_location'=>$this->input->post('agency_location'),
		'agency_city'=>$agency_city,
		'agency_state'=>$agency_state,
		'agency_country'=>$agency_country,
		'agency_contact_email'=>$this->input->post('agency_contact_email'),
		'agency_phone'=>$this->input->post('agency_phone'),
		'agency_customer_support_no'=>$this->input->post('agency_customer_support_no'),
		'agency_website'=>seturl($this->input->post('agency_website')),
		'agent_member_of_association'=>$member_ass,
		'agent_app_approved'=>$this->input->post('agent_app_approved'),
		'agent_background_approved'=>$this->input->post('agent_background_approved'),
		'agent_phone_approved'=>$this->input->post('agent_phone_approved'),
		'agent_level'=>$this->input->post('agent_level'),	
		'agent_update_date' =>date('Y-m-d H:i:s')
		);
		
		
		$this->db->where('user_id',$user_id);
		$this->db->update('agent_profile',$agent_data);
		
		
		/*********upload doc****/
		
		
		if($_FILES['file_up']['name']!='')
		{

			$cnt=1; 
			
			$this->load->library('upload');
			$rand=randomCode(); 
				
			for($i=0;$i<count($_FILES['file_up']['name']);$i++)
			 {
			 
				if($_FILES['file_up']['name'][$i]!='')
				{
				
					$_FILES['userfile']['name']    =   $_FILES['file_up']['name'][$i];
					$_FILES['userfile']['type']    =   $_FILES['file_up']['type'][$i];
					$_FILES['userfile']['tmp_name']=   $_FILES['file_up']['tmp_name'][$i];
					$_FILES['userfile']['error']   =   $_FILES['file_up']['error'][$i];
					$_FILES['userfile']['size']    =   $_FILES['file_up']['size'][$i]; 

					$config['file_name']     = $rand.'_agent_document_'.$user_id.'_'.$i;
					$config['upload_path'] =base_path().'upload/agent_doc/';					
					$config['allowed_types'] = 'jpg|jpeg|gif|png|pdf';

					$this->upload->initialize($config);

					if (!$this->upload->do_upload())
					{		
					 $error =  $this->upload->display_errors(); 
					} 
					
					$picture = $this->upload->data();

					$agentdata_doc=array(					
						'user_id'=>$user_id,
						'agent_id'=>$agent_id,
						'agent_document'=>$picture['file_name'],
						'document_date'=>date('Y-m-d H:i:s')					
					);
					$this->db->insert('agent_document',$agentdata_doc);
				}										
			}
		}
		
		
		
	
	}
	
	
	
	
	
	function update_agent()
	{
		
		$data=array(
		'agent_status'=>$this->input->post('agent_status'),
		'agent_app_approved'=>$this->input->post('agent_app_approved'),
		'agent_background_approved'=>$this->input->post('agent_background_approved'),
		'agent_phone_approved'=>$this->input->post('agent_phone_approved'),
		'agent_level'=>$this->input->post('agent_level')		
		);
		
		
		$this->db->where('user_id',$this->input->post('user_id'));
		$this->db->update('agent_profile',$data);
		
	}
	
	function get_agent_document($user_id)
	{
		$this->db->select('*');
		$this->db->from('agent_document');		
		$this->db->where('user_id',$user_id);	
		$this->db->order_by('agent_document_id','asc');	
		
		$query=$this->db->get();
		
		if($query->num_rows()>0) 
		{
			return $query->result();
		}
		
		return 0;	
	}
	
	
	/*** check the user unique email address
	*	var string $email
	*  return boolen
	**/

	function chkinviteemailTaken($email)
	{
		
		if($this->input->post('invite_id')>0)
		{
		 	$query = $this->db->query("select * from ".$this->db->dbprefix('agent_invitation')." where invite_id!='".$this->input->post('invite_id')."' and  agent_email='".$email."'");
			
		} else {
		
			 $query = $this->db->query("select * from ".$this->db->dbprefix('agent_invitation')." where agent_email='".$email."'");
			 
		}
		
		 if($query->num_rows()>0)
		 {
			return true;
		 }
		 else 
		 {
			return false;
		 }		
	}
	
	
	function add_invite()
	{
		$data=array(
		'agent_first_name' => $this->input->post('agent_first_name'),
		'agent_last_name' => $this->input->post('agent_last_name'),
		'agent_email' => $this->input->post('agent_email'),
		'agent_company_name' =>  $this->input->post('agent_company_name'),
		'agent_phone_no' => $this->input->post('agent_phone_no'),
		'agent_about' => $this->input->post('agent_about'),
		'invite_date'=>date('Y-m-d H:i:s'),
		'invite_ip'=>$_SERVER['REMOTE_ADDR'],
		'admin_id' => $this->session->userdata('admin_id')
		);
		
		$this->db->insert('agent_invitation',$data);
		
		return mysql_insert_id();
	}
	
	
	function edit_invite()
	{
		$data=array(
		'agent_first_name' => $this->input->post('agent_first_name'),
		'agent_last_name' => $this->input->post('agent_last_name'),
		'agent_email' => $this->input->post('agent_email'),
		'agent_company_name' =>  $this->input->post('agent_company_name'),
		'agent_phone_no' => $this->input->post('agent_phone_no'),
		'agent_about' => $this->input->post('agent_about'),
		'admin_id' => $this->session->userdata('admin_id')
		);
		
		$this->db->where('invite_id',$this->input->post('invite_id'));
		$this->db->update('agent_invitation',$data);
	}
	
	
	
	function get_total_invite_agent_count()
	{		
		$this->db->select('*');
		$this->db->from('agent_invitation');	
		$this->db->order_by('invite_id','desc');
		$query = $this->db->get();


		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	
	
	function get_all_invite_agent_result($offset, $limit)
	{		
		$this->db->select('*');
		$this->db->from('agent_invitation');	
		$this->db->limit($limit,$offset);
		$this->db->order_by('invite_id','desc');
		
		$query = $this->db->get();


		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
	
	function get_total_search_invite_agent_count($option,$keyword)
	{		
		$this->db->select('*');
		$this->db->from('agent_invitation');	
		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		
		$this->db->order_by('invite_id','desc');
		$query = $this->db->get();


		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	
	
	function get_all_search_invite_agent_result($option,$keyword,$offset, $limit)
	{		
		$this->db->select('*');
		$this->db->from('agent_invitation');
		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
			
		$this->db->limit($limit,$offset);
		$this->db->order_by('invite_id','desc');
		
		$query = $this->db->get();


		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
	
	
	function delete_agent($user_id)
	{
		//==agent profile
		//===agent cities
		//===agent document
		
		//
		//=trip user id
		
		
		
		//===trip assign agent_id (user_id)
		//==trip additional information
		//==trip agent invite
		//==trip_coversation
		///==trip offer
		//===trip_offer_conversation
		//==trip_offer_attachment
			
	}
	
	
	function get_total_userlogin_count()
	{
		$this->db->select('*');
		$this->db->from('user_login ug');
		$this->db->join('user us', 'ug.user_id= us.user_id');
		$this->db->join('agent_profile ag', 'us.user_id= ag.user_id');	
		$this->db->order_by('ug.login_id','desc');
		$query = $this->db->get();


		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_userlogin_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user_login ug');
		$this->db->join('user us', 'ug.user_id= us.user_id');
		$this->db->join('agent_profile ag', 'us.user_id= ag.user_id');	
		$this->db->limit($limit,$offset);
		$this->db->order_by('ug.login_id','desc');
		$query = $this->db->get();


		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
	
	////////=========all ===============
	
	
	function get_total_agent_count()
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->order_by('ag.agent_id','desc');		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;	
	}
	
	
	function get_all_agent_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->order_by('ag.agent_id','desc');
		$this->db->limit($limit,$offset);		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
		
		
	}	
	
	
	function get_total_search_agent_count($option,$keyword)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		$this->db->order_by('ag.agent_id','desc');
				
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;		
		
	}
	
	
	function get_all_search_agent_result($option,$keyword,$offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		$this->db->order_by('ag.agent_id','desc');
		$this->db->limit($limit,$offset);
		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;		
		
	}
	
	
	///============active==================
	
	
	function get_total_active_agent_count()
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->where('ag.agent_status',1);
		$this->db->where('us.user_status',1);
		$this->db->order_by('ag.agent_id','desc');		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}		
		return 0;		
		
	}
	
	
	function get_active_agent_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->where('ag.agent_status',1);
		$this->db->where('us.user_status',1);
		$this->db->order_by('ag.agent_id','desc');
		$this->db->limit($limit,$offset);		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
		
		
	}
	
	
	
	function get_total_search_active_agent_count($option,$keyword)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');		
		$this->db->where('ag.agent_status',1);
		$this->db->where('us.user_status',1);
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		$this->db->order_by('ag.agent_id','desc');
				
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;		
		
	}
	
	
	function get_active_search_agent_result($option,$keyword,$offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->where('ag.agent_status',1);
		$this->db->where('us.user_status',1);		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		$this->db->order_by('ag.agent_id','desc');
		$this->db->limit($limit,$offset);
		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;		
		
	}
	
	
	
	///============inactive==================
	
	
	function get_total_inactive_agent_count()
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->where('ag.agent_status',0);
		$this->db->order_by('ag.agent_id','desc');		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}		
		return 0;		
		
	}
	
	
	function get_inactive_agent_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->where('ag.agent_status',0);
		$this->db->order_by('ag.agent_id','desc');
		$this->db->limit($limit,$offset);		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
		
		
	}
	
	
	
	function get_total_search_inactive_agent_count($option,$keyword)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');		
		$this->db->where('ag.agent_status',0);
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		$this->db->order_by('ag.agent_id','desc');
				
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;		
		
	}
	
	
	function get_inactive_search_agent_result($option,$keyword,$offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->where('ag.agent_status',0);		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		$this->db->order_by('ag.agent_id','desc');
		$this->db->limit($limit,$offset);
		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;		
		
	}
	
	
	
	
	/////================suspend=====
	
	
	
	
	function get_total_suspend_agent_count()
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->where('us.user_status',2);
		$this->db->order_by('ag.agent_id','desc');		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}		
		return 0;		
		
	}
	
	
	function get_suspend_agent_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->where('us.user_status',2);
		$this->db->order_by('ag.agent_id','desc');
		$this->db->limit($limit,$offset);		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
		
		
	}
	
	
	
	function get_total_search_suspend_agent_count($option,$keyword)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');		
		$this->db->where('us.user_status',2);
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		$this->db->order_by('ag.agent_id','desc');
				
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;		
		
	}
	
	
	function get_suspend_search_agent_result($option,$keyword,$offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->where('us.user_status',2);
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		$this->db->order_by('ag.agent_id','desc');
		$this->db->limit($limit,$offset);
		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;		
		
	}
	
	
	
}
?>