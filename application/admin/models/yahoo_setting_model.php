<?php

class Yahoo_setting_model extends CI_Model {
	
    function Yahoo_setting_model()
    {
       parent::__construct();	
    }   
	
	/** admin facebook setting update function
	* var integer $facebook_application_id
	* var integer $facebook_login_enable
	* var string $facebook_api_key
	* var string $facebook_secret_key	
	* var string $facebook_url	
	**/
	function yahoo_setting_update()
	{
		$data = array(			
			'consumer_key' => $this->input->post('consumer_key'),
			'consumer_secret' => $this->input->post('consumer_secret'),
			'yahoo_enable' => $this->input->post('yahoo_enable'),
			
		);
		$this->db->where('yahoo_setting_id',$this->input->post('yahoo_setting_id'));
		$this->db->update('yahoo_setting',$data);
	}
	
	/*** get facebook setting details
	*  return single record array
	**/
	function get_one_yahoo_setting()
	{
		$query = $this->db->get_where('yahoo_setting');
		return $query->row();
	}
}
?>