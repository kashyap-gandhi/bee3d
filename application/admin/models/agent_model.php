<?php
class Agent_model extends CI_Model {	
    function Agent_model()
    {
        parent::__construct();	
    }   
	
	
	
	
	function get_total_report_agent_count()
	{
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->from('admin ad');
		$this->db->join('entry_report_current ec','ad.admin_id=ec.admin_id','left');
		$this->db->order_by('ad.admin_id','desc');
		
		$query = $this->db->get();
		

		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
		
	}
	
	
	function get_all_report_agent_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('admin ad');
		$this->db->join('entry_report_current ec','ad.admin_id=ec.admin_id','left');
		$this->db->order_by('ad.admin_id','desc');
		$this->db->limit($limit,$offset);
		
		$query = $this->db->get();
		

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;		
	}
	
	
	
	

	
	
	/*** check the user unique email address
	*	var string $email
	*  return boolen
	**/

	function emailTaken($email)
	{
	
		if($this->input->post('user_id')>0)
		{
		 	$query = $this->db->query("select * from ".$this->db->dbprefix('user')." where user_id!='".$this->input->post('user_id')."' and  email='".$email."'");
			
		} else {
		
			 $query = $this->db->query("select * from ".$this->db->dbprefix('user')." where email='".$email."'");
			 
		}
		
		 if($query->num_rows()>0)
		 {
			return true;
		 }
		 else 
		 {
			return false;
		 }		
	}
	
	function get_agent_by_id($user_id)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->where('ag.user_id',$user_id);		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->row();
		}
		
		return 0;	
		
		
	}
	
	
	function add_new_report($admin_id,$new_add,$new_approve,$new_reject)
	{
		//====all report
		
		$get_total_report=$this->db->get_where('entry_report_all',array('admin_id'=>$admin_id));
		
		if($get_total_report->num_rows()>0)
		{
			$all_report=$get_total_report->row();
			
			$all_data_up=array(
				'total_add'=>$all_report->total_add+$new_add,
				'total_approve'=>$all_report->total_approve+$new_approve,
				'total_reject'=>$all_report->total_reject+$new_reject				
			);
			
			
			$this->db->where('admin_id',$admin_id);
			$this->db->update('entry_report_all',$all_data_up);
			
		}
		else
		{
			$all_data_up=array(
				'total_add'=>$new_add,
				'total_approve'=>$new_approve,
				'total_reject'=>$new_reject,
				'admin_id'=>$admin_id			
			);
			
			$this->db->insert('entry_report_all',$all_data_up);
			
		}	
		
		///===current report
		
		$get_current_report=$this->db->get_where('entry_report_current',array('admin_id'=>$admin_id));
		
		if($get_current_report->num_rows()>0)
		{
			$current_report=$get_current_report->row();
			
			$current_data_up=array(
				'total_add'=>$current_report->total_add+$new_add,
				'total_approve'=>$current_report->total_approve+$new_approve,
				'total_reject'=>$current_report->total_reject+$new_reject				
			);
			
			
			$this->db->where('admin_id',$admin_id);
			$this->db->update('entry_report_current',$current_data_up);
			
		}
		else
		{
			$current_data_up=array(
				'total_add'=>$new_add,
				'total_approve'=>$new_approve,
				'total_reject'=>$new_reject,
				'admin_id'=>$admin_id			
			);
			
			$this->db->insert('entry_report_current',$current_data_up);
			
		}		
		
	
	}
	
	function add_agent()
	{
	
		
		
		$email_verification_code=randomCode();
		
		
		$password=randomCode();
		
		
		
		
		
		$first_name=$this->input->post('first_name');
		$last_name=$this->input->post('last_name');
		
		$profile_name = $this->input->post('agency_name');
		$profile_name=clean_url($profile_name);
		
		
			
	$chk_url_exists=$this->db->query("select MAX(profile_name) as profile_name from ".$this->db->dbprefix('user')." where profile_name like '".$profile_name."%'");
		
		if($chk_url_exists->num_rows()>0)
		{			
				$get_pr=$chk_url_exists->row();					
				
				$strre='0';
				if($get_pr->profile_name!='')
				{
					$strre=str_replace($profile_name,'',$get_pr->profile_name);
				}
				
				if($strre=='0') 
				{					
					$newcnt=''; 
					  				
				} 
				elseif($strre=='') 
				{					
					$newcnt='1';   				
				}
				else
				{				
					$newcnt=(int)$strre+1;			
				}
				
			 	$profile_name=$profile_name.$newcnt;
						
		}
			
			
			
		
		
		
		$data = array(		
				'full_name' => $first_name.' '.$last_name,		
				'first_name' => $first_name,
				'last_name' => $last_name,	
				'profile_name'=>$profile_name,			
				'email' => $this->input->post('email'),			
				'password' => md5($password),		
				'sign_up_ip' => $this->input->ip_address(),
				'email_verification_code'=>$email_verification_code,
				'zip_code'=>$this->input->post('zip_code'),
				'mobile_no'=>$this->input->post('mobile_no'),			
				'phone_no'=>$this->input->post('phone_no'),						
				'sign_up_date' => date('Y-m-d H:i:s'),
				); 
		$this->db->insert('user', $data);
		$user_id = mysql_insert_id();
		
		
		/*****create profile****/
		
		$data_profile=array(
		'user_id'=>$user_id,
		'current_city'=>$this->input->post('current_city'),
		'about_user'=>$this->input->post('about_user')
		);
		
		$this->db->insert('user_profile',$data_profile);
		
		
		
		
		
		
		
		
			/*** user notification ****/
	
		$user_notification=mysql_query("SHOW COLUMNS FROM ".$this->db->dbprefix('user_notification'));
		$res=mysql_fetch_array($user_notification);
		$fields="";
		$values="";
				
		while($res=mysql_fetch_array($user_notification)){
		//print_r($res['Field']);echo '<br>';
		if($fields==""){$fields.="(`".$res['Field']."`"; $values.="('".$user_id."'";}
		else {$fields.=",`".$res['Field']."`";	 $values.=",'1'";}
		}
		$fields.=")";
		 $values.=")";								   
		$insert_val= $fields.' values '.$values;
		
		$this->db->query("insert into ".$this->db->dbprefix('user_notification')." ".$insert_val."");
		
	
		/*******************/
	
	$post_ass=$this->input->post('agent_member_of_association');
	$member_ass='';
	
	if($post_ass)
	{
	
		$member_ass=implode(',',$post_ass);
	
	}
		
		
		
		
		
		$agency_location = explode(',',$this->input->post('agency_location'));
		
		$agency_city='';
		$agency_state='';
		$agency_country='';
		
		
		if(isset($agency_location[0]))
		{
			$agency_city=$agency_location[0];
		}
		if(isset($agency_location[1]))
		{
			$agency_state=$agency_location[1];
		}
		
		if(isset($agency_location[2]))
		{
			$agency_country=$agency_location[2];
		}
		
		
		
		
		
		$agent_data=array(
		'user_id'=>$user_id,
		'agency_name'=>$this->input->post('agency_name'),
		'contact_person_name'=>$this->input->post('contact_person_name'),
		'agency_location'=>$this->input->post('agency_location'),
		'agency_city'=>$agency_city,
		'agency_state'=>$agency_state,
		'agency_country'=>$agency_country,
		'agency_contact_email'=>$this->input->post('agency_contact_email'),
		'agency_phone'=>$this->input->post('agency_phone'),
		'agency_customer_support_no'=>$this->input->post('agency_customer_support_no'),
		'agency_website'=>seturl($this->input->post('agency_website')),
		'agent_ip'=>$_SERVER['REMOTE_ADDR'],
		'agent_date'=> date('Y-m-d H:i:s'),
		'agent_status'=>1,
		'agent_app_approved'=>$this->input->post('agent_app_approved'),
		'agent_background_approved'=>$this->input->post('agent_background_approved'),
		'agent_phone_approved'=>$this->input->post('agent_phone_approved'),
		'agent_level'=>$this->input->post('agent_level'),
		'agent_member_of_association'=>$member_ass,
		'admin_id'=>$this->session->userdata('admin_id')
		);
		
		
		
		$this->db->insert('agent_profile',$agent_data);
		
		
		$admin_id=$this->session->userdata('admin_id');
		
		$new_add=1;
		
		$new_approve=0;
		$new_reject=0;
		
		
		if($this->session->userdata('admintype') == 1)
		{
			 if($this->input->post('agent_app_approved') == 1)
			 {
			 	$up_agent_data=array('super_admin_approve'=>1,'agent_app_approved'=>1);
				$us_data=array('user_status'=>1);
		
				$new_approve=1;				
			 }
			 else
			 {
			 	$up_agent_data=array('super_admin_approve'=>0,'agent_app_approved'=>0);
				$us_data=array('user_status'=>0);
				$new_reject=1;
			 }
			 
		}
		else
		{
				$up_agent_data=array('super_admin_approve'=>0,'agent_app_approved'=>0);
				$us_data=array('user_status'=>0);
				$new_reject=1;
		}
		
		$this->db->where('user_id',$user_id);
		$this->db->update('agent_profile',$up_agent_data);
		
		$this->db->where('user_id',$user_id);
		$this->db->update('user',$us_data);
		
		////===report part====
		
		$this->add_new_report($admin_id,$new_add,$new_approve,$new_reject);
		
		////===report part====
		
		
	
		return $password;
		
			
			
			
			
	}
	
	
	
	function edit_agent()
	{
		
		$user_id=$this->input->post('user_id');
		$agent_id=$this->input->post('agent_id');
		
		$user_info=get_user_profile_by_id($user_id);
		
		$image_settings = image_setting();
		
		if($_FILES['profileimage']['name']!='')
		{
			
				 
				 
				 $this->load->library('upload');
				 $rand=randomCode(); 
				  
				 $_FILES['userfile']['name']     =   $_FILES['profileimage']['name'];
				 $_FILES['userfile']['type']     =   $_FILES['profileimage']['type'];
				 $_FILES['userfile']['tmp_name'] =   $_FILES['profileimage']['tmp_name'];
				 $_FILES['userfile']['error']    =   $_FILES['profileimage']['error'];
				 $_FILES['userfile']['size']     =   $_FILES['profileimage']['size'];
	  
				 
				 $config['file_name'] = $rand;
				 $config['upload_path'] = base_path().'upload/user_orig/';
				 $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
					
					
				$this->upload->initialize($config);
 
				  if (!$this->upload->do_upload())
				  {
					$error =  $this->upload->display_errors();   
				  } 
				   
				   
				  $picture = $this->upload->data();		
				  $image_name=$picture['file_name'];
				 

					
				$this->load->library('image_lib');					
				$this->image_lib->clear();	
			
				$config['image_library'] = 'GD2';
				$config['thumb_marker'] = "";
				$config['maintain_ratio'] = TRUE;
				$config['create_thumb'] = TRUE;
				$config['quality'] = '100%';
				$config['master_dim'] = 'width';
				$config['source_image'] = $picture['full_path'];
				$config['new_image'] = base_path().'upload/user_thumb/'.$picture['file_name'];					
				$config['width'] = $image_settings->image_small_thumb_width;
				$config['height'] = $image_settings->image_small_thumb_height;
				$this->image_lib->initialize($config);
				
				
					
				if(!$this->image_lib->resize()){
					$error = $this->image_lib->display_errors();				
				}
							
					
					
				$this->image_lib->clear();
				
				$config['image_library'] = 'GD2';
				$config['thumb_marker'] = "";
				$config['maintain_ratio'] = TRUE;
				$config['create_thumb'] = TRUE;
				$config['quality'] = '100%';
				$config['master_dim'] = 'width';
				$config['source_image'] = $picture['full_path'];
				$config['new_image'] = base_path().'upload/user/'.$picture['file_name'];
				$config['width'] = $image_settings->image_medium_thumb_width;
				$config['height'] = $image_settings->image_medium_thumb_height;
				$this->image_lib->initialize($config);
		
				if(!$this->image_lib->resize()){
					$error = $this->image_lib->display_errors();				
				}
						
						
				$this->image_lib->clear();
				
				
				
				
				 if($user_info->profile_image != '') 
					{
						if(file_exists(base_path().'upload/user_orig/'.$user_info->profile_image))
						{
							unlink(base_path().'upload/user_orig/'.$user_info->profile_image);
						}
						if(file_exists(base_path().'upload/user_thumb/'.$user_info->profile_image))
						{
							unlink(base_path().'upload/user_thumb/'.$user_info->profile_image);
						}
						
						if(file_exists(base_path().'upload/user/'.$user_info->profile_image))
						{
							unlink(base_path().'upload/user/'.$user_info->profile_image);
						}
					}
							
				
				
			
				$data_img=array(
					'profile_image' => $picture['file_name'],
				);
				$this->db->where('user_id',$user_id);
				$this->db->update('user_profile',$data_img);
			}
		
		
		
		
		
		/***********user detail*****/
		
		$first_name=$this->input->post('first_name');
		$last_name=$this->input->post('last_name');
		
		
		$profile_name = $this->input->post('agency_name');
		$profile_name=clean_url($profile_name);
		
		//$profile_name=clean_url($first_name.' '.substr($last_name,0,1));
		
		
			
	$chk_url_exists=$this->db->query("select MAX(profile_name) as profile_name from ".$this->db->dbprefix('user')." where user_id!='".$user_id."'  and profile_name like '".$profile_name."%'");
		
		if($chk_url_exists->num_rows()>0)
		{			
				$get_pr=$chk_url_exists->row();					
				
				$strre='0';
				if($get_pr->profile_name!='')
				{
					$strre=str_replace($profile_name,'',$get_pr->profile_name);
				}
				
				if($strre=='0') 
				{					
					$newcnt=''; 
					  				
				} 
				elseif($strre=='') 
				{					
					$newcnt='1';   				
				}
				else
				{				
					$newcnt=(int)$strre+1;			
				}
				
			 	$profile_name=$profile_name.$newcnt;
						
		}
			
			
			
		
		
		
		$data = array(		
				'full_name' => $first_name.' '.$last_name,		
				'first_name' => $first_name,
				'last_name' => $last_name,	
				'profile_name'=>$profile_name,			
				'email' => $this->input->post('email'),			
				'zip_code'=>$this->input->post('zip_code'),
				'mobile_no'=>$this->input->post('mobile_no'),			
				'phone_no'=>$this->input->post('phone_no'),						
				
				); 
		$this->db->where('user_id',$user_id);		
		$this->db->update('user', $data);
		
		
		
		
			
		/*****create profile****/
		
		$data_profile=array(
		'about_user'=>$this->input->post('about_user')
		);
		$this->db->where('user_id',$user_id);		
		$this->db->update('user_profile',$data_profile);
		
		
		
			/***********agent detail********/
	
		$post_ass=$this->input->post('agent_member_of_association');
		$member_ass='';
		
		if($post_ass)
		{
		
			$member_ass=implode(',',$post_ass);
		
		}
		
		
		$agency_location = explode(',',$this->input->post('agency_location'));
		
		$agency_city='';
		$agency_state='';
		$agency_country='';
		
		
		if(isset($agency_location[0]))
		{
			$agency_city=$agency_location[0];
		}
		if(isset($agency_location[1]))
		{
			$agency_state=$agency_location[1];
		}
		
		if(isset($agency_location[2]))
		{
			$agency_country=$agency_location[2];
		}
		
		
		
		if($this->input->post('agent_app_approved') == 1)
		{
		
		
			$agent_data=array(
				'agency_name'=>$this->input->post('agency_name'),
				'contact_person_name'=>$this->input->post('contact_person_name'),
				'agency_location'=>$this->input->post('agency_location'),
				'agency_city'=>$agency_city,
				'agency_state'=>$agency_state,
				'agency_country'=>$agency_country,
				'agency_contact_email'=>$this->input->post('agency_contact_email'),
				'agency_phone'=>$this->input->post('agency_phone'),
				'agency_customer_support_no'=>$this->input->post('agency_customer_support_no'),
				'agency_website'=>seturl($this->input->post('agency_website')),
				'agent_member_of_association'=>$member_ass,
				'agent_app_approved'=>$this->input->post('agent_app_approved'),
				'agent_background_approved'=>$this->input->post('agent_background_approved'),
				'agent_phone_approved'=>$this->input->post('agent_phone_approved'),
				'agent_level'=>$this->input->post('agent_level'),	
				'agent_update_date' =>date('Y-m-d H:i:s'),
				'super_admin_approve' => 1
			);
		
		}
		else
		{
				
			
			$agent_data=array(
			'agency_name'=>$this->input->post('agency_name'),
			'contact_person_name'=>$this->input->post('contact_person_name'),
			'agency_location'=>$this->input->post('agency_location'),
			'agency_city'=>$agency_city,
			'agency_state'=>$agency_state,
			'agency_country'=>$agency_country,
			'agency_contact_email'=>$this->input->post('agency_contact_email'),
			'agency_phone'=>$this->input->post('agency_phone'),
			'agency_customer_support_no'=>$this->input->post('agency_customer_support_no'),
			'agency_website'=>seturl($this->input->post('agency_website')),
			'agent_member_of_association'=>$member_ass,
			'agent_app_approved'=>$this->input->post('agent_app_approved'),
			'agent_background_approved'=>$this->input->post('agent_background_approved'),
			'agent_phone_approved'=>$this->input->post('agent_phone_approved'),
			'agent_level'=>$this->input->post('agent_level'),	
			'agent_update_date' =>date('Y-m-d H:i:s')
			
			);
		}
		
		$this->db->where('user_id',$user_id);
		$this->db->update('agent_profile',$agent_data);
		
		
		
		
		$password=randomCode();
		
		
		$agent_detail=$this->get_agent_by_id($user_id);
		
		$admin_id=$agent_detail->admin_id;
		
		$new_add=0;		
		$new_approve=0;
		$new_reject=0;
		
		
		
		if($this->input->post('agent_app_approved') == 1)
		{
				$userdata = array(
					'user_status'=>1
				);		
				$this->db->where('user_id',$user_id);
				$this->db->update('user',$userdata);
			
				$data=array(			
					'agent_app_approved'=>1,
					'agent_background_approved'=>$this->input->post('agent_background_approved'),
					'agent_phone_approved'=>$this->input->post('agent_phone_approved'),
					'agent_level'=>$this->input->post('agent_level'),
					'super_admin_approve'=>1		
				);
				
				$new_approve=1;
				
		}
		else
		{
			
				/*$userdata = array(
					'user_status'=>0
				);		
				$this->db->where('user_id',$this->input->post('user_id'));
				$this->db->update('user',$userdata);*/
				
				$data=array(			
					'agent_app_approved'=>0,
					'agent_background_approved'=>$this->input->post('agent_background_approved'),
					'agent_phone_approved'=>$this->input->post('agent_phone_approved'),
					'agent_level'=>$this->input->post('agent_level'),
					'super_admin_approve'=>0
				);
				
				$new_reject=1;
		}
		
		
		////===report part====
		
		if($admin_id>0)
		{
			//===already apporved then do not update
			if($agent_detail->agent_app_approved==1 && $new_approve==1)
			{
				$new_approve=0;
			}
			
			//===already in waiting or rejected then do not update
			if($agent_detail->agent_app_approved==0 && $new_reject==1)
			{
				$new_reject=0;
			}
			
			///====if already apporoved and now reject
			if($agent_detail->agent_app_approved==1 && $new_reject==1)
			{
				$new_approve=-1;
				$new_reject=1;
				
			}
			
			///====if already reject or waiting and now approved
			if($agent_detail->agent_app_approved==0 && $new_approve==1)
			{
				$new_approve=1;
				$new_reject=-1;
				
			}
			
			$this->add_new_report($admin_id,$new_add,$new_approve,$new_reject);
		}
		
		////===report part====
		
		
		
		
		
		$this->db->where('user_id',$user_id);
		$this->db->update('agent_profile',$data);
		
		
		
		
		
		
		
		$agent_detail=$this->get_agent_by_id($user_id);

		
		if($this->input->post('agent_app_approved') == 1)
		{
			if($agent_detail->verify_email == 0)
			{
				$data_user = array('password'=>md5($password));
				
				$this->db->where('user_id',$user_id);
				$this->db->update('user',$data_user);
				return $password;				
			}
		}
		
		
		
		
		
		
		
		/*********upload doc****/
		
		
		if($_FILES['file_up']['name']!='')
		{

			$cnt=1; 
			
			$this->load->library('upload');
			$rand=randomCode(); 
				
			for($i=0;$i<count($_FILES['file_up']['name']);$i++)
			 {
			 
				if($_FILES['file_up']['name'][$i]!='')
				{
				
					$_FILES['userfile']['name']    =   $_FILES['file_up']['name'][$i];
					$_FILES['userfile']['type']    =   $_FILES['file_up']['type'][$i];
					$_FILES['userfile']['tmp_name']=   $_FILES['file_up']['tmp_name'][$i];
					$_FILES['userfile']['error']   =   $_FILES['file_up']['error'][$i];
					$_FILES['userfile']['size']    =   $_FILES['file_up']['size'][$i]; 

					$config['file_name']     = $rand.'_agent_document_'.$user_id.'_'.$i;
					$config['upload_path'] =base_path().'upload/agent_doc/';					
					$config['allowed_types'] = 'jpg|jpeg|gif|png|pdf';

					$this->upload->initialize($config);

					if (!$this->upload->do_upload())
					{		
					 $error =  $this->upload->display_errors(); 
					} 
					
					$picture = $this->upload->data();

					$agentdata_doc=array(					
						'user_id'=>$user_id,
						'agent_id'=>$agent_id,
						'agent_document'=>$picture['file_name'],
						'document_date'=>date('Y-m-d H:i:s')					
					);
					$this->db->insert('agent_document',$agentdata_doc);
				}										
			}
		}
		
		
		
		
		
		
	
	}
	
	
	
	
	
	function update_agent()
	{
	
		
		$password=randomCode();
		
		$user_id=$this->input->post('user_id');
		
		$agent_detail=$this->get_agent_by_id($user_id);
		
		$admin_id=$agent_detail->admin_id;
		
		$new_add=0;		
		$new_approve=0;
		$new_reject=0;
		
		
		
		if($this->input->post('agent_app_approved') == 1)
		{
				$userdata = array(
					'user_status'=>1
				);		
				$this->db->where('user_id',$this->input->post('user_id'));
				$this->db->update('user',$userdata);
			
				$data=array(			
					'agent_app_approved'=>1,
					'agent_background_approved'=>$this->input->post('agent_background_approved'),
					'agent_phone_approved'=>$this->input->post('agent_phone_approved'),
					'agent_level'=>$this->input->post('agent_level'),
					'super_admin_approve'=>1		
				);
				
				$new_approve=1;
		}
		else
		{
			
				/*$userdata = array(
					'user_status'=>0
				);		
				$this->db->where('user_id',$this->input->post('user_id'));
				$this->db->update('user',$userdata);*/
				
				$data=array(			
					'agent_app_approved'=>0,
					'agent_background_approved'=>$this->input->post('agent_background_approved'),
					'agent_phone_approved'=>$this->input->post('agent_phone_approved'),
					'agent_level'=>$this->input->post('agent_level'),
					'super_admin_approve'=>0
				);
				
				$new_reject=1;
		}
		
		
		////===report part====
		
		if($admin_id>0)
		{
			//===already apporved then do not update
			if($agent_detail->agent_app_approved==1 && $new_approve==1)
			{
				$new_approve=0;
			}
			
			//===already in waiting or rejected then do not update
			if($agent_detail->agent_app_approved==0 && $new_reject==1)
			{
				$new_reject=0;
			}
			
			///====if already apporoved and now reject
			if($agent_detail->agent_app_approved==1 && $new_reject==1)
			{
				$new_approve=-1;
				$new_reject=1;
				
			}
			
			///====if already reject or waiting and now approved
			if($agent_detail->agent_app_approved==0 && $new_approve==1)
			{
				$new_approve=1;
				$new_reject=-1;
				
			}
			
			$this->add_new_report($admin_id,$new_add,$new_approve,$new_reject);
		}
		
		////===report part====
		
		
		
		$this->db->where('user_id',$this->input->post('user_id'));
		$this->db->update('agent_profile',$data);
		
		
		
		
		
		
		
		
		
		$agent_detail=$this->get_agent_by_id($this->input->post('user_id'));

		
		if($this->input->post('agent_app_approved') == 1)
		{
			if($agent_detail->verify_email == 0)
			{
				$data_user = array('password'=>md5($password));
				
				$this->db->where('user_id',$this->input->post('user_id'));
				$this->db->update('user',$data_user);
				return $password;
				
			}
		}
		
		
	
		
	}
	
	/**********Resend invitation**********/
	function resend_invitation_agent($user_id='')
	{
	
	
		$password=randomCode();
		
		$data=array(
			'password'=>md5($password),
		);
			
		$this->db->where('user_id',$user_id);
		if($this->db->update('user',$data))
		{
			return $password;
		}
		else
		{
			return 0;
		}	
		
	}
	/*********End resend invitation*********/
	
	function get_agent_document($user_id)
	{
		$this->db->select('*');
		$this->db->from('agent_document');		
		$this->db->where('user_id',$user_id);	
		$this->db->order_by('agent_document_id','asc');	
		
		$query=$this->db->get();
		
		if($query->num_rows()>0) 
		{
			return $query->result();
		}
		
		return 0;	
	}
	
	
	/*** check the user unique email address
	*	var string $email
	*  return boolen
	**/

	function chkinviteemailTaken($email)
	{
		
		if($this->input->post('invite_id')>0)
		{
		 	$query = $this->db->query("select * from ".$this->db->dbprefix('agent_invitation')." where invite_id!='".$this->input->post('invite_id')."' and  agent_email='".$email."'");
			
		} else {
		
			 $query = $this->db->query("select * from ".$this->db->dbprefix('agent_invitation')." where agent_email='".$email."'");
			 
		}
		
		 if($query->num_rows()>0)
		 {
			return true;
		 }
		 else 
		 {
			return false;
		 }		
	}
	
	
	function add_invite()
	{
		$data=array(
		'agent_first_name' => $this->input->post('agent_first_name'),
		'agent_last_name' => $this->input->post('agent_last_name'),
		'agent_email' => $this->input->post('agent_email'),
		'agent_company_name' =>  $this->input->post('agent_company_name'),
		'agent_phone_no' => $this->input->post('agent_phone_no'),
		'agent_about' => $this->input->post('agent_about'),
		'invite_date'=>date('Y-m-d H:i:s'),
		'invite_ip'=>$_SERVER['REMOTE_ADDR'],
		'admin_id' => $this->session->userdata('admin_id')
		);
		
		$this->db->insert('agent_invitation',$data);
		
		return mysql_insert_id();
	}
	
	
	function edit_invite()
	{
		$data=array(
		'agent_first_name' => $this->input->post('agent_first_name'),
		'agent_last_name' => $this->input->post('agent_last_name'),
		'agent_email' => $this->input->post('agent_email'),
		'agent_company_name' =>  $this->input->post('agent_company_name'),
		'agent_phone_no' => $this->input->post('agent_phone_no'),
		'agent_about' => $this->input->post('agent_about'),
		'admin_id' => $this->session->userdata('admin_id')
		);
		
		$this->db->where('invite_id',$this->input->post('invite_id'));
		$this->db->update('agent_invitation',$data);
	}
	
	
	
	function get_total_invite_agent_count()
	{		
		$this->db->select('*');
		$this->db->from('agent_invitation');	
		$this->db->order_by('invite_id','desc');
		$query = $this->db->get();


		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	
	
	function get_all_invite_agent_result($offset, $limit)
	{		
		$this->db->select('*');
		$this->db->from('agent_invitation');	
		$this->db->limit($limit,$offset);
		$this->db->order_by('invite_id','desc');
		
		$query = $this->db->get();


		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
	
	function get_total_search_invite_agent_count($option,$keyword)
	{		
		$this->db->select('*');
		$this->db->from('agent_invitation');	
		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		
		$this->db->order_by('invite_id','desc');
		$query = $this->db->get();


		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	
	
	function get_all_search_invite_agent_result($option,$keyword,$offset, $limit)
	{		
		$this->db->select('*');
		$this->db->from('agent_invitation');
		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
			
		$this->db->limit($limit,$offset);
		$this->db->order_by('invite_id','desc');
		
		$query = $this->db->get();


		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
	
	
	function delete_agent($user_id)
	{
		
		///===check Block report
	
		$check_user_block_report=$this->db->get_where('block_report',array('user_id'=>$user_id));
		
		if($check_user_block_report->num_rows()>0)
		{
			$this->db->delete('block_report',array('user_id'=>$user_id));
		}	
		
		///===check Block by report
	
		$check_user_block_by_report=$this->db->get_where('block_report',array('report_by_user_id'=>$user_id));
		
		if($check_user_block_by_report->num_rows()>0)
		{
			$this->db->delete('block_report',array('report_by_user_id'=>$user_id));
		}	
		
		
		
		///===check Agent cities	
		$check_user_cities=$this->db->get_where('agent_cities',array('user_id'=>$user_id));
		
		if($check_user_cities->num_rows()>0)
		{
			$this->db->delete('agent_cities',array('user_id'=>$user_id));
		}	
		
		
		
		
		///===check Deal user id 	
		$check_deal_user=$this->db->get_where('deal',array('user_id'=>$user_id));
		
		if($check_deal_user->num_rows()>0)
		{
			$res_deal=$check_deal_user->result();
			
			foreach($res_deal as $del)
			{
				//**Delete deal related quote*/
				$check_trip_quote=$this->db->get_where('trip_quote',array('deal_id'=>$del->deal_id));
		
				if($check_trip_quote->num_rows()>0)
				{
					$this->db->delete('trip_quote',array('deal_id'=>$del->deal_id));
				}				
				
			}		
			
			$this->db->delete('deal',array('user_id'=>$user_id));
		}	
		
		
		//////////=== Check Transaction  Delete transaction
		$check_transaction =$this->db->get_where('transaction',array('user_id'=>$user_id));
		
		if($check_transaction->num_rows()>0)
		{
			$this->db->delete('transaction',array('user_id'=>$user_id));
		}	
		
		
		////Delete trip
		$check_trip =$this->db->get_where('trip',array('user_id'=>$user_id));
		
		if($check_trip->num_rows()>0)
		{
			$res_trip=$check_trip->result();
			
			foreach($res_trip as $trp)
			{
				
				//**delete trip additionl information*/
				$check_trip_additional_info=$this->db->get_where('trip_additional_information',array('trip_id'=>$trp->trip_id));
		
				if($check_trip_additional_info->num_rows()>0)
				{
					$this->db->delete('trip_additional_information',array('trip_id'=>$trp->trip_id));
				}	
				
				
				//**delete trip transaction information*/
				$check_trip_transaction=$this->db->get_where('transaction',array('trip_id'=>$trp->trip_id));
		
				if($check_trip_transaction->num_rows()>0)
				{
					$this->db->delete('transaction',array('trip_id'=>$trp->trip_id));
				}	
				
				//**delete trip alerts from user alerts*/
				$check_trip_alerts=$this->db->get_where('user_alerts',array('trip_id'=>$trp->trip_id));
		
				if($check_trip_alerts->num_rows()>0)
				{
					$this->db->delete('user_alerts',array('trip_id'=>$trp->trip_id));
				}	
				
				
				//**delete trip block reports*/
				$check_trip_block_alerts=$this->db->get_where('block_report',array('trip_id'=>$trp->trip_id));
		
				if($check_trip_block_alerts->num_rows()>0)
				{
					$this->db->delete('block_report',array('trip_id'=>$trp->trip_id));
				}	
				
				
				//**delete trip relates user review*/
				$check_trip_user_review=$this->db->get_where('user_review',array('trip_id'=>$trp->trip_id));
		
				if($check_trip_user_review->num_rows()>0)
				{
					$this->db->delete('user_review',array('trip_id'=>$trp->trip_id));
				}	
				
				//**delete trip agent invite*/
				$check_trip_agent_invite=$this->db->get_where('trip_agent_invite',array('trip_id'=>$trp->trip_id));
		
				if($check_trip_agent_invite->num_rows()>0)
				{
					$this->db->delete('trip_agent_invite',array('trip_id'=>$trp->trip_id));
				}	
				
				//**delete trip Converasation*/
				$check_trip_conversation=$this->db->get_where('trip_coversation',array('trip_id'=>$trp->trip_id));
		
				if($check_trip_conversation->num_rows()>0)
				{
					
					$res_trip_rply = $check_trip_conversation->result();
					//**delete trip Converasation from rplay convarsation id*/
					
					
						foreach($res_trip_rply as $trp_rply)
						{
							$check_trip_replay_conversation=$this->db->get_where('trip_coversation',array('reply_trip_conversation_id'=>$trp_rply->trip_id));
							
							if($check_trip_replay_conversation->num_rows()>0)
							{
								$this->db->delete('trip_coversation',array('reply_trip_conversation_id'=>$trp_rply->trip_conversation_id));
							}	
							
						}
					
					$this->db->delete('trip_coversation',array('trip_id'=>$trp->trip_id));
				}	
				
				
				
				//**delete trip offer*/
				$check_trip_offer=$this->db->get_where('trip_offer',array('trip_id'=>$trp->trip_id));
		
				if($check_trip_offer->num_rows()>0)
				{
					
					
					
						foreach($res_trip_offer as $trp_offer)
						{
							
							/*****trip offer document***/
							$check_trip_offer_docs=$this->db->get_where('trip_offer_attachment',array('trip_offer_id'=>$trp_offer->trip_offer_id));		
							if($check_trip_offer_docs->num_rows()>0)
							{
								$res_trip_offer = $check_trip_offer_docs->result();
								
								foreach($res_trip_offer as $trp_offer_doc)
								{
									if($trp_offer_doc->offer_attachment != '')
									{
										if(file_exists(base_path().'upload/trip_offer_doc/'.$trp_offer_doc->offer_attachment))
										{
											unlink(base_path().'upload/trip_offer_doc/'.$trp_offer_doc->offer_attachment);
										}
									}
									
								}
								$this->db->delete('trip_offer_attachment',array('trip_offer_id'=>$trp_offer->trip_offer_id));
								
							}
						}
				
					
					/****trip offer conversation*/
					$check_trip_offer_con=$this->db->get_where('trip_offer_conversation',array('trip_id'=>$trp->trip_id));
					
					
					if($check_trip_offer_con->num_rows() > 0)
					{
						$res_trip_offer_con = $check_trip_offer_con->result();
						
						foreach($res_trip_offer_con as $trp_offer_con)
						{
							$this->db->delete('trip_offer_conversation',array('trip_offer_conversation_id'=>$trp_offer_con->trip_offer_conversation_id));
						}
					}	
					
					
					$this->db->delete('trip_offer',array('trip_id'=>$trp->trip_id));
				}	
				/********/
			
			}		
			
			$this->db->delete('trip',array('user_id'=>$res_trip->trip_id));
		}
		
		/***********End delete trip**********/	
		
		
		//////////=== Check trip conversation
		$check_trip_convration_post =$this->db->get_where('trip_coversation',array('post_to_user_id'=>$user_id));
		
		if($check_trip_convration_post->num_rows()>0)
		{
			$this->db->delete('trip_coversation',array('post_to_user_id'=>$user_id));
		}	
		
		$check_trip_convration_post_by =$this->db->get_where('trip_coversation',array('post_by_user_id'=>$user_id));
		
		if($check_trip_convration_post_by->num_rows()>0)
		{
			$this->db->delete('trip_coversation',array('post_by_user_id'=>$user_id));
		}	
		
		//////////=== Check trip agent invite
		$check_trip_agent_invite =$this->db->get_where('trip_agent_invite',array('invite_by_user_id'=>$user_id));
		
		if($check_trip_agent_invite->num_rows()>0)
		{
			$this->db->delete('trip_agent_invite',array('invite_by_user_id'=>$user_id));
		}	
		
		$check_trip_agent_invite_to =$this->db->get_where('trip_agent_invite',array('invite_to_user_id'=>$user_id));
		
		if($check_trip_agent_invite_to->num_rows()>0)
		{
			$this->db->delete('trip_agent_invite',array('invite_to_user_id'=>$user_id));
		}	
		
		
		/*Delete user alerts*/
		$check_user_alerts_to =$this->db->get_where('user_alerts',array('alerts_to_user_id'=>$user_id));
		
		if($check_user_alerts_to->num_rows()>0)
		{
			$this->db->delete('user_alerts',array('alerts_to_user_id'=>$user_id));
		}
		
		$check_user_alerts_by =$this->db->get_where('user_alerts',array('alerts_by_user_id'=>$user_id));
		
		if($check_user_alerts_by->num_rows()>0)
		{
			$this->db->delete('user_alerts',array('alerts_by_user_id'=>$user_id));
		}
		
		/**Delete user review*/
		
		$check_user_review_to =$this->db->get_where('user_review',array('review_to_user_id'=>$user_id));
		
		if($check_user_review_to->num_rows()>0)
		{
			$this->db->delete('user_review',array('review_to_user_id'=>$user_id));
		}
		
		$check_user_review_by =$this->db->get_where('user_review',array('review_by_user_id'=>$user_id));
		
		if($check_user_review_by->num_rows()>0)
		{
			$this->db->delete('user_review',array('review_by_user_id'=>$user_id));
		}
		
		
		///===check agent profile
	
		$check_agent_profile=$this->db->get_where('agent_profile',array('user_id'=>$user_id));
		
		if($check_agent_profile->num_rows()>0)
		{
			$this->db->delete('agent_profile',array('user_id'=>$user_id));
		}
		
		
		/**Delete Agent Document*/
		
		$check_user_agent_document =$this->db->get_where('agent_document',array('user_id'=>$user_id));
		
		if($check_user_agent_document->num_rows()>0)
		{
			$res_user_doc = $check_user_agent_document->result();
			
			foreach($res_user_doc as $res_doc)
			{
				if($res_doc->offer_attachment != '')
				{
					if(file_exists(base_path().'upload/agent_doc/'.$res_doc->agent_document))
					{
						unlink(base_path().'upload/agent_doc/'.$res_doc->agent_document);
					}
				}
				
			}
			$this->db->delete('agent_document',array('user_id'=>$user_id));	
			
		}
		
		
		///===check user tripqote
	
		$check_user_quote=$this->db->get_where('trip_quote',array('user_id'=>$user_id));
		
		if($check_user_quote->num_rows()>0)
		{
			$this->db->delete('trip_quote',array('user_id'=>$user_id));
		}
		 
		/////*********check user card info****/       
		$check_user_card_info =$this->db->get_where('user_card_info',array('user_id'=>$user_id));
		
		if($check_user_card_info->num_rows()>0)
		{
			$this->db->delete('user_card_info',array('user_id'=>$user_id));
		}	
			
		
		
		///===check user notification
	
		$check_user_notification=$this->db->get_where('user_notification',array('user_id'=>$user_id));
		
		if($check_user_notification->num_rows()>0)
		{
			$this->db->delete('user_notification',array('user_id'=>$user_id));
		}
	
		
		///===check user suspend
	
		$check_user_suspend=$this->db->get_where('user_suspend',array('user_id'=>$user_id));
		
		if($check_user_suspend->num_rows()>0)
		{
		
			$resup=$check_user_suspend->result();
			
			foreach($resup as $sup)
			{
				
				$check_suspend_msg=$this->db->get_where('user_suspend_message',array('user_suspend_id'=>$sup->user_suspend_id));
		
				if($check_suspend_msg->num_rows()>0)
				{
					$this->db->delete('user_suspend_message',array('user_suspend_id'=>$sup->user_suspend_id));
				}				
				
			}		
			
			$this->db->delete('user_suspend',array('user_id'=>$sup->user_id));
			
		}
		
			///===check user login
	
		$check_user_login=$this->db->get_where('user_login',array('user_id'=>$user_id));
		
		if($check_user_login->num_rows()>0)
		{
			$this->db->delete('user_login',array('user_id'=>$user_id));
		}
		
		///===check user profile
	
		$check_user_profile=$this->db->get_where('user_profile',array('user_id'=>$user_id));
		
		if($check_user_profile->num_rows()>0)
		{
				$res_img=$check_user_profile->row();
			
				if($res_img->profile_image != '')
				{
					if(file_exists(base_path().'upload/user_orig/'.$res_img->profile_image))
					{
						unlink(base_path().'upload/user_orig/'.$res_img->profile_image);
					}
					if(file_exists(base_path().'upload/user_thumb/'.$res_img->profile_image))
					{
						unlink(base_path().'upload/user_thumb/'.$res_img->profile_image);
					}
					
					if(file_exists(base_path().'upload/user_medium/'.$res_img->profile_image))
					{
						unlink(base_path().'upload/user_medium/'.$res_img->profile_image);
					}
				}
			
			$this->db->delete('user_profile',array('user_id'=>$user_id));
		}
		
		
		
		
		///===check user 
	
		$check_user_own=$this->db->get_where('user',array('user_id'=>$user_id));
		
		if($check_user_own->num_rows()>0)
		{
			$this->db->delete('user',array('user_id'=>$user_id));
		}
			
			////==destroy cache====	
			$this->load->driver('cache');			
			
			$supported_cache=check_supported_cache_driver();
			
			////==destroy now====		
			if(isset($supported_cache))
			{
				if($supported_cache!='' && $supported_cache!='none')
				{	
					if($this->cache->$supported_cache->get('user_login'.$user_id))
					{								
						$this->cache->$supported_cache->delete('user_login'.$user_id);						
					}
				}
				
			}	
		
		
		
	}
	
	
	function get_total_userlogin_count()
	{
		$this->db->select('*');
		$this->db->from('user_login ug');
		$this->db->join('user us', 'ug.user_id= us.user_id');
		$this->db->join('agent_profile ag', 'us.user_id= ag.user_id');	
		$this->db->order_by('ug.login_id','desc');
		$query = $this->db->get();


		if ($query->num_rows() > 0) {
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_userlogin_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user_login ug');
		$this->db->join('user us', 'ug.user_id= us.user_id');
		$this->db->join('agent_profile ag', 'us.user_id= ag.user_id');	
		$this->db->limit($limit,$offset);
		$this->db->order_by('ug.login_id','desc');
		$query = $this->db->get();


		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
	/******Pending Agent list****////////
	function get_total_pending_agent_count()
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->order_by('ag.agent_id','desc');	
		$this->db->where('ag.super_admin_approve',0);	
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;	
	}
	
	function get_all_pending_agent_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->order_by('ag.agent_id','desc');
		$this->db->where('ag.super_admin_approve',0);	
		$this->db->limit($limit,$offset);		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
		
		
	}	
	
	
	function get_total_search_pending_agent_count($option,$keyword)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');		
		$this->db->where('ag.super_admin_approve',0);
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		$this->db->order_by('ag.agent_id','desc');
				
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;		
		
	}
	
	
	function get_all_search_pending_agent_result($option,$keyword,$offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');		
		$this->db->where('ag.super_admin_approve',0);
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		$this->db->order_by('ag.agent_id','desc');
		$this->db->limit($limit,$offset);
		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;		
		
	}
	
	/********End Pending Agent List********///////
	////////=========all ===============
	
	
	function get_total_agent_count()
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->order_by('ag.agent_id','desc');		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;	
	}
	
	
	function get_all_agent_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->order_by('ag.agent_id','desc');
		$this->db->limit($limit,$offset);		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
		
		
	}	
	
	
	function get_total_search_agent_count($option,$keyword)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		$this->db->order_by('ag.agent_id','desc');
				
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;		
		
	}
	
	
	function get_all_search_agent_result($option,$keyword,$offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		$this->db->order_by('ag.agent_id','desc');
		$this->db->limit($limit,$offset);
		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;		
		
	}
	
	
	///============active==================
	
	
	function get_total_active_agent_count()
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		//$this->db->where('ag.agent_status',1);
		$this->db->where('ag.agent_app_approved',1);
		$this->db->where('us.user_status',1);
		$this->db->order_by('ag.agent_id','desc');		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}		
		return 0;		
		
	}
	
	
	function get_active_agent_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		//$this->db->where('ag.agent_status',1);
		$this->db->where('ag.agent_app_approved',1);
		$this->db->where('us.user_status',1);
		$this->db->order_by('ag.agent_id','desc');
		$this->db->limit($limit,$offset);		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
		
		
	}
	
	
	
	function get_total_search_active_agent_count($option,$keyword)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');		
		//$this->db->where('ag.agent_status',1);
		$this->db->where('ag.agent_app_approved',1);
		$this->db->where('us.user_status',1);
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		$this->db->order_by('ag.agent_id','desc');
				
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;		
		
	}
	
	
	function get_active_search_agent_result($option,$keyword,$offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		//$this->db->where('ag.agent_status',1);
		$this->db->where('ag.agent_app_approved',1);
		$this->db->where('us.user_status',1);		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		$this->db->order_by('ag.agent_id','desc');
		$this->db->limit($limit,$offset);
		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;		
		
	}
	
	
	
	///============inactive==================
	
	
	function get_total_inactive_agent_count()
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->where('ag.agent_app_approved',0);
		$this->db->order_by('ag.agent_id','desc');		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}		
		return 0;		
		
	}
	
	
	function get_inactive_agent_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->where('ag.agent_app_approved',0);
		$this->db->order_by('ag.agent_id','desc');
		$this->db->limit($limit,$offset);		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
		
		
	}
	
	
	
	function get_total_search_inactive_agent_count($option,$keyword)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');		
		$this->db->where('ag.agent_app_approved',0);
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		$this->db->order_by('ag.agent_id','desc');
				
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;		
		
	}
	
	
	function get_inactive_search_agent_result($option,$keyword,$offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->where('ag.agent_app_approved',0);		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		$this->db->order_by('ag.agent_id','desc');
		$this->db->limit($limit,$offset);
		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;		
		
	}
	
	
	
	
	/////================suspend=====
	
	
	
	
	function get_total_suspend_agent_count()
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->where('us.user_status',2);
		$this->db->order_by('ag.agent_id','desc');		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}		
		return 0;		
		
	}
	
	
	function get_suspend_agent_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->where('us.user_status',2);
		$this->db->order_by('ag.agent_id','desc');
		$this->db->limit($limit,$offset);		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
		
		
	}
	
	
	
	function get_total_search_suspend_agent_count($option,$keyword)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');		
		$this->db->where('us.user_status',2);
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		$this->db->order_by('ag.agent_id','desc');
				
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;		
		
	}
	
	
	function get_suspend_search_agent_result($option,$keyword,$offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user us');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('agent_profile ag','us.user_id=ag.user_id');
		$this->db->where('us.user_status',2);
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}		
		
		$this->db->order_by('ag.agent_id','desc');
		$this->db->limit($limit,$offset);
		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;		
		
	}
	
	
	
}
?>