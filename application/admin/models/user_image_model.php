<?php

class User_image_model extends CI_Model {
	
    function User_image_model()
    {
        parent::__construct();	
    }   

	
	
	
	function check_user_profile_exists($user_id)
	{
		
		
		
		$query=$this->db->query("select * from ".$this->db->dbprefix('user')." where user_id='".$user_id."'");
		
		if($query->num_rows()>0)
		{
			return true;
		}
		else
		{
			return false;		
		}
		
	
	}
	
	function get_user_profile($user_id)
	{
		
		$query=$this->db->query("select * from ".$this->db->dbprefix('user')." usr, ".$this->db->dbprefix('user_profile')." pr where usr.user_id=pr.user_id and usr.user_id='".$user_id."'");		
		return $query->row();
	
	}
	
	
	function get_user_total_image($user_id)
	{
		$this->db->select('*');
		$this->db->from('images im');
		$this->db->join('user us','im.image_user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->where('im.image_user_id',$user_id);

		$this->db->order_by('im.image_id','desc');
	
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();		
		}
		
		return 0;
	}
	
	
	function get_user_image($user_id,$limit,$offset)
	{
		$this->db->select('*');
		$this->db->from('images im');
		$this->db->join('user us','im.image_user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->where('im.image_user_id',$user_id);

		$this->db->order_by('im.image_id','desc');
		$this->db->limit($limit,$offset);
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}
		
		return 0;
	}
	
	
	function get_image_by_id($image_id)
	{
		$this->db->select('*');
		$this->db->from('images im');
		$this->db->join('user us','im.image_user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->where('im.image_id',$image_id);
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->row();		
		}
		
		return 0;
	}
	
	
	function get_total_search_image_count($user_id,$option,$keyword)
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		$this->db->select('*');
		$this->db->from('images img');
		$this->db->join('user us','img.image_user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->where('img.image_user_id',$user_id);
		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
		
		
		
		$this->db->order_by('img.image_id','desc');
				
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();		
		}
		
		return 0;
	}
	
	function get_all_search_image_result($user_id,$option,$keyword,$offset, $limit)
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
		
		$this->db->select('*');
		$this->db->from('images img');
		$this->db->join('user us','img.image_user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->where('img.image_user_id',$user_id);
		
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
		
		
		
		$this->db->order_by('img.image_id','desc');
		$this->db->limit($limit,$offset);
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}
		
		return 0;
	}
	
}
?>