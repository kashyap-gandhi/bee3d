<?php
class Package_model extends CI_Model
{

	function Package_model()
	{
		parent::__construct();
	}
	
	
	
	
	
	function package_item_insert()
	{
			
			$data=array(			
			'item_name' => $this->input->post('item_name'),
			'item_status' => $this->input->post('item_status')		
			);
			
			$this->db->insert('package_item',$data);
			
	}
	
	
	function package_item_update()
	{
			
			$data=array(			
			'item_name' => $this->input->post('item_name'),
			'item_status' => $this->input->post('item_status')		
			);
			
			$this->db->where('package_item_id',$this->input->post('package_item_id'));
			$this->db->update('package_item',$data);
			
	}
	
	function get_package_item_detail($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('package_item')." where package_item_id='".$id."'");
		return $query->row();
	}
	
	
	
	function get_total_package_item_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('package_item'));		
		return $query->num_rows();		
	}
	
	function get_all_package_item($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('package_item')." order by package_item_id desc limit ".$limit." offset ".$offset);		
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	
	
	function get_all_package_items()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('package_item')." where item_status=1 order by item_name asc");		
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	
	/////////////////===================package============
	
	function package_insert()
	{
			
		
		$package_short_description=$this->input->post('package_short_description');
		$package_short_description=str_replace('"','KSYDOU',$package_short_description);
		$package_short_description=str_replace("'",'KSYSING',$package_short_description);
				
	
		
		
		$package_description=$this->input->post('package_description');
		$package_description=str_replace('"','KSYDOU',$package_description);
		$package_description=str_replace("'",'KSYSING',$package_description);
		
		$package_item='';
				
		$post_item=$this->input->post('package_item');
		
		if($post_item)
		{
			
			foreach($post_item as $item_id)
			{
				$package_item.= $item_id.',';
			}
			
			$package_item = substr($package_item,'0',-1);
		}
		
			
			
			
			$data=array(			
			'package_name' => $this->input->post('package_name'),
			'package_short_description' => $package_short_description,
			'package_description' => $package_description,
			'package_price' => $this->input->post('package_price'),
                        'package_is_free' => $this->input->post('package_is_free'),
                        'package_points' => $this->input->post('package_points'),
                        'package_type' => $this->input->post('package_type'),
			'package_days' => $this->input->post('package_days'),
			'package_item' => $package_item,
			'package_date' => date('Y-m-d H:i:s'),
			'package_status' => $this->input->post('package_status')		
			);
			
			$this->db->insert('package',$data);
			
	}
	
	
	function package_update()
	{
			
		$package_short_description=$this->input->post('package_short_description');
		$package_short_description=str_replace('"','KSYDOU',$package_short_description);
		$package_short_description=str_replace("'",'KSYSING',$package_short_description);
				
	
		
		
		$package_description=$this->input->post('package_description');
		$package_description=str_replace('"','KSYDOU',$package_description);
		$package_description=str_replace("'",'KSYSING',$package_description);
		
		$package_item='';
				
		$post_item=$this->input->post('package_item');
		
		if($post_item)
		{
			
			foreach($post_item as $item_id)
			{
				$package_item.= $item_id.',';
			}
			
			$package_item = substr($package_item,'0',-1);
		}
		
			
			
			
			$data=array(			
			'package_name' => $this->input->post('package_name'),
			'package_short_description' => $package_short_description,
			'package_description' => $package_description,
			'package_price' => $this->input->post('package_price'),
                        'package_is_free' => $this->input->post('package_is_free'),
                        'package_points' => $this->input->post('package_points'),
                        'package_type' => $this->input->post('package_type'),
			'package_days' => $this->input->post('package_days'),
			'package_item' => $package_item,
			'package_date' => date('Y-m-d H:i:s'),
			'package_status' => $this->input->post('package_status')		
			);
			
			$this->db->where('package_id',$this->input->post('package_id'));
			$this->db->update('package',$data);
			
	}
	
	function get_package_detail($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('package')." where package_id='".$id."'");
		return $query->row();
	}
	
	
	
	function get_total_package_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('package'));		
		return $query->num_rows();		
	}
	
	function get_all_package($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('package')." order by package_id desc limit ".$limit." offset ".$offset);		
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	
	
	
	
	
	
	function delete_package($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('package')." where package_id='".$id."'");		
		
		if($query->num_rows()>0)
		{
			$this->db->delete('package',array('package_id' => $id));	
		}		
		
		return 0;
	
	}
	
}

?>