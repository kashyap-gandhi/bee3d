<?php

class Transaction_model extends CI_Model {
	
    function Transaction_model()
    {
       parent::__construct();	
    } 
	


	function get_one_transaction_detail($id)
	{	
		
		$this->db->select('*');
		$this->db->from('user_package upkg');
		$this->db->join('user us','upkg.user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->join('packages pkg','upkg.package_id=pkg.package_id');
		$this->db->where('upkg.user_package_id',$id);
		
		$query=$this->db->get();
	
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		return 0;
		
	}
	
	
	/////////=========reports=============	
	
	
	///**************daily*********//
	
	function get_daily_result($option)
	{
	
		$start_day = date('Y-m-d H:i:s');
		
		if($option == 'five') {
			$end_day = date('Y-m-d H:i:s',mktime(0,0,0,date('m'),date('d')-4,date('Y')));
		}
		
		if($option == 'ten') {
			$end_day = date('Y-m-d H:i:s',mktime(0,0,0,date('m'),date('d')-9,date('Y')));
		}
		
		if($option == 'fifteen') {
			$end_day = date('Y-m-d H:i:s',mktime(0,0,0,date('m'),date('d')-14,date('Y')));
		}
		
		if($option == 'thirty') {
			$end_day = date('Y-m-d H:i:s',mktime(0,0,0,date('m'),date('d')-29,date('Y')));
		}
		
		if($option == 'all') {
			$end_day = date('Y-m-d H:i:s');
		}
		
		$this->db->select('*');
		$this->db->from('user_package upkg');
		$this->db->join('user us','upkg.user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->join('packages pkg','upkg.package_id=pkg.package_id');
		
		
		if($end_day != '') {
			$this->db->where("upkg.package_purchase_date BETWEEN '$end_day' AND '$start_day'");  
		} else {
			//$this->db->where("Month(ts.transaction_date_time) = '".date('m')."'"); 
		}
		
		$this->db->order_by('upkg.user_package_id','desc');
		
		
		
		
		$query=$this->db->get();
	
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;

	}
	
	
	function total_daily_earning($date)
	{
		$date = explode('-',$date);
		
		$earning_price = '0.00';
		
		$this->db->select('SUM(upkg.pay_amount) as total_earn');
		$this->db->from('user_package upkg');
		$this->db->join('user us','upkg.user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->join('packages pkg','upkg.package_id=pkg.package_id');
		
		
		$this->db->where("Year(upkg.package_purchase_date) = '".$date['0']."'"); 
		
		if(array_key_exists('1',$date)){
			$this->db->where("Month(upkg.package_purchase_date) = '".$date['1']."'"); 
		}
		
		if(array_key_exists('2',$date)){
			$this->db->where("Day(upkg.package_purchase_date) = '".$date['2']."'"); 
		}
		
		
		
		
		$this->db->order_by('upkg.user_package_id','desc');
		$query=$this->db->get();
		
		
		
		if ($query->num_rows() > 0) {
			$row=$query->row();
			
			$earning_price= $row->total_earn;
		}
		

		
		
		return number_format($earning_price,'2','.','');
	}
	
	function total_earning($option){
	
		$earning_price = '0.00';
		$end_day ='';
		$month = '';
		$year = '';
		
		$start_day = date('Y-m-d H:i:s');
		if($option == 'five') {
			$end_day = date('Y-m-d H:i:s',mktime(0,0,0,date('m'),date('d')-4,date('Y')));
		}
		
		if($option == 'ten') {
			$end_day = date('Y-m-d H:i:s',mktime(0,0,0,date('m'),date('d')-9,date('Y')));
		}
		
		if($option == 'fifteen') {
			$end_day = date('Y-m-d H:i:s',mktime(0,0,0,date('m'),date('d')-14,date('Y')));
		}
		
		if($option == 'thirty') {
			$end_day = date('Y-m-d H:i:s',mktime(0,0,0,date('m'),date('d')-29,date('Y')));
		}
		
		if($option == 'all') {
			$end_day =date('Y-m-d H:i:s');
		}
		
		if($option == 'one') {$month = '-1'; }
		if($option == 'two') { $month = '-2'; }
		if($option == 'three') { $month = '-3'; }
		if($option == 'six') { $month = '-6'; }
		
		if($option == 'one_year') {$year = '-1'; }
		if($option == 'two_year') { $year = '-2'; }
		if($option == 'three_year') { $year = '-3'; }
		if($option == 'six_year') { $year = '-6'; }
		
		$this->db->select('SUM(upkg.pay_amount) as total_earn');
		$this->db->from('user_package upkg');
		$this->db->join('user us','upkg.user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->join('packages pkg','upkg.package_id=pkg.package_id');
		
		if($end_day != '') { $this->db->where("upkg.package_purchase_date BETWEEN '$end_day' AND '$start_day'"); }
		
		elseif($month != ''){
			$this->db->where("upkg.package_purchase_date BETWEEN DATE_ADD('".date('Y-m-d')."', INTERVAL '".$month."' MONTH ) AND '".date('Y-m-d')."'"); 
			
		} elseif($year != ''){  
			$this->db->where("upkg.package_purchase_date BETWEEN DATE_ADD('".date('Y-m-d')."', INTERVAL '".$year."' YEAR ) AND '".date('Y-m-d')."'"); 
		} else {
		
			if($option == 'current') {
				$this->db->where("Year(upkg.package_purchase_date) = '".date('Y')."'"); 
				$this->db->where("Month(upkg.package_purchase_date) = '".date('m')."'"); 
				
			} elseif($option == 'current_year') {
				$this->db->where("Year(upkg.package_purchase_date) = '".date('Y')."'"); 
				  
			} else {
				//$this->db->where("Month(ts.transaction_date_time) = '".date('m')."'"); 
			}
		}
		
		$query=$this->db->get();
		
		
		
		if ($query->num_rows() > 0) {
			$row=$query->row();
			
			$earning_price= $row->total_earn;
		}
		
		return number_format($earning_price,'2','.','');
	}
	
	
	
	///**************monthly*********//
	
	function get_monthly_result($option)
	{
		
		
		if($option == 'one') {$month = '-1'; }
		
		if($option == 'two') { $month = '-2'; }
		
		if($option == 'three') { $month = '-3'; }
		
		if($option == 'six') { $month = '-6'; }

		$this->db->select('*');
		$this->db->from('user_package upkg');
		$this->db->join('user us','upkg.user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->join('packages pkg','upkg.package_id=pkg.package_id');
		
		if($option == 'current') {
			$this->db->where("Year(upkg.package_purchase_date) = '".date('Y')."'"); 
			$this->db->where("Month(upkg.package_purchase_date) = '".date('m')."'");
			  
		} elseif($option == 'all') {
		
		} else {
			$this->db->where("upkg.package_purchase_date BETWEEN DATE_ADD('".date('Y-m-d')."', INTERVAL '".$month."' MONTH ) AND '".date('Y-m-d')."'"); 
		}
			
		$this->db->order_by('upkg.user_package_id','desc');
		
		$query=$this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;

	}
	
		///**************yearly*********//
	
	function get_yearly_result($option)
	{
		if($option == 'one_year') {$year = '-1'; }
		
		if($option == 'two_year') { $year = '-2'; }
		
		if($option == 'three_year') { $year = '-3'; }
		
		if($option == 'six_year') { $year = '-6'; }

		$this->db->select('*');
		$this->db->from('user_package upkg');
		$this->db->join('user us','upkg.user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->join('packages pkg','upkg.package_id=pkg.package_id');
		
		
		if($option == 'current_year') {
			$this->db->where("Year(upkg.package_purchase_date) = '".date('Y')."'");   
		} elseif($option == 'all') {
		
		} else {
			$this->db->where("upkg.package_purchase_date BETWEEN DATE_ADD('".date('Y-m-d')."', INTERVAL '".$year."' YEAR ) AND '".date('Y-m-d')."'"); 
		}
			
		$this->db->order_by('upkg.user_package_id','desc');
		
		$query=$this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;

	}

	
	/////////=========earning=============	
	
	
	
	
	function get_total_search_escrow_count($option,$keyword)
	{
	
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));

		$this->db->select('*');
		$this->db->from('user_package upkg');
		$this->db->join('user us','upkg.user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->join('packages pkg','upkg.package_id=pkg.package_id');
		
				
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
		
		
		
		
		$this->db->order_by('upkg.user_package_id','desc');
		$query=$this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->num_rows(); 
		}
		
		
	}
	
	function get_search_escrow_result($option,$keyword,$offset,$limit)
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));

		
		$this->db->select('*');
		$this->db->from('user_package upkg');
		$this->db->join('user us','upkg.user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->join('packages pkg','upkg.package_id=pkg.package_id');
		
		$this->db->like($option,$keyword);
			
		if(substr_count($keyword,' ')>=1)
		{
			$ex=explode(' ',$keyword);
			
			foreach($ex as $val)
			{
				$this->db->or_like($option,$val);
			}	
		}
		
		$this->db->order_by('upkg.user_package_id','desc');
		$this->db->limit($limit,$offset);
		
		
		
		$query=$this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
		/*** get total transaction 
	*  return number
	**/
	function get_total_transaction_count()
	{
		$this->db->select('*');
		$this->db->from('user_package upkg');
		$this->db->join('user us','upkg.user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->join('packages pkg','upkg.package_id=pkg.package_id');
		$this->db->order_by('upkg.user_package_id','desc');
		$query=$this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->num_rows(); 
		}
	}
	
	
	/*** get transaction details
	*  return multiple records array
	**/
	function get_transaction_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('user_package upkg');
		$this->db->join('user us','upkg.user_id=us.user_id','left');
		$this->db->join('user_profile up','us.user_id=up.user_id','left');
		$this->db->join('packages pkg','upkg.package_id=pkg.package_id');
		$this->db->order_by('upkg.user_package_id','desc');
		$this->db->limit($limit,$offset);
		$query=$this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	} 
	 
	 

	 
	/*** get wallet transaction details
	*  return single records array
	**/
	function worker_pay($worker_id,$task_id){

		$this->db->select('*');
		$this->db->from('worker wrk');
		$this->db->join('user us','wrk.user_id=us.user_id');
		$this->db->join('user_profile up','wrk.user_id=up.user_id');
		$this->db->join('wallet wl','us.user_id=wl.user_id','left');
		$this->db->where('wrk.worker_id',$worker_id);
		$this->db->where('wl.task_id',$task_id);
		$query=$this->db->get();
		//echo $this->db->last_query(); die();
		if($query->num_rows()>0)
		{		
			return $query->row();
		}
		
		return 0;
	}
	
	
	/*** get total refund transaction 
	*  return number
	**/
	function get_total_refund_count()
	{
		$this->db->select('*');
		$this->db->from('dispute dsp');
		$this->db->join('task tk','dsp.task_id=tk.task_id');
		$this->db->join('user us','tk.user_id = us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('wallet wl','us.user_id=wl.user_id','left');
		$this->db->join('transaction tr','wl.user_id=tr.user_id','left');
		$this->db->where('wl.task_id = tr.task_id');
		$this->db->where('tk.task_id = wl.task_id');
		$this->db->where('dsp.dispute_status !=','1');
		$this->db->where('wl.debit !=','');
		$query=$this->db->get();
		
		return $query->num_rows();
	}
	
	
	/*** get refund transaction details
	*  return multiple records array
	**/
	function get_refund_result($offset, $limit)
	{
		$this->db->select('*');
		$this->db->from('dispute dsp');
		$this->db->join('task tk','dsp.task_id=tk.task_id');
		$this->db->join('user us','tk.user_id = us.user_id');
		$this->db->join('user_profile up','us.user_id=up.user_id');
		$this->db->join('wallet wl','us.user_id=wl.user_id','left');
		$this->db->join('transaction tr','wl.user_id=tr.user_id','left');
		$this->db->where('wl.task_id = tr.task_id');
		$this->db->where('tk.task_id = wl.task_id');
		$this->db->where('dsp.dispute_status !=','1');
		$this->db->where('wl.debit !=','');
		
		$this->db->limit($limit,$offset);
		$query=$this->db->get();
		
		//echo $this->db->last_query(); //die();
		
		if($query->num_rows()>0)
		{		
			return $query->result();
		}
		
		return 0;
	}
	
	
	function get_total_search_refund_count($option,$keyword){
	
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));

		if($option=='taskername')
		{
		
		    $this->db->select('*');
			$this->db->from('transaction ts');
			$this->db->join('task tk','ts.task_id=tk.task_id');
			$this->db->join('worker wr','tk.task_worker_id=wr.worker_id','left');
			$this->db->join('user ur','wr.user_id=ur.user_id','left');
			$this->db->join('user_profile up','ur.user_id=up.user_id','left');
			
			$this->db->like('ur.full_name',$keyword);
		    $this->db->or_like('ur.first_name',$keyword);
		    $this->db->or_like('ur.last_name',$keyword);
		}
		
		if($option=='askername' || $option=='taskname')
		{
			$this->db->select('*');
			$this->db->from('transaction ts');
			$this->db->join('task tk','ts.task_id=tk.task_id');
		    $this->db->join('user ur','ts.user_id=ur.user_id','left');
		    $this->db->join('user_profile up','ur.user_id=up.user_id','left');
		
			if($option=='askername'){
			   $this->db->like('ur.full_name',$keyword);
			   $this->db->or_like('ur.first_name',$keyword);
			   $this->db->or_like('ur.last_name',$keyword);
			}
			
			if($option=='taskname')
			{
			   $this->db->like('tk.task_name',$keyword);
			}
		}
		
		
		$this->db->order_by('ts.transaction_id','desc');
		
		$query=$this->db->get();
		
		return $query->num_rows();
	}
	
	function get_search_refund_result($option,$keyword,$offset,$limit)
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));

		if($option=='taskername')
		{
		
		    $this->db->select('*');
			$this->db->from('transaction ts');
			$this->db->join('task tk','ts.task_id=tk.task_id');
			$this->db->join('worker wr','tk.task_worker_id=wr.worker_id','left');
			$this->db->join('user ur','wr.user_id=ur.user_id','left');
			$this->db->join('user_profile up','ur.user_id=up.user_id','left');
			
			$this->db->like('ur.full_name',$keyword);
		    $this->db->or_like('ur.first_name',$keyword);
		    $this->db->or_like('ur.last_name',$keyword);
		}
		
		if($option=='askername' || $option=='taskname')
		{
			$this->db->select('*');
			$this->db->from('transaction ts');
			$this->db->join('task tk','ts.task_id=tk.task_id');
		    $this->db->join('user ur','ts.user_id=ur.user_id','left');
		    $this->db->join('user_profile up','ur.user_id=up.user_id','left');
		
			if($option=='askername'){
			   $this->db->like('ur.full_name',$keyword);
			   $this->db->or_like('ur.first_name',$keyword);
			   $this->db->or_like('ur.last_name',$keyword);
			}
			
			if($option=='taskname')
			{
			   $this->db->like('tk.task_name',$keyword);
			}
		}

		$this->db->order_by('ts.transaction_id','desc');
		$this->db->limit($limit,$offset);
		$query=$this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
	function get_user_by_task($task_id)
	{
		$this->db->select('*');
		$this->db->from('task tk');
		$this->db->join('user ur','tk.user_id=ur.user_id');
		$this->db->join('user_profile up','ur.user_id=up.user_id','left');	
		$this->db->where('tk.task_id',$task_id);
		$query=$this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		return 0;
	}
	
	
	/*** get total worker pay
	*  return number
	**/
	function get_total_paying_count()
	{
		$this->db->select('*');
		$this->db->from('wallet wl');
		$this->db->join('task tk','wl.task_id=tk.task_id');
		$this->db->join('user ur','wl.user_id=ur.user_id','left');
		$this->db->join('user_profile up','ur.user_id=up.user_id','left');
		$this->db->where('wl.debit >',0);
		$this->db->where('wl.credit =',0);
		$this->db->where('wl.task_id !=',0);
		$this->db->order_by('wl.id','desc');
		$query=$this->db->get();
		
		return $query->num_rows(); 
	}
	
	
	/*** get transaction details
	*  return multiple records array
	**/
	function get_paying_result($offset, $limit)
	{	
		$this->db->select('*');
		$this->db->from('wallet wl');
		$this->db->join('task tk','wl.task_id=tk.task_id');
		$this->db->join('user ur','wl.user_id=ur.user_id','left');
		$this->db->join('user_profile up','ur.user_id=up.user_id','left');
		$this->db->where('wl.debit >',0);
		$this->db->where('wl.credit =',0);
		$this->db->where('wl.task_id !=',0);
		$this->db->order_by('wl.id','desc');		
		$this->db->limit($limit,$offset);
		$query=$this->db->get();
		
		
		
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	} 
	
	function get_total_search_paying_count($option,$keyword)
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));

		if($option=='taskername')
		{
		
		    $this->db->select('*');
			$this->db->from('wallet wl');
			$this->db->join('task tk','wl.task_id=tk.task_id');
			$this->db->join('worker wr','tk.task_worker_id=wr.worker_id','left');
			$this->db->join('user ur','wr.user_id=ur.user_id','left');
			$this->db->join('user_profile up','ur.user_id=up.user_id','left');
			
			$this->db->like('ur.full_name',$keyword);
		    $this->db->or_like('ur.first_name',$keyword);
		    $this->db->or_like('ur.last_name',$keyword);
		}
		
		if($option=='askername' || $option=='taskname')
		{
			$this->db->select('*');
			$this->db->from('wallet wl');
			$this->db->join('task tk','wl.task_id=tk.task_id');
		    $this->db->join('user ur','wl.user_id=ur.user_id','left');
		    $this->db->join('user_profile up','ur.user_id=up.user_id','left');
		
			if($option=='askername'){
			   $this->db->like('ur.full_name',$keyword);
			   $this->db->or_like('ur.first_name',$keyword);
			   $this->db->or_like('ur.last_name',$keyword);
			}
			
			if($option=='taskname')
			{
			   $this->db->like('tk.task_name',$keyword);
			}
		}
		
		$this->db->where('wl.debit >',0);
		$this->db->where('wl.credit =',0);
		$this->db->where('wl.task_id !=',0);
			
		$this->db->order_by('wl.id','desc');
		$query=$this->db->get();

		
		return $query->num_rows();
	}
	
	function get_search_paying_result($option,$keyword,$offset,$limit)
	{
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));

		if($option=='taskername')
		{
		
		    $this->db->select('*');
			$this->db->from('wallet wl');
			$this->db->join('task tk','wl.task_id=tk.task_id');
			$this->db->join('worker wr','tk.task_worker_id=wr.worker_id','left');
			$this->db->join('user ur','wr.user_id=ur.user_id','left');
			$this->db->join('user_profile up','ur.user_id=up.user_id','left');
			
			$this->db->like('ur.full_name',$keyword);
		    $this->db->or_like('ur.first_name',$keyword);
		    $this->db->or_like('ur.last_name',$keyword);
		}
		
		if($option=='askername' || $option=='taskname')
		{
			$this->db->select('*');
			$this->db->from('wallet wl');
			$this->db->join('task tk','wl.task_id=tk.task_id');
		    $this->db->join('user ur','wl.user_id=ur.user_id','left');
		    $this->db->join('user_profile up','ur.user_id=up.user_id','left');
		
			if($option=='askername'){
			   $this->db->like('ur.full_name',$keyword);
			   $this->db->or_like('ur.first_name',$keyword);
			   $this->db->or_like('ur.last_name',$keyword);
			}
			
			if($option=='taskname')
			{
			   $this->db->like('tk.task_name',$keyword);
			}
		}
		
		$this->db->where('wl.debit >',0);
		$this->db->where('wl.credit =',0);
		$this->db->where('wl.task_id !=',0);
			
		$this->db->order_by('wl.id','desc');
		$this->db->limit($limit,$offset);
		$query=$this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return 0;
	}
	
	
	
		
	
	
}
?>