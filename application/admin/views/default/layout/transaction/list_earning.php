<script type="text/javascript" language="javascript">
	function gomain(x)
	{
		if(x == 'all')
		{
			window.location.href= '<?php echo site_url('package/list_earning');?>';
		}
	}
	
	
	function getlimit(limit)
	{
		if(limit=='0'){
			return false;
		} else {
			window.location.href='<?php echo site_url('package/list_earning');?>/'+limit;
		}
	}	
	
	function getsearchlimit(limit)
	{
		if(limit=='0'){
			return false;
		} else {
			window.location.href='<?php echo site_url('package/list_earning');?>/'+limit+'/<?php echo $option.'/'.$keyword; ?>';
		}
	}
function chk_valid()
	{
		
		var keyword = document.getElementById('keyword').value;
		
		if(keyword=='')
		{
			alert('Please enter search keyword');	
			return false;
			
		}
		
		else
		{
			return true;			
		}
		
		
		
	}	
					
function setchecked(elemName,status){
	elem = document.getElementsByName(elemName);
	for(i=0;i<elem.length;i++){
		elem[i].checked=status;
	}
}

function setaction(elename, actionval, actionmsg, formname) {
	vchkcnt=0;
	elem = document.getElementsByName(elename);
	
	for(i=0;i<elem.length;i++){
		if(elem[i].checked) vchkcnt++;	
	}
	if(vchkcnt==0) {
		alert('Please select a record')
	} else {
		
		if(confirm(actionmsg))
		{
			document.getElementById('action').value=actionval;	
			document.getElementById(formname).submit();
		}		
		
	}
}
	
	
</script>

<div style=" margin-left:49px;"><br />
<?php  
	   $this->active = $this->uri->uri_string();
	   $this->strdd = explode("/",$this->active);
	   
	    if($this->strdd[1] == 'list_earning') { $class_all = 'themed'; } else { $class_all= 'white'; }
		
	   if($this->strdd[1] == 'daily_report') { $class_day = 'themed'; } else { $class_day= 'white'; }
	   if($this->strdd[1] == 'monthly_report') { $class_month = 'themed'; } else { $class_month= 'white'; }
	   if($this->strdd[1] == 'yearly_report') { $class_year = 'themed'; } else { $class_year= 'white'; }
	   
	   
	   $check_daily_report=get_rights('list_daily_report');
	    $check_monthly_report=get_rights('list_monthly_report');
		 $check_yearly_report=get_rights('list_yearly_report');
	   
	    echo anchor('package/list_earning','All',' title="All" class="button '.$class_all.'"').'&nbsp';
		
	   
	   if($check_daily_report==1) { 
	   echo anchor('package/daily_report','Daily Report',' title="Daily Report" class="button '.$class_day.'"').'&nbsp';
	   }
	   
	    if($check_monthly_report==1) { 
	   
	   echo anchor('package/monthly_report','Monthly Report',' title="Monthly Report" class="button '.$class_month.'"').'&nbsp';
	   
	   }
	   
	     if($check_yearly_report==1) { 
	   echo anchor('package/yearly_report','Yearly Report',' title="Yearly Report" class="button '.$class_year.'"');
	   }
?>
</div>


<div id="content">        
 
	<div class="clear"></div>
	<div class="column full">
	
	<div class="box">		
		<div class="box themed_box">
		<h2 class="box-header">Earning List</h2>
			
			<div class="box-content box-table">
			<table class="tablebox">
            
            <div id="topbar" style="border:#CCC solid 1px;">
                    <div style="float:left; width:135px;">
					   <strong>Show :</strong>
                            <?php if($search_type=='normal') { ?>
                            <select name="limit" id="limit" onchange="getlimit(this.value)" style="width:80px;">
                            <?php } if($search_type=='search') { ?>
                             <select name="limit" id="limit" onchange="getsearchlimit(this.value)" style="width:80px;">
                            <?php } ?>
                                <option value="0">Per Page</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="75">75</option>
                                <option value="100">100</option>
                            </select>
                      </div>
                    <div style="float:left; width: 770px;">
                     <form name="frm_search" id="frm_search" method="post" action="<?php echo site_url('package/list_earning'); ?>/<?php echo $limit; ?>" onSubmit="return chk_valid();">
                     <strong style="float:left; margin-bottom:10px; margin-top:6px;">&nbsp;&nbsp;&nbsp;Search By &nbsp;</strong>&nbsp;&nbsp;
                        <select name="option" id="option" style="width:100px; float:left;" onChange="gomain(this.value)">
                            <option value="all">All</option> 
                            <option value="us.first_name" <?php if($option=='us.first_name'){?> selected="selected"<?php }?>>First name</option>
							<option value="us.last_name" <?php if($option=='us.last_name'){?> selected="selected"<?php }?>>Last Name</option>
							<option value="pkg.package_name" <?php if($option=='pkg.package_name'){?> selected="selected"<?php }?>>Package Name</option>
                            
            <option value="upkg.pay_amount" <?php if($option=='upkg.pay_amount'){?> selected="selected"<?php }?>>Pay Amount</option>
            <option value="upkg.pay_transaction_id" <?php if($option=='upkg.pay_transaction_id'){?> selected="selected"<?php }?>>Transaction ID</option>
      <option value="upkg.subscr_id" <?php if($option=='upkg.subscr_id'){?> selected="selected"<?php }?>>Subscription ID</option>
      <option value="upkg.ipn_track_id" <?php if($option=='upkg.ipn_track_id'){?> selected="selected"<?php }?>>IPN Track ID</option>
                            
						</select>
					   
                       &nbsp;&nbsp; <input type="text" name="keyword" id="keyword" value="<?php echo $keyword;?>" class="textfield" style="float:left; display:block; margin-top:4px; margin-left:3px;"/> 
					          
                        <input type="submit" name="submit" id="submit" value="Search" class="button themed"/> 
                     </form>
                     </div>
                     
                     
                     <div style="float:right;">
            
				   <form name="frm_listearning" id="frm_listearning" action="<?php echo site_url('package/action_earning');?>" method="post">
          <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
          <input type="hidden" name="limit" id="limit" value="<?php echo $limit; ?>" />
          <input type="hidden" name="option" id="option" value="<?php echo $option; ?>" />
          <input type="hidden" name="keyword" id="keyword" value="<?php echo $keyword; ?>" />
          <input type="hidden" name="search_type" id="search_type" value="<?php echo $search_type; ?>" />
          
           <input type="hidden" name="action" id="action" />
				
               
               <a href="javascript:void(0)"  onclick="setaction('chk[]','delete', 'Are you sure, you want to delete selected Record(s)?', 'frm_listearning')"  class="button white" ><span class="icon_text cancel"></span>Delete</a>
            		
                 </div>
                 
                 
                 
                 
                     
					</div>
            
            
				<thead class="table-header">
					<tr> 
                        
                       <th class="first tc"><a href="javascript:void(0)" onClick="javascript:setchecked('chk[]',1)" style="color:#000;"><?php echo "Check All"; ?></a> |
           <a href="javascript:void(0)" onClick="javascript:setchecked('chk[]',0)" style="color:#000;"><?php echo "Clear All"; ?></a></th>
           				<th>User Name</th>                                 
                        <th>User Email</th>
                        <th>Pkg. Name</th>
                        <th>Pay Amount(<?php echo $site_setting->currency_symbol;?>)</th>   
                        <th>Purc. Date</th>  
                         <th>End Date</th>  
                        <th>IP</th>  
                        <th>Transaction ID</th>
                        <th>Subscr ID</th>
                        <th>IPN Track ID</th>                         
                        <th>Details</th>              
					</tr>
				</thead>
				
				<tbody class="openable-tbody">
                 <?php
                    if($result)
                    {
                        $i=0;
                        foreach($result as $row)
                        {
							//echo '<pre>'; print_r($row);
                            if($i%2=="0")
                            {
                                $cl = "odd";	
                            }else{	
                                $cl = "even";	
                            }
							
						
                  ?>
                  <tr class="<?php echo $cl; ?>">
                 <td style="width:140px;"><input type="checkbox" name="chk[]" id="chk" value="<?php echo $row->user_package_id;?>" /></td>
                         <td><?php echo $row->first_name.' '.$row->last_name; ?></td>
                          <td><?php echo $row->email; ?></td>
                           <td><?php echo $row->package_name; ?></td>
                            <td><?php echo $row->pay_amount; ?></td>
                            
                              <td><?php echo date($site_setting->date_time_format,strtotime($row->package_purchase_date)); ?></td>
                                <td><?php echo date($site_setting->date_time_format,strtotime($row->package_end_date)); ?></td>
                                
                                
                             <td><?php echo $row->host_ip; ?></td>
                             
                              <td><?php echo $row->pay_transaction_id; ?></td>
                               <td><?php echo $row->subscr_id; ?></td>               
	                        <td><?php echo $row->ipn_track_id; ?></td>
                      
                         <td><?php echo anchor('package/transaction_detail/'.$row->user_package_id,'View','class="button white" title="View Details" target="_blank"'); ?></td> 
                      
                        
                        
                        
                  </tr>
                  
                  <?php
                            $i++;
                        }
                    }
					 else {
								  ?>        
								  
								  <tr class="alter">
                                    <td colspan="20" align="center" valign="middle" height="30">No Transaction has been added yet.</td></tr>
								  
								  <?php } ?>
				
				</tbody>
			</table>
            
                 <ul class="pagination">
                		 <?php echo $page_link; ?>
            	</ul>
			</div>
		</div>
	</div>
</div>