
<div id="content" align="center">

	<div align="left" class="column half">
		<div class="box">	
            <h2 class="box-header">Transaction ID : <?php echo $row->pay_transaction_id; ?></h2> 
			<div class="box-content">
			
			 
            
			  <table class="tablebox">
                  <tbody class="openable-tbody">
                  
                     <tr><td colspan="2" align="left" valign="top">
                      
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      
                      <tr>
                      
                     	
                          
                          <td align="left" valign="top">
                      
                       <table border="0" cellpadding="0" cellspacing="0" width="100%">
                       <tr>
                          <td style="text-align:left; width:35%"><label class="form-label">Name</label></td>
                          <td style="text-align:left; width:65%">: <?php echo $row->first_name.' '.$row->last_name; ?></td>
                      </tr>
                      
                        <tr>
                          <td style="text-align:left; width:35%"><label class="form-label">Email</label></td>
                          <td style="text-align:left; width:65%">: <?php echo $row->email; ?></td>
                      </tr>
                      
                      <tr><td colspan="2"><hr/></td></tr>
                      
                      
                      
                      
                      
                        <tr>
                          <td style="text-align:left;"><label class="form-label">Pay Amount </label></td>
                          <td style="text-align:left;">: <?php echo $row->pay_amount;?></td>
                      </tr>
                      
                    
                      <tr>
                          <td style="text-align:left;"><label class="form-label">Host Ip </label></td>
                          <td style="text-align:left;">: <?php echo $row->host_ip;?></td>
                      </tr>
                      
                      
                      
                      
                       <tr><td colspan="2"><hr/></td></tr>
                     
                      
                     <tr>
                          <td style="text-align:left;"><label class="form-label">Payer ID </label></td>
                          <td style="text-align:left;">: <?php echo $row->payer_id;?></td>
                      </tr>
                      
                 
                      
                         <tr>
                          <td style="text-align:left;"><label class="form-label">Subscription ID </label></td>
                          <td style="text-align:left;">: <?php echo $row->subscr_id;?></td>
                      </tr>
                      
                      
                      <tr>
                          <td style="text-align:left;"><label class="form-label">IPN Track ID</label></td>
                          <td style="text-align:left;">: <?php echo $row->ipn_track_id;?> </td>
                      </tr>
                      
                    
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Receiver ID </label></td>
                          <td style="text-align:left;">: <?php echo $row->receiver_id;?></td>
                      </tr>
                      
                   
                      <tr>
                          <td style="text-align:left;"><label class="form-label">Payer Status </label></td>
                          <td style="text-align:left;">: <?php echo $row->payer_status; ?></td>
                      </tr>
                      
                      <tr>
                          <td style="text-align:left;"><label class="form-label">Payer Email ID</label></td>
                          <td style="text-align:left;">: <?php echo $row->payer_email;?></td>
                      </tr>
                      
                      
                      
                      <tr><td colspan="2"><hr/></td></tr>
                      
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Subscription Month </label></td>
                          <td style="text-align:left;">: <?php echo $row->recurring_month;?></td>
                      </tr>
                      
                      
                      
                      
                      <tr>
                          <td style="text-align:left;"><label class="form-label">Purchase Date </label></td>
                         <td>: <?php echo date($site_setting->date_time_format,strtotime($row->package_purchase_date)); ?></td>
                      </tr>
                      
                        <tr>
                          <td style="text-align:left;"><label class="form-label">End Date </label></td>
                         <td>: <?php echo date($site_setting->date_time_format,strtotime($row->package_end_date)); ?></td>
                      </tr>
                      
                      
                      <tr><td colspan="2"><hr/></td></tr>
                  
                  
                  
               
                  
                  
                  		<tr>
                          <td style="text-align:left;"><label class="form-label">Package Name </label></td>
                          <td style="text-align:left;">: <table border="0"> <?php echo $row->package_name; ?></table></td>
                      </tr> 
                      
                      
                      
                  
                
                     
                     
                      
                      
                       
                      
                      
                    
                      </table>
                     
                      
                    	</td></tr></table></td></tr></tbody></table>
                      
                 
			  	    
             </div>
              


			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>