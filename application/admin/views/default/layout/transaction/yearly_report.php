<script type="text/javascript">
  function show_status(val)
	{	
		document.forms["frm_daily_search"].submit();
	}   
</script>
<div style=" margin-left:49px;"><br />
<?php  
	   $this->active = $this->uri->uri_string();
	   $this->strdd = explode("/",$this->active);
	   
	    if($this->strdd[1] == 'list_earning') { $class_all = 'themed'; } else { $class_all= 'white'; }
		if($this->strdd[1] == 'daily_report') { $class_day = 'themed'; } else { $class_day= 'white'; }
	   if($this->strdd[1] == 'monthly_report') { $class_month = 'themed'; } else { $class_month= 'white'; }
	   if($this->strdd[1] == 'yearly_report') { $class_year = 'themed'; } else { $class_year= 'white'; }
	   
	   
	   $check_daily_report=get_rights('list_daily_report');
	    $check_monthly_report=get_rights('list_monthly_report');
		 $check_yearly_report=get_rights('list_yearly_report');
	   
	   
	      echo anchor('package/list_earning','All',' title="All" class="button '.$class_all.'"').'&nbsp';
		  
	   if($check_daily_report==1) { 
	   echo anchor('package/daily_report','Daily Report',' title="Daily Report" class="button '.$class_day.'"').'&nbsp';
	   }
	   
	    if($check_monthly_report==1) { 
	   
	   echo anchor('package/monthly_report','Monthly Report',' title="Monthly Report" class="button '.$class_month.'"').'&nbsp';
	   
	   }
	   
	     if($check_yearly_report==1) { 
	   echo anchor('package/yearly_report','Yearly Report',' title="Yearly Report" class="button '.$class_year.'"');
	   }
?>
</div>
<div id="content">        
  
	<div class="clear"></div>
	<div class="column full">
	
	<div class="box">		
		<div class="box themed_box">
      
		<h2 class="box-header">Yearly Earning Report <span style="float:right; margin-right:10px;">Total : <?php echo $site_setting->currency_symbol.$this->package_model->total_earning($option);?></span></h2>
			
			<div class="box-content box-table">
			<table class="tablebox">
            
               <div id="topbar" style="border:#CCC solid 1px;">

                     <form name="frm_daily_search" id="frm_daily_search" method="post" action="<?php echo site_url('package/yearly_report'); ?>">
                     <strong style="float:left; margin-bottom:10px; margin-top:4px;">&nbsp;&nbsp;&nbsp;Search By &nbsp;</strong>&nbsp;&nbsp;
                     
                       <select name="option" id="option" style="width:110px; float:left;" onChange="javascript:show_status(this.value);">
                            <option value="current_year" <?php if($option=='current_year'){?> selected="selected"<?php }?>>Current Year</option> 
                            <option value="one_year" <?php if($option=='one_year'){?> selected="selected"<?php }?>>Last 1 Year</option>
							<option value="two_year" <?php if($option=='two_year'){?> selected="selected"<?php }?>>Last 2 Years</option>
                            <option value="three_year" <?php if($option=='three_year'){?> selected="selected"<?php }?>>Last 3 Years</option>
                             <option value="six_year" <?php if($option=='six_year'){?> selected="selected"<?php }?>>Last 6 Years</option>
							<option value="all" <?php if($option=='all'){?> selected="selected"<?php }?>>All</option>
                       </select> 
                     </form>
					 </div> 

                
		
      
				<thead class="table-header">
					<tr> 
                        <th class="first tc">User Name</th>                                 
                        <th>User Email</th>
                        <th>Pkg. Name</th>
                        <th>Pay Amount(<?php echo $site_setting->currency_symbol;?>)</th>   
                        <th>Purc. Date</th>  
                         <th>End Date</th>  
                        <th>IP</th>  
                        <th>Transaction ID</th>
                        <th>Subscr ID</th>
                        <th>IPN Track ID</th>                         
                        <th>Details</th>  
					</tr>
				</thead>
				
				<tbody class="openable-tbody">
                
                 <?php 
                    if($result)
                    {	
						$year_showdate =''; $year_counter = 0; $year_showhedder=''; 
						$month_showdate =''; $month_counter = 0; $month_showhedder=''; 
						$showdate=''; $counter = 0; $showhedder=''; 
                        $i=0;
                        foreach($result as $row)
                        {
							//echo '<pre>'; print_r($row);
                            if($i%2=="0")
                            {
                                $cl = "odd";	
                            }else{	
                                $cl = "even";	
                            }
							
							
							
							$earning_price= '0.00';
							$worker_pay = '0.00';
							
							
						
							//==================== Year Header Display  Start================//
							
								if($year_showdate == '') {
								
									$total_daily_earning = $this->package_model->total_daily_earning(date('Y',strtotime($row->package_purchase_date)));
									
									$year_showdate = date('Y',strtotime($row->package_purchase_date));
									echo $year_showhedder = '<tr><th colspan="11" class="cat-header" style="background:#292929; border: 1px solid #292929; text-align:left;  padding-left:20px; ">'.date('Y',strtotime($row->package_purchase_date)).'<span style="float:right; margin-right:35px;">Total : '.$site_setting->currency_symbol.$total_daily_earning.'</span></th></tr>';	
								}
	
								
								if($year_showdate != date('Y',strtotime($row->package_purchase_date))){
									
									$total_daily_earning = $this->package_model->total_daily_earning(date('Y-m',strtotime($row->package_purchase_date)));
	
									$year_showdate = date('Y',strtotime($row->package_purchase_date));
									$year_showhedder = '<tr><th colspan="11" class="cat-header"  style="background:#292929; border: 1px solid #292929;text-align:left; padding-left:20px; ">'.date('Y',strtotime($row->package_purchase_date)).'<span style="float:right; margin-right:35px;">Total : '.$site_setting->currency_symbol.$total_daily_earning.'</span></th></tr>';	
									
									if($year_counter == 0) { $year_counter = 1;}
									
								} else { 
									$year_counter = 0; 
								}	
							
								if($year_counter == 1){ 
									echo $year_showhedder;								
								}
							
							//==================== Year Header Display  end================//
							
							
							//==================== Month Header Display  Start================//
							if($month_showdate == '') {
							
								$total_daily_earning = $this->package_model->total_daily_earning(date('Y-m',strtotime($row->package_purchase_date)));
								
								$month_showdate = date('Y-m',strtotime($row->package_purchase_date));
								echo $month_showhedder = '<tr><th colspan="11" class="cat-header" style="text-align:left; padding-left:20px; ">'.date('M, Y',strtotime($row->package_purchase_date)).'<span style="float:right; margin-right:35px;">Total : '.$site_setting->currency_symbol.$total_daily_earning.'</span></th></tr>';	
							}

							
							if($month_showdate != date('Y-m',strtotime($row->package_purchase_date))){
								
								$total_daily_earning = $this->package_model->total_daily_earning(date('Y-m',strtotime($row->package_purchase_date)));

								$month_showdate = date('Y-m',strtotime($row->package_purchase_date));
								$month_showhedder = '<tr><th colspan="11" class="cat-header"  style="text-align:left; padding-left:20px; ">'.date('M, Y',strtotime($row->package_purchase_date)).'<span style="float:right; margin-right:35px;">Total : '.$site_setting->currency_symbol.$total_daily_earning.'</span></th></tr>';	
								
								if($month_counter == 0) { $month_counter = 1;}
								
							} else { 
								$month_counter = 0; 
							}	
						
							if($month_counter == 1){ 
								echo $month_showhedder;	
							}
						//==================== Month Header Display  end================//
						
							
						//==================== Day Header Display  Start================//
							if($showdate == '') {
							
								$total_daily_earning = $this->package_model->total_daily_earning(date('Y-m-d',strtotime($row->package_purchase_date)));
								
								$showdate = date('Y-m-d',strtotime($row->package_purchase_date));
								echo $showhedder = '<tr><th colspan="11" class="cat-header"  style="background:#ececec; border: 1px solid #d8d8d8; text-align:left; color:#0669AF;padding-left:20px; ">'.date($site_setting->date_format,strtotime(date('Y-m-d',strtotime($row->package_purchase_date)))).'<span style="float:right; margin-right:35px;">Total : '.$site_setting->currency_symbol.$total_daily_earning.'</span></th></tr>';	
							}

							
							if($showdate != date('Y-m-d',strtotime($row->package_purchase_date))){
								
								$total_daily_earning = $this->package_model->total_daily_earning(date('Y-m-d',strtotime($row->package_purchase_date)));

								$showdate = date('Y-m-d',strtotime($row->package_purchase_date));
								$showhedder = '<tr><th colspan="11" class="cat-header"  style="background:#ececec; border: 1px solid #d8d8d8; text-align:left; color:#0669AF;padding-left:20px;">'.date($site_setting->date_format,strtotime(date('Y-m-d',strtotime($row->package_purchase_date)))).'<span style="float:right; margin-right:35px;">Total : '.$site_setting->currency_symbol.$total_daily_earning.'</span></th></tr>';	
								if($counter == 0) { $counter = 1;}
							} else { 
								$counter = 0; 
							}
							
							if($counter == 1){ 
								echo $showhedder;
							}
						//==================== Day Header Display  End================//
						?>

                 	 <tr class="<?php echo $cl; ?>">
                  	 <td><?php echo $row->first_name.' '.$row->last_name; ?></td>
                          <td><?php echo $row->email; ?></td>
                           <td><?php echo $row->package_name; ?></td>
                            <td><?php echo $row->pay_amount; ?></td>
                            
                              <td><?php echo date($site_setting->date_time_format,strtotime($row->package_purchase_date)); ?></td>
                                <td><?php echo date($site_setting->date_time_format,strtotime($row->package_end_date)); ?></td>
                                
                                
                             <td><?php echo $row->host_ip; ?></td>
                             
                              <td><?php echo $row->pay_transaction_id; ?></td>
                               <td><?php echo $row->subscr_id; ?></td>               
	                        <td><?php echo $row->ipn_track_id; ?></td>
                      
                         <td><?php echo anchor('package/transaction_detail/'.$row->user_package_id,'View','class="button white" title="View Details" target="_blank"'); ?></td> 
                      
               
                  </tr>   
                  <?php
                            $i++;
                        }
                    }
					 else {
								  ?>        
								  
								  <tr class="alter">
                                    <td colspan="15" align="center" valign="middle" height="30">No Daily Report has been added yet.</td></tr>
								  
								  <?php } ?>
				
				</tbody>
			</table>
            
           
			</div>
		</div>
	</div>
</div>