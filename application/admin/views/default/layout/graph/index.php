<div id="content">
	
    <?php $site_setting=site_setting(); ?>
    	
<!-- 1. Add these JavaScript inclusions in the head of your page -->

<script type="text/javascript" src="<?Php echo front_base_url().getThemeName(); ?>/js/jquery.min.js"></script>

<script type="text/javascript" src="<?Php echo upload_url(); ?>highcharts/highcharts.js"></script>


<!-- 1b) Optional: the exporting module -->
<script type="text/javascript" src="<?Php echo upload_url(); ?>highcharts/modules/exporting.js"></script>


		<script type="text/javascript">
$(function () {
    var chart, chart1, chart2, chart3, chart4, chart5;
	
			/*exporting: {
				enabled: true
			  },
		*/
		
	
	
    $(document).ready(function() {
        
		
		
		chart = new Highcharts.Chart({
            chart: {
                renderTo: 'weeklyimage',
                type: 'line',
                marginRight: 0,
                marginBottom: 25
            },
            title: {
                text: '',
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                categories: ['Mon', 'Tue', 'Wen', 'Thur', 'Fri', 'Sat', 'Sun']
            },
            yAxis: {
                title: {
                    text: 'Total'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': '+this.y;
                }
            },
			
			
			
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'top',
                x: 10,
                y: 0,
                borderWidth: 0
            },
            series: [{
                name: 'Images',
                data: [<?php if($weekly_image) {  foreach($weekly_image as $date => $total) { echo $total.','; } } ?>]
            }]
        });
		
		
		
		chart1 = new Highcharts.Chart({
            chart: {
                renderTo: 'monthlyimage',
                type: 'line',
                marginRight: 0,
                marginBottom: 25
            },
            title: {
                text: '',
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Total'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                         this.x +': '+this.y;
                }
            },
			
			
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'top',
                x: 10,
                y: 0,
                borderWidth: 0
            },
            series: [{
                name: 'Images',
                data: [<?php if($monthly_image) {  foreach($monthly_image as $date => $total) { echo $total.','; } } ?>]
            }]
        });
		
		
		
		
		
		
		chart2 = new Highcharts.Chart({
            chart: {
                renderTo: 'yearlyimage',
                type: 'line',
                marginRight: 0,
                marginBottom: 25
            },
            title: {
                text: '',
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                categories: [<?php for($i=$year_first;$i<=$year_last;$i++) { echo "'".$i."',"; } ?>]
            },
            yAxis: {
                title: {
                    text: 'Total'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': '+this.y;
                }
            },
			
			
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'top',
                x: 10,
                y: 0,
                borderWidth: 0
            },
            series: [{
                name: 'Images',
                data: [<?php if($yearly_image) {  foreach($yearly_image as $date => $total) { echo $total.','; } } ?>]
            }]
        });
		
		
		


		
		chart3 = new Highcharts.Chart({
            chart: {
                renderTo: 'weeklyimagebar',
                type: 'column',
                marginRight: 0,
                marginBottom: 25
            },
            title: {
                text: '',
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                categories: ['Mon', 'Tue', 'Wen', 'Thur', 'Fri', 'Sat', 'Sun']
            },
            yAxis: {
				 min: 0,
                title: {
                    text: 'Total'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return this.x +': ' +this.y;
                }
            },
			
			
			
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'top',
                x: 10,
                y: 0,
                borderWidth: 0,
				floating: true,
                shadow: true
            },
			
			 plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
			
			
            series: [{
                name: 'Images',
                data: [<?php if($weekly_image) {  foreach($weekly_image as $date => $total) { echo $total.','; } } ?>]
            }]
        });
		
		
		
		chart4 = new Highcharts.Chart({
            chart: {
                renderTo: 'monthlyimagebar',
                type: 'column',
                marginRight: 0,
                marginBottom: 25
            },
            title: {
                text: '',
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
				 min: 0,
                title: {
                    text: 'Total'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return this.x +': ' +this.y;
                }
            },
			
			
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'top',
                x: 10,
                y: 0,
                 borderWidth: 0,
				floating: true,
                shadow: true
            },
			
			
			plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
			
			
            series: [{
                name: 'Images',
                data: [<?php if($monthly_image) {  foreach($monthly_image as $date => $total) { echo $total.','; } } ?>]
            }]
        });
		
		
		
		
		
		
		chart5 = new Highcharts.Chart({
            chart: {
                renderTo: 'yearlyimagebar',
                type: 'column',
                marginRight: 0,
                marginBottom: 25
            },
            title: {
                text: '',
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                categories: [<?php for($i=$year_first;$i<=$year_last;$i++) { echo "'".$i."',"; } ?>]
            },
            yAxis: {
			
				 min: 0,
                title: {
                    text: 'Total'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return this.x +': ' +this.y;
                }
            },
			
			
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'top',
                x: 10,
                y: 0,
                 borderWidth: 0,
				floating: true,
                shadow: true
            },
			
			plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
			
            series: [{
                name: 'Images',
                data: [<?php if($yearly_image) {  foreach($yearly_image as $date => $total) { echo $total.','; } } ?>]
            }]
        });
		
		
		
		
		

		
    });
    
});
		</script>
        

    
    
		<div class="column half fl">
		
			<div class="box">
			<h2 class="box-header">Weekly Average Images(Line)</h2>
			<div class="box-content">
				
                
                <div id="weeklyimage" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                
			</div>
			</div>
			
		</div>
		
		
        
        <div class="column half fr">
		
			<div class="box">
			<h2 class="box-header">Weekly Average Images(Bar)</h2>
			<div class="box-content">
				
                
                <div id="weeklyimagebar" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                
			</div>
			</div>
			
		</div>
        
        
		
		
       <div class="column half fl">
		
			<div class="box">
			<h2 class="box-header">Monthly Average Images(Line)</h2>
			<div class="box-content">
				
                  <div id="monthlyimage" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                
			</div>
			</div>
			
		</div>
       
       
       <div class="column half fr">
		
			<div class="box">
			<h2 class="box-header">Monthly Average Images(Bar)</h2>
			<div class="box-content">
				
                  <div id="monthlyimagebar" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                
			</div>
			</div>
			
		</div> 
        
        
        
        
        
        <div class="column half fl">
		
			<div class="box">
			<h2 class="box-header">Yearly Average Images(Line)</h2>
			<div class="box-content">
				
                  <div id="yearlyimage" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                
			</div>
			</div>
			
		</div>
        
        
        
        <div class="column half fr">
		
			<div class="box">
			<h2 class="box-header">Yearly Average Images(Bar)</h2>
			<div class="box-content">
				
                  <div id="yearlyimagebar" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                
			</div>
			</div>
			
		</div>
        
        
        
        
        
        
        
	<div class="clear"></div>
	</div>