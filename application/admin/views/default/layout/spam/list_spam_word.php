<script type="text/javascript" language="javascript">
	function delete_rec(id,offset)
	{
		var ans = confirm("Are you sure to delete this word?");
		if(ans)
		{
			location.href = "<?php echo site_url('spam_setting/delete_word'); ?>/"+id+"/"+offset;
		}else{
			return false;
		}
	}
	
	
	
	function getsearchlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			
			window.location.href='<?php echo site_url('admin/search_list_admin');?>/'+limit+'/<?php echo $option.'/'.$keyword; ?>';
		}
	
	}
	
	function gomain(x)
	{
		
		if(x == 'all')
		{
			window.location.href= '<?php echo site_url('admin/list_admin');?>';
		}
		if(x == 'superadmin')
		{
			
			window.location.href= '<?php echo site_url('admin/list_admin_superadmin');?>';
		}
		if(x == 'admin')
		{
			
			window.location.href= '<?php echo site_url('admin/list_admin_admin');?>';
		}
		
	}
</script>
<div id="content">  
	<?php if($msg != ""){
            if($msg == "insert"){ $error = 'New Record has been added Successfully.';}
            if($msg == "update"){ $error = 'Record has been updated Successfully.';}
            if($msg == "delete"){ $error = 'Record has been deleted Successfully.';}
			if($msg == "rights") {  $error = 'Rights has been updated Successfully.';}
    ?>
        <div class="column full">
            <span class="message information"><strong><?php echo $error;?></strong></span>
        </div>
    <?php } ?>
	<div class="clear"></div>
	<div class="column full">
	
	<div class="box">		
		<div class="box themed_box">
		<h2 class="box-header">Spam Words </h2>
			
			<div class="box-content box-table">
			<table class="tablebox" id="tabledata">  
				<div id="topbar" style="border:#CCC solid 1px;">
                    
                    
                    
                    
                    <div style="float:right;"><?php echo anchor('spam_setting/add_word','<span class="icon_text addnew"></span>Add New', 'class="button white" id="addadmin" style="margin:0px;"'); ?>	</div>
                    
                 
					   
							<script>
                                //jQuery("#addadmin").fancybox();
                            </script>
                    
                </div>
          		<thead class="table-header">
					<tr> 
                        <th class="first tc">Spam Word</th>
                       
                        <th>Action</th>                                  
                         
                        
                    	
                                                        
                    </tr>
                </thead>
                
                <tbody class="openable-tbody">
				<?php
                    if($result)
                    {
                        $i=0;
                        foreach($result as $row)
                        {
                            if($i%2=="0")
                            {
                                $cl = "odd";	
                            }else{	
                                $cl = "even";	
                            }
                  ?>
					<tr class="<?php echo $cl; ?>">
                   		<td><?php echo $row->spam_word; ?></td>
                        
                    
                       	
						<td>
                        	
							
							<?php echo anchor('spam_setting/edit_spam_word/'.$row->spam_word_id.'/'.$offset,'<span class="icon_single edit"></span>','class="button white" id="admin_'.$row->spam_word_id.'" title="Edit Board"'); ?>
					   
						
                        
					   		<a href="#" onClick="delete_rec('<?php echo $row->spam_word_id; ?>','<?php echo $offset; ?>')" class="button white"><span class="icon_single cancel"></span></a>
                       </td>
                    </tr>
					<?php
							$i++;
							}
						} else { ?>
						<tr class="alter"><td colspan="15" align="center" valign="middle" height="30">No Word has been added yet</td></tr>
					<?php 	}
					?> 
                  	</tr>
                </tbody>
                </table>
                <ul class="pagination">
					<?php echo $page_link; ?>
                </ul>

           </div>
       </div>
    </div>
</div>