
<div id="content" align="center">

	<div align="left" class="column half">
		<div class="box">	
            <h2 class="box-header"><?php echo anchor(front_base_url().'user/'.$result->profile_name,ucfirst($result->first_name).' '.ucfirst(substr($result->last_name,0,1)),' style="color:#004C7A;" target="_blank"'); ?></h2> 
			<div class="box-content">
			
			 
            
			  <table class="tablebox">
                  <tbody class="openable-tbody">
                  
                     <tr><td colspan="2" align="left" valign="top">
                      
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      
                      <tr>
                      
                     	 <td align="left" valign="top" width="150">
                        <?php if($result->profile_image!='') {?>
                                 <img src="<?php echo upload_url();?>upload/user/<?php echo $result->profile_image;?>" style="border-radius:5px; width:120px; height:120px;"/>
                          <?php } else {?>
                                  <img src="<?php echo upload_url();?>upload/no_image.png" style="border-radius:5px; width:120px; height:120px;"/>
                          <?php }?>
                          
                          </td>
                          
                          <td align="left" valign="top">
                      
                       <table border="0" cellpadding="0" cellspacing="0" width="70%">
                       <tr>
                          <td style="text-align:left; width:35%"><label class="form-label">User Name </label></td>
                          <td style="text-align:left; width:65%">: <?php echo anchor(front_base_url().'user/'.$result->profile_name,ucfirst($result->profile_name),' style="color:#004C7A;" target="_blank"'); ?></td>
                      </tr>
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Firstname </label></td>
                          <td style="text-align:left;">: <?php echo $result->first_name;?></td>
                      </tr>
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Lastname </label></td>
                          <td style="text-align:left;">: <?php echo $result->last_name;?></td>
                      </tr>
                      
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Email </label></td>
                          <td style="text-align:left;">: <?php echo $result->email;?></td>
                      </tr>
                      
                     
                      
                      </table>
                     
                      
                    	  </td>
                      
                      </tr>
                      </table>
                                             
                      </td></tr>
                      
                       <tr><td colspan="2"><hr/></td></tr>
                       
                       <tr>
                          <td style="text-align:left; width:30%;"><label class="form-label">Mobile Number </label></td>
                          <td style="text-align:left;width:70%;">: <?php echo $result->mobile_no;?></td>
                      </tr>
                      
                  	  <tr>
                          <td style="text-align:left;"><label class="form-label">About User </label></td>
                          <td style="text-align:left;">: <?php echo $result->about_user;?></td> 
                      </tr>
                      
                      <tr><td colspan="2"><hr/></td></tr>
                   
                      
                      
                      
                      
                  </tbody>
			  	 </table>   
             </div>
              


			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>