<script type="text/javascript" language="javascript">
	function delete_rec(id,offset)
	{
		var ans = confirm("Are you sure to delete this report type?");
		if(ans)
		{
			location.href = "<?php echo site_url('spam_setting/delete_pin'); ?>/"+id+"/"+offset;
		}else{
			return false;
		}
	}
	
	
	
	function getsearchlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			
			window.location.href='<?php echo site_url('admin/search_list_admin');?>/'+limit+'/<?php echo $option.'/'.$keyword; ?>';
		}
	
	}
	
	function gomain(x)
	{
		
		if(x == 'all')
		{
			window.location.href= '<?php echo site_url('admin/list_admin');?>';
		}
		if(x == 'superadmin')
		{
			
			window.location.href= '<?php echo site_url('admin/list_admin_superadmin');?>';
		}
		if(x == 'admin')
		{
			
			window.location.href= '<?php echo site_url('admin/list_admin_admin');?>';
		}
		
	}
</script>
<div id="content">  
	<?php if($msg != ""){
            if($msg == "insert"){ $error = 'New Record has been added Successfully.';}
            if($msg == "update"){ $error = 'Record has been updated Successfully.';}
            if($msg == "delete"){ $error = 'Record has been deleted Successfully.';}
			if($msg == "rights") {  $error = 'Rights has been updated Successfully.';}
    ?>
        <div class="column full">
            <span class="message information"><strong><?php echo $error;?></strong></span>
        </div>
    <?php } ?>
	<div class="clear"></div>
	<div class="column full">
	
	<div class="box">		
		<div class="box themed_box">
		<h2 class="box-header">Pins </h2>
			
			<div class="box-content box-table">
			<table class="tablebox" id="tabledata">  
				
          		<thead class="table-header">
					<tr> 
                        <th class="first tc">Pin Description</th>
                        <th>Pin Link</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Pin Date</th>   
                        <th>Pin ip</th>                                  
                        <th>Status</th>
                        <th>View Detail</th>      
                        <th>Make Spam Pin</th>  
                        <th>Action</th>        
                        
                    	
                                                        
                    </tr>
                </thead>
                
                <tbody class="openable-tbody">
				<?php
                    if($result)
                    {
                        $i=0;
                        foreach($result as $row)
                        {
                            if($i%2=="0")
                            {
                                $cl = "odd";	
                            }else{	
                                $cl = "even";	
                            }
                  ?>
					<tr class="<?php echo $cl; ?>">
                   		<td><?php echo $row->pin_description; ?></td>
                        <td></td>
                         <td><a href="#"><?php echo $row->profile_name;?></a></td>
                        <td><?php echo $row->email;?></td>
                        <td><?php echo $row->pin_date;?></td>
                        <td><?php echo $row->pin_ip;?></td>
                        <td><?php if($row->pin_status==1) { echo "Active"; } if($row->pin_status==0) { echo "Inactive"; } ?></td>
                        
                        
                        <td><?php echo anchor('spam_setting/pin_detail/'.$row->pin_id,'View Detail','class="button white"'); ?></td>
                        <td><?php echo anchor('spam_setting/comment_detail/'.$row->pin_id,'Button','class="button white"'); ?></td>
                        <td><?php echo anchor('spam_setting/edit_pin_type/'.$row->pin_id.'/'.$offset,'<span class="icon_single edit"></span>','class="button white" id="admin_'.$row->pin_id.'" title="Edit Board"'); ?>
					   
						
                        
					   		<a href="#" onClick="delete_rec('<?php echo $row->pin_id; ?>','<?php echo $offset; ?>')" class="button white"><span class="icon_single cancel"></span></a></td>
                        
                    
                       
                        	
							
							
                    </tr>
					<?php
							$i++;
							}
						} else { ?>
						<tr class="alter"><td colspan="15" align="center" valign="middle" height="30">No Board has been added yet</td></tr>
					<?php 	}
					?> 
                  	</tr>
                </tbody>
                </table>
                <ul class="pagination">
					<?php echo $page_link; ?>
                </ul>

           </div>
       </div>
    </div>
</div>