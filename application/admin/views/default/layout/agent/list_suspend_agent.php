<script type="text/javascript" language="javascript">

	
	function getlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			window.location.href='<?php echo site_url('agent/list_suspend_agent');?>/'+limit;
		}
	
	}	
	
	function getsearchlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			
			window.location.href='<?php echo site_url('agent/search_suspend_agent');?>/'+limit+'/<?php echo $option.'/'.$keyword; ?>';
		}
	
	}
	
	function gomain(x)
	{
		
		if(x == 'all')
		{
			window.location.href= '<?php echo site_url('agent/list_suspend_agent');?>';
		}
	}
	
	function setchecked(elemName,status){
		elem = document.getElementsByName(elemName);
		for(i=0;i<elem.length;i++){
			elem[i].checked=status;
		}
	}


	function setaction(elename, actionval, actionmsg, formname) 
	{
		vchkcnt=0;
		elem = document.getElementsByName(elename);
		
		for(i=0;i<elem.length;i++){
			if(elem[i].checked) vchkcnt++;	
		}
		if(vchkcnt==0) {
			alert('Please select a record')
		} else {
			
			if(confirm(actionmsg))
			{
				document.getElementById('action').value=actionval;	
				document.getElementById(formname).submit();
			}		
			
		}
	}


	function chk_valid()
	{
		
		var keyword = document.getElementById('keyword').value;
		
		if(keyword=='')
		{
			alert('Please enter search keyword');	
			return false;
			
		}
		
		else
		{
			return true;			
		}
		
		
		
	}
	$(document).ready(function(){	
	var intRegex = /^\d+$/;
		$(".suspendsubmit").click(function(){
		
			var user_id=$(this).attr('id').replace('susid_','');
			
			var noofday=$("#no_of_day_"+user_id).val();
			var ispermanent=$("input[name=is_permanent_"+user_id+"]:checked").val();
			var suspendreason=$("#suspend_reason_"+user_id).val();
			
			var offs=$("#offset_"+user_id).val();
			var limts=$("#limit_"+user_id).val();
			var srchtype=$("#search_type_"+user_id).val();
			var opton=$("#option_"+user_id).val();
			var keywrd=$("#keyword_"+user_id).val();
			
		
			
			if(noofday=='' || noofday==0 || noofday==undefined) { alert('No of days required.');  return false; 	}
			
				if(!intRegex.test(noofday))
				{
					alert('No of days is contains only integer.');  return false;
				}
			
			if(ispermanent=='' || ispermanent==0 || ispermanent==undefined) { alert('Suspend Type is required.');  return false; 	}
			if(suspendreason=='' || suspendreason==0 || suspendreason==undefined) { alert('Suspend reason is required.');  return false; 	}
				
				
			
			
			var res = $.ajax({						
				type: 'POST',
				url: '<?php echo site_url('agent/edit_suspend/suspend');?>/',
				data: {no_of_day:noofday,is_permanent:ispermanent,suspend_reason:suspendreason,user_id:user_id,offset:offs,limit:limts,search_type:srchtype,option:opton,keyword:keywrd},
				dataType: 'html', 
				cache: false,
				async: false                     
			}).responseText;		
			
				
				window.location.href=res;
			
		});
		
		
		
	});
	
</script>

<div id="content">        
	<?php $error='';
		
		 if($msg != ""){
            if($msg == "insert"){ $error = 'New Record has been added Successfully.';}
            if($msg == "update"){ $error = 'Record has been updated Successfully.';}
            if($msg == "delete"){ $error = 'Agent has been deleted Successfully.';}
			if($msg == "edit"){ $error = 'Agent status has been changed Successfully.';}
			if($msg == "active"){ $error = 'Agent has been activated Successfully.';}
			if($msg == "inactive"){ $error = 'Agent has been inactivated Successfully.';}
			if($msg == "suspend"){ $error = 'Agent has been suspended Successfully.';}
			if($msg == "passwordchange"){ $error = 'Agent password has been changed Successfully.';}
			
    ?>
        <div class="column full">
            <span class="message information"><strong><?php echo $error;?></strong></span>
        </div>
    <?php } ?>
	<div class="clear"></div>
	<div class="column full">
	
	<div class="box">		
		<div class="box themed_box">
		<h2 class="box-header">Suspended Agents </h2>
			
			<div class="box-content box-table">
			<table class="tablebox">
            
                <div id="topbar" style="border:#CCC solid 1px;">
                    <div style="float:left;">
                        <strong>Show</strong>
                            <?php if($search_type=='normal') { ?>
                            <select name="limit" id="limit" onchange="getlimit(this.value)" style="width:80px;">
                            <?php } if($search_type=='search') { ?>
                             <select name="limit" id="limit" onchange="getsearchlimit(this.value)" style="width:80px;">
                            <?php } ?>
                                <option value="0">Per Page</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="75">75</option>
                                <option value="100">100</option>
                            </select>
                      </div>
                    
                     <form name="frm_search" id="frm_search" method="post" action="<?php echo site_url('agent/search_suspend_agent'); ?>/<?php echo $limit; ?>" onSubmit="return chk_valid();">
                     <strong>&nbsp;&nbsp;&nbsp;Search By</strong>&nbsp;
                        <select name="option" id="option" style="width:100px;" onChange="gomain(this.value)">
                            <option value="all">All</option> 
            <option value="us.profile_name" <?php if($option=='us.profile_name'){?> selected="selected"<?php }?>>Username</option>
            <option value="us.email"  <?php if($option=='us.email'){?> selected="selected"<?php }?>>E-mail</option> 
           <option value="us.first_name" <?php if($option=='us.first_name'){?> selected="selected"<?php }?>>First Name</option>
            <option value="us.last_name" <?php if($option=='us.last_name'){?> selected="selected"<?php }?>>Last Name</option>
            <option value="ag.agency_name"  <?php if($option=='ag.agency_name'){?> selected="selected"<?php }?>>Agency Name</option>
            <option value="ag.contact_person_name"  <?php if($option=='ag.contact_person_name'){?> selected="selected"<?php }?>>Contact Person Name</option>    
            <option value="ag.agency_contact_email"  <?php if($option=='ag.agency_contact_email'){?> selected="selected"<?php }?>>Agency Contact Email</option>
           <option value="ag.agency_phone"  <?php if($option=='ag.agency_phone'){?> selected="selected"<?php }?>>Agency Phone</option>
           <option value="ag.agency_customer_support_no"  <?php if($option=='ag.agency_customer_support_no'){?> selected="selected"<?php }?>>Agency Customer Support no.</option>       
                   </select>
        
                        <input type="text" name="keyword" id="keyword" value="<?php echo $keyword;?>" class="textfield"/>                
                        <input type="submit" name="submit" id="submit" value="Search" class="button themed" /> 
                     </form> 
              
              
              
              <div style="float:right;">
             
             
               <form  name="frm_agents" id="frm_agents" action="<?php echo site_url('agent/action_agent/suspend');?>/" method="post">
           
              <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
              <input type="hidden" name="limit" id="limit" value="<?php echo $limit; ?>" />
              <input type="hidden" name="option" id="option" value="<?php echo $option; ?>" />
              <input type="hidden" name="keyword" id="keyword" value="<?php echo $keyword; ?>" />
              <input type="hidden" name="search_type" id="search_type" value="<?php echo $search_type; ?>" />
              
               <input type="hidden" name="action" id="action" />
                
				
                
                  <a href="<?php echo site_url('agent/add_agent');?>"  class="button white" style="margin: 0px;"><span class="icon_text addnew"></span> Add Agent</a>
                   
                   
                  <?php //Super Admin portion
						if($this->session->userdata('admintype') == 1){?>      
                   <a href="javascript:void(0)"  onclick="setaction('chk[]','active', 'Are you sure, you want to active selected record(s)?','frm_agents')" class="button white" style="margin: 0px;"><span class="icon_text accept"></span> Active</a>
                   
                      <a href="javascript:void(0)"  onclick="setaction('chk[]','inactive', 'Are you sure, you want to inactive selected record(s)?','frm_agents')" class="button white" style="margin: 0px;"><span class="icon_text inactive"></span> Inactive</a>
               
                 <a href="javascript:void(0)"  onclick="setaction('chk[]','delete', 'Only agent related records are delete, not user. Are you sure, you want to delete selected record(s)?','frm_agents')" class="button white" style="margin: 0px;"><span class="icon_text cancel"></span> Delete</a>
               <?php }?>  
                 
                 
                 </div>
                 </div>
                

				<thead class="table-header">
					<tr> 
                    	 <th class="first tc"><a href="javascript:void(0)" onClick="javascript:setchecked('chk[]',1)" style="color:#000;"><?php echo "Check All"; ?></a> |
           <a href="javascript:void(0)" onClick="javascript:setchecked('chk[]',0)" style="color:#000;"><?php echo "Clear All"; ?></a></th>
                        <th class="first tc">User Name</th>
                        
                        <th>Email</th>
                      
                        <th>Agency</th>
                       
                        <th>Phone</th>
                       
                        <th>Contact Person</th>
                        
                        <th>Level</th>
                                     
                        
                        
                        
                        <th>Status</th>
                        
                        <th>Coversation</th>
                        
                        <th>Detail</th> 
                         <?php //Super Admin portion
						if($this->session->userdata('admintype') == 1){?>
                        <th>Change Password</th>
                        <th class="tc">&nbsp;</th> 
                        <?php }?>  
                       
					</tr>
				</thead>
				
				<tbody class="openable-tbody">
				<?php
                    if($result)
                    {
                        $i=0;
                        foreach($result as $row)
                        {
                            if($i%2=="0")
                            {
                                $cl = "odd";	
                            }else{	
                                $cl = "even";	
                            }
                  ?>
					<tr class="<?php echo $cl; ?>">
                   		<td><input type="checkbox" name="chk[]" id="chk" value="<?php echo $row->user_id;?>" /></td>
                        <td class="tc"><?php echo anchor(front_base_url().'travelagent/'.$row->profile_name,ucwords($row->first_name.' '.$row->last_name),' style="color:#004C7A;" target="_blank"'); ?></td>
                     
                          <td><span style=" color:<?php if($row->verify_email==1) { ?> #009900 <?php } else { ?> #FF0000 <?php } ?>;"><?php  echo $row->email; ?></span></td>
                                               
                        
                       
                        <td><?php echo ucwords($row->agency_name); ?></td>
                        
                      
                        <td><?php echo $row->agency_phone; ?></td>
                     
                        
                        <td><?php echo ucwords($row->contact_person_name); ?></td>
                        <td><?php echo $row->agent_level; ?></td>
                        
                        
                     
                        
                     
                        
                        <td>
                        <?php if($row->user_status==2) { echo "Suspend"; } elseif($row->user_status==1) { echo "Active"; } elseif($row->user_status==0) { echo "Inactive"; } ?>
                        </td>
                        
                          <td><?php  echo anchor('suspend/index/'.$row->user_id,'Conversation','class="button white" target="_blank"'); ?></td>
                          
                                                
                        <td><?php  echo anchor('agent/agent_detail/'.$row->user_id,'View Detail','class="button white"'); ?></td>
                        
                         <?php //Super Admin portion
						if($this->session->userdata('admintype') == 1){?>
                        <td class="tc"><?php echo anchor('agent/change_password/suspend/'.$row->user_id.'/'.$offset.'/'.$limit.'/'.$search_type.'/'.$keyword.'/'.$option,'Change Password','class="button white"'); ?></td>
                        
                         <?php if($row->user_status==2) { ?>
                        <td class="tc">&nbsp;</td>
                        <?php } else { ?>
                        <td class="tc"><a class="button themed openable"><span class="icon_single extend"></span></a></td>
                        <?php } ?>
                        
                        
                  	</tr>
                    
                     <tr class="<?php echo $cl; ?> openable-tr">
                   		<td colspan="3" style="text-align:center;">&nbsp;</td>
						<td colspan="10" style="text-align:left;">
                       
                        
							<div class="fl typo" style="padding:25px;">
                               
                            <label class="form-label">Number Of Day</label>
                            <input type="text" name="no_of_day_<?php echo $row->user_id;?>" id="no_of_day_<?php echo $row->user_id;?>" class="form-field"  style="width:300px;" />
                            
                                    <label class="form-label">Suspend Reason</label>
                                    <textarea name="suspend_reason_<?php echo $row->user_id;?>" id="suspend_reason_<?php echo $row->user_id;?>" class="" style="height:65px; width:300px; padding: 2px 0 2px 5px;
border: 1px solid silver; -webkit-border-radius: 5px; margin-bottom:10px;"></textarea>   
                                
                                    <label class="form-label">Is Permanent?</label>
                                    <input type="radio" value="0" name="is_permanent_<?php echo $row->user_id;?>" id="is_permanent_<?php echo $row->user_id;?>" /><label class="radio" for="radio2"><strong>Temporary</strong></label>
                                    <input  type="radio" value="1" name="is_permanent_<?php echo $row->user_id;?>"  id="is_permanent_<?php echo $row->user_id;?>" /><label class="radio" for="radio3"><strong>Permanent</strong></label>
   
                                   
                                   
                                  <label class="form-label">&nbsp;</label>
                                   <input type="hidden" name="user_id" value="<?php echo $row->user_id;?>" class="button themed" id="user_id">  	<input type="hidden" name="offset_<?php echo $row->user_id;?>" id="offset_<?php echo $row->user_id;?>"  value="<?php echo $offset; ?>" />
                  <input type="hidden" name="limit_<?php echo $row->user_id;?>" id="limit_<?php echo $row->user_id;?>"  value="<?php echo $limit; ?>" />
                  <input type="hidden" name="option_<?php echo $row->user_id;?>" id="option_<?php echo $row->user_id;?>"  value="<?php echo $option; ?>" />
                  <input type="hidden" name="keyword_<?php echo $row->user_id;?>" id="keyword_<?php echo $row->user_id;?>" value="<?php echo $keyword; ?>" />
                  <input type="hidden" name="search_type_<?php echo $row->user_id;?>" id="search_type_<?php echo $row->user_id;?>"  value="<?php echo $search_type; ?>" />
                                 
                                   <input type="button" name="Submit" id="susid_<?php echo $row->user_id;?>" value="Submit" class="button themed suspendsubmit" >
                            </div>
						<div class="clear"></div>
                  
						</td>
                        <td colspan="3" style="text-align:center;">&nbsp;</td>
                        <?php } // Super admin portion?>
					</tr>
				  <?php
                            $i++;
                        }
                    }
                  else { ?>
						<tr class="alter"><td colspan="12" align="center" valign="middle" height="30">No Agent has been suspended yet</td></tr>
					<?php 	}
					?> 	
				</tbody>
			</table>
				
                <ul class="pagination">
					<?php echo $page_link; ?>
                </ul>
			</div>
		</div>
	</div>
</div>