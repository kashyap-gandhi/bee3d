
<div id="content" align="center">

<?php if($msg!=''){

		if($msg=='accept') { ?>
			
		<div class="column full">
			<span class="message information"><strong>Application has been approved successfully.</strong></span>
		</div>
        
        
    <?php } if($msg=='reject') { ?>
    <div class="column full">
			<span class="message information"><strong>Application has been rejected successfully.</strong></span>
		</div>
      <?php } } ?>
      
      
    
	<div align="left" class="column half">
		<div class="box">	
            <h2 class="box-header"><?php echo anchor(front_base_url().'travelagent/'.$row->profile_name,ucfirst($row->first_name).' '.ucfirst($row->last_name),' style="color:#004C7A;" target="_blank"'); ?></h2> 
			<div class="box-content">
			
			 
            
			  <table class="tablebox">
                  <tbody class="openable-tbody">
                  
                    <tr> <td colspan="2" align="right" valign="top"><input type="button" name="" id="" value="Edit Agent" class="button themed" onclick="window.location.href='<?php echo site_url('agent/edit_agent/'.$row->user_id);?>'" /></td></tr>
                  
                     <tr><td colspan="2" align="left" valign="top">
                      
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      
                      <tr>
                      
                     	 <td align="left" valign="top" width="150">
                        <?php if($row->profile_image!='') {
						
								
								if(file_exists(base_path().'upload/user/'.$row->profile_image))
								{
								?>
                                 <img src="<?php echo upload_url();?>upload/user/<?php echo $row->profile_image;?>" style="border-radius:5px; width:120px; height:120px;"/>
                                <?php
								
								} else { 
						
						?>
                         <img src="<?php echo upload_url();?>upload/no_image.png" style="border-radius:5px; width:120px; height:120px;"/>
                        
                        <?php } ?>
                                
                          <?php } else {?>
                                  <img src="<?php echo upload_url();?>upload/no_image.png" style="border-radius:5px; width:120px; height:120px;"/>
                          <?php }?>
                          
                          </td>
                          
                          <td align="left" valign="top">
                      
                       <table border="0" cellpadding="0" cellspacing="0" width="70%">
                      
                      
                     
                       
                       
                       <tr>
                          <td style="text-align:left; width:35%"><label class="form-label">User Name </label></td>
                          <td style="text-align:left; width:65%">: <?php echo anchor(front_base_url().'travelagent/'.$row->profile_name,$row->profile_name,' style="color:#004C7A;" target="_blank"'); ?></td>
                      </tr>
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">First Name </label></td>
                          <td style="text-align:left;">: <?php echo $row->first_name;?></td>
                      </tr>
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Last Name </label></td>
                          <td style="text-align:left;">: <?php echo $row->last_name;?></td>
                      </tr>
                      
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Email </label></td>
                          <td style="text-align:left;">: <?php echo $row->email;?></td>
                      </tr>
                      
                   
                      
                      </table>
                     
                      
                    	  </td>
                      
                      </tr>
                      </table>
                       
                  </td></tr>
                  
                  
                      
                       <tr><td colspan="2"><hr/></td></tr>
                       
                   
                      
                  	  <tr>
                          <td style="text-align:left; width:170px;"><label class="form-label">About User </label></td>
                          <td style="text-align:left;">: <?php if($row->about_user == ''){echo '<b>N/A</b>';}else{echo $row->about_user;}?></td> 
                      </tr>
                      
                      
                      
                      
                      <tr><td colspan="2"><hr/></td></tr>
                      
                      
                        <tr>
                          <td style="text-align:left;"><label class="form-label">Howmany time Suspend?</label></td>
                          <td style="text-align:left;"> : <?php echo $this->user_model->get_suspend_user_count($row->user_id);?></td>
                      </tr>
                      
                      
                     <tr><td colspan="2"><hr/></td></tr>
                     
                     
                       
                    
                    
                    <tr>
                          <td style="text-align:left;"><label class="form-label">Agency Name </label></td>
                          <td style="text-align:left;">: <?php echo $row->agency_name;?></td>
                      </tr>
                      
                      
                        <tr>
                          <td style="text-align:left;"><label class="form-label">Contact Person Name </label></td>
                          <td style="text-align:left;">: <?php echo $row->contact_person_name;?></td>
                      </tr>
                       
                       
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Contact Email</label></td>
                          <td style="text-align:left;">: <?php if($row->agency_contact_email == ''){echo '<b>N/A</b>';}else{echo $row->agency_contact_email;}?></td>
                      </tr>
                      
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Agency Phone</label></td>
                          <td style="text-align:left;">: <?php echo $row->agency_phone;?></td>
                      </tr>
                      
                      
                      <tr>
                          <td style="text-align:left;"><label class="form-label">Agency Customer Support No.</label></td>
                          <td style="text-align:left;">: <?php if($row->agency_customer_support_no == ''){ echo '<b>N/A</b>';}else{echo $row->agency_customer_support_no;}?></td>
                      </tr>
                      
                      
                       <tr><td colspan="2"><hr/></td></tr>
                      
                      
                      <tr>
                          <td style="text-align:left;"><label class="form-label">Agency Website</label></td>
                          <td style="text-align:left;">: <?php if($row->agency_website == ''){ echo '<b>N/A</b>';}else {echo $row->agency_website;}?></td>
                      </tr>
                      
                     
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Agency Location</label></td>
                          <td style="text-align:left;">: <?php echo $row->agency_location;?></td>
                      </tr>
                      
                      
                      <tr>
                          <td style="text-align:left;"><label class="form-label">Member Of Association</label></td>
                          <td style="text-align:left;">: <?php $member_assoc=$row->agent_member_of_association;
						  
						  if($member_assoc!='') { $exp_assoc=explode(',',$member_assoc);
						  
						  	foreach($exp_assoc as $exp) {
							
								$association_detail=get_member_association_by_id($exp);
								
								if($association_detail) { echo $association_detail->member_association.','; }
								
								}													
							}
						  
						  
						  ?></td>
                      </tr> 
                     
                     
                     
                     
                      <tr><td colspan="2"><hr/></td></tr>  
                      
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Agent Document</label></td>
                          <td style="text-align:left;">:
                         
                      		<?php if($agent_document) { $id=1;
										foreach($agent_document as $docs) { 
										
									
									if($docs->agent_document!='') {			
										if(file_exists(base_path().'upload/agent_doc/'.$docs->agent_document)) {
							?>
									
                                    <a href="<?php echo upload_url().'upload/agent_doc/'.$docs->agent_document;?>" target="_blank">Doc <?php echo $id++; ?></a><br>
	
									
										
							<?php } } } }  else { echo "<b>N/A</b>";}?>
									
                       
                      
	                      </td>
                      </tr>
                      
                       <tr><td colspan="2"><hr/></td></tr>  
                      
                      
                      
                      
                 <tr>
                  <td style="text-align:left;"><label class="form-label">Created Date</label></td>
                  <td style="text-align:left;">: <?php echo date($site_setting->date_time_format,strtotime($row->agent_date));?></td>
              </tr>
                      
                    <tr>
                          <td style="text-align:left;"><label class="form-label">IP</label></td>
                          <td style="text-align:left;">
                         
                        <table border="0" cellpadding="2" cellspacing="2" align="left" width="200">
                        <tr>
                        <td align="left" valign="top">:  <?php echo $row->agent_ip;?></td>
                        <td align="left" valign="top"> <img src="<?php echo base_url().$theme;?>/images/agentip.png" border="0" width="30" height="30" /></td></tr></table>
                        
                        </td>
                      </tr>
                      
                      <?php //Super Admin portion
						if($this->session->userdata('admintype') == 1){?>   
                     <tr><td colspan="2"><hr/></td></tr>    
                    
                    
                 <form method="post" name="frmagent" id="frmagent" action="<?php echo site_url('agent/agent_detail/'.$user_id);?>">   
                    
                    
                  
                    
                    
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Agent Application Approve</label></td>
                          <td style="text-align:left;" align="left" valign="top">  
                      
                        <table border="0" cellpadding="2" cellspacing="2" align="left" width="200">
                        <tr>
                        <td align="left" valign="top">
                        :  <input type="radio" name="agent_app_approved" value="1" <?php if($row->agent_app_approved==1) {?> checked <?php } ?> />&nbsp;</td>
                         <td align="left" valign="top">Approve</td>
                         <td align="left" valign="top">
                        <input type="radio" name="agent_app_approved" value="0" <?php if($row->agent_app_approved==0) {?> checked <?php } ?> />&nbsp;</td>
                        <td align="left" valign="top">Reject</td>
                        
                        <td align="left" valign="top"><img src="<?php echo base_url().$theme;?>/images/agentapplication.png" border="0" width="30" height="30" /></td>
                        </tr>
                        </table>
                        
                        
                  		  </td>
                    </tr>
                      
                      
                      
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Agent Background Approve</label></td>
                          <td style="text-align:left;">   
                        
                        
                        
                         <table border="0" cellpadding="2" cellspacing="2" align="left" width="200">
                        <tr>
                        <td align="left" valign="top">
                        :  <input type="radio" name="agent_background_approved" value="1" <?php if($row->agent_background_approved==1) {?> checked <?php } ?> />&nbsp;</td>
                         <td align="left" valign="top">Approve</td>
                         <td align="left" valign="top">
                        <input type="radio" name="agent_background_approved" value="0" <?php if($row->agent_background_approved==0) {?> checked <?php } ?> />&nbsp;</td>
                        <td align="left" valign="top">Reject</td>
                        
                        <td align="left" valign="top"><img src="<?php echo base_url().$theme;?>/images/agentbackground.png" border="0" width="30" height="30" /></td>
                        </tr>
                        </table>
                        
                        
                        
                  
                        
                  		  </td>
                    </tr>
                      
                      
                      
                      
                        <tr>
                          <td style="text-align:left;"><label class="form-label">Agent Phone Approve</label></td>
                          <td style="text-align:left;">
                      
                        
                        
                           <table border="0" cellpadding="2" cellspacing="2" align="left" width="200">
                        <tr>
                        <td align="left" valign="top">
                        :  <input type="radio" name="agent_phone_approved" value="1" <?php if($row->agent_phone_approved==1) {?> checked <?php } ?> />&nbsp;</td>
                         <td align="left" valign="top">Approve</td>
                         <td align="left" valign="top">
                        <input type="radio" name="agent_phone_approved" value="0" <?php if($row->agent_phone_approved==0) {?> checked <?php } ?> />&nbsp;</td>
                        <td align="left" valign="top">Reject</td>
                        
                        <td align="left" valign="top"><img src="<?php echo base_url().$theme;?>/images/agentphone.png" border="0" width="30" height="30" /></td>
                        </tr>
                        </table>
                        
                        
                        
                        
                  		  </td>
                    </tr>
                    
                    
                      
                      
                      
                         <tr>
                          <td style="text-align:left;"><label class="form-label">Agent Level</label></td>
                          <td style="text-align:left;">:   
                        
                         <input type="text" name="agent_level" id="agent_level" value="<?php echo $row->agent_level;?>" class="form-field width40" />
                        
                  		  </td>
                    </tr>
                    
                    
                      
                      
                      <tr>
                      <td>&nbsp;</td>
                      <td align="left" valign="top">  <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>" />
                      <input type="hidden" name="email" id="email" value="<?php echo $row->email;?>"/>
                       <input type="hidden" name="super_admin_approve" id="super_admin_approve" value="<?php echo $row->super_admin_approve;?>"/>
                      <input type="submit" name="submit" id="submit" class="button themed" /></td>
                      </tr>
                      
                      
                      </form>
                     
                      
                    
                      
                    <tr><td colspan="2"><hr/></td></tr>
                       
                    <?php }?>   
                       
                         
                   
                    
                      
                  </tbody>
			  	 </table>   
             </div>
              


			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>