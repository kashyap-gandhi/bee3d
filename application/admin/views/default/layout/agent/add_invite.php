<div id="content" align="center">

 	<?php if($error!=''){ ?>
		<div class="column full">
			<span class="message information"><strong><?php echo $error;?></strong></span>
		</div>
    <?php }?>

	<div align="left" class="column half">
		<div class="box">
			<h2 class="box-header">Invite Agent</h2> 
			<div class="box-content">	
			  <?php
			
				$attributes = array('name'=>'frm_add_invite');
				echo form_open('agent/add_invite',$attributes);
			  ?>					
				  <label class="form-label">First Name </label> 
				  <input type="text" name="agent_first_name" id="agent_first_name" value="<?php echo $agent_first_name; ?>" class="form-field width40"/>
									
				  <label class="form-label">Last Name </label>
                  <input type="text" name="agent_last_name" id="agent_last_name" value="<?php echo $agent_last_name; ?>" class="form-field width40"/>
                   
                  <label class="form-label">Email </label>
                  <input type="text" name="agent_email" id="agent_email" value="<?php echo $agent_email; ?>" class="form-field width40"/> 
                    
                    
                      <label class="form-label">Company Name </label>
                  <input type="text" name="agent_company_name" id="agent_company_name" value="<?php echo $agent_company_name; ?>" class="form-field width40"/>
                  
                  
                  
                     <label class="form-label">Phone No. </label>
                  <input type="text" name="agent_phone_no" id="agent_phone_no" value="<?php echo $agent_phone_no; ?>" class="form-field width40"/> 
                                 

                  <label class="form-label">About Agent </label>
                  <textarea class="form-field small" name="agent_about" cols="" rows="" id="agent_about"><?php echo $agent_about; ?></textarea>
     
     
     
     
       
                  
				   <label class="form-label">&nbsp;</label> 
                
                   
              <input type="hidden" name="invite_id" id="invite_id" value="<?php echo $invite_id; ?>" />
				   <input type="submit" name="submit" value="Submit" class="button themed"/>
				 
                  
                   <input type="button" name="cancel" value="Cancel" class="button themed" onClick="location.href='<?php echo site_url('agent/list_invite_agent'); ?>'"/>  
				  
			  </form>
			
            
            
            </div>
		</div>
	</div>
	<div class="clear"></div>
</div>