<div id="content" align="center">

 	<?php if($error!=''){ ?>
		<div class="column full">
			<span class="message information"><strong><?php echo $error;?></strong></span>
		</div>
    <?php }?>

	<div align="left" class="column half">
		<div class="box">
			<h2 class="box-header">Add User</h2> 
			<div class="box-content">	
			  <?php
			
				$attributes = array('name'=>'frm_add_agent');
				echo form_open('agent/add_agent',$attributes);
			  ?>					
				  <label class="form-label">First Name </label> 
				  <input type="text" name="first_name" id="first_name" value="<?php echo $first_name; ?>" class="form-field width40"/>
									
				  <label class="form-label">Last Name </label>
                  <input type="text" name="last_name" id="last_name" value="<?php echo $last_name; ?>" class="form-field width40"/>
                   
                  <label class="form-label">Email </label>
                  <input type="text" name="email" id="email" value="<?php echo $email; ?>" class="form-field width40"/> 
                                  <br />
                  (Note : Password automatically sent to above email address.)<br />
<br /><br /><br />

<hr/>

                  <label class="form-label">About User </label>
                  <textarea class="form-field small" name="about_user" cols="" rows="" id="about_user"><?php echo $about_user; ?></textarea>
     
     
     
     <hr/>
     
      			  <label class="form-label">Agency Name </label>
                  <input type="text" name="agency_name" id="agency_name" value="<?php echo $agency_name; ?>" class="form-field width40"/>
                  
                  
                    <label class="form-label">Contact Person Name </label>
                  <input type="text" name="contact_person_name" id="contact_person_name" value="<?php echo $contact_person_name; ?>" class="form-field width40"/>
                  
                  
                  
                  
                   <label class="form-label">Contact Email </label>
                  <input type="text" name="agency_contact_email" id="agency_contact_email" value="<?php echo $agency_contact_email; ?>" class="form-field width40"/>
                  
                  
                  
                    <label class="form-label">Agency Phone </label>
                  <input type="text" name="agency_phone" id="agency_phone" value="<?php echo $agency_phone; ?>" class="form-field width40"/>
                  
                  
                  
                    <label class="form-label">Agency Customer Support No. </label>
                  <input type="text" name="agency_customer_support_no" id="agency_customer_support_no" value="<?php echo $agency_customer_support_no; ?>" class="form-field width40"/>
                  
                  
                  
    <hr/>
    
    
    			 <label class="form-label">Agency Website </label>
                  <input type="text" name="agency_website" id="agency_website" value="<?php echo $agency_website; ?>" class="form-field width40"/>
                  
                  
                  
                   <label class="form-label">Agency Location </label>
                  <input type="text" name="agency_location" id="agency_location" value="<?php echo $agency_location; ?>" class="form-field width40"/>
                  
                  
                  
                    <label class="form-label">Member Of Association </label>
               
                  		<?php 
						$ascnt=1;
							if($member_association)
							{
								foreach($member_association as $ma)
								{  if($ascnt==6) {  echo "<br/><br />"; $ascnt=1; } ?>
                  
                <input type="checkbox" name="agent_member_of_association[]" <?php if($agent_member_of_association){  if(in_array($ma->member_association_id,$agent_member_of_association)){ ?> checked="checked"<?php } } ?> value="<?php echo $ma->member_association_id;?>" />&nbsp;<?php echo $ma->member_association;?>&nbsp;&nbsp;
                
                <?php  $ascnt++; }  }  ?>
                
                
<div class="clear"></div>

                  
                  
                  
                  
                  
                  
                  <hr/>
                  
               <?php //Super Admin portion
						if($this->session->userdata('admintype') == 1){?>   
      <label class="form-label">Agent Application Approve</label>
                        <table border="0" cellpadding="2" cellspacing="2" align="left" width="200">
                        <tr>
                        <td align="left" valign="top">
              <input type="radio" name="agent_app_approved" value="1" <?php if($agent_app_approved==1) {?> checked <?php } ?> />&nbsp;</td>
                         <td align="left" valign="top">Approve</td>
                         <td align="left" valign="top">
                        <input type="radio" name="agent_app_approved" value="0" <?php if($agent_app_approved==0) {?> checked <?php } ?> />&nbsp;</td>
                        <td align="left" valign="top">Reject</td>
                        
                        <td align="left" valign="top"><img src="<?php echo base_url().$theme;?>/images/agentapplication.png" border="0" width="30" height="30" /></td>
                        </tr>
                        </table>
                        
                       
                  		
                        
                        <br /><br /><br /><br />


                    
                    
                      <label class="form-label">Agent Background Approve</label>
                        
                         <table border="0" cellpadding="2" cellspacing="2" align="left" width="200">
                        <tr>
                        <td align="left" valign="top">
                         <input type="radio" name="agent_background_approved" value="1" <?php if($agent_background_approved==1) {?> checked <?php } ?> />&nbsp;</td>
                         <td align="left" valign="top">Approve</td>
                         <td align="left" valign="top">
                        <input type="radio" name="agent_background_approved" value="0" <?php if($agent_background_approved==0) {?> checked <?php } ?> />&nbsp;</td>
                        <td align="left" valign="top">Reject</td>
                        
                        <td align="left" valign="top"><img src="<?php echo base_url().$theme;?>/images/agentbackground.png" border="0" width="30" height="30" /></td>
                        </tr>
                        </table>
                        
                        
                        
                        <br /><br /><br /><br />

                        
                        
                        
                      <label class="form-label">Agent Phone Approve</label>
                        
                        
                           <table border="0" cellpadding="2" cellspacing="2" align="left" width="200">
                        <tr>
                        <td align="left" valign="top">
                         <input type="radio" name="agent_phone_approved" value="1" <?php if($agent_phone_approved==1) {?> checked <?php } ?> />&nbsp;</td>
                         <td align="left" valign="top">Approve</td>
                         <td align="left" valign="top">
                        <input type="radio" name="agent_phone_approved" value="0" <?php if($agent_phone_approved==0) {?> checked <?php } ?> />&nbsp;</td>
                        <td align="left" valign="top">Reject</td>
                        
                        <td align="left" valign="top"><img src="<?php echo base_url().$theme;?>/images/agentphone.png" border="0" width="30" height="30" /></td>
                        </tr>
                        </table>
                        
                        
                        <br /><br /><br /><br />
                        
                      

                        
                        
                     <label class="form-label">Agent Level</label> <input type="text" name="agent_level" id="agent_level" value="<?php echo $agent_level;?>" class="form-field width40" />
                        
                  	
                    
                    
                  <hr/>
                  
                  
     
       
              <?php /*?>    <label class="form-label">Status </label> 
				  <select name="user_status" id="user_status" class="form-field settingselectbox required">
				  	<option value="0" <?php if($user_status=='0'){ echo "selected"; } ?>>Inactive</option>
					<option value="1" <?php if($user_status=='1'){ echo "selected"; } ?>>Active</option>	
				  </select>
                  
				   <label class="form-label">&nbsp;</label> <?php */?>
                   
                   
               <?php  }?> 
              
                   
              
				   <input type="submit" name="submit" value="Submit" class="button themed"/>
				 
                  
                   <input type="button" name="cancel" value="Cancel" class="button themed" onClick="location.href='<?php echo site_url('agent/list_all_agent'); ?>'"/>  
				  
			  </form>
			
             <script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>  
          <script> 
		var options = {		
		types: ['(cities)'],
		  //componentRestrictions: {country: 'us'}
		};
	
	   var start_from = new google.maps.places.Autocomplete($("#agency_location")[0], options);
        google.maps.event.addListener(agency_location, 'place_changed', function() {
                var place_start = agency_location.getPlace();
                console.log(agency_location.address_components);
            }); 
			</script>
            
            </div>
		</div>
	</div>
	<div class="clear"></div>
</div>