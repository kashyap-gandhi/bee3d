<script type="text/javascript" language="javascript">

	
	function getlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			window.location.href='<?php echo site_url('agent/list_report');?>/'+limit;
		}
	
	}	
	
	function getsearchlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			
			window.location.href='<?php echo site_url('agent/search_list_report');?>/'+limit+'/<?php echo $option.'/'.$keyword; ?>';
		}
	
	}
	
	function gomain(x)
	{
		
		if(x == 'all')
		{
			window.location.href= '<?php echo site_url('agent/list_report');?>';
		}
	}
	
	function setchecked(elemName,status){
		elem = document.getElementsByName(elemName);
		for(i=0;i<elem.length;i++){
			elem[i].checked=status;
		}
	}


	function setaction(elename, actionval, actionmsg, formname) 
	{
		vchkcnt=0;
		elem = document.getElementsByName(elename);
		
		for(i=0;i<elem.length;i++){
			if(elem[i].checked) vchkcnt++;	
		}
		if(vchkcnt==0) {
			alert('Please select a record')
		} else {
			
			if(confirm(actionmsg))
			{
				document.getElementById('action').value=actionval;	
				document.getElementById(formname).submit();
			}		
			
		}
	}


	function chk_valid()
	{
		
		var keyword = document.getElementById('keyword').value;
		
		if(keyword=='')
		{
			alert('Please enter search keyword');	
			return false;
			
		}
		
		else
		{
			return true;			
		}
		
		
		
	}
	
	
</script>

<div id="content">        
	<?php $error='';
		
		 if($msg != ""){
            if($msg == "insert"){ $error = 'New Record has been added Successfully.';}
            if($msg == "update"){ $error = 'Record has been updated Successfully.';}
            if($msg == "delete"){ $error = 'Agent has been deleted Successfully.';}
			if($msg == "edit"){ $error = 'Agent status has been changed Successfully.';}
			if($msg == "active"){ $error = 'Agent has been activated Successfully.';}
			if($msg == "inactive"){ $error = 'Agent has been inactivated Successfully.';}
			if($msg == "suspend"){ $error = 'Agent has been suspended Successfully.';}
			if($msg == "reset"){ $error = 'Administrator Report has been reset Successfully.';}
			
			
			
			
			
    ?>
        <div class="column full">
            <span class="message information"><strong><?php echo $error;?></strong></span>
        </div>
    <?php } ?>
	<div class="clear"></div>
	<div class="column full">
	
	<div class="box">		
		<div class="box themed_box">
		<h2 class="box-header">Reports</h2>
			
			<div class="box-content box-table">
			<table class="tablebox">
            
                <div id="topbar" style="border:#CCC solid 1px;">
                    <div style="float:left;">
                        <strong>Show</strong>
                            <?php if($search_type=='normal') { ?>
                            <select name="limit" id="limit" onchange="getlimit(this.value)" style="width:80px;">
                            <?php } if($search_type=='search') { ?>
                             <select name="limit" id="limit" onchange="getsearchlimit(this.value)" style="width:80px;">
                            <?php } ?>
                                <option value="0">Per Page</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="75">75</option>
                                <option value="100">100</option>
                            </select>
                      </div>
                    
                     <form name="frm_search" id="frm_search" method="post" action="<?php echo site_url('agent/search_list_report'); ?>/<?php echo $limit; ?>" onSubmit="return chk_valid();">
                     <strong>&nbsp;&nbsp;&nbsp;Search By</strong>&nbsp;
                        <select name="option" id="option" style="width:100px;" onChange="gomain(this.value)">
                            <option value="all">All</option> 
            <option value="ad.username" <?php if($option=='ad.username'){?> selected="selected"<?php }?>>Username</option>
            <option value="ad.email"  <?php if($option=='ad.email'){?> selected="selected"<?php }?>>E-mail</option> 
           
                   </select>
        
                        <input type="text" name="keyword" id="keyword" value="<?php echo $keyword;?>" class="textfield"/>                
                        <input type="submit" name="submit" id="submit" value="Search" class="button themed" /> 
                     </form> 
              
              
              
              <div style="float:right;">
             
             
               <form  name="frm_agents" id="frm_agents" action="<?php echo site_url('agent/action_report');?>/" method="post">
           
              <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
              <input type="hidden" name="limit" id="limit" value="<?php echo $limit; ?>" />
              <input type="hidden" name="option" id="option" value="<?php echo $option; ?>" />
              <input type="hidden" name="keyword" id="keyword" value="<?php echo $keyword; ?>" />
              <input type="hidden" name="search_type" id="search_type" value="<?php echo $search_type; ?>" />
              
               <input type="hidden" name="action" id="action" />
                
                
                
                   
				<?php 
						if($this->session->userdata('admintype') == 1){?>
                        
                   <a href="javascript:void(0)"  onclick="setaction('chk[]','reset', 'Are you sure, you want to reset selected record(s)?','frm_agents')" class="button white" style="margin: 0px;"><span class="icon_text accept"></span> Reset</a>
                   
                 
               <?php } ?>  
                 
                 </div>
                 </div>
                

				<thead class="table-header">
					<tr> 
                    	 <th class="first tc"><a href="javascript:void(0)" onClick="javascript:setchecked('chk[]',1)" style="color:#000;"><?php echo "Check All"; ?></a> |
           <a href="javascript:void(0)" onClick="javascript:setchecked('chk[]',0)" style="color:#000;"><?php echo "Clear All"; ?></a></th>
                        
                        <th>User Name</th>                        
                        <th>Email</th>
                        <th>Total Added</th>
                       <th>Total Approved</th>
                       <th>Total Waiting/Rejected</th>
                      
					</tr>
				</thead>
				
				<tbody class="openable-tbody">
				<?php
                    if($result)
                    {
                        $i=0;
                        foreach($result as $row)
                        {
                            if($i%2=="0")
                            {
                                $cl = "odd";	
                            }else{	
                                $cl = "even";	
                            }
                  ?>
					<tr class="<?php echo $cl; ?>">
                   		<td><input type="checkbox" name="chk[]" id="chk" value="<?php echo $row->admin_id;?>" /></td>
                        <td class="tc"><?php echo ucwords($row->username); ?></td>
                     
                        <td><?php  echo $row->email; ?></td>
                                               
                        
                      <td><?php  echo $row->total_add; ?></td>
                      
                        <td><?php echo $row->total_approve; ?></td>
                        <td><?php echo $row->total_reject; ?></td>
                      
                        
                  	</tr>
                    
                     
				  <?php
                            $i++;
                        }
                    }
                  else { ?>
						<tr class="alter"><td colspan="5" align="center" valign="middle" height="30">No Agent has been added yet</td></tr>
					<?php 	}
					?> 	
				</tbody>
			</table>
				
                <ul class="pagination">
					<?php echo $page_link; ?>
                </ul>
			</div>
		</div>
	</div>
</div>