<script type="text/javascript" language="javascript">

	
	function getlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			window.location.href='<?php echo site_url('agent/list_invite_agent');?>/'+limit;
		}
	
	}	
	
	function getsearchlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			
			window.location.href='<?php echo site_url('agent/search_invite_agent');?>/'+limit+'/<?php echo $option.'/'.$keyword; ?>';
		}
	
	}
	
	function gomain(x)
	{
		
		if(x == 'all')
		{
			window.location.href= '<?php echo site_url('agent/list_invite_agent');?>';
		}
	}
	
	function setchecked(elemName,status){
		elem = document.getElementsByName(elemName);
		for(i=0;i<elem.length;i++){
			elem[i].checked=status;
		}
	}


	function setaction(elename, actionval, actionmsg, formname) 
	{
		vchkcnt=0;
		elem = document.getElementsByName(elename);
		
		for(i=0;i<elem.length;i++){
			if(elem[i].checked) vchkcnt++;	
		}
		if(vchkcnt==0) {
			alert('Please select a record')
		} else {
			
			if(confirm(actionmsg))
			{
				document.getElementById('action').value=actionval;	
				document.getElementById(formname).submit();
			}		
			
		}
	}


	function chk_valid()
	{
		
		var keyword = document.getElementById('keyword').value;
		
		if(keyword=='')
		{
			alert('Please enter search keyword');	
			return false;
			
		}
		
		else
		{
			return true;			
		}
		
		
		
	}
	
	
</script>

<div id="content">        
	<?php $error='';
		
		 if($msg != ""){
            if($msg == "insert"){ $error = 'New Record has been added Successfully.';}
            if($msg == "update"){ $error = 'Record has been updated Successfully.';}
            if($msg == "delete"){ $error = 'Agent has been deleted Successfully.';}
			if($msg == "edit"){ $error = 'Agent status has been changed Successfully.';}
			
			
			
    ?>
        <div class="column full">
            <span class="message information"><strong><?php echo $error;?></strong></span>
        </div>
    <?php } ?>
	<div class="clear"></div>
	<div class="column full">
	
	<div class="box">		
		<div class="box themed_box">
		<h2 class="box-header">Invited Agents </h2>
			
			<div class="box-content box-table">
			<table class="tablebox">
            
                <div id="topbar" style="border:#CCC solid 1px;">
                    <div style="float:left;">
                        <strong>Show</strong>
                            <?php if($search_type=='normal') { ?>
                            <select name="limit" id="limit" onchange="getlimit(this.value)" style="width:80px;">
                            <?php } if($search_type=='search') { ?>
                             <select name="limit" id="limit" onchange="getsearchlimit(this.value)" style="width:80px;">
                            <?php } ?>
                                <option value="0">Per Page</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="75">75</option>
                                <option value="100">100</option>
                            </select>
                      </div>
                    
                     <form name="frm_search" id="frm_search" method="post" action="<?php echo site_url('agent/search_invite_agent'); ?>/<?php echo $limit; ?>" onSubmit="return chk_valid();">
                     <strong>&nbsp;&nbsp;&nbsp;Search By</strong>&nbsp;
                        <select name="option" id="option" style="width:100px;" onChange="gomain(this.value)">
                            <option value="all">All</option> 
        
          
          <option value="agent_first_name" <?php if($option=='agent_first_name'){?> selected="selected"<?php }?>>Agent First Name</option>
          <option value="agent_last_name" <?php if($option=='agent_last_name'){?> selected="selected"<?php }?>>Agent Last Name</option>
          <option value="agent_email"  <?php if($option=='agent_email'){?> selected="selected"<?php }?>>E-mail</option> 
          <option value="agent_phone_no" <?php if($option=='agent_phone_no'){?> selected="selected"<?php }?>>Agent Phone No.</option>
          <option value="agent_company_name" <?php if($option=='agent_company_name'){?> selected="selected"<?php }?>>Agent Company Name</option>
           
                   </select>
        
                        <input type="text" name="keyword" id="keyword" value="<?php echo $keyword;?>" class="textfield"/>                
                        <input type="submit" name="submit" id="submit" value="Search" class="button themed" /> 
                     </form> 
              
              
              
              <div style="float:right;">
             
             
               <form  name="frm_agents" id="frm_agents" action="<?php echo site_url('agent/action_invite_agent');?>" method="post">
           
              <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
              <input type="hidden" name="limit" id="limit" value="<?php echo $limit; ?>" />
              <input type="hidden" name="option" id="option" value="<?php echo $option; ?>" />
              <input type="hidden" name="keyword" id="keyword" value="<?php echo $keyword; ?>" />
              <input type="hidden" name="search_type" id="search_type" value="<?php echo $search_type; ?>" />
              
               <input type="hidden" name="action" id="action" />
                
				
                   <a href="<?php echo site_url('agent/add_invite');?>"  class="button white" style="margin: 0px;"><span class="icon_text addnew"></span> Add</a>
                   
                     
               
                 <a href="javascript:void(0)"  onclick="setaction('chk[]','delete', 'Are you sure, you want to delete selected record(s)?','frm_agents')" class="button white" style="margin: 0px;"><span class="icon_text cancel"></span> Delete</a>
                 
                  <?php echo anchor('agent/import_invite','Import CSV','class="button white"  style="margin: 0px;"'); ?> 
                 
                 </div>
                 </div>
                

				<thead class="table-header">
					<tr> 
                    	 <th class="first tc"><a href="javascript:void(0)" onClick="javascript:setchecked('chk[]',1)" style="color:#000;"><?php echo "Check All"; ?></a> |
           <a href="javascript:void(0)" onClick="javascript:setchecked('chk[]',0)" style="color:#000;"><?php echo "Clear All"; ?></a></th>
                        <th class="first tc">First Name</th>
                         <th class="first tc">Last Name</th>
                        
                        <th>Email</th>  
                        <th>Comapany Name</th>
                        <th>Phone</th>
                       
                        <th>Invited On</th>
                        
                        <th>Invite Status</th>
                        
                        <th>Detail</th> 
                       
					</tr>
				</thead>
				
				<tbody class="openable-tbody">
				<?php
                    if($result)
                    {
                        $i=0;
                        foreach($result as $row)
                        {
                            if($i%2=="0")
                            {
                                $cl = "odd";	
                            }else{	
                                $cl = "even";	
                            }
                  ?>
					<tr class="<?php echo $cl; ?>">
                   		<td><input type="checkbox" name="chk[]" id="chk" value="<?php echo $row->invite_id;?>" /></td>
                      
                        <td class="tc"><?php echo ucwords($row->agent_first_name); ?></td>
                         <td class="tc"><?php echo ucwords($row->agent_last_name); ?></td>
                     
                        <td><?php echo $row->agent_email; ?></td>
                        <td><?php echo $row->agent_company_name; ?></td>
                       

                        <td><?php echo $row->agent_phone_no; ?></td>
                        
                        <td><?php echo date($site_setting->date_time_format,strtotime($row->invite_date)); ?></td>
                        
                        
                       
                        <td><?php if($row->is_register==1) { ?> 
                        
                        <img src="<?php echo base_url().getThemeName();?>/gfx/accept.png" border="0" />
                        <?php } else { ?>
                        
                       
                        <?php } ?></td>
                                               
                        
                   
                                                
                        <td><?php  echo anchor('agent/edit_invite/'.$row->invite_id,'View Detail','class="button white"'); ?></td>
                     
                        
                        
                  	</tr>
                    
                     
				  <?php
                            $i++;
                        }
                    }
                  else { ?>
						<tr class="alter"><td colspan="8" align="center" valign="middle" height="30">No Invitation has been added yet</td></tr>
					<?php 	}
					?> 	
				</tbody>
			</table>
				
                <ul class="pagination">
					<?php echo $page_link; ?>
                </ul>
			</div>
		</div>
	</div>
</div>