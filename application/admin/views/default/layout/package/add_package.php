
<div id="content" align="center">

 	
 	<?php if($error!=''){ ?>
		<div class="column full">
			<span class="message information"><strong><?php echo $error;?></strong></span>
		</div>
    <?php }?>
    
    
    	<div align="left" class="column half">
		<div class="box">
			<h2 class="box-header"><?php if($package_id=='') {?>Add Package<?php } if($package_id!='') {  ?>Edit Package<?php } ?></h2> 
			<div class="box-content">
            <!-- Form --> 
             <?php
            $attributes = array('name'=>'frm_addpackage');
            echo form_open('package/add_package',$attributes);
      ?> 
                    
                           <label class="form-label">Package Name : </label> 
                           <span>      
                           <input class="form-field width40" name="package_name" id="package_name" type="text" value="<?php echo $package_name; ?>" /> 
                                 </span> 
                            

                           
                           
                           
                           <label class="form-label">Is Free: </label> 
                                    <span> 
                                   
                                   <select name="package_is_free" id="package_is_free" class="form-field settingselectbox required"> 
                                            <option value="0" <?php if($package_is_free==0) { ?> selected="selected" <?php } ?>>No</option>
                                            <option value="1" <?php if($package_is_free==1) { ?> selected="selected" <?php } ?>>Yes</option>
                                    </select> 
                                        
                                    </span>
                           
                           
                           
                           <label class="form-label">Package Type: </label> 
                                    <span> 
                                   
                                   <select name="package_type" id="package_type" class="form-field settingselectbox required"> 
                                            <option value="0" <?php if($package_type==0) { ?> selected="selected" <?php } ?>>Membership</option>
                                            <option value="1" <?php if($package_type==1) { ?> selected="selected" <?php } ?>>Buy Points</option>
                                    </select> 
                                        
                                    </span>
                           
                           
                           


                            
                             <label class="form-label">Package Price : </label> 
                             <span>
                                    <input class="form-field width40" name="package_price" id="package_price" type="text" value="<?php echo $package_price; ?>" /> 
                                 </span> 
                            
                             
                             
                              <label class="form-label">Package Points : </label> 
                             <span>
                                    <input class="form-field width40" name="package_points" id="package_points" type="text" value="<?php echo $package_points; ?>" /> 
                                 </span> 
                            
                             
                             

                     <label class="form-label">Package Days : </label> 
                                   
                     <span><input class="form-field width40" name="package_days" id="package_days" type="text" value="<?php echo $package_days; ?>" /> 
                           </span>
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                                   <label class="form-label">Status: </label> 
                                    <span> 
                                   
                                   <select name="package_status" id="package_status" class="form-field settingselectbox required"> 
                                            <option value="0" <?php if($package_status==0) { ?> selected="selected" <?php } ?>>Inactive</option>
                                            <option value="1" <?php if($package_status==1) { ?> selected="selected" <?php } ?>>Active</option>
                                    </select> 
                                        
                                    </span>




<label class="form-label">Package Item : </label> 

<?php 

                                    $ex_package_item=array();


                                    if($post_package_item)
                                    {
                                            $ex_package_item=$post_package_item;
                                    }
                                    else
                                    {
                                            if($package_item!='')
                                            {
                                                    $ex_package_item=explode(',',$package_item);
                                            }
                                    }

                                                    $db_package_item=$this->package_model->get_all_package_items(); 

                                                    if($db_package_item)
                                                    {

                                                            foreach($db_package_item as $items)
                                                            {

                                                            ?>


                                    <p><input type="checkbox" value="<?php echo $items->package_item_id; ?>" name="package_item[]" <?php if(in_array($items->package_item_id,$ex_package_item)) { ?> checked <?php } ?> /><?php echo ucfirst($items->item_name); ?></p>

                                    <?php }

                                    } else { echo "No item has been added yet.&nbsp;".anchor('package/add_package_item','Add Now','style="color:#000000;"'); } ?>

                           




<label class="form-label">Short Description : </label> 
<span>
    <textarea cols="80" rows="8" name="package_short_description" id="package_short_description"><?php echo $package_short_description; ?></textarea> 
</span> 




<label><br/><br/></label>


<label class="form-label">Package Description : </label> 
<span>
<textarea  cols="80" rows="8" name="package_description" id="package_description"><?php echo $package_description; ?></textarea> 
</span> 






<label class="form-label">&nbsp;</label> 



                            <?php if($package_id=='') { ?>
                                    <input class="button themed" type="submit" value="Submit" /> 
                            <?php } else { ?>
                            <input class="button themed" type="submit" value="Update" /> 
                            <?php } ?>
                            <input type="hidden" name="package_id" id="package_id" value="<?php echo $package_id; ?>" />
                            <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
                                    <input class="button themed" type="button" value="Cancle" onClick="window.location.href='<?php echo site_url('package/list_package');?>'" /> 
                           
                    <!-- End of fieldset --> 
            </form> 
            <!-- End of Form -->	
<!-- End of Form -->	
									
								</div>
		</div>
	</div>
	<div class="clear"></div>
</div>

