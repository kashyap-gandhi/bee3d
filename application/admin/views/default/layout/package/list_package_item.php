<script>	
    function setchecked(elemName,status){
        elem = document.getElementsByName(elemName);
        for(i=0;i<elem.length;i++){
            elem[i].checked=status;
        }
    }

    function setaction(elename, actionval, actionmsg, formname) {
        vchkcnt=0;
        elem = document.getElementsByName(elename);
	
        for(i=0;i<elem.length;i++){
            if(elem[i].checked) vchkcnt++;	
        }
        if(vchkcnt==0) {
            alert('Please select a record')
        } else {
		
            if(confirm(actionmsg))
            {
                document.getElementById('action').value=actionval;	
                document.getElementById(formname).submit();
            }		
		
        }
    }
</script>

<div id="content">        
    <?php
    if ($msg != "") {
        if ($msg == 'active') {
            $error = 'Package Item has been activated successfully.';
        }
        if ($msg == 'inactive') {
            $error = 'Package Item has been inactivated successfully.';
        }
        if ($msg == 'delete') {
            $error = 'Package Item has been deleted successfully.';
        }
        if ($msg == 'insert') {
            $error = 'New Package Item has been added successfully.';
        }
        if ($msg == 'update') {
            $error = 'Package Item has been updated successfully.';
        }
        ?>	





        <div class="column full">
            <span class="message information"><strong><?php echo $error; ?></strong></span>
        </div>
<?php } ?>
    <div class="clear"></div>
    <div class="column full">

        <div class="box">		
            <div class="box themed_box">
                <h2 class="box-header">Package Item</h2>

                <div class="box-content box-table">

                    <form name="frm_listpackageitem" id="frm_listpackageitem" action="<?php echo site_url('package/package_item_action'); ?>" method="post">
                        <input type="hidden" name="action" id="action" />
                        <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />

                        <table class="tablebox">

                            <div id="topbar" style="border:#CCC solid 1px;">



                                <div style="float:right;">


                                    &nbsp;  <?php echo anchor('package/add_package_item', '<span class="icon_text addnew"></span>add new', 'class="button white"  style="margin: 0px;"'); ?>


                                    <a href="javascript:void(0)"  onclick="setaction('chk[]','delete', 'Are you sure, you want to delete selected record(s)?', 'frm_listpackageitem')"  class="button white" ><span class="icon_text cancel"></span>Delete</a>



                                    <a href="javascript:void(0)"  onclick="setaction('chk[]','active', 'Are you sure, you want to active selected record(s)?', 'frm_listpackageitem')" class="button white" >Active</a>

                                    <a href="javascript:void(0)"  onclick="setaction('chk[]','inactive', 'Are you sure, you want to inactive selected record(s)?', 'frm_listpackageitem')" class="button white">Inactive</a>









                                </div>




                            </div>




                            <thead class="table-header">


                                <tr> 


                                    <th class="first tc"><a href="javascript:void(0)" onclick="javascript:setchecked('chk[]',1)" style="color:#000;"><?php echo "Check All"; ?></a>&nbsp; |&nbsp; 
           <a href="javascript:void(0)" onclick="javascript:setchecked('chk[]',0)" style="color:#000;"><?php echo "Clear All"; ?></a></th> 
                                    <th>Name</th> 
                                    <th>Status</th> 
                                    <th>Action</th>




                                </tr>
                            </thead>


                            <tbody class="openable-tbody">
<?php
if ($result) {
    $cnt = 0;
    foreach ($result as $res) {


        if ($cnt % 2 == 0) {
            $cls = 'odd';
        } else {
            $cls = 'even';
        }
        ?>

          
                        
                                        <tr class="<?php echo $cls; ?>"> 
                                            <td class="tc"><input type="checkbox" id='chk' name="chk[]" value="<?php echo $res->package_item_id; ?>" /></td> 
                                            <td class="tooltip" title="Edit"><?php echo anchor('package/edit_package_item/' . $res->package_item_id . '/' . $offset, substr(ucfirst($res->item_name), 0, 25)); ?></td> 

                                            <td><?php if ($res->item_status == 1) { echo "Active";  } else { echo "Inactive";  } ?></td> 
                                            <td><?php echo anchor('package/edit_package_item/' . $res->package_item_id . '/' . $offset, '<span class="icon_single edit"></span>','class="button white"'); ?></td>
                                        </tr> 



        <?php $cnt++;
    } ?>
                                 

<?php } else { ?>
                                     <tr class="alter"><td colspan="9" align="center" valign="middle">No Package Item has been added yet.</td></tr>
<?php } ?>
                            </tbody>
			</table>
				<ul class="pagination">
					<?php echo $page_link; ?>
                </ul>
				</form>
			</div>
		</div>
	</div>
</div>