<script>	
    function setchecked(elemName,status){
        elem = document.getElementsByName(elemName);
        for(i=0;i<elem.length;i++){
            elem[i].checked=status;
        }
    }

    function setaction(elename, actionval, actionmsg, formname) {
        vchkcnt=0;
        elem = document.getElementsByName(elename);
	
        for(i=0;i<elem.length;i++){
            if(elem[i].checked) vchkcnt++;	
        }
        if(vchkcnt==0) {
            alert('Please select a record')
        } else {
		
            if(confirm(actionmsg))
            {
                document.getElementById('action').value=actionval;	
                document.getElementById(formname).submit();
            }		
		
        }
    }
</script>

<div id="content">  
    <?php
    if ($msg != "") {
        if ($msg == 'active') {
            $error = 'Package has been activated successfully.';
        }
        if ($msg == 'inactive') {
            $error = 'Package has been inactivated successfully.';
        }
        if ($msg == 'delete') {
            $error = 'Package has been deleted successfully.';
        }
        if ($msg == 'insert') {
            $error = 'New Package has been added successfully.';
        }
        if ($msg == 'update') {
            $error = 'Package has been updated successfully.';
        }
        ?>	




        <div class="column full">
            <span class="message information"><strong><?php echo $error; ?></strong></span>
        </div>
    <?php } ?>
    <div class="clear"></div>
    <div class="column full">

        <div class="box">		
            <div class="box themed_box">
                <h2 class="box-header">Package</h2>

                <div class="box-content box-table">


                    <form name="frm_listpackage" id="frm_listpackage" action="<?php echo site_url('package/package_action'); ?>" method="post">
                        <input type="hidden" name="action" id="action" />
                        <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />



                        <table class="tablebox">

                            <div id="topbar" style="border:#CCC solid 1px;">



                                <div style="float:right;">


                                    &nbsp;  <?php echo anchor('package/add_package', '<span class="icon_text addnew"></span>add new', 'class="button white"  style="margin: 0px;"'); ?>


                                    <a href="javascript:void(0)"  onclick="setaction('chk[]','delete', 'Are you sure, you want to delete selected record(s)?', 'frm_listpackage')"  class="button white" ><span class="icon_text cancel"></span>Delete</a>



                                    <a href="javascript:void(0)"  onclick="setaction('chk[]','active', 'Are you sure, you want to active selected record(s)?', 'frm_listpackage')" class="button white" >Active</a>

                                    <a href="javascript:void(0)"  onclick="setaction('chk[]','inactive', 'Are you sure, you want to inactive selected record(s)?', 'frm_listpackage')" class="button white">Inactive</a>









                                </div>




                            </div>





                            <thead class="table-header">


                                <tr> 


                                    <th class="first tc"><a href="javascript:void(0)" onclick="javascript:setchecked('chk[]',1)" style="color:#000;"><?php echo "Check All"; ?></a>&nbsp; |&nbsp; 
           <a href="javascript:void(0)" onclick="javascript:setchecked('chk[]',0)" style="color:#000;"><?php echo "Clear All"; ?></a></th> 
                                    <th>Name</th> 

                                    <th>Price</th>
                                    
                                    <th>Pkg. Type</th>
                                    <th>Points</th>
                                    
                                    <th>Free</th>
                                    
                                    <th>Days</th>
                                    <th>Create</th> 


                                    <th>Status</th> 
                                    <th>Action</th>




                                </tr>
                            </thead>


                            <tbody class="openable-tbody">
                                <?php
                                if ($result) {
                                    $cnt = 0;
                                    foreach ($result as $res) {


                                        if ($cnt % 2 == 0) {
                                            $cls = 'odd';
                                        } else {
                                            $cls = 'even';
                                        }
                                        ?>

                                        <tr class="<?php echo $cls; ?>"> 
                                            <td class="first tc"><input type="checkbox" id='chk' name="chk[]" value="<?php echo $res->package_id; ?>" /></td> 
                                            <td class="tooltip" title="Edit"><?php echo anchor('package/edit_package/' . $res->package_id . '/' . $offset, substr(ucfirst($res->package_name), 0, 25)); ?></td> 
                                            <td><?php echo $res->package_price; ?></td> 
                                            
                                            <td><?php if($res->package_type==1){ echo "Buy Points"; } else { echo "Membership"; } ?></td> 
                                            <td><?php echo $res->package_points; ?></td> 
                                            
                                            <td><?php if($res->package_is_free==1){ echo "Yes"; }  ?></td> 
                                            
                                            <td><?php echo $res->package_days; ?></td> 
                                            <td><?php echo date('d M,Y H:i:s', strtotime($res->package_date)); ?></td> 
                                            <td><?php if ($res->package_status == 1) {
                                    echo "Active";
                                } else {
                                    echo "Inactive";
                                } ?></td> 
                                            <td><?php echo anchor('package/edit_package/' . $res->package_id . '/' . $offset, '<span class="icon_single edit"></span>', 'class="button white"'); ?></td>
                                        </tr> 



                                        <?php $cnt++;
                                    } ?>


<?php } else { ?>
                                    <tr class="alter"><td colspan="12" align="center" valign="middle">No Package has been added yet.</td></tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <ul class="pagination">
<?php echo $page_link; ?>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>