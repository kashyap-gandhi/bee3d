<div id="content" align="center">

 	<?php if($error!=''){ ?>
		<div class="column full">
			<span class="message information"><strong><?php echo $error;?></strong></span>
		</div>
    <?php }?>

	<div align="left" class="column half">
		<div class="box">
			<h2 class="box-header">Yahoo Settings </h2> 
			<div class="box-content">	
			  <?php
				$attributes = array('name'=>'frm_yahoo_setting');
				echo form_open('yahoo_setting/add_yahoo_setting',$attributes);
			  ?>
			  
			  	  <label class="form-label">Yahoo Enabled </label>
				  <select name="yahoo_enable" id="yahoo_enable" class="form-field settingselectbox required">
				  	<option value="0" <?php if($yahoo_enable == '0'){	echo 'selected="selected"';	} ?> >No</option>
					<option value="1" <?php if($yahoo_enable == '1'){	echo 'selected="selected"';	} ?>>Yes</option>
				  </select>
				  					
				 
									
				  <label class="form-label">Consumer Key </label> 
				  <input type="text" name="consumer_key" id="consumer_key" value="<?php echo $consumer_key; ?>" class="form-field width40"/>
				  
				  <label class="form-label">Consumer Secret  </label> 
				  <input type="text" name="consumer_secret" id="consumer_secret" value="<?php echo $consumer_secret; ?>" class="form-field width40"/>
				  
				  <label class="form-label">&nbsp;</label> 
				  <input type="hidden" name="yahoo_setting_id" id="yahoo_setting_id" value="<?php echo $yahoo_setting_id; ?>" />
				  <input type="submit" class="button themed" name="submit" value="Update" onclick=""/>
				  
			  </form>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>