<div id="content" align="center">

 	<?php if($error!=''){ ?>
		<div class="column full">
			<span class="message information"><strong><?php echo $error;?></strong></span>
		</div>
    <?php }?>

	<div align="left" class="column half">
		<div class="box">
			<h2 class="box-header">Commission Settings </h2> 
			<div class="box-content">	
			  <?php
				$attributes = array('name'=>'frm_commission_setting');
				echo form_open_multipart('site_setting/commission_setting',$attributes);
			  ?>					

				  <label class="form-label">Design Commission </label>
				   <input type="text" name="admin_commission_image" id="admin_commission_image" value="<?php echo $admin_commission_image; ?>" class="form-field width40"/>
				  
				  <label class="form-label">Video Commission </label>
				  <input type="text" name="admin_commission_video" id="admin_commission_video" value="<?php echo $admin_commission_video; ?>" class="form-field width40"/>
                  
                   <label class="form-label">Challenge Commission  </label>
				  <input type="text" name="admin_commission_challange" id="admin_commission_challange" value="<?php echo $admin_commission_challange; ?>" class="form-field width40"/>
                  
                  
            
				  <label class="form-label">&nbsp;</label>
				  <input type="hidden" name="commission_id" id="commission_id" value="<?php echo $commission_id; ?>" />
				  <input type="submit" class="button themed" name="submit" value="Update" onclick=""/>
				  
			  </form>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>