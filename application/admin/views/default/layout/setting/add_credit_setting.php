<div id="content" align="center">

 	<?php if($error!=''){ ?>
		<div class="column full">
			<span class="message information"><strong><?php echo $error;?></strong></span>
		</div>
    <?php }?>

	<div align="left" class="column half">
		<div class="box">
			<h2 class="box-header">Credit Settings </h2> 
			<div class="box-content">	
			  <?php
				$attributes = array('name'=>'frm_credit_setting');
				echo form_open_multipart('site_setting/credit_setting',$attributes);
			  ?>					

				  <label class="form-label">Point to Paisa Conversation Rate :  (1 point = <?php echo $point2paisa.' '.$site_setting->currency_code; ?>) </label>
				   <input type="text" name="point2paisa" id="point2paisa" value="<?php echo $point2paisa; ?>" class="form-field width40"/>
				  
			
            
				  <label class="form-label">&nbsp;</label>
				  <input type="hidden" name="credit_setting_id" id="credit_setting_id" value="<?php echo $credit_setting_id; ?>" />
				  <input type="submit" class="button themed" name="submit" value="Update" onclick=""/>
				  
			  </form>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>