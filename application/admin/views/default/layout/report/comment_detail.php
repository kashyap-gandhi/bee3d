
<div id="content" align="center">

	<div align="left" class="column half">
		<div class="box">	
            <h2 class="box-header">Report Details</h2> 
			<div class="box-content">
			
			<?php
			$image_id=$result->image_id;
			
			$image_detail=get_image_details($image_id);
			
			$album_detail=get_image_album_details_by_id($image_id);
			
			
			$comment_detail=get_comment_by_id($result->comment_id);
			
			
			?>
			 
            
			  <table class="tablebox">
                  <tbody class="openable-tbody">
                  
                 
                 <?php if($image_detail) {
				 
						
						$show_image='';
						
						
						if($image_detail->image_name!='') {
																																
							$orig_name=$image_detail->image_name;							
							
									 
							if(file_exists(base_path().'upload/image_thumb/'.$orig_name)) { 
								$show_image=upload_url().'upload/image_thumb/'.$orig_name;
							}
							else if(file_exists(base_path().'upload/image_medium/'.$orig_name)) { 
								$show_image=upload_url().'upload/image_medium/'.$orig_name;
							}
							else
							{				
								if(file_exists(base_path().'upload/image_orig/'.$orig_name)) {									
									$show_image=upload_url().'upload/image_orig/'.$orig_name;									
								}
							}
													  
					}
			
			
		
			
			
				$image_user_profileimage=upload_url().'upload/no_image.png';
					
				if($image_detail->profile_image!='') {  
				
					if(file_exists(base_path().'upload/user_thumb/'.$image_detail->profile_image)) { 							
						$image_user_profileimage=upload_url().'upload/user_thumb/'.$image_detail->profile_image;
					} else {
						
						if(file_exists(base_path().'upload/user/'.$image_detail->profile_image)) { 							
							$image_user_profileimage=upload_url().'upload/user/'.$image_detail->profile_image;
						}
					} 
				 } 
				 
				 
				  ?> 
                      <tr><td colspan="2"><h2>Image Detail</h2><hr/></td></tr>
                     
                     
                     <tr><td colspan="2" align="left" valign="top">
                      
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      
                      <tr>
                      
                     	 <td align="left" valign="top" width="150">
                        
                        <p>Image</p>
                          <img src="<?php echo $show_image;?>" style="border-radius:5px; width:120px; height:120px;"/><br /><br />

<p>User Image</p>
                        
                          <img src="<?php echo $image_user_profileimage;?>" style="border-radius:5px; max-width:120px; max-height:120px;"/>
                         
                         
                          
                          </td>
                          
                          <td align="left" valign="top">
                      
                       <table border="0" cellpadding="0" cellspacing="0" width="70%">
                       
                       <?php if($album_detail) {?>
                       
                       <tr>
                          <td style="text-align:left; width:35%" align="left" valign="top"><label class="form-label">Album Name </label></td>
                          <td style="text-align:left; width:65%">
						  
                          <table cellpadding="2" cellspacing="0" border="0" style="padding:0px ; margin:0px;">
                          
                          
						  <?php foreach($album_detail as $alb) {  ?>
                          
						  <tr><td align="left" valign="top"> : <?php  echo anchor(front_base_url().'a/'.$alb->album_unique_code,ucfirst($alb->album_name).'('.$alb->album_unique_code.')',' style="color:#004C7A;" target="_blank"'); ?></td></tr>
                          
						
						
						<?php } ?>
                         </table>
                         
                         </td>
                      </tr>
                      
                      <?php } ?>
                      
                       
                       
                        <tr>
                          <td style="text-align:left;"><label class="form-label">Image Unique Code </label></td>
                          <td style="text-align:left;">: <?php echo anchor(front_base_url().$image_detail->image_unique_code,'<b>'.$image_detail->image_unique_code.'</b>',' style="color:#004C7A;" target="_blank"'); ?></td>
                      </tr>
                      
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Image Delete Code </label></td>
                          <td style="text-align:left;">: <?php echo $image_detail->image_delete_code;?></td>
                      </tr>
                      
                      
                       <?php if($image_detail->image_title!='') { ?>
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Image Title </label></td>
                          <td style="text-align:left;">: <?php echo $image_detail->image_title;?></td>
                      </tr>
                      <?php } ?>
                      
                      <?php if($image_detail->image_description!='') { ?>
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Image Description </label></td>
                          <td style="text-align:left;">: <?php echo $image_detail->image_description;?></td>
                      </tr>
                      <?php } ?>
                      
                      
                      
                      
                      <?php if($image_detail->profile_name!='') { ?>
					  
                       <tr>
                          <td style="text-align:left; width:35%"><label class="form-label">User Name </label></td>
                          <td style="text-align:left; width:65%">: <?php echo anchor(front_base_url().'user/'.$image_detail->profile_name,ucfirst($image_detail->profile_name),' style="color:#004C7A;" target="_blank"'); ?></td>
                      </tr>
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Firstname </label></td>
                          <td style="text-align:left;">: <?php echo $image_detail->first_name;?></td>
                      </tr>
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Lastname </label></td>
                          <td style="text-align:left;">: <?php echo $image_detail->last_name;?></td>
                      </tr>
                      
                        <tr>
                          <td style="text-align:left;"><label class="form-label">Email </label></td>
                          <td style="text-align:left;">: <?php echo $image_detail->email;?></td>
                      </tr>
                      
                      <?php } ?>
                      
                      
                      
                      
                      
                     
                      
                    
                      </table>
                     
                      
                    	  </td>
                      
                      </tr>
                      </table>
                      
                      
                      </td></tr>
                      
                    <?php }  if($comment_detail) {
					
					
					$comment_user_profileimage=upload_url().'upload/no_image.png';
					
				if($comment_detail->profile_image!='') {  
				
					if(file_exists(base_path().'upload/user_thumb/'.$comment_detail->profile_image)) { 							
						$comment_user_profileimage=upload_url().'upload/user_thumb/'.$comment_detail->profile_image;
					} else {
						
						if(file_exists(base_path().'upload/user/'.$comment_detail->profile_image)) { 							
							$comment_user_profileimage=upload_url().'upload/user/'.$comment_detail->profile_image;
						}
					} 
				 } 
				 
				 
				   ?>  
                        <tr><td colspan="2"><hr/></td></tr>
                       <tr><td colspan="2"><h2>Comment User Detail</h2><hr/></td></tr>
                     
                     
                     <tr><td colspan="2" align="left" valign="top">
                      
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      
                      <tr>
                      
                     	 <td align="left" valign="top" width="150">
                        <p>Comment User Image</p>
                                 <img src="<?php echo $comment_user_profileimage;?>" style="border-radius:5px; max-width:120px; max-height:120px;"/>
                         
                         
                          
                          </td>
                          
                          <td align="left" valign="top">
                      
                       <table border="0" cellpadding="0" cellspacing="0" width="70%">
                       
                       <tr>
                          <td style="text-align:left; width:35%"><label class="form-label">User Name </label></td>
                          <td style="text-align:left; width:65%">: <?php echo anchor(front_base_url().'user/'.$comment_detail->profile_name,ucfirst($comment_detail->profile_name),' style="color:#004C7A;" target="_blank"'); ?></td>
                      </tr>
                      
                      
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Firstname </label></td>
                          <td style="text-align:left;">: <?php echo $comment_detail->first_name;?></td>
                      </tr>
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Lastname </label></td>
                          <td style="text-align:left;">: <?php echo $comment_detail->last_name;?></td>
                      </tr>
                      
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Email </label></td>
                          <td style="text-align:left;">: <?php echo $comment_detail->email;?></td>
                      </tr>
                      
                      <tr>
                          
                      </tr>
                      
                      </table>
                     
                      
                    	  </td>
                      
                      </tr>
                      </table>
                      
                      
                      </td></tr>
                      
                    
                     <?php }  
					
					
					$report_user_profileimage=upload_url().'upload/no_image.png';
					
				if($result->profile_image!='') {  
				
					if(file_exists(base_path().'upload/user_thumb/'.$result->profile_image)) { 							
						$report_user_profileimage=upload_url().'upload/user_thumb/'.$result->profile_image;
					} else {
						
						if(file_exists(base_path().'upload/user/'.$result->profile_image)) { 							
							$report_user_profileimage=upload_url().'upload/user/'.$result->profile_image;
						}
					} 
				 } 
				 
				 
				   ?>  
                   
                   
                   
                      <?php if($result->profile_name!='') { ?>
                        <tr><td colspan="2"><hr/></td></tr>
                       <tr><td colspan="2"><h2>Report User Detail</h2><hr/></td></tr>
                     
                     
                     <tr><td colspan="2" align="left" valign="top">
                      
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      
                      <tr>
                      
                     	 <td align="left" valign="top" width="150">
                        <p>Report User Image</p>
                                 <img src="<?php echo $report_user_profileimage;?>" style="border-radius:5px;max-width:120px; max-height:120px;"/>
                         
                         
                          
                          </td>
                          
                          <td align="left" valign="top">
                      
                       <table border="0" cellpadding="0" cellspacing="0" width="70%">
                 
                    
                      
                       <tr>
                          <td style="text-align:left; width:35%"><label class="form-label">User Name </label></td>
                          <td style="text-align:left; width:65%">: <?php echo anchor(front_base_url().$result->profile_name,ucfirst($result->profile_name),' style="color:#004C7A;" target="_blank"'); ?></td>
                      </tr>
                      
                      
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Firstname </label></td>
                          <td style="text-align:left;">: <?php echo $result->first_name;?></td>
                      </tr>
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Lastname </label></td>
                          <td style="text-align:left;">: <?php echo $result->last_name;?></td>
                      </tr>
                      
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Email </label></td>
                          <td style="text-align:left;">: <?php echo $result->email;?></td>
                      </tr>
                      
                   
                      
                      </table>
                     
                      
                    	  </td>
                      
                      </tr>
                      </table>
                     
                      
                      </td></tr>
                      
                      <?php } ?>
                      
                      
                      
                       <tr><td colspan="2"><hr/></td></tr>
                      
                   
                      
                      
                          <tr>
                          <td style="text-align:left;"><label class="form-label">Report IP </label></td>
                          <td style="text-align:left;">: <?php echo $result->report_ip;?></td>
                      </tr>
                      
                          <tr>
                          <td style="text-align:left;"><label class="form-label">Report Date </label></td>
                          <td style="text-align:left;">: <?php echo date($site_setting->date_time_format,strtotime($result->report_date));?></td>
                      </tr>
                      
                      
                      	 <?php if($result->report_message!='') { ?>
                        <tr>
                          <td style="text-align:left;"><label class="form-label">Message </label></td>
                          <td style="text-align:left;">: <?php echo $result->report_message;?></td>
                      </tr>
                     
                     <?php }  ?> 
                   
                        <tr><td colspan="2"><hr/></td></tr>
                     <?php
					 
					 if($comment_detail) { ?> 
                     
                     
                      <?php
			    $attributes = array('name'=>'frm_category');
				echo form_open('report/comment_detail/'.$report_id,$attributes);
				?>
                      
                     
                       
                       <tr>
                         
                          <td style="text-align:left; width:30%;"><label class="form-label">User Comment</label></td>
                          </tr><tr><td><textarea name="user_comment" id="user_comment" style="resize:none;" cols="50" rows="5"><?php echo $comment_detail->user_comment;?></textarea>                 </td></tr><tr> <td> 
                          <input type="submit" name="submit" id="submit" value="update" class="button white" />
                      </td>
                          
                          </td>
                      </tr>
                      
                  	  
                  
                      <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
          <input type="hidden" name="limit" id="limit" value="<?php echo $limit; ?>" />
                       <input type="hidden" name="image_comment_id" id="image_comment_id" value="<?php echo $comment_detail->image_comment_id;?>" />
                       </form>
                       
                       
                       <?php } ?>
                       
                       
                  </tbody>
			  	 </table>   
             </div>
              


			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>