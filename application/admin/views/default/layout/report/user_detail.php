
<div id="content" align="center">

	<div align="left" class="column half">
		<div class="box">	
            <h2 class="box-header">Report Details</h2> 
			<div class="box-content">
			
			<?php
			 $user_id=$result->user_id;		
			$user_detail=get_user_profile_by_id($user_id);
			
			$report_user_detail=get_user_profile_by_id($result->report_by_user_id);
					
			?>
			 
            
			  <table class="tablebox">
                  <tbody class="openable-tbody">
                  
                 
                 <?php if($user_detail) {
				 
				 
					  		
						
			
				$image_user_profileimage=upload_url().'upload/no_image.png';
					
				if($user_detail->profile_image!='') {  
				
					if(file_exists(base_path().'upload/user_thumb/'.$user_detail->profile_image)) { 							
						$image_user_profileimage=upload_url().'upload/user_thumb/'.$user_detail->profile_image;
					} else {
						
						if(file_exists(base_path().'upload/user/'.$user_detail->profile_image)) { 							
							$image_user_profileimage=upload_url().'upload/user/'.$user_detail->profile_image;
						}
					} 
				 } 
				 
				 
				  ?> 
                     <tr><td colspan="2"><h2>User Detail</h2><hr/></td></tr>
                     
                     
                     <tr><td colspan="2" align="left" valign="top">
                      
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      
                      <tr>
                      
                     	 <td align="left" valign="top" width="150">
                        
                        

<p>User Image</p>
                        
                          <img src="<?php echo $image_user_profileimage;?>" style="border-radius:5px; max-width:120px; max-height:120px;"/>
                         
                         
                          
                          </td>
                          
                          <td align="left" valign="top">
                      
                       <table border="0" cellpadding="0" cellspacing="0" width="70%">
                       
                     
                      
                      <?php if($user_detail->profile_name!='') { ?>
					  
                       <tr>
                          <td style="text-align:left; width:35%"><label class="form-label">User Name </label></td>
                          <td style="text-align:left; width:65%">: <?php echo anchor(front_base_url().'user/'.$user_detail->profile_name,ucfirst($user_detail->profile_name),' style="color:#004C7A;" target="_blank"'); ?></td>
                      </tr>
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Firstname </label></td>
                          <td style="text-align:left;">: <?php echo $user_detail->first_name;?></td>
                      </tr>
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Lastname </label></td>
                          <td style="text-align:left;">: <?php echo $user_detail->last_name;?></td>
                      </tr>
                      
                        <tr>
                          <td style="text-align:left;"><label class="form-label">Email </label></td>
                          <td style="text-align:left;">: <?php echo $user_detail->email;?></td>
                      </tr>
                      
                     
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Total Album </label></td>
                          <td style="text-align:left;">: <?php echo get_user_total_album($user_detail->user_id);?></td>
                      </tr>
                      <tr>
                          <td style="text-align:left;"><label class="form-label">Total Images </label></td>
                          <td style="text-align:left;">: <?php echo get_user_total_images($user_detail->user_id);?></td>
                      </tr>
                      <tr>
                          <td style="text-align:left;"><label class="form-label">Total Like</label></td>
                          <td style="text-align:left;">: <?php echo get_user_total_image_like($user_detail->user_id);?></td>          
                      </tr>
                     
                      <tr>
                          <td style="text-align:left;"><label class="form-label">Total Usage</label></td>
                          <td style="text-align:left;">: <?php echo get_user_total_usage($user_detail->user_id);?> MB</td>          
                      </tr>
                      
                        <tr>
                          <td >&nbsp;</td><td>
                          <?php echo anchor('user_album/list_album/'.$user_detail->user_id,'Album','class="button white" style="margin: 0px;" id="List Album" title="List Album" target="_blank"'); ?>
                          
                          <?php echo anchor('user_image/list_images/'.$user_detail->user_id,'Image','class="button white" style="margin: 0px;" id="List Image" title="List Image" target="_blank"'); ?>
                           </td>
                      </tr>
                      
                      <?php } ?>
                      
                      
                      
                      
                      
                     
                      
                    
                      </table>
                     
                      
                    	  </td>
                      
                      </tr>
                      </table>
                      
                      
                      </td></tr>
                      
                    <?php } 
					
					
					
					
					if($report_user_detail) {
					
					$report_user_profileimage=upload_url().'upload/no_image.png';
					
				if($report_user_detail->profile_image!='') {  
				
					if(file_exists(base_path().'upload/user_thumb/'.$report_user_detail->profile_image)) { 							
						$report_user_profileimage=upload_url().'upload/user_thumb/'.$report_user_detail->profile_image;
					} else {
						
						if(file_exists(base_path().'upload/user/'.$report_user_detail->profile_image)) { 							
							$report_user_profileimage=upload_url().'upload/user/'.$report_user_detail->profile_image;
						}
					} 
				 } 
				 
				 
				   ?>  
                     
                     
                     <?php  ?>
                      
                        <tr><td colspan="2"><hr/></td></tr>
                       <tr><td colspan="2"><h2>Report User Detail</h2><hr/></td></tr>
                     
                     
                     <tr><td colspan="2" align="left" valign="top">
                      
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      
                      <tr>
                      
                     	 <td align="left" valign="top" width="150">
                        <p>Report User Image</p>
                                 <img src="<?php echo $report_user_profileimage;?>" style="border-radius:5px;max-width:120px; max-height:120px;"/>
                         
                         
                          
                          </td>
                          
                          <td align="left" valign="top">
                      
                       <table border="0" cellpadding="0" cellspacing="0" width="70%">
                      
                
                      
                      
                       <tr>
                          <td style="text-align:left; width:35%"><label class="form-label">User Name </label></td>
                          <td style="text-align:left; width:65%">: <?php echo anchor(front_base_url().'user/'.$report_user_detail->profile_name,ucfirst($report_user_detail->profile_name),' style="color:#004C7A;" target="_blank"'); ?></td>
                      </tr>
                      
                      
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Firstname </label></td>
                          <td style="text-align:left;">: <?php echo $report_user_detail->first_name;?></td>
                      </tr>
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Lastname </label></td>
                          <td style="text-align:left;">: <?php echo $report_user_detail->last_name;?></td>
                      </tr>
                      
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Email </label></td>
                          <td style="text-align:left;">: <?php echo $report_user_detail->email;?></td>
                      </tr>
                      
                     
                      </table>
                     
                      
                    	  </td>
                      
                      </tr>
                      </table>
                      
                      
                      </td></tr>
                      
                     
                     <?php } ?> 
                   
                      <tr><td colspan="2"><hr/></td></tr>
                      
                   
                      
                      
                          <tr>
                          <td style="text-align:left;"><label class="form-label">Report IP </label></td>
                          <td style="text-align:left;">: <?php echo $result->report_ip;?></td>
                      </tr>
                      
                          <tr>
                          <td style="text-align:left;"><label class="form-label">Report Date </label></td>
                          <td style="text-align:left;">: <?php echo date($site_setting->date_time_format,strtotime($result->report_date));?></td>
                      </tr>
                      
                      
                      	 <?php if($result->report_message!='') { ?>
                        <tr>
                          <td style="text-align:left;"><label class="form-label">Message </label></td>
                          <td style="text-align:left;">: <?php echo $result->report_message;?></td>
                      </tr>
                     
                     <?php }  ?> 
                      
                  
                       
                  </tbody>
			  	 </table>   
             </div>
              


			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>