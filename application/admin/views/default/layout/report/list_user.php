<script type="text/javascript" language="javascript">

	
	
	function getlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			window.location.href='<?php echo site_url('report/report_user');?>/'+limit;
		}
	
	}
	
	function getsearchlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			
			window.location.href='<?php echo site_url('report/search_report_user');?>/'+limit+'/<?php echo $option.'/'.$keyword; ?>';
		}
	
	}
	
	function gomain(x)
	{
		
		if(x == 'all')
		{
			window.location.href= '<?php echo site_url('report/report_user');?>';
		}
		
		
	}
	
	function setchecked(elemName,status)
	{
		elem = document.getElementsByName(elemName);
		for(i=0;i<elem.length;i++){
			elem[i].checked=status;
		}
	}

	function setaction(elename, actionval, actionmsg, formname) 
	{
		vchkcnt=0;
		elem = document.getElementsByName(elename);
		
		for(i=0;i<elem.length;i++){
			if(elem[i].checked) vchkcnt++;	
		}
		if(vchkcnt==0) {
			alert('Please select a record')
		} else {
			
			if(confirm(actionmsg))
			{
				document.getElementById('action').value=actionval;	
				document.getElementById(formname).submit();
			}		
			
		}
	}

	function chk_valid()
	{
		
		var keyword = document.getElementById('keyword').value;
		
		if(keyword=='')
		{
			alert('Please enter search keyword');	
			return false;
			
		}
		
		else
		{
			return true;			
		}
		
		
		
	}
</script>
<div id="content">  
	<?php if($msg != ""){ $error ='';
            if($msg == "insert"){ $error = 'New Record has been added Successfully.';}
            if($msg == "update"){ $error = 'Record has been updated Successfully.';}
            if($msg == "delete"){ $error = 'Record has been deleted Successfully.';}
    ?>
        <div class="column full">
            <span class="message information"><strong><?php echo $error;?></strong></span>
        </div>
    <?php } ?>
	<div class="clear"></div>
	<div class="column full">
	
	<div class="box">		
		<div class="box themed_box">
		<h2 class="box-header">User</h2>
			
			<div class="box-content box-table">
			<table class="tablebox" >  
				
                
                <div id="topbar" style="border:#CCC solid 1px;">
                    <div style="float:left;">
                
                
                <strong>Show</strong>
                            <?php if($search_type=='normal') { ?>
                            <select name="limit" id="limit" onchange="getlimit(this.value)" style="width:80px;">
                            <?php } if($search_type=='search') { ?>
                             <select name="limit" id="limit" onchange="getsearchlimit(this.value)" style="width:80px;">
                            <?php } ?>
                                <option value="0">Per Page</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="75">75</option>
                                <option value="100">100</option>
                            </select>
                            
                            
                      </div>
                    
                   
                   
                     
			 
			 <div style="float:right;"> 
     <form name="frm_reportuser" id="frm_reportuser" action="<?php echo site_url('report/action_report_user');?>" method="post">
          <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
          <input type="hidden" name="limit" id="limit" value="<?php echo $limit; ?>" />
          <input type="hidden" name="option" id="option" value="<?php echo $option; ?>" />
          <input type="hidden" name="keyword" id="keyword" value="<?php echo $keyword; ?>" />
          <input type="hidden" name="search_type" id="search_type" value="<?php echo $search_type; ?>" />
          
           <input type="hidden" name="action" id="action" />
				
						
               
               <a href="javascript:void(0)"  onclick="setaction('chk[]','delete_report', 'Are you sure, you want to delete selected Report(s)?', 'frm_reportuser')"  class="button white" ><span class="icon_text cancel"></span>Delete Report</a>
                  
                  
                  
                  <a href="javascript:void(0)"  onclick="setaction('chk[]','delete_user', 'Are you sure, you want to delete selected User(s), It will delete all user data but do not free user email and unique name?', 'frm_reportuser')" class="button white" ><span class="icon_text cancel"></span>Delete User</a>
                  
               
                  
                 
                 </div>
                 </div>
                 
                 
                 
          		<thead class="table-header">
					<tr> 
                    <tr> 
						 <th class="first tc"><a href="javascript:void(0)" onClick="javascript:setchecked('chk[]',1)" style="color:#000;"><?php echo "Check All"; ?></a> |
           <a href="javascript:void(0)" onClick="javascript:setchecked('chk[]',0)" style="color:#000;"><?php echo "Clear All"; ?></a></th>
           
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Total Images</th>
                        <th>Total Usage(MB)</th>
                        <th>Registered On</th>
                        
                        <th>Report User</th>
                        <th>Email</th>
                        <th>Report Date</th>   
                        <th>Report ip</th>                                     
                       
                        <th>View Detail</th>      
                    
                       
                        
                    	
                                                        
                    </tr>
                </thead>
                
                <tbody class="openable-tbody">
				<?php
                    if($result)
                    {
                        $i=0;
                        foreach($result as $row)
                        {
                            if($i%2=="0")
                            {
                                $cl = "odd";	
                            }else{	
                                $cl = "even";	
                            }
                  ?>
					<tr class="<?php echo $cl; ?>">
                   		 
                        <td style="width:140px;"><input type="checkbox" name="chk[]" id="chk" value="<?php echo $row->report_id;?>" /></td>
                         <td><a href="<?php echo front_base_url().'user/'.$row->profile_name; ?>" target="_blank" style="text-transform:capitalize;"><b><?php echo $row->first_name.' '.$row->last_name; ?></b></a></td>
                         
                         
                        <td><?php echo $row->email; ?><?php if($row->verify_email==1) { ?> 
                        
                        <img src="<?php echo base_url().getThemeName();?>/gfx/accept.png" border="0" />
                         <?php } else { ?>
                         
                             <img src="<?php echo base_url().getThemeName();?>/gfx/cancel.png" border="0" /> 
                         <?php } ?></td>
                       <td><?php echo get_user_total_images($row->user_id); ?></td>
                    <td><?php echo get_user_total_usage($row->user_id); ?></td>
                    
                    
                    <td><?php echo date($site_setting->date_time_format,strtotime($row->sign_up_date)); ?></td>
                       
                        
                         <td><a href="<?php echo front_base_url().'user/'.$row->report_profile_name; ?>" target="_blank"><b><?php echo $row->report_first_name.' '.$row->report_last_name;?></b></a></td>
                        
                        <td><?php echo $row->report_email;?></td>
                        <td><?php echo date($site_setting->date_time_format,strtotime($row->report_date));?></td>
                        <td><?php echo $row->report_ip;?></td>
                        
                        <td><?php echo anchor('report/user_detail/'.$row->report_id,'View Detail','class="button white"'); ?></td>
                      
                       
                    
                       
                        	
							
							
                    </tr>
					<?php
							$i++;
							}
						} else { ?>
						<tr class="alter"><td colspan="15" align="center" valign="middle" height="30">No User has been reported yet</td></tr>
					<?php 	}
					?> 
                  	</tr>
                </tbody>
                </table>
                <ul class="pagination">
					<?php echo $page_link; ?>
                </ul>

           </div>
       </div>
    </div>
</div>