<script type="text/javascript" language="javascript">
	function delete_rec(id,offset)
	{
		var ans = confirm("Are you sure to delete this image?");
		if(ans)
		{
			location.href = "<?php echo site_url('/gallery/delete_gallery'); ?>/"+id+"/"+offset;
		}else{
			return false;
		}
	}
</script>

<div id="content">        
	<?php if($msg != ""){
            if($msg == "insert"){ $error = 'New Record has been added Successfully.';}
            if($msg == "update"){ $error = 'Record has been updated Successfully.';}
            if($msg == "delete"){ $error = 'Record has been deleted Successfully.';}
    ?>
        <div class="column full">
            <span class="message information"><strong><?php echo $error;?></strong></span>
        </div>
    <?php } ?>
	<div class="clear"></div>
	<div class="column full">
	
	<div class="box">		
		<div class="box themed_box">
		<h2 class="box-header">Gallery</h2>
			
			<div class="box-content box-table">
			<table class="tablebox">
            
                <div id="topbar" style="border:#CCC solid 1px;">
                    <div style="float:left;">
                
                      </div>
                    
                  
			 
			 <div style="float:right;"> 
              
          <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
           <input type="hidden" name="action" id="action" />
				 &nbsp;<?php echo anchor('gallery/add_gallery','<span class="icon_text addnew"></span>Add Gallery','class="button white" style="margin: 0px;" id="Add Gallery" title="Add Gallery"'); ?>
						 <script>
							//jQuery("#AddNewsletter").fancybox();
						</script>
               
             
                 
                 </div>
                 </div>

				<thead class="table-header">
				
		  
					<tr> 
						<th class="first tc">Gallery title</th>
                        <th>Image</th>
                           <th>Order</th>
                                        
						
                          <th>Status</th>                                    
                        <th>Action</th>
                        
                      </tr>
				</thead>
				
				
				<tbody class="openable-tbody">
				<?php
                   	if($result)
									{
										$i=0;
										foreach($result as $row)
										{
											 if($i%2=="0")
                                             {
                                                $cl = "odd";	
                                               }else{	
                                               $cl = "even";	
                                              }
								  ?>
					<tr onclick="<?php echo $cl; ?>(this);" class="<?php echo $cl; ?>">
                    
                    <td><?php echo $row->gallery_title; ?></td>
                            <td  class="thumb-td tc">
                            
                              <a class="lightbox" href="<?php echo upload_url();?>upload/gallery_orig/<?php echo $row->gallery_image;?>" title="<?php echo $row->gallery_image;?>"><img src="<?php echo upload_url();?>upload/gallery/<?php echo $row->gallery_image;?>" alt="image-gallery"/></a></td>
                       
                      
                                    
                                    
                                      <td align="center" valign="middle"><?php echo $row->gallery_order; ?></td>
                                      
                                      
                       <td><?php if($row->gallery_status=="1"){ echo "Active"; }else{ echo "Inactive"; } ?></td>
                      
					  
					   <td><?php echo anchor('gallery/edit_gallery/'.$row->gallery_id.'/'.$offset,'<span class="icon_single edit"></span>','class="button white" id="cat_'.$row->gallery_id.'" title="Edit Gallery"'); ?>
					   
					   
                       
                        
					<a href="#" onClick="delete_rec('<?php echo $row->gallery_id; ?>','<?php echo $offset; ?>')" class="button white"><span class="icon_single cancel"></span></a>
                    
                    </td>
                      
                  	</tr>
				  <?php
                            $i++;
                        }
                    }
                   else {
								  ?>        
								  
								  <tr class="alter">
                                    <td colspan="15" align="center" valign="middle" height="30">No Records found.</td></tr>
								  
								  <?php } ?>  
				</tbody>
			</table>
				
                <ul class="pagination">
					<?php echo $page_link; ?>
                </ul>
				</form>
			</div>
		</div>
	</div>
</div>