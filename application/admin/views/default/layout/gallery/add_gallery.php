<div id="content" align="center">

 	<?php if($error!=''){ ?>
		<div class="column full">
			<span class="message information"><strong><?php echo $error;?></strong></span>
		</div>
    <?php }?>

	<div align="left" class="column half">
		<div class="box">
			<h2 class="box-header">Gallery detail</h2> 
			<div class="box-content">
			
			  <?php
			    $attributes = array('name'=>'frm_category');
				echo form_open_multipart('gallery/add_gallery',$attributes);
				?>
			  <label class="form-label">Gallery Title</label>
			<input type="text" name="gallery_title" id="gallery_title" class="form-field width40" value="<?php echo $gallery_title;?>" />
			  
			  	  
			<br />
		
			
			<label class="form-label">Gallery Image </label>
			<input type="file" name="file_up" id="file_up" class="form-field width40" value="" />
			
			<input type="hidden" name="prev_gallery_image" id="prev_gallery_image" class="form-field width40" value="<?php echo $gallery_image;?>" />
         
          
          <?php if($gallery_id!="") {?>
          	<a class="lightbox" href="<?php echo upload_url();?>upload/gallery_orig/<?php echo $gallery_image;?>" title="<?php echo $gallery_image;?>"><img src="<?php echo upload_url();?>upload/gallery/<?php echo $gallery_image;?>" alt="image-gallery"/><br/><b><?php echo $gallery_image;?></b></a><br />
          <?php } ?>
          
          <label class="form-label">Gallery Order</label>
          <input type="text" name="gallery_order" id="gallery_order" class="form-field width40" value="<?php echo $gallery_order;?>" />
			
		    <label class="form-label">Status </label>
				 <select name="gallery_status" id="gallery_status" class="form-field settingselectbox required">
						<option value="0" <?php if($gallery_status  == 0){ ?> selected="selected" <?php } ?>> Inactive</option>
						<option value="1" <?php if($gallery_status  == 1){ ?> selected="selected" <?php } ?>> Active</option>	 	  				 												
				  </select>
                 
				  									
				
				  <label class="form-label">&nbsp;</label>
				  <input type="hidden" name="gallery_id" id="gallery_id" value="<?php echo $gallery_id; ?>" />
                  <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
				  <?php if($gallery_id!="") {?>
				  
                   <input type="submit" class="button themed" name="submit" id="submit" value="Update" />
				  <?php } else  {?>
				   <input type="submit" class="button themed" name="submit" id="submit" value="Submits" />
				          
				  <?php }?>
			  </form>

			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>