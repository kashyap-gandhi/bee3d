<div id="content" align="center">

 	<?php  
		if($error != "") {
			
			if($error == 'insert') {
				echo '<div class="column full"><span class="message information"><strong>Record has been updated Successfully.</strong></span></div>';
			}
		
			if($error != "insert"){	
				echo '<div class="column full"><span class="message information"><strong>'.$error.'</strong></span></div>';	
			}
		}
	?>		

	<div align="left" class="column half">
		<div class="box">
			<h2 class="box-header"><?php if($deal_id==""){ echo 'Add Deal'; } else { echo 'Edit Deal'; }?> </h2> 
			<div class="box-content">	
			  <?php
				$attributes = array('name'=>'frm_adddeal');
				echo form_open_multipart('deal/add_deal',$attributes);
			  ?>					
				  <label class="form-label">Deal Title </label> 
				  <textarea name="deal_title" id="deal_title" style="width:355px; height:95px;"><?php echo $deal_title; ?></textarea>
                
                   <label class="form-label">Deal Amount </label>
                  <input type="text" name="deal_amount" id="deal_amount" value="<?php echo $deal_amount; ?>" class="form-field width40" style="width:39%;padding: 2px 0 2px 5px;"/> 
                  
                  
                     <label class="form-label">Deal Days </label>
                  <input type="text" name="deal_days" id="deal_days" value="<?php echo $deal_days; ?>" class="form-field width40" style="width:39%;padding: 2px 0 2px 5px;"/> 
                  
                    <label class="form-label">Deal Image </label>
                  <input type="file" name="deal_image" id="deal_image"  class="form-field width40" style="width:39%;padding: 2px 0 2px 5px;"/> 
                  
                  
                  
                  
                    <?php
				 $deal_image_url= front_base_url().'upload/deal_image.png';
		 					
				 if($prev_deal_image!='') {  
					if(file_exists(base_path().'upload/deal/'.$prev_deal_image)) {
						$deal_image_url=front_base_url().'upload/deal/'.$prev_deal_image;	
					}					
				}
				?>  <div style="width:155px; height:155px; text-align:center;overflow:hidden;" >
                          <img src="<?php echo $deal_image_url; ?>" width="155" />     
                   <input type="hidden" name="prev_deal_image" id="prev_deal_image" value="<?php echo $prev_deal_image;?>" />
                   </div><div class="clear"></div>
                   
                   
                  
                  
									
				  <label class="form-label">Deal Summary </label>
                  <textarea name="deal_summary" id="deal_summary"  style="width:355px; height:95px;"><?php echo $deal_summary; ?></textarea>
                  
                  <label class="form-label">Deal Description </label>
                  <textarea name="deal_description" id="deal_description"  style="width:355px; height:95px;"><?php echo $deal_description; ?></textarea>
                  
                  
                  
                  
                   
               
                 
              
                 
                     
                  <label class="form-label">Status </label> 
                  <select name="deal_status" id="deal_status" class="form-field settingselectbox required">
                        <option value="0" <?php if($deal_status==0){ echo "selected"; } ?>>Inactive</option>
                        <option value="1" <?php if($deal_status==1){ echo "selected"; } ?>>Pending</option>
                        <option value="2" <?php if($deal_status==2){ echo "selected"; } ?>>Active</option>
                        <option value="3" <?php if($deal_status==3){ echo "selected"; } ?>>Reject</option>														
                  </select>
                  
                  
                  
				   <label class="form-label">&nbsp;</label> 
                   
                   <input type="hidden" name="deal_id" id="deal_id" value="<?php echo $deal_id; ?>" />
				  
                    <?php if($deal_id==""){ ?>
						<input type="submit" name="submit" value="Submit" class="button themed" />
					<?php }else { ?>
						<input type="submit" name="submit" value="Update" class="button themed" onclick=""/>
					<?php } ?>
                  
                   <input type="button" name="cancel" value="Cancel" class="button themed" onClick="location.href='<?php echo site_url('deal/list_deal'); ?>'"/>  
				  
			  </form>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>