<script type="text/javascript" language="javascript">

	
	function getlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			window.location.href='<?php echo site_url('deal/list_deal/'.$filter);?>/'+limit;
		}
	
	}	
	
	function getsearchlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			
			window.location.href='<?php echo site_url('deal/search_list_deal/'.$filter);?>/'+limit+'/<?php echo $option.'/'.$keyword; ?>';
		}
	
	}
	
	function gomain(x)
	{
		
		if(x == 'all')
		{
			window.location.href= '<?php echo site_url('deal/list_deal');?>';
		}
	}
	
	function setchecked(elemName,status){
		elem = document.getElementsByName(elemName);
		for(i=0;i<elem.length;i++){
			elem[i].checked=status;
		}
	}


	function setaction(elename, actionval, actionmsg, formname) 
	{
		vchkcnt=0;
		elem = document.getElementsByName(elename);
		
		for(i=0;i<elem.length;i++){
			if(elem[i].checked) vchkcnt++;	
		}
		if(vchkcnt==0) {
			alert('Please select a record')
		} else {
			
			if(confirm(actionmsg))
			{
				document.getElementById('action').value=actionval;	
				document.getElementById(formname).submit();
			}		
			
		}
	}


	function chk_valid()
	{
		
		var keyword = document.getElementById('keyword').value;
		
		if(keyword=='')
		{
			alert('Please enter search keyword');	
			return false;
			
		}
		
		else
		{
			return true;			
		}
		
		
		
	}
	
	
</script>

<div id="content">        
	<?php $error='';
		
		 if($msg != ""){
            if($msg == "insert"){ $error = 'New Record has been added Successfully.';}
            if($msg == "update"){ $error = 'Record has been updated Successfully.';}
            if($msg == "delete"){ $error = 'Deal has been deleted Successfully.';}
			if($msg == "edit"){ $error = 'Deal status has been changed Successfully.';}
			if($msg == "accept"){ $error = 'Deal has been approved Successfully.';}
			if($msg == "inactive"){ $error = 'Deal has been inactivated Successfully.';}
			if($msg == "reject"){ $error = 'Deal has been rejected Successfully.';}
			if($msg == "pending"){ $error = 'Deal has been punt in pending mode Successfully.';}			
    ?>
        <div class="column full">
            <span class="message information"><strong><?php echo $error;?></strong></span>
        </div>
    <?php } ?>
	<div class="clear"></div>
	<div class="column full">
	
	<div class="box">		
		<div class="box themed_box">
		<h2 class="box-header"><?php echo ucfirst($filter);?> Deals </h2>
			
			<div class="box-content box-table">
			<table class="tablebox">
            
                <div id="topbar" style="border:#CCC solid 1px;">
                   
                   
                   
                   
                    <div style="float:left;">
                        <strong>Show</strong>
                            <?php if($search_type=='normal') { ?>
                            <select name="limit" id="limit" onchange="getlimit(this.value)" style="width:80px;">
                            <?php } if($search_type=='search') { ?>
                             <select name="limit" id="limit" onchange="getsearchlimit(this.value)" style="width:80px;">
                            <?php } ?>
                                <option value="0">Per Page</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="75">75</option>
                                <option value="100">100</option>
                            </select>
                      </div>
                    
                    
                    
                  <div style="float:left;">  
  <form name="frm_search" id="frm_search" method="post" action="<?php echo site_url('deal/search_list_deal/'.$filter.'/'.$limit); ?>" onSubmit="return chk_valid();">
                     <strong>&nbsp;&nbsp;&nbsp;Search By</strong>&nbsp;
                        <select name="option" id="option" style="width:100px;" onChange="gomain(this.value)">
                            <option value="all">All</option> 
            
            <option value="de.deal_title" <?php if($option=='de.deal_title'){?> selected="selected"<?php }?>>Deal Title</option>
                              
            <option value="us.profile_name" <?php if($option=='us.profile_name'){?> selected="selected"<?php }?>>Username</option>
            <option value="us.email"  <?php if($option=='us.email'){?> selected="selected"<?php }?>>E-mail</option> 
            <option value="us.first_name" <?php if($option=='us.first_name'){?> selected="selected"<?php }?>>First Name</option>
            <option value="us.last_name" <?php if($option=='us.last_name'){?> selected="selected"<?php }?>>Last Name</option>
            
            <option value="ag.agency_name"  <?php if($option=='ag.agency_name'){?> selected="selected"<?php }?>>Agency Name</option>
            <option value="ag.contact_person_name"  <?php if($option=='ag.contact_person_name'){?> selected="selected"<?php }?>>Contact Person Name</option>    
            <option value="ag.agency_contact_email"  <?php if($option=='ag.agency_contact_email'){?> selected="selected"<?php }?>>Agency Contact Email</option>
           <option value="ag.agency_phone"  <?php if($option=='ag.agency_phone'){?> selected="selected"<?php }?>>Agency Phone</option>
           <option value="ag.agency_customer_support_no"  <?php if($option=='ag.agency_customer_support_no'){?> selected="selected"<?php }?>>Agency Customer Support no.</option>       
                   </select>
        
                        <input type="text" name="keyword" id="keyword" value="<?php echo $keyword;?>" class="textfield"/>                
                        <input type="submit" name="submit" id="submit" value="Search" class="button themed" /> 
                     </form> 
              </div>
              
              <style>
			  .filter_link_select { padding:0px 5px; color:#009900;  font-size:15px; font-weight:bold; }
			   .filter_link {  padding:0px 5px; font-size:13px; font-weight:bold; text-decoration:underline; }
			  </style>
              
              
              <?php
			  
			  
$filter_link_all=site_url('deal/list_deal/all/'.$limit);
$filter_link_pending=site_url('deal/list_deal/pending/'.$limit);
$filter_link_accept=site_url('deal/list_deal/accept/'.$limit);
$filter_link_reject=site_url('deal/list_deal/reject/'.$limit);
$filter_link_inactive=site_url('deal/list_deal/inactive/'.$limit);

if($search_type=='search') {
	$filter_link_all=site_url('deal/search_list_deal/all/'.$limit.'/'.$option.'/'.$keyword);
	$filter_link_pending=site_url('deal/search_list_deal/pending/'.$limit.'/'.$option.'/'.$keyword);
	$filter_link_accept=site_url('deal/search_list_deal/accept/'.$limit.'/'.$option.'/'.$keyword);
	$filter_link_reject=site_url('deal/search_list_deal/reject/'.$limit.'/'.$option.'/'.$keyword);
	$filter_link_inactive=site_url('deal/search_list_deal/inactive/'.$limit.'/'.$option.'/'.$keyword);
}




			  	$filter_all_select='class="filter_link"';
				$filter_pending_select='class="filter_link"';
			  	$filter_accept_select='class="filter_link"';
			  	$filter_inactive_select='class="filter_link"';
			  	$filter_reject_select='class="filter_link"';
				
				
			  
			   if($filter=='all') { $filter_all_select='class="filter_link_select"'; }
			   if($filter=='pending') { $filter_pending_select='class="filter_link_select"'; }
			   if($filter=='accept') { $filter_accept_select='class="filter_link_select"'; }
			   if($filter=='reject') { $filter_reject_select='class="filter_link_select"'; }
			   if($filter=='inactive') { $filter_inactive_select='class="filter_link_select"'; }
              
              
              ?>
              
              <div style="float:left; padding:4px 0px 0px 30px;">
             
              <a href="<?php echo $filter_link_all; ?>" <?php echo $filter_all_select; ?>>All</a> 
              <a href="<?php echo $filter_link_pending; ?>" <?php echo $filter_pending_select; ?>>Pending</a> 
              <a href="<?php echo $filter_link_inactive; ?>" <?php echo $filter_inactive_select; ?>>Inactive</a>  
              <a href="<?php echo $filter_link_accept; ?>" <?php echo $filter_accept_select; ?>>Accept</a> 
              <a href="<?php echo $filter_link_reject; ?>" <?php echo $filter_reject_select; ?>>Reject</a>
             
              </div>
              
              <div style="float:right;">
             
             
               <form  name="frm_agents" id="frm_agents" action="<?php echo site_url('deal/action_deal/'.$filter);?>/" method="post">
           
             
              <input type="hidden" name="filter" id="filter" value="<?php echo $filter; ?>" />
              
              <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
              <input type="hidden" name="limit" id="limit" value="<?php echo $limit; ?>" />
              <input type="hidden" name="option" id="option" value="<?php echo $option; ?>" />
              <input type="hidden" name="keyword" id="keyword" value="<?php echo $keyword; ?>" />
              <input type="hidden" name="search_type" id="search_type" value="<?php echo $search_type; ?>" />
              
               <input type="hidden" name="action" id="action" />
                
                
              
				
                   <a href="javascript:void(0)"  onclick="setaction('chk[]','pending', 'Are you sure, you want to Pending selected record(s)?','frm_agents')" class="button white" style="margin: 0px;"><span class="icon_text pending"></span> Pending</a>
                
                
                   <a href="javascript:void(0)"  onclick="setaction('chk[]','accept', 'Are you sure, you want to Accept selected record(s)?','frm_agents')" class="button white" style="margin: 0px;"><span class="icon_text accept"></span> Accept</a>
                   
                     <a href="javascript:void(0)"  onclick="setaction('chk[]','reject', 'Are you sure, you want to Reject selected record(s)?','frm_agents')" class="button white" style="margin: 0px;"><span class="icon_text cancel"></span> Reject</a>
                     
                      <a href="javascript:void(0)"  onclick="setaction('chk[]','inactive', 'Are you sure, you want to Inactive selected record(s)?','frm_agents')" class="button white" style="margin: 0px;"><span class="icon_text inactive"></span> Inactive</a>
                      
                      
               
                 <a href="javascript:void(0)"  onclick="setaction('chk[]','delete', 'Are you sure, you want to delete selected record(s)?','frm_agents')" class="button white" style="margin: 0px;"><span class="icon_text cancel"></span> Delete</a>
                 
                 
                 
                 </div>
                 </div>
                

				<thead class="table-header">
					<tr> 
                    	 <th class="first tc"><a href="javascript:void(0)" onClick="javascript:setchecked('chk[]',1)" style="color:#000;"><?php echo "Check All"; ?></a> |
           <a href="javascript:void(0)" onClick="javascript:setchecked('chk[]',0)" style="color:#000;"><?php echo "Clear All"; ?></a></th>
                       
                       
                      
                       
                        <th class="first tc">Deal Title</th>
                        <th>Amount</th>
                        <th>Deal Summary</th>
                        
                        <th>User Name</th>
                        <th>Email</th>                       
                        <th>Agency</th>
               
                        
                        
                        <th>Create Date</th>
                        <th>Edit Date</th>
                        <th>IP</th>
                        
                        <th>Status</th>
                    
                    <th>Action</th>
                       
                       
					</tr>
				</thead>
				
				<tbody class="openable-tbody">
				<?php
                    if($result)
                    {
                        $i=0;
                        foreach($result as $row)
                        {
                            if($i%2=="0")
                            {
                                $cl = "odd";	
                            }else{	
                                $cl = "even";	
                            }
                  ?>
					<tr class="<?php echo $cl; ?>">
                   		<td><input type="checkbox" name="chk[]" id="chk" value="<?php echo $row->deal_id;?>" /></td>
                        
                        
                        <td><?php echo $row->deal_title; ?></td>
                        <td><?php echo $row->deal_amount; ?></td>
                        <td><?php echo $row->deal_summary; ?></td>
                        
                        <td class="tc"><?php echo anchor(front_base_url().'travelagent/'.$row->profile_name,ucwords($row->first_name.' '.$row->last_name),' style="color:#004C7A;" target="_blank"'); ?></td>
                     
                        <td><?php echo $row->email; ?></td>
                        <td><?php echo ucwords($row->agency_name); ?></td>
                        
                        <td><?php echo date($site_setting->date_time_format,strtotime($row->deal_date)); ?></td>
                        
                         <td><?php if($row->deal_update_date!='' && $row->deal_update_date!='0000-00-00 00:00:00'){ 
						 echo date($site_setting->date_time_format,strtotime($row->deal_update_date)); } ?></td>
                        
                        <td><?php echo $row->deal_ip; ?></td>
                        
                        
                       
                       
                        <td>
                        <?php if($row->deal_status==0) { echo "Inactive"; } 
						elseif($row->deal_status==1) { echo "Pending"; } 
						elseif($row->deal_status==2) { echo "Accept"; }
						elseif($row->deal_status==3) { echo "Reject"; }
						
						
						 ?>
                        </td>
                        
                           
                             <td><?php  echo anchor('deal/edit_deal/'.$row->deal_id,'<span class="icon_single edit"></span>','class="button white" title="Edit Deal"');  ?></td>          
                      
                        
                        
                  	</tr>
                    
                     
				  <?php
                            $i++;
                        }
                    }
                  else { ?>
						<tr class="alter"><td colspan="11" align="center" valign="middle" height="30">No Deal has been <?php if($filter=='all'){ ?>added<?php } else { echo ucfirst($filter); } ?> yet</td></tr>
					<?php 	}
					?> 	
				</tbody>
			</table>
				
                <ul class="pagination">
					<?php echo $page_link; ?>
                </ul>
			</div>
		</div>
	</div>
</div>