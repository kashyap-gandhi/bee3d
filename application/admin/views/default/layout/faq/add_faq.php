<?php
$CI =& get_instance();	
$base_url = $CI->config->slash_item('base_url_site');											
$base_path = $CI->config->slash_item('base_path');
?>


<div id="content" align="center">

 	
 	<?php if($error!=''){ ?>
		<div class="column full">
			<span class="message information"><strong><?php echo $error;?></strong></span>
		</div>
    <?php }?>

	<div align="left" class="column half">
		<div class="box">
			<h2 class="box-header"><?php if($faq_id==""){ echo 'Add Faq'; } else { echo 'Edit Faq'; }?> </h2> 
			<div class="box-content">	
			   <?php
					$attributes = array('name'=>'frm_faq');
					echo form_open('faq/add_faq',$attributes);
				?>					
				  <label class="form-label">Category </label>
                  	 <span onmouseover="show_bg('faq_home')" onmouseout="hide_bg('faq_home')">
				  <select name="faq_category_id" id="faq_category_id" class="form-field settingselectbox required" >
                        <option value=""> -- Select One -- </option>
                        <?php foreach($category as $p){ ?>
																
                        <option value="<?php echo $p->faq_category_id; ?>" <?php if($p->faq_category_id == $faq_category_id){ echo "selected"; } ?>><?php echo $p->faq_category_name; ?></option>
														<?php	} ?>													
                  </select>		</span>
                  
                  
				<?php /*?>  <label class="form-label" width="30%;">Show On Help </label>
                  <input type="radio" name="faq_home" id="faq_home" value="1" <?php if($faq_home=='1') { ?> checked="checked" <?php } ?> /> Yes 
									
				<input type="radio" name="faq_home" id="faq_home" value="0" <?php if($faq_home=='0') { ?> checked="checked" <?php } ?> style="width:20px;"/> No  <br /><br /><?php */?>
                   
                  <label class="form-label">Order </label>
                    <span onMouseOver="show_bg('faq_order')" onMouseOut="hide_bg('faq_order')">
				<input type="text" name="faq_order" id="faq_order" class="form-field width40" value="<?php echo $faq_order; ?>" onFocus="show_bg('faq_order')" onBlur="hide_bg('faq_order')"/>
				   </span>
                  
                 
                                 
                     
                  <label class="form-label">Status </label> 
                                
                   <span onMouseOver="show_bg('active')" onMouseOut="hide_bg('active')">
							  <select name="active" class="form-field settingselectbox required" id="active">
								<option value="0" <?php if($active=='0'){ echo "selected"; } ?>>Inactive</option>
								<option value="1" <?php if($active=='1'){ echo "selected"; } ?>>Active</option>														
							  </select>
							  </span>
                  
                   <label class="form-label">Question </label>
                 
                   <span onMouseOver="show_bg('question')" onMouseOut="hide_bg('question')">
						  <input type="text" name="question" id="question" class="form-field width40" value="<?php echo $question; ?>" onFocus="show_bg('question')" onBlur="hide_bg('question')"/>
						  </span>
                          
                     <label class="form-label">Answer </label>
                 
                  
                  <script src="<?php echo front_base_url(); ?>editor/js/jquery-1.4.4.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?php echo front_base_url(); ?>editor/js/jquery-ui-1.8.7.custom.min.js" type="text/javascript" charset="utf-8"></script>
		<link rel="stylesheet" href="<?php echo front_base_url(); ?>editor/css/smoothness/jquery-ui-1.8.7.custom.css" type="text/css" media="screen" charset="utf-8">

		<!-- elRTE -->
		<script src="<?php echo front_base_url(); ?>editor/js/elrte.min.js" type="text/javascript" charset="utf-8"></script>
		<link rel="stylesheet" href="<?php echo front_base_url(); ?>editor/css/elrte.min.css" type="text/css" media="screen" charset="utf-8">
		
		<!-- elFinder -->
		<link rel="stylesheet" href="<?php echo front_base_url(); ?>editor/css/elfinder.css" type="text/css" media="screen" charset="utf-8" /> 
		<script src="<?php echo front_base_url(); ?>editor/js/elfinder.full.js" type="text/javascript" charset="utf-8"></script> 
		
		<!-- elRTE and elFinder translation messages -->
		<!--<script src="<?php echo front_base_url(); ?>editor/js/i18n/elrte.ru.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?php echo front_base_url(); ?>editor/js/i18n/elfinder.ru.js" type="text/javascript" charset="utf-8"></script>-->

		<script type="text/javascript" charset="utf-8">
			jQuery().ready(function() {
				var opts1 = {
					cssClass : 'el-rte',
					lang     : 'en',  // Set your language
					allowSource : 1,  // Allow user to view source
					height   : 450,   // Height of text area
					toolbar  : 'maxi',   // Your options here are 'tiny', 'compact', 'normal', 'complete', 'maxi', or 'custom'
					cssfiles : ['<?php echo front_base_url(); ?>editor/css/elrte-inner.css'],
					// elFinder
					fmAllow  : 1,
					fmOpen : function(callback) {
						$('<div id="myelfinder" />').elfinder({
							url : '<?php echo front_base_url(); ?>editor/connectors/php/connector.php', //You must configure this file. Look for 'URL'.  
							lang : 'en',
							dialog : { width : 900, modal : true, title : 'Files' }, // Open in dialog window
							closeOnEditorCallback : true, // Close after file select
							editorCallback : callback     // Pass callback to file manager
						})
					}
					//End of elFinder
				}
				
				$('#answer').elrte(opts1);
				
			})
		</script>
        
        
        <textarea id="answer" name="answer" cols="50" rows="4">
				<?php echo $answer; ?>
			</textarea>
                  
                  
				   <label class="form-label">&nbsp;</label> 
                   
                   <input type="hidden" name="faq_id" id="faq_id" value="<?php echo $faq_id; ?>" />
				   <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />

                    <?php if($faq_id==""){ ?>
						<input type="submit" name="submit" value="Submit" class="button themed" />
					<?php }else { ?>
						<input type="submit" name="submit" value="Update" class="button themed" onclick=""/>
					<?php } ?>
                  
                   <input type="button" name="cancel" value="Cancel" class="button themed" onClick="location.href='<?php echo site_url('admin/list_admin'); ?>'"/>  
				  
			  </form>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>












