<script type="text/javascript" language="javascript">
	function delete_rec(id,offset)
	{
		var ans = confirm("Are you sure to delete FAQ Category?");
		if(ans)
		{
			location.href = "<?php echo site_url('faq_category/delete_faq_category'); ?>/"+id+"/"+offset;
		}else{
			return false;
		}
	}
	
	
	function setchecked(elemName,status){
	elem = document.getElementsByName(elemName);
	for(i=0;i<elem.length;i++){
		elem[i].checked=status;
	}
}



function setaction(elename, actionval, actionmsg, formname) {
	vchkcnt=0;
	elem = document.getElementsByName(elename);
	
	for(i=0;i<elem.length;i++){
		if(elem[i].checked) vchkcnt++;	
	}
	if(vchkcnt==0) {
		alert('Please select a record')
	} else {
		
		if(confirm(actionmsg))
		{
			document.getElementById('action').value=actionval;	
			document.getElementById(formname).submit();
		}		
		
	}
}




	function getlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			window.location.href='<?php echo site_url('faq_category/list_faq_category');?>/'+limit;
		}
	
	}	
	
	function getsearchlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			
			window.location.href='<?php echo site_url('faq_category/search_list_faq_category');?>/'+limit+'/<?php echo $option.'/'.$keyword; ?>';
		}
	
	}
	
	function gomain(x)
	{
		
		if(x == 'all')
		{
			window.location.href= '<?php echo base_url('faq_category/list_faq_category');?>';
		}
	}

</script>
<div id="content">
         
          <div id="text">
            <div id="1">
			
			
			 <?php if($msg!='') { 
            
			if($msg=='delete') { $error='Record(s) has been deleted Successfully.'; }
            if($msg=='insert') { $error='New Record has been deleted Successfully.'; } 
			if($msg=='update') { $error='Record has been updated Successfully.'; }  
			if($msg=='active') { $error='Record has been activated Successfully.'; }  
			if($msg=='inactive') { $error='Record has been inactivated Successfully.'; } 
			if($msg=='help_page') { $error='Record has been show on help page Successfully.'; } 
			if($msg=='not_help_page') { $error='Record has been remove from help page Successfully.'; }  
			
			?>
        <div class="column full">
            <span class="message information"><strong><?php echo $error;?></strong></span>
        </div>
        
        <?php } ?>
            
            
            
            <div class="column full">
	
	<div class="box">		
		<div class="box themed_box">
		<h2 class="box-header">FAQ CATEGORY</h2>
			
			<div class="box-content box-table">
            
			<table class="tablebox" id="tabledata"> 

            
             <div id="topbar" style="border:#CCC solid 1px;">
                    
                     
                    
                     
                     
         
                      
                     <div style="float:right;">
                     
                     <form name="frm_listproject" id="frm_listproject" action="<?php echo site_url('faq_category/action_faq_category');?>" method="post">
            
           
           
           <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
           <input type="hidden" name="action" id="action" />
            
                 
                 <?php echo anchor('faq_category/add_faq_category','<span class="icon_text addnew"></span>Add','class="button white" id="Add" title="Add"'); ?>
                 
                 
                 
              <a href="javascript:void(0)"  onclick="setaction('chk[]','active', 'Are you sure, you want to active selected Category(s)?', 'frm_listproject')" class="button white" >Active</a>
                  
                  
                  
               <a href="javascript:void(0)"  onclick="setaction('chk[]','inactive', 'Are you sure, you want to inactive selected FAQ(s)?', 'frm_listproject')" class="button white">Inactive</a>
                  
                 
                 
                   
					
                 </div>
                 
                 
                 
                    
                 </div> 
				
          		<thead class="table-header">
                
					<tr> <th><a href="javascript:void(0)" onclick="javascript:setchecked('chk[]',1)" style="color:#000;"><?php echo "Check All"; ?></a>&nbsp; |&nbsp; 
           <a href="javascript:void(0)" onclick="javascript:setchecked('chk[]',0)" style="color:#000;"><?php echo "Clear All"; ?></a></th>
                         
                         <th><strong>Category Name</strong></th>
									
									<th><strong>Order</strong></th>
								
									<th><strong>Active</strong></th>    
									<th><strong>Action</strong></th>         
                        
                    	
                                                        
                    </tr>
                </thead>
                
                <tbody class="openable-tbody">
				<?php
								  	if($result)
									{
										$i=0;
										foreach($result as $row)
										{
											if($i%2=="0")
											{
												$fc = "toggle";
												$cl = "alter";
											}else{
												$fc = "toggle1";
												$cl = "alter1";
											}
								  ?>
					
                    <td width="180" align="center" valign="middle"><input type="checkbox" name="chk[]" id="chk" value="<?php echo $row->faq_category_id;?>" /></td>
                    <td><?php echo $row->faq_category_name; ?></td>
                   		
                      
                        
                        <td><?php echo $row->faq_category_order; ?></td>
                      
                        <td><?php if($row->active=="1"){ echo "Active"; }else{ echo "Inactive"; } ?></td>
                       
                        <td align="center">
						<?php echo anchor('faq_category/edit_faq_category/'.$row->faq_category_id .'/'.$offset,'<span class="icon_single edit"></span>','class="button white" id="admin_'.$row->faq_category_id .'" title="Edit Faq"'); ?>
                        
                        <?php $chk_delete=$this->faq_category_model->chk_category_faq($row->faq_category_id); 
									
									if($chk_delete!=1 || $chk_delete!='1') { ?> 
									
							
                                    
                                    	<a href="#" onClick="delete_rec('<?php echo $row->faq_category_id; ?>','<?php echo $offset; ?>')" class="button white"><span class="icon_single cancel"></span></a><?php } ?>
						
						</td> 
                        
                        
                        
                    
                       
                        	
							
							
                    </tr>
					<?php
							$i++;
							}
						} else { ?>
						<tr class="alter"><td colspan="15" align="center" valign="middle" height="30">No FAQ Category has been added yet</td></tr>
					<?php 	}
					?> 
                  	</tr>
                </tbody>
                </table> </form>
               
                <ul class="pagination">
					<?php //echo $page_link; ?>
                </ul>

           </div>
       </div>
    </div>
</div>
            
            
            
            
			
			
			<div style="clear:both; height:25px;">
            
         <table border="0" cellpadding="0" cellspacing="0" width="100%"  height="25" >
			
			<tr> 
            
            
            
            </td>
            
                    
            
            
            
            
            </tr>
				</table>
						
			</div>
			
			
              
            </div>
          </div>
        </div>