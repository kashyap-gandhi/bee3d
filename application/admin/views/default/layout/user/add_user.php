<div id="content" align="center">

 	<?php if($error!=''){ ?>
		<div class="column full">
			<span class="message information"><strong><?php echo $error;?></strong></span>
		</div>
    <?php }?>

	<div align="left" class="column half">
		<div class="box">
			<h2 class="box-header">Add User</h2> 
			<div class="box-content">	
			  <?php
			
				$attributes = array('name'=>'frm_add_user');
				echo form_open('user/add_user',$attributes);
			  ?>					
				  <label class="form-label">First Name </label> 
				  <input type="text" name="first_name" id="first_name" value="<?php echo $first_name; ?>" class="form-field width40"/>
									
				  <label class="form-label">Last Name </label>
                  <input type="text" name="last_name" id="last_name" value="<?php echo $last_name; ?>" class="form-field width40"/>
                   
                  <label class="form-label">Email </label>
                  <input type="text" name="email" id="email" value="<?php echo $email; ?>" class="form-field width40"/> 
                                  <br />
                  (Note : Password automatically sent to above email address.)<br />
<br />

                  <label class="form-label">About User </label>
                  <textarea class="form-field small" name="about_user" cols="" rows="" id="about_user"><?php echo $about_user; ?></textarea>
     
     
     
     
       
                  <label class="form-label">Status </label> 
				  <select name="user_status" id="user_status" class="form-field settingselectbox required">
				  	<option value="0" <?php if($user_status=='0'){ echo "selected"; } ?>>Inactive</option>
					<option value="1" <?php if($user_status=='1'){ echo "selected"; } ?>>Active</option>	
				  </select>
                  
				   <label class="form-label">&nbsp;</label> 
                
                   
              
				   <input type="submit" name="submit" value="Submit" class="button themed"/>
				 
                  
                   <input type="button" name="cancel" value="Cancel" class="button themed" onClick="location.href='<?php echo site_url('user/list_user'); ?>'"/>  
				  
			  </form>
			
            
            
            </div>
		</div>
	</div>
	<div class="clear"></div>
</div>