 <?php
			    $attributes = array('name'=>'frm_category');
				echo form_open('user/comment_detail_update/'.$result->image_comment_id,$attributes);
				?>
<div id="content" align="center">
<?php if($error!=''){ ?>
		<div class="column full">
			<span class="message information"><strong><?php echo $error;?></strong></span>
		</div>
    <?php }?>

	<div align="left" class="column half">
		<div class="box">	
            <h2 class="box-header">Comment Detail</h2> 
			<div class="box-content">
			
            
            <?php
			 $image_id=$result->image_id;
			
			$image_detail=get_image_details($image_id);
			
		
			?>
			 
            
			  <table class="tablebox">
                  <tbody class="openable-tbody">
                  
                  
                    <?php if($image_detail) {
				 
				 
					$pin_show_image=upload_url().'upload/no_image.png';
	  		
					if($image_detail->image_name!='') {
																
							$orig_name=$image_detail->image_name;											
									 
							if(file_exists(base_path().'upload/image_thumb/'.$orig_name)) 
							{ 
								$pin_show_image=upload_url().'upload/image_thumb/'.$orig_name;
							}
							else if(file_exists(base_path().'upload/image_medium/'.$orig_name)) 
							{ 
								$pin_show_image=upload_url().'upload/image_medium/'.$orig_name;
							}
							else
							{				
								if(file_exists(base_path().'upload/image_orig/'.$orig_name)) 
								{									
									$pin_show_image=upload_url().'upload/image_orig/'.$orig_name;									
								}
							}
								  
					} 

			
			//$board_detail=get_board_by_id($pin_detail->board_id);
			
				$pin_user_profileimage=upload_url().'upload/no_image.png';
				
				if($image_detail->profile_image!='') {  
				
					if(file_exists(base_path().'upload/user_thumb/'.$image_detail->profile_image)) { 							
						$pin_user_profileimage=upload_url().'upload/user_thumb/'.$image_detail->profile_image;
					} else {
						
						if(file_exists(base_path().'upload/user/'.$image_detail->profile_image)) { 							
							$pin_user_profileimage=upload_url().'upload/user/'.$image_detail->profile_image;
						}
					} 
				 } 
				 
				 
				  ?> 
                     <tr><td colspan="2"><h2>Image Detail</h2><hr/></td></tr>
                     
                     
                     <tr><td colspan="2" align="left" valign="top">
                      
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      
                      <tr>
                      
                     	 <td align="left" valign="top" width="150">
                        
                        <p>Image</p>
                          <img src="<?php echo $pin_show_image;?>" style="border-radius:5px; width:120px; height:120px;"/><br /><br />

<p>User Image</p>
                        
                                 <img src="<?php echo $pin_user_profileimage;?>" style="border-radius:5px; max-width:120px; max-height:120px;"/>
                         
                         
                          
                          </td>
                          
                          <td align="left" valign="top">
                      
                       <table border="0" cellpadding="0" cellspacing="0" width="70%">
                       
                       <?php if($image_detail) { 
					   ?>
                       
                       
                       
                       <tr>
                          <td style="text-align:left; width:35%"><label class="form-label">Image Title </label></td>
                          <td style="text-align:left; width:65%">: <?php echo anchor(front_base_url().$image_detail->image_unique_code,ucfirst($image_detail->image_title),' style="color:#004C7A;" target="_blank"'); ?></td>
                      </tr>
                      
                      <?php } ?>
                      
                        <tr>
                          <td style="text-align:left;"><label class="form-label">Image Description </label></td>
                          <td style="text-align:left;">: <?php echo $image_detail->image_description;?></td>
                      </tr>
                      
                      
                      
                       <tr>
                          <td style="text-align:left; width:35%"><label class="form-label">User Name </label></td>
                          <td style="text-align:left; width:65%">: <?php echo anchor(front_base_url().'user/'.$image_detail->profile_name,ucfirst($result->profile_name),' style="color:#004C7A;" target="_blank"'); ?></td>
                      </tr>
                      
                      
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Firstname </label></td>
                          <td style="text-align:left;">: <?php echo $image_detail->first_name;?></td>
                      </tr>
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Lastname </label></td>
                          <td style="text-align:left;">: <?php echo $image_detail->last_name;?></td>
                      </tr>
                      
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Email </label></td>
                          <td style="text-align:left;">: <?php echo $image_detail->email;?></td>
                      </tr>
                      
                      <tr>
                          
                      </tr>
                      
                      </table>
                     
                      
                    	  </td>
                      
                      </tr>
                      </table>
                       
                      
                      </td></tr>
                      
                    <?php } 
					
					
					$comment_user_profileimage=upload_url().'upload/no_image.png';
					
				if($result->profile_image!='') {  
				
					if(file_exists(base_path().'upload/user_thumb/'.$result->profile_image)) { 							
						$comment_user_profileimage=upload_url().'upload/user_thumb/'.$result->profile_image;
					} else {
						
						if(file_exists(base_path().'upload/user/'.$result->profile_image)) { 							
							$comment_user_profileimage=upload_url().'upload/user/'.$result->profile_image;
						}
					} 
				 } 
				 
				 
				   ?>  
                      
                       <tr><td colspan="2"><h2>Comment User Detail</h2><hr/></td></tr>
                     
                     
                     <tr><td colspan="2" align="left" valign="top">
                      
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      
                      <tr>
                      
                     	 <td align="left" valign="top" width="150">
                        <p>Comment User Image</p>
                                 <img src="<?php echo $comment_user_profileimage;?>" style="border-radius:5px; max-width:120px; max-height:120px;"/>    
                          </td>
                          
                          <td align="left" valign="top">
                      
                       <table border="0" cellpadding="0" cellspacing="0" width="70%">
                       
                       <tr>
                          <td style="text-align:left; width:35%"><label class="form-label">User Name </label></td>
                          <td style="text-align:left; width:65%">: <?php echo anchor(front_base_url().'user/'.$result->profile_name,ucfirst($result->profile_name),' style="color:#004C7A;" target="_blank"'); ?></td>
                      </tr>
                      
                      
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Firstname </label></td>
                          <td style="text-align:left;">: <?php echo $result->first_name;?></td>
                      </tr>
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Lastname </label></td>
                          <td style="text-align:left;">: <?php echo $result->last_name;?></td>
                      </tr>
                      
                      
                       <tr>
                          <td style="text-align:left;"><label class="form-label">Email </label></td>
                          <td style="text-align:left;">: <?php echo $result->email;?></td>
                      </tr>
                      
                      <tr>
                          
                      </tr>
                      
                      </table>
                     
                      
                    	  </td>
                      
                      </tr>
                      </table>
                       
                      
                      </td></tr>
                      
                      
                       <tr><td colspan="2"><hr/></td></tr>
                       
                       <tr>
                         
                          <td style="text-align:left; width:30%;"><label class="form-label">Comment Textarea</label></td>
                          </tr><tr><td><textarea name="user_comment" id="user_comment" style="resize:none;" cols="50" rows="5"><?php echo $result->user_comment;?></textarea>                 </td></tr><tr> <td> 
                          <input type="submit" name="submit" id="submit" value="update" class="button white" />
                      </td>
                          
                          </td>
                      </tr>
                      
                  	 
                     <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
                       <input type="hidden" name="coming" id="coming" value="<?php echo $coming; ?>" />
                      
                      
                     
                       <input type="hidden" name="image_comment_id" id="image_comment_id" value="<?php echo $result->image_comment_id;?>" />
                  </tbody>
			  	 </table>   
             </div>
              


			</div>
		</div>
	</div>
	<div class="clear"></div>
</div></form>