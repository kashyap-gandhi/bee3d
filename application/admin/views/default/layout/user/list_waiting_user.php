<script type="text/javascript" language="javascript">
	function delete_rec(id,offset)
	{
		var ans = confirm("Are you sure to delete user request?");
		if(ans)
		{
			location.href = "<?php echo site_url('user/delete_user_request'); ?>/"+id+"/"+offset;
		}else{
			return false;
		}
	}
	
	function getlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			window.location.href='<?php echo site_url('user/list_waiting_user');?>/'+limit;
		}
	
	}	
	
	
</script>

<script type="text/javascript" language="javascript">
	
	
	
	
	function setchecked(elemName,status){
		elem = document.getElementsByName(elemName);
		for(i=0;i<elem.length;i++){
			elem[i].checked=status;
		}
	}


	function setaction(elename, actionval, actionmsg, formname) {
	vchkcnt=0;
	elem = document.getElementsByName(elename);
	
	for(i=0;i<elem.length;i++){
		if(elem[i].checked) vchkcnt++;	
	}
	if(vchkcnt==0) {
		alert('Please select at least one record from listing')
	} else {
		
		if(confirm(actionmsg))
		{
			document.getElementById('action').value=actionval;	
			document.getElementById(formname).submit();
		}		
		
	}
}
</script>

<div id="content">        
	<?php if($msg != ""){ 
            if($msg == "insert"){ $error = 'New Record has been added Successfully.';}
            if($msg == "update"){ $error = 'Record has been updated Successfully.';}
            if($msg == "delete"){ $error = 'Record has been deleted Successfully.';}
			if($msg == 'password'){ $error = 'Password has been changed Successfully.';}
			if($msg == 'request_send'){ $error = 'Joining request has been send Successfully.';}
    ?>
        <div class="column full">
            <span class="message information"><strong><?php echo $error;?></strong></span>
        </div>
    <?php } ?>
	<div class="clear"></div>
	<div class="column full">
	
	<div class="box">		
		<div class="box themed_box">
		<h2 class="box-header">All Waiting List </h2>
			
			<div class="box-content box-table">
            
            
            
			<table class="tablebox" id="tabledata">
            <form name="frm_waitinguser" id="frm_waitinguser" method="post" action="<?php echo site_url('user/send_email_all'); ?>"  onSubmit="return chk_valid();">
          
                <input type="hidden" name="action" id="action" />
               
              <div id="topbar" style="border:#CCC solid 1px;">
                    
                    
                      
                     <div style="float:right;">
                     
                     
              
               <a href="javascript:void(0)"  onclick="setaction('chk[]','send', 'Are you sure, you want to send mail','frm_waitinguser')" class="button white" style="margin: 0px;"> Send</a>
               
                 
                 </div>
                 </div>
                 
                 
                 
                 
                 
                 
				<thead class="table-header">
					<tr> 
                         <th class="first tc"><a href="javascript:void(0)" onClick="javascript:setchecked('chk[]',1)" style="color:#000;"><?php echo "Check All"; ?></a> |
           <a href="javascript:void(0)" onClick="javascript:setchecked('chk[]',0)" style="color:#000;"><?php echo "Clear All"; ?></a></th>
                        <th class="first tc">Email</th> 
                        <th>Request Code</th>
                        <th>Request On</th>
                        <th>Invitation Send</th>
                        <th>Country</th>
                        <th>City</th>
                        <th>IP Address</th>                                                          
                        <th>Joining Request</th>
                       
  
					</tr>
				</thead>
				
				<tbody class="openable-tbody">
				<?php
                    if($result)
                    {
                        $i=0;
                        foreach($result as $row)
                        {
                            if($i%2=="0")
                            {
                                $cl = "odd";	
                            }else{	
                                $cl = "even";	
                            }
						
                  ?>
					<tr class="<?php echo $cl; ?>">
                       <td><input type="checkbox" name="chk[]" id="chk[]" value="<?php echo $row->request_id;?>" /></td>
                        <td class="tc"><?php echo $row->request_email ?></td>
                        <td><?php echo $row->request_code; ?></td>
                         <td><?php echo date('d M,Y H:i:s',strtotime($row->request_date)); ?></td>
                         
                          <td><?php if($row->request_code!='') { ?> 
                        
                        <img src="<?php echo base_url().getThemeName();?>/gfx/accept.png" border="0" />
                         <?php } else { ?>
                         
                             <img src="<?php echo base_url().getThemeName();?>/gfx/cancel.png" border="0" /> 
                         <?php } ?></td>
                         
                         
                         <td><?php echo $row->country_name; ?></td>
                         <td><?php echo $row->city; ?></td>
                        <td><?php echo $row->request_ip; ?></td>
                        <td class="tc"><?php echo anchor('user/send_join_request/'.$row->request_id,'Send','class="button white"'); ?></td>
                       
                  	</tr>
                   
                    
				  <?php
                            $i++;
                        }
                    }
                  else { ?>
						<tr class="alter"><td colspan="5" align="center" valign="middle" height="30">No User request has been added yet</td></tr>
					<?php 	}
					?> 	
				</tbody></form>
			</table>
				
               <ul class="pagination">
					<?php echo $page_link; ?>
                </ul>
			</div>
		</div>
	</div>
</div>