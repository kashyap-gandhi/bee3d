<?php
	if($this->session->userdata('admin_id')=="")
	{
		echo "<script>window.location.href='".site_url('home')."'</script>";
	}
	 /*For Selected active class in menu*/
	 $this->active = $this->uri->uri_string();
	 $this->strdd = explode("/",$this->active);
	 
	if(!isset($this->strdd[1]))
	{
		$this->strdd[1]='';
	}

	$site_setting=site_setting();
?>

<style type="text/css">
.topdetail{
padding-top:5px;
}
</style>


<ul id="topbar">
     
      <li class="topdetail">IP : <b><?php echo $_SERVER['REMOTE_ADDR']; ?></b></li>
     <li class="s_1"></li>
      <li class="topdetail">Server Dt. : <b><?php echo date('d M, Y h:i:s A');?></b></li>
      
      
       <li class="s_1"></li>
      <li class="topdetail">Last Login : <b><?php echo get_last_admin_login_detail();?></b></li>
      
       <li class="s_1"></li>
      <li class="topdetail">Contact : <b><a href="" target="_blank">iWeb</a></b></li>
      
      
       <li class="fr"><?php echo anchor('home/logout','<span class="icon_text logout"></span>logout',' title="logout" class="button red fl"');?></li>
        <li class="s_1 fr"></li>
        
        
        <li class="fr"><a href="<?php echo front_base_url(); ?>" title="fronts side" target="_blank" class="button white fl"><span class="icon_text admin"></span>Front</a></li>
        <li class="s_1 fr"></li>
		<li class="fr"><?php echo anchor(site_url(),'<span class="icon_text admin"></span>'.$this->session->userdata('username'),' title="admin" class="button white fl"');?></li>
		<li class="clear"></li>
        
        
      </ul>
      
   
	
    <?php 
	$chk_admin_list=get_rights('list_admin');
	$chk_admin_login=get_rights('admin_login');
	
	?>
    
      <ul id="navbar">
        <li <?php  if($this->strdd[0] == 'home' || $this->strdd[0] == 'graph' || $this->strdd[0] == 'cronjob' ){ echo 'class="active"'; }?> > <?php echo anchor('home/dashboard','<span class="icon_text dashboard"></span>Dashboard',' title="Dashboard"');?> </li>
        
         <?php  if($this->strdd[0] == 'home' || $this->strdd[0] == 'graph' || $this->strdd[0] == 'cronjob' ){ ?>
        <ul>
          
           
              <li><?php echo anchor('graph/user','Graphical Registration Report',' title="Graphical Registration Report" class="subbutton white"');?></li>
              
              
           <?php /*?>  <li><?php echo anchor('graph/','Graphical Trip Report',' title="Graphical Image Report" class="subbutton white"');?></li> 
              
              <li><?php echo anchor('graph/earning','Graphical Earning Report',' title="Graphical Earning Report" class="subbutton white"');?></li> 
              
              
              
               <li><?php echo anchor('cronjob/list_cronjob','Cron Job Report',' title="Cron Job Report" class="subbutton white"');?></li><?php */?>
        
      
              
          </ul>
          
            <?php } ?>
        
        <?php if(($this->strdd[0] == 'admin' || $this->strdd[0] == 'rights') && ($chk_admin_list==1 || $chk_admin_login==1)) { ?>
       
       	 <li  class="active"><?php  
		 		 
		 if($chk_admin_list==1) { echo anchor('admin/list_admin','<span class="icon_text settings"></span>Admin',' title="Administrator Management"');  }  
		 
		 elseif($chk_admin_login==1) { echo anchor('admin/admin_login','<span class="icon_text settings"></span>Admin Login History',' title="Administrator Login List"'); } 
		 
		 ?></li>
         
         <ul>
          	
            <?php if($chk_admin_list==1) { ?>
            
            <li <?php if($this->strdd[1] == 'list_admin') { echo 'class="active"'; }?>><?php echo anchor('admin/list_admin','Administrator',' title="Administrator List" class="subbutton white"');?></li>
            
            <?php } if($chk_admin_login==1) {  ?>
            
            <li <?php if($this->strdd[1] == 'admin_login' && $chk_admin_login==1) { echo 'class="active"'; }?>><?php echo anchor('admin/admin_login','Admin Login History',' title="Administrator Login List" class="subbutton white"');?></li>
            
            <?php } ?>
            
          </ul>
          
		<?php } else { 
		
		if($chk_admin_list==1 ||$chk_admin_login==1) { ?>
        <li><?php
        
		 if($chk_admin_list==1) { echo anchor('admin/list_admin','<span class="icon_text settings"></span>Admin',' title="Administrator Management"');  }  
		 
		 elseif($chk_admin_login==1) { echo anchor('admin/admin_login','<span class="icon_text settings"></span>Admin',' title="Administrator Login List"'); } 
		
		?></li>
        <?php } } ?>
          
        
        <?php if($this->strdd[0] == 'user' || $this->strdd[0] == 'review') { ?>
         <li class="active"><?php echo anchor('user/list_user','<span class="icon_text users"></span>User',' title="Users"');?></li>
             <ul>  
              
                <li <?php if($this->strdd[1] == 'list_user' || $this->strdd[1] == 'search_list_user') { echo 'class="active"'; }?>><?php echo anchor('user/list_user','All Users',' title="All Users List" class="subbutton white"');?></li>
                  
                <li <?php if($this->strdd[1] == 'list_active_user') { echo 'class="active"'; }?>><?php echo anchor('user/list_active_user','Active Users',' title="Active Users List" class="subbutton white"');?></li>
                
                 <li <?php if($this->strdd[1] == 'list_inactive_user') { echo 'class="active"'; }?>><?php echo anchor('user/list_inactive_user','Inactive Users',' title="Inactive Users List" class="subbutton white"');?></li>
                
                
                <li <?php if($this->strdd[1] == 'list_suspend_user') { echo 'class="active"'; }?>><?php echo anchor('user/list_suspend_user','Suspend Users',' title="Suspend Users List" class="subbutton white"');?></li>
                
                <li <?php if($this->strdd[1] == 'list_deleted_user') { echo 'class="active"'; }?>><?php echo anchor('user/list_deleted_user','Deleted Users',' title="Deleted Users List" class="subbutton white"');?></li>
                
                <li <?php if($this->strdd[1] == 'user_login') { echo 'class="active"'; }?>><?php echo anchor('user/user_login','Users Login',' title="Users Login List" class="subbutton white"');?></li>
                
              
                
                 <?php /*?><li <?php if($this->strdd[1] == 'list_comment_detail' || $this->strdd[1] == 'comment_detail' ) { echo 'class="active"'; }?>><?php echo anchor('user/list_comment_detail','Comments',' title="All Comment List" class="subbutton white"');?></li><?php */?>
     
             </ul>
         <?php } else { ?>
         <li><?php echo anchor('user/list_user','<span class="icon_text users"></span>User',' title="Users"');?></li>
         <?php } ?>


		       
         
         
         
         
         
      
          <?php if($this->strdd[0] == 'category' || $this->strdd[1] == 'list_design_category' || $this->strdd[1] == 'add_design_category' ||  $this->strdd[1] == 'edit_design_category'  || $this->strdd[1] == 'list_video_category' || $this->strdd[1] == 'add_video_category' ||  $this->strdd[1] == 'edit_video_category'  || $this->strdd[1] == 'list_challenge_category' || $this->strdd[1] == 'add_challenge_category' ||  $this->strdd[1] == 'edit_challenge_category') { ?>
          
          <li class="active"><?php  echo anchor('category/list_design_category','<span class="icon_text settings"></span>Category',' title="Design Category Management"'); ?></li>
          
          <ul>
				
                
                <li <?php if($this->strdd[1] == 'pending_design_category' || $this->strdd[1] == 'action_pending_design_category' ||  $this->strdd[1] == 'list_design_category' || $this->strdd[1] == 'add_design_category'  ||  $this->strdd[1] == 'edit_design_category' ) { echo 'class="active"'; }?>><?php echo anchor('category/list_design_category','Design Category',' title="Design Category List" class="subbutton white"');?></li>
                
                 <li <?php if($this->strdd[1] == 'pending_video_category' || $this->strdd[1] == 'action_pending_video_category' ||  $this->strdd[1] == 'list_video_category' || $this->strdd[1] == 'add_video_category'  ||  $this->strdd[1] == 'edit_video_category' ) { echo 'class="active"'; }?>><?php echo anchor('category/list_video_category','Video Category',' title="Video Category List" class="subbutton white"');?></li>
                 
          <li <?php if($this->strdd[1] == 'list_challenge_category' || $this->strdd[1] == 'add_challenge_category'  ||  $this->strdd[1] == 'edit_challenge_category' ) { echo 'class="active"'; }?>><?php echo anchor('category/list_challenge_category','Challenge Category',' title="Challenge Category List" class="subbutton white"');?></li>
                
             <li <?php if($this->strdd[1] == 'list_store_category' || $this->strdd[1] == 'add_store_category'  ||  $this->strdd[1] == 'edit_store_category' ) { echo 'class="active"'; }?>><?php echo anchor('category/list_store_category','Store Category',' title="Store Category List" class="subbutton white"');?></li>  
                
                
          </ul>
            <?php } else{ ?>
           <li>  
           <?php  echo anchor('category/list_design_category','<span class="icon_text settings"></span>Category',' title="Design Category Management"'); ?>
           </li>
           <?php  }  ?>
        
        
         
         
         
         
        
         
         
         
         
                  
         
         <?php /*if($this->strdd[0] == 'report' || $this->strdd[1] == 'report_comment'  || $this->strdd[1] == 'report_image' || $this->strdd[1] == 'report_album' || $this->strdd[1] == 'report_user' || ($this->strdd[1] == 'image_detail' && $this->strdd[0] == 'report') || ($this->strdd[1] == 'comment_detail' && $this->strdd[0] == 'report') || ($this->strdd[1] == 'album_detail' && $this->strdd[0] == 'report')|| ($this->strdd[1] == 'user_detail' && $this->strdd[0] == 'report') )  { ?>
         
             <li class="active"><?php echo anchor('report/report_image','<span class="icon_text content"></span>Report',' title="Report Management"');?></li>
             
             
            <ul>   
                
                <li <?php if($this->strdd[1] == 'report_image' || $this->strdd[1] == 'image_detail')  { echo 'class="active"'; }?>><?php echo anchor('report/report_image','Image',' title="Reported Image List" class="subbutton white"');?></li>
                
              <li <?php if($this->strdd[1] == 'report_album' || $this->strdd[1] == 'album_detail')  { echo 'class="active"'; }?>><?php echo anchor('report/report_album','Album',' title="Reported Album List" class="subbutton white"');?></li>
                
              	<li <?php if($this->strdd[1] == 'report_comment' || $this->strdd[1] == 'comment_detail') { echo 'class="active"'; }?>><?php echo anchor('report/report_comment','Comment',' title="Reported Comment List" class="subbutton white"');?></li>
                
          		<li <?php if($this->strdd[1] == 'report_user' || $this->strdd[1] == 'user_detail') { echo 'class="active"'; }?>><?php echo anchor('report/report_user','User',' title="Reported User List" class="subbutton white"');?></li>
                
               
             </ul>
         <?php } else { ?>
         <li><?php echo anchor('report/report_image','<span class="icon_text content"></span>Report',' title="Report Management"');?></li>
         <?php } */ ?>
         
         
         
         
         
           <?php /*if($this->strdd[0] == 'transaction') { ?>
       <li class="active"><?php  echo anchor('transaction/list_earning','<span class="icon_text settings"></span>Transactions',' title="Transactions"'); ?></li>
       
       		<ul>
				<li <?php if($this->strdd[1] == 'list_earning' || $this->strdd[1] == 'daily_report' || $this->strdd[1]=='monthly_report'  || $this->strdd[1] == 'yearly_report') { echo 'class="active"'; }?>><?php echo anchor('package/list_earning','Earning List',' title="All Earning List" class="subbutton white"');?></li>
                
               
                
          </ul>
       
           <?php } else{ ?>
           <li>  
           <?php  echo anchor('transaction/list_earning','<span class="icon_text settings"></span>Transactions',' title="Transactions"'); ?>
           </li>
           <?php  }*/  ?>
		   
		   
		   
        
          
          
          <?php if($this->strdd[0] == 'transaction_type' || $this->strdd[0] == 'payments_gateways' || $this->strdd[0]=='wallet' || $this->strdd[0]=='wallet_setting'||$this->strdd[0] =='gateways_details' || $this->strdd[0] == 'transaction' || $this->strdd[0] == 'wallet_report' || $this->strdd[1] =='search_withdrawal' || $this->strdd[1] == 'search_walletreview') { ?>
             
             <li class="active"><?php echo anchor('payments_gateways/list_payment_gateway','<span class="icon_text media"></span>Payment',' title="Payment Module"');?></li>
             
             <ul>
              <li <?php if($this->strdd[1] == 'add_wallet_setting') { echo 'class="active"'; }?>><?php echo anchor('wallet_setting/add_wallet_setting','Wallet Settings  ',' title="Wallet Settings" class="subbutton white"');?></li>
				<?php /* ?><li <?php if($this->strdd[1] == 'list_paypal') { echo 'class="active"'; }?>><?php echo anchor('transaction_type/list_paypal','Paypal Setting',' title="Paypal Setting" class="subbutton white"');?></li> <?php */ ?>
                <li <?php if($this->strdd[1] == 'list_payment_gateway' || $this->strdd[1]=='list_gateway_detail'  || $this->strdd[1]=='edit_detail' ) { echo 'class="active"'; }?>><?php echo anchor('payments_gateways/list_payment_gateway','Payment Gateway',' title="Payment Gateway" class="subbutton white"');?></li>
                
                 <li <?php if($this->strdd[1] == 'list_wallet_withdraw' || $this->strdd[1] =='search_withdrawal') { echo 'class="active"'; }?>><?php echo anchor('wallet/list_wallet_withdraw','Wallet Withdraw ',' title="Wallet Withdraw" class="subbutton white"');?></li>
                
                <li <?php if($this->strdd[1] == 'list_wallet_review' || $this->strdd[1] == 'search_walletreview' ) { echo 'class="active"'; }?>><?php echo anchor('wallet/list_wallet_review','Wallet Review (User Credits)',' title="Wallet Review" class="subbutton white"');?></li>
                
                
               <!-- <li <?php //if($this->strdd[1] == 'list_admin_wallet_review' || $this->strdd[1] == 'search_admin_walletreview' ) { echo 'class="active"'; }?>><?php //echo anchor('wallet/list_wallet_review','Admin Wallet Review (Admin Commissions)',' title="Admin Wallet Review" class="subbutton white"');?></li>-->
                 
                
                 <?php /*
                  <li <?php if($this->strdd[1] == 'list_escrow') { echo 'class="active"'; }?>><?php echo anchor('transaction/list_escrow','Escrow',' title="Escrow" class="subbutton white"');?></li>
                 
                 <li <?php if($this->strdd[1] == 'list_paying') { echo 'class="active"'; }?>><?php echo anchor('transaction/list_paying','Paying',' title="Paying" class="subbutton white"');?></li>
                
                 <li <?php if($this->strdd[1] == 'list_earning') { echo 'class="active"'; }?>><?php echo anchor('transaction/list_earning','Earning',' title="Earning" class="subbutton white"');?></li>
                 
                 <li <?php if($this->strdd[1] == 'list_refund') { echo 'class="active"'; }?>><?php echo anchor('transaction/list_refund','Refund',' title="Refund" class="subbutton white"');?></li> */ ?>
      
             </ul>
         <?php } else { ?>
         <li><?php echo anchor('payments_gateways/list_payment_gateway','<span class="icon_text media"></span>Payment',' title="Payment Module"');?></li>
         <?php } ?>
         
             <?php /*if($this->strdd[0] == 'paypal') { ?>
          
          <li class="active"><?php echo anchor('paypal','<span class="icon_text settings"></span>Paypal',' title="Paypal Management"'); ?>  </li>
            <?php } else{ ?>
           <li>  
           <?php echo anchor('paypal','<span class="icon_text settings"></span>Paypal',' title="Paypal Management"');?>
           </li>
           <?php  } */ ?>
           
           
           
           
        
        
        
        
         
          <?php if($this->strdd[0] == 'gallery') { ?>
          
          <li class="active"><?php echo anchor('gallery/list_gallery','<span class="icon_text settings"></span>Gallery',' title="Gallery Management"'); ?>  </li>
            <?php } else{ ?>
           <li>  
           <?php echo anchor('gallery/list_gallery','<span class="icon_text settings"></span>Gallery',' title="Gallery Management"');?>
           </li>
           <?php  }  ?>
           
           
           
              
           
      
           
         
           
        
           <?php if($this->strdd[0] == 'faq' ||$this->strdd[1] == 'list_faq_category' || $this->strdd[1] == 'add_faq' ||  $this->strdd[1] == 'add_faq_category' ) { ?>
          
          <li class="active"><?php  echo anchor('faq/list_faq','<span class="icon_text settings"></span>Faq',' title="Faq Management"'); ?></li>
          
          <ul>
				
                
                <li <?php if($this->strdd[1] == 'list_faq_category' || $this->strdd[1] == 'add_faq_category') { echo 'class="active"'; }?>><?php echo anchor('faq_category/list_faq_category','Faq Category',' title="Faq Category List" class="subbutton white"');?></li>
                
                <li <?php if($this->strdd[1] == 'list_faq' || $this->strdd[1] == 'add_faq' ) { echo 'class="active"'; }?>><?php echo anchor('faq/list_faq','Faq List',' title="Faq List" class="subbutton white"');?></li>
                
                
          </ul>
            <?php } else{ ?>
           <li>  
           <?php  echo anchor('faq/list_faq','<span class="icon_text settings"></span>Faq',' title="Faq Management"'); ?>
           </li>
           <?php  }  ?>
        
        

         
         
         
         <?php if($this->strdd[0] == 'pages') { ?>
             <li class="active"><?php echo anchor('pages/list_pages','<span class="icon_text content"></span>Pages',' title="Pages Management"');?></li>
            <!-- <ul>
             	<li <?php if($this->strdd[1] == 'list_pages' || $this->strdd[1] == 'search_list_pages' || $this->strdd[1] == 'add_pages' || $this->strdd[1] == 'edit_pages') { echo 'class="active"'; }?>><?php echo anchor('pages/list_pages','Pages',' title="Pages List" class="subbutton white"');?></li>
                
                
             </ul>-->
         <?php } else { ?>
         <li><?php echo anchor('pages/list_pages','<span class="icon_text content"></span>Pages',' title="Pages Management"');?></li>
         <?php } ?>
         
         
         
         
         
           <?php if($this->strdd[0] == 'package' || $this->strdd[1] == 'list_package_item' || $this->strdd[1] == 'add_package_item' ||  $this->strdd[1] == 'edit_package_item'  || $this->strdd[1] == 'list_package' || $this->strdd[1] == 'add_package' || $this->strdd[1] == 'edit_package') { ?>
          
          <li class="active"><?php  echo anchor('package/list_package','<span class="icon_text settings"></span>Packages',' title="Package Management"'); ?></li>
          
          <ul>
				
                
                <li <?php if($this->strdd[1] == 'list_package_item' || $this->strdd[1] == 'add_package_item'  || $this->strdd[1] == 'edit_package_item') { echo 'class="active"'; }?>><?php echo anchor('package/list_package_item','Package Item',' title="Package Item List" class="subbutton white"');?></li>
                
                <li <?php if($this->strdd[1] == 'list_package' || $this->strdd[1] == 'add_package'  || $this->strdd[1] == 'edit_package' ) { echo 'class="active"'; }?>><?php echo anchor('package/list_package','Package List',' title="Package List" class="subbutton white"');?></li>
                
                
          </ul>
            <?php } else{ ?>
           <li>  
           <?php  echo anchor('package/list_package','<span class="icon_text settings"></span>Packages',' title="Package Management"');  ?>
           </li>
           <?php  }  ?>
         
         
        
         <?php  if($this->strdd[0] == 'country' || $this->strdd[0] == 'state' || $this->strdd[0] == 'city') { ?>
             <li class="active"><?php echo anchor('country/list_country','<span class="icon_text content"></span>Globalization ',' title="Globalization Management"');?></li>
            <?php /*?> <ul>
             	<li <?php if($this->strdd[1] == 'list_country' || $this->strdd[1] == 'search_list_country' || $this->strdd[1] == 'add_country' || $this->strdd[1] == 'edit_country') { echo 'class="active"'; }?>><?php echo anchor('country/list_country','Country',' title="Country List" class="subbutton white"');?></li>
                
                	<li <?php if($this->strdd[1] == 'list_state' || $this->strdd[1] == 'search_list_state' || $this->strdd[1] == 'add_state' || $this->strdd[1] == 'edit_state') { echo 'class="active"'; }?>><?php echo anchor('state/list_state','State',' title="State List" class="subbutton white"');?></li>
                    
                    <li <?php if($this->strdd[1] == 'list_city' || $this->strdd[1] == 'search_list_city' || $this->strdd[1] == 'add_city' || $this->strdd[1] == 'edit_city') { echo 'class="active"'; }?>><?php echo anchor('city/list_city','City',' title="City List" class="subbutton white"');?></li>
             </ul>
             <?php */?>
             
             
         <?php } else { ?>
         <li><?php echo anchor('country/list_country','<span class="icon_text content"></span>Globalization',' title="Globalization Management"');?></li>
         <?php } ?>
         
         
         
         
         <?php if($this->strdd[0] == 'newsletter') { ?>
             <li class="active"><?php echo anchor('newsletter/list_newsletter','<span class="icon_text content"></span>Newsletter',' title="Newsletter Management"');?></li>
             <ul>
             	<li <?php if($this->strdd[1] == 'list_newsletter' || $this->strdd[1] == 'search_newsletter' || $this->strdd[1] == 'add_newsletter' || $this->strdd[1] == 'show_all_subscriber' || $this->strdd[1] == 'search_subscriber_user' ) { echo 'class="active"'; }?>><?php echo anchor('newsletter/list_newsletter','All Newsletter',' title="All Newsletter List" class="subbutton white"');?></li>
                
                	<li <?php if($this->strdd[1] == 'list_newsletter_user' || $this->strdd[1] == 'search_newsletter_user' || $this->strdd[1] =='add_newsletter_user' ||  $this->strdd[1] =='edit_newsletter_user' || $this->strdd[1] =='import_newsletter_user') { echo 'class="active"'; }?>><?php echo anchor('newsletter/list_newsletter_user','Newsletter User',' title="Newsletter User List" class="subbutton white"');?></li>
                    
                    
                    <li <?php if($this->strdd[1] == 'newsletter_job' || $this->strdd[1] == 'search_newsletter_job' || $this->strdd[1] =='add_newsletter_job') { echo 'class="active"'; }?>><?php echo anchor('newsletter/newsletter_job','Newsletter Job',' title="Newsletter Job" class="subbutton white"');?></li>
                  <li <?php if($this->strdd[1] == 'newsletter_setting') { echo 'class="active"'; }?>><?php echo anchor('newsletter/newsletter_setting','Settings',' title="Newsletter Settings" class="subbutton white"');?></li>
				
             </ul>
         <?php } else { ?>
         <li><?php echo anchor('newsletter/list_newsletter','<span class="icon_text content"></span>Newsletter',' title="Newsletter Management"');?></li>
         <?php } ?>
         
         
         

         <?php if($this->strdd[0] == 'spam_setting') { ?>
         <li class="active"> <?php echo anchor('spam_setting/add_spam_setting','<span class="icon_text settings"></span>Spam Setting',' title="Spam Management"');?></li>
         
			<ul>
				<li <?php if($this->strdd[1] == 'add_spam_setting') { echo 'class="active"'; }?>><?php echo anchor('spam_setting/add_spam_setting','Spam Setting',' title="Spam Setting" class="subbutton white"');?></li>    
				
			<?php /*?>	<li <?php if($this->strdd[1] == 'spam_report' || $this->strdd[1] == 'search_list_spam_report' ) { echo 'class="active"'; }?>><?php echo anchor('spam_setting/spam_report','Spam Report',' title="Spam Report" class="subbutton white"');?></li>  
                 <?php */?>
                 
				
				<li <?php if($this->strdd[1] == 'spamer' || $this->strdd[1] == 'add_spammer' || $this->strdd[1] == 'search_list_spam' ) { echo 'class="active"'; }?>><?php echo anchor('spam_setting/spamer','Spamer',' title="Spamer List" class="subbutton white"');?></li> 
                
                 <li <?php if($this->strdd[1] == 'list_spam_word' || $this->strdd[1] == 'add_word'  ) { echo 'class="active"'; }?>><?php echo anchor('spam_setting/list_spam_word','Spam Word',' title="Spam Word Management" class="subbutton white"');?></li>
                
                
               <!--  <li <?php //if($this->strdd[1] == 'list_report_type' || $this->strdd[1] == 'list_report_type' ) { echo 'class="active"'; }?>><?php //echo anchor('spam_setting/list_report_type','Report Type',' title="Report Type Management" class="subbutton white"');?></li>-->
                 
                              
                  
                
                   
				
            </ul>
         <?php } else{ ?>
          <li><?php echo anchor('spam_setting/add_spam_setting','<span class="icon_text settings"></span>Spam Setting',' title="Spam Management"');?></li>
         <?php } ?>
         
         
         

         <?php if($this->strdd[0] == 'site_setting' || $this->strdd[0] == 'meta_setting' || $this->strdd[0] == 'facebook_setting' || $this->strdd[0] == 'twitter_setting' || $this->strdd[0] == 'email_setting' || $this->strdd[0] == 'task_setting' || $this->strdd[0] == 'user_setting' || $this->strdd[0] == 'email_template' || $this->strdd[0] == 'template_setting' || $this->strdd[0]=='paging_setting' || $this->strdd[1]=='board' || $this->strdd[1]=='add_google_setting' || $this->strdd[1]=='add_yahoo_setting' || $this->strdd[1]=='commission_setting' ) { ?>
        <li class="active"><?php echo anchor('site_setting/add_site_setting','<span class="icon_text settings"></span>settings',' title="Settings"');?></li>
		
			<ul>
				<li <?php if($this->strdd[1] == 'add_site_setting') { echo 'class="active"'; }?>><?php echo anchor('site_setting/add_site_setting','Site',' title="Site Setting" class="subbutton white"');?></li>
				
				<li <?php if($this->strdd[0] == 'meta_setting') { echo 'class="active"'; }?>><?php echo anchor('meta_setting/add_meta_setting','Meta',' title="Meta Setting" class="subbutton white"');?></li>
				
				<li <?php if($this->strdd[0] == 'facebook_setting') { echo 'class="active"'; }?>><?php echo anchor('facebook_setting/add_facebook_setting','Facebook',' title="Facebook Setting" class="subbutton white"');?></li>
				
				<li <?php if($this->strdd[0] == 'twitter_setting') { echo 'class="active"'; }?>><?php echo anchor('twitter_setting/add_twitter_setting','Twitter',' title="Twitter Setting" class="subbutton white"');?></li>
                
                
                <li <?php if($this->strdd[1] == 'add_google_setting') { echo 'class="active"'; }?>><?php echo anchor('google_setting/add_google_setting','Google',' title="Google Setting" class="subbutton white"');?></li>
                
                 <li <?php if($this->strdd[1] == 'add_yahoo_setting') { echo 'class="active"'; }?>><?php echo anchor('yahoo_setting/add_yahoo_setting','Yahoo',' title="Yahoo Setting" class="subbutton white"');?></li>
				
				<li <?php if($this->strdd[0] == 'email_setting') { echo 'class="active"'; }?>><?php echo anchor('email_setting/add_email_setting','Email',' title="Email Setting" class="subbutton white"');?></li>
				
				<li <?php if($this->strdd[1] == 'add_image_setting') { echo 'class="active"'; }?>><?php echo anchor('site_setting/add_image_setting','Image Size',' title="Image Size Setting" class="subbutton white"');?></li>
				
			
				
				<li <?php if($this->strdd[1] == 'add_user_setting') { echo 'class="active"'; }?>><?php echo anchor('user_setting/add_user_setting','User',' title="User Setting" class="subbutton white"');?></li>
                
                <li <?php if($this->strdd[0] == 'email_template') { echo 'class="active"'; }?>><?php echo anchor('email_template/list_email_template','Email Templates',' title="Email Templates Management" class="subbutton white"');?></li>
                
                
                 
                 <li <?php if($this->strdd[1] == 'list_template') { echo 'class="active"'; }?>><?php echo anchor('template_setting/list_template','Templates',' title="Template Manager" class="subbutton white"');?></li>
                 
                  <li <?php if($this->strdd[0] == 'paging_setting') { echo 'class="active"'; }?>><?php echo anchor('paging_setting/add_paging','Paging',' title="Paging Setting" class="subbutton white"');?></li>
                  
                  
                  <li <?php if($this->strdd[1] == 'commission_setting') { echo 'class="active"'; }?>><?php echo anchor('site_setting/commission_setting','Commission',' title="Commission Setting" class="subbutton white"');?></li>
                  
                  
                   <li <?php if($this->strdd[1] == 'credit_setting') { echo 'class="active"'; }?>><?php echo anchor('site_setting/credit_setting','Conversation rate',' title="Credit Setting" class="subbutton white"');?></li>
                  
                  
                 
			</ul>
			
		<?php } else { ?>
		<li><?php echo anchor('site_setting/add_site_setting','<span class="icon_text settings"></span>settings',' title="Settings"');?></li>
		 <?php } ?>
         
         
         
	  
	  
        </ul> 
        
        
        
         
      </ul>
	  <div id="subnavbar">
		<!--<form id="search_form" method="post" action="">
			<input type="text" name="search_input" value="Search..." id="search_input" class="fl" />
		</form>-->
	  </div>