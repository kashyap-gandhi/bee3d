<script type="text/javascript">
function display()
{
   
    document.getElementById("tabs-4").style.display="block";
	document.getElementById("tabs-3").style.display="none";
}

function display1()
{
    
    document.getElementById("tabs-4").style.display="none";
	document.getElementById("tabs-3").style.display="block";
}
</script>
<div id="content">

	<?php  if($msg == 'valid'){ ?>
		<span class="message information"><strong>You have logged in successfully.</strong></span>
	<?php } if($msg == 'no_rights') {?>
   	 <span class="message information"><strong>You do not have permmission to access this area.</strong></span>
    <?php } ?>
	
	<?php
	
	$site_setting=site_setting();
	
	
	?>
    
    
    
    <div class="clear"></div>
    

    
    
    <div class="box tabs black_box">
		<h2 class="box-header">Site Statistics</h2>
		
		<div class="box-content box-table">
		<div id="tabs-7">
		<table class="tablebox">
		<thead class="table-header">
		<tr>
			
			<th>Total User</th>
                        <th>Total Daily Login User</th>
           
			  <th>Total Design</th>                          
                          <th>Total Video</th>                          
			<th>Total Challenge</th>                        
                        <th>Total 3D Store</th>
            
                        <th>Total Earn</th> 
            
         
            
            
			
		</tr>
		</thead>
		<tbody>
			<tr class="odd">
				
				
				<td><?php echo get_total_user();?>	 </td>                                
                                <td><?php echo get_daily_login_user();?>	 </td>
                                
                                
                                <td><?php echo get_total_design();?>	 </td>   
                                <td><?php echo get_total_video();?>	 </td>
                                <td><?php echo get_total_challenge();?>	 </td>
                                <td><?php echo get_total_store();	?></td>

                
                
                                <td><?php echo get_total_earning_on_wallet();	?>	 </td>
                             
                

			</tr>	
			
				
			
            
            
			
		</tbody>
		</table>
		</div>
		
	
        
        
		</div>
	</div>
    
    <div class="clear"></div>
    
	<!-- statics-->
    <?php
	
	$cur_date=date('Y-m-d');
		
	$first_date= get_first_day_of_week($cur_date);
	$last_date= get_last_day_of_week($cur_date);
	
	$cur_month=date('m');
	$cur_year=date('Y');
	
	?>
	<div class="box tabs black_box">
		<h2 class="box-header">Reports</h2>
		
		<div class="box-content box-table">
		<div id="tabs-7">
		<table class="tablebox">
		<thead class="table-header">
		<tr>
			<th align="left" valign="middle" style="background:#FFFFFF; text-align:left !important; padding:0px 0px 0px 20px;" colspan="2"><b></b></th>
			<th>Users</th>
          
            <th>Designs</th>
           
			<th>Videos</th>
                        
                        <th>Challenges</th>
                        
                        <th>3D Stores</th>
			
             <th>Earn</th>

          
            
			
		</tr>
		</thead>
		<tbody>
			<tr class="even">
				
				<td class="tc" colspan="2" ><b>Today</b></td>
				
                <td><?php echo get_user_signup_date($cur_date,$cur_date); ?> </td>
                
                <td><?php echo get_design_date($cur_date,$cur_date); ?> </td>                
               <td><?php echo get_video_date($cur_date,$cur_date); ?> </td>
                <td><?php echo get_challenge_date($cur_date,$cur_date); ?></td>
                <td><?php echo get_store_date($cur_date,$cur_date); ?></td>
                
                
                <td><?php echo get_daily_earning_on_wallet($cur_date); ?></td>
               
				
				
			</tr>	
			
			<tr class="odd">
				
				<td class="tc" colspan="2"><b>This Week</b></td>
				<td><?php echo get_user_signup_date($first_date,$last_date); ?> </td>
                                
                                
                                <td><?php echo get_design_date($first_date,$last_date); ?> </td>                
               <td><?php echo get_video_date($first_date,$last_date); ?> </td>
                <td><?php echo get_challenge_date($first_date,$last_date); ?></td>
                <td><?php echo get_store_date($first_date,$last_date); ?></td>
                
                
                 <td><?php echo get_weekly_earning_on_wallet($first_date,$last_date); ?></td>
                 
                 
			</tr>	
			<tr class="even">
				
				<td class="tc" colspan="2"><b>This Month</b></td>
				<td><?php echo get_user_signup_month($cur_month,$cur_year); ?> </td>
                
                <td><?php echo get_design_month($cur_month,$cur_year); ?> </td>                
               <td><?php echo get_video_month($cur_month,$cur_year); ?> </td>
                <td><?php echo get_challenge_month($cur_month,$cur_year); ?></td>
                <td><?php echo get_store_month($cur_month,$cur_year); ?></td>
                
                <td><?php echo get_monthly_earning_on_wallet($cur_month,$cur_year); ?></td>
				
			</tr>
            
            <tr class="odd">
				
				<td class="tc" colspan="2"><b>This Year</b></td>
				<td><?php echo get_user_signup_year($cur_year); ?> </td>
                
                
                
              	<td><?php echo get_design_year($cur_year); ?> </td>
                <td><?php echo get_video_year($cur_year); ?></td>
		<td><?php echo get_challenge_year($cur_year); ?></td>
                <td><?php echo get_store_year($cur_year); ?></td>
                                
                                
                                
                <td><?php echo get_yearly_earning_on_wallet($cur_year); ?></td>
			</tr>
			
		</tbody>
		</table>
		</div>
		
	
        
        
		</div>
	</div>
	<div class="clear"></div>
	
	
    
     	
<!-- 1. Add these JavaScript inclusions in the head of your page -->

<script type="text/javascript" src="<?Php echo front_base_url().getThemeName(); ?>/js/jquery.min.js"></script>

<script type="text/javascript" src="<?Php echo upload_url(); ?>highcharts/highcharts.js"></script>


<!-- 1b) Optional: the exporting module -->
<script type="text/javascript" src="<?Php echo upload_url(); ?>highcharts/modules/exporting.js"></script>


<script type="text/javascript">
$(function () {
    var chart, chart1, chart2, chart3, chart4, chart5;
	
			/*exporting: {
				enabled: true
			  },
		*/
		
	
	
    $(document).ready(function() {
        
		
		
	chart = new Highcharts.Chart({
            chart: {
                renderTo: 'weeklyregistration',
                type: 'line',
                marginRight: 0,
                marginBottom: 25
            },
            title: {
                text: '',
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                categories: ['Mon', 'Tue', 'Wen', 'Thur', 'Fri', 'Sat', 'Sun']
            },
            yAxis: {
                title: {
                    text: 'Total'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': ' +this.y;
                }
            },
			
			
			
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'top',
                x: 10,
                y: 0,
                borderWidth: 0
            },
            series: [{
                name: 'Registration',
                data: [<?php if($weekly_registration) {  foreach($weekly_registration as $date => $total) { echo $total.','; } } ?>]
            }]
        });
		
	chart1 = new Highcharts.Chart({
            chart: {
                renderTo: 'weeklypin',
                type: 'line',
                marginRight: 0,
                marginBottom: 25
            },
            title: {
                text: '',
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                categories: ['Mon', 'Tue', 'Wen', 'Thur', 'Fri', 'Sat', 'Sun']
            },
            yAxis: {
                title: {
                    text: 'Total'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': ' +this.y;
                }
            },
			
			
			
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'top',
                x: 10,
                y: 0,
                borderWidth: 0
            },
            series: [{
                name: 'New',
                data: [<?php if($weekly_new_trip) {  foreach($weekly_new_trip as $date => $total) { echo $total.','; } } ?>]
            },{
                name: 'Assign',
                data: [<?php if($weekly_assign_trip) {  foreach($weekly_assign_trip as $date => $total) { echo $total.','; } } ?>]
            },{
                name: 'Complete',
                data: [<?php if($weekly_complete_trip) {  foreach($weekly_complete_trip as $date => $total) { echo $total.','; } } ?>]
            },{
                name: 'Expire',
                data: [<?php if($weekly_close_trip) {  foreach($weekly_close_trip as $date => $total) { echo $total.','; } } ?>]
            }]
        });
        
        
        
        
        
        chart2 = new Highcharts.Chart({
            chart: {
                renderTo: 'weeklydesignvideostore',
                type: 'line',
                marginRight: 0,
                marginBottom: 25
            },
            title: {
                text: '',
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                categories: ['Mon', 'Tue', 'Wen', 'Thur', 'Fri', 'Sat', 'Sun']
            },
            yAxis: {
                title: {
                    text: 'Total'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': ' +this.y;
                }
            },			
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'top',
                x: 10,
                y: 0,
                borderWidth: 0
            },
            series: [{
                name: 'Design',
                data: [<?php if($weekly_new_design) {  foreach($weekly_new_design as $date => $total) { echo $total.','; } } ?>]
            },{
                name: 'Video',
                data: [<?php if($weekly_new_video) {  foreach($weekly_new_video as $date => $total) { echo $total.','; } } ?>]
            },{
                name: 'Store',
                data: [<?php if($weekly_new_store) {  foreach($weekly_new_store as $date => $total) { echo $total.','; } } ?>]
            }]
        });
		
  
   });
    
});
		</script>  
	
    

    <div class="column half fl">
		
			<div class="box">
			<h2 class="box-header">Weekly Average Registrations</h2>
			<div class="box-content">
				
                
                <div id="weeklyregistration" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                
			</div>
			</div>
			
		</div>
        
        
        
        <div class="column half fr">
		
			<div class="box">
			<h2 class="box-header">Weekly Average Challenge</h2>
			<div class="box-content">
			    
                <div id="weeklypin" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                
			</div>
			</div>
			
		</div>
        
        
	<div class="clear"></div>
    

        
         <div class="column">
		
			<div class="box">
			<h2 class="box-header">Weekly Average Design - Video - 3D Store</h2>
			<div class="box-content">
				
                
                <div id="weeklydesignvideostore" style="min-width: 400px; height: 450px; margin: 0 auto"></div>
                
			</div>
			</div>
			
		</div>
    


	<div class="clear"></div>
</div>