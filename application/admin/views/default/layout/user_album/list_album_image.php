<script type="text/javascript" language="javascript">
	function delete_rec(id,msg,cid,offset)
	{
		var ans = confirm("Are you sure to delete this image?");
		if(ans)
		{
			location.href = "<?php echo site_url('image/delete_image'); ?>/"+id+"/"+msg+"/"+offset+"/"+cid;
		}else{
			return false;
		}
	}
	
	function getlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			window.location.href='<?php echo site_url('user_album/list_album_image/'.$album_id);?>/'+limit;
		}
	
	}	
	
	function getsearchlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			
			window.location.href='<?php echo site_url('user_album/search_all_album/'.$album_id);?>/'+limit+'/<?php echo $option.'/'.$keyword; ?>';
		}
	
	}
	
	function gomain(x)
	{
		
		if(x == 'all')
		{
			window.location.href= '<?php echo site_url('user_album/list_album_image/'.$album_id);?>';
		}
	}
	
	function chk_valid()
	{
		
		var keyword = document.getElementById('keyword').value;
		
		if(keyword=='')
		{
			alert('Please enter search keyword');	
			return false;
			
		}
		
		else
		{
			return true;			
		}
		
		
		
	}
</script>


<?php $album_details = get_album_by_id($album_id); 
?>

<div id="content">  
	<?php if($msg != ""){
		if($msg == "fail"){ $error = 'Record can not deleted Successfully.';}
		if($msg == "delete"){ $error = 'Record has been deleted Successfully.';}
    ?>
     <div class="column full">
            <span class="message information"><strong><?php echo $error;?></strong></span>
        </div>
    <?php } ?>
    
	<div class="clear"></div>
	<div class="column full">      
               
       <div class="box">		
		<div class="box themed_box">
   
		<h2 class="box-header">Images of Albums : <?php echo $album_details->album_name;?></h2>   
      	<div class="box-content box-table">       
            
              
                 <!-- board user-->            
                              
              <table class="tablebox" >
              
              <div id="topbar" style="border:#CCC solid 1px;">
                    <div style="float:left;">
                        <strong>Show</strong>
                            <?php if($search_type=='normal') { ?>
                            <select name="limit" id="limit" onchange="getlimit(this.value)" style="width:80px;">
                            <?php } if($search_type=='search') { ?>
                             <select name="limit" id="limit" onchange="getsearchlimit(this.value)" style="width:80px;">
                            <?php } ?>
                                <option value="0">Per Page</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="75">75</option>
                                <option value="100">100</option>
                            </select>
                   
                    
                       </div>
                     
                    
            <form name="frm_search" id="frm_search" method="post" action="<?php echo site_url('user_album/search_all_album/'.$album_id).'/'.$limit; ?>" onSubmit="return chk_valid();">
                     <strong>&nbsp;&nbsp;&nbsp;Search By</strong>&nbsp;
                        <select name="option" id="option" style="width:100px;" onchange="gomain(this.value)">
                         <option value="all">All</option> 
                           <option value="img.image_title" <?php if($option=='img.image_title'){?> selected="selected"<?php }?>>Image Title</option>	  
                           <option value="img.image_description" <?php if($option=='img.image_description'){?> selected="selected"<?php }?>>Image Description</option>
                               <option value="img.image_unique_code" <?php if($option=='img.image_unique_code'){?> selected="selected"<?php }?>>Unique Code</option>	
                               <option value="img.image_delete_code" <?php if($option=='img.image_delete_code'){?> selected="selected"<?php }?>>Delete Code</option>	
                            
                        </select>
        
                        <input type="text" name="keyword" id="keyword" value="<?php echo $keyword;?>" class="textfield"/>                
                        <input type="submit" name="submit" id="submit" value="Search" class="button themed" /> 
                     </form> 
                     
                     
                     <div style="float:right;">
            
				 &nbsp;
					
                 </div>
                 
                 
                 
                    
                 </div>  
                
                    <thead class="table-header">
                        <tr> 
                            <th class="first tc">Title</th>
                            	<th>Description</th>
                                <th>Image</th>
                                <th>User Name</th>
                            
                                <th>Views</th>
                                <th>Likes</th>
                                <th>Unique Code</th>
                                <th>Delete Code</th>
                                <th>Added Date</th>
                                <th>Comment</th>
                                <th>Action</th>                                           
                        </tr>
                    </thead>
                    
                    <tbody class="openable-tbody">
                    <?php 
	
					
						
				        $site_setting = site_setting();
						
                        if($image_details)
                        {
							
                            $i=0;
							
                            foreach($image_details as $img)
                            {
								
                                if($i%2=="0")
                                {
                                    $cl = "odd";	
                                }else{	
                                    $cl = "even";	
                                }
								
								
								 $user_profile=$this->user_album_model->get_user_profile($img->user_id);
								 
								
								 $login_user_profileimage=upload_url().'upload/no_userimage.png';
								 
								 if($user_profile){
									 if($user_profile->profile_image!='') {  
					
										if(file_exists(base_path().'upload/user_thumb/'.$user_profile->profile_image)) { 							
											$login_user_profileimage=upload_url().'upload/user_thumb/'.$user_profile->profile_image;
										} else {
											
											if(file_exists(base_path().'upload/user/'.$user_profile->profile_image)) { 							
												$login_user_profileimage=upload_url().'upload/user/'.$user_profile->profile_image;
											}
										} 
									 } 
								}
				 
				 
								
									$show_image='';
						
						
									if($img->image_name!='') {
																																
												$orig_name=$img->image_name;							
												
														 
												if(file_exists(base_path().'upload/image_thumb/'.$orig_name)) { 
													$show_image=upload_url().'upload/image_thumb/'.$orig_name;
												}
												else if(file_exists(base_path().'upload/image_medium/'.$orig_name)) { 
													$show_image=upload_url().'upload/image_medium/'.$orig_name;
												}
												else
												{				
													if(file_exists(base_path().'upload/image_orig/'.$orig_name)) {									
														$show_image=upload_url().'upload/image_orig/'.$orig_name;									
													}
												}
													  
										} 
								
								
								
								 $image_url_link='';

						if($img->image_url_title!='') { $image_url_link=front_base_url().$img->image_url_title;  } 
						else { $image_url_link= front_base_url().$img->image_unique_code; }
								
					?>
                    
                    
						<tr class="<?php echo $cl; ?>">
                        
                            <td><?php echo '<b>'.$img->image_title.'</b>'; ?></td>
                            
                            <td><?php if(strlen($img->image_description)>30) { echo substr($img->image_description,0,30); } else { echo $img->image_description; } ?></td>
                            
                            <td class="thumb-td tc"> 
                                <a class="lightbox" href="<?php echo $show_image;?>">
								<img src="<?php echo $show_image;?>"  />
                                </a>
                            </td>   
                            
                            
                            
                          	<td>
                            <?php if($img->user_id > 0){?>
                            <a href="<?php echo front_base_url().'user/'.$img->profile_name;?>" target="_blank" style="text-transform:capitalize;"><?php echo $img->first_name.' '.$img->last_name; ?></a>
                            <?php }else {?><?php echo "N/A";}?></td> 
                            
                          
                           <td><?php echo $img->image_view_count;?></td>
                             
                             
                              <td><?php echo $total_likes_on_image=get_total_image_likes($img->image_id);?></td>
                              
                              
                              
                            <td><?php  echo anchor($image_url_link,'<b>'.$img->image_unique_code.'</b>','target="_blank"');?></td>
                             <td><?php echo $img->image_delete_code;?></td>
                           
                            <td><?php echo date($site_setting->date_time_format,strtotime($img->image_date)); ?></td> 
                            
                            
                              <?php $total_comment = get_image_point_comment($img->image_id);?>
                            
                            <td><?php echo anchor('image/list_comment/'.$img->image_id,$total_comment.' Comments','class="button white" target="_blank"'); ?></td>        
                            
                            
                            <td>
								<?php echo anchor('image/image_detail/'.$img->image_id,'<span class="icon_single edit"></span>','class="button white" title="Edit image"'); ?>
                            	 <a href="#" onClick="delete_rec('<?php echo $img->image_id; ?>','user_board','<?php echo $album_id;?>','<?php echo $offset;?>')" class="button white"><span class="icon_single cancel"></span></a>
                            </td>   
                        </tr>
                        
								
					<?php	 						
							 $i++;
							 } }
					else { 
					?> 
                    
                   <tr class="alter"><td colspan="9" align="center" valign="middle" height="30">No Image has been added yet</td></tr>
					<?php 	}
					?> 	 
					
				</tbody>
				</table>
                
                <ul class="pagination">
					<?php echo $page_link; ?>
                </ul>
						
	<div class="clear"></div>
</div>         
<!-- board user-->     
           <div class="clear"></div> 
  	</div>          
       	</div>
</div>                    
     	</div>
         