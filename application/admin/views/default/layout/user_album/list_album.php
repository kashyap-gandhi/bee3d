<script type="text/javascript" language="javascript">
	function delete_rec(id)
	{
		var ans = confirm("Are you sure to delete this album?");
		if(ans)
		{
			location.href = "<?php echo site_url('user_album/delete_album/'.$user_id); ?>/"+id+"/<?php echo $limit.'/'.$offset; ?>";
		}else{
			return false;
		}
	}
	
	
	
	function getlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			window.location.href='<?php echo site_url('user_album/list_album/'.$user_id);?>/'+limit;
		}
	
	}	
	
	function getsearchlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			
			window.location.href='<?php echo site_url('user_album/search_all_user_album/'.$user_id);?>/'+limit+'/<?php echo $option.'/'.$keyword; ?>';
		}
	
	}
	
	function gomain(x)
	{
		
		if(x == 'all')
		{
			window.location.href= '<?php echo site_url('user_album/list_album/'.$user_id);?>';
		}
	}
	
	
	function chk_valid()
	{
		
		var keyword = document.getElementById('keyword').value;
		
		if(keyword=='')
		{
			alert('Please enter search keyword');	
			return false;
			
		}
		
		else
		{
			return true;			
		}
		
		
		
	}
	
</script>

<div id="content">  
	<?php if($msg != ""){
            if($msg == "insert"){ $error = 'New Record has been added Successfully.';}
            if($msg == "update"){ $error = 'Record has been updated Successfully.';}
            if($msg == "delete"){ $error = 'Record has been deleted Successfully.';}
			if($msg == "rights") {  $error = 'Rights has been updated Successfully.';}
			if($msg == "fail") {  $error = 'Record cannot deleted Successfully.';}
    ?>
        <div class="column full">
            <span class="message information"><strong><?php echo $error;?></strong></span>
        </div>
    <?php } ?>
	<div class="column full">
	
        <div class="box">		
            <div class="box themed_box">
            <h2 class="box-header">User Albums</h2>
                
                <div class="box-content box-table">
                <table class="tablebox">
                
                
                <div id="topbar" style="border:#CCC solid 1px;">
                    <div style="float:left;">
                        <strong>Show</strong>
                            <?php if($search_type=='normal') { ?>
                            <select name="limit" id="limit" onchange="getlimit(this.value)" style="width:80px;">
                            <?php } if($search_type=='search') { ?>
                             <select name="limit" id="limit" onchange="getsearchlimit(this.value)" style="width:80px;">
                            <?php } ?>
                                <option value="0">Per Page</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="75">75</option>
                                <option value="100">100</option>
                            </select>
                   
                    
                       </div>
                     
                    
            <form name="frm_search" id="frm_search" method="post" action="<?php echo site_url('user_album/search_all_user_album/'.$user_id).'/'.$limit; ?>" onSubmit="return chk_valid();">
                     <strong>&nbsp;&nbsp;&nbsp;Search By</strong>&nbsp;
                        <select name="option" id="option" style="width:100px;" onchange="gomain(this.value)">
                         <option value="all">All</option> 

 <option value="alb.album_name" <?php if($option=='alb.album_name'){?> selected="selected"<?php }?>>Album Name</option>
 <option value="us.first_name" <?php if($option=='us.first_name'){?> selected="selected"<?php }?>>First Name</option>
 <option value="us.last_name" <?php if($option=='us.last_name'){?> selected="selected"<?php }?>>Last Name</option>
    <option value="alb.album_unique_code" <?php if($option=='alb.album_unique_code'){?> selected="selected"<?php }?>>Unique Code</option>	
                            
                              
                              
                        </select>
        
                        <input type="text" name="keyword" id="keyword" value="<?php echo $keyword;?>" class="textfield"/>                
                        <input type="submit" name="submit" id="submit" value="Search" class="button themed" /> 
                     </form> 
                     
                     
                     <div style="float:right;">
            
				 &nbsp;
					
                 </div>
                 
                 
                 
                    
                 </div>
                 
                 
                 
                 
                   
                
                    <thead class="table-header">
                        <tr> 
                            <th class="first tc">Album Name</th>
                            <th>Unique Code</th>
                             <th>User Name</th>
                            <th>Album Cover</th>
                            <th>Images</th>
                             <th>Views</th>
                            <th>Album Date</th>
                            <th>Action</th>                                         
                        </tr>
                    </thead>
                    
                    <tbody class="openable-tbody">
                    <?php 
						
						$brd_cnt=0;
                        if($user_album)
                        {
							$brd_cnt=count($user_album);
                            $i=0;
							
                            foreach($user_album as $alb)
                            {
                                if($i%2=="0")
                                {
                                    $cl = "odd";	
                                }else{	
                                    $cl = "even";	
                                }
									 $get_image=get_album_total_image($alb->album_id);
								
									 $total_image=0;  if($get_image) { $total_image=$get_image; } 
									 
									 $user_profile=$this->user_album_model->get_user_profile($alb->album_id);
					?>
                    
						<tr class="<?php echo $cl; ?>">
                            <td><?php echo anchor(front_base_url().'a/'.$alb->album_unique_code,'<b>'.ucfirst($alb->album_name).'</b>');?></td>
                              
                             <td><?php echo $alb->album_unique_code; ?></td> 
                              
                               <td><a href="<?php echo front_base_url().'user/'.$alb->profile_name;?>" target="_blank" style="text-transform:capitalize;"><?php echo $alb->first_name.' '.$alb->last_name; ?></a></td>
                               
                            <td class="thumb-td tc">
                                <?php  $get_cover_image=get_album_cover_image($alb->album_id); 
								
										if($get_cover_image!='none') { 
                                ?>
                               			<a href="<?php echo front_base_url().'a/'.$alb->album_unique_code;?>" target="_blank"><img src="<?php echo $get_cover_image;?>" width="50px" height="50px"></a><?php
										} else { ?>
                                	 <img src=""  id="coverimage<?php echo $alb->album_id;?>" width="50px" height="50px"  />
                                <?php } ?>
                            </td>
                            <?php 
								$total_image=get_album_total_image($alb->album_id); 
							?>
                         
                            <td><?php echo anchor('user_album/list_album_image/'.$alb->album_id,'<b>'.$total_image.'</b>-Images'); ?></td>
                            
                            <td><?php echo $alb->album_view_count; ?></td>
                            
                            <td><?php echo date($site_setting->date_time_format,strtotime($alb->album_date));?></td>
                            
                            <td><?php echo anchor('user_album/edit_album/'.$alb->album_id.'/0','<span class="icon_single edit"></span>','class="button white" id="admin_'.$alb->album_id.'" title="Edit Album"'); ?>
                            <a href="javascript:void(0)" onClick="delete_rec('<?php echo $alb->album_id; ?>','album','0','<?php echo $offset;?>')" class="button white"><span class="icon_single cancel"></span></a>
                            </td>  
                                
                        </tr>
								
					<?php			
							
							 $i++;
							}
						} else { ?>
						<tr class="alter"><td colspan="8" align="center" valign="middle" height="30">No Album has been added yet</td></tr>
					<?php 	}  ?> 
					</tr>
				</tbody>
				</table>
                
                 <ul class="pagination">
					<?php echo $page_link; ?>
                </ul>
              
                   
    
               </div>
           </div>
        </div>
    </div>