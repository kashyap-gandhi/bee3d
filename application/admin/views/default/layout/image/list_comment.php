<script type="text/javascript" language="javascript">
	function delete_rec(id,offset)
	{
		var ans = confirm("Are you sure to delete this report type?");
		if(ans)
		{
			location.href = "<?php echo site_url('user/delete_comment'); ?>/"+id+"/"+offset;
		}else{
			return false;
		}
	}

		
	
	function getlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			window.location.href='<?php echo site_url('image/list_comment/'.$image_id);?>/'+limit;
		}
	
	}	
	
	function getsearchlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			
			window.location.href='<?php echo site_url('image/search_all_comments/'.$image_id);?>/'+limit+'/<?php echo $option.'/'.$keyword; ?>';
		}
	
	}
	
	function gomain(x)
	{
		
		if(x == 'all')
		{
			window.location.href= '<?php echo site_url('image/list_comment/'.$image_id);?>';
		}
	}
	function chk_valid()
	{
		
		var keyword = document.getElementById('keyword').value;
		
		if(keyword=='')
		{
			alert('Please enter search keyword');	
			return false;
			
		}
		
		else
		{
			return true;			
		}
		
		
		
	}
	
	function setchecked(elemName,status){
	elem = document.getElementsByName(elemName);
	for(i=0;i<elem.length;i++){
		elem[i].checked=status;
	}
}

function setaction(elename, actionval, actionmsg, formname) {
	vchkcnt=0;
	elem = document.getElementsByName(elename);
	
	for(i=0;i<elem.length;i++){
		if(elem[i].checked) vchkcnt++;	
	}
	if(vchkcnt==0) {
		alert('Please select a record')
	} else {
		
		if(confirm(actionmsg))
		{
			document.getElementById('action').value=actionval;	
			document.getElementById(formname).submit();
		}		
		
	}
}
	
</script>
<div id="content">  
	<?php if($msg != ""){ $error = '';
            if($msg == "fail"){ $error = 'Record can not deleted Successfully.';}
			 if($msg == "insert"){ $error = 'New Record has been added Successfully.';}
            if($msg == "update"){ $error = 'Record has been updated Successfully.';}
            if($msg == "delete"){ $error = 'Record has been deleted Successfully.';}
			if($msg == "active"){ $error = 'Record has been activated Successfully.';}
			  if($msg == "inactive"){ $error = 'Record has been inactivated Successfully.';}
    ?>
        <div class="column full">
            <span class="message information"><strong><?php echo $error;?></strong></span>
        </div>
    <?php } ?>
	<div class="clear"></div>
	<div class="column full">
	
	<div class="box">		
		<div class="box themed_box">
		<h2 class="box-header">Image Comments </h2>
			
			<div class="box-content box-table">
			<table class="tablebox">  
            
				<div id="topbar" style="border:#CCC solid 1px;">
                    <div style="float:left;">
                        <strong>Show</strong>
                            <?php if($search_type=='normal') { ?>
                            <select name="limit" id="limit" onchange="getlimit(this.value)" style="width:80px;">
                            <?php } if($search_type=='search') { ?>
                             <select name="limit" id="limit" onchange="getsearchlimit(this.value)" style="width:80px;">
                            <?php } ?>
                                <option value="0">Per Page</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="75">75</option>
                                <option value="100">100</option>
                            </select>
                   
                    
                       </div>
                     
                    
            <form name="frm_search" id="frm_search" method="post" action="<?php echo site_url('image/search_all_comments/'.$image_id).'/'.$limit; ?>" onSubmit="return chk_valid();">
                     <strong>&nbsp;&nbsp;&nbsp;Search By</strong>&nbsp;
                        <select name="option" id="option" style="width:100px;" onchange="gomain(this.value)">
                         <option value="all">All</option> 
                         <option value="imc.user_comment" <?php if($option=='imc.user_comment'){?> selected="selected"<?php }?>>Comment</option>
                          <option value="im.image_title" <?php if($option=='im.image_title'){?> selected="selected"<?php }?>>Image Title</option>
                         <option value="im.image_description" <?php if($option=='im.image_description'){?> selected="selected"<?php }?>>Image Description</option>
                         <option value="im.image_unique_code" <?php if($option=='im.image_unique_code'){?> selected="selected"<?php }?>>Image Unique Code</option>
                        
                         <option value="us.first_name" <?php if($option=='us.first_name'){?> selected="selected"<?php }?>>First Name</option>
                         <option value="us.last_name" <?php if($option=='us.last_name'){?> selected="selected"<?php }?>>Last Name</option>     
                        </select>
        
                        <input type="text" name="keyword" id="keyword" value="<?php echo $keyword;?>" class="textfield"/>                
                        <input type="submit" name="submit" id="submit" value="Search" class="button themed" /> 
                     </form> 
                     
                     
                     <div style="float:right;">
            
			 <form name="frm_listallimagecomment" id="frm_listallimagecomment" action="<?php echo site_url('image/action_image_comment/'.$image_id);?>" method="post">
          <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
          <input type="hidden" name="limit" id="limit" value="<?php echo $limit; ?>" />
          <input type="hidden" name="option" id="option" value="<?php echo $option; ?>" />
          <input type="hidden" name="keyword" id="keyword" value="<?php echo $keyword; ?>" />
          <input type="hidden" name="search_type" id="search_type" value="<?php echo $search_type; ?>" />
          
           <input type="hidden" name="action" id="action" />
				
               
               <a href="javascript:void(0)"  onclick="setaction('chk[]','delete', 'Are you sure, you want to delete selected Comment(s)?', 'frm_listallimagecomment')"  class="button white" ><span class="icon_text cancel"></span>Delete</a>
                  
                  
                  
                  <a href="javascript:void(0)"  onclick="setaction('chk[]','active', 'Are you sure, you want to active selected Comment(s)?', 'frm_listallimagecomment')" class="button white" >Active</a>
                  
                  <a href="javascript:void(0)"  onclick="setaction('chk[]','inactive', 'Are you sure, you want to inactive selected Comment(s)?', 'frm_listallimagecomment')" class="button white">Inactive</a>
					
                 </div>
                 
                 
                 
                    
                 </div>
                 
                 
          		<thead class="table-header">
					<tr> 
                      <th class="first tc"><a href="javascript:void(0)" onClick="javascript:setchecked('chk[]',1)" style="color:#000;"><?php echo "Check All"; ?></a> |
           <a href="javascript:void(0)" onClick="javascript:setchecked('chk[]',0)" style="color:#000;"><?php echo "Clear All"; ?></a></th><th>Comment</th>
                      
                     	 <th>Image Title</th>
                          <th>Comment Date</th>
                           <th>Likes</th>
                     
                        <th>User Name</th>
                        <th>Image</th>
                        <th>IP</th> 
                  
                        <th>Image Unique Code</th>                                  
                        <th>Status</th>
                        <th>Action</th>        
                        
                    	
                                                        
                    </tr>
                </thead>
                
                <tbody class="openable-tbody">
				<?php
                    if($result)
                    {
                        $i=0;
                        foreach($result as $row)
                        {
                            if($i%2=="0")
                            {
                                $cl = "odd";	
                            }else{	
                                $cl = "even";	
                            }
							
							
							$pin_show_image='';
							
							if($row->image_name!='') {
																					
									$orig_name=$row->image_name;											
											 
									if(file_exists(base_path().'upload/image_thumb/'.$orig_name)) 
									{ 
										$pin_show_image=upload_url().'upload/image_thumb/'.$orig_name;
									}
									else if(file_exists(base_path().'upload/image_medium/'.$orig_name)) 
									{ 
										$pin_show_image=upload_url().'upload/image_medium/'.$orig_name;
									}
									else
									{				
										if(file_exists(base_path().'upload/image_orig/'.$orig_name)) 
										{									
											$pin_show_image=upload_url().'upload/image_orig/'.$orig_name;									
										}
									}
										  
							} 
											
											
											
                  ?>
					<tr class="<?php echo $cl; ?>">
                   		 <td style="width:140px;"><input type="checkbox" name="chk[]" id="chk" value="<?php echo $row->image_comment_id;?>" /></td>
                         
                         <td><?php echo substr($row->user_comment,0,30) ; ?></td>
                        
                          <td><?php if($row->image_title!='') { echo anchor('image/image_detail/'.$row->image_id.'/0','<b>'.$row->image_title.'</b>'); } else {  echo anchor('image/image_detail/'.$row->image_id.'/0','<b>N/A</b>');  } ?></td>
                     
                       <td><?php echo date($site_setting->date_time_format,strtotime($row->comment_date));?></td>
                       
                       
                        <td><?php echo $total_comment_likes=get_comment_likes($row->image_comment_id); ?></td>
                        
                        
                        <td><a href="<?php echo front_base_url().'user/'.$row->profile_name;?>" target="_blank" style="text-transform:capitalize;"><?php echo $row->first_name.' '.$row->last_name; ?></a>
						
						</td>
                      <td  class="thumb-td tc">
						
						<?php if($pin_show_image!='') { ?>
                                
                          <img src="<?php echo $pin_show_image; ?>" width="50px" height="50px"  />
                          <?php } ?></td>
                          
                          
                          
                        <td><?php echo $row->comment_ip;?></td>
                       
                        <td><?php echo $row->image_unique_code;?></td>
                        
                        <td><?php if($row->comment_status==1) { echo "Active"; } if($row->comment_status==0) { echo "Inactive"; } ?></td>
                        
                        
                        <td><?php echo anchor('user/comment_detail/'.$row->image_comment_id.'/'.$offset.'/ret','<span class="icon_single edit"></span>','class="button white" id="admin_'.$row->image_comment_id.'" title="Edit Board"'); ?></td>
                        
                    
                       
                        	
							
							
                    </tr>
					<?php
							$i++;
							}
						} else { ?>
						<tr class="alter"><td colspan="15" align="center" valign="middle" height="30">No Comment has been added yet</td></tr>
					<?php 	}
					?> 
                  	</tr>
                </tbody>
                </table>
                <ul class="pagination">
					<?php echo $page_link; ?>
                </ul>

           </div>
       </div>
    </div>
</div>