
<div id="content" align="center">
	<?php if($msg != ""){
            if($msg == "insert"){ $error = 'New Record has been added Successfully.';}
            if($msg == "update"){ $error = 'Record has been updated Successfully.';}
            if($msg == "delete"){ $error = 'User has been deleted Successfully.';}
			if($msg == "edit"){ $error = 'User status has been changed Successfully.';}
    ?>
        
    <?php } 
    if($error!=''){?>
    <div class="column full">
            <span class="message information"><strong><?php echo $error;?></strong></span>
        </div>
       <?php }?> 
    
	<div class="clear"></div>
	
    
	<div align="left" class="column half">
		<div class="box">	
            <h2 class="box-header"><?php
			
			 			$image_url_link='';

						if($image_details->image_url_title!='') { $image_url_link=front_base_url().$image_details->image_url_title;  } 
						else { $image_url_link= front_base_url().$image_details->image_unique_code; }
						
						
						
			
			 echo anchor($image_url_link, $image_title,'style="color: #0669AF;" target="_blank"'); ?></h2> 
			<div class="box-content">
            
           
			
			  <?php
					$attributes = array('name'=>'frm_image');
					echo form_open_multipart('image/update_image/'.$image_id,$attributes);
			  ?>
            
			  <table class="tablebox">
                  <tbody class="openable-tbody">
                  
                     <tr><td colspan="2" align="left" valign="top">
                      
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      <tr><th colspan="2"><strong><i><h3>Image Detail</i></h3></strong></th></tr>
                      
                      <tr>
                      	<td style="text-align:left; width:30%;"><label class="form-label">Image Description</label></td>
                        <td style="text-align:left; width:70%;">:  <input type="text" name="image_description" id="image_description" value="<?php echo $image_description; ?>" class="form-field width60"/> </td>
                      </tr>
					  
					   <tr>
                      	<td style="text-align:left; width:30%;"><label class="form-label">Image Title</label></td>
                        <td style="text-align:left; width:70%;">:  <input type="text" name="image_title" id="image_title" value="<?php echo $image_title; ?>" class="form-field width60"/> </td>
                      </tr>
                      
                       <tr>
                      	<td style="text-align:left; width:30%;"><label class="form-label">Image Original Name</label></td>
                        <td style="text-align:left; width:70%;">:  <?php echo $image_original_name; ?></td>
                      </tr>
                      
                   
					   <tr>
                      	<td style="text-align:left; width:30%;"><label class="form-label">Show On Home Page</label></td>
                        <td style="text-align:left; width:70%;">:  <input type="radio" name="show_on_home_page" id="show_on_home_page_no" value="0" <?php if($show_on_home_page == 0){?>checked="checked"<?php }?>/>&nbsp;NO  <input type="radio" name="show_on_home_page" id="show_on_home_page_yes" value="1" <?php if($show_on_home_page == 1){?>checked="checked"<?php }?>/>&nbsp;Yes</td>
                      </tr>
                      
                         <tr> <?php 
						 		if($image_user_id > 0){
									$user_profile=get_user_name($image_user_id);
								}else
								{
									$user_profile = 'N/A';
								}
								?>
                      		<td style="text-align:left; width:30%;"><label class="form-label">User Name</label></td>
                        	<td style="text-align:left; width:70%;">:<?php echo $user_profile; ?></td>
                      </tr>
                      
                       <tr> 
                       		<?php $image_point_comment = get_image_point_comment($image_id);?>	
                      		<td style="text-align:left; width:30%;"><label class="form-label">Image Point</label></td>
                        	<td style="text-align:left; width:70%;">:<?php echo $image_point_comment; ?></td>
                      </tr>
                      
                       <tr> 
                       		<td style="text-align:left; width:30%;"><label class="form-label">Image Unique Code</label></td>
                        	<td style="text-align:left; width:70%;">:<?php echo $image_unique_code; ?></td>
                      </tr>
                      
                      <tr> 
                       		<td style="text-align:left; width:30%;"><label class="form-label">Image Delete Code</label></td>
                        	<td style="text-align:left; width:70%;">:<?php echo $image_delete_code; ?></td>
                      </tr>
                      
                      <tr> 
                       		<td style="text-align:left; width:30%;"><label class="form-label">Image View Count</label></td>
                        	<td style="text-align:left; width:70%;">:<?php echo $image_view_count; ?></td>
                      </tr>
                     
                      <tr><td colspan="2"><hr/></td></tr>
                      
                      <tr><th colspan="2"><strong><i><h3>Image Detail</i></h3></strong></th></tr>
                      			
                    <tr>
                      	<td><label class="form-label">Image</label></td>
                        <td>
						<?php $show_image=front_base_url().'upload/image_orig/'.$image_name;
						$image_path = base_path().'upload/image_orig/'.$image_name;
						$image_size = get_file_size($image_path);
						?>
						
      <img src="<?php echo $show_image;?>" border="0"  id="main_image_opc" alt="<?php echo $image_description; ?>" style="width:450px; height:350px;"/>
     <br />	<br />	<input type="file" name="image_file_up" id ="image_file_up" class="form-field width60"/>
      
                        </td>
                      </tr>
                      
                    <tr> 
                       		<td style="text-align:left; width:30%;"><label class="form-label">Image Size</label></td>
                        	<td style="text-align:left; width:70%;">:<?php echo $image_size."KB"; ?></td>
                      </tr>
                      
                      <?php $image_info = image_setting();?>
                       <tr> 
                       		<td style="text-align:left; width:30%;"><label class="form-label">Image Width</label></td>
                        	<td style="text-align:left; width:70%;">:<?php echo $image_info->image_medium_thumb_width; ?></td>
                      </tr> 
                      
                       <tr> 
                       		<td style="text-align:left; width:30%;"><label class="form-label">Image Height</label></td>
                        	<td style="text-align:left; width:70%;">:<?php echo $image_info->image_medium_thumb_height; ?></td>
                      </tr> 
                  
                  <tr>
                      <td colspan="2">
                      <input type="hidden" name="image_id" id="image_id" value="<?php echo $image_id; ?>" />
                      <input type="submit" class="button themed" name="submit" id="submit" value="Update" />
                      </td>
                  </tr>
                      
                      </table>
                       
                      
                  </tbody>
			  	 </table>   
             </div>
              


			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>