<script type="text/javascript" language="javascript">
    function delete_rec(id,offset)
    {
        var ans = confirm("Are you sure to delete Category?");
        if(ans)
        {
            location.href = "<?php echo site_url('/category/delete_design_category/pending'); ?>/"+id+"/"+offset;
        }else{
            return false;
        }
    }
</script>

<div id="content">        
    <?php
    if ($msg != "") {
        if ($msg == "insert") {
            $error = 'New Record has been added Successfully.';
        }
        if ($msg == "update") {
            $error = 'Record has been updated Successfully.';
        }
        if ($msg == "delete") {
            $error = 'Record has been deleted Successfully.';
        }
        if ($msg == "notfound") {
            $error = 'Record cannot found.';
        }
        ?>
        <div class="column full">
            <span class="message information"><strong><?php echo $error; ?></strong></span>
        </div>
<?php } ?>
    <div class="clear"></div>
    <div class="column full">

        <div class="box">		
            <div class="box themed_box">
                <h2 class="box-header">Pending Design Categories</h2>

                <div class="box-content box-table">
                    <table class="tablebox" id="tabledata">

                        <div id="topbar" style="border:#CCC solid 1px;">
                            <!--<div style="float:left;">
                          <span class="tag tag-red">NOTE : </span> <span class="typo"><a class="spam">You can delete the category which have No Boards.</a></span>
                              </div>-->



                            <div style="float:right;"> 
                                <form name="frm_listuser" id="frm_listuser" action="<?php //echo site_url('newsletter/action_newsletter'); ?>" method="post">

                                    <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
                                    <input type="hidden" name="action" id="action" />
                                    &nbsp;<?php echo anchor('category/pending_design_category', '<span class="icon_text edit"></span>Pending Category', 'class="button white" style="margin: 0px;" id="Pending Category" title="Pending Category"'); ?>
                                    
                                    &nbsp;<?php echo anchor('category/add_design_category', '<span class="icon_text addnew"></span>Add Category', 'class="button white" style="margin: 0px;" id="Add Category" title="Add Category"'); ?>
                                    
                                   




                            </div>
                        </div>

                        <thead class="table-header">


                            <tr> 
                                <th class="first tc">image</th>
                                <th>Category</th>

                                <th>Parent</th>

                                <th>Status</th>                                    
                                <th>Action</th>
                            </tr>
                        </thead>


                        <tbody class="openable-tbody">
                            <?php
                            if ($result) {
                                $i = 0;
                                foreach ($result as $row) {
                                    if ($i % 2 == "0") {
                                        $cl = "odd";
                                    } else {
                                        $cl = "even";
                                    }
                                    ?>
                                    <tr onclick="<?php echo $cl; ?>(this);" class="<?php echo $cl; ?>">


                                        <td  class="thumb-td tc"><a class="lightbox" href="<?php echo upload_url(); ?>upload/category_orig/<?php echo $row->category_image; ?>" title="<?php echo $row->category_image; ?>"><img src="<?php echo upload_url(); ?>upload/category/<?php echo $row->category_image; ?>" alt="image-gallery"/></a></td>
                                        <td><?php echo $row->category_name; ?></td>

                                        <td><?php
                                        
                                        if($row->category_parent_id>0){
                                            
                                            $get_parent_cat=$this->category_model->get_one_design_category($row->category_parent_id);
                                            
                                            if($get_parent_cat){
                                                echo $get_parent_cat['category_name'];
                                            }
                                            
                                        }
                                        
                                        ?></td>



                                        <td><?php if ($row->category_status == "1") {
                                        echo "Active";
                                    } else {
                                        echo "Inactive";
                                    } ?></td>


                                        <td><?php echo anchor('category/edit_design_category/' . $row->category_id . '/' . $offset, '<span class="icon_single edit"></span>', 'class="button white" id="cat_' . $row->category_id . '" title="Edit Category"'); ?>


                                    <?php
                                    $check_sub_category = $this->category_model->check_design_category_have_sub_category($row->category_id);

                                    if ($check_sub_category == 0) {
                                        ?>   <a href="#" onClick="delete_rec('<?php echo $row->category_id; ?>','<?php echo $offset; ?>')" class="button white"><span class="icon_single cancel"></span></a>
                                    <?php } ?>

                                        </td>

                                    </tr>
                                    <?php
                                    $i++;
                                }
                            } else {
                                ?>        

                                <tr class="alter">
                                    <td colspan="15" align="center" valign="middle" height="30">No Records found.</td></tr>

<?php } ?>  
                        </tbody>
                    </table>


                    </form>
                </div>
            </div>
        </div>
    </div>