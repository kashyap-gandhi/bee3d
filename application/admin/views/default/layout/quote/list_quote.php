<script type="text/javascript" language="javascript">
	function delete_rec(id,offset)
	{
		var ans = confirm("Are you sure to delete user?");
		if(ans)
		{
			location.href = "<?php echo site_url('user/delete_user'); ?>/"+id+"/"+offset;
		}else{
			return false;
		}
	}
	
	function getlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			window.location.href='<?php echo site_url('quote/list_all_quote');?>/'+limit;
		}
	
	}	
	
	function getsearchlimit(limit)
	{
		if(limit=='0')
		{
		return false;
		}
		else
		{
			
			window.location.href='<?php echo site_url('quote/search_list_quote');?>/'+limit+'/<?php echo $option.'/'.$keyword; ?>';
		}
	
	}
	
	function gomain(x)
	{
		
		if(x == 'all')
		{
			window.location.href= '<?php echo site_url('quote/list_all_quote');?>';
		}
	}
	
	function chk_valid()
	{
		
		var keyword = document.getElementById('keyword').value;
		
		if(keyword=='')
		{
			alert('Please enter search keyword');	
			return false;
			
		}
		
		else
		{
			return true;			
		}
		
		
		
	}
	
</script>
 
<div id="content">        
	<?php if($msg != ""){ 
            if($msg == "approve"){ $error = 'Quote has been approved Successfully.';}
            if($msg == "update"){ $error = 'Record has been updated Successfully.';}
            if($msg == "delete"){ $error = 'Record has been deleted Successfully.';}
			elseif($msg == 'password'){ $error = 'Password has been changed Successfully.';}
    ?>
        <div class="column full">
            <span class="message information"><strong><?php echo $error;?></strong></span>
        </div>
    <?php } ?>
	<div class="clear"></div>
	<div class="column full">
	
	<div class="box">		
		<div class="box themed_box">
		<h2 class="box-header">All Quote List </h2>
			
			<div class="box-content box-table">
			<table class="tablebox">
            
                <div id="topbar" style="border:#CCC solid 1px;">
                    <div style="float:left;">
                        <strong>Show</strong>
                            <?php if($search_type=='normal') { ?>
                            <select name="limit" id="limit" onchange="getlimit(this.value)" style="width:80px;">
                            <?php } if($search_type=='search') { ?>
                             <select name="limit" id="limit" onchange="getsearchlimit(this.value)" style="width:80px;">
                            <?php } ?>
                                <option value="0">Per Page</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="75">75</option>
                                <option value="100">100</option>
                            </select>
                   
                    
                       </div>
                     
                    
                     <form name="frm_search" id="frm_search" method="post" action="<?php echo site_url('quote/search_list_quote'); ?>/<?php echo $limit; ?>" onSubmit="return chk_valid();">
                     <strong>&nbsp;&nbsp;&nbsp;Search By</strong>&nbsp;
                        <select name="option" id="option" style="width:100px;" onchange="gomain(this.value)">
                            <option value="all">All</option> 
                            <option value="from" <?php if($option=='from'){?> selected="selected"<?php }?>>From</option>
                             <option value="to"  <?php if($option=='to'){?> selected="selected"<?php }?>>To</option> 
                        </select>
        
                        <input type="text" name="keyword" id="keyword" value="<?php echo $keyword;?>" class="textfield"/>                
                        <input type="submit" name="submit" id="submit" value="Search" class="button themed" /> 
                     </form> 
                     
                     
                    <!-- <div style="float:right;">
            
				 &nbsp;<?php //echo anchor('user/add_user','<span class="icon_text addnew"></span>Add User','class="button white" style="margin: 0px;" id="Add User" title="Add User"'); ?>
					
                 </div>-->
                 
                 
                 
                    
                 </div>

				<thead class="table-header">
					<tr> 
                       
                        <th class="first tc">Email</th>
                        <th>Type(Air ticket/Trip)</th> 
                        <th>From</th>                                    
                        <th>To</th>
                         <th>Agent Name</th>
                         <th>Status</th>
                         <th>Date</th>
                        <th>Preview</th>
                      
					</tr>
				</thead>
				
				<tbody class="openable-tbody">
				<?php
                    if($result)
                    {
                        $i=0;
                        foreach($result as $row)
                        {
                            if($i%2=="0")
                            {
                                $cl = "odd";	
                            }else{	
                                $cl = "even";	
                            }
							
						$quote_user_info = $this->agent_model->get_agent_by_id($row->user_id);
						//print_r($quote_user_info);
						
                  ?>
					<tr class="<?php echo $cl; ?>">
                        
                        <td><?php echo $row->trip_quote_email; ?></td>
                        
                        <td>
                        	<?php if($row->quote_type == 1){
								echo "Air ticket";
								}else{
								echo "Trip";
								}
							?>
                        </td>
                        
                        <td><?php 
						if($row->quote_type == 1){
						echo $row->airline_from;
						}
						else
						{
							echo $row->trip_from_place;
						} ?></td>
                        
                         <td><?php
						 if($row->quote_type == 1){
						  echo $row->airline_to;
						  }
						  else
						  {
						  	echo $row->trip_to_place;
						  } ?></td>
                        
                        <td class="tc"><?php echo anchor(front_base_url().'user/'.$quote_user_info->profile_name,ucfirst($quote_user_info->first_name).' '.ucfirst($quote_user_info->last_name).'('.$quote_user_info->agency_name.')',' style="color:#004C7A;" target="_blank"'); ?>
                        </td>
                        
                        <td><?php if($row->trip_quote_approve==1) { ?> 
                        
                        <img src="<?php echo base_url().getThemeName();?>/gfx/accept.png" border="0" />
                         <?php } else { ?>
                         
                             <img src="<?php echo base_url().getThemeName();?>/gfx/cancel.png" border="0" /> 
                         <?php } ?></td>
                        
                         <td><?php echo date($site_setting->date_time_format,strtotime($row->quote_date)); ?></td>
                         
                        <td>
						<?php 
						if($row->quote_type == 1){
						echo anchor('quote/air_quote_detail/'.$row->quote_id,'<span class="icon_single edit"></span>',' title="View Quote" class="button white" id="viewquote_'.$row->quote_id.'" ');
						}
						else
						{
							echo anchor('quote/trip_quote_detail/'.$row->quote_id,'<span class="icon_single edit"></span>',' title="View Quote" class="button white" id="viewquote_'.$row->quote_id.'" ');
						}
						?>
                        </td>
                        
                        <script>
							jQuery("#viewquote_"+<?php echo $row->quote_id;?>).fancybox({width : '100px'});
						</script>
                        
                        <!--<td><?php  //echo anchor('quote/quote_detail/'.$row->user_id,'View Detail','class="button white"');  ?></td>-->
                  	</tr>
                   
                    
				  <?php
                            $i++;
                        }
                    }
                  else { ?>
						<tr class="alter"><td colspan="15" align="center" valign="middle" height="30">No User has been added yet</td></tr>
					<?php 	}
					?> 	
				</tbody>
			</table>
				
               <ul class="pagination">
					<?php echo $page_link; ?>
                </ul>
			</div>
		</div>
	</div>
</div>