<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


	
	
	// --------------------------------------------------------------------

	/**
	 * Site Base Path
	 *
	 * @access	public
	 * @param	string	the Base Path string
	 * @return	string
	 */
	function base_path()
	{		
		$CI =& get_instance();
		return $base_path = $CI->config->slash_item('base_path');		
	}
	
	
	// --------------------------------------------------------------------

	/**
	 * Site Front Url
	 *
	 * @access	public
	 * @param	string	the Front Url string
	 * @return	string
	 */
	/*function front_base_url()
	{		
		$CI =& get_instance();
		return $base_path = $CI->config->slash_item('base_url_site');		
	}*/
	
	// --------------------------------------------------------------------

	/**
	 * Site Front ActiveTemplate
	 *
	 * @access	public
	 * @param	string	current theme folder name
	 * @return	string
	 */
	function upload_url()
     {
          $CI =& get_instance();
			   return $base_path = $CI->config->slash_item('base_url_site');
	}
	
	function front_base_url()
	{
		$CI =& get_instance();
		$chk_index=$CI->config->slash_item('index_page');
		
		if($chk_index!='') { 
			$base_path = $CI->config->slash_item('base_url_site').$chk_index;
		} else { 
			$base_path = $CI->config->slash_item('base_url_site');
		}
		return $base_path ;
	}
	function getThemeName()
	{
		
		$default_theme_name='default';
		
		$CI =& get_instance();
		$query = $CI->db->get_where("template_manager",array('active_template'=>1 ,'is_admin_template'=>1));
		$row = $query->row();
		
		$theme_name=trim($row->template_name);
		
		if(is_dir(APPPATH.'views/'.$theme_name))
		{
			return $theme_name;
		}
		else
		{
			return $default_theme_name;	
		}
		
	}
	
		
	
	// --------------------------------------------------------------------

	/**
	 * Check user login
	 *
	 * @return	boolen
	 */
	function check_admin_authentication()
	{		
		$CI =& get_instance();
		
			if($CI->session->userdata('admin_id')!='')
			{
				return true;
			}
			else
			{
				return false;
			}
	
	}
	
	
	// --------------------------------------------------------------------

	/**
	 * get login user id
	 *
	 * @return	integer
	 */
	function get_authenticateUserID()
	{		
		$CI =& get_instance();
		return $CI->session->userdata('user_id');
	}
	
	
	/***** get last admin login detail
	**
	***/
	
	function get_last_admin_login_detail()
	{
		$CI =& get_instance();
		
		$CI->db->select('adl.*,ad.username,ad.email,ad.admin_type');
		$CI->db->from('admin_login adl');
		$CI->db->join('admin ad','adl.admin_id=ad.admin_id','left');
		$CI->db->order_by('adl.login_id','desc');
		$CI->db->limit(1);
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();
			
			return $res->login_ip.'('.date('d M, Y h:i:s A',strtotime($res->login_date)).')';
		}
	
		return $_SERVER['REMOTE_ADDR'].'('.date('d M, Y h:i:s A').')';
		
	}
	
	
	
	
	
	/*** get total user
	*
	**/
	
	function get_total_user()
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('user');
                $CI->db->join('user_profile','user.user_id=user_profile.user_id','left');
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;
	}
	
	
	
	
	
	
	/**** current login user
	**
	***/
	
	function get_current_login_user()
	{
		
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('user_login');
		$CI->db->where('DATE(login_date_time)',date('Y-m-d'));
		$CI->db->where('login_status',1);
		
		$query=$CI->db->get();
		
		
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;
	
	}
	
	
	/*** daily login user
	**
	**/
	
	function get_daily_login_user()
	{	
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('user_login');
		$CI->db->join('user','user_login.user_id=user.user_id','left');
		$CI->db->join('user_profile','user.user_id=user_profile.user_id','left');
		$CI->db->where('DATE(login_date_time)',date('Y-m-d'));	
		$CI->db->group_by('user_login.user_id');
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;
		
	}
	
	
	/*** get total number of city
	**
	**/
	
	function get_total_city()
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('city');
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;
	}
	
	
	/*** get total number of state
	**
	**/
	
	function get_total_state()
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('state');
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;
	}
	
	
	/*** get total number of country
	**
	**/
	
	function get_total_country()
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('country');
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;
	}
	
	
	
	
	
	
	function get_total_design()
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('design dg');
		$CI->db->join('user us','dg.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
	
	
	
	
	function get_total_video()
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('video vd');
                $CI->db->join('user us','vd.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
	
        
        function get_total_challenge()
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('challenge ch');
                $CI->db->join('user us','ch.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
	
	function get_total_store()
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('store st');
                $CI->db->join('user us','st.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
	
	
	
	function get_total_comment()
	{
		$CI =& get_instance();
			
		$CI->db->select('*');
		$CI->db->from('image_comment cmt');
		$CI->db->join('user us','cmt.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
	
	
	
	
	
	/***********daily************/
	
	function get_user_signup_date($first_date,$last_date)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('user us');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('DATE(us.sign_up_date) >= ',$first_date);
		$CI->db->where('DATE(us.sign_up_date) <= ',$last_date);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
		
		
	}
	
	
	
	
	function get_design_date($first_date,$last_date)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('design dg');
		$CI->db->join('user us','dg.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('DATE(dg.design_date) >= ',$first_date);
		$CI->db->where('DATE(dg.design_date) <= ',$last_date);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
		
		
	}
	
	function get_video_date($first_date,$last_date)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('video vd');
                $CI->db->join('user us','vd.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('DATE(vd.video_date) >= ',$first_date);
		$CI->db->where('DATE(vd.video_date) <= ',$last_date);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
        
        function get_challenge_date($first_date,$last_date)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('challenge ch');
                $CI->db->join('user us','ch.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('DATE(ch.challenge_date) >= ',$first_date);
		$CI->db->where('DATE(ch.challenge_date) <= ',$last_date);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
        
        function get_store_date($first_date,$last_date)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('store st');
                $CI->db->join('user us','st.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('DATE(st.store_date) >= ',$first_date);
		$CI->db->where('DATE(st.store_date) <= ',$last_date);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
	
	function get_comment_date($first_date,$last_date)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('image_comment cmt');
		$CI->db->join('user us','cmt.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('DATE(cmt.comment_date) >= ',$first_date);
		$CI->db->where('DATE(cmt.comment_date) <= ',$last_date);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
	
	function get_report_date($first_date,$last_date)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('block_report rpt');
		$CI->db->where('DATE(rpt.report_date) >= ',$first_date);
		$CI->db->where('DATE(rpt.report_date) <= ',$last_date);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
		
		
	}	
	
	
	/***********monthly************/
	
	function get_user_signup_month($month_pass,$year_pass)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('user us');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('MONTH(us.sign_up_date)',$month_pass);
		$CI->db->where('YEAR(us.sign_up_date)',$year_pass);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
		
		
	}
	
	
	
	
	function get_design_month($month_pass,$year_pass)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('design dg');
		$CI->db->join('user us','dg.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('MONTH(dg.design_date)',$month_pass);
		$CI->db->where('YEAR(dg.design_date)',$year_pass);

		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
		
		
	}
	
	function get_video_month($month_pass,$year_pass)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('video vd');
                $CI->db->join('user us','vd.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('MONTH(vd.video_date)',$month_pass);
		$CI->db->where('YEAR(vd.video_date)',$year_pass);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
        
        
        function get_challenge_month($month_pass,$year_pass)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('challenge ch');
                $CI->db->join('user us','ch.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('MONTH(ch.challenge_date)',$month_pass);
		$CI->db->where('YEAR(ch.challenge_date)',$year_pass);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
        
          function get_store_month($month_pass,$year_pass)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('store st');
                $CI->db->join('user us','st.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('MONTH(st.store_date)',$month_pass);
		$CI->db->where('YEAR(st.store_date)',$year_pass);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
	
	function get_comment_month($month_pass,$year_pass)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('image_comment cmt');
		$CI->db->join('user us','cmt.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('MONTH(cmt.comment_date)',$month_pass);
		$CI->db->where('YEAR(cmt.comment_date)',$year_pass);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
	
	function get_report_month($month_pass,$year_pass)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('block_report cmt');
		$CI->db->where('MONTH(cmt.report_date)',$month_pass);
		$CI->db->where('YEAR(cmt.report_date)',$year_pass);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
		
		
	}	
	
	
	
	/***********yearly************/
	
	function get_user_signup_year($year_pass)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('user us');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('YEAR(us.sign_up_date)',$year_pass);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
		
		
	}
	
	

	
	function get_design_year($year_pass)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('design dg');
		$CI->db->join('user us','dg.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('YEAR(dg.design_date)',$year_pass);

		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
		
		
	}
	
	function get_video_year($year_pass)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('video vd');
                $CI->db->join('user us','vd.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('YEAR(vd.video_date)',$year_pass);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
        
        function get_challenge_year($year_pass)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('challenge ch');
                $CI->db->join('user us','ch.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('YEAR(ch.challenge_date)',$year_pass);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
        
        
         function get_store_year($year_pass)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('store st');
                $CI->db->join('user us','st.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('YEAR(st.store_date)',$year_pass);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
        
	
	function get_comment_year($year_pass)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('igc_image_comment cmt');
		$CI->db->join('user us','cmt.user_id=us.user_id');
		$CI->db->join('user_profile up','us.user_id=up.user_id');
		$CI->db->where('YEAR(cmt.comment_date)',$year_pass);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
	
	function get_report_year($year_pass)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('igc_block_report cmt');
		$CI->db->where('YEAR(cmt.report_date)',$year_pass);
	
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
		
		
	}	
	
        
        
        
        
        /*** get total earning on wallets
	**
	**/
	
	function get_total_earning_on_wallet()
	{
		$CI =& get_instance();
		
		$CI->db->select('SUM(credit) as total_paid');
		$CI->db->from('wallet_admin');
		
				
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();
			
			if($res->total_paid>0)
			{
				return $res->total_paid;
			}
			
			return 0.00;
		}
		
		return 0.00;
		
	}
	
	
	
	
	
	//////====daily===
	
	function get_daily_earning_on_wallet($cur_date)
	{		
		
		$CI =& get_instance();
		
		$CI->db->select('SUM(credit) as total_paid');
		$CI->db->from('wallet_admin');
		$CI->db->where('DATE(wallet_date)',$cur_date);
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();
			
			if($res->total_paid>0)
			{
				return $res->total_paid;
			}
			
			return 0.00;
		}
		
		return 0.00;
		
		
	}
	
	

	
	
	//////====weekly===
	
	function get_weekly_earning_on_wallet($first_date,$last_date)
	{		
		
		$CI =& get_instance();
		
		
		$date=date('Y-m-d');
		
		$first_date= get_first_day_of_week($date);
		$last_date= get_last_day_of_week($date);
		
		
		$CI->db->select('SUM(credit) as total_paid');
		$CI->db->from('wallet_admin');
		$CI->db->where('DATE(wallet_date) >=',$first_date);
		$CI->db->where('DATE(wallet_date) <=',$last_date);
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();
			
			if($res->total_paid>0)
			{
				return $res->total_paid;
			}
			
			return 0.00;
		}
		
		return 0.00;
		
		
	}
	
	
	
	
	
	
	//////====monthly===
	
	function get_monthly_earning_on_wallet($cur_month,$cur_year)
	{		
		
		$CI =& get_instance();
		
		$CI->db->select('SUM(credit) as total_paid');
		$CI->db->from('wallet_admin');
		$CI->db->where('MONTH(wallet_date)',$cur_month);
		$CI->db->where('YEAR(wallet_date) ',$cur_year);
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();
			
			if($res->total_paid>0)
			{
				return $res->total_paid;
			}
			
			return 0.00;
		}
		
		return 0.00;
		
		
	}
	
	
	
	
	
	//////====yearly===
	
	function get_yearly_earning_on_wallet($cur_year)
	{		
		
		$CI =& get_instance();
		
		$CI->db->select('SUM(credit) as total_paid');
		$CI->db->from('wallet_admin');
		$CI->db->where('YEAR(wallet_date)',$cur_year);
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();
			
			if($res->total_paid>0)
			{
				return $res->total_paid;
			}
			
			return 0.00;
		}
		
		return 0.00;
		
		
	}
	
	
	
	
	
	
	/*** get total earning by on purchase package
	**
	**/
	
	function get_total_earning_on_package()
	{
		$CI =& get_instance();
		
		$CI->db->select('SUM(upkg.pay_amount) as total_paid');
		$CI->db->from('user_package upkg');
		$CI->db->join('user us','upkg.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
				
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();
			
			if($res->total_paid>0)
			{
				return $res->total_paid;
			}
			
			return 0.00;
		}
		
		return 0.00;
		
	}
	
	
	
	
	
	//////====daily===
	
	function get_daily_earning_on_package($cur_date)
	{		
		
		$CI =& get_instance();
		
		$CI->db->select('SUM(upkg.pay_amount) as total_paid');
		$CI->db->from('user_package upkg');
		$CI->db->join('user us','upkg.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('DATE(upkg.package_purchase_date)',$cur_date);
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();
			
			if($res->total_paid>0)
			{
				return $res->total_paid;
			}
			
			return 0.00;
		}
		
		return 0.00;
		
		
	}
	
	

	
	
	//////====weekly===
	
	function get_weekly_earning_on_package($first_date,$last_date)
	{		
		
		$CI =& get_instance();
		
		
		$date=date('Y-m-d');
		
		$first_date= get_first_day_of_week($date);
		$last_date= get_last_day_of_week($date);
		
		
		$CI->db->select('SUM(upkg.pay_amount) as total_paid');
		$CI->db->from('user_package upkg');
		$CI->db->join('user us','upkg.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('DATE(upkg.package_purchase_date) >=',$first_date);
		$CI->db->where('DATE(upkg.package_purchase_date) <=',$last_date);
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();
			
			if($res->total_paid>0)
			{
				return $res->total_paid;
			}
			
			return 0.00;
		}
		
		return 0.00;
		
		
	}
	
	
	
	
	
	
	//////====monthly===
	
	function get_monthly_earning_on_package($cur_month,$cur_year)
	{		
		
		$CI =& get_instance();
		
		$CI->db->select('SUM(upkg.pay_amount) as total_paid');
		$CI->db->from('user_package upkg');
		$CI->db->join('user us','upkg.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('MONTH(upkg.package_purchase_date)',$cur_month);
		$CI->db->where('YEAR(upkg.package_purchase_date) ',$cur_year);
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();
			
			if($res->total_paid>0)
			{
				return $res->total_paid;
			}
			
			return 0.00;
		}
		
		return 0.00;
		
		
	}
	
	
	
	
	
	//////====yearly===
	
	function get_yearly_earning_on_package($cur_year)
	{		
		
		$CI =& get_instance();
		
		$CI->db->select('SUM(upkg.pay_amount) as total_paid');
		$CI->db->from('user_package upkg');
		$CI->db->join('user us','upkg.user_id=us.user_id','left');
		$CI->db->join('user_profile up','us.user_id=up.user_id','left');
		$CI->db->where('YEAR(upkg.package_purchase_date)',$cur_year);
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();
			
			if($res->total_paid>0)
			{
				return $res->total_paid;
			}
			
			return 0.00;
		}
		
		return 0.00;
		
		
	}
	
	
	
	

	
	
	/**
	 * @param DateTime $date A given date
	 * @param int $firstDay 0-6, Sun-Sat respectively
	 * @return DateTime
	 */
	function get_first_day_of_week($date) 
	{
		 $day_of_week = date('N', strtotime($date)); 
		 $week_first_day = date('Y-m-d', strtotime($date . " - " . ($day_of_week - 1) . " days")); 
		 return $week_first_day;
	}

	
	function get_last_day_of_week($date)
	{
		 $day_of_week = date('N', strtotime($date)); 
		 $week_last_day = date('Y-m-d', strtotime($date . " + " . (7 - $day_of_week) . " days"));   
    	 return $week_last_day;
	}
	
	/************************************************report end****************************/
	
	/** send email
	 * @return	integer
	 */
	 
	function email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name='',$email_attachment='')
	{
				
		$CI =& get_instance();
		$query = $CI->db->get_where("email_setting",array('email_setting_id'=>1));
		$email_set=$query->row();
					
									
		$CI->load->library(array('email'));
			
		///////====smtp====
		
		if($email_set->mailer=='smtp')
		{
		
			$config['protocol']='smtp';  
			$config['smtp_host']=trim($email_set->smtp_host);  
			$config['smtp_port']=trim($email_set->smtp_port);  
			$config['smtp_timeout']='30';  
			$config['smtp_user']=trim($email_set->smtp_email);  
			$config['smtp_pass']=trim($email_set->smtp_password);  
					
		}
		
		/////=====sendmail======
		
		elseif(	$email_set->mailer=='sendmail')
		{	
		
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = trim($email_set->sendmail_path);
			
		}
		
		/////=====php mail default======
		
		else
		{
		
		}
			
			
		$config['wordwrap'] = TRUE;	
		$config['mailtype'] = 'html';
		$config['crlf'] = '\n\n';
		$config['newline'] = '\n\n';
		
		$CI->email->initialize($config);	
		
		$CI->email->clear();
		
		if($email_from_name!='')
		{
			$CI->email->from($email_address_from,$email_from_name);
		}
		else
		{
			$CI->email->from($email_address_from);
		}
		
		if($email_attachment!='')
		{
			$CI->email->attach($email_attachment);		
		}
		
		$CI->email->reply_to($email_address_reply);
		$CI->email->to($email_to);
		$CI->email->subject($email_subject);
		$CI->email->message($str);
		$CI->email->send();
		
		

	}
	
	
	
	
	/**
	 * generate random code
	 *
	 * @return	string
	 */
	
	function randomCode()
	{
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$pass = array(); 
		
		for ($i = 0; $i < 12; $i++) {
		$n = rand(0, strlen($alphabet)-1); //use strlen instead of count
		$pass[$i] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}
	
	function check_image_code($rand)
	{
		$CI =& get_instance();
		
		$query=$CI->db->get_where('images',array('image_unique_code'=>$rand));
		
		if($query->num_rows()>0)
		{
			return 1;
		}
		
		return 0;
	}
	
	function unique_image_code($rand)
	{		
		$chk=check_image_code($rand);
		
		if($chk==1)
		{
			$rand=randomCode();
			unique_image_code($rand);		
		}
				
		return $rand;  	
		
	}	
	
	/*** load site setting
	*  return single record array
	**/
	
	function site_setting()
	{		
		$CI =& get_instance();
		$query = $CI->db->get("site_setting");
		return $query->row();
	
	}
	
	/*** load user setting
	*  return single record array
	**/
	
	function user_setting()
	{		
		$CI =& get_instance();
		$query = $CI->db->get("user_setting");
		return $query->row();
	
	}
	
	/*** get all currency code details
	*  return all record array
	**/
	
	function get_currency()
	{		
		$CI =& get_instance();
		$query = $CI->db->get('currency_code');
		return $query->result();
	
	}
	
	/*** get all timezone details
	*  return all record array
	**/
	
	function get_timezone()
	{		
		$CI =& get_instance();
		$query = $CI->db->get('timezone');
		return $query->result();
	
	}
	
	/*** get all languages details
	*  return all record array
	**/
	
	function get_languages()
	{		
		$CI =& get_instance();
		$query = $CI->db->get('language');
		return $query->result();
	
	}
	
	/*** load assigns right setting
	*  return number 1 or 0
	**/
	function get_rights($rights_name)
	{
		$CI =& get_instance();
		$right_detail = $CI->db->get_where("rights",array('rights_name'=>trim($rights_name)));
		
		if($right_detail->num_rows()>0)
			{
			
				$right_result=$right_detail->row();
				$rights_id=$right_result->rights_id;

			$query=$CI->db->get_where("rights_assign",array('rights_id'=>$rights_id,'admin_id'=>$CI->session->userdata('admin_id')));
			
			if($query->num_rows()>0)
			{
				$result=$query->row();
				
				if($result->rights_set=='1' || $result->rights_set==1)
				{
					return 1;
				}
				else
				{
					return 0;
				}					
			}
			else
			{
				return 0;
			}	
		}
		else
		{
			return 0;		
		}
	
	}
        
        
        function credit_setting(){
            $CI =& get_instance();
		$query = $CI->db->get_where("credit_setting",array('credit_setting_id'=>1));
		if($query->num_rows>0){
			return $query->row();		
			
		}
        }
        
        function pointtoamountconvert($points){
            
            $credit_setting=credit_setting();
                        
            $rate=$credit_setting->point2paisa;
            
            $final_amount=$rate*$points;
            
            return $final_amount;
        }
	
	
	
	/*** get category name
	*  return string categoryname
	**/
	
	function get_category_name($cid)
	{		
		$CI =& get_instance();
		$query = $CI->db->get_where("category",array('category_id'=>$cid));
		if($query->num_rows>0){
			$name = $query->row();
			
				return $name->category_name;
			
		}
	}
	
	/*** get user name
	*  return string username
	**/
	
	function get_user_name($uid)
	{		
		$CI =& get_instance();
		$query = $CI->db->get_where("user",array('user_id'=>$uid));
		$user = $query->row();
		return anchor(front_base_url().'user/'.$user->profile_name,ucfirst($user->first_name).' '.ucfirst(substr($user->last_name,0,1)),' style="color:#004C7A;" target="_blank"');
	}
        
        function get_user_name_no_link($uid)
	{		
		$CI =& get_instance();
		$query = $CI->db->get_where("user",array('user_id'=>$uid));
		$user = $query->row();
		return ucfirst($user->first_name).' '.ucfirst($user->last_name);
	}
	
	
	/*count image point from image_comment*/
	function get_image_point_comment($image_id)
	{
		$CI =& get_instance();
		$query = $CI->db->get_where("image_comment",array('image_id '=>$image_id));
		return $query->num_rows();
	}
	
	/***** get all city list
	***
	**/
	
	function city_list()
	{
			
		$CI =& get_instance();
		
		$CI->db->order_by('city_name','asc');
		$query = $CI->db->get_where("city",array('active'=>1));
		
		if($query->num_rows()>0)
		{
			 return $query->result();
		}
		
		return 0;			
	}
	
	
	/***** get all city list
	***
	**/
	
	function state_list()
	{
			
		$CI =& get_instance();
		
		$CI->db->order_by('state_name','asc');
		$query = $CI->db->get_where("state",array('active'=>1));
		
		if($query->num_rows()>0)
		{
			 return $query->result();
		}
		
		return 0;			
	}
	
	/*** get city name
	*  return string cityname
	**/
	
	function get_city_name($cid)
	{		
		$CI =& get_instance();
		$query = $CI->db->get_where("city",array('city_id'=>$cid));
		$name = $query->row();
		return $name->city_name;
	}
	
	/*** load image setting
	*  return single record array
	**/
	function image_setting()
    {        
        $CI =& get_instance();
        $query = $CI->db->get("image_setting");
        return $query->row();    
    }
	
	
	
	/*** get force_download 
	*  return string cityname
	**/
	
	function force_download($filename = '', $data = false, $enable_partial = true, $speedlimit = 0)
    {
        if ($filename == '')
        {
            return FALSE;
        }
        
        if($data === false && !file_exists($filename))
            return FALSE;

        // Try to determine if the filename includes a file extension.
        // We need it in order to set the MIME type
        if (FALSE === strpos($filename, '.'))
        {
            return FALSE;
        }
    
        // Grab the file extension
        $x = explode('.', $filename);
        $extension = end($x);

        // Load the mime types
        @include(APPPATH.'config/mimes'.EXT);
    
        // Set a default mime if we can't find it
        if ( ! isset($mimes[$extension]))
        {
            if (ereg('Opera(/| )([0-9].[0-9]{1,2})', $_SERVER['HTTP_USER_AGENT']))
                $UserBrowser = "Opera";
            elseif (ereg('MSIE ([0-9].[0-9]{1,2})', $_SERVER['HTTP_USER_AGENT']))
                $UserBrowser = "IE";
            else
                $UserBrowser = '';
            
            $mime = ($UserBrowser == 'IE' || $UserBrowser == 'Opera') ? 'application/octetstream' : 'application/octet-stream';
        }
        else
        {
            $mime = (is_array($mimes[$extension])) ? $mimes[$extension][0] : $mimes[$extension];
        }
        
        $size = $data === false ? filesize($filename) : strlen($data);
        
        if($data === false)
        {
            $info = pathinfo($filename);
            $name = $info['basename'];
        }
        else
        {
            $name = $filename;
        }
        
        // Clean data in cache if exists
        @ob_end_clean();
        
        // Check for partial download
        if(isset($_SERVER['HTTP_RANGE']) && $enable_partial)
        {
            list($a, $range) = explode("=", $_SERVER['HTTP_RANGE']);
            list($fbyte, $lbyte) = explode("-", $range);
            
            if(!$lbyte)
                $lbyte = $size - 1;
            
            $new_length = $lbyte - $fbyte;
            
            header("HTTP/1.1 206 Partial Content", true);
            header("Content-Length: $new_length", true);
            header("Content-Range: bytes $fbyte-$lbyte/$size", true);
        }
        else
        {
            header("Content-Length: " . $size);
        }
        
        // Common headers
        header('Content-Type: ' . $mime, true);
        header('Content-Disposition: attachment; filename="' . $name . '"', true);
        header("Expires: 0", true);
        header('Accept-Ranges: bytes', true);
        header("Cache-control: private", true);
        header('Pragma: private', true);header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        
        // Open file
        if($data === false) {
            $file = fopen($filename, 'r');
            
            if(!$file)
                return FALSE;
        }
        
        // Cut data for partial download
        if(isset($_SERVER['HTTP_RANGE']) && $enable_partial)
            if($data === false)
                fseek($file, $range);
            else
                $data = substr($data, $range);
        
        // Disable script time limit
        @set_time_limit(0);
        
        // Check for speed limit or file optimize
        if($speedlimit > 0 || $data === false)
        {
            if($data === false)
            {
                $chunksize = $speedlimit > 0 ? $speedlimit * 1024 : 512 * 1024;
            
                while(!feof($file) and (connection_status() == 0))
                {
                    $buffer = fread($file, $chunksize);
                    echo $buffer;
                    flush();
                    
                    if($speedlimit > 0)
                        sleep(1);
                }
                
                fclose($file);
            }
            else
            {
                $index = 0;
                $speedlimit *= 1024; //convert to kb
                
                while($index < $size and (connection_status() == 0))
                {
                    $left = $size - $index;
                    $buffersize = min($left, $speedlimit);
                    
                    $buffer = substr($data, $index, $buffersize);
                    $index += $buffersize;
                    
                    echo $buffer;
                    flush();
                    sleep(1);
                }
            }
        }
        else
        {
            echo $data;
        }
		
		//$this->db->cache_delete_all();
		ob_clean();
        flush();
		
    }
	
	
	

	
	
	/****  create seo friendly url 
	* var string $text
	**/ 	  
  
  	function clean_url($text) 
	{ 
	
		$text=strtolower($text); 
		$code_entities_match = array( '&quot;' ,'!' ,'@' ,'#' ,'$' ,'%' ,'^' ,'&' ,'*' ,'(' ,')' ,'+' ,'{' ,'}' ,'|' ,':' ,'"' ,'<' ,'>' ,'?' ,'[' ,']' ,'' ,';' ,"'" ,',' ,'.' ,'_' ,'/' ,'*' ,'+' ,'~' ,'`' ,'=' ,' ' ,'---' ,'--','--','�'); 
		$code_entities_replace = array('' ,'-' ,'-' ,'' ,'' ,'' ,'-' ,'-' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'-' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'-' ,'' ,'-' ,'-' ,'' ,'' ,'' ,'' ,'' ,'-' ,'-' ,'-','-'); 
		$text = str_replace($code_entities_match, $code_entities_replace, $text); 
		return $text; 
	} 
	
	
	
	/********offset to timezone
	***  var $offset
	** return string timezone name
	***/
	
	function tzOffsetToName($offset, $isDst = null)
    {
        if ($isDst === null)
        {
            $isDst = date('I');
        }

        $offset *= 3600;
        $zone    = timezone_name_from_abbr('', $offset, $isDst);

        if ($zone === false)
        {
            foreach (timezone_abbreviations_list() as $abbr)
            {
                foreach ($abbr as $city)
                {
                    if ((bool)$city['dst'] === (bool)$isDst &&
                        strlen($city['timezone_id']) > 0    &&
                        $city['offset'] == $offset)
                    {
                        $zone = $city['timezone_id'];
                        break;
                    }
                }

                if ($zone !== false)
                {
                    break;
                }
            }
        }
    
        return $zone;
    }
	
	
	
	function get_parent_category()
	{	
		$CI =& get_instance();
		
		
	  $query = $CI->db->get_where("task_category",array('category_status'=>1,'category_parent_id'=>0));

		if($query->num_rows()>0)
		{
			 return $query->result();
		}

		return 0;

	}
	
	
	function sub_category($pid)
	{
		$CI =& get_instance();
		
		

	  $query = $CI->db->get_where("task_category",array('category_parent_id'=>$pid));

		if($query->num_rows()>0)
		{
			 return $query->result();
		}
		
		
		return 0;	
	}
	
	
	
	
	/**** get all category
	*** return array of category
	****/
	
	function get_all_category()
    {        
	   $CI =& get_instance();

	   $CI->db->order_by('category_name','asc');
	   $query = $CI->db->get_where("category",array('category_status'=>1));
	   
	   if($query->num_rows()>0)
	   {
			return $query->result();
	   }
	
	  return 0;
  
       
    }
	
	
	/**
 * Array to CSV
 *
 * download == "" -> return CSV string
 * download == "toto.csv" -> download file toto.csv
 */
	

    function array_to_csv($array, $download = "")
    {
        if ($download != "")
        {    
            header('Content-Type: application/csv');
            header('Content-Disposition: attachement; filename="' . $download . '"');
        }        

        ob_start();
        $f = fopen('php://output', 'w') or show_error("Can't open php://output");
        $n = 0;        
        foreach ($array as $line)
        {
            $n++;
            if (!fputcsv($f, $line))
            {
                show_error("Can't write line $n: $line");
            }
        }
        fclose($f) or show_error("Can't close php://output");
        $str = ob_get_contents();
        ob_end_clean();

        if ($download == "")
        {
            return $str;    
        }
        else
        {    
            echo $str;
        }        
    }


// ------------------------------------------------------------------------

/**
 * Query to CSV
 *
 * download == "" -> return CSV string
 * download == "toto.csv" -> download file toto.csv
 */

    function query_to_csv($query, $headers = TRUE, $download = "")
    {
        if ( ! is_object($query) OR ! method_exists($query, 'list_fields'))
        {
            show_error('invalid query');
        }
        
        $array = array();
        
        if ($headers)
        {
            $line = array();
            foreach ($query->list_fields() as $name)
            {
                $line[] = $name;
            }
            $array[] = $line;
        }
        
        foreach ($query->result_array() as $row)
        {
            $line = array();
            foreach ($row as $item)
            {
                $line[] = $item;
            }
            $array[] = $line;
        }

        echo array_to_csv($array, $download);
    }


	
	
		
	/**** get city latitude and longitude
		*
	***/
	function get_cityDetail($city_id)
	{
		$CI =& get_instance();
	
		$query = $CI->db->get_where("city",array('city_id'=>$city_id));
		
		if($query->num_rows()>0)
		{
			return $query->row();
		}
		
		return 0;	
	}
	
	/*** load user notification setting
	*  return single record array
	**/
	
	function notification_setting($user_id)
	{		
		$CI =& get_instance();
		$query = $CI->db->get_where("user_notification", array('user_id'=>$user_id));
		return $query->row();
	
	}

	
	
	
	/**** get domain name from url
	**/
	
	function get_domain_name($url)
	{
		$matches=parse_url($url);
		
		if(isset($matches['host'])) {
			 $domain= $matches['host'];
			 
			 $domain=str_replace(array('www.'),'',$domain);
			 
			 return $domain;
		}
		
		return $url;
	} 
	
	
	/*** load paging setting
	*  return single record array
	**/
	
	function paging_setting()
	{		
		$CI =& get_instance();
		$query = $CI->db->get("paging_setting");
		return $query->row();	
	}
	
	/*********detect amount and currency from content ***/
	
	
	
	function findAmountAndCurrency($s, &$amount, &$currency)
	{

			$re_curr="/[�\$��]+/";
			$cs='';
			$currency=get_currency();
			if($currency)
			{
				foreach($currency as $cur)
				{
					$cs.=mb_convert_encoding(trim($cur->currency_symbol), 'UTF-8', 'HTML-ENTITIES');
				}
			}
 	
		 $re_curr="/[$cs]+/"; 	 
		 $re_amount="/[0-9\.]+/";
		
		$amount = '';
		 preg_match($re_amount, $s, $matches);
		 
		 if(isset($matches[0]))
		 {
		 	$amount = floatval($matches[0]);	
		}
		
		$currency = '';
		 preg_match($re_curr, $s, $matches);
		 if(isset($matches[0]))
		 {
		 	$currency = $matches[0];
		}
   }
   
   
   
	
  
 	/*********amount from content
	** return amount,currency, currency_code array
	***/
	
	
    function get_amount_from_content($s)
	{
		$currencySymbols = array();
		$currencyCodes = array();
		//$currencySymbols = array('�'=>'GBP', '$'=>'USD','�'=>'EUR','kc'=>'CZK','kr'=>'NOK','Ft'=>'HUF','?'=>'ILS','�'=>'JPY','zt'=>'PLN','CHF'=>'CHF');

		$currency=get_currency();
		if($currency)
		{
			foreach($currency as $cur)
			{
				$currencySymbols[mb_convert_encoding(trim($cur->currency_symbol), 'UTF-8', 'HTML-ENTITIES')]=trim($cur->currency_code);
				$currencyCodes[trim($cur->currency_code)]=mb_convert_encoding(trim($cur->currency_symbol), 'UTF-8', 'HTML-ENTITIES');
			}
		}
	
	
	
	
  		findAmountAndCurrency($s, $amount, $currency);

		$identifier=''; 
		
		if(in_array(trim($currency),$currencyCodes)) {
	
			$code_index=array_keys($currencyCodes,$currency);
			if(isset($code_index[0])) {
				$identifier=$code_index[0];
			} elseif(array_key_exists(trim($currency),$currencySymbols)) {
				$identifier=$currencySymbols[$currency];
			}
		} 
		
		elseif(array_key_exists(trim($currency),$currencySymbols)) {
			$identifier=$currencySymbols[$currency];
		} 
		elseif(array_key_exists(trim($currency),$currencyCodes)) {
				$identifier=$currencyCodes[$currency];
		}
	/*	echo("Amount: " . $amount . "<br/>");
		echo("Currency: " . $currency . "<br/>");
		echo("Identified Currency: " .  $identifier. "<br/>");*/
		
		$response=array(
		'amount'=>$amount,
		'currency'=>$currency,
		'currency_code'=>$identifier		
		);
		
		return $response;
  }
  
  
	function seturl($url)
	{
		
		if($url!='')
		{
		
			if(substr_count($url,'http://')>=1)
			{
				$url=$url;
			}
			elseif(substr_count($url,'https://')>=1)
			{
				$url=$url;
			}
			else
			{
				$url=str_replace(array(':','/','//','::'),'',$url);
				$url='http://'.$url;
			}
			
			return $url;
		}
		else
		{
			return false;
		}		
		
	}
	
/* End of file custom_helper.php */
/* Location: ./system/application/helpers/custom_helper.php */

?>