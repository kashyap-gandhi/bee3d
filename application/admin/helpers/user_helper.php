<?php
	/*
	Function name :get_user_profile_by_id()
	Parameter : $user_id(user id) 
	Return : array of user profile details
	Use : get user profile information
	*/
	
	function get_user_profile_by_id($user_id)
	{
		$CI =& get_instance();
		
		$query=$CI->db->query("select * from ".$CI->db->dbprefix('user')." usr, ".$CI->db->dbprefix('user_profile')." pr where usr.user_id=pr.user_id and usr.user_id='".$user_id."' ");	
		
		if($query->num_rows()>0)
		{		
			return $query->row();
		}
		
		return 0;
	}
	
	
	function get_agent_document($user_id)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('agent_document');
		$CI->db->where('user_id',$user_id);	
		
		$query = $CI->db->get();
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
	}
	
	
	
	function member_association()
	{
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('member_association ma');
		$CI->db->where('ma.association_status',1);
		$query=$CI->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}

		return 0;
	}
	
	
	function get_member_association_by_id($member_association_id)
	{
		$CI =& get_instance();
		
		$query=$CI->db->get_where('member_association',array('member_association_id'=>$member_association_id));
		
		if($query->num_rows()>0)
		{
			return $query->row();
		}
		
		return 0;
	}
	
	
	/////===your statics====
	
	function get_total_add_agent()
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('entry_report_all');
		$CI->db->where('admin_id',$CI->session->userdata('admin_id'));
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();
			return $res->total_add;
		}
		
		return 0;
	}
	
	function get_total_approved_agent()
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('entry_report_all');
		$CI->db->where('admin_id',$CI->session->userdata('admin_id'));
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();
			return $res->total_approve;
		}
		
		return 0;
	}
	
	
	function get_total_rejected_agent()
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('entry_report_all');
		$CI->db->where('admin_id',$CI->session->userdata('admin_id'));
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();
			return $res->total_reject;
		}
		
		return 0;
	}
	
	
	/////===your statics====
	
	function get_current_total_add_agent()
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('entry_report_current');
		$CI->db->where('admin_id',$CI->session->userdata('admin_id'));
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();
			return $res->total_add;
		}
		
		return 0;
	}
	
	function get_current_total_approved_agent()
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('entry_report_current');
		$CI->db->where('admin_id',$CI->session->userdata('admin_id'));
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();
			return $res->total_approve;
		}
		
		return 0;
	}
	
	
	function get_current_total_rejected_agent()
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('entry_report_current');
		$CI->db->where('admin_id',$CI->session->userdata('admin_id'));
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();
			return $res->total_reject;
		}
		
		return 0;
	}
	
	
?>