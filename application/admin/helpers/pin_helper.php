<?php
	/*
	Function name :get_user_profile_by_id()
	Parameter : $user_id(user id) 
	Return : array of user profile details
	Use : get user profile information
	*/
	
	function get_user_profile_by_id($user_id)
	{
		$CI =& get_instance();
		
		$query=$CI->db->query("select * from ".$CI->db->dbprefix('user')." usr, ".$CI->db->dbprefix('user_profile')." pr where usr.user_id=pr.user_id and usr.user_id='".$user_id."' ");	
		
		if($query->num_rows()>0)
		{		
			return $query->row();
		}
		
		return 0;
	}
	
	
	
	/*
	Function name :check_user_follow()
	Parameter : $follower_user_id(user id) ,  $follow_user_id(user id) , 
	Return : 0 or 1
	Use : check user follow the another use or not
	*/
	
	function check_user_follow($follower_user_id,$follow_user_id)
	{
		$CI =& get_instance();
		
		$query=$CI->db->get_where("user_follow",array('follower_user_id'=>$follower_user_id,'follow_user_id'=>$follow_user_id));	
		
		if($query->num_rows()>0)
		{		
			return 1;
		}
		
		return 0;
	}
	
	
	/*
	Function name :check_user_follow_board()
	Parameter : $user_id(user id) ,  $board_id(board id) , 
	Return : 0 or 1
	Use : check user follow the board or not
	*/
	
	function check_user_follow_board($user_id,$board_id)
	{
		$CI =& get_instance();
		
		$query=$CI->db->get_where("board_follow",array('user_id'=>$user_id,'board_id'=>$board_id));	
		
		if($query->num_rows()>0)
		{		
			return 1;
		}
		
		return 0;
	}
	
	
	/*
	Function name :check_user_follow_all_board()
	Parameter : $user_id(user id) ,  $follow_user_id(user id) , 
	Return : 0 or 1
	Use : check user follow all boards or not
	*/
	
	function check_user_follow_all_board($user_id,$follow_user_id)
	{
		$CI =& get_instance();
		
		////=====get follower user all board
		$user_board=array();
		
		$get_user_all_board=$CI->db->get_where("board",array('board_user_id'=>$follow_user_id));	
		
		if($get_user_all_board->num_rows()>0)
		{			
			$res_board=$get_user_all_board->result();
			
			foreach($res_board as $brd) { 
				$user_board[]=$brd->board_id;
			}
		}
		
		/////======get user follow board for this follower user
		$follow_board=array();
		$get_user_follow_board=$CI->db->get_where("board_follow",array('user_id'=>$user_id,'board_user_id'=>$follow_user_id));	
		
		if($get_user_follow_board->num_rows()>0)
		{		
			$res_follow_board=$get_user_follow_board->result();
			
			foreach($res_follow_board as $fbrd) {				
				$follow_board[]=$fbrd->board_id;
			}
			
		}
		
		
		/////====now do array difference
		$result_arr = array_diff($user_board, $follow_board);
		
		if($result_arr) {
		
			return 0;
		}
		
		return 1;
	}
	
	
	/*
	Function name :get_user_total_follow()
	Parameter : $follower_user_id(user id)
	Return : integer total follow
	Use : ger user total follow user
	*/
	
	function get_user_total_follow($follower_user_id)
	{
		$CI =& get_instance();
		
		//$query=$CI->db->get_where("user_follow",array('follower_user_id'=>$follower_user_id));	
		
		$CI->db->select('*');
        $CI->db->from('user_follow uf');
        $CI->db->join('user us','uf.follow_user_id=us.user_id');
        $CI->db->join('user_profile up','us.user_id=up.user_id');
        $CI->db->where('uf.follower_user_id',$follower_user_id);
        $CI->db->where('uf.follow_user_id != ',$follower_user_id);
        $CI->db->group_by('us.user_id');
        
        $query=$CI->db->get();   
		
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
	
		/*
	Function name :get_user_total_following()
	Parameter : $follow_user_id(user id)
	Return : integer total following
	Use : ger user total following user
	*/
	
	function get_user_total_following($follow_user_id)
    {
        $CI =& get_instance();
        
        //$query=$CI->db->get_where("user_follow",array('follow_user_id'=>$follow_user_id));    
        
        $CI->db->select('*');
        $CI->db->from('user_follow uf');
        $CI->db->join('user us','uf.follower_user_id=us.user_id');
        $CI->db->join('user_profile up','us.user_id=up.user_id');
        $CI->db->where('uf.follow_user_id',$follow_user_id);
        $CI->db->where('uf.follower_user_id != ',$follow_user_id);
        $CI->db->group_by('us.user_id');
        
        $query=$CI->db->get();        
        
        if($query->num_rows()>0)
        {        
            return $query->num_rows();
        }
        
        return 0;
    }

	
	
	/*
	Function name :get_user_total_board()
	Parameter : $user_id(user id)
	Return : integer total board
	Use : ger user total boards
	*/
	
	function get_user_total_board($user_id)
	{
		$CI =& get_instance();
		
		$query=$CI->db->get_where("board",array('board_user_id'=>$user_id));	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
	
	
	/*
	Function name :get_user_total_pin()
	Parameter : $user_id(user id)
	Return : integer total pin
	Use : ger user total pins
	*/
	
	function get_user_total_pin($user_id)
	{
		$CI =& get_instance();
		
		$query=$CI->db->get_where("pin",array('user_id'=>$user_id));	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
	

	/*
	Function name :get_user_total_pin_like()
	Parameter : $user_id(user id)
	Return : integer total pin like
	Use : ger user total like pins
	*/
	
	function get_user_total_pin_like($user_id)
	{
		$CI =& get_instance();
		
		$query=$CI->db->get_where("pin_like",array('user_id'=>$user_id));	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
	
	
	/*
	Function name :get_box_suggestion_boards()
	Parameter : none 
	Return : array of display in box suggestion board 
	Use : get suggestion board display in box
	*/
	
	
	function get_box_suggestion_boards()
	{
		$CI =& get_instance();
		
		$CI->db->order_by('display_order','asc');
		$query=$CI->db->get_where("board_suggestion",array('board_status'=>1,'display_in_box'=>1));	
		
		if($query->num_rows()>0)
		{		
			return $query->result();
		}
		
		return 0;
	
	}
	
	/*
	Function name :get_suggestion_boards()
	Parameter : none 
	Return : array of suggestion board 
	Use : get suggestion board
	*/
	
	
	function get_suggestion_boards()
	{
		$CI =& get_instance();
		
		$query=$CI->db->get_where("board_suggestion",array('board_status'=>1,'display_in_box'=>0));	
		
		if($query->num_rows()>0)
		{		
			return $query->result();
		}
		
		return 0;
	
	}
	
	/*
	Function name :get_album_by_id()
	Parameter : $album_id(album id) 
	Return : array of user album detail
	Use : get user album detail
	*/
	
	function get_album_by_id($album_id)
	{
		
		$CI =& get_instance();
		
		
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('album alb');
		$CI->db->join('user us','alb.user_id=us.user_id');
		$CI->db->join('user_profile up','us.user_id=up.user_id');		
		$CI->db->where('alb.album_id',$album_id);
		
		$query=$CI->db->get();
		if($query->num_rows()>0) {
			return $query->row();
		}
		return 0;
		
	
	}
	
	
	
	/*
	Function name :get_album_total_image()
	Parameter : $album_id(album_id id)
	Return : integer total album pin
	Use : ger album total images
	*/
	
	function get_album_total_image($album_id)
	{
		
		$CI =& get_instance();
		
		$query=$CI->db->get_where("album_image",array('album_id'=>$album_id));	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	
	}
	
	
	/*
	Function name :get_board_by_url_name()
	Parameter : $board_url_name(board url name) 
	Return : array of user board detail
	Use : get user board detail
	*/
	
	function get_board_by_url_name($board_url_name)
	{
		
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('board brd');
		$CI->db->join('user us','brd.board_user_id=us.user_id');
		$CI->db->join('user_profile up','us.user_id=up.user_id');		
		$CI->db->where('brd.board_url_name',$board_url_name);
		
		$query=$CI->db->get();
		if($query->num_rows()>0) {
			return $query->row();
		}
		return 0;
		
	}
	
	
	/*
	Function name :get_user_pin()
	Parameter : $user_id(user id), $board_id(board id) 
	Return : array of user pins
	Use : get user pins detail
	*/
	
	function get_user_pin($user_id,$board_id='none')
	{		
		$CI =& get_instance();
			
		
		if($board_id!='none') {	
			
								
		if(substr_count($board_id,',')) { 
		
			$sel_brd='';
			
			$exbrd=explode(',',$board_id);
			
			foreach($exbrd as $brd) {
				$sel_brd.="'".$brd."',";
			}
			
			$sel_brd=substr($sel_brd,0,-1);
					
	$query = $CI->db->query("select * from ".$CI->db->dbprefix('pin')." where user_id='".$user_id."'  and board_id in(".$sel_brd.")");
		} else {
	   $query = $CI->db->query("select * from ".$CI->db->dbprefix('pin')." where user_id='".$user_id."'  and board_id ='".$board_id."'");
		
			}
		
		} else {
		 
		  $query = $CI->db->query("select * from ".$CI->db->dbprefix('pin')." where user_id='".$user_id."'");
		 
		 }
		 
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
	
	}
	
	
	
	
	
	/*
	Function name :get_album_image()
	Parameter : $album_id(album_id) 
	Return : array of album images
	Use : get album all images detail
	*/
	
	function get_album_image($album_id)
	{		
		$CI =& get_instance();
			
	
	   $query = $CI->db->query("select * from ".$CI->db->dbprefix('album_image')." where  album_id ='".$album_id."' order by album_id desc");
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
	
	}
	
	
	/*
	Function name :check_board_has_guset()
	Parameter : $board_id(board id)
	Return : 1 or 0
	Use :check board have guest invited or not
	*/
	
	function check_board_has_guset($board_id)
	{
		$CI =& get_instance();
		$query=$CI->db->get_where('board_user',array('board_id'=>$board_id,'accept_request'=>1));
		
		if($query->num_rows()>0) {
			return 1;
		}
		 
		 return 0;
		
	}
	
	
	/*
	Function name :board_guset_user()
	Parameter : $board_id(board id)
	Return : array of board guest user
	Use :get board guest user details
	*/
	
	function board_guset_user($board_id)
	{
		$CI =& get_instance();
		
		 
		 
		$CI->db->select('*');
		$CI->db->from('board_user');
		$CI->db->join('user','board_user.allow_user_id = user.user_id');
		//$CI->db->join('user','pin.user_id = user.user_id');
		$CI->db->join('user_profile','user.user_id = user_profile.user_id');
		$CI->db->where('board_user.board_id',$board_id);
		$CI->db->where('board_user.accept_request =',1);
		
		$query=$CI->db->get();
		if($query->num_rows()>0) {
			return $query->result();
		}
		 
		 return 0;
		
	}
	
	function get_image()
	{
		
		$CI =& get_instance();
		$query = $CI->db->query("select * from ".$CI->db->dbprefix('images'));
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
	
	}
	
	
	/*
	Function name :get_image()
	Parameter : $pin_id(pin id)
	Return : array of pin images
	Use : get pin images
	*/
	
	function get_cover_image($image_id)
	{
		
		$CI =& get_instance();
		$query = $CI->db->query("select * from ".$CI->db->dbprefix('images')." where image_id='".$image_id."'");
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return 0;
	
	}
	
	
	/*
	Function name :get_image_details()
	Parameter : $image_id
	Return : array of image details
	Use : image details 
	*/
	
	function get_image_details($image_id) 
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('images img');
		$CI->db->where('img.image_id',$image_id);
		
		
		$query=$CI->db->get();
		if($query->num_rows()>0) {
			return $query->row();
		}
		return 0;
	}
	
	
	/*
	Function name :check_user_allow_board_change()
	Parameter : $user_id,$board_id
	Return : integer 1 or 0
	Use : check login user have allow to edit board
	*/
	
	function check_user_allow_board_change($user_id,$board_id)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('board_user');
		$CI->db->where('allow_user_id',$user_id);
		$CI->db->where('board_id',$board_id);
		
		$query=$CI->db->get();
		if($query->num_rows()>0) {
			return 1;
		}
		return 0;
		
	}
	
	
	/*
	Function name :get_album_cover_image()
	Parameter : $album_id
	Return : string image name
	Use : get Album cover image
	*/
	
	function get_album_cover_image($album_id)
	{
		
		$CI =& get_instance();
		
		$image_name='none';
		
		$CI->db->select('*');
		$CI->db->from('album');
		$CI->db->where('album_id',$album_id);
		
		
		$query=$CI->db->get();
		if($query->num_rows()>0) {
			
			$album_detail=$query->row();
			
			if($album_detail->album_cover_image_id > 0) {			
					if($album_detail->album_cover_image_id !='') {	
					
						$get_album_image_from_images=$CI->db->get_where('images',array('image_id'=>$album_detail->album_cover_image_id));
						
						if($get_album_image_from_images->num_rows()>0) {	
						
						$get_album_image_from_images=$get_album_image_from_images->row();
						 $orig_name=$get_album_image_from_images->image_name; 
						
						 
								if($orig_name != '')
								{
									 $exp_name=explode('.',$orig_name);							
									 $medium_name=$exp_name[0].'_medium.'.trim($exp_name[1]);	
							  		 $small_name=$exp_name[0].'_small.'.trim($exp_name[1]);			 
									 
									 
									 	if(file_exists(base_path().'upload/image_orig/'.$orig_name)) { 
											return upload_url().'upload/image_orig/'.$orig_name;
											}
										
										else if(file_exists(base_path().'upload/image_medium/'.$medium_name)) { 
											return upload_url().'upload/image_medium/'.$medium_name;
										}
										else 
										{
											if(file_exists(base_path().'upload/image_thumb/'.$small_name)) { 
											return upload_url().'upload/image_thumb/'.$small_name;
										}
										}
								}
						}
					}					
						
			}	
		}
		
		if($image_name=='none') {
			
				$image_details=get_album_image($album_id);	
						
				if($image_details) { 			
			        foreach($image_details as $img) {
					
						$images=get_cover_image($img->image_id);
											
						if($images) {							
							foreach($images as $pinimg) {							
								if($pinimg->image_name!='') {					
														
									if(file_exists(base_path().'upload/image_thumb/'.$pinimg->image_name)) { 
										return upload_url().'upload/image_thumb/'.$pinimg->image_name;
									}
									else 
									{
										return upload_url().'upload/image_orig/'.$pinimg->image_name; break;    
									}
									
								}									
							}							
						}										
					}
				} 
			////
			
		}
		
	
		return $image_name;
	}
	
	/*
	Function name :get_user_board_list()
	Parameter : $user_id
	Return : array of user board list
	Use : get user board list
	*/
	function get_user_board_list($user_id)
	{
		
		$CI =& get_instance();
	
		
		$query=$CI->db->query("SELECT CONCAT_WS(' ',brd.board_name , org.board_name) as final_board_name , brd.*  FROM ".$CI->db->dbprefix('board brd')." left outer join ".$CI->db->dbprefix('board org')." on  brd.original_board_id=org.board_id  WHERE brd.board_user_id = ".$user_id." ORDER BY brd.board_order asc ");
		
		
		//SELECT CONCAT_WS(' ',brd.board_name , org.board_name) as final_board_name , brd.*,org.*  FROM (`pnc_board` brd) left outer join pnc_board org on  brd.original_board_id=org.board_id  WHERE `brd`.`board_user_id` = '1' ORDER BY `brd`.`board_name` asc 
		
				
		if($query->num_rows()>0) {
			return $query->result();
		}
		
		return 0;
	}
	
	/*
	Function name :get_user_today_board()
	Parameter : $user_id
	Return : integer of user total today board
	Use : get user total board add same day
	*/
	
	function get_user_today_board($user_id)
	{
		$CI =& get_instance();
		
		$query=$CI->db->query("select * from ".$CI->db->dbprefix('board')." where board_user_id='".$user_id."' and DATE(board_date)='".date('Y-m-d')."'");
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;
		
		
	}
	
	/*
	Function name :get_user_today_pin()
	Parameter : $user_id
	Return : integer of user total today pin
	Use : get user total pin add same day
	*/
	
	function get_user_today_pin($user_id)
	{
		$CI =& get_instance();
		
		$query=$CI->db->query("select * from ".$CI->db->dbprefix('pin')." where user_id='".$user_id."' and DATE(pin_date)='".date('Y-m-d')."'");
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		
		return 0;
		
		
	}
	
	function get_user_total_pins($user_id)
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('pin pn');
		$CI->db->join('user us','pn.user_id=us.user_id');
		$CI->db->join('user_profile up','us.user_id=up.user_id');
		$CI->db->where('pn.user_id',$user_id);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			return $query->num_rows();
		}
		
		return 0;
	}
	
	
	
	function get_report_type_by_id($report_id)
	{
		$CI =& get_instance();
		$query = $CI->db->get_where('report_type',array('report_id'=>$report_id));
		
		if($query->num_rows()>0) {
			return $query->row();
		}
		return 0;
	}
	
	
	/*
	Function name :get_comment_by_id()
	Parameter : $comment_id(comment id) 
	Return : array of user comment detail
	Use : get user comment detail
	*/
	
	function get_comment_by_id($comment_id)
	{
		
		
		
		
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('pin_comment cmt');
		$CI->db->join('user us','cmt.comment_user_id=us.user_id');
		$CI->db->join('user_profile up','us.user_id=up.user_id');		
		$CI->db->where('cmt.pin_comment_id',$comment_id);
		
		$query=$CI->db->get();
		if($query->num_rows()>0) {
			return $query->row();
		}
		return 0;
		
	
	}
	
	
	
	
	function get_user_total_usage($user_id)
	{
		$temp=array();
		
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('pin_image pnimg');
		$CI->db->join('user us','pnimg.pin_user_id=us.user_id');
		$CI->db->join('user_profile up','us.user_id=up.user_id');
		$CI->db->where('pnimg.pin_user_id',$user_id);
		
		$query=$CI->db->get();	
		
		if($query->num_rows()>0)
		{		
			$res= $query->result();
			
			foreach($res as $pnimg)
			{
				
				
				if($pnimg->pin_image_name!='')
				{
					$orig_name=$pnimg->pin_image_name;
					
					$exp_name=explode('.',$orig_name);
			
			
				 $medium_name=$exp_name[0].'_medium.'.trim($exp_name[1]);	
				 $small_name=$exp_name[0].'_small.'.trim($exp_name[1]);
				
					if(function_exists('filesize'))
					{
						if(file_exists(base_path().'upload/pin/'.$orig_name)) {						
							$file_size=filesize(base_path().'upload/pin/'.$orig_name);	
							
							if($file_size>0)
							{	
								$temp[] = ($file_size * .0009765625) * .0009765625; 	
							}
						}
						
						if(file_exists(base_path().'upload/pin_medium/'.$medium_name)) {
							$file_size=filesize(base_path().'upload/pin_medium/'.$medium_name);		
							
							if($file_size>0)
							{	
								$temp[] = ($file_size * .0009765625) * .0009765625; 	
							}						
						}
						
						if(file_exists(base_path().'upload/pin_thumb/'.$small_name)) {
							$file_size=filesize(base_path().'upload/pin_thumb/'.$small_name);		
							
							if($file_size>0)
							{	
								$temp[] = ($file_size * .0009765625) * .0009765625; 	
							}						
						}
					}
				
				
				}	///===if image	
			
			}		
			
			
			/*echo "<pre>";
			print_r($temp); die;*/
			if($temp)
			{
				return number_format(array_sum($temp),2);
			}
			
			return 0;
		}
		
		return 0;
	}
	
	
	
	function get_file_size($file_name)
	{
		if(function_exists('filesize')){
		
			if(file_exists($file_name)) {		
							
				$file_size=filesize($file_name);	
				
				if($file_size>0)
				{	
					$filesize = ($file_size * .0009765625) * .0009765625; 
					return number_format($filesize,2);
				}
			}
		}
		
		return 0;
	}
	
	
	/*
	Function name :get_pin_comments()
	Parameter : $pin_id
	Return : array of pin comments
	Use : pin comments 
	*/
	
	function get_pin_comments($pin_id) 
	{
		$CI =& get_instance();
		
		$CI->db->select('*');
		$CI->db->from('pin_comment pnc');
		$CI->db->join('user us','pnc.comment_user_id=us.user_id');
		$CI->db->join('user_profile up','us.user_id=up.user_id');		
		$CI->db->where('pnc.pin_id',$pin_id);
		$CI->db->where('pnc.comment_status',1);
		$CI->db->order_by('pnc.pin_comment_id','asc');
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0) {
			return $query->result();
		}
		return 0;
	}
	

	
	
?>