<?php
class Image extends  CI_Controller {

	function Image()
	{
		 parent::__construct();	
		$this->load->model('image_model');
		$this->load->model('user_image_model');
	 	$this->load->model('user_album_model');		
		
	}
	
	
	function index()
	{
		redirect('image/list_all_image');	
	}
	
	
	
	//////////////////////////========================request email image upload
	
	function image_email_upload($limit=15,$offset=0,$msg='')
	{
		
		//$check_rights=get_rights('list_all_pin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='4';
		$config['base_url'] = site_url('image/image_email_upload/'.$limit.'/');
		$config['total_rows'] = $this->image_model->get_total_activate_email_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['image_details'] = $this->image_model->get_all_activate_email_result($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/image/list_activate_email',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

		
	}
	
	function search_email_upload($limit=15,$option='',$keyword='',$offset=0,$msg='')
	{
		//$check_rights=get_rights('list_all_pin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
		
	
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		if($_POST)
		{		
			$option=$this->input->post('option');
			$keyword=$this->input->post('keyword');
		}
		else
		{
			$option=$option;
			$keyword=$keyword;			
		}
		
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='6';
		$config['base_url'] = site_url('image/search_email_upload/'.$limit.'/'.$option.'/'.$keyword.'/');
		$config['total_rows'] = $this->image_model->get_total_search_activate_email_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['image_details'] = $this->image_model->get_all_search_activate_email_result($option,$keyword,$offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']=$option;
		$data['keyword']=$keyword;
		$data['search_type']='search';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/image/list_activate_email',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}
	
	
	
	function email_image_add($user_id=0)
	{
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['site_setting'] = site_setting();
		
		
		if($_POST)
		{
			$this->image_model->add_email_image();
			redirect('image/image_email_upload/15/0/insert');
		}
		
		$data['user_id']=$user_id;
			
		$this->load->view($theme .'/layout/image/add_email_image',$data);

				
	}
	
	//////////////////////////========================request email image upload
	
	
	function list_all_image($limit=15,$offset=0,$msg='')
	{
		//$check_rights=get_rights('list_all_pin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='4';
		$config['base_url'] = site_url('image/list_all_image/'.$limit.'/');
		$config['total_rows'] = $this->image_model->get_total_image_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['image_details'] = $this->image_model->get_all_image_result($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/image/list_all_image',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}
	
	
	function search_all_image($limit=15,$option='',$keyword='',$offset=0,$msg='')
	{
		//$check_rights=get_rights('list_all_pin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
		
	
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		if($_POST)
		{		
			$option=$this->input->post('option');
			$keyword=$this->input->post('keyword');
		}
		else
		{
			$option=$option;
			$keyword=$keyword;			
		}
		
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='6';
		$config['base_url'] = site_url('image/search_all_image/'.$limit.'/'.$option.'/'.$keyword.'/');
		$config['total_rows'] = $this->image_model->get_total_search_image_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['image_details'] = $this->image_model->get_all_search_image_result($option,$keyword,$offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']=$option;
		$data['keyword']=$keyword;
		$data['search_type']='search';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/image/list_all_image',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}
	
	
	
	function action_all_image()
	{
		
		 $offset=$this->input->post('offset');
		  $limit=$this->input->post('limit');
		 $action=$this->input->post('action');
		 $image_id=$this->input->post('chk');
		$search_type=$this->input->post('search_type');
		
		$option=$this->input->post('option');
		$keyword=$this->input->post('keyword');
		
		$red_link='list_all_image/'.$limit;
		if($search_type=='search')
		{
			$red_link='search_all_image/'.$limit.'/'.$option.'/'.$keyword;
		}
		
		
		if($action=='delete')
		{	
			foreach($image_id as $id)
			{
			
				$this->image_model->delete_image($id);	
			
			}
			
			redirect('image/'.$red_link.'/'.$offset.'/delete');
		}
		
		if($action=='active')
		{	
			foreach($image_id as $id)
			{
			
				$this->db->where('image_id',$id);
				$this->db->update('images',array('image_active'=>1));
			
			}
			
			redirect('image/'.$red_link.'/'.$offset.'/active');
		}
		
		if($action=='inactive')
		{	
			foreach($image_id as $id)
			{
			
				$this->db->where('image_id',$id);
				$this->db->update('images',array('image_active'=>0));
			
			}
			
			redirect('image/'.$red_link.'/'.$offset.'/inactive');
		}
		
		if($action=='show_home')
		{	
			foreach($image_id as $id)
			{
			
				$this->db->where('image_id',$id);
				$this->db->update('images',array('image_show_on_home'=>1));
			
			}
			
			redirect('image/'.$red_link.'/'.$offset.'/update');
		}
		
		if($action=='dont_show_home')
		{	
			foreach($image_id as $id)
			{
			
				$this->db->where('image_id',$id);
				$this->db->update('images',array('image_show_on_home'=>0));
			
			}
			
			redirect('image/'.$red_link.'/'.$offset.'/update');
		}
		
		
		
		
	}
	
	
	
	
	function image_detail($image_id,$msg ='')
	{
		//$check_rights=get_rights('list_admin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
		
		$data = array();
	    $theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['msg'] = $msg; 
		$data['error'] = '';
		
		$image_details = $this->user_image_model->get_image_by_id($image_id);
		
		$data["image_details"] = $image_details;
		$data["image_description"] = $image_details->image_description;
		$data["image_original_name"] = $image_details->image_original_name;
		$data["image_title"] = $image_details->image_title;
		$data["image_user_id"] = $image_details->image_user_id;
		$data["image_view_count"]= $image_details->image_view_count;
		$data['image_delete_code'] = $image_details->image_delete_code;
		$data['image_unique_code'] = $image_details->image_unique_code;
		
		$data["image_name"] = $image_details->image_name;
		
		$data["show_on_home_page"] = $image_details->image_show_on_home;
		$data["image_id"] = $image_id;
		$data['site_setting'] = site_setting();
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/image/image_detail',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	
	function update_image($image_id)
	{

		$data = array();
		$data['msg'] = ''; 
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		$paging_setting=paging_setting();
		
		$data['paging_setting']=$paging_setting;
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('image_description', 'image Description', 'required');
		
	
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
				
			}else{
				$data["error"] = "";
			}
			
		    $data["image_description"] = $this->input->post('image_description');
			$data["image_title"] = $this->input->post('image_title');
			$data["image_details"] = $this->user_image_model->get_image_by_id($image_id);
			$data["image_id"] = $image_id;
			
			
			$image_details = $this->user_image_model->get_image_by_id($image_id);
			$data["image_details"] = $image_details;
			$data["image_user_id"] = $image_details->image_user_id;
			$data["image_view_count"]= $image_details->image_view_count;
			$data["image_original_name"]= $image_details->image_original_name;
			$data['image_delete_code'] = $image_details->image_delete_code;
			$data['image_unique_code'] = $image_details->image_unique_code;
			$data["image_name"] = $image_details->image_name;
			$data["show_on_home_page"] = $image_details->image_show_on_home;
			$data['site_setting'] = site_setting();
		
			$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
			$this->template->write_view('center',$theme .'/layout/image/image_detail',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();
		
		}else{
				
			if($this->input->post('image_id')!='')
			{	
				$this->image_model->update_image();		
			}

			redirect('image/image_detail/'.$this->input->post('image_id').'/update','refresh');
			
		}				
			
	}
	
	
	
	function delete_image($image_id,$msg,$offset=0,$id=0)
	{
			
		$image_exists=1;
		if($image_id==0) {$image_exists=0; }
		
		$get_image_detail=get_image_details($image_id);
		
		$data['image_details']=$get_image_detail;
		
		
		if(!$get_image_detail) { $image_exists=0; } 
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['theme']=$theme;
		
		
		$site_setting=site_setting();
		$data['site_setting']=$site_setting;
		
		
		$redirectlink = 'image/list_all_image/15/'.$offset;
	
		if($image_exists==0) 
		{ 	
			redirect($redirectlink.'/fail');
		} 
		else {
								
			redirect($redirectlink.'/delete');					
		}				
		
			
		   			
	}
	
	
	
	///////////==============image commments=================
	
	function list_comment($image_id,$limit=15,$offset=0, $msg='')
	{
		
		//$check_rights=get_rights('list_admin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		
		$data["image_id"]	= $image_id;
		
		$this->load->library('pagination');
		$config['uri_segment']='5';
		$config['base_url'] = site_url('image/list_comment/'.$image_id.'/'.$limit.'/');
		$config['total_rows'] = $this->image_model->get_total_comment_count($image_id);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result']=$this->image_model->get_list_comment_detail_result($image_id,$offset, $limit);
		
		/*echo "<pre>";
		print_r($data['result']);
		die;*/
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		
		
		$data['site_setting'] = site_setting();
		
		
		
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/image/list_comment.php',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	
	}
	
	function search_all_comments($image_id,$limit=15,$option='',$keyword='',$offset=0,$msg='')
	{

		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		if($_POST)
		{		
			$option=$this->input->post('option');
			$keyword=$this->input->post('keyword');
		}
		else
		{
			$option=$option;
			$keyword=$keyword;			
		}
		
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='6';
		$config['base_url'] = site_url('image/search_all_comments/'.$image_id.'/'.$limit.'/'.$option.'/'.$keyword.'/');
		$config['total_rows'] = $this->image_model->get_total_search_comment_count($image_id,$option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->image_model->get_all_search_comment_result($image_id,$option,$keyword,$offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['image_id']=$image_id;
		$data['limit']=$limit;
		$data['option']=$option;
		$data['keyword']=$keyword;
		$data['search_type']='search';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/image/list_comment',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}
	
	
	
	function action_image_comment($image_id)
	{
		
		 $offset=$this->input->post('offset');
		  $limit=$this->input->post('limit');
		 $action=$this->input->post('action');
		 $comment_id=$this->input->post('chk');
		$search_type=$this->input->post('search_type');
		
		$option=$this->input->post('option');
		$keyword=$this->input->post('keyword');
		
		$red_link='list_comment/'.$image_id.'/'.$limit;
		if($search_type=='search')
		{
			$red_link='search_all_comments/'.$image_id.'/'.$limit.'/'.$option.'/'.$keyword;
		}
		
		
		if($action=='delete')
		{	
			foreach($comment_id as $id)
			{
			
				////===
				
				$check_image_comment=$this->db->get_where('image_comment',array('image_comment_id'=>$id));
				
				if($check_image_comment->num_rows()>0)
				{
					$image_comment=$check_image_comment->result();
					
					foreach($image_comment as $imgcmt)
					{
						
						///=====check image comment like
						
						$check_comment_like=$this->db->get_where('image_comment_like',array('comment_id'=>$imgcmt->image_comment_id));
						
						if($check_comment_like->num_rows()>0)
						{
							$this->db->delete('image_comment_like',array('comment_id'=>$imgcmt->image_comment_id));	
						}
						
						////====check comment report
						
						$check_comment_report=$this->db->get_where('block_report',array('comment_id'=>$imgcmt->image_comment_id));
						
						if($check_comment_report->num_rows()>0)
						{
							$this->db->delete('block_report',array('comment_id'=>$imgcmt->image_comment_id));	
						}
						
						////=====
						
					}
					
					///===delete all comment
					$this->db->delete('image_comment',array('image_comment_id'=>$id));				
				}
				
				///====
			
			}
			
			redirect('image/'.$red_link.'/'.$offset.'/delete');
		}
		
		if($action=='active')
		{	
			foreach($comment_id as $id)
			{
			
				$this->db->where('image_comment_id',$id);
				$this->db->update('image_comment',array('comment_status'=>1));
			
			}
			
			redirect('image/'.$red_link.'/'.$offset.'/active');
		}
		
		if($action=='inactive')
		{	
			foreach($comment_id as $id)
			{
			
				$this->db->where('image_comment_id',$id);
				$this->db->update('image_comment',array('comment_status'=>0));
			
			}
			
			redirect('image/'.$red_link.'/'.$offset.'/inactive');
		}
		
		
		
		
	}
	
	
	
	//////////////////////////========================request delettion
	
	function image_delete_request($limit=15,$offset=0,$msg='')
	{
	  	$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='4';
		$config['base_url'] = site_url('image/image_delete_request/'.$limit.'/');
		$config['total_rows'] = $this->image_model->get_total_image_delete_request_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['image_details'] = $this->image_model->get_image_delete_request_count($offset, $limit);
	
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/image/list_all_image_delete_request',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	
	}
	
	function search_all_delete_image_request($limit=15,$option='',$keyword='',$offset=0,$msg='')
	{
		//$check_rights=get_rights('list_all_pin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
		
	
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		if($_POST)
		{		
			$option=$this->input->post('option');
			$keyword=$this->input->post('keyword');
		}
		else
		{
			$option=$option;
			$keyword=$keyword;			
		}
		
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='6';
		$config['base_url'] = site_url('image/search_all_delete_image_request/'.$limit.'/'.$option.'/'.$keyword.'/');
		$config['total_rows'] = $this->image_model->get_total_search_delete_image_request_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['image_details'] = $this->image_model->get_all_search_delete_image_request_result($option,$keyword,$offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']=$option;
		$data['keyword']=$keyword;
		$data['search_type']='search';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/image/image_delete_request',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}
	
	
	
	
	function action_request_image()
	{
		
		 $offset=$this->input->post('offset');
		  $limit=$this->input->post('limit');
		 $action=$this->input->post('action');
		 $request_id=$this->input->post('chk');
		$search_type=$this->input->post('search_type');
		
		$option=$this->input->post('option');
		$keyword=$this->input->post('keyword');
		
		$red_link='image_delete_request/'.$limit;
		if($search_type=='search')
		{
			$red_link='search_all_delete_image_request/'.$limit.'/'.$option.'/'.$keyword;
		}
		
		
		if($action=='delete')
		{	
			foreach($request_id as $id)
			{
			
				$this->db->delete('image_delete_request',array('img_delete_request_id'=>$id));
			
			}
			
			redirect('image/'.$red_link.'/'.$offset.'/delete');
		}
		
		
		
		
	}
	
	
	
	function request_detail($img_delete_request_id=0)
	{
		
		//$check_rights=get_rights('list_admin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
		
		$data = array();
	    $theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		
		
		$request_details = $this->image_model->get_delete_request_by_id($img_delete_request_id);
		
		if(!$request_details)
		{
			redirect('image/image_delete_request/');
		}
		
		$data["request_img_link"] = $request_details->request_img_link;
		$data["request_reason"] = $request_details->request_reason;
		$data["request_date"] = $request_details->request_date;
		$data["request_ip"] = $request_details->request_ip;
		
		
		
		$data["img_delete_request_id"] = $img_delete_request_id;
		$data['site_setting'] = site_setting();
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/image/request_detail',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
		
	}
	
		
	
}
?>