<?php

class Site_setting extends CI_Controller {


	function Site_setting()
	{
		parent::__construct();	
		$this->load->model('site_setting_model');
	}
	
	/*** site setting home page
	**/
	function index()
	{
		redirect('site_setting/add_site_setting');	
	}
	
	
	
	/** admin site setting display and update function
	* var integer $site_setting_id
	* var integer $site_online
	* var integer $captcha_enable
	* var string $site_name
	* var integer $site_version
	* var integer $site_language
	* var string $currency_code
	* var string $date_format
	* var string $time_format
	* var string $date_time_format
	* var string $site_tracker
	* var text $how_it_works_video
	* var integer $zipcode_min
	* var integer $zipcode_max
	* var string $error			
	**/
	function add_site_setting()
	{
		$data = array();
		$check_rights= get_rights('add_site_setting');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('site_name', 'SITE NAME', 'required');
		
		$this->form_validation->set_rules('site_offline_title', 'SITE OFFLINE TITLE', 'required');
		$this->form_validation->set_rules('site_offline_desc', 'SITE OFFLINE DESCRIPTION', 'required');
		
		//$this->form_validation->set_rules('zipcode_min', 'ZIPCODE MANIMUM', 'required|numeric');
		//$this->form_validation->set_rules('zipcode_max', 'ZIPCODE MAXIMUM', 'required|numeric');
		$this->form_validation->set_rules('date_format', 'DATE FORMAT', 'required');
		$this->form_validation->set_rules('time_format', 'TIME FORMAT', 'required');
		//$this->form_validation->set_rules('activate_email_upload', 'Activate Email', 'required|valid_email');
		
		$this->form_validation->set_rules('site_email', 'Site Email', 'required|valid_email');
		
		
		//$this->form_validation->set_rules('google_map_key', 'GOOGLE MAP KEY', 'required');
		//$this->form_validation->set_rules('default_latitude', 'Default Latitude', 'required');
		//$this->form_validation->set_rules('default_longitude', 'Default Longitude', 'required');
		
		$this->form_validation->set_rules('captcha_public_key', 'Captcha Public Key', 'required');
		$this->form_validation->set_rules('captcha_private_key', 'Captcha Private Key', 'required');
		
		
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			if($this->input->post('site_setting_id'))
			{
				$one_site_setting = site_setting(); 
				
				$data["site_setting_id"] = $this->input->post('site_setting_id');
				$data["site_online"] = $this->input->post('site_online');
				
				$data["site_offline_title"] = $this->input->post('site_offline_title');
				$data["site_offline_desc"] = $this->input->post('site_offline_desc');
				
				$data["captcha_enable"] = $this->input->post('captcha_enable');
				$data["site_name"] = $this->input->post('site_name');
				$data["site_version"] = $this->input->post('site_version');
				$data["site_language"] = $this->input->post('site_language');
				$data["currency_code"] = $this->input->post('currency_code');
				$data["date_format"] = $this->input->post('date_format');
				$data["time_format"] = $this->input->post('time_format');	
				$data["date_time_format"] = $this->input->post('date_time_format');	
				$data["site_tracker"] = $this->input->post('site_tracker');
				$data["how_it_works_video"] = $this->input->post('how_it_works_video');
				$data["zipcode_min"] = $this->input->post('zipcode_min');	
				$data["zipcode_max"] = $this->input->post('zipcode_max');
				
				$data["site_timezone"] = $this->input->post('site_timezone');
				
				
				$data["google_map_key"] = $this->input->post('google_map_key');
				$data["default_latitude"] = $this->input->post('default_latitude');
				$data["default_longitude"] = $this->input->post('default_longitude');
				
				$data["site_video"] = $this->input->post('site_video');
				
				$data["site_email"] = $this->input->post('site_email');
				$data["google_plus_link"] = $this->input->post('google_plus_link');
				$data["pinterest_link"] = $this->input->post('pinterest_link');
                                $data["facebook_link"] = $this->input->post('facebook_link');
                                $data["twitter_link"] = $this->input->post('twitter_link');
                                $data["linkedin_link"] = $this->input->post('linkedin_link');
				
				$data["captcha_public_key"] = $this->input->post('captcha_public_key');
				$data["captcha_private_key"] = $this->input->post('captcha_private_key');
				
				
				
			}else{
				$one_site_setting = site_setting(); 

				$data["site_setting_id"] = $one_site_setting->site_setting_id;
				$data["site_online"] = $one_site_setting->site_online;
				
				$data["site_offline_title"] = $one_site_setting->site_offline_title;
				$data["site_offline_desc"] = $one_site_setting->site_offline_desc;
				
				
				$data["captcha_enable"] = $one_site_setting->captcha_enable;
				$data["site_name"] = $one_site_setting->site_name;
				$data["site_version"] = $one_site_setting->site_version;
				$data["site_language"] = $one_site_setting->site_language;
				$data["currency_code"] = $one_site_setting->currency_code;
				$data["date_format"] = $one_site_setting->date_format;
				$data["time_format"] = $one_site_setting->time_format;
				$data["date_time_format"] = $one_site_setting->date_time_format;	
				$data["site_tracker"] = $one_site_setting->site_tracker;
				$data["how_it_works_video"] = $one_site_setting->how_it_works_video;
				$data["zipcode_min"] = $one_site_setting->zipcode_min;
				$data["zipcode_max"] = $one_site_setting->zipcode_max;
				$data["site_timezone"] = $one_site_setting->site_timezone;
				
				
				$data["google_map_key"] = $one_site_setting->google_map_key;
				$data["default_latitude"] = $one_site_setting->default_latitude;
				$data["default_longitude"] = $one_site_setting->default_longitude;
				
				$data["site_video"] = $one_site_setting->site_video;
				
				$data["site_email"] = $one_site_setting->site_email;
				$data["google_plus_link"] = $one_site_setting->google_plus_link;
				$data["pinterest_link"] = $one_site_setting->pinterest_link;
                                
                                $data["facebook_link"] = $one_site_setting->facebook_link;
                                $data["twitter_link"] = $one_site_setting->twitter_link;
                                $data["linkedin_link"] = $one_site_setting->linkedin_link;
				
				$data["captcha_public_key"] = $one_site_setting->captcha_public_key;
				$data["captcha_private_key"] = $one_site_setting->captcha_private_key;
				
			}

		
		}else{
				$this->site_setting_model->site_setting_update();
				$one_site_setting = site_setting(); 
				
				$data["error"] = "Site settings updated successfully.";
				$data["site_setting_id"] = $one_site_setting->site_setting_id;
				$data["site_online"] = $one_site_setting->site_online;
				
				$data["site_offline_title"] = $one_site_setting->site_offline_title;
				$data["site_offline_desc"] = $one_site_setting->site_offline_desc;
				
				
				$data["captcha_enable"] = $one_site_setting->captcha_enable;
				$data["site_name"] = $one_site_setting->site_name;
				$data["site_version"] = $one_site_setting->site_version;
				$data["site_language"] = $one_site_setting->site_language;
				$data["currency_code"] = $one_site_setting->currency_code;
				$data["date_format"] = $one_site_setting->date_format;
				$data["time_format"] = $one_site_setting->time_format;
				$data["date_time_format"] = $one_site_setting->date_time_format;	
				$data["site_tracker"] = $one_site_setting->site_tracker;
				$data["how_it_works_video"] = $one_site_setting->how_it_works_video;
				$data["zipcode_min"] = $one_site_setting->zipcode_min;
				$data["zipcode_max"] = $one_site_setting->zipcode_max;
				$data["site_timezone"] = $one_site_setting->site_timezone;
				
				$data["google_map_key"] = $one_site_setting->google_map_key;
				$data["default_latitude"] = $one_site_setting->default_latitude;
				$data["default_longitude"] = $one_site_setting->default_longitude;
				
				$data["site_video"] = $one_site_setting->site_video;
				
			
				$data["site_email"] = $one_site_setting->site_email;
				$data["google_plus_link"] = $one_site_setting->google_plus_link;
				$data["pinterest_link"] = $one_site_setting->pinterest_link;
                                $data["facebook_link"] = $one_site_setting->facebook_link;
                                $data["twitter_link"] = $one_site_setting->twitter_link;
                                $data["linkedin_link"] = $one_site_setting->linkedin_link;
				
				
				$data["captcha_public_key"] = $one_site_setting->captcha_public_key;
				$data["captcha_private_key"] = $one_site_setting->captcha_private_key;
				
			
			
		}	
		
			$data['language'] = get_languages();
			$data['currency'] = get_currency();
			$data['site_setting'] = site_setting(); 
		
			$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
			$this->template->write_view('center',$theme .'/layout/setting/add_site_setting',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();			
	}
	
	/** admin image size setting display and update function
	* var integer $p_width
	* var integer $p_height
	* var integer $u_width
	* var integer $u_height
	* var string $error			
	**/
	function add_image_setting()
	{
		$data = array();
		$check_rights=get_rights('add_image_setting');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		$this->load->library('form_validation');
		
		
		$this->form_validation->set_rules('user_width', 'User Image Width', 'required|integer');
		$this->form_validation->set_rules('user_height', 'User Image Height', 'required|integer');
                $this->form_validation->set_rules('user_small_thumb_width', 'User Small Image width', 'required|integer');
		$this->form_validation->set_rules('user_small_thumb_height', 'User Small Image Height', 'required|integer');
                
		
                $this->form_validation->set_rules('category_width', 'Categoty Image width', 'required|integer');
		$this->form_validation->set_rules('category_height', 'Category Image Height', 'required|integer');
                
                
                $this->form_validation->set_rules('gallery_thumb_width', 'Gallery Thumb Width', 'required|integer');
		$this->form_validation->set_rules('gallery_thumb_height', 'Gallery Thumb Height', 'required|integer');
                
		
		$this->form_validation->set_rules('design_width', 'Design Width', 'required|integer');
		$this->form_validation->set_rules('design_height', 'Design Height', 'required|integer');
		

		$err = '';
		
		if($_POST)
		{
			
			
			if($this->input->post('user_width') <= 0){
				$err.='<p>User Thumbnail Width should be greator than Zero.</p>';
			}
			if($this->input->post('user_height') <= 0){
				$err.='<p>User Thumbnail Height should be greator than Zero.</p>';
			}
		}
		
		if($this->form_validation->run() == FALSE || $err!=''){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			if($err!=''){
				$data["error"] .= $err;
			}
			if($this->input->post('image_setting_id'))
			{
				$data["image_setting_id"] = $this->input->post('image_setting_id');
				
                                $data["user_width"] = $this->input->post('user_width');
				$data["user_height"] = $this->input->post('user_height');
                                $data["user_small_thumb_width"] = $this->input->post('user_small_thumb_width');
				$data["user_small_thumb_height"] = $this->input->post('user_small_thumb_height');
                                
				$data["category_width"] = $this->input->post('category_width');
				$data["category_height"] = $this->input->post('category_height');
                                
				
                                $data["gallery_thumb_width"] = $this->input->post('gallery_thumb_width');
				$data["gallery_thumb_height"] = $this->input->post('gallery_thumb_height');
                                
                                
				$data["design_width"] = $this->input->post('design_width');
				$data["design_height"] = $this->input->post('design_height');
				
				
				
				
				
			}else{
				$one_img_setting = $this->site_setting_model->get_one_img_setting();
				
				$data["image_setting_id"] = $one_img_setting->image_setting_id;
                                
				$data["user_width"] = $one_img_setting->user_width;
				$data["user_height"] = $one_img_setting->user_height;
                                $data["user_small_thumb_width"] = $one_img_setting->user_small_thumb_width;
				$data["user_small_thumb_height"] = $one_img_setting->user_small_thumb_height;
                                
				$data["category_width"] = $one_img_setting->category_width;
				$data["category_height"] = $one_img_setting->category_height;
                                
				$data["gallery_thumb_width"] = $one_img_setting->gallery_thumb_width;
				$data["gallery_thumb_height"] = $one_img_setting->gallery_thumb_height;
                                
                                $data["design_width"] = $one_img_setting->design_width;
				$data["design_height"] = $one_img_setting->design_height;
				
                                
                                
				
			}
			$data['site_setting'] = site_setting();

			$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
			$this->template->write_view('center',$theme .'/layout/setting/add_image_setting',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();
		}else{
			$this->site_setting_model->img_setting_update();

			$data["error"] = "Image size settings updated successfully.";
			$data["image_setting_id"] = $this->input->post('image_setting_id');
			
                        
                       $data["user_width"] = $this->input->post('user_width');
                        $data["user_height"] = $this->input->post('user_height');
                        $data["user_small_thumb_width"] = $this->input->post('user_small_thumb_width');
                        $data["user_small_thumb_height"] = $this->input->post('user_small_thumb_height');

                        $data["category_width"] = $this->input->post('category_width');
                        $data["category_height"] = $this->input->post('category_height');


                        $data["gallery_thumb_width"] = $this->input->post('gallery_thumb_width');
                        $data["gallery_thumb_height"] = $this->input->post('gallery_thumb_height');


                        $data["design_width"] = $this->input->post('design_width');
                        $data["design_height"] = $this->input->post('design_height');
	
			$data['site_setting'] = site_setting();	
			
			$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
			$this->template->write_view('center',$theme .'/layout/setting/add_image_setting',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();
		}					
	}
        
        
        ///===commission settting
        /** admin commission setting display and update function
	* var string $error			
	**/
	function commission_setting()
	{
		$data = array();
		/*$check_rights=get_rights('add_image_setting');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}*/
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		$this->load->library('form_validation');
		
		
		$this->form_validation->set_rules('admin_commission_image', 'User Image Width', 'required|numeric');
		$this->form_validation->set_rules('admin_commission_video', 'User Image Height', 'required|numeric');
                $this->form_validation->set_rules('admin_commission_challange', 'User Small Image width', 'required|numeric');
		

		$err = '';
		
		
		
		if($this->form_validation->run() == FALSE || $err!=''){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			if($err!=''){
				$data["error"] .= $err;
			}
			if($this->input->post('commission_id'))
			{
				$data["commission_id"] = $this->input->post('commission_id');
				
                                $data["admin_commission_image"] = $this->input->post('admin_commission_image');
				$data["admin_commission_video"] = $this->input->post('admin_commission_video');
                                $data["admin_commission_challange"] = $this->input->post('admin_commission_challange');
				
				
				
				
				
			}else{
				$one_img_setting = $this->site_setting_model->get_commission_setting();
				
				$data["commission_id"] = $one_img_setting->commission_id;
                                
				$data["admin_commission_image"] = $one_img_setting->admin_commission_image;
				$data["admin_commission_video"] = $one_img_setting->admin_commission_video;
                                $data["admin_commission_challange"] = $one_img_setting->admin_commission_challange;
				
                                
                                
				
			}
			$data['site_setting'] = site_setting();

			$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
			$this->template->write_view('center',$theme .'/layout/setting/add_commission_setting',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();
		}else{
			$this->site_setting_model->commission_setting_update();

			$data["error"] = "Commission settings has been updated successfully.";
			$data["commission_id"] = $this->input->post('commission_id');
				
                        $data["admin_commission_image"] = $this->input->post('admin_commission_image');
                        $data["admin_commission_video"] = $this->input->post('admin_commission_video');
                        $data["admin_commission_challange"] = $this->input->post('admin_commission_challange');
	
			$data['site_setting'] = site_setting();	
			
			$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
			$this->template->write_view('center',$theme .'/layout/setting/add_commission_setting',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();
		}					
	}
        
        
        
         ///===credit settting
        /** admin credit setting display and update function
	* var string $error			
	**/
	function credit_setting()
	{
		$data = array();
		/*$check_rights=get_rights('add_image_setting');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}*/
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		$this->load->library('form_validation');
		
		
		$this->form_validation->set_rules('point2paisa', 'Point to Paisa Conversation rate', 'required|numeric');
		

		$err = '';
		
		
		
		if($this->form_validation->run() == FALSE || $err!=''){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			if($err!=''){
				$data["error"] .= $err;
			}
			if($this->input->post('credit_setting_id'))
			{
				$data["credit_setting_id"] = $this->input->post('credit_setting_id');
				
                                $data["point2paisa"] = $this->input->post('point2paisa');
				
				
			}else{
				$one_img_setting = $this->site_setting_model->get_credit_setting();
				
				$data["credit_setting_id"] = $one_img_setting->credit_setting_id;
                                
				$data["point2paisa"] = $one_img_setting->point2paisa;
				
			}
			$data['site_setting'] = site_setting();

			$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
			$this->template->write_view('center',$theme .'/layout/setting/add_credit_setting',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();
		}else{
			$this->site_setting_model->credit_setting_update();

			$data["error"] = "Coversation rate has been updated successfully.";
			$data["credit_setting_id"] = $this->input->post('credit_setting_id');
				
                        $data["point2paisa"] = $this->input->post('point2paisa');
                        
			$data['site_setting'] = site_setting();	
			
			$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
			$this->template->write_view('center',$theme .'/layout/setting/add_credit_setting',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();
		}					
	}
        
}
?>