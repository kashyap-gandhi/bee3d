<?php

class Package extends CI_Controller {

    function Package() {
        parent::__construct();
        $this->load->model('package_model');
        $this->load->model('home_model');
    }

    function index($msg='') {
        redirect('package/list_package');
    }

    function list_package_item($offset=0, $msg='') {
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        $this->load->library('pagination');

        $limit = '15';

        $config['base_url'] = site_url('package/list_package_item/');
        $config['total_rows'] = $this->package_model->get_total_package_item_count();
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        $data['page_link'] = $this->pagination->create_links();
        $data['result'] = $this->package_model->get_all_package_item($offset, $limit);
        $data['msg'] = $msg;
        $data['offset'] = $offset;

        $data['msg'] = $msg;
        $data['offset'] = $offset;
        $data['limit'] = $limit;
        $data['option'] = '';
        $data['keyword'] = '';
        $data['search_type'] = 'normal';

        $data['site_setting'] = site_setting();

        $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
        $this->template->write_view('center', $theme . '/layout/package/list_package_item', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
        $this->template->render();
    }

    function add_package_item() {


        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        $data['site_setting'] = site_setting();


        $this->form_validation->set_rules('item_name', 'Item Name', 'required|alpha_numeric_space');
        $this->form_validation->set_rules('item_status', 'Status', 'required');

        if ($this->form_validation->run() == FALSE) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }
            $data["package_item_id"] = $this->input->post('package_item_id');
            $data["item_name"] = $this->input->post('item_name');
            $data["item_status"] = $this->input->post('item_status');


            if ($this->input->post('offset') == "") {
                $limit = '15';
                $totalRows = $this->package_model->get_total_package_item_count();
                $data["offset"] = (int) ($totalRows / $limit) * $limit;
            } else {
                $data["offset"] = $this->input->post('offset');
            }

            $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
            $this->template->write_view('center', $theme . '/layout/package/add_package_item', $data, TRUE);
            $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
            $this->template->render();
        } else {
            if ($this->input->post('package_item_id')) {
                $this->package_model->package_item_update();
                $msg = "update";
            } else {
                $this->package_model->package_item_insert();
                $msg = "insert";
            }
            $offset = $this->input->post('offset');
            redirect('package/list_package_item/' . $offset . '/' . $msg);
        }
    }

    function edit_package_item($id, $offset=0) {
        
        
        
         $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        $data['site_setting'] = site_setting();


        $package_item_detail = $this->package_model->get_package_item_detail($id);

        $data['package_item_id'] = $id;

        $data["item_name"] = $package_item_detail->item_name;
        $data["item_status"] = $package_item_detail->item_status;

        $data['offset'] = $offset;
        $data['error'] = '';


         $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
            $this->template->write_view('center', $theme . '/layout/package/add_package_item', $data, TRUE);
            $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
            $this->template->render();
    }

    function package_item_action() {

        $offset = $this->input->post('offset');
        $action = $this->input->post('action');
        $package_item_id = $this->input->post('chk');

        if ($action == 'delete') {
            foreach ($package_item_id as $id) {
                $this->db->delete('package_item', array('package_item_id' => $id));
            }

            redirect('package/list_package_item/' . $offset . '/delete');
        }


        if ($action == 'active') {
            foreach ($package_item_id as $id) {
                $this->db->query("update " . $this->db->dbprefix('package_item') . " set item_status=1 where package_item_id='" . $id . "'");
            }

            redirect('package/list_package_item/' . $offset . '/active');
        }

        if ($action == 'inactive') {
            foreach ($package_item_id as $id) {
                $this->db->query("update " . $this->db->dbprefix('package_item') . " set item_status=0 where package_item_id='" . $id . "'");
            }

            redirect('package/list_package_item/' . $offset . '/inactive');
        }
    }

    function list_package($offset=0, $msg='') {
        
         $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');
        
        $this->load->library('pagination');
        $limit = '15';

        $config['base_url'] = site_url('package/list_package/');
        $config['total_rows'] = $this->package_model->get_total_package_count();
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        $data['page_link'] = $this->pagination->create_links();
        $data['result'] = $this->package_model->get_all_package($offset, $limit);
        $data['msg'] = $msg;
        $data['offset'] = $offset;
       $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
            $this->template->write_view('center', $theme . '/layout/package/list_package', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
        $this->template->render();
    }

    function add_package() {
        
        
         $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');
        
        
        $this->form_validation->set_rules('package_name', 'Package Name', 'required|alpha_numeric_space');
        $this->form_validation->set_rules('package_short_description', 'Short Description', 'required');
        $this->form_validation->set_rules('package_description', 'Package Description', 'required');
        
        if($_POST){
            if($this->input->post('package_is_free')!=1){
                $this->form_validation->set_rules('package_price', 'Package Price', 'required|numeric');            
            }
            
            if($this->input->post('package_is_free')!=1 && $this->input->post('package_type')!=1){
                $this->form_validation->set_rules('package_days', 'Package Days', 'required|is_natural_no_zero');
            }
        }
        
        $this->form_validation->set_rules('package_status', 'Status', 'required');

        if ($this->form_validation->run() == FALSE) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }
            $data["package_id"] = $this->input->post('package_id');
            $data["package_name"] = $this->input->post('package_name');
            $data["package_short_description"] = $this->input->post('package_short_description');
            $data["package_description"] = $this->input->post('package_description');
            $data["package_price"] = $this->input->post('package_price');
            
            $data["package_is_free"] = $this->input->post('package_is_free');
            $data["package_points"] = $this->input->post('package_points');
            $data["package_type"] = $this->input->post('package_type');
            
            $data["package_days"] = $this->input->post('package_days');
            $data["package_item"] = '';
            $data["post_package_item"] = $this->input->post('package_item');
            $data["package_status"] = $this->input->post('package_status');

            if ($this->input->post('offset') == "") {
                $limit = '15';
                $totalRows = $this->package_model->get_total_package_count();
                $data["offset"] = (int) ($totalRows / $limit) * $limit;
            } else {
                $data["offset"] = $this->input->post('offset');
            }

            $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
            $this->template->write_view('center', $theme . '/layout/package/add_package', $data, TRUE);
            $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
            $this->template->render();
        } else {
            if ($this->input->post('package_id')) {
                $this->package_model->package_update();
                $msg = "update";
            } else {
                $this->package_model->package_insert();
                $msg = "insert";
            }
            $offset = $this->input->post('offset');
            redirect('package/list_package/' . $offset . '/' . $msg);
        }
    }

    function edit_package($id, $offset=0) {
        
        
         $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        $package_detail = $this->package_model->get_package_detail($id);

        $data['package_id'] = $id;


        $package_short_description = $package_detail->package_short_description;
        $package_short_description = str_replace('KSYDOU', '"', $package_short_description);
        $package_short_description = str_replace('KSYSING', "'", $package_short_description);

        $data["package_short_description"] = $package_short_description;


        $package_description = $package_detail->package_description;
        $package_description = str_replace('KSYDOU', '"', $package_description);
        $package_description = str_replace('KSYSING', "'", $package_description);

        $data["package_description"] = $package_description;


        $data["package_name"] = $package_detail->package_name;
        $data["package_price"] = $package_detail->package_price;
        
        
        $data["package_is_free"] = $package_detail->package_is_free;
        $data["package_points"] = $package_detail->package_points;
        $data["package_type"] = $package_detail->package_type;
        
        
        $data["package_days"] = $package_detail->package_days;

        $data["post_package_item"] = '';

        $data["package_item"] = $package_detail->package_item;
        $data["package_status"] = $package_detail->package_status;


        $data['offset'] = $offset;
        $data['error'] = '';


        $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
            $this->template->write_view('center', $theme . '/layout/package/add_package', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
        $this->template->render();
    }

    function package_action() {

        $offset = $this->input->post('offset');
        $action = $this->input->post('action');
        $package_id = $this->input->post('chk');

        if ($action == 'delete') {
            foreach ($package_id as $id) {
                $this->package_model->delete_package($id);
            }

            redirect('package/list_package/' . $offset . '/delete');
        }

        if ($action == 'active') {
            foreach ($package_id as $id) {
                $this->db->query("update " . $this->db->dbprefix('package') . " set package_status=1 where package_id='" . $id . "'");
            }

            redirect('package/list_package/' . $offset . '/active');
        }

        if ($action == 'inactive') {
            foreach ($package_id as $id) {
                $this->db->query("update " . $this->db->dbprefix('package') . " set package_status=0 where package_id='" . $id . "'");
            }

            redirect('package/list_package/' . $offset . '/inactive');
        }
    }

}

?>