<?php
class Quote extends CI_Controller {
	function Quote()
	{
		 parent::__construct();	
		$this->load->model('quote_model');
		$this->load->model('agent_model');
	}
	
	function index()
	{
		redirect('quote/list_user');
	}
	
	
	/***all quote***/
	function list_all_quote($limit=20,$offset=0,$msg='')
	{
		
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$check_rights=get_rights('list_quote');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		
		
		
		
		$this->load->library('pagination');

		//$limit = '10';
		$config['uri_segment']='4';
		$config['base_url'] = site_url('user/list_all_quote/'.$limit.'/');
		$config['total_rows'] = $this->quote_model->get_total_quote_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->quote_model->get_quote_result($offset, $limit);
		/*echo "<pre>";
		print_r($data['result']);
		die;*/
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		$data['site_setting'] = site_setting();

		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/quote/list_quote',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	
	}
	
	
	/***quote detail***/
	function air_quote_detail($id=0)
	{
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['site_setting'] = site_setting();
		$data['one_quote'] = $this->quote_model->get_one_quote($id);
		
		$this->template->write_view('center',$theme .'/layout/quote/air_quote_detail',$data,TRUE);
		$this->template->render();
		
	}
	
	function approve_air_quote($quote_id='')
	{
		
		if($quote_id == '')
		{
			redirect('quote/list_all_quote');
		}
		$data['approve_quote'] = $this->quote_model->approve_quote($quote_id);
		$data['one_quote'] = $this->quote_model->get_one_quote($quote_id);
		
		$one_quote = $data['one_quote'];
		
		$quote_user_info = get_user_profile_by_id($one_quote->user_id);
		
		
				$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='AirLine Quote'");				$email_temp=$email_template->row();
				
				if($email_temp)
				{
				
				
				$user_name = $quote_user_info->full_name;
				$quote_user_link = site_url('user/'.$quote_user_info->profile_name);
				
				$air_line_from = $one_quote->airline_from;
				$air_line_to = $one_quote->airline_to;
				$airline_message = $one_quote->quote_message;
				$quote_email = $one_quote->trip_quote_email;
				
				$email_from_name=$email_temp->from_name;
				$email_address_from=$email_temp->from_address;
				$email_address_reply=$email_temp->reply_address;
				
				$email_subject=$email_temp->subject;				
				$email_message=$email_temp->message;
				
				$email = $quote_user_info->email;
				$email_to=$quote_user_info->email;
				
				
				$email_message=str_replace('{break}','<br/>',$email_message);
				$email_message=str_replace('{user_name}',$user_name,$email_message);
				$email_message=str_replace('{quote_user_link}',$quote_user_link,$email_message);
				$email_message=str_replace('{quote_email}',$quote_email,$email_message);
				$email_message=str_replace('{air_line_from}',$air_line_from,$email_message);
				$email_message=str_replace('{air_line_to}',$air_line_to,$email_message);
				$email_message=str_replace('{airline_message}',$airline_message,$email_message);
				$email_temp_url=base_url().'email/';
				$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
				$str=$email_message;
				
				/** custom_helper email function **/
				email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
			}
			$limit=20;
			$offset=0;
			redirect('quote/list_all_quote/'.$limit.'/'.$offset.'/approve');
			

		
	}
	
	
	function trip_quote_detail($id=0)
	{
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['site_setting'] = site_setting();
		$data['one_quote'] = $this->quote_model->get_one_quote($id);
		
		$this->template->write_view('center',$theme .'/layout/quote/trip_quote_detail',$data,TRUE);
		$this->template->render();
		
	}
	
	function approve_trip_quote($quote_id='')
	{
		
		if($quote_id == '')
		{
			redirect('quote/list_all_quote');
		}
		$data['approve_trip_quote'] = $this->quote_model->approve_quote($quote_id);
		$data['one_quote'] = $this->quote_model->get_one_quote($quote_id);
		
		$one_quote = $data['one_quote'];
		
		$quote_user_info = get_user_profile_by_id($one_quote->user_id);
		
		
				$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Trip Quote'");				$email_temp=$email_template->row();
				
				if($email_temp)
				{
				
				
				$user_name = $quote_user_info->full_name;
				$quote_user_link = site_url('user/'.$quote_user_info->profile_name);
				
				$trip_from = $one_quote->trip_from_place;
				$trip_to = $one_quote->trip_to_place;
				$trip_message = $one_quote->quote_message;
				$quote_email = $one_quote->trip_quote_email;
				
				$trip_total_person = $one_quote->total_people;
				$trip_total_adult = $one_quote->total_adult;
				$trip_total_child = $one_quote->total_child;
				$trip_day = $one_quote->trip_days;
				$trip_night= $one_quote->trip_nights;
				$trip_phone = $one_quote->trip_quote_phone;
				
				$email_from_name=$email_temp->from_name;
				$email_address_from=$email_temp->from_address;
				$email_address_reply=$email_temp->reply_address;
				
				$email_subject=$email_temp->subject;				
				$email_message=$email_temp->message;
				
				$email = $quote_user_info->email;
				$email_to=$quote_user_info->email;
				
				
				$email_message=str_replace('{break}','<br/>',$email_message);
				$email_message=str_replace('{user_name}',$user_name,$email_message);
				$email_message=str_replace('{quote_user_link}',$quote_user_link,$email_message);
				$email_message=str_replace('{quote_email}',$quote_email,$email_message);
				$email_message=str_replace('{trip_from}',$trip_from,$email_message);
				$email_message=str_replace('{trip_to}',$trip_to,$email_message);
				$email_message=str_replace('{trip_message}',$trip_message,$email_message);
				$email_message=str_replace('{trip_total_person}',$trip_total_person,$email_message);
				$email_message=str_replace('{trip_total_adult}',$trip_total_adult,$email_message);
				$email_message=str_replace('{trip_total_child}',$trip_total_child,$email_message);
				$email_message=str_replace('{trip_day}',$trip_day,$email_message);
				$email_message=str_replace('{trip_night}',$trip_night,$email_message);
				$email_message=str_replace('{trip_phone}',$trip_phone,$email_message);
				
				
				$email_temp_url=base_url().'email/';
				$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
				$str=$email_message;
				
				/** custom_helper email function **/
				email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
			}
			$limit=20;
			$offset=0;
			redirect('quote/list_all_quote/'.$limit.'/'.$offset.'/approve');
			

		
	}
	
		 /*********************************************search users*********************************************/
	
	function search_list_quote($limit=20,$option='',$keyword='',$offset=0,$msg='')
	{
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$check_rights=get_rights('list_quote');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		
		
		
		$this->load->library('pagination');
		
		
		if($_POST)
		{		
			$option=$this->input->post('option');
			$keyword=$this->input->post('keyword');
		}
		else
		{
			$option=$option;
			$keyword=$keyword;			
		}
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
	
		$config['uri_segment']='5';
		$config['base_url'] = site_url('quote/search_list_quote/'.$limit.'/'.$option.'/'.$keyword.'/');
		$config['total_rows'] = $this->quote_model->get_total_search_quote_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->quote_model->get_search_quote_result($option,$keyword,$offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		
		
		//$data['statelist']=$this->project_category_model->get_state();
		
		$data['site_setting'] = site_setting();
		
		$data['limit']=$limit;
		$data['option']=$option;
		$data['keyword']=$keyword;
		$data['search_type']='search';

		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/quote/list_quote',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*****************************************************************************************************************************/
	
	
	
	
	/*** check the user unique email address
	*	var string $email
	*  return boolen
	**/
		
	function email_check($email)
	{
		$cemail = $this->user_model->emailTaken($email);
		
		if($cemail)
		{
			$this->form_validation->set_message('email_check', 'There is an existing account associated with this email');
			return FALSE;
		}
		else
		{
				return TRUE;
		}
	}	
	
	
	function add_user()
	{
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$site_setting = site_setting();
		
		
		
		
		
		$this->form_validation->set_rules('first_name', 'First Name', 'required|alpha');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required|alpha');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');		
	/*	$this->form_validation->set_rules('zip_code', 'Postal Code', 'required|alpha_numeric|min_length['.$site_setting->zipcode_min.']|max_length['.$site_setting->zipcode_max.']');	
		$this->form_validation->set_rules('mobile_no', 'Mobile No.', 'numeric|exact_length[10]');	
		$this->form_validation->set_rules('phone_no', 'Phone No.', 'numeric|min_length[9]|max_length[12]');	*/
		
		if($this->form_validation->run() == FALSE)
		{
				if(validation_errors())
				{													
					$data["error"] = validation_errors();
				}
				else
				{		
					$data["error"] = "";							
				}
				
				
				
					$data['first_name']=$this->input->post('first_name');
					$data['last_name']=$this->input->post('last_name');
					$data['email']=$this->input->post('email');
					$data['zip_code']=$this->input->post('zip_code');
					$data['mobile_no']=$this->input->post('mobile_no');
					$data['phone_no']=$this->input->post('phone_no');
					
					$data['is_worker']=$this->input->post('is_worker');
					$data['user_status']=$this->input->post('user_status');
					
					$data['current_city']=$this->input->post('current_city');
					$data['about_user']=$this->input->post('about_user');	
					
					
		} else	{
				
			
			$insert_return_paasword=$this->user_model->add_user();
			
			
			
			
			
					$email=$this->input->post('email');
					$get_user_info=$this->db->get_where('user',array('email'=>$email));
					$user_info=$get_user_info->row();
					
					$username =$user_info->full_name;				
					$email_verification_code=$user_info->email_verification_code;
					$user_id=$user_info->user_id;
					
					$verification_link=front_base_url().'verify/'.$user_id.'/'.$email_verification_code;
				
				
			/////////////============welcome email===========	

				
				$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Welcome Email'");
				$email_temp=$email_template->row();
				
				
				$email_address_from=$email_temp->from_address;
				$email_address_reply=$email_temp->reply_address;
				
				$email_subject=$email_temp->subject;				
				$email_message=$email_temp->message;			
				
								
				$email_to=$this->input->post('email');		
				
			
				
				$email_message=str_replace('{break}','<br/>',$email_message);
				$email_message=str_replace('{user_name}',$username,$email_message);			
				$email_message=str_replace('{email}',$email,$email_message);
				
				$email_temp_url=upload_url().'email/';
				$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
				
				
				$str=$email_message;		
				
					
				/** custom_helper email function **/
					
				email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str);
				
				
				
				/////////////============new user email===========	


				
				$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='New User Join'");
				$email_temp=$email_template->row();
				
				
				$email_address_from=$email_temp->from_address;
				$email_address_reply=$email_temp->reply_address;
				
				$email_subject=$email_temp->subject;				
				$email_message=$email_temp->message;
				
				$username =$user_info->full_name;
				$password = $insert_return_paasword;
				$email = $this->input->post('email');
				
				$email_to=$this->input->post('email');
				
				$login_link=front_base_url().'sign_up';
				
				
				
				$email_message=str_replace('{break}','<br/>',$email_message);
				$email_message=str_replace('{user_name}',$username,$email_message);
				$email_message=str_replace('{password}',$password,$email_message);
				$email_message=str_replace('{email}',$email,$email_message);
				$email_message=str_replace('{login_link}',$login_link,$email_message);
				$email_message=str_replace('{verify_email_link}',$verification_link,$email_message);
				
				$email_temp_url=upload_url().'email/';
				$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
				
				
				
				$str=$email_message;
			
				
				/** custom_helper email function **/
				
				email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str);

				

				/////////////============new user email===========	
			
			
				
				
				
				
			
			
			
			
			redirect('user/list_user/20/0/insert/');
			
			
			/////===send email and make random password  and auto active worker, user, app approved 
		
			///=====email to user login detail and if worker then worker edit link
				
				
				
				
		}			
			
		
		$data['site_setting'] =	$site_setting;	
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/user/add_user',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
		
		
		
		
	}
	
	

	
	
	/***waiting users***/
	
	function list_waiting_user($limit=20,$offset=0,$msg='')
	{
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$check_rights=get_rights('list_waiting_user');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		
		
		
		
		$this->load->library('pagination');

		//$limit = '10';
		$config['uri_segment']='4';
		$config['base_url'] = site_url('user/list_waiting_user/'.$limit.'/');
		$config['total_rows'] = $this->user_model->get_total_waiting_user_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->user_model->get_waiting_user_result($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		$data['site_setting'] = site_setting();

		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/user/list_waiting_user',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	
	function send_join_request($request_id)
	{
		$this->user_model->send_join_request($request_id);
		
		redirect('user/list_waiting_user/20/0/request_send');
	}
	function send_email_all()
	{ 

	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$check_rights=get_rights('list_waiting_user');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		
		
		 $action=$this->input->post('action');
		 $request_arr=$this->input->post('chk');
		
		
		//print_r($trans_id);
		//exit;
		
		
		if($action=='send')
		{	
			foreach($request_arr as $request_id)
			{
				$this->user_model->send_join_request($request_id);
			}
			
			redirect('user/list_waiting_user/20/0/request_send');
		}
		
		
		
	}
	
		
	 function edit_status($user_id,$page){
	// echo $page;  die();
	 	if($user_id==0 || $user_id = '') {			
			redirect('user/list_user');	
		}
		
	/* echo '<pre>'; print_r($_POST);
		 die();*/
		
		 
		 if($_POST){
				
				$user_status = $this->input->post('user_status_'.$this->input->post('user_id'));
			  
			 
					$userdata = array(
						'user_status' => $user_status
					);		
					$this->db->where('user_id',$this->input->post('user_id'));
					$this->db->update('user',$userdata);
					
					if($user_status == 2){
					
					//echo '<pre>'; print_r($_POST);
					$day = $this->input->post('no_of_day');
					
					$suspend_to_date =date('Y-m-d H:i:s',mktime(date('H'),date('i'),date('s'),date('m'),date('d')+$day,date('Y')));
					
					
						$suspenddata = array(
							'user_id' => $this->input->post('user_id'),
							'suspend_from_date' => date('Y-m-d H:i:s'),
							'suspend_to_date' => $suspend_to_date,
							'suspend_reason' => $this->input->post('suspend_reason'),
							'is_permanent' => $this->input->post('is_permanent'),
						);
						//print_r($suspenddata); die();		
						$this->db->insert('user_suspend',$suspenddata);
					}
					
					
					if($user_status == 3)
					{
						$this->user_model->deactivateuser($this->input->post('user_id'));
					}
				
				
		 }
		 
		if($page == 'all') { redirect('user/list_user');	} 
		if($page == 'suspend') { redirect('user/list_suspend_user');	}
		
		if($page == 'active') { redirect('user/list_active_user');	}
		if($page == 'inactive') { redirect('user/list_inactive_user');	}
		
		 
	 }
	 
	 function change_password($user_id){
 		
		
		$user_info=$this->user_model->get_one_user($user_id);

		$password = randomCode();

		$data=array(
			'password'=>md5($password)		
		);
		
		$this->db->where('user_id',$user_id);		
		$this->db->update('user', $data); 
		
		///================Send Email To user start
			$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Change Password By Admin'");
			$email_temp=$email_template->row();
			
			
			$email_address_from=$email_temp->from_address;
			$email_address_reply=$email_temp->reply_address;
			
			$email_subject=$email_temp->subject;				
			$email_message=$email_temp->message;			
			
							
			$email_to=$user_info->email;		
		
			$email_message=str_replace('{break}','<br/>',$email_message);	
			$email_message=str_replace('{user_name}',ucfirst($user_info->full_name),$email_message);		
			$email_message=str_replace('{password}',$password,$email_message);	
			
			
			$email_temp_url=upload_url().'email/';
			$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
				
			
			$str=$email_message;
			
			/** custom_helper email function **/
				
			email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str);
		///================Send Email To user end
		redirect('user/list_user/20/0/password/');	

	 }
	 
	 /***user action***/
	
	 function action_user()
	{ 
	//echo '<pre>'; print_r($_POST); die();
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$check_rights=get_rights('list_user');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		
		
		
		
		
		 $offset=$this->input->post('offset');
		 $action=$this->input->post('action');
		 $user_id=$this->input->post('chk');
		 $fname=$this->input->post('fname');
		
		//print_r($trans_id);
		//exit;
		
		
		if($action=='delete')
		{	
			foreach($user_id as $id)
			{
			
				$this->user_model->deactivateuser($id);
			
			}
			
			redirect('user/'.$fname.'/20/'.$offset.'/delete');
		}
		elseif($action=='edit'){
		
			foreach($user_id as $id)
			{
				
				$user_status = $this->input->post('user_status_'.$id);

				$userdata = array(
						'user_status' => $user_status
				);		
				$this->db->where('user_id',$id);
				$this->db->update('user',$userdata);
				
				if($user_status == 2){

				$day = $this->input->post('no_of_day_'.$id);
				
				$suspend_to_date =date('Y-m-d H:i:s',mktime(date('H'),date('i'),date('s'),date('m'),date('d')+$day,date('Y')));
				
				
					$suspenddata = array(
						'user_id' => $id,
						'suspend_from_date' => date('Y-m-d H:i:s'),
						'suspend_to_date' => $suspend_to_date,
						'suspend_reason' => $this->input->post('suspend_reason_'.$id),
						'is_permanent' => $this->input->post('is_permanent_'.$id),
					);	
					$this->db->insert('user_suspend',$suspenddata);
				}
			}
	
			redirect('user/'.$fname.'/20/'.$offset.'/edit');
		}
		
		
	}
	
	 

	
	

	
	
	
	/*********************************************active users*********************************************/
	
	function list_active_user($limit=20,$offset=0,$msg='')
	{
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$check_rights=get_rights('list_user');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		$this->load->library('pagination');

		//$limit = '10';
		$config['uri_segment']='4';
		$config['base_url'] = site_url('user/list_active_user/'.$limit.'/');
		$config['total_rows'] = $this->user_model->get_total_active_user_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->user_model->get_active_user_result($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		$data['site_setting'] = site_setting();

		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/user/list_active_user',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	
	/***export all acive users***/
	
	function export_active_user($time='')
	{
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$check_rights=get_rights('list_worker');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		
		
		$this->load->dbutil();
		$query = $this->db->query("select * from ".$this->db->dbprefix('user')." where user_status=1 order by user_id");		
		
		$delimiter = ",";
		$newline = "\r\n";

		$csv= $this->dbutil->csv_from_result($query, $delimiter, $newline);
		
		 $file_name = "active_user_".date('d-m-Y').".csv";
   		 force_download($file_name,$csv);
		$this->db->cache_delete_all();	
	}
	
	
	/*********************************************inactive users*********************************************/
	
	function list_inactive_user($limit=20,$offset=0,$msg='')
	{
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$check_rights=get_rights('list_user');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		$this->load->library('pagination');

		//$limit = '10';
		$config['uri_segment']='4';
		$config['base_url'] = site_url('user/list_inactive_user/'.$limit.'/');
		$config['total_rows'] = $this->user_model->get_total_inactive_user_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->user_model->get_inactive_user_result($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		$data['site_setting'] = site_setting();

		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/user/list_inactive_user',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	
	/**************************************************suspend users**************************************************/
	
	function list_suspend_user($limit=20,$offset=0,$msg='')
	{
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$check_rights=get_rights('list_user');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		$this->load->library('pagination');

		//$limit = '10';
		$config['uri_segment']='4';
		$config['base_url'] = site_url('user/list_suspend_user/'.$limit.'/');
		$config['total_rows'] = $this->user_model->get_total_suspend_user_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->user_model->get_suspend_user_result($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		$data['site_setting'] = site_setting();

		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/user/list_suspend_user',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	
	
	/**************************************************delete users**************************************************/
	
	function list_deleted_user($limit=20,$offset=0,$msg='')
	{
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$check_rights=get_rights('list_user');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		$this->load->library('pagination');

		//$limit = '10';
		$config['uri_segment']='4';
		$config['base_url'] = site_url('user/list_active_user/'.$limit.'/');
		$config['total_rows'] = $this->user_model->get_total_delete_user_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->user_model->get_delete_user_result($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		$data['site_setting'] = site_setting();

		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/user/list_deleted_user',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	
	function delete_forever()
	{
	     $theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$check_rights=get_rights('list_user');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
	     
	     $offset=$this->input->post('offset');
		 $action=$this->input->post('action');
		 $user_id=$this->input->post('chk');
	   foreach($user_id as $id)
			{
				$this->user_model->delete_forever($id);
			}
			
			redirect('user/list_deleted_user/20/'.$offset.'/delete');
	}
	
	/***export all deleted users***/
	function export_deleted_user($time='')
	{
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$check_rights=get_rights('list_worker');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		
		
		$this->load->dbutil();
		$query = $this->db->query("select * from ".$this->db->dbprefix('user')." where user_status=3 order by user_id");		
		
		$delimiter = ",";
		$newline = "\r\n";

		$csv= $this->dbutil->csv_from_result($query, $delimiter, $newline);
		
		 $file_name = "Deleted_user_".date('d-m-Y').".csv";
   		 force_download($file_name,$csv);
		$this->db->cache_delete_all();	
	}
	
	
	
	
	/*******************************************************user all login*******************************************************/
	function user_login($offset=0,$msg='')
	{
		
		$check_rights=get_rights('user_login');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$this->load->library('pagination');

		$limit = '20';
		
		$config['base_url'] = site_url('user/user_login/');
		$config['total_rows'] = $this->user_model->get_total_userlogin_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['msg']=$msg;
		$data['result'] = $this->user_model->get_userlogin_result($offset, $limit);
		$data['offset'] = $offset;
		
		$data['site_setting'] = site_setting();

		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/user/list_user_login',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	
	function action_login()
	{
		$check_rights=get_rights('user_login');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		
		
		$offset=$this->input->post('offset');
		$action=$this->input->post('action');
		$login_id=$this->input->post('chk');
			
		
		if($action=='delete')
		{		
			foreach($login_id as $id)
			{			
				$this->db->query("delete from ".$this->db->dbprefix('user_login')." where login_id='".$id."'");
			}
			
			redirect('user/user_login/'.$offset.'/delete');
		}	
		
		
		
	}
	
	

	 
	 /**********************************comment part *************************************/
	 
	 
	function list_comment_detail($limit='15',$offset=0,$msg='')
	{
		
	    $theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		
		$this->load->library('pagination');
        // $check_rights=get_rights('list_admin');
		
		//if(	$check_rights==0) {			
			//redirect('home/dashboard/no_rights');	
		//}
		
		//$user_comment=$this->user_model->get_comment_detail();

		$config['uri_segment']='4';
		$config['base_url'] = site_url('user/list_comment_detail/'.$limit.'/');
		$config['total_rows'] = $this->user_model->get_total_comment_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->user_model->get_list_comment_detail_result($offset, $limit);
		
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		
		
		$data['site_setting'] = site_setting();

		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/user/list_comment_detail',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	function search_all_comments($limit=15,$option='',$keyword='',$offset=0,$msg='')
	{

		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		if($_POST)
		{		
			$option=$this->input->post('option');
			$keyword=$this->input->post('keyword');
		}
		else
		{
			$option=$option;
			$keyword=$keyword;			
		}
		
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='6';
		$config['base_url'] = site_url('user/search_all_comments/'.$limit.'/'.$option.'/'.$keyword.'/');
		$config['total_rows'] = $this->user_model->get_total_search_comment_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->user_model->get_all_search_comment_result($option,$keyword,$offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']=$option;
		$data['keyword']=$keyword;
		$data['search_type']='search';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/user/list_comment_detail',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}
	
	
	function action_all_comment()
	{
		
		 $offset=$this->input->post('offset');
		  $limit=$this->input->post('limit');
		 $action=$this->input->post('action');
		 $comment_id=$this->input->post('chk');
		$search_type=$this->input->post('search_type');
		
		$option=$this->input->post('option');
		$keyword=$this->input->post('keyword');
		
		$red_link='list_comment_detail/'.$limit;
		if($search_type=='search')
		{
			$red_link='search_all_comments/'.$limit.'/'.$option.'/'.$keyword;
		}
		
		
		if($action=='delete')
		{	
			foreach($comment_id as $id)
			{
			
				////===
				
				$check_image_comment=$this->db->get_where('image_comment',array('image_comment_id'=>$id));
				
				if($check_image_comment->num_rows()>0)
				{
					$image_comment=$check_image_comment->result();
					
					foreach($image_comment as $imgcmt)
					{
						
						///=====check image comment like
						
						$check_comment_like=$this->db->get_where('image_comment_like',array('comment_id'=>$imgcmt->image_comment_id));
						
						if($check_comment_like->num_rows()>0)
						{
							$this->db->delete('image_comment_like',array('comment_id'=>$imgcmt->image_comment_id));	
						}
						
						////====check comment report
						
						$check_comment_report=$this->db->get_where('block_report',array('comment_id'=>$imgcmt->image_comment_id));
						
						if($check_comment_report->num_rows()>0)
						{
							$this->db->delete('block_report',array('comment_id'=>$imgcmt->image_comment_id));	
						}
						
						////=====
						
					}
					
					///===delete all comment
					$this->db->delete('image_comment',array('image_comment_id'=>$id));				
				}
				
				///====
			
			}
			
			redirect('user/'.$red_link.'/'.$offset.'/delete');
		}
		
		if($action=='active')
		{	
			foreach($comment_id as $id)
			{
			
				$this->db->where('image_comment_id',$id);
				$this->db->update('image_comment',array('comment_status'=>1));
			
			}
			
			redirect('user/'.$red_link.'/'.$offset.'/active');
		}
		
		if($action=='inactive')
		{	
			foreach($comment_id as $id)
			{
			
				$this->db->where('image_comment_id',$id);
				$this->db->update('image_comment',array('comment_status'=>0));
			
			}
			
			redirect('user/'.$red_link.'/'.$offset.'/inactive');
		}
		
		
		
		
	}
	

		
	
	
	
	
	
}
?>