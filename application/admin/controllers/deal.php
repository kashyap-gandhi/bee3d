<?php
class Deal extends  CI_Controller {

	function Deal()
	{
		 parent::__construct();	
		$this->load->model('deal_model');
		$this->load->model('user_model');
			
	}
	
	
	function index()
	{
		redirect('deal/list_deal');	
	}
	
	
	function add_deal()
	{
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		/*$check_rights=get_rights('list_admin');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		*/
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('deal_title', 'Deal Title', 'trim|required');
		$this->form_validation->set_rules('deal_amount', 'Deal Amount', 'trim|required|numeric');
		$this->form_validation->set_rules('deal_days', 'Deal Days', 'required|numeric');
		
		
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			$data["deal_id"] = $this->input->post('deal_id');
			$data["user_id"] = $this->input->post('user_id');
			$data["deal_title"] = $this->input->post('deal_title');
			$data["deal_summary"] = $this->input->post('deal_summary');
			$data["deal_description"] = $this->input->post('deal_description');
			$data["deal_amount"] = $this->input->post('deal_amount');		
			$data["deal_status"] = $this->input->post('deal_status');
			$data['deal_days']=$this->input->post('deal_days');
			$data['site_setting'] = site_setting();
			$data['prev_deal_image']=$this->input->post('prev_deal_image');
					
			
			$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
			$this->template->write_view('center',$theme .'/layout/deal/add_deal',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();
		}else{
				
			if($this->input->post('deal_id')!='')
			{	
				$this->deal_model->deal_update();
				$msg = "update";
			}else{
				$this->deal_model->deal_insert();			
				$msg = "insert";
			}
			$offset = $this->input->post('offset');
			redirect('deal/list_deal/all/15/0/'.$msg);
		}				
	}
	
	function edit_deal($deal_id=0,$offset=0)
	{
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		/*$check_rights=get_rights('list_admin');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}*/
		
		if($deal_id=='' || $deal_id==0)
		{
			redirect('deal/list_deal');
		}
		
		
		$deal_detail = $this->deal_model->get_one_deal_by_id($deal_id);
		
		if(!$deal_detail)
		{
			redirect('deal/list_deal');
		}
		
		
		$data["error"] = "";
	
		$data["deal_id"] = $deal_id;
		$data["user_id"] = $deal_detail->user_id;
		$data["deal_title"] = $deal_detail->deal_title;
		$data["deal_summary"] = $deal_detail->deal_summary;
		$data["deal_description"] = $deal_detail->deal_description;
		$data["deal_amount"] = $deal_detail->deal_amount;
		$data["deal_status"] = $deal_detail->deal_status;
		$data['deal_days']=$deal_detail->deal_days;
		$data['prev_deal_image']=$deal_detail->deal_image;
		
		$data['site_setting'] = site_setting();
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/deal/add_deal',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	
	function list_deal($filter='all',$limit=15,$offset=0,$msg='')
	{
		//$check_rights=get_rights('list_all_pin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
		
		if($filter=='')
		{
			redirect('deal/list_deal/all');	
		}
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='4';
		
		$config['base_url'] = site_url('deal/list_deal/'.$filter.'/'.$limit.'/');
		$config['total_rows'] = $this->deal_model->get_total_deal_count($filter);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->deal_model->get_all_deal_result($filter,$offset, $limit);
		
		
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		$data['filter']=$filter;
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/deal/list_deal',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();
	}
	
	function search_list_deal($filter='all',$limit=15,$option='',$keyword='',$offset=0,$msg='')
	{
		//$check_rights=get_rights('list_all_pin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
		
		if($filter=='')
		{
			redirect('deal/search_list_deal/all/'.$limit.'/'.$option.'/'.$keyword.'/');	
		}
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		
		
		$this->load->library('pagination');

	
		if($_POST)
		{		
			$option=$this->input->post('option');
			$keyword=$this->input->post('keyword');
		}
		else
		{
			$option=$option;
			$keyword=$keyword;			
		}
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
	
		$config['uri_segment']='6';
		
		$config['base_url'] = site_url('deal/search_list_deal/'.$filter.'/'.$limit.'/'.$option.'/'.$keyword.'/');
		$config['total_rows'] = $this->deal_model->get_total_search_deal_count($filter,$option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->deal_model->get_all_search_deal_result($filter,$option,$keyword,$offset, $limit);
		
		
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']=$option;
		$data['keyword']=$keyword;
		$data['search_type']='search';
		
		$data['filter']=$filter;
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/deal/list_deal',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();
	}
	
	
	function action_deal($filter='all',$limit=20)
	{
		
		$filter=$this->input->post('filter');
		
		$offset=$this->input->post('offset');
		$limit=$this->input->post('limit');
		$action=$this->input->post('action');
		$deal_id=$this->input->post('chk');
		$search_type=$this->input->post('search_type');
		
		$option=$this->input->post('option');
		$keyword=$this->input->post('keyword');
		
		$red_link='list_deal/'.$filter.'/'.$limit;
		
		if($search_type=='search')
		{
			$red_link='search_list_deal/'.$filter.'/'.$limit.'/'.$option.'/'.$keyword;
		}
		
		
		if($action=='delete')
		{	
			foreach($deal_id as $id)
			{			
				$this->db->delete('deal',array('deal_id'=>$id));	
			}
			
			redirect('deal/'.$red_link.'/'.$offset.'/delete');
		}
		
		if($action=='accept')
		{	
			foreach($deal_id as $id)
			{			
				$this->db->where('deal_id',$id);
				$this->db->update('deal',array('deal_status'=>2));			
			}
			
			redirect('deal/'.$red_link.'/'.$offset.'/accept');
		}
		
		if($action=='pending')
		{	
			foreach($deal_id as $id)
			{			
				$this->db->where('deal_id',$id);
				$this->db->update('deal',array('deal_status'=>1));			
			}
			
			redirect('deal/'.$red_link.'/'.$offset.'/pending');
		}
		
		if($action=='reject')
		{	
			foreach($deal_id as $id)
			{			
				$this->db->where('deal_id',$id);
				$this->db->update('deal',array('deal_status'=>3));			
			}
			
			redirect('deal/'.$red_link.'/'.$offset.'/reject');
		}
		
		if($action=='inactive')
		{	
			foreach($deal_id as $id)
			{			
				$this->db->where('deal_id',$id);
				$this->db->update('deal',array('deal_status'=>0));			
			}
			
			redirect('deal/'.$red_link.'/'.$offset.'/inactive');
		}
		
		
	}
	
	
}
?>