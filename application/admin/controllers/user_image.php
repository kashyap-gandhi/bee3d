<?php
class User_image extends CI_Controller {
	function User_image()
	{
		 parent::__construct();	
		$this->load->model('user_image_model');
		$this->load->model('image_model');
		$this->load->model('user_model');
		$this->load->model('user_album_model');
	}
	
	function index()
	{
		redirect('image/list_all_image');
	}
	
	
	
	
	function list_images($user_id,$limit=15,$offset=0,$msg='')
	{
		//$check_rights=get_rights('user_list_pin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
		
		$check_user_profile=$this->user_image_model->check_user_profile_exists($user_id);
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		if(!$check_user_profile) 
		{ 	
			redirect('user/list_active_user');
		
		} else {

			$user_profile=$this->user_image_model->get_user_profile($user_id);
			
			if(!$user_profile) 
			{ 	
				redirect('user/list_active_user');
					
			} else {
					$data['tab_select']='board';
					$data['user_profile']=$user_profile;
					$data['profile_name']=$user_profile->profile_name;	
					$data['user_id']=$user_profile->user_id;

					$this->load->library('pagination');
	
					$config['uri_segment']='5';
					$config['base_url'] = site_url('user_image/list_images/'.$user_id.'/'.$limit.'/');
					$config['total_rows'] = $this->user_image_model->get_user_total_image($user_id);
					$config['per_page'] = $limit;		
					$this->pagination->initialize($config);		
					$data['page_link'] = $this->pagination->create_links();
					
					$data['image_details'] = $this->user_image_model->get_user_image($user_id,$limit,$offset);
				
					
					$data['msg'] = $msg;
					$data['offset'] = $offset;
					$data['theme']=$theme;	
					
					$data['limit']=$limit;
					$data['option']='';
					$data['keyword']='';
					$data['search_type']='normal';
					
					$data['site_setting'] = site_setting();
				
				    $this->template->write_view('center',$theme .'/layout/user_image/list_image',$data,TRUE);
			}
		
		}
		
		
		
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	}
	
	
	function search_all_image($user_id,$limit=15,$option='',$keyword='',$offset=0,$msg='')
	{
		//$check_rights=get_rights('user_list_pin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
		
		
		$check_user_profile=$this->user_image_model->check_user_profile_exists($user_id);
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		if(!$check_user_profile) 
		{ 	
			redirect('user/list_active_user');
		
		} else {

			$user_profile=$this->user_image_model->get_user_profile($user_id);
			
			if(!$user_profile) 
			{ 	
				redirect('user/list_active_user');
					
			} else {
	
	
				$theme = getThemeName();
				$this->template->set_master_template($theme .'/template.php');
				
				if($_POST)
				{		
					$option=$this->input->post('option');
					$keyword=$this->input->post('keyword');
				}
				else
				{
					$option=$option;
					$keyword=$keyword;			
				}
				
				
				$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
				
				
				$this->load->library('pagination');
				
				$config['uri_segment']='7';
				$config['base_url'] = site_url('user_image/search_all_image/'.$user_id.'/'.$limit.'/'.$option.'/'.$keyword.'/');
				$config['total_rows'] = $this->user_image_model->get_total_search_image_count($user_id,$option,$keyword);
				$config['per_page'] = $limit;		
				$this->pagination->initialize($config);		
				$data['page_link'] = $this->pagination->create_links();
				
				$data['tab_select']='board';
				$data['user_profile']=$this->user_image_model->get_user_profile($user_id);
				
				$data['image_details'] = $this->user_image_model->get_all_search_image_result($user_id,$option,$keyword,$offset, $limit);
				$data['msg'] = $msg;
				$data['offset'] = $offset;
				$data['theme']=$theme;	
				$data['user_id']=$user_id;
				$data['limit']=$limit;
				$data['option']=$option;
				$data['keyword']=$keyword;
				$data['search_type']='search';
				
				
				
				$data['site_setting'] = site_setting();
				
				$this->template->write_view('center',$theme .'/layout/user_image/list_image',$data,TRUE);	
			}
		}
				
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}	
	
	
	function action_all_image($user_id)
	{
		
		 $offset=$this->input->post('offset');
		  $limit=$this->input->post('limit');
		 $action=$this->input->post('action');
		 $image_id=$this->input->post('chk');
		$search_type=$this->input->post('search_type');
		
		$option=$this->input->post('option');
		$keyword=$this->input->post('keyword');
		
		$red_link='list_images/'.$user_id.'/'.$limit;
		if($search_type=='search')
		{
			$red_link='search_all_image/'.$user_id.'/'.$limit.'/'.$option.'/'.$keyword;
		}
		
		
		if($action=='delete')
		{	
			foreach($image_id as $id)
			{
			
				$this->image_model->delete_image($id);	
			
			}
			
			redirect('user_image/'.$red_link.'/'.$offset.'/delete');
		}
		
		if($action=='active')
		{	
			foreach($image_id as $id)
			{
			
				$this->db->where('image_id',$id);
				$this->db->update('images',array('image_active'=>1));
			
			}
			
			redirect('user_image/'.$red_link.'/'.$offset.'/active');
		}
		
		if($action=='inactive')
		{	
			foreach($image_id as $id)
			{
			
				$this->db->where('image_id',$id);
				$this->db->update('images',array('image_active'=>0));
			
			}
			
			redirect('user_image/'.$red_link.'/'.$offset.'/inactive');
		}
		
		
		if($action=='show_home')
		{	
			foreach($image_id as $id)
			{
			
				$this->db->where('image_id',$id);
				$this->db->update('images',array('image_show_on_home'=>1));
			
			}
			
			redirect('user_image/'.$red_link.'/'.$offset.'/update');
		}
		
		if($action=='dont_show_home')
		{	
			foreach($image_id as $id)
			{
			
				$this->db->where('image_id',$id);
				$this->db->update('images',array('image_show_on_home'=>0));
			
			}
			
			redirect('user_image/'.$red_link.'/'.$offset.'/update');
		}
		
		
		
		
		
	}
	
	
	
	
	
	
}
?>