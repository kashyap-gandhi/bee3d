<?php
class Gallery extends CI_Controller {
	function Gallery()
	{
		parent::__construct();
		$this->load->model('gallery_model');
		$this->load->model('home_model');
		
		
	}
	
	function index()
	{
		
		redirect('gallery/list_gallery');
	}
	
	
	
	function list_gallery($offset=0,$msg='')
	{
		
		$data = array();
	     $theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
	
		//$check_rights= get_rights('list_category'); 
		
		//if(	$check_rights==0) {			
			//redirect('home/dashboard/no_rights');	
		//}
		
		$this->load->library('pagination');

		$limit = '10';
		
		$config['base_url'] = site_url('gallery/list_gallery/');
		$config['total_rows'] = $this->gallery_model->get_total_gallery_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['site_setting'] = site_setting();
		
		$data['result'] = $this->gallery_model->get_gallery_result($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
	
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
	    $this->template->write_view('center',$theme .'/layout/gallery/list_gallery',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
		
		
	}
	
	function add_gallery($offset=0,$limit=0)
	{
		
		
		 $data = array();
	     $theme = getThemeName();
		 $this->template->set_master_template($theme .'/template.php');

		
		
		$this->form_validation->set_rules('gallery_title', 'Gallery Name', 'required|alpha_numeric_space');
		$this->form_validation->set_rules('gallery_order', 'Gallery order', 'required|is_natural_no_zero');
		
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
		
			$data["gallery_id"] = $this->input->post('gallery_id ');
			$data["gallery_title"] = $this->input->post('gallery_title');
			$data["gallery_status"] = $this->input->post('gallery_status');
			$data["gallery_image"] = $this->input->post('gallery_image');
			$data["gallery_order"] = $this->input->post('gallery_order');

			$data['result'] = $this->gallery_model->get_gallery_result($offset, $limit);
			
			$data["active"] = $this->input->post('active');
			if($this->input->post('offset')=="")
			{
				$limit = '10';
				$totalRows = $this->gallery_model->get_total_gallery_count();
				$data["offset"] = (int)($totalRows/$limit)*$limit;
			}else{
				$data["offset"] = $this->input->post('offset');
			}
			
			
			$data['site_setting'] = site_setting();

			$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
	        $this->template->write_view('center',$theme .'/layout/gallery/add_gallery',$data,TRUE);
		    $this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		    $this->template->render();
			
		}
		else{
			if($this->input->post('gallery_id'))
			{
				$this->gallery_model->gallery_update();
				$msg = "update";
			}else{
				$this->gallery_model->gallery_insert();
				$msg = "insert";
			}
			$offset = $this->input->post('offset');
			redirect('gallery/list_gallery/'.$offset.'/'.$msg);
		}				
	}
	
	function edit_gallery($id=0,$offset=0,$limit=0)
	{
		  $data = array();
	     $theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');

		//$check_rights= get_rights('list_category');

		
		//if(	$check_rights==0) {			
		//redirect('home/dashboard/no_rights');	
		//}
		$one_gallery= $this->gallery_model->get_one_gallery($id);
		
			$data["error"] = "";
			$data['gallery_id']=$id;
			$data["gallery_title"] = $one_gallery['gallery_title'];
			$data["gallery_order"] = $one_gallery['gallery_order'];
			$data["gallery_status"] = $one_gallery['gallery_status'];
			$data["gallery_image"] = $one_gallery['gallery_image'];
			$data["offset"] = $offset;
			$data['all_gallery'] = $this->gallery_model->get_gallery_result($offset, $limit);
		    $data['site_setting'] = site_setting();
		    $this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
	        $this->template->write_view('center',$theme .'/layout/gallery/add_gallery',$data,TRUE);
		    $this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		    $this->template->render();
	}
	function delete_gallery($id=0,$offset=0)
	{
		
		$one_gallery= $this->gallery_model->get_one_gallery($id);
		
		$gallery_image = $one_gallery['gallery_image'];
	
	
		if($gallery_image!='')
		{
			if(file_exists(base_path().'upload/gallery/'.$gallery_image))
			{
				$link=base_path().'upload/gallery/'.$gallery_image;
				unlink($link);
			}
			if(file_exists(base_path().'upload/gallery_orig/'.$gallery_image))
			{
				$link2=base_path().'upload/gallery_orig/'.$gallery_image;
				unlink($link2);
			}
		}
		
	   $this->db->delete('gallery',array('gallery_id'=>$id));
		redirect('gallery/list_gallery/'.$offset.'/delete');
	}
	
}
?>