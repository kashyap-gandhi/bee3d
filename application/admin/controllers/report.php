<?php class Report extends CI_Controller 
{
	function Report()
	{
		parent::__construct();	
		$this->load->model('report_model');
		$this->load->model('user_model');
		$this->load->model('image_model');	
	}
	
	function index()
	{
		echo "<script>window.location.href='".site_url('report/report_image')."'</script>";
	}
	
	/********************report_comment**************************/	
	
	function report_comment($limit='20',$offset=0,$msg='')
	{
	    $theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');		
		
		$this->load->library('pagination');
        // $check_rights=get_rights('list_admin');
		
		//if(	$check_rights==0) {			
			//redirect('home/dashboard/no_rights');	
		//}
		//$limit = '10';
		$config['uri_segment']='4';
		$config['base_url'] = site_url('report/report_comment/'.$limit.'/');
		
		$config['total_rows'] = $this->report_model->get_total_comment();
	
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->report_model->get_list_comment_result($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		$data['site_setting'] = site_setting();

		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/report/list_comment',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	function comment_detail($id=0,$limit=20,$offset=0)
	{	    
	
		if($id=='' || $id==0)
		{
			redirect('report/report_comment');
		}
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['site_setting'] = site_setting();
		$data['report_id']=$id;
		$data['limit']=$limit;
		$data['offset']=$offset;
		
		if($_POST)
		{
			if($this->input->post('user_comment')!='')
			{			
				$resultupdate = $this->report_model->comment_detail_update();
			}
			
			redirect('report/report_comment/'.$limit.'/'.$offset.'/update');
			
		}
		
		
		$data['result'] = $this->report_model->get_one_comment($id);		
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/report/comment_detail',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	   
	}

	
	function action_report_comment($limit=20,$offset=0)
	{ 
		 
	
		$offset=$this->input->post('offset');
		$limit=$this->input->post('limit');
		$action=$this->input->post('action');
		$report_id=$this->input->post('chk');
		$search_type=$this->input->post('search_type');
		
		$option=$this->input->post('option');
		$keyword=$this->input->post('keyword');
		
		
		$red_link='report_comment/'.$limit;
		
		if($search_type=='search')
		{
			$red_link='search_report_comment/'.$limit.'/'.$option.'/'.$keyword;
		}
		
		
		if($action=='delete_report')
		{	
			foreach($report_id as $id)
			{
			
				$this->db->delete('block_report',array('report_id'=>$id));
			
			}
			
			redirect('report/'.$red_link.'/'.$offset.'/delete');
		}
		
		if($action=='delete_comment')
		{	
			foreach($report_id as $id)
			{
			
				$query=$this->db->get_where('block_report',array('report_id'=>$id));
				
				if($query->num_rows()>0)
				{
					$res=$query->row();
					
					if($res->comment_id>0)
					{
						
						/////////
						
						
						$check_image_comment=$this->db->get_where('image_comment',array('image_comment_id'=>$res->comment_id));
						if($check_image_comment->num_rows()>0)
						{								
							
							$check_comment_like=$this->db->get_where('image_comment_like',array('comment_id'=>$res->comment_id));
						
							if($check_comment_like->num_rows()>0)
							{
								$this->db->delete('image_comment_like',array('comment_id'=>$res->comment_id));	
							}
							
							$this->db->delete('image_comment',array('image_comment_id'=>$res->comment_id));
						}
						
						//////////				
						
					}
				}
				
				$this->db->delete('block_report',array('report_id'=>$id));
			
			}
			
			redirect('report/'.$red_link.'/'.$offset.'/delete');
		}
		
		
		
			
	}	
	
	/********************report_image**************************/
	
	function report_image($limit='20',$offset=0,$msg='')
	{   
	    $theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		
		$this->load->library('pagination');
        // $check_rights=get_rights('list_admin');
		
		//if(	$check_rights==0) {			
			//redirect('home/dashboard/no_rights');	
		//}
		
		$config['uri_segment']='4';	
		$config['base_url'] = site_url('report/report_image/'.$limit.'/');
		$config['total_rows'] = $this->report_model->get_total_image_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->report_model->get_list_image_result($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		$data['site_setting'] = site_setting();

		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/report/list_image',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	}
		
	function image_detail($id=0)
	{
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['site_setting'] = site_setting();	
		
		$data['result'] = $this->report_model->get_one_image($id);		
	
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/report/image_detail',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	function action_report_image($limit=20,$offset=0)
	{ 
	
		$offset=$this->input->post('offset');
		$limit=$this->input->post('limit');
		$action=$this->input->post('action');
		$report_id=$this->input->post('chk');
		$search_type=$this->input->post('search_type');
		
		$option=$this->input->post('option');
		$keyword=$this->input->post('keyword');
		
		
		$red_link='report_image/'.$limit;
		
		if($search_type=='search')
		{
			$red_link='search_report_image/'.$limit.'/'.$option.'/'.$keyword;
		}
		
		
		if($action=='delete_report')
		{	
			foreach($report_id as $id)
			{
			
				$this->db->delete('block_report',array('report_id'=>$id));
			
			}
			
			redirect('report/'.$red_link.'/'.$offset.'/delete');
		}
		
		if($action=='delete_image')
		{	
			foreach($report_id as $id)
			{
			
				$query=$this->db->get_where('block_report',array('report_id'=>$id));
				
				if($query->num_rows()>0)
				{
					$res=$query->row();
					
					if($res->image_id>0)
					{
						$this->image_model->delete_image($res->image_id);
					}
				}
				
				$this->db->delete('block_report',array('report_id'=>$id));
			
			}
			
			redirect('report/'.$red_link.'/'.$offset.'/delete');
		}
		
		
		
		
	}
	
	/***********************report user***********************/
	
	
	function report_user($limit='20',$offset=0,$msg='')
	{   
	    $theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		
		$this->load->library('pagination');
        // $check_rights=get_rights('list_admin');
		
		//if(	$check_rights==0) {			
			//redirect('home/dashboard/no_rights');	
		//}
		
		$config['uri_segment']='4';	
		$config['base_url'] = site_url('report/report_user/'.$limit.'/');
		$config['total_rows'] = $this->report_model->get_total_user_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->report_model->get_list_user_result($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		$data['site_setting'] = site_setting();

		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/report/list_user',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();	
	}
		
	function user_detail($id=0)
	{
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data['site_setting'] = site_setting();	
		
		$data['result'] = $this->report_model->get_one_user($id);		
	
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/report/user_detail',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	function action_report_user($limit=20,$offset=0)
	{ 
	
		$offset=$this->input->post('offset');
		$limit=$this->input->post('limit');
		$action=$this->input->post('action');
		$report_id=$this->input->post('chk');
		$search_type=$this->input->post('search_type');
		
		$option=$this->input->post('option');
		$keyword=$this->input->post('keyword');
		
		
		$red_link='report_user/'.$limit;
		
		if($search_type=='search')
		{
			$red_link='search_report_user/'.$limit.'/'.$option.'/'.$keyword;
		}
		
		
		if($action=='delete_report')
		{	
			foreach($report_id as $id)
			{		
				$this->db->delete('block_report',array('report_id'=>$id));			
			}			
			redirect('report/'.$red_link.'/'.$offset.'/delete');
		}
		
		if($action=='delete_user')
		{	
			foreach($report_id as $id)
			{			
				
				$query=$this->db->get_where('block_report',array('report_id'=>$id));
				
				if($query->num_rows()>0)
				{
					$res=$query->row();
					
					if($res->user_id>0)
					{
						$this->user_model->deactivateuser($res->user_id);	
					}
				}
				
						
				$this->db->delete('block_report',array('report_id'=>$id));			
			}			
			redirect('report/'.$red_link.'/'.$offset.'/delete');
		}
		
		
		
	}
	

}	
?>