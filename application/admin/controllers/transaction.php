<?php
class Transaction extends CI_Controller {
	function Transaction()
	{
		parent::__construct();	
		$this->load->model('transaction_model');
		//$this->load->model('worker_model');
		
	}
	
	function index()
	{
		redirect('transaction/list_escrow');
		
	}
	
		
	function transaction_detail($id=0)
	{
	    $theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
			
		$details = $this->package_model->get_one_transaction_detail($id);
		
		if(!$details)
		{
			redirect('package/list_earning');
		}
		 
		 $data['row']=$details;
		
		$data['site_setting'] = site_setting();
			
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/package/transaction_detail',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	
		
		
	/////////=========reports=========	
	
	function daily_report()
	{
		

		$check_rights=get_rights('list_daily_report');
		
		if($check_rights==0) {			
			/*echo "<script>window.location.href='".site_url('home/dashboard/no_rights')."'</script>";*/	
		}
		
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		//echo '<pre>'; print_r($_POST);
		
		
		if($_POST)
		{			
			$option=$this->input->post('option');
		} else {
			$option = 'five';
		}
		$data['option'] = $option;
		$data['result'] = $this->package_model->get_daily_result($option);
		$data['site_setting'] = site_setting();

		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/package/daily_report',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	function monthly_report()
	{
		

		$check_rights=get_rights('list_monthly_report');
		
		if($check_rights==0) {			
			/*echo "<script>window.location.href='".site_url('home/dashboard/no_rights')."'</script>";	*/
		}
		
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		
		if($_POST)
		{			
			$option=$this->input->post('option');
		} else {
			$option = 'current';
		}
		$data['option'] = $option;
		$data['result'] = $this->package_model->get_monthly_result($option);
		$data['site_setting'] = site_setting();

		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/package/monthly_report',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	function yearly_report()
	{
		
		$check_rights=get_rights('list_yearly_report');
		
		if($check_rights==0) {			
			/*echo "<script>window.location.href='".site_url('home/dashboard/no_rights')."'</script>";*/	
		}
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');


		if($_POST)
		{			
			$option=$this->input->post('option');
		} else {
			$option = 'current_year';
		}
		$data['option'] = $option;
		$data['result'] = $this->package_model->get_yearly_result($option);
		$data['site_setting'] = site_setting();

		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/package/yearly_report',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
		
		
	/////////=========earning=============	
		
		
	
	function list_earning($limit=20,$option='',$keyword='',$offset=0)
	{
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$check_rights=get_rights('list_transaction');
		
		if($check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		$this->load->library('pagination');
		
		if($_POST)
		{			
			$option=$this->input->post('option');
			$keyword=$this->input->post('keyword');
			$data['search_type']='search';
			
			$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
			$this->load->library('pagination');
			$config['uri_segment']='5';
			$config['base_url'] = site_url('package/list_earning/'.$limit.'/'.$option.'/'.$keyword.'/');
			$config['total_rows'] = $this->package_model->get_total_search_escrow_count($option,$keyword);
			$config['per_page'] = $limit;		
			$this->pagination->initialize($config);		
			$data['page_link'] = $this->pagination->create_links();
			
			$data['result'] = $this->package_model->get_search_escrow_result($option,$keyword,$offset,$limit);
			
			
			
		} else {
		
			if($option!='' && $keyword!='') {
			
				if($offset>0)
				{
					$config['uri_segment']='6';
				}
				else
				{
					$config['uri_segment']='5';
				}
				
			    $config['base_url'] = site_url('package/list_earning/'.$limit.'/'.$option.'/'.$keyword.'/');
				$config['total_rows'] = $this->package_model->get_total_search_escrow_count($option,$keyword);
				$data['result'] = $this->package_model->get_search_escrow_result($option,$keyword,$offset,$limit);
				
				$data['search_type']='search';
			 } else {

				$config['uri_segment']='4';
				$config['base_url'] = site_url('package/list_earning/'.$limit.'/');
				$config['total_rows'] = $this->package_model->get_total_transaction_count();
				$data['result'] = $this->package_model->get_transaction_result($offset, $limit);

				$data['search_type']='normal';
			}

			$config['per_page'] = $limit;		
			$this->pagination->initialize($config);		
			$data['page_link'] = $this->pagination->create_links();

			$option=$option;
			$keyword=$keyword;
			
		}
		
		$data['offset'] = $offset;
		$data['limit']= $limit;
		$data['option']=$option;
		$data['keyword']=$keyword;

		$data['site_setting'] = site_setting();

		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/package/list_earning',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
		
		
	
	function action_earning()
	{
		$offset=$this->input->post('offset');
		$limit=$this->input->post('limit');
		$action=$this->input->post('action');
		$user_package_id=$this->input->post('chk');
		$search_type=$this->input->post('search_type');
		
		$option=$this->input->post('option');
		$keyword=$this->input->post('keyword');
		
		$red_link='list_earning/'.$limit;
		if($search_type=='search')
		{
			$red_link='list_earning/'.$limit.'/'.$option.'/'.$keyword;
		}
		
		
		if($action=='delete')
		{	
			foreach($user_package_id as $id)
			{
			
				$this->db->delete('user_package',array('user_package_id'=>$id));
			
			}
			
			redirect('package/'.$red_link);
		}
	}
		
		
		
	function list_escrow($limit=20,$option='',$keyword='',$offset=0)
	{
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$check_rights=get_rights('list_transaction');
		
		if($check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		$this->load->library('pagination');
		
		if($_POST)
		{			
			$option=$this->input->post('option');
			$keyword=$this->input->post('keyword');
			$data['search_type']='search';
			
			$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
			
			$config['uri_segment']='5';
			$config['base_url'] = site_url('transaction/list_escrow/'.$limit.'/'.$option.'/'.$keyword.'/');
			$config['total_rows'] = $this->transaction_model->get_total_search_escrow_count($option,$keyword);
			$config['per_page'] = $limit;		
			$this->pagination->initialize($config);		
			$data['page_link'] = $this->pagination->create_links();
			
			$data['result'] = $this->transaction_model->get_search_escrow_result($option,$keyword,$offset,$limit);
			
			
			
		} else {

			if($option!='' && $keyword!='') {
			
				if($offset>0)
				{
					$config['uri_segment']='6';
				}
				else
				{
					$config['uri_segment']='5';
				}
				
			    $config['base_url'] = site_url('transaction/list_escrow/'.$limit.'/'.$option.'/'.$keyword.'/');
				$config['total_rows'] = $this->transaction_model->get_total_search_escrow_count($option,$keyword);
				$data['result'] = $this->transaction_model->get_search_escrow_result($option,$keyword,$offset,$limit);
				
				$data['search_type']='search';
			 } else {
			
				$config['uri_segment']='4';
				$config['base_url'] = site_url('transaction/list_escrow/'.$limit.'/');	
				$config['total_rows'] = $this->transaction_model->get_total_transaction_count();
				$data['result'] = $this->transaction_model->get_transaction_result($offset, $limit);

				$data['search_type']='normal';
			}
			
			
			$config['per_page'] = $limit;		
			$this->pagination->initialize($config);		
			$data['page_link'] = $this->pagination->create_links();

			$option=$option;
			$keyword=$keyword;
		
		}
		
		$data['offset'] = $offset;
		$data['limit']= $limit;
		$data['option']=$option;
		$data['keyword']=$keyword;
		
		$data['site_setting'] = site_setting();

		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/transaction/list_escrow',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	

	function list_refund($limit=20,$offset=0)
	{
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$check_rights=get_rights('list_transaction');
		
		if($check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		
		
		$this->load->library('pagination');

		$config['uri_segment']='4';
		$config['base_url'] = site_url('transaction/list_refund_transaction/'.$limit.'/');
		$config['total_rows'] = $this->transaction_model->get_total_refund_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->transaction_model->get_refund_result($offset, $limit);
		//echo '<pre>'; print_r($data['result']);


		$data['site_setting'] = site_setting();

		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/transaction/list_refund_transaction',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
		
	
	function list_paying($limit=20,$option='',$keyword='',$offset=0)
	{
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$check_rights=get_rights('list_transaction');
		
		if($check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
		$this->load->library('pagination');
		
		if($_POST)
		{			
			$option=$this->input->post('option');
			$keyword=$this->input->post('keyword');
			$data['search_type']='search';
			
			$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
			$this->load->library('pagination');
			$config['uri_segment']='5';
			$config['base_url'] = site_url('transaction/list_paying/'.$limit.'/'.$option.'/'.$keyword.'/');
			$config['total_rows'] = $this->transaction_model->get_total_search_paying_count($option,$keyword);
			$config['per_page'] = $limit;		
			$this->pagination->initialize($config);		
			$data['page_link'] = $this->pagination->create_links();
			
			$data['result'] = $this->transaction_model->get_search_paying_result($option,$keyword,$offset,$limit);
			
			
			
		} else {
		
			if($option!='' && $keyword!='') {
			
				if($offset>0)
				{
					$config['uri_segment']='6';
				}
				else
				{
					$config['uri_segment']='5';
				}
				
			    $config['base_url'] = site_url('transaction/list_paying/'.$limit.'/'.$option.'/'.$keyword.'/');
				$config['total_rows'] = $this->transaction_model->get_total_search_paying_count($option,$keyword);
				$data['result'] = $this->transaction_model->get_search_paying_result($option,$keyword,$offset,$limit);
				
				$data['search_type']='search';
			 } else {

				$config['uri_segment']='4';
				$config['base_url'] = site_url('transaction/list_paying/'.$limit.'/');
				$config['total_rows'] = $this->transaction_model->get_total_paying_count();
				$data['result'] = $this->transaction_model->get_paying_result($offset, $limit);

				$data['search_type']='normal';
			}

			$config['per_page'] = $limit;		
			$this->pagination->initialize($config);		
			$data['page_link'] = $this->pagination->create_links();

			$option=$option;
			$keyword=$keyword;
			
		}
		
		$data['offset'] = $offset;
		$data['limit']= $limit;
		$data['option']=$option;
		$data['keyword']=$keyword;

		$data['site_setting'] = site_setting();

		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/transaction/list_paying',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
}
?>