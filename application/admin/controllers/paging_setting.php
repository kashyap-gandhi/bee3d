<?php
class Paging_setting extends CI_Controller {
	function Paging_setting()
	{
		 parent::__construct();	
		$this->load->model('paging_setting_model');	
	}
	
	
	
	function index()
	{
		redirect('paging_setting/add_paging/');
	}
	
	
	
	function add_paging()
	{
		$data = array();
		//$check_rights=get_rights('add_paging');
		
		//if(	$check_rights==0) {			
		  //redirect('home/dashboard/no_rights');	
		//}
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		$this->load->library('form_validation');
		
		
		$this->form_validation->set_rules('album_like_limit','ALBUM LIKE LIMIT','required|numeric');
		$this->form_validation->set_rules('profile_album_limit','PROFILE ALBUM LIMIT','required|numeric');
		$this->form_validation->set_rules('profile_image_limit','PROFILE IMAGE LIMIT','required|numeric');
		$this->form_validation->set_rules('profile_like_limit','PROFILE LIKE LIMIT','required|numeric');
		$this->form_validation->set_rules('profile_activity_limit','PROFILE ACTIVITY LIMIT','required|numeric');
		$this->form_validation->set_rules('home_image_limit','HOME IMAGE LIMIT','required|numeric');
		$this->form_validation->set_rules('source_image_limit','SOURCE IMAGE LIMIT','required|numeric');
		$this->form_validation->set_rules('category_image_limit','CATEGORY IMAGE LIMIT','required|numeric');
		$this->form_validation->set_rules('popular_image_limit','POPULAR IMAGE LIMIT','required|numeric');
		$this->form_validation->set_rules('search_image_limit','SEARCH IMAGE LIMIT','required|numeric');
		

		$err = '';
		
		
		
		if($this->form_validation->run() == FALSE || $err!=''){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			if($err!=''){
				$data["error"] .= $err;
			}
			if($this->input->post('paging_setting_id'))
			{
				$data["paging_setting_id"] = $this->input->post('paging_setting_id');
				$data["album_like_limit"] = $this->input->post('album_like_limit');
				$data["profile_album_limit"] = $this->input->post('profile_album_limit');
				$data["profile_image_limit"] = $this->input->post('profile_image_limit');
				$data["profile_like_limit"] = $this->input->post('profile_like_limit');
				$data["profile_activity_limit"] = $this->input->post('profile_activity_limit');
				$data["home_image_limit"] = $this->input->post('home_image_limit');
				$data["source_image_limit"] = $this->input->post('source_image_limit');
				$data["category_image_limit"] = $this->input->post('category_image_limit');
				$data["popular_image_limit"] = $this->input->post('popular_image_limit');
				$data["search_image_limit"] = $this->input->post('search_image_limit');	
				
				
			}else{
				$one_page_setting = $this->paging_setting_model->get_one_paging_setting();
				
				$data["paging_setting_id"] = $one_page_setting->paging_setting_id;
				$data["album_like_limit"] = $one_page_setting->album_like_limit;
				$data["profile_album_limit"] = $one_page_setting->profile_album_limit;
				$data["profile_image_limit"] = $one_page_setting->profile_image_limit;
				$data["profile_like_limit"] = $one_page_setting->profile_like_limit;
				$data["profile_activity_limit"] = $one_page_setting->profile_activity_limit;
				$data["home_image_limit"] = $one_page_setting->home_image_limit;
				$data["source_image_limit"] = $one_page_setting->source_image_limit;
				$data["category_image_limit"] = $one_page_setting->category_image_limit;
				$data["popular_image_limit"] =$one_page_setting->popular_image_limit;
				$data["search_image_limit"] = $one_page_setting->search_image_limit;
				
			}
			//$data['site_setting'] = site_setting();

			$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
			$this->template->write_view('center',$theme .'/layout/paging_setting/add_paging',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();
		}else{
			$this->paging_setting_model->paging_setting_update();

			$data["error"] = "Paging updated successfully.";
				$data["paging_setting_id"] = $this->input->post('paging_setting_id');
				$data["album_like_limit"] = $this->input->post('album_like_limit');
				$data["profile_album_limit"] = $this->input->post('profile_album_limit');
				$data["profile_image_limit"] = $this->input->post('profile_image_limit');
				$data["profile_like_limit"] = $this->input->post('profile_like_limit');
				$data["profile_activity_limit"] = $this->input->post('profile_activity_limit');
				$data["home_image_limit"] = $this->input->post('home_image_limit');
				$data["source_image_limit"] = $this->input->post('source_image_limit');
				$data["category_image_limit"] = $this->input->post('category_image_limit');
				$data["popular_image_limit"] = $this->input->post('popular_image_limit');
				$data["search_image_limit"] = $this->input->post('search_image_limit');	
	
			//$data['site_setting'] = site_setting();	
			
			$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
			$this->template->write_view('center',$theme .'/layout/paging_setting/add_paging',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();
		}					
	}
}
?>