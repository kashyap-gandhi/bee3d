<?php


class Home extends CI_Controller {
	
	
	function Home()
	{
		parent::__construct();	
		$this->load->model('home_model');	
		$this->load->model('user_model');	
		$this->load->model('graph_model');		
	}
	
	
	public function index($msg = '')
	{
		if(check_admin_authentication())
		{
			redirect('home/dashboard');
		}
		
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$data=array();
		$data['msg'] = $msg; //login fail message

		$this->template->write_view('header',$theme .'/layout/common/header',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/common/login',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	//////////==========function check userername and password====
	function login()
	{	
		$login =$this->home_model->check_login();
		if($login == '1')
		{
			redirect("home/dashboard/valid");
		}else{
			redirect("home/index/invalid");
		}
	}
	
	
	
	function forgot_password()
	{
		
		
		
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
					
		
		
		
		
		
		
		
		}
		
		else
		{
		
			$chk_mail=$this->home_model->forgot_email();
			
			if($chk_mail==0)
			{
					
				$data['error']='email_not_found';
				
			
			
			}
			elseif($chk_mail==2)
			{
				$data['error']='record_not_found';	
				
			
			}
			else
			{
				$data['error']='success';	
				
			
			}
			
		
		}
		
		
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
	

		$this->template->write_view('header',$theme .'/layout/common/header',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/common/forgot_password',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
		
		
		
		
	}
	
	
	
	
	
	function dashboard($msg='')
    {
	
	
        $theme = getThemeName();
        $this->template->set_master_template($theme .'/template.php');

       $data = array();
       $data['msg'] = $msg; //login success message
       
       
	   $date=date('Y-m-d');
		
		$week_first_date= get_first_day_of_week($date);
		$week_last_date= get_last_day_of_week($date);	
		
		$data['week_first_date']=$week_first_date;
		$data['week_last_date']=$week_last_date;
		
		$data['weekly_registration']=$this->graph_model->get_weekly_registration($week_first_date,$week_last_date);		
		$data['weekly_fb_registration']=$this->graph_model->get_weekly_fb_registration($week_first_date,$week_last_date);
		$data['weekly_tw_registration']=$this->graph_model->get_weekly_tw_registration($week_first_date,$week_last_date);
		
		$data['weekly_total_travel_agent_registration']=''; //$this->graph_model->get_weekly_total_agent_registration($week_first_date,$week_last_date);
		$data['weekly_total_invite_agent_registration']=''; //$this->graph_model->get_weekly_total_invite_agent_registration($week_first_date,$week_last_date);
		
		$data['weekly_total_travel_agent_registration']=''; //$this->graph_model->get_weekly_total_agent_registration($week_first_date,$week_last_date);
		$data['weekly_total_invite_agent_registration']=''; //$this->graph_model->get_weekly_total_invite_agent_registration($week_first_date,$week_last_date);
		
		
		
		$data['weekly_trip']='';		
		
		///==trip posted date in this week
		$data['weekly_new_trip']=$this->graph_model->get_challenge_status(0,$week_first_date,$week_last_date);
		
		//====trip assign in this week check assign_date and assign_agent_id>0
		$data['weekly_assign_trip']=$this->graph_model->get_challenge_status(1,$week_first_date,$week_last_date);
		
		///====trip complete in this week check trip_activity_status = 2 or 3 and assign_agent_id>0
		$data['weekly_complete_trip']=$this->graph_model->get_challenge_status(2,$week_first_date,$week_last_date);
		
		///=======trip clsoe in this week check trip_activity_status =3 and assign_agent_id=0 or blank
		$data['weekly_close_trip']=$this->graph_model->get_challenge_status(3,$week_first_date,$week_last_date);
                
                
                $data['weekly_new_design']=$this->graph_model->get_weekly_design($week_first_date,$week_last_date);
                $data['weekly_new_store']=$this->graph_model->get_weekly_store($week_first_date,$week_last_date);
                $data['weekly_new_video']=$this->graph_model->get_weekly_video($week_first_date,$week_last_date);
                
	
		
	   
	  

       $this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
       $this->template->write_view('center',$theme .'/layout/common/dashboard',$data,TRUE);
       $this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
       $this->template->render();

}

	
	//function of logout
	function logout()
	{
		$this->session->sess_destroy();
		redirect("home/index/valid");
	}
	
	
	
}

?>