<?php
class User_album extends CI_Controller {
	function User_album()
	{
		 parent::__construct();	
		$this->load->model('user_album_model');
		$this->load->model('user_model');
		$this->load->model('album_model');
		
		
	}
	
	function index()
	{
		redirect('album/list_all_album');
	}
	
	
	
	
	
	/***all users***/
	
	function list_album($user_id,$limit=15,$offset = 0,$msg='')
	{
	  
		
		$check_user_profile=$this->user_album_model->check_user_profile_exists($user_id);
		
		$data["msg"] = $msg;
		 
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');

		
		if(!$check_user_profile) 
		{ 	
			redirect('user/list_active_user');
		
		} else {
			
			
		
			$user_profile=$this->user_album_model->get_user_profile($user_id);
			
			
			if(!$user_profile) 
			{ 	
				redirect('user/list_active_user');
					
			} else {
		
				
				    $data['tab_select']='board';
					$data['user_profile']=$user_profile;
					$data['profile_name']=$user_profile->profile_name;	
					$data['user_id']=$user_profile->user_id;

					$this->load->library('pagination');
	
					$config['uri_segment']='5';
					$config['base_url'] = site_url('user_album/list_album/'.$user_id.'/'.$limit.'/');
					$config['total_rows'] = $this->user_album_model->get_user_total_album($user_id);
					$config['per_page'] = $limit;		
					$this->pagination->initialize($config);		
					$data['page_link'] = $this->pagination->create_links();
					
					$data['user_album']=$this->user_album_model->get_user_album($user_id,$limit,$offset);
				
					
					$data['msg'] = $msg;
					$data['offset'] = $offset;
					$data['theme']=$theme;	
					
					$data['limit']=$limit;
					$data['option']='';
					$data['keyword']='';
					$data['search_type']='normal';
					
					$data['site_setting'] = site_setting();
				
				 	$this->template->write_view('center',$theme .'/layout/user_album/list_album',$data,TRUE);
					
			}
		
		}
		
		
		
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	
	}
	
	
	function search_all_user_album($user_id,$limit=15,$option='',$keyword='',$offset=0,$msg='')
	{
	
	    $check_user_profile=$this->user_album_model->check_user_profile_exists($user_id);
		
		$data["msg"] = $msg;
		 
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');

		
		if(!$check_user_profile) 
		{ 	
			redirect('user/list_active_user');
		
		} else {
			
			
		
			$user_profile=$this->user_album_model->get_user_profile($user_id);
			
			
			if(!$user_profile) 
			{ 	
				redirect('user/list_active_user');
					
			} else {
		 
				$theme = getThemeName();
				$this->template->set_master_template($theme .'/template.php');
				
				if($_POST)
				{		
					$option=$this->input->post('option');
					$keyword=$this->input->post('keyword');
				}
				else
				{
					$option=$option;
					$keyword=$keyword;			
				}
				
				
				$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
				
				
				$data['tab_select']='board';
				$data['user_profile']=$user_profile;
				$data['profile_name']=$user_profile->profile_name;	
				$data['user_id']=$user_profile->user_id;
					
					
				$this->load->library('pagination');
		
			
				$config['uri_segment']='7';
				$config['base_url'] = site_url('user_album/search_all_user_album/'.$user_id.'/'.$limit.'/'.$option.'/'.$keyword.'/');
				$config['total_rows'] = $this->user_album_model->get_total_all_search_album_count($user_id,$option,$keyword);
				$config['per_page'] = $limit;		
				$this->pagination->initialize($config);		
				$data['page_link'] = $this->pagination->create_links();
				
				$data['user_album'] = $this->user_album_model->get_all_search_album_result($user_id,$option,$keyword,$offset, $limit);
				$data['msg'] = $msg;
				$data['offset'] = $offset;
				$data['theme']=$theme;	
				
				
				$data['limit']=$limit;
				$data['option']=$option;
				$data['keyword']=$keyword;
				$data['search_type']='search';
				
				
				
				
				$data['site_setting'] = site_setting();
				
				$this->template->write_view('center',$theme .'/layout/user_album/list_album',$data,TRUE);
			}
		}
		
		
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	
	   
		
		
	  
	
	}
	
	
		function delete_album($user_id,$album_id,$limit=15,$offset= 0)
		{
			$get_album_details=get_album_by_id($album_id);
			
			
			
			$redirectlink = 'user_album/list_album/'.$user_id.'/'.$limit.'/'.$offset;
			
	
			if($get_album_details) 
			{ 				
				$this->album_model->delete_album($album_id);						
				redirect($redirectlink.'/delete');						
			} else {
				redirect($redirectlink.'/fail');
			}				
			
							
		}	
	
	
	//////////========================
	
	
	function list_album_image($album_id,$limit=15,$offset=0,$msg='')
	{
		
	    $theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$this->load->library('pagination');
	
		$config['uri_segment']='5';
		$config['base_url'] = site_url('user_album/list_album_image/'.$album_id.'/'.$limit.'/');
		$config['total_rows'] = $this->user_album_model->get_album_total_image($album_id);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['image_details'] = $this->user_album_model->get_album_images($album_id,$limit,$offset);
		
		
		$data["album_id"] = $album_id;
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		

		

		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/user_album/list_album_image',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	function search_all_album($album_id,$limit=15,$option='',$keyword='',$offset=0,$msg='')
	{

		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		if($_POST)
		{		
			$option=$this->input->post('option');
			$keyword=$this->input->post('keyword');
		}
		else
		{
			$option=$option;
			$keyword=$keyword;			
		}
		
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
		
		
		$this->load->library('pagination');
		
		$config['uri_segment']='7';
		$config['base_url'] = site_url('user_album/search_all_album/'.$album_id.'/'.$limit.'/'.$option.'/'.$keyword.'/');
		$config['total_rows'] = $this->user_album_model->get_total_search_image_count($album_id,$option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		
		
		
		$data['image_details'] = $this->user_album_model->get_all_search_image_result($album_id,$option,$keyword,$offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		$data['album_id']=$album_id;
		$data['limit']=$limit;
		$data['option']=$option;
		$data['keyword']=$keyword;
		$data['search_type']='search';
		
		
		
		$data['site_setting'] = site_setting();
	
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/user_album/list_album_image',$data,TRUE);	
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}
	
	
	//////////========================
	
	function edit_album($id=0,$offset=0)
	{
		
		  $data = array();
	      $theme = getThemeName();
		  $this->template->set_master_template($theme .'/template.php');

		
		$one_album = $this->user_album_model->get_one_album($id);
		$one_album_img=$this->user_album_model->get_user_album_img($id);
		$data['one_album'] = $one_album;
		
		
			$data["error"] = "";
			$data['album_id']=$id;
			$data["album_name"] = $one_album['album_name'];
			$data["album_description"] = $one_album['album_description'];
			$data["album_unique_code"] = $one_album['album_unique_code'];
			$data["album_privacy"] = $one_album['album_is_public'];
			$data["album_layout_id"] = $one_album['album_layout_id'];
			
			if($one_album_img) {
				//$data["album_image_name"]=$one_album_img['pin_image_name'];
				$data["album_image_id"]=$one_album_img['image_id'];
			} else {
				//$data["pin_image_name"]='';
				$data["album_image_id"]='';
			}
			
			
			$data["user_id"] = $one_album['user_id'];
				
			
			$data["offset"] = $offset;
			
		    $data['site_setting'] = site_setting();
		    $this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
			$this->template->write_view('center',$theme .'/layout/user_album/add_album',$data,TRUE);
		    $this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		    $this->template->render();
	}
	
	function add_album($id=0,$offset=0)
	{

		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		//$check_rights=get_rights('list_admin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
		
		
		
		
		$this->load->library('form_validation');
		
	
		$this->form_validation->set_rules('album_name', 'Album Name', 'required|alpha_alpha_numeric');
		$this->form_validation->set_rules('album_description', 'Album Description', 'required');
		//$this->form_validation->set_rules('board_order', 'Board Order', 'required');
	
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
		    $data["album_id"] = $this->input->post('album_id');
			$data["album_name"] = $this->input->post('album_name');
			$data["album_description"] = $this->input->post('album_description');
			$data["album_privacy"] = $this->input->post('album_privacy');
			$data["album_layout_id"] = $this->input->post('album_layout_id');
			$data["user_id"] = $this->input->post('user_id');
			
			
			
			$one_album = $this->user_album_model->get_one_album($this->input->post('album_id'));
			$one_album_img=$this->user_album_model->get_user_album_img($this->input->post('album_id'));
			
		
			$data['one_album'] = $one_album;
			
			$data["album_unique_code"] = $one_album['album_unique_code'];
			
			if($one_album_img) {
				//$data["pin_image_name"]=$one_board_img['pin_image_name'];
				$data["album_image_id"]=$one_album_img['image_id'];
			} else {
				//$data["pin_image_name"]='';
				$data["album_image_id"]='';
			}
			
			
				
			if($this->input->post('offset')=="")
			{
				$limit = '10';
				$config['total_rows'] = $this->user_album_model->get_user_total_album($board_id);
				$data["offset"] = (int)($totalRows/$limit)*$limit;
			}else{
				$data["offset"] = $this->input->post('offset');
			}
			
			
			$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
			$this->template->write_view('center',$theme .'/layout/user_album/add_album',$data,TRUE);
			$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			$this->template->render();
		}else{
				
			if($this->input->post('album_id')!='')
			{	
				$this->user_album_model->album_update();
				$msg = "update";
			}else{
				$this->user_board_model->album_insert();			
				$msg = "insert";
			}
			$album_id = $this->input->post('album_id');
			$offset = $this->input->post('offset');
			$limit = '10';

			redirect('album/list_all_album/15/'.$offset.'/'.$msg,'refresh');
			
		}				
			
	}
	
	
	
	
}
?>