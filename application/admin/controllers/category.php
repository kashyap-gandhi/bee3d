<?php

class Category extends CI_Controller {

    function Category() {
        parent::__construct();
        $this->load->model('category_model');
        $this->load->model('home_model');
    }

    function index() {

        redirect('category/list_design_category');
    }
    
    
      ///=====store category==========
    
    
    
    function list_store_category($offset=0, $msg='') {

        $data = array();
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');


        /*$check_rights = get_rights('list_category');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }*/

        $this->load->library('pagination');

        $limit = '10';

        $config['base_url'] = site_url('category/list_store_category/');
        $config['total_rows'] = $this->category_model->get_total_store_category_count();
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        $data['page_link'] = $this->pagination->create_links();

        $data['site_setting'] = site_setting();

        $data['result'] = $this->category_model->get_store_category_result($offset, $limit);
        $data['msg'] = $msg;
        $data['offset'] = $offset;

        $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
        $this->template->write_view('center', $theme . '/layout/category/list_store_category', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
        $this->template->render();
    }

    function add_store_category() {


        $data = array();
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        /*$check_rights = get_rights('list_category');

        if ($check_rights == 0) {
            echo "<script>parent.window.location.href='" . site_url('home/dashboard/no_rights') . "'</script>";
        }*/

        $this->form_validation->set_rules('category_name', 'Category Name', 'required|alpha_numeric_space_special_char');

        if ($this->form_validation->run() == FALSE) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }

            $data["category_id"] = $this->input->post('category_id ');
            $data["category_name"] = $this->input->post('category_name');
            $data["category_parent_id"] = $this->input->post('category_parent_id');
            $data["category_status"] = $this->input->post('category_status');
            $data["category_url_name"] = $this->input->post('category_url_name');
            $data["category_description"] = $this->input->post('category_description');
            $data["category_image"] = $this->input->post('category_image');

            //$data['all_category'] = $this->category_model->get_challenge_parent_category();


            $data["active"] = $this->input->post('active');
            if ($this->input->post('offset') == "") {
                $limit = '10';
                $totalRows = $this->category_model->get_total_store_category_count();
                $data["offset"] = (int) ($totalRows / $limit) * $limit;
            } else {
                $data["offset"] = $this->input->post('offset');
            }


            $data['site_setting'] = site_setting();

            $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
            $this->template->write_view('center', $theme . '/layout/category/add_store_category', $data, TRUE);
            $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
            $this->template->render();
        } else {
            if ($this->input->post('category_id')) {
                $this->category_model->store_category_update();
                $msg = "update";
            } else {
                $this->category_model->store_category_insert();
                $msg = "insert";
            }
            $offset = $this->input->post('offset');
            redirect('category/list_store_category/' . $offset . '/' . $msg);
        }
    }

    function edit_store_category($id=0, $offset=0) {
        $data = array();
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        /*$check_rights = get_rights('list_category');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }*/
        
        $one_category = $this->category_model->get_one_store_category($id);
        
        if(!$one_category){
            
            redirect('category/list_store_category/' . $offset . '/notfound');
        }

        $data["error"] = "";
        $data['category_id'] = $id;
        $data["category_name"] = $one_category['category_name'];
        $data["category_parent_id"] = $one_category['category_parent_id'];
        $data["category_status"] = $one_category['category_status'];
        $data["category_url_name"] = $one_category['category_url_name'];
        $data["category_description"] = $one_category['category_description'];
        $data["category_image"] = $one_category['category_image'];
        $data["offset"] = $offset;
        //$data['all_category'] = $this->category_model->get_challenge_parent_category();
        $data['site_setting'] = site_setting();
        $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
        $this->template->write_view('center', $theme . '/layout/category/add_store_category', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
        $this->template->render();
    }

    function delete_store_category($id=0, $offset=0) {
        
        $one_category = $this->category_model->get_one_store_category($id);
        
        if(!$one_category){
            
            redirect('category/list_store_category/' . $offset . '/notfound');
        }
        
        $cat_image=$one_category['category_image'];
         if($cat_image!=''){
            if (file_exists(base_path() . 'upload/category/' . $cat_image)) {
                $link = base_path() . 'upload/category/' . $cat_image;
                unlink($link);
            }
            if (file_exists(base_path() . 'upload/category_orig/' . $cat_image)) {
                $link2 = base_path() . 'upload/category_orig/' . $cat_image;
                unlink($link2);
            }
         }
        
        $this->db->delete('store_category', array('category_id' => $id));
        redirect('category/list_store_category/' . $offset . '/delete');
    }
    
    
    
    
    
    
    ///=====challenge category==========
    
    
    
    function list_challenge_category($offset=0, $msg='') {

        $data = array();
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');


        /*$check_rights = get_rights('list_category');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }*/

        $this->load->library('pagination');

        $limit = '10';

        $config['base_url'] = site_url('category/list_challenge_category/');
        $config['total_rows'] = $this->category_model->get_total_challenge_category_count();
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        $data['page_link'] = $this->pagination->create_links();

        $data['site_setting'] = site_setting();

        $data['result'] = $this->category_model->get_challenge_category_result($offset, $limit);
        $data['msg'] = $msg;
        $data['offset'] = $offset;

        $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
        $this->template->write_view('center', $theme . '/layout/category/list_challenge_category', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
        $this->template->render();
    }

    function add_challenge_category() {


        $data = array();
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        /*$check_rights = get_rights('list_category');

        if ($check_rights == 0) {
            echo "<script>parent.window.location.href='" . site_url('home/dashboard/no_rights') . "'</script>";
        }*/

        $this->form_validation->set_rules('category_name', 'Category Name', 'required|alpha_numeric_space_special_char');

        if ($this->form_validation->run() == FALSE) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }

            $data["category_id"] = $this->input->post('category_id ');
            $data["category_name"] = $this->input->post('category_name');
            $data["category_parent_id"] = $this->input->post('category_parent_id');
            $data["category_status"] = $this->input->post('category_status');
            $data["category_url_name"] = $this->input->post('category_url_name');
            $data["category_description"] = $this->input->post('category_description');
            $data["category_image"] = $this->input->post('category_image');

            //$data['all_category'] = $this->category_model->get_challenge_parent_category();


            $data["active"] = $this->input->post('active');
            if ($this->input->post('offset') == "") {
                $limit = '10';
                $totalRows = $this->category_model->get_total_challenge_category_count();
                $data["offset"] = (int) ($totalRows / $limit) * $limit;
            } else {
                $data["offset"] = $this->input->post('offset');
            }


            $data['site_setting'] = site_setting();

            $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
            $this->template->write_view('center', $theme . '/layout/category/add_challenge_category', $data, TRUE);
            $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
            $this->template->render();
        } else {
            if ($this->input->post('category_id')) {
                $this->category_model->challenge_category_update();
                $msg = "update";
            } else {
                $this->category_model->challenge_category_insert();
                $msg = "insert";
            }
            $offset = $this->input->post('offset');
            redirect('category/list_challenge_category/' . $offset . '/' . $msg);
        }
    }

    function edit_challenge_category($id=0, $offset=0) {
        $data = array();
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        /*$check_rights = get_rights('list_category');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }*/
        
        $one_category = $this->category_model->get_one_challenge_category($id);
        
        if(!$one_category){
            
            redirect('category/list_challenge_category/' . $offset . '/notfound');
        }

        $data["error"] = "";
        $data['category_id'] = $id;
        $data["category_name"] = $one_category['category_name'];
        $data["category_parent_id"] = $one_category['category_parent_id'];
        $data["category_status"] = $one_category['category_status'];
        $data["category_url_name"] = $one_category['category_url_name'];
        $data["category_description"] = $one_category['category_description'];
        $data["category_image"] = $one_category['category_image'];
        $data["offset"] = $offset;
        //$data['all_category'] = $this->category_model->get_challenge_parent_category();
        $data['site_setting'] = site_setting();
        $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
        $this->template->write_view('center', $theme . '/layout/category/add_challenge_category', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
        $this->template->render();
    }

    function delete_challenge_category($id=0, $offset=0) {
        
        $one_category = $this->category_model->get_one_challenge_category($id);
        
        if(!$one_category){
            
            redirect('category/list_challenge_category/' . $offset . '/notfound');
        }
        
        $cat_image=$one_category['category_image'];
         if($cat_image!=''){
            if (file_exists(base_path() . 'upload/category/' . $cat_image)) {
                $link = base_path() . 'upload/category/' . $cat_image;
                unlink($link);
            }
            if (file_exists(base_path() . 'upload/category_orig/' . $cat_image)) {
                $link2 = base_path() . 'upload/category_orig/' . $cat_image;
                unlink($link2);
            }
         }
        
        $this->db->delete('challenge_category', array('category_id' => $id));
        redirect('category/list_challenge_category/' . $offset . '/delete');
    }
    
    
    
    
    ///=====video category==========
    
    
    function list_video_category($offset=0, $msg='') {

        $data = array();
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');


        /*$check_rights = get_rights('list_category');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }*/

        $this->load->library('pagination');

        $limit = '10';

        $config['base_url'] = site_url('category/list_video_category/');
        $config['total_rows'] = $this->category_model->get_total_video_category_count();
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        $data['page_link'] = $this->pagination->create_links();

        $data['site_setting'] = site_setting();

        $data['result'] = $this->category_model->get_video_category_result($offset, $limit);
        $data['msg'] = $msg;
        $data['offset'] = $offset;

        $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
        $this->template->write_view('center', $theme . '/layout/category/list_video_category', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
        $this->template->render();
    }

    
    
    function pending_video_category($offset=0, $msg='') {

        $data = array();
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');


        /*$check_rights = get_rights('list_category');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }*/

        $this->load->library('pagination');

        $limit = '10';

        $config['base_url'] = site_url('category/pending_video_category/');
        $config['total_rows'] = $this->category_model->get_total_pending_video_category_count();
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        $data['page_link'] = $this->pagination->create_links();

        $data['site_setting'] = site_setting();

        $data['result'] = $this->category_model->get_pending_video_category_result($offset, $limit);
        $data['msg'] = $msg;
        $data['offset'] = $offset;

        $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
        $this->template->write_view('center', $theme . '/layout/category/pending_video_category', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
        $this->template->render();
    }

    
    function add_video_category() {


        $data = array();
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        /*$check_rights = get_rights('list_category');

        if ($check_rights == 0) {
            echo "<script>parent.window.location.href='" . site_url('home/dashboard/no_rights') . "'</script>";
        }*/
        
        
         if($_POST){
            $data['all_category'] = $this->listing_video_dropdown_category($this->input->post('category_parent_id'),0);
        } else {
            $data['all_category'] = $this->listing_video_dropdown_category(0,0);
        }

        $this->form_validation->set_rules('category_name', 'Category Name', 'required|alpha_numeric_space_special_char');

        if ($this->form_validation->run() == FALSE) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }

            $data["category_id"] = $this->input->post('category_id ');
            $data["category_name"] = $this->input->post('category_name');
            $data["category_parent_id"] = $this->input->post('category_parent_id');
            $data["category_status"] = $this->input->post('category_status');
            $data["category_url_name"] = $this->input->post('category_url_name');
            $data["category_description"] = $this->input->post('category_description');
            $data["category_image"] = $this->input->post('category_image');

            //$data['all_category'] = $this->category_model->get_video_parent_category();


            $data["active"] = $this->input->post('active');
            if ($this->input->post('offset') == "") {
                $limit = '10';
                $totalRows = $this->category_model->get_total_video_category_count();
                $data["offset"] = (int) ($totalRows / $limit) * $limit;
            } else {
                $data["offset"] = $this->input->post('offset');
            }


            $data['site_setting'] = site_setting();

            $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
            $this->template->write_view('center', $theme . '/layout/category/add_video_category', $data, TRUE);
            $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
            $this->template->render();
        } else {
            if ($this->input->post('category_id')) {
                $this->category_model->video_category_update();
                $msg = "update";
            } else {
                $this->category_model->video_category_insert();
                $msg = "insert";
            }
            $offset = $this->input->post('offset');
            redirect('category/list_video_category/' . $offset . '/' . $msg);
        }
    }

    function edit_video_category($id=0, $offset=0) {
        $data = array();
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        /*$check_rights = get_rights('list_category');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }*/
        
        $one_category = $this->category_model->get_one_video_category($id);
        
        if(!$one_category){
            
            redirect('category/list_video_category/' . $offset . '/notfound');
        }

        $data["error"] = "";
        $data['category_id'] = $id;
        $data["category_name"] = $one_category['category_name'];
        $data["category_parent_id"] = $one_category['category_parent_id'];
        $data["category_status"] = $one_category['category_status'];
        $data["category_url_name"] = $one_category['category_url_name'];
        $data["category_description"] = $one_category['category_description'];
        $data["category_image"] = $one_category['category_image'];
        $data["offset"] = $offset;
        //$data['all_category'] = $this->category_model->get_video_parent_category();
        $data['all_category'] = $this->listing_video_dropdown_category($one_category['category_parent_id'],0);
        
        $data['site_setting'] = site_setting();
        $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
        $this->template->write_view('center', $theme . '/layout/category/add_video_category', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
        $this->template->render();
    }
    
    
    
    
	function listing_video_dropdown_category($select,$id='',$level=0)
	{
			
		$this->cnt++;
			
			
		$cat='';
		$id= empty($id) ? 0 : $id;
		
		$check_cate=$this->category_model->get_video_category_multi($id);
		if($check_cate) {
                   
				foreach($check_cate as $key=>$res) {			
					
					if($select==$res->category_id) {
					
					$cat.='<option value="'.$res->category_id.'" selected="selected">';
					
					}  else {
					
					$cat.='<option value="'.$res->category_id.'">';
					
					}
					
					
					
					
						if($res->category_parent_id!=0) {
					
							for ($i = 0; $i < $level; $i++) {
                                                            $cat.="&nbsp;&nbsp;&nbsp;&nbsp;"; 
                                                            
                                                          }				
							
								$cat.='|_';
						}
						
						$cat.=$res->category_name.'</option>';	
					
								
					$check_sub_cate=$this->category_model->get_video_category_multi($res->category_id);
					if($check_sub_cate) {	
					
						$cat.=$this->listing_video_dropdown_category($select,$res->category_id,$level+1);
						$this->cnt--;	
					}
					
					$this->cnt--;			
				
			   } 
			   		  
		}
                
                
			
		return $cat;  
	
	}
	
	

    function delete_video_category($from='list',$id=0, $offset=0) {
        
        if($from!='list' || $from!='pending'){
            $from='list';
        }
        
        $one_category = $this->category_model->get_one_video_category($id);
        
        if(!$one_category){
            
            redirect('category/'.$from.'_video_category/' . $offset . '/notfound');
        }
        
        $cat_image=$one_category['category_image'];
         if($cat_image!=''){
            if (file_exists(base_path() . 'upload/category/' . $cat_image)) {
                $link = base_path() . 'upload/category/' . $cat_image;
                unlink($link);
            }
            if (file_exists(base_path() . 'upload/category_orig/' . $cat_image)) {
                $link2 = base_path() . 'upload/category_orig/' . $cat_image;
                unlink($link2);
            }
         }
        
        $this->db->delete('video_category', array('category_id' => $id));
        redirect('category/'.$from.'_video_category/' . $offset . '/delete');
    }
    
    ///=====design category==========
    
    function list_design_category($offset=0, $msg='') {

        $data = array();
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');


        /*$check_rights = get_rights('list_category');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }*/

        $this->load->library('pagination');

        $limit = '10';

        $config['base_url'] = site_url('category/list_design_category/');
        $config['total_rows'] = $this->category_model->get_total_design_category_count();
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        $data['page_link'] = $this->pagination->create_links();

        $data['site_setting'] = site_setting();

        $data['result'] = $this->category_model->get_design_category_result($offset, $limit);
        $data['msg'] = $msg;
        $data['offset'] = $offset;

        $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
        $this->template->write_view('center', $theme . '/layout/category/list_design_category', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
        $this->template->render();
    }

    function add_design_category() {


        $data = array();
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        /*$check_rights = get_rights('list_category');

        if ($check_rights == 0) {
            echo "<script>parent.window.location.href='" . site_url('home/dashboard/no_rights') . "'</script>";
        }*/
        
        
        if($_POST){
            $data['all_category'] = $this->listing_design_dropdown_category($this->input->post('category_parent_id'),0);
        } else {
            $data['all_category'] = $this->listing_design_dropdown_category(0,0);
        }
        
       
        
        
        $this->form_validation->set_rules('category_name', 'Category Name', 'required|alpha_numeric_space_special_char');

        if ($this->form_validation->run() == FALSE) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }

            $data["category_id"] = $this->input->post('category_id ');
            $data["category_name"] = $this->input->post('category_name');
            $data["category_parent_id"] = $this->input->post('category_parent_id');
            $data["category_status"] = $this->input->post('category_status');
            $data["category_url_name"] = $this->input->post('category_url_name');
            $data["category_description"] = $this->input->post('category_description');
            $data["category_image"] = $this->input->post('category_image');

            


            $data["active"] = $this->input->post('active');
            if ($this->input->post('offset') == "") {
                $limit = '10';
                $totalRows = $this->category_model->get_total_design_category_count();
                $data["offset"] = (int) ($totalRows / $limit) * $limit;
            } else {
                $data["offset"] = $this->input->post('offset');
            }


            $data['site_setting'] = site_setting();

            $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
            $this->template->write_view('center', $theme . '/layout/category/add_design_category', $data, TRUE);
            $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
            $this->template->render();
        } else {
            if ($this->input->post('category_id')) {
                $this->category_model->design_category_update();
                $msg = "update";
            } else {
                $this->category_model->design_category_insert();
                $msg = "insert";
            }
            $offset = $this->input->post('offset');
            redirect('category/list_design_category/' . $offset . '/' . $msg);
        }
    }

    function edit_design_category($id=0, $offset=0) {
        $data = array();
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');

        /*$check_rights = get_rights('list_category');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }*/
        
        $one_category = $this->category_model->get_one_design_category($id);
        
        if(!$one_category){
            
            redirect('category/list_design_category/' . $offset . '/notfound');
        }

        $data["error"] = "";
        $data['category_id'] = $id;
        $data["category_name"] = $one_category['category_name'];
        $data["category_parent_id"] = $one_category['category_parent_id'];
        $data["category_status"] = $one_category['category_status'];
        $data["category_url_name"] = $one_category['category_url_name'];
        $data["category_description"] = $one_category['category_description'];
        $data["category_image"] = $one_category['category_image'];
        $data["offset"] = $offset;
        $data['all_category'] = $this->listing_design_dropdown_category($one_category['category_parent_id'],0);
        $data['site_setting'] = site_setting();
        $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
        $this->template->write_view('center', $theme . '/layout/category/add_design_category', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
        $this->template->render();
    }
    
    
    
    
    
	function listing_design_dropdown_category($select,$id='',$level=0)
	{
			
		$this->cnt++;
			
			
		$cat='';
		$id= empty($id) ? 0 : $id;
		
		$check_cate=$this->category_model->get_design_category_multi($id);
		if($check_cate) {
                   
				foreach($check_cate as $key=>$res) {			
					
					if($select==$res->category_id) {
					
					$cat.='<option value="'.$res->category_id.'" selected="selected">';
					
					}  else {
					
					$cat.='<option value="'.$res->category_id.'">';
					
					}
					
					
					
					
						if($res->category_parent_id!=0) {
					
							//for ($i = 0; $i < $this->cnt; $i++) {
                                                         for ($i = 0; $i < $level; $i++) {
                                                            $cat.="&nbsp;&nbsp;&nbsp;&nbsp;"; 
                                                            
                                                            }				
							
								$cat.='|_';
						}
						
						$cat.=$res->category_name.'</option>';	
					
								
					$check_sub_cate=$this->category_model->get_design_category_multi($res->category_id);
					if($check_sub_cate) {	
					
						$cat.=$this->listing_design_dropdown_category($select,$res->category_id,$level+1);
						$this->cnt--;	
					}
					
					$this->cnt--;			
				
			   } 
			   		  
		}
                
               
			
		return $cat;  
	
	}
	
	
	

    function delete_design_category($from='list',$id=0, $offset=0) {
        
        if($from!='list' || $from!='pending'){
            $from='list';
        }
        
        $one_category = $this->category_model->get_one_design_category($id);
        
        if(!$one_category){
            
            redirect('category/'.$from.'_design_category/' . $offset . '/notfound');
        }
        
        $cat_image=$one_category['category_image'];
        
        if($cat_image!=''){
            if (file_exists(base_path() . 'upload/category/' . $cat_image)) {
                $link = base_path() . 'upload/category/' . $cat_image;
                unlink($link);
            }
            if (file_exists(base_path() . 'upload/category_orig/' . $cat_image)) {
                $link2 = base_path() . 'upload/category_orig/' . $cat_image;
                unlink($link2);
            }
        }
        
        
        
        $this->db->delete('design_category', array('category_id' => $id));
        redirect('category/'.$from.'_design_category/' . $offset . '/delete');
    }
    
    
    
       function pending_design_category($offset=0, $msg='') {

        $data = array();
        $theme = getThemeName();
        $this->template->set_master_template($theme . '/template.php');


        /*$check_rights = get_rights('list_category');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }*/

        $this->load->library('pagination');

        $limit = '10';

        $config['base_url'] = site_url('category/pending_design_category/');
        $config['total_rows'] = $this->category_model->get_total_pending_design_category_count();
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        $data['page_link'] = $this->pagination->create_links();

        $data['site_setting'] = site_setting();

        $data['result'] = $this->category_model->get_pending_design_category_result($offset, $limit);
        $data['msg'] = $msg;
        $data['offset'] = $offset;

        $this->template->write_view('header_menu', $theme . '/layout/common/header_menu', $data, TRUE);
        $this->template->write_view('center', $theme . '/layout/category/pending_design_category', $data, TRUE);
        $this->template->write_view('footer', $theme . '/layout/common/footer', $data, TRUE);
        $this->template->render();
    }

}

?>