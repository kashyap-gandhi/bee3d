<?php
class Agent extends  CI_Controller {

	function Agent()
	{
		 parent::__construct();	
		$this->load->model('agent_model');
		$this->load->model('user_model');
			
	}
	
	
	function index()
	{
		redirect('agent/list_agent');	
	}

	
	
	function list_report($limit=15,$offset=0,$msg='')
	{
		$check_rights=get_rights('list_report');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='4';
		$config['base_url'] = site_url('agent/list_report/'.$limit.'/');
		$config['total_rows'] = $this->agent_model->get_total_report_agent_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->agent_model->get_all_report_agent_result($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/list_report',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}
	
	
	function search_list_report($limit=15,$option='',$keyword='',$offset=0,$msg='')
	{
		$check_rights=get_rights('list_report');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
	
	
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		if($_POST)
		{		
			$option=$this->input->post('option');
			$keyword=$this->input->post('keyword');
		}
		else
		{
			$option=$option;
			$keyword=$keyword;			
		}
		
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='6';
		$config['base_url'] = site_url('agent/search_list_report/'.$limit.'/'.$option.'/'.$keyword.'/');
		$config['total_rows'] = $this->agent_model->get_total_search_report_agent_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->agent_model->get_all_search_report_agent_result($option,$keyword,$offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']=$option;
		$data['keyword']=$keyword;
		$data['search_type']='search';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/list_report',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}
	
	
	
	function action_report()
	{
		
		 $offset=$this->input->post('offset');
		 $limit=$this->input->post('limit');
		 $action=$this->input->post('action');
		 $admin_id=$this->input->post('chk');
		 $search_type=$this->input->post('search_type');
		
		 $option=$this->input->post('option');
		 $keyword=$this->input->post('keyword');
		
		$red_link='list_report/'.$limit;
		
		if($search_type=='search')
		{
			$red_link='search_list_report/'.$limit.'/'.$option.'/'.$keyword;
		}
		
		
		
		
		if($action=='reset')
		{	
			foreach($admin_id as $id)
			{
			
				
				$check_report=$this->db->get_where('entry_report_current',array('admin_id'=>$id));
				
				if($check_report->num_rows()>0)
				{
					$userdata = array(
						'total_add' => 0,
						'total_approve' => 0,
						'total_reject' => 0
					);		
					$this->db->where('admin_id',$id);
					$this->db->update('entry_report_current',$userdata);				
				}		
			
			}
			
			redirect('agent/'.$red_link.'/'.$offset.'/reset');
		}
		
		
		
	
		
	}
	
	
	
		
	/*** check the user unique email address
	*	var string $email
	*  return boolen
	**/
		
	function email_check($email)
	{
		$cemail = $this->user_model->emailTaken($email);
		
		if($cemail)
		{
			$this->form_validation->set_message('email_check', 'There is an existing account associated with this email');
			return FALSE;
		}
		else
		{
				return TRUE;
		}
	}	
	
		
	function add_agent()
	{
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$site_setting = site_setting();
		
		$data['member_association'] = member_association();
		
		
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|alpha');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|alpha');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');		
	
		
		
		$this->form_validation->set_rules('agency_name', 'Agency Name', 'trim|required|alpha_dot_space');
		$this->form_validation->set_rules('contact_person_name', 'Contact Person Name', 'trim|required|alpha_space');
		
		
		$this->form_validation->set_rules('agency_contact_email', 'Contact Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('agency_phone', 'Agency Phone', 'trim|required|phone_valid|min_length[10]|max_length[18]');
		
		$this->form_validation->set_rules('agency_customer_support_no', 'Agency Customer Support No.', 'trim|phone_valid|min_length[10]|max_length[18]');
		$this->form_validation->set_rules('agency_website', 'Agency Website', 'trim|valid_url');
		
				
		$this->form_validation->set_rules('agency_location', 'Agency Location ', 'trim|required');
		
			//Super Admin portion
		if($this->session->userdata('admintype') == 1){
			$this->form_validation->set_rules('agent_app_approved', 'Agent Application Approve', 'trim|required');
			$this->form_validation->set_rules('agent_background_approved', 'Agent Background Approve', 'trim|required');
			$this->form_validation->set_rules('agent_phone_approved', 'Agent Phone Approve', 'trim|required');
			$this->form_validation->set_rules('agent_level', 'Agent Level', 'trim|numeric');
			}
		
		
		
		
		if($this->form_validation->run() == FALSE)
		{
			if(validation_errors())
			{													
				$data["error"] = validation_errors();
			}
			else
			{		
				$data["error"] = "";							
			}
				
				
				
			$data['first_name']=$this->input->post('first_name');
			$data['last_name']=$this->input->post('last_name');
			$data['email']=$this->input->post('email');
			
			
			
			$data['user_status']=$this->input->post('user_status');
			
		
			$data['about_user']=$this->input->post('about_user');	
			
			
			$data['agency_name']=$this->input->post('agency_name');
			$data['contact_person_name']=$this->input->post('contact_person_name');
			
			$data['agency_contact_email']=$this->input->post('agency_contact_email');
			$data['agency_phone']=$this->input->post('agency_phone');
			$data['agency_customer_support_no']=$this->input->post('agency_customer_support_no');
			
			$data['agency_website']=$this->input->post('agency_website');
			$data['agency_location']=$this->input->post('agency_location');
			
			$data['agent_member_of_association']=$this->input->post('agent_member_of_association');
			
			
			$data['agent_app_approved']=$this->input->post('agent_app_approved');
			$data['agent_background_approved']=$this->input->post('agent_background_approved');
			$data['agent_phone_approved']=$this->input->post('agent_phone_approved');
			$data['super_admin_approve']=$this->input->post('super_admin_approve');
			
			
			$data['agent_level']=$this->input->post('agent_level');
					
					
					
					
		} else	{
				
			
					$insert_return_paasword=$this->agent_model->add_agent();
			
			
					//Super Admin portion
			if($this->session->userdata('admintype') == 1 && $this->input->post('agent_app_approved') == 1)
			{
                   
				   
				   	$email=$this->input->post('email');
					$get_user_info=$this->db->get_where('user',array('email'=>$email));
					$user_info=$get_user_info->row();
					
					
					$username =$user_info->full_name;						
					$password = $insert_return_paasword;
					
					$email = $this->input->post('email');	
					
							
					
					$email_verification_code=$user_info->email_verification_code;
					$user_id=$user_info->user_id;
					
					$agent_activation_link=front_base_url().'agentactivate/'.$user_id.'/'.$email_verification_code;
					$agent_preview_profile_link=front_base_url().'previewprofile/'.$user_id.'/'.$email_verification_code;
					
					
				
				/////////////============welcome email===========	

				
				$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='AgentJoin'");
				$email_temp=$email_template->row();
				
				$email_from_name=$email_temp->from_name;
				
				$email_address_from=$email_temp->from_address;
				$email_address_reply=$email_temp->reply_address;
				
				$email_subject=$email_temp->subject;				
				$email_message=$email_temp->message;			
				
				$email_to=$email;		
				
			
				
				$email_message=str_replace('{break}','<br/>',$email_message);
				
				$email_message=str_replace('{user_name}',$username,$email_message);			
				$email_message=str_replace('{email}',$email,$email_message);
				$email_message=str_replace('{password}',$password,$email_message);
				
				$email_message=str_replace('{agent_activation_link}',$agent_activation_link,$email_message);
				
				
				$email_temp_url=upload_url().'email/';
				$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
				
				$str=$email_message;		
				
					
				/** custom_helper email function **/
					
				email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
				
				
				
				/////////////============new user email===========	


				
				$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='AgentProfilePreview'");
				$email_temp=$email_template->row();
				
				
				$email_from_name=$email_temp->from_name;
				
				$email_address_from=$email_temp->from_address;
				$email_address_reply=$email_temp->reply_address;
				
				$email_subject=$email_temp->subject;				
				$email_message=$email_temp->message;
				
				
				
				$email_to=$email;
				
				
			
				$login_link=front_base_url().'sign_up';
				
				$email_message=str_replace('{break}','<br/>',$email_message);
				
				$email_message=str_replace('{user_name}',$username,$email_message);
				
				$email_message=str_replace('{agent_activation_link}',$agent_activation_link,$email_message);
				$email_message=str_replace('{agent_preview_profile_link}',$agent_preview_profile_link,$email_message);
				
				
				$email_temp_url=upload_url().'email/';
				$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
				
				
				
				$str=$email_message;
			
				
				/** custom_helper email function **/
				
				email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);

				

				/////////////============new user email===========	
			
			
				} ///Only super admin can mail send
				
				
				
			
			
			
			
			redirect('agent/list_all_agent/15/0/insert/');
			
			
			/////===send email and make random password  and auto active worker, user, app approved 
		
			///=====email to user login detail and if worker then worker edit link
				
				
				
				
		}		
		
	
			
			
		$data['theme']=$theme;
		$data['site_setting'] =	$site_setting;	
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/add_agent',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
		
		
		
	}
	
	
	function edit_agent($user_id=0)
	{
	
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$site_setting = site_setting();
		
		if($user_id>0)
		{
			
			$agent_detail=$this->agent_model->get_agent_by_id($user_id);
						
			if(!$agent_detail) { redirect('agent/list_active_agent/'); } 
			
			
			
			
		} else {   redirect('agent/list_active_agent/');  }
		
		
		
		$data['user_id']=$user_id;
		
		$data['member_association'] = member_association();
		$data['agent_document'] = get_agent_document($user_id);	
		
		
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|alpha');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|alpha');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');		
	
		
		
		$this->form_validation->set_rules('agency_name', 'Agency Name', 'trim|required|alpha_dot_space');
		$this->form_validation->set_rules('contact_person_name', 'Contact Person Name', 'trim|required|alpha_space');
		
		
		$this->form_validation->set_rules('agency_contact_email', 'Contact Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('agency_phone', 'Agency Phone', 'trim|required|phone_valid|min_length[10]|max_length[12]');
		
		$this->form_validation->set_rules('agency_customer_support_no', 'Agency Customer Support No.', 'trim|phone_valid|min_length[10]|max_length[12]');
		$this->form_validation->set_rules('agency_website', 'Agency Website', 'trim|valid_url');
		
				
		$this->form_validation->set_rules('agency_location', 'Agency Location ', 'trim|required');
		
		if($this->session->userdata('admintype') == 1){
		$this->form_validation->set_rules('agent_app_approved', 'Agent Application Approve', 'trim|required');
		$this->form_validation->set_rules('agent_background_approved', 'Agent Background Approve', 'trim|required');
		$this->form_validation->set_rules('agent_phone_approved', 'Agent Phone Approve', 'trim|required');
		$this->form_validation->set_rules('agent_level', 'Agent Level', 'trim|numeric');
		}
		
		
		
		
		if($this->form_validation->run() == FALSE)
		{
				if(validation_errors())
				{													
					$data["error"] = validation_errors();
				}
				else
				{		
					$data["error"] = "";							
				}
				
				
				
				if($_POST)
				{
					$data['first_name']=$this->input->post('first_name');
					$data['last_name']=$this->input->post('last_name');
					$data['email']=$this->input->post('email');
									
					$data['user_status']=$this->input->post('user_status');				
				
					$data['about_user']=$this->input->post('about_user');	
						
					
					$data['agent_id']=$this->input->post('agent_id');
										
					$data['agency_name']=$this->input->post('agency_name');
					$data['contact_person_name']=$this->input->post('contact_person_name');
					
					$data['agency_contact_email']=$this->input->post('agency_contact_email');
					$data['agency_phone']=$this->input->post('agency_phone');
					$data['agency_customer_support_no']=$this->input->post('agency_customer_support_no');
					
					$data['agency_website']=$this->input->post('agency_website');
					$data['agency_location']=$this->input->post('agency_location');
					
					$data['agent_member_of_association']=$this->input->post('agent_member_of_association');
					
					$data['agent_app_approved']=$this->input->post('agent_app_approved');
					$data['agent_background_approved']=$this->input->post('agent_background_approved');
					$data['agent_phone_approved']=$this->input->post('agent_phone_approved');
					$data['super_admin_approve']=$this->input->post('super_admin_approve');
					
					
					$data['agent_level']=$this->input->post('agent_level');
					
				}
				else
				{
					$data['first_name']=$agent_detail->first_name;
					$data['last_name']=$agent_detail->last_name;
					$data['email']=$agent_detail->email;
									
					$data['user_status']=$agent_detail->user_status;
									
					$data['about_user']=$agent_detail->about_user;	
						
					
					$data['agent_id']=$agent_detail->agent_id;
										
					$data['agency_name']=$agent_detail->agency_name;
					$data['contact_person_name']=$agent_detail->contact_person_name;
					
					$data['agency_contact_email']=$agent_detail->agency_contact_email;
					$data['agency_phone']=$agent_detail->agency_phone;
					$data['agency_customer_support_no']=$agent_detail->agency_customer_support_no;
					
					$data['agency_website']=$agent_detail->agency_website;
					$data['agency_location']=$agent_detail->agency_location;
					$data['super_admin_approve']=$agent_detail->super_admin_approve;
					
					$exp_assoc=array();
					
					if($agent_detail->agent_member_of_association!='')
					{
						if(substr_count($agent_detail->agent_member_of_association,',')>=1)
						{
							$exp_assoc=explode(',',$agent_detail->agent_member_of_association);
						}
						else
						{
							$exp_assoc[]=$agent_detail->agent_member_of_association;
						}
					}
					
					$data['agent_member_of_association']=$exp_assoc;
					
					$data['agent_app_approved']=$agent_detail->agent_app_approved;
					$data['agent_background_approved']=$agent_detail->agent_background_approved;
					$data['agent_phone_approved']=$agent_detail->agent_phone_approved;
					
					
					
					$data['agent_level']=$agent_detail->agent_level;
				}
					
					
					
		} else	{
				
		
			$insert_return_paasword=$this->agent_model->edit_agent();
			
			if($this->input->post('agent_app_approved') == 1 && $insert_return_paasword != '')
			{
			
						
				
						$email=$this->input->post('email');
						$get_user_info=$this->db->get_where('user',array('email'=>$email));
						$user_info=$get_user_info->row();
						
						
						$username =$user_info->full_name;						
						$password = $insert_return_paasword;
						
						$email = $this->input->post('email');	
						
								
						
						$email_verification_code=$user_info->email_verification_code;
						$user_id=$user_info->user_id;
						
						$agent_activation_link=front_base_url().'agentactivate/'.$user_id.'/'.$email_verification_code;
						
						$agent_preview_profile_link=front_base_url().'previewprofile/'.$user_id.'/'.$email_verification_code;
						
						
						
					
						/////////////============welcome email===========	

				
					$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='AgentJoin'");
					$email_temp=$email_template->row();
					
					$email_from_name=$email_temp->from_name;
					
					$email_address_from=$email_temp->from_address;
					$email_address_reply=$email_temp->reply_address;
					
					$email_subject=$email_temp->subject;				
					$email_message=$email_temp->message;			
					
					$email_to=$email;		
				
					
				
					
					$email_message=str_replace('{break}','<br/>',$email_message);
					
					$email_message=str_replace('{user_name}',$username,$email_message);			
					$email_message=str_replace('{email}',$email,$email_message);
					$email_message=str_replace('{password}',$password,$email_message);
					
					$email_message=str_replace('{agent_activation_link}',$agent_activation_link,$email_message);
					
					
					$email_temp_url=upload_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
					
					$str=$email_message;		
					
					/** custom_helper email function **/
						
					email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
					
					
					
					/////////////============new user email===========	


				
					$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='AgentProfilePreview'");
					$email_temp=$email_template->row();
					
					
					$email_from_name=$email_temp->from_name;
					
					$email_address_from=$email_temp->from_address;
					$email_address_reply=$email_temp->reply_address;
					
					$email_subject=$email_temp->subject;				
					$email_message=$email_temp->message;
					
					
					
					$email_to=$email;
					
					
				
					$login_link=front_base_url().'sign_up';
					
					$email_message=str_replace('{break}','<br/>',$email_message);
					
					$email_message=str_replace('{user_name}',$username,$email_message);
					
					$email_message=str_replace('{agent_activation_link}',$agent_activation_link,$email_message);
					$email_message=str_replace('{agent_preview_profile_link}',$agent_preview_profile_link,$email_message);
					
					
					$email_temp_url=upload_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
					
					
					
					$str=$email_message;
				
					
					/** custom_helper email function **/
					
					email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
	
					
	
					/////////////============new user email===========	
				}
					
					
			
			if($this->input->post('agent_app_approved') == 1 && $insert_return_paasword == '')
			{
				
						
						
						
						/////////////============application approved email===========	
						
					
				
					$email=$this->input->post('email');
					$get_user_info=$this->db->get_where('user',array('email'=>$email));
					$user_info=$get_user_info->row();
					
					$username =$user_info->full_name;						
					$user_id=$user_info->user_id;
						
					
						

				
					$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Appapproved'");
					$email_temp=$email_template->row();
					
					$email_from_name=$email_temp->from_name;
					
					$email_address_from=$email_temp->from_address;
					$email_address_reply=$email_temp->reply_address;
					
					$email_subject=$email_temp->subject;				
					$email_message=$email_temp->message;			
					
					$email_to=$email;		
				
					
				
					
					$email_message=str_replace('{break}','<br/>',$email_message);
					
					$email_message=str_replace('{user_name}',$username,$email_message);			
					
					
					$email_temp_url=upload_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
					
					$str=$email_message;		
					
					/** custom_helper email function **/
						
					email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
					
					
					
					/////////////============application approved email===========	

					
					
					
					}
					
					$msg=	'';
					if($this->input->post('agent_app_approved') == 1)
					{
						$msg='accept';
					}	
					elseif($this->input->post('agent_app_approved') == 0)
					{
						$msg='reject';
					}
					
					
			redirect('agent/agent_detail/'.$user_id.'/'.$msg);
			
		
		}		
		
	
		
			
		$data['theme']=$theme;
		$data['site_setting'] =	$site_setting;	
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/edit_agent',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
		
		
		
	}
	
	
	
	function agent_detail($user_id=0,$msg='')
	{
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$site_setting = site_setting();
		
		
		
		if($user_id>0)
		{
			
			$agent_detail=$this->agent_model->get_agent_by_id($user_id);
						
			if(!$agent_detail) { redirect('agent/list_active_agent/'); } 
			
			
			
			
		} else {   redirect('agent/list_active_agent/');  }
		
		
		
		
		if($_POST)
		{				
			
			
					$insert_return_paasword = $this->agent_model->update_agent();
					
					if($this->input->post('agent_app_approved') == 1 && $insert_return_paasword != '')
					{
			
						
				
						$email=$this->input->post('email');
						$get_user_info=$this->db->get_where('user',array('email'=>$email));
						$user_info=$get_user_info->row();
						
						
						$username =$user_info->full_name;						
						$password = $insert_return_paasword;
						
						$email = $this->input->post('email');	
						
								
						
						$email_verification_code=$user_info->email_verification_code;
						$user_id=$user_info->user_id;
						
						$agent_activation_link=front_base_url().'agentactivate/'.$user_id.'/'.$email_verification_code;
						
						$agent_preview_profile_link=front_base_url().'previewprofile/'.$user_id.'/'.$email_verification_code;
						
						
						
					
						/////////////============welcome email===========	

				
					$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='AgentJoin'");
					$email_temp=$email_template->row();
					
					$email_from_name=$email_temp->from_name;
					
					$email_address_from=$email_temp->from_address;
					$email_address_reply=$email_temp->reply_address;
					
					$email_subject=$email_temp->subject;				
					$email_message=$email_temp->message;			
					
					$email_to=$email;		
				
					
				
					
					$email_message=str_replace('{break}','<br/>',$email_message);
					
					$email_message=str_replace('{user_name}',$username,$email_message);			
					$email_message=str_replace('{email}',$email,$email_message);
					$email_message=str_replace('{password}',$password,$email_message);
					
					$email_message=str_replace('{agent_activation_link}',$agent_activation_link,$email_message);
					
					
					$email_temp_url=upload_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
					
					$str=$email_message;		
					
					/** custom_helper email function **/
						
					email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
					
					
					
					/////////////============new user email===========	


				
					$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='AgentProfilePreview'");
					$email_temp=$email_template->row();
					
					
					$email_from_name=$email_temp->from_name;
					
					$email_address_from=$email_temp->from_address;
					$email_address_reply=$email_temp->reply_address;
					
					$email_subject=$email_temp->subject;				
					$email_message=$email_temp->message;
					
					
					
					$email_to=$email;
					
					
				
					$login_link=front_base_url().'sign_up';
					
					$email_message=str_replace('{break}','<br/>',$email_message);
					
					$email_message=str_replace('{user_name}',$username,$email_message);
					
					$email_message=str_replace('{agent_activation_link}',$agent_activation_link,$email_message);
					$email_message=str_replace('{agent_preview_profile_link}',$agent_preview_profile_link,$email_message);
					
					
					$email_temp_url=upload_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
					
					
					
					$str=$email_message;
					
					/** custom_helper email function **/
					
					email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
	
					
	
					/////////////============new user email===========	
				}
				
					
					if($this->input->post('agent_app_approved') == 1 && $insert_return_paasword == '')
					{
						
						
						/////////////============application approved email===========	
						
					
				
						$email=$this->input->post('email');
						$get_user_info=$this->db->get_where('user',array('email'=>$email));
						$user_info=$get_user_info->row();
						
						$username =$user_info->full_name;						
						$user_id=$user_info->user_id;
						
					
						

				
					$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Appapproved'");
					$email_temp=$email_template->row();
					
					$email_from_name=$email_temp->from_name;
					
					$email_address_from=$email_temp->from_address;
					$email_address_reply=$email_temp->reply_address;
					
					$email_subject=$email_temp->subject;				
					$email_message=$email_temp->message;			
					
					$email_to=$email;		
				
					
				
					
					$email_message=str_replace('{break}','<br/>',$email_message);
					
					$email_message=str_replace('{user_name}',$username,$email_message);			
					
					
					$email_temp_url=upload_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
					
					$str=$email_message;		
					
					/** custom_helper email function **/
						
					email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
					
					
					
					/////////////============application approved email===========	

					
						
					}	
					
					if($this->input->post('agent_app_approved') == 1)
					{
						$msg='accept';
					}
					elseif($this->input->post('agent_app_approved') == 0)
					{
						$msg='reject';
					}
					
					
		}
		
	
		$agent_detail=$this->agent_model->get_agent_by_id($user_id);
		
		$data['msg']=$msg;
		
		$data["error"] = "";			
		$data['user_id']=$user_id;
		
		$data['row']=$agent_detail;	
		
		
		$data['agent_document']=$this->agent_model->get_agent_document($user_id);
		
		$data['theme']=$theme;
		$data['site_setting'] =	$site_setting;	
		
		
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/agent_detail',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	
	function send_email_on_active($user_id)
	{
		
		$agent_detail=$this->agent_model->get_agent_by_id($user_id);
		
		$password='';
		
			if($agent_detail->verify_email == 0)
			{
				$password=randomCode();
				
				$data_user = array('password'=>md5($password));
				
				$this->db->where('user_id',$user_id);
				$this->db->update('user',$data_user);
								
			}
			
			
		
					if($agent_detail->agent_app_approved == 1 && $password != '')
					{
			
						
				
						$email=$agent_detail->email;
						$get_user_info=$this->db->get_where('user',array('email'=>$email));
						$user_info=$get_user_info->row();
						
						
						$username =$user_info->full_name;						
						
						
					
								
						
						$email_verification_code=$user_info->email_verification_code;
						$user_id=$user_info->user_id;
						
						$agent_activation_link=front_base_url().'agentactivate/'.$user_id.'/'.$email_verification_code;
						
						$agent_preview_profile_link=front_base_url().'previewprofile/'.$user_id.'/'.$email_verification_code;
						
						
						
					
						/////////////============welcome email===========	

				
					$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='AgentJoin'");
					$email_temp=$email_template->row();
					
					$email_from_name=$email_temp->from_name;
					
					$email_address_from=$email_temp->from_address;
					$email_address_reply=$email_temp->reply_address;
					
					$email_subject=$email_temp->subject;				
					$email_message=$email_temp->message;			
					
					$email_to=$email;		
				
					
				
					
					$email_message=str_replace('{break}','<br/>',$email_message);
					
					$email_message=str_replace('{user_name}',$username,$email_message);			
					$email_message=str_replace('{email}',$email,$email_message);
					$email_message=str_replace('{password}',$password,$email_message);
					
					$email_message=str_replace('{agent_activation_link}',$agent_activation_link,$email_message);
					
					
					$email_temp_url=upload_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
					
					$str=$email_message;		
					
					/** custom_helper email function **/
						
					email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
					
					
					
					/////////////============new user email===========	


				
					$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='AgentProfilePreview'");
					$email_temp=$email_template->row();
					
					
					$email_from_name=$email_temp->from_name;
					
					$email_address_from=$email_temp->from_address;
					$email_address_reply=$email_temp->reply_address;
					
					$email_subject=$email_temp->subject;				
					$email_message=$email_temp->message;
					
					
					
					$email_to=$email;
					
					
				
					$login_link=front_base_url().'sign_up';
					
					$email_message=str_replace('{break}','<br/>',$email_message);
					
					$email_message=str_replace('{user_name}',$username,$email_message);
					
					$email_message=str_replace('{agent_activation_link}',$agent_activation_link,$email_message);
					$email_message=str_replace('{agent_preview_profile_link}',$agent_preview_profile_link,$email_message);
					
					
					$email_temp_url=upload_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
					
					
					
					$str=$email_message;
					
					/** custom_helper email function **/
					
					email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
	
					
	
					/////////////============new user email===========	
				}
				
					
					if($agent_detail->agent_app_approved == 1 && $password == '')
					{
						
						
						/////////////============application approved email===========	
						
					
				
						$email=$agent_detail->email;
						$get_user_info=$this->db->get_where('user',array('email'=>$email));
						$user_info=$get_user_info->row();
						
						$username =$user_info->full_name;						
						$user_id=$user_info->user_id;
						
					
						

				
					$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Appapproved'");
					$email_temp=$email_template->row();
					
					$email_from_name=$email_temp->from_name;
					
					$email_address_from=$email_temp->from_address;
					$email_address_reply=$email_temp->reply_address;
					
					$email_subject=$email_temp->subject;				
					$email_message=$email_temp->message;			
					
					$email_to=$email;		
				
					
				
					
					$email_message=str_replace('{break}','<br/>',$email_message);
					
					$email_message=str_replace('{user_name}',$username,$email_message);			
					
					
					$email_temp_url=upload_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
					
					$str=$email_message;		
					
					/** custom_helper email function **/
						
					email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
					
					
					
					/////////////============application approved email===========	

					
						
					}	
		
	}
	
	
	////////////=====agents==========
	
	
	function list_all_agent($limit=15,$offset=0,$msg='')
	{
		//$check_rights=get_rights('list_all_pin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='4';
		$config['base_url'] = site_url('agent/list_all_agent/'.$limit.'/');
		$config['total_rows'] = $this->agent_model->get_total_agent_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->agent_model->get_all_agent_result($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/list_all_agent',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}
	
	
	function search_all_agent($limit=15,$option='',$keyword='',$offset=0,$msg='')
	{
		//$check_rights=get_rights('list_all_pin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
		
	
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		if($_POST)
		{		
			$option=$this->input->post('option');
			$keyword=$this->input->post('keyword');
		}
		else
		{
			$option=$option;
			$keyword=$keyword;			
		}
		
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='6';
		$config['base_url'] = site_url('agent/search_all_agent/'.$limit.'/'.$option.'/'.$keyword.'/');
		$config['total_rows'] = $this->agent_model->get_total_search_agent_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->agent_model->get_all_search_agent_result($option,$keyword,$offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']=$option;
		$data['keyword']=$keyword;
		$data['search_type']='search';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/list_all_agent',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}
	
	
	
	////********Pending agent for approval by super admin**********/////////
	
	function list_pending_agent($limit=15,$offset=0,$msg='')
	{
		
		$check_rights=get_rights('list_pending_agent');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='4';
		$config['base_url'] = site_url('agent/list_pending_agent/'.$limit.'/');
		$config['total_rows'] = $this->agent_model->get_total_pending_agent_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->agent_model->get_all_pending_agent_result($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/list_pending_agent',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	
	}
	
	function search_pending_agent($limit=15,$option='',$keyword='',$offset=0,$msg='')
	{
		
		$check_rights=get_rights('list_pending_agent');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}
	
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		if($_POST)
		{		
			$option=$this->input->post('option');
			$keyword=$this->input->post('keyword');
		}
		else
		{
			$option=$option;
			$keyword=$keyword;			
		}
		
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='6';
		$config['base_url'] = site_url('agent/search_pending_agent/'.$limit.'/'.$option.'/'.$keyword.'/');
		$config['total_rows'] = $this->agent_model->get_total_search_pending_agent_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->agent_model->get_all_search_pending_agent_result($option,$keyword,$offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']=$option;
		$data['keyword']=$keyword;
		$data['search_type']='search';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/list_pending_agent',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	
	}
	
	////////**********End approval of super admin**********//////////////////
	
	function list_active_agent($limit=15,$offset=0,$msg='')
	{
		//$check_rights=get_rights('list_all_pin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='4';
		$config['base_url'] = site_url('agent/list_active_agent/'.$limit.'/');
		$config['total_rows'] = $this->agent_model->get_total_active_agent_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->agent_model->get_active_agent_result($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/list_active_agent',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}
	
	
	function search_active_agent($limit=15,$option='',$keyword='',$offset=0,$msg='')
	{
		//$check_rights=get_rights('list_all_pin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
		
	
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		if($_POST)
		{		
			$option=$this->input->post('option');
			$keyword=$this->input->post('keyword');
		}
		else
		{
			$option=$option;
			$keyword=$keyword;			
		}
		
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='6';
		$config['base_url'] = site_url('agent/search_active_agent/'.$limit.'/'.$option.'/'.$keyword.'/');
		$config['total_rows'] = $this->agent_model->get_total_search_active_agent_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->agent_model->get_active_search_agent_result($option,$keyword,$offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']=$option;
		$data['keyword']=$keyword;
		$data['search_type']='search';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/list_active_agent',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}
	
	
	
	
	function list_inactive_agent($limit=15,$offset=0,$msg='')
	{
		//$check_rights=get_rights('list_all_pin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='4';
		$config['base_url'] = site_url('agent/list_inactive_agent/'.$limit.'/');
		$config['total_rows'] = $this->agent_model->get_total_inactive_agent_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->agent_model->get_inactive_agent_result($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/list_inactive_agent',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}
	
	
	function search_inactive_agent($limit=15,$option='',$keyword='',$offset=0,$msg='')
	{
		//$check_rights=get_rights('list_all_pin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
		
	
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		if($_POST)
		{		
			$option=$this->input->post('option');
			$keyword=$this->input->post('keyword');
		}
		else
		{
			$option=$option;
			$keyword=$keyword;			
		}
		
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='6';
		$config['base_url'] = site_url('agent/search_inactive_agent/'.$limit.'/'.$option.'/'.$keyword.'/');
		$config['total_rows'] = $this->agent_model->get_total_search_inactive_agent_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->agent_model->get_inactive_search_agent_result($option,$keyword,$offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']=$option;
		$data['keyword']=$keyword;
		$data['search_type']='search';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/list_inactive_agent',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}
	
	
	
	
	function list_suspend_agent($limit=15,$offset=0,$msg='')
	{
		//$check_rights=get_rights('list_all_pin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='4';
		$config['base_url'] = site_url('agent/list_suspend_agent/'.$limit.'/');
		$config['total_rows'] = $this->agent_model->get_total_suspend_agent_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->agent_model->get_suspend_agent_result($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/list_suspend_agent',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}
	
	
	function search_suspend_agent($limit=15,$option='',$keyword='',$offset=0,$msg='')
	{
		//$check_rights=get_rights('list_all_pin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
		
	
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		if($_POST)
		{		
			$option=$this->input->post('option');
			$keyword=$this->input->post('keyword');
		}
		else
		{
			$option=$option;
			$keyword=$keyword;			
		}
		
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='6';
		$config['base_url'] = site_url('agent/search_suspend_agent/'.$limit.'/'.$option.'/'.$keyword.'/');
		$config['total_rows'] = $this->agent_model->get_total_search_suspend_agent_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->agent_model->get_suspend_search_agent_result($option,$keyword,$offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']=$option;
		$data['keyword']=$keyword;
		$data['search_type']='search';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/list_suspend_agent',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}
	
	
	
	
	function agent_login($offset=0,$msg='')
	{
		/*$check_rights=get_rights('user_login');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}*/
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$this->load->library('pagination');

		$limit = '20';
		
		$config['base_url'] = site_url('agent/agent_login/');
		$config['total_rows'] = $this->agent_model->get_total_userlogin_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['msg']=$msg;
		$data['result'] = $this->agent_model->get_userlogin_result($offset, $limit);
		$data['offset'] = $offset;
		
		$data['site_setting'] = site_setting();

		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/list_agent_login',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}

	
	function action_login()
	{
		/*$check_rights=get_rights('user_login');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}*/
		
		
		$offset=$this->input->post('offset');
		$action=$this->input->post('action');
		$login_id=$this->input->post('chk');
			
		
		if($action=='delete')
		{		
			foreach($login_id as $id)
			{			
				$this->db->query("delete from ".$this->db->dbprefix('user_login')." where login_id='".$id."'");
			}
			
			redirect('agent/agent_login/'.$offset.'/delete');
		}	
		
	}
	
	
	
	
	
	
	function action_agent($type)
	{
		
		 $offset=$this->input->post('offset');
		 $limit=$this->input->post('limit');
		 $action=$this->input->post('action');
		 $user_id=$this->input->post('chk');
		 $search_type=$this->input->post('search_type');
		
		 $option=$this->input->post('option');
		 $keyword=$this->input->post('keyword');
		
		$red_link='list_'.$type.'_agent/'.$limit;
		
		if($search_type=='search')
		{
			$red_link='search_'.$type.'_agent/'.$limit.'/'.$option.'/'.$keyword;
		}
		
		
		if($action=='delete')
		{	
			foreach($user_id as $id)
			{
			
				$this->agent_model->delete_agent($id);	
			
			}
			
			redirect('agent/'.$red_link.'/'.$offset.'/delete');
		}
		
		if($action=='active')
		{	
			foreach($user_id as $id)
			{
			
				
				////===report part====
				
				$agent_detail=$this->agent_model->get_agent_by_id($id);
		
				$admin_id=$agent_detail->admin_id;
				
				$new_add=0;		
				$new_approve=1;
				$new_reject=0;
				
				
		
				if($admin_id>0)
				{	
					//===already apporved then do not update
					if($agent_detail->agent_app_approved==1 && $new_approve==1)
					{
						$new_approve=0;
					}
					
					//===already in waiting or rejected then do not update
					if($agent_detail->agent_app_approved==0 && $new_reject==1)
					{
						$new_reject=0;
					}
					
					///====if already apporoved and now reject
					if($agent_detail->agent_app_approved==1 && $new_reject==1)
					{
						$new_approve=-1;
						$new_reject=1;
						
					}
					
					///====if already reject or waiting and now approved
					if($agent_detail->agent_app_approved==0 && $new_approve==1)
					{
						$new_approve=1;
						$new_reject=-1;
						
					}
			
					$this->agent_model->add_new_report($admin_id,$new_add,$new_approve,$new_reject);
				}
				
				////===report part====
				
				
				$this->db->where('user_id',$id);
				$this->db->update('agent_profile',array('agent_status'=>1,'agent_app_approved'=>1,'super_admin_approve'=>1));
				
				$userdata = array(
					'user_status' => 1
				);		
				$this->db->where('user_id',$id);
				$this->db->update('user',$userdata);
				
				$this->send_email_on_active($id);	
				
				
			
			}
			
			redirect('agent/'.$red_link.'/'.$offset.'/active');
		}
		
		if($action=='inactive')
		{	
			foreach($user_id as $id)
			{
				
				////===report part====
				
				$agent_detail=$this->agent_model->get_agent_by_id($id);
		
				$admin_id=$agent_detail->admin_id;
				
				$new_add=0;		
				$new_approve=0;
				$new_reject=1;
				
				
		
				if($admin_id>0)
				{
					//===already apporved then do not update
					if($agent_detail->agent_app_approved==1 && $new_approve==1)
					{
						$new_approve=0;
					}
					
					//===already in waiting or rejected then do not update
					if($agent_detail->agent_app_approved==0 && $new_reject==1)
					{
						$new_reject=0;
					}
					
					///====if already apporoved and now reject
					if($agent_detail->agent_app_approved==1 && $new_reject==1)
					{
						$new_approve=-1;
						$new_reject=1;
						
					}
					
					///====if already reject or waiting and now approved
					if($agent_detail->agent_app_approved==0 && $new_approve==1)
					{
						$new_approve=1;
						$new_reject=-1;
						
					}
					
					$this->agent_model->add_new_report($admin_id,$new_add,$new_approve,$new_reject);
				}
				
				////===report part====
				
				
				$this->db->where('user_id',$id);
				$this->db->update('agent_profile',array('agent_status'=>1,'agent_app_approved'=>0,'super_admin_approve'=>0));
				
				$userdata = array(
					'user_status' => 0
				);		
				$this->db->where('user_id',$id);
				$this->db->update('user',$userdata);
				
				
		
				
			
			}
			
			redirect('agent/'.$red_link.'/'.$offset.'/inactive');
		}
		
	
		
	}
	
	
	function edit_suspend($type)
	{
		
		$user_id=$this->input->post('user_id');
		
		$no_of_day=$this->input->post('no_of_day');
		$suspend_reason=$this->input->post('suspend_reason');
		$is_permanent=$this->input->post('is_permanent');
		
		
		$suspend_to_date =date('Y-m-d H:i:s',mktime(date('H'),date('i'),date('s'),date('m'),date('d')+$no_of_day,date('Y')));
					
					
		$suspenddata = array(
			'user_id' => $user_id,
			'suspend_from_date' => date('Y-m-d H:i:s'),
			'suspend_to_date' => $suspend_to_date,
			'suspend_reason' => $suspend_reason,
			'is_permanent' => $is_permanent,
		);
		
		
		$this->db->insert('user_suspend',$suspenddata);
		
		$userdata = array(
			'user_status' => 2
		);		
		$this->db->where('user_id',$user_id);
		$this->db->update('user',$userdata);
		
		
		
		 $offset=$this->input->post('offset');
		 $limit=$this->input->post('limit');
		
		 $search_type=$this->input->post('search_type');
		
		 $option=$this->input->post('option');
		 $keyword=$this->input->post('keyword');
		
		
		$red_link='list_'.$type.'_agent/'.$limit;
		
		if($search_type=='search')
		{
			$red_link='search_'.$type.'_agent/'.$limit.'/'.$option.'/'.$keyword;
		}
		
		
		echo site_url('agent/'.$red_link.'/'.$offset.'/suspend');
		die;
		
	}	
	
	function change_password($type='all',$user_id,$offset=0,$limit=15,$search_type='normal',$keyword='',$option='')
	{
		
		
		$user_info=$this->user_model->get_one_user($user_id);

		$password = randomCode();

		$data=array(
			'password'=>md5($password)		
		);
		
		$this->db->where('user_id',$user_id);		
		$this->db->update('user', $data); 
		
		///================Send Email To user start
			$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Change Password By Admin'");
			$email_temp=$email_template->row();
			
			
			$email_from_name=$email_temp->from_name;
			
			$email_address_from=$email_temp->from_address;
			$email_address_reply=$email_temp->reply_address;
			
			$email_subject=$email_temp->subject;				
			$email_message=$email_temp->message;			
			
							
			$email_to=$user_info->email;		
		
			$email_message=str_replace('{break}','<br/>',$email_message);	
			$email_message=str_replace('{user_name}',ucfirst($user_info->full_name),$email_message);		
			$email_message=str_replace('{password}',$password,$email_message);	
			
			$email_temp_url=upload_url().'email/';
				$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
				
			
			$str=$email_message;
			
			/** custom_helper email function **/
				
			email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
		///================Send Email To user end
		
		
		
		$red_link='list_'.$type.'_agent/'.$limit;
		
		if($search_type=='search')
		{
			$red_link='search_'.$type.'_agent/'.$limit.'/'.$option.'/'.$keyword;
		}
		
		
		redirect('agent/'.$red_link.'/'.$offset.'/passwordchange');
	}
	
	function resend_verification($type='all',$user_id,$offset=0,$limit=15,$search_type='normal',$keyword='',$option='')
	{
			
			$get_user_info=$this->db->get_where('user',array('user_id'=>$user_id));
			
			if($get_user_info->num_rows()>0)
			{
				$user_info=$get_user_info->row();
				
				
				$email=$user_info->email;
				
				
				$username =ucfirst($user_info->first_name).' '.ucfirst($user_info->last_name);	
				$profile_name =$user_info->profile_name;	
							
				$email_verification_code=$user_info->email_verification_code;
				$user_id=$user_info->user_id;
				
				$verification_link=front_base_url().'verify/'.$user_id.'/'.$email_verification_code;
			
				
							
				$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Resend Verification'");
				$email_temp=$email_template->row();
			
				if($email_temp)
				{
				$email_from_name=$email_temp->from_name;
				
				$email_address_from=$email_temp->from_address;
				$email_address_reply=$email_temp->reply_address;
				
				$email_subject=$email_temp->subject;				
				$email_message=$email_temp->message;
				
				
				
				$email_to=$email;
				
				
				
				$email_message=str_replace('{break}','<br/>',$email_message);
				$email_message=str_replace('{user_name}',$username,$email_message);
				$email_message=str_replace('{verify_link}',$verification_link,$email_message);
				
				
					$email_temp_url=upload_url().'email/';
			$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
				
				 $str=$email_message;
			
				
				/** custom_helper email function **/
				if($email_to!='')
				{
					email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
				}
				
				

			}
			
			
			}
			
			
			
			
			$red_link='list_'.$type.'_agent/'.$limit;
		
			if($search_type=='search')
			{
				$red_link='search_'.$type.'_agent/'.$limit.'/'.$option.'/'.$keyword;
			}
			
			
			redirect('agent/'.$red_link.'/'.$offset.'/resendverification');
			
			
			
	}
	
	
	function resend_email_invitation($user_id='')
	{
		
		
		$insert_return_paasword = $this->agent_model->resend_invitation_agent($user_id);
	
		if($insert_return_paasword)
		{
			
			
			$user_info = get_user_profile_by_id($user_id);
			$username =$user_info->full_name;						
			$password = $insert_return_paasword;
			
			$email = $user_info->email;	
			
			$email_verification_code=$user_info->email_verification_code;
			$user_id=$user_info->user_id;
							
			$agent_activation_link=front_base_url().'agentactivate/'.$user_id.'/'.$email_verification_code;
			$agent_preview_profile_link=front_base_url().'previewprofile/'.$user_id.'/'.$email_verification_code;
			
						
			/////////////============welcome email===========	
	
			$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='AgentJoin'");
			$email_temp=$email_template->row();
			
			$email_from_name=$email_temp->from_name;
			$email_address_from=$email_temp->from_address;
			$email_address_reply=$email_temp->reply_address;
			$email_subject=$email_temp->subject;				
			$email_message=$email_temp->message;			
			$email_to=$email;	
			
			
			$email_message=str_replace('{break}','<br/>',$email_message);
			$email_message=str_replace('{user_name}',$username,$email_message);			
			$email_message=str_replace('{email}',$email,$email_message);
			$email_message=str_replace('{password}',$password,$email_message);
			$email_message=str_replace('{agent_activation_link}',$agent_activation_link,$email_message);
			$email_temp_url=upload_url().'email/';
			$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
			$str=$email_message;	
			/** custom_helper email function **/
			email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
			redirect('agent/list_all_agent/15/0/resendverification/');
			/////////////============new user email===========	
		}
		else
		{
			redirect('agent/list_all_agent/15/0/notresendverification/');
			
		}
		
				
		
	}
	
	
		
	//////////============invite part
	
	
	
	/*** check the user unique email address
	*	var string $email
	*  return boolen
	**/
		
	function inviteemail_check($agent_email)
	{
		$cemail = $this->agent_model->chkinviteemailTaken($agent_email);
		
		if($cemail)
		{
			$this->form_validation->set_message('email_check', 'Invitation already sent to with this email.');
			return FALSE;
		}
		else
		{
				return TRUE;
		}
	}	
	
	
	
	function import_invite()
	{
			
		$data = array();
		
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
	
		$data['error']='';
		
		$data['site_setting'] = site_setting();
	
		 $this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		 $this->template->write_view('center',$theme .'/layout/agent/invite_agent',$data,TRUE);
		 $this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		 $this->template->render();
		
				
	}
	
	
	function add_csv_upload()
	{
		$data = array();
		
		
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		
			$base_path = base_path();
		
		
		
			
				$this->load->library('upload');
				$rand=rand(0,100000);

$allowed_array=array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel');


if(!in_array($_FILES["upcsv"]["type"],$allowed_array))
{
	$data['error'] = '<p>File you are trying to upload is not valid.</p>';
				
				$data['site_setting'] = site_setting();
				
				
				
				   $this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
			       $this->template->write_view('center',$theme .'/layout/agent/invite_agent',$data,TRUE);
			       $this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			       $this->template->render();
}
else
{		 
	  
		  
			/*
				$_FILES['userfile']['name']     =   $_FILES['upcsv']['name'];
				$_FILES['userfile']['type']     =   $_FILES['upcsv']['type'];  ///echo "<br/>inni<br />";
				$_FILES['userfile']['tmp_name'] =   $_FILES['upcsv']['tmp_name'];
				$_FILES['userfile']['error']    =   $_FILES['upcsv']['error'];
				$_FILES['userfile']['size']     =   $_FILES['upcsv']['size']; 
				
				
			$config['file_name']     =$rand.'inviteagent';
			$config['upload_path'] = $base_path.'admin/upload_csv/'; 
			$config['allowed_types'] = 'csv';
			$config['max_size'] = '300';
			
			
			 $this->upload->initialize($config);*/
			 
		/*	 
			if (!$this->upload->do_upload())
			{		
				
				$error = array('error' => $this->upload->display_errors());
				$err = str_replace("<p>","",$error['error']);
				$err = str_replace("</p>","",$err);*/
		
		move_uploaded_file($_FILES["upcsv"]["tmp_name"],  $base_path.'admin/upload_csv/'.$rand.'inviteagent.csv');
				
		if(!file_exists($base_path.'admin/upload_csv/'.$rand.'inviteagent.csv'))
		{
				$data['error'] = '<p>Fail to upload a File, Please try again.</p>';
				
				$data['site_setting'] = site_setting();
				
				
				
				   $this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
			       $this->template->write_view('center',$theme .'/layout/agent/invite_agent',$data,TRUE);
			       $this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
			       $this->template->render();
			   
			}
			else{
			
			
			
			   ////=====send email setting
				$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='InviteEmail'");
				$email_temp=$email_template->row();
				
				
				$email_from_name=$email_temp->from_address;
				
				$email_address_from=$email_temp->from_address;
				$email_address_reply=$email_temp->reply_address;
				
				$email_subject=$email_temp->subject;				
				
				 ////=====send email setting	
					
					
			
					//$picture = $this->upload->data();
								
				  
				  // $file_name=$base_path.'admin/upload_csv/'.$picture['file_name'];
				  	
					$file_name=$base_path.'admin/upload_csv/'.$rand.'inviteagent.csv';
									
						
						$fp = fopen("$file_name", "r");//for open uploaded file
						$data = fgetcsv($fp, 500, ",");
						$cnt = 1;
						$users = array();
						while (($data = fgetcsv($fp, 500, ",")) != FALSE) //for get content
						{
							
																
								if($data[0]!='' && $data[2]!='')
								{
									
									$email=trim(str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$data[2])));
									
									if($email!='')
									{
									
									
										$chk_user=$this->agent_model->chkinviteemailTaken($email);
									
										if($chk_user== FALSE)
										{
										
										
										
										
										
										$first_name=trim(str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/",'@','1','2','3','4','5','6','7','8','9','0'),'',$data[0])));
										
										$last_name=trim(str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/",'@','1','2','3','4','5','6','7','8','9','0'),'',$data[1])));
										
										$company_name=trim(str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$data[3])));
										
										
										
										$data=array(
										'agent_first_name' => $first_name,
										'agent_last_name' => $last_name,
										'agent_email' => $email,
										'agent_company_name' => $company_name,
										'invite_date'=>date('Y-m-d H:i:s'),
										'invite_ip'=>$_SERVER['REMOTE_ADDR'],
										'admin_id' => $this->session->userdata('admin_id')
										);
										
										$this->db->insert('agent_invitation',$data);
		
		
							
							$last_id=mysql_insert_id();
							
								////////////=====send email====
							$agent_info='';
							
								$agent_email=$email;
								
								$get_agent_info=$this->db->get_where('agent_invitation',array('invite_id'=>$last_id));
								$agent_info=$get_agent_info->row();
								
								$agent_name = ucwords($agent_info->agent_first_name.' '.$agent_info->agent_last_name);					
								$agent_phone_no =$agent_info->agent_phone_no;	
											
								
								$invite_id=$agent_info->invite_id;
								
								$agent_signup_link=front_base_url().'home/agent_register/';
								
								$agent_signup_link_start='<a href="'.$agent_signup_link.'">';					
								$agent_signup_link_end='</a>';
					
				
				
								/////////////============ invite email===========	
				
								$email_message=$email_temp->message;	
										
									
													
									$email_to=$agent_email;		
									
									$str='';
									
									
									$email_message=str_replace('{break}','<br/>',$email_message);
									$email_message=str_replace('{agent_name}',$agent_name,$email_message);			
									$email_message=str_replace('{agent_email}',$agent_email,$email_message);
									$email_message=str_replace('{agent_signup_link}',$agent_signup_link,$email_message);
									$email_message=str_replace('{agent_signup_link_start}',$agent_signup_link_start,$email_message);
									$email_message=str_replace('{agent_signup_link_end}',$agent_signup_link_end,$email_message);
									
									$email_temp_url=upload_url().'email/';
									$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
									
									
									$str=$email_message;		
									
									
									/** custom_helper email function **/
									
									email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
								
								
								
								////////////=====send email====
							
							
							
															
										}
										
										
									}																	
									
								}
								
							
								
						}
			
			
					fclose($fp);
					
					/*if(is_file($base_path.'admin/upload_csv/'.$picture['file_name']))
					{
		
						unlink($base_path.'admin/upload_csv/'.$picture['file_name']);
					}*/
					
					if(is_file($file_name))
					{		
						unlink($file_name);
					}
					
					$limit=20;
					redirect('agent/list_invite_agent/'.$limit.'/0/insert');
			
			}		
		
		}
	}
	
	
	function add_invite()
	{
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$site_setting = site_setting();
		
		
		
		
		
		$this->form_validation->set_rules('agent_first_name', 'First Name', 'required|alpha');
		$this->form_validation->set_rules('agent_last_name', 'Last Name', 'required|alpha');
		$this->form_validation->set_rules('agent_phone_no', 'Phone No.', 'trim|numeric');
		$this->form_validation->set_rules('agent_email', 'Email', 'required|valid_email|callback_inviteemail_check');	
		$this->form_validation->set_rules('agent_about', 'About Agent', 'required');	
		
	
		if($this->form_validation->run() == FALSE)
		{
				if(validation_errors())
				{													
					$data["error"] = validation_errors();
				}
				else
				{		
					$data["error"] = "";							
				}
				
				
					$data['invite_id']=$this->input->post('invite_id');
					$data['agent_first_name']=$this->input->post('agent_first_name');
					$data['agent_last_name']=$this->input->post('agent_last_name');
					$data['agent_email']=$this->input->post('agent_email');
					$data['agent_phone_no']=$this->input->post('agent_phone_no');
					$data['agent_about']=$this->input->post('agent_about');
					$data['agent_company_name']=$this->input->post('agent_company_name');
					
					
		} else	{
				
				
				$data["error"] = "";			
				$data['invite_id']=$this->input->post('invite_id');
				$data['agent_first_name']=$this->input->post('agent_first_name');
				$data['agent_last_name']=$this->input->post('agent_last_name');
				$data['agent_email']=$this->input->post('agent_email');
				$data['agent_phone_no']=$this->input->post('agent_phone_no');
				$data['agent_about']=$this->input->post('agent_about');
				$data['agent_company_name']=$this->input->post('agent_company_name');
			
				
				if($this->input->post('invite_id')>0)
				{
					$msg='update';
					
					$this->agent_model->edit_invite();
					
				}
				else
				{
			
					$insert_return_paasword=$this->agent_model->add_invite();
					
					$msg='insert';
			
			
			
			
					$agent_email=$this->input->post('agent_email');
					
					$get_agent_info=$this->db->get_where('agent_invitation',array('agent_email'=>$agent_email));
					$agent_info=$get_agent_info->row();
					
					$agent_name = ucwords($agent_info->agent_first_name.' '.$agent_info->agent_last_name);					
					$agent_phone_no =$agent_info->agent_phone_no;	
								
					
					$invite_id=$agent_info->invite_id;
					
					$agent_signup_link=front_base_url().'home/agent_register/';
					
					$agent_signup_link_start='<a href="'.$agent_signup_link.'">';					
					$agent_signup_link_end='</a>';
					
				
				
				/////////////============ invite email===========	

				
					$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='InviteEmail'");
					$email_temp=$email_template->row();
					
					
					$email_from_name=$email_temp->from_address;
					
					$email_address_from=$email_temp->from_address;
					$email_address_reply=$email_temp->reply_address;
					
					$email_subject=$email_temp->subject;				
					$email_message=$email_temp->message;			
					
									
					$email_to=$agent_email;		
					
					
					
					$email_message=str_replace('{break}','<br/>',$email_message);
					$email_message=str_replace('{agent_name}',$agent_name,$email_message);			
					$email_message=str_replace('{agent_email}',$agent_email,$email_message);
					$email_message=str_replace('{agent_signup_link}',$agent_signup_link,$email_message);
					$email_message=str_replace('{agent_signup_link_start}',$agent_signup_link_start,$email_message);
					$email_message=str_replace('{agent_signup_link_end}',$agent_signup_link_end,$email_message);
					
					$email_temp_url=upload_url().'email/';
					$email_message=str_replace('{email_temp_url}',$email_temp_url,$email_message);
					
					
					$str=$email_message;		
					
					
					/** custom_helper email function **/
					
					email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name);
				
				
			
				
				}
				
				
			
			
			
			
			redirect('agent/list_invite_agent/15/0/'.$msg);
			
		
				
		}			
			
		
		$data['site_setting'] =	$site_setting;	
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/add_invite',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
		
		
		
	}
	
	function edit_invite($invite_id=0)
	{
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		$site_setting = site_setting();
		
		
		if($invite_id>0)
		{
			
			$invite_detail=$this->db->get_where('agent_invitation',array('invite_id'=>$invite_id));
						
			if(!$invite_detail) { redirect('agent/list_invite_agent/'); } 
			
			$invite_details=$invite_detail->row();
			
			
		} else {   redirect('agent/list_invite_agent/');  }
		
		
		
		$data["error"] = "";			
		$data['invite_id']=$invite_id;
		$data['agent_first_name']=$invite_details->agent_first_name;
		$data['agent_last_name']=$invite_details->agent_last_name;
		$data['agent_email']=$invite_details->agent_email;
		$data['agent_phone_no']=$invite_details->agent_phone_no;
		$data['agent_about']=$invite_details->agent_about;
		$data['agent_company_name']=$invite_details->agent_company_name;	
			
		
		$data['site_setting'] =	$site_setting;	
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/add_invite',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);
		$this->template->render();
	}
	
	
	
	function list_invite_agent($limit=15,$offset=0,$msg='')
	{
		//$check_rights=get_rights('list_all_pin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='4';
		$config['base_url'] = site_url('agent/list_invite_agent/'.$limit.'/');
		$config['total_rows'] = $this->agent_model->get_total_invite_agent_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->agent_model->get_all_invite_agent_result($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']='';
		$data['keyword']='';
		$data['search_type']='normal';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/list_invite_agent',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}
	
	
	function search_invite_agent($limit=15,$option='',$keyword='',$offset=0,$msg='')
	{
		//$check_rights=get_rights('list_all_pin');
		
		//if(	$check_rights==0) {			
		//	redirect('home/dashboard/no_rights');	
		//}
		
	
	
		$theme = getThemeName();
		$this->template->set_master_template($theme .'/template.php');
		
		if($_POST)
		{		
			$option=$this->input->post('option');
			$keyword=$this->input->post('keyword');
		}
		else
		{
			$option=$option;
			$keyword=$keyword;			
		}
		
		
		$keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));
		
		
		$this->load->library('pagination');

	
		$config['uri_segment']='6';
		$config['base_url'] = site_url('agent/search_invite_agent/'.$limit.'/'.$option.'/'.$keyword.'/');
		$config['total_rows'] = $this->agent_model->get_total_search_invite_agent_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		
		$data['result'] = $this->agent_model->get_all_search_invite_agent_result($option,$keyword,$offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$data['theme']=$theme;	
		
		$data['limit']=$limit;
		$data['option']=$option;
		$data['keyword']=$keyword;
		$data['search_type']='search';
		
		
		
		$data['site_setting'] = site_setting();
			
	
		
		$this->template->write_view('header_menu',$theme .'/layout/common/header_menu',$data,TRUE);
		$this->template->write_view('center',$theme .'/layout/agent/list_invite_agent',$data,TRUE);
		$this->template->write_view('footer',$theme .'/layout/common/footer',$data,TRUE);

		$this->template->render();	

	}
	
	
	function action_invite_agent()
	{
		
		/*$check_rights=get_rights('user_login');
		
		if(	$check_rights==0) {			
			redirect('home/dashboard/no_rights');	
		}*/
		
		
		
		
		$offset=$this->input->post('offset');
		 $limit=$this->input->post('limit');
		 $action=$this->input->post('action');
		 $invite_id=$this->input->post('chk');
		 $search_type=$this->input->post('search_type');
		
		 $option=$this->input->post('option');
		 $keyword=$this->input->post('keyword');
		
		$red_link='list_invite_agent/'.$limit;
		
		if($search_type=='search')
		{
			$red_link='search_invite_agent/'.$limit.'/'.$option.'/'.$keyword;
		}
		
		
		if($action=='delete')
		{		
			foreach($invite_id as $id)
			{			
				$this->db->query("delete from ".$this->db->dbprefix('agent_invitation')." where invite_id='".$id."'");
			}
			
			redirect('agent/'.$red_link.'/'.$offset.'/delete');
		}	
		
	
	}
	
	////===========invite part==========
	
	
}
?>